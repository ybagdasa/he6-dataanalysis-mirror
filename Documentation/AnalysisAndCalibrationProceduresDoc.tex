\documentclass[12pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage[pdftex]{graphicx} % support the \includegraphics command and options
 \DeclareGraphicsRule{*}{mps}{*}{} 


% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
%\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
\usepackage{bm}
\usepackage{amsmath}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{listings}
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
%\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC5
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents

\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%%% END Article customizations

\title{$^6$He $\beta$-$\nu$ Correlation Experiment Data Analysis and Calibration Procedures}
\author{Yelena Bagdasarova}
\date{\today} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\sloppy 
\maketitle
\tableofcontents

\section{Introduction}
This document contains an overview of data analysis and calibrations for the $^6$He Experiment and provides instructions on how to use the data analysis software and how to perform and apply the necessary calibrations in the software. 


\section{Calibrations}
\subsection{MWPC Position Calibration}
\subsubsection{Position Reconstruction in the Analyzer}
The X and Y positions of each event in the MWPC is determined by the center of gravity calculation on the charge collected by the 6 wire groups of the bottom cathode and the difference in charge deposited on the ends of the 21 capacitively coupled anode wires, respectively. This is done for both the experiment and the simulation in the Analyzer and experiment and simulation get their own position calibrations. For more detail on the electrode design and position reconstruction algorithm refer to section 3.1.1.2 of RH Thesis.

\begin{figure}[ht]
\begin{subfigure}[t]{0.52\textwidth}
%\vspace{0pt}
\centering
\includegraphics[height=.90\textwidth]{Figures/MWPCFigure.png} 
%\vspace{.1cm}
\subcaption{\label{fig:MWPCfig}{\footnotesize Drawing of the MWPC high voltage anode wire plane and the top and bottom grounded cathode wire planes.}}
\end{subfigure}%
%\hspace{.5 cm}
\quad
\begin{subfigure}[t]{0.52\textwidth}
%\vspace{0pt}
\centering
\includegraphics[height= .90\textwidth]{Figures/MWPCAnodeCapacitors.png} 
%\vskip 1cm
\subcaption{\label{fig:AnodeCaps}{\footnotesize Capacitor charge division scheme of anode wires.}}
\end{subfigure}
\caption{\label{fig:MWPCOverview}}
\end{figure}

\subsubsection{Position Calibration for Experiment and Simulation}
Calibration of the experiment is performed using an $^{55}$Fe x-ray source that shines uniformly on the active MWPC volume. In the simulation, the anode and cathode signals from the detected x-rays are simulated and reconstruction of the X and Y positions is performed in the Analyzer for both experiment and simulation. The reconstructed positions of the events forms sharp peaks in both X and Y corresponding to the known locations of the anode and cathode wires as shown in Figure~\ref{fig:MWPCPos}. Independent X and Y transformation functions are constructed and applied event by event to make the calculated locations of the peaks coincide with their geometric locations. These transformations are exported as a MWPCPos map which is used by the Analyzer to correct the position measurement for subsequent experiment data or simulation runs. 

\begin{figure}[ht]
	\begin{subfigure}[t]{0.33\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height=.80\textwidth]{Figures/MWPCImage.png} 
		%\vspace{.1cm}
		\subcaption{\label{fig:MWPCPeaks}{\footnotesize Calibrated MWCP image from $^{55}$Fe source.}}
	\end{subfigure}%
	%\hspace{.5 cm}
	\begin{subfigure}[t]{0.33\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height= .80\textwidth]{Figures/MWPC_X.png} 
		%\vskip 1cm
		\subcaption{\label{fig:MWPCImage}{\footnotesize X Spectrum.}}
	\end{subfigure}
	\begin{subfigure}[t]{0.33\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height= .80\textwidth]{Figures/MWPC_Y.png} 
		%\vskip 1cm
		\subcaption{\label{fig:MWPCImage}{\footnotesize  Y Spectrum}}
	\end{subfigure}
\caption{\label{fig:MWPCPos}}
\end{figure}

After the position calibration is performed for the experiment, the normalized signal distributions of triggered cathode wire groups for each event (Figure~\ref{fig:cathdist}) is compiled and exported as an \textbf{MWPCCathodeSignalMapXX.txt}, where XX is a calibration ID. This map is used by the Post Processor to recreate the position-dependent cathode triggered signal response for the simulation. For more information about the MWPC position calibration for the experiment refer to section 4.3 of RH thesis.  

\subsubsection{Simulating the anode and cathode signals}
In the MCP simulation, trjectories of the beta particles through the volume and ionization energy deposition in the gas mixture is simulated with GEANT4. Ionization events are generated by converting the energy deposited by the primary beta particle in each step into ion pairs based on the ionization energy of the MWPC gas mixture. The total number of electrons produced from all the steps from a primary beta is summed for each anode cell involved. This total number of ionization events, along with the mean and RMS of the location of the events (steps) along the anode wire (X) is passed to the Post Processor. 

In the PP, the electrons from the ionization events each produce their own avalanches. The locations of each electron along the anode wire is "recovered" based on a uniform distribution centered at the mean and RMS of all the electrons. The charge generated from an avalanche is determined from an exponential distribution. 

Anode charge division signal: The total charge from these avalanches from each anode wire is capacitively divided between the left and right anode signal based on the number of capacitors on either side of the anode wire. Eventually, this is then translated to the Y position for the primary beta event by the Analyzer.  
\begin{figure}[ht]
		\centering
		\includegraphics[height=.40\textwidth]{Figures/CathodeSignalResponse.png} 
		%\vspace{.1cm}
		\caption{\label{fig:cathdist}{\footnotesize Normalized distribution of triggered cathode wire groups for an avalanche occuring at some position in X}}
\end{figure}
Cathode charge distribution signal: the cathode triggered wire group response is looked up from the \textbf{MWPCCathodeSignalMapXX.txt} for each avalanche at its location in X based on a nearest neighbor approach. The response for a particular location in X (Figure~\ref{fig:cathdist}) is multiplied by the avalanche charge and summed over all avalanches of that event. This is then translated to the X position for the primary beta event by the Analyzer.

\subsection{MWPC Energy Deposition $(\Delta E)$ Calibration}
The energy deposited in the MWPC gas (ionization energy) from a single event is proportional to the size of the charge collected by the anodes (anode signal sum). The $^{55}$Fe energy spectrum in Figure~\ref{fig:MWPCenergyUncal} shows a histogram of the integrated signal for events occuring in a 2x2 mm area (bin) at some position on the anode plane. The right peak corresponds to the fully-deposited energy of the 5.9 keV X-ray, which serves to calibrate the absolute energy scale. However, as Figure~\ref{fig:gainmap} shows, the relative position of this energy peak varies from bin to bin across the MWPC. During the experiment calibration, the peak position is fit for each position bin, and unique to the experiment calibration, the Analyzer exports this gain map (\textbf{MWPCGainMapXX.txt}) to be used by the PP to fold in the MWPC energy response into the MC Simulation (since this is not directly simulated otherwise).   

To determine the correction for this non-uniformity of the gain, the gain for each 2x2 mm bin must be scaled to match the gain of the bin with the highest gain (dubbed the "lossless" gain) so that the peak positions in the energy spectra for all the bins is the same. This position-dependent scaling correction (MWPCGain map) is then applied event by event in the Analysis of subsequent data or simulations. The absolute scaling from channels to energy is then applied using the 5.9 keV peak position as an absolute reference (MWPCE map) (Figure~\ref{fig:MWPCenergyCal}).

Note that the MWPC position calibration must be done before performing the gain calibration.

\begin{figure}[ht]
	\begin{subfigure}[t]{0.30\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height=.80\textwidth]{Figures/FigMWPCEnergyUnCal.pdf} 
		%\vspace{.1cm}
		\subcaption{\label{fig:MWPCenergyUncal}{\footnotesize Uncalibrated anode signal sum spectrum.}}
	\end{subfigure}\hfill
	%\hspace{.5 cm}
	\begin{subfigure}[t]{0.30\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height= .80\textwidth]{Figures/FigMWPCEnergyCal.pdf} 
		%\vskip 1cm
		\subcaption{\label{fig:MWPCenergyCal}{\footnotesize Calibrated energy deposition spectrum.}}
	\end{subfigure}\hfill
	\begin{subfigure}[t]{0.30\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height= .80\textwidth]{Figures/FigMWPCGainMap.pdf} 
		%\vskip 1cm
		\subcaption{\label{fig:gainmap}{\footnotesize Gain Map: Relative anode signal sum of 5.9 keV energy peak as a function of position.}}
	\end{subfigure}
\caption{\label{fig:MWPCEnergy}{\footnotesize MWPC energy deposition specra and gain map from $^{55}$Fe calibration.}}
\end{figure}
\subsection{Other factors of simulation, energy broadening from ionization fluctuations and electrical noise->position peaks broadening}

\subsection{Software Methods for the MWPC Calibrations}
\subsubsection*{Experiment Calibration}
In order to calibrate the MWPC position reconstruction for the simulation, the experiment MWPC position and energy deposition calibrations must be performed first on $^{55}$Fe data. This is because the simulation relies on the \textbf{MWPCGainMapXX.txt} and \textbf{MWPCCathodeSignalMapXX.txt} from the experiment to simulate the MWPC detector response. An analysis script like the following is used to process the simulation data, apply the aformentioned MWPC calibrations.
\begin{verbatim}
SetPurpose 1 0
CalConfigExp Amp OFF
Calibration/CalibrateMWPCPreview Fe55_02192016 1 Fe55
#Calibration/ConfigCalMWPC Micro 0
#Calibration/CalibrateMWPC Fe55_02192016 1 11 Fe55
exit
\end{verbatim}
where the arguments of \verb|Calibration/CalibrateMWPCPreview| are \verb|<filenamePreffix>|, \verb|<fileID>|, and \verb|<SourceType>| = Bi207 or Fe55. The Preveiw function generates the root file \textbf{preffix\_id\_MWPCPreview.root}. The peak positions themselves must be determined by hand from the h\_Cathode\_X (X) and h\_Anode\_Pos (Y) histograms and copied into the \textbf{MWPCPosPeakXX.txt}, where XX is the chosen calibration ID. Once the peak position map is available, \verb|Calibration/CalibrateMWPC| can be run which 
\begin{itemize}
\item As of now simply copies over the peak position map into \textbf{MWPCPositionCalibrationXX.txt} to be used by the peak fitting calibration routines of the Analyzer which determine the position correction to be applied to the reconstructed MWPC position.
\item Applies the position calibration.
\item Generates the \textbf{MWPCCathodeSignalMapXX.txt} to be used to simulate the detector response in the PP.
\item Generates the \textbf{MWPCGainMapXX.txt} to be used to simulate the detector response in the PP and to correct for the gain non-uniformity for subsequent analysis of the simulation.
\item Generates \textbf{MWPCECalibrationXX.txt} to be used to set the absolute energy scaling for the charge collected by the anodes (anode signal sum) for the 5.9 keV energy peak.
\end{itemize}
The arguments for this function are \verb|<filenamePreffix>|, \verb|<fileID>|, \verb|<calID>| (input/output) and \verb|<SourceType>| = Bi207 or Fe55.
\subsubsection*{Simulation Calibration}
In order to calibrate the MWPC position reconstruction for the simulation, the corresponding calibration for the experiment must be performed first (see above). To perform the simulation calibration, a simulation of the $^{55}$Fe source must be run, including post processing with \textbf{MWPCGainMapXX.txt} and \textbf{MWPCCathodeSignalMapXX.txt} from a recent MWPC calibration of the experiment where XX is the calibration ID. An analysis script like the following is used to process the simulation data
\begin{verbatim}
SetPurpose 0 0
SetCalIDSim MWPCGainMap 11
CalConfigSim MWPCGainMap ON
#Calibration/CalibrateMWPCPreviewSim 360101
Calibration/ConfigCalMWPC Pol 0
Calibration/CalibrateMWPCSim 360101 12
exit
\end{verbatim}
where the arguments of \verb|Calibration/CalibrateMWPCPreviewSim| and \verb|Calibration/CalibrateMWPCSim| are \verb|<SimFileID>| and \verb|SimCalID|(input/output). Just as for the experiment calibration, the Preveiw function generates the root file \textbf{SimMWPCCalPreview\_id\_MWPCPreview.root}. The X and Y peak positions themselves must be determined by hand from the h\_Cathode\_X (X) and h\_Anode\_Pos (Y) histograms and copied into the \textbf{SimMWPCPosPeakXX.txt}, where XX is the chosen calibration ID. Once the peak position map is available, \verb|Calibration/CalibrateMWPCSim| can be run which 
\begin{itemize}
\item As of now simply copies over the peak position map into \textbf{SimMWPCPositionCalibrationXX.txt} to be used by the peak fitting calibration routines of the Analyzer which determine the position correction to be applied to the reconstructed MWPC position.
\item Applies the position calibration.
\item Generates \textbf{SimMWPCGainMapXX.txt} to be used to correct for the gain non-uniformity for subsequent analysis of the simulation.
\item Generates \textbf{SimMWPCECalibrationXX.txt} to be used to set the absolute energy scaling for the charge collected by the anodes (anode signal sum) for the 5.9 keV energy peak.
\end{itemize}

\subsection{Scintillator-PMT Energy Response}
The energy deposited into the Scintillator by the primary beta event $E_{dep}$ is proportional to the integrated charge read out on the PMT cathode $Q_{int}$. For an ideal detector, this relationship is linear and can be determined by first fitting the $^{207}$Bi integrated charge spectra to an anlaytical function to find the peak positions $Q_{int}(peak)$ as shown in Figure~\ref{fig:ScintPeaks}, and then fitting those to the positions of the simulated peaks $E_{dep}^{sim}(peak)$ with a linear function as shown in Figure~\ref{fig:LinearEnergy}. The transformation applied to the PMT readout is then 
\begin{equation}
E_\beta^{exp} = a_{fit}*Q_{int}+b_{fit}
\label{eqn:linearfunc}  
\end{equation}
where $a_{fit}$ and $b_{fit}$ are the determined fit parameters. Since the response isn't perfectly linear (reasons for this outlined in section 4.7 of RH thesis) an approximate Gaussian response function $F(E_\beta^{sim},E_{dep},\sigma_{peak})$ is applied in the PP to map $E_{dep} \rightarrow E_\beta^{sim}$ where $\sigma_{peak}$ is a parameter used to match the 975.7 keV peak region in the simulated spectrum to the calibrated experiment spectrum via $\chi^2$ minimization. The matching is done recursively, as the adjustment of $\sigma_{peak}$ affects the position of simulated peaks and thus the fit parameters of Equation~\ref{eqn:linearfunc}. 
\begin{figure}[ht]
	\begin{subfigure}[t]{0.45\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height=.80\textwidth]{Figures/FigPMTSpectrumAnaCal.pdf} 
		%\vspace{.1cm}
		\subcaption{\label{fig:ScintPeaks}{\footnotesize Analytical fit of the $^{207}$Bi energy deposition spectrum of the Scintillator-PMT coincident with the MWPC.}}
	\end{subfigure}\hfill
	%\hspace{.5 cm}
	\quad
	\begin{subfigure}[t]{0.45\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height= .80\textwidth]{Figures/FigPMTEPeakFit.pdf} 
		%\vskip 1cm
		\subcaption{\label{fig:LinearEnergy}{\footnotesize Linear fit of the 3 peak positions from experiment (integrated charge) vs simulation (energy deposited in keV).}}
	\end{subfigure}
	\caption{\label{fig:ScintOverview}}
\end{figure}
\subsubsection{Software Methods}
\subsubsection*{Simulation Calibration}
In order to calibrate the Scint-PMT Energy Response, the simulation calibration must be performed first. To perform the simulation calibration, a simulation of the $^{207}$Bi source must be run, and ideally, all of the MWPC calibrations are applied in the Analysis of the data before performing the Scint-PMT Energy Response calibrations. This includes  
\begin{itemize}
	\item Post Processing with \textbf{MWPCGainMapXX.txt} and \textbf{MWPCCathodeSignalMapXX.txt} from a recent MWPC calibration of the experiment.
	\item Analysis using pre-existing MWPC calibrations: 
	\begin{itemize}
		\item MWPC position correction from simulation: \textbf{SimMWPCPosCalibrationXX.txt} 
		\item MWPC $\Delta E$ correction from simulation: \textbf{SimMWPCECalibrationXX.txt}
		\item MWPC gain uniformity correction from experiment: \textbf{MWPCGainMapXX.txt} 
	\end{itemize}
\end{itemize}
where XX is the calibration ID. If the MWPC calibrations are unavailable, default calibrations will be performed in their place, and the Scint-PMT Calibration will remain largely unaffected. 
An analysis script like the following is used to process the simulation data, apply the aformentioned MWPC calibrations, and fit the three energy peaks of Figure~\ref{fig:ScintPeaks}.
\begin{verbatim}
SetPurpose 0 0
SetCalIDSim MWPCPos 6
CalConfigSim MWPCPos ON
SetCalIDSim MWPCE 6
CalConfigSim MWPCE ON
SetCalIDSim MWPCGainMap 10
CalConfigSim MWPCGainMap ON
CalConfigSim MWPCEffMap OFF
SetCondition MWPC_R 0 14.0
Calibration/CalibrateBetaSim 280101 11 Bi207
exit
\end{verbatim}
where the arguments of \verb|Calibration/CalibrateBetaSim| are \verb|<SimFileID>|, \verb|SimCalID|(output), and \verb|<SourceType>| = Bi207 or Fe55. 
The calibration generates the file  \textbf{SimScintCalibrationXX.txt} which contains the fit parameters of the energy peaks in keV (listed in Table 4.1 of RH Thesis). It also generates the root file \textbf{SimBetaCal\_simId.root} which contains the relevent histograms pertaining to the MWPC and Scint-PMT Energy Response. 
\subsubsection*{Experiment Calibration}
In order to calibrate the Scint-PMT Energy Response, the simulation calibration (outlined above) must be performed first. The analysis script for the experiment calibration looks like 
\begin{verbatim}
SetPurpose 1 0
SetMode Beta
CalConfigExp Amp OFF
SetCalIDExp MWPCPos 10
CalConfigExp MWPCPos OFF
SetCalIDExp MWPCE 10
CalConfigExp MWPCE OFF
SetCalIDExp MWPCGainMap 10
CalConfigExp MWPCGainMap OFF
ConfigBackground OFF
SetCondition MWPC_R 0 14.0 
Calibration/CalibrateBeta Bi207_03092016 1 32 10 Bi207 OFF
exit
\end{verbatim}
where the arguments of \verb|Calibration/CalibrateBeta| are \verb|<filenamePreffix>|, \verb|<fileID>|, \verb|ExpCalID|(output), \verb|SimCalID|(input), \verb|<SourceType>| = Bi207 or Fe55, and \verb|<ScintGainMapSwitch>| = ON or OFF.
The calibration generates the file  \textbf{ScintCalibrationXX.txt} which contains the fit parameters of the energy peaks in channel units. It also generates the root file \textbf\textbf{preffix\_id\_BetaCal.root} which contains the relevent histograms pertaining to the MWPC and Scint-PMT Energy Response. 
To implement the calibration during analysis, the commands \verb|SetCalIDExp Scint <ExpCalID>| and \verb|CalConfigExp Scint ON| are called before processing along with the simulation counterparts \verb|SetCalIDSim Scint <ExpSimID>| and \verb|CalConfigSim Scint ON|. If the calibrations are off, an approximate ``raw" calibration will be performed by default.   
\subsection{MCP Position Calibration}
The X and Y hit positions of events on the MCP are determined by the difference of collected charge pulse arrival times at the two ends of the two perpendicular delay line anodes as described in section 3.3 of RH thesis. The position calibration consists of correcting distortions of the reconstructed image of the precisely machined MCP mask cast generated by diffuse (untrapped) $^6$He decays or the $^{241}$Am $\alpha$ source by making the image match the geometry of the mask.

In the calibration, the grid cross points and 3 points in between are first located for each grid square as shown by the crosses in Figure ~\ref{fig:GridSquare}. Coefficients of local 2nd order polynomials are then fit in order to transform the cross points in each grid square to their ideal locations. The local coefficents for each square are saved to the MCPPos map, which is used by the Analyzer to correct the reconstructed MCP position for subsequent experiment runs. The calibration also outputs the corrected MCP image which is then recalibrated to determine the accuracy and resolution of the MCP position calibration. The resolution is input as a detector response parameter in the PP of the MC Simulation.     

To correct the reconstructed MCP position using the position calibration, the Analyzer algorithm first identifies which square each event belongs to before applying the transformation. In the process, it enforces a fiducuial grid area cut as shown in Figure~\ref{fig:GridCut}.

\begin{figure}[ht]
	\begin{subfigure}[t]{0.45\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height=.90\textwidth]{Figures/FigMCPGridOn2.pdf} 
		%\vspace{.1cm}
		\subcaption{\label{fig:GridSquare}{\footnotesize Grid crossings and in between points of the MCP mask image identified by the MCP position calibration algorithm.}}
	\end{subfigure}\hfill
	%\hspace{.5 cm}
	\quad
	\begin{subfigure}[t]{0.45\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height= .90\textwidth]{Figures/FigMCPPosCal.pdf} 
		%\vskip 1cm
		\subcaption{\label{fig:GridCut}{\footnotesize Calibrated MCP image from diffuse (untrapped) $^6$He decays.}}
	\end{subfigure}
	\caption{MCP Position Calibration\label{fig:MCPCalibration}}
\end{figure}

\subsubsection{Software Methods}
The calibration of the MCP Position is performed in 3 steps using the Analyzer macro script \textbf{CalibrationMCPPos.mac}. The main components of the script are the set purpose and mode parameters which are set for Experiment the the MCP mode
\begin{verbatim}
SetPurpose 1 0
SetMode MCP
\end{verbatim}
the X/Y MCP anode time sum conditions, and the commands to run the calibration processes:
\begin{verbatim}
#03092016#######################
SetCondition TXSumLow 0 79.0
SetCondition TXSumHigh 0 86.0
SetCondition TYSumLow 0 86.0
SetCondition TYSumHigh 0 97.0
Calibration/ConfigCalMCP Local -0.22 -0.72 1.052 0.982 37.8
#Calibration/CalibratePreviewMCP Diffuse_Intense_He6_03092016 1
Calibration/CalibrateMCP Diffuse_Intense_He6_03092016 1 95 Grid
#Calibration/CalibrateMCPRes Diffuse_Intense_He6_03092016 1 95
\end{verbatim}
The first command to run (only) is \verb|Calibration/CalibratePreviewMCP| followed by \verb|<filenamePreffix>| and \verb|<fileID>|. This generates the file \textbf{preffix\_id\_MCPPreview.root} in the Histograms directory. After checking out the image, comment out the PreviewMCP command and run the two commands \verb|Calibration/ConfigCalMCP| to configure the pre-calibration and \verb|Calibration/CalibrateMCP| to perform the calibration. The arguments of the former are \verb|<-x0> <-y0>|, where (x0,y0) are the coordinates of the center square (the origin), \verb|<XScale> <YScale>|, position scaling factors wrt to the origin, and \verb|<r0>|, the initial radius cut. These are roughly determined by the user from the MCPPreview image and are used to help the alogrithm more easily find the cross-points. They are usually consistent from run to run and are not expected to change.
The arguments of \verb|Calibration/CalibrateMCP| are again \verb|<filenamePreffix>| and \verb|<fileID>| followed the by assigned calibration ID \verb|<CalID>| and the cut option (\verb|Grid| or \verb|Radius|). The calibration generates the file \textbf{preffix\_id\_MCPCal.root} which contains the preview image, the pre-calibrated image (shifted and scaled) (Figure~\ref{fig:MCPPreCal}), and the final calibrated image (with the grid cut) (Figure~\ref{fig:GridCut}). The file also contains a histogram of the deviation of the pre-calibrated locations of the grid crosspoints and in-between points to the ideal locations based on the mask (Figure~\ref{fig:GridCut}). The calibration also outputs the file \textbf{MCPPosLocalCalibrationXX.txt} which is subsequently used on future analysis to correct the MCP Position readout. 

\begin{figure}[ht]
	\begin{subfigure}[t]{0.45\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height=.90\textwidth]{Figures/FigMCPPreCal.pdf} 
		%\vspace{.1cm}
		\subcaption{\label{fig:MCPPreCal}{\footnotesize Pre-calibrated MCP image (shifted, stretched, and with radius cut) from diffuse (untrapped) $^6$He decays.}}
	\end{subfigure}\hfill
	%\hspace{.5 cm}
	\quad
	\begin{subfigure}[t]{0.45\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height= .90\textwidth]{Figures/FigMCPCalDev.pdf} 
		%\vskip 1cm
		\subcaption{\label{fig:GridCut}{\footnotesize Histogram of the deviation of the pre-calibrated locations of the grid crosspoints and in-between points to the ideal locations based on the mask.}}
	\end{subfigure}
	\caption{\label{fig:MCPCalibration2}}
\end{figure}

Finally, the command \verb|Calibration/CalibrateMCPRes| which takes the arguments \verb|<filenamePreffix> <fileID> <calID>| recalibrates the corrected MCP image to asses the accuracy and resolution of the calibration. It generates the file \textbf{preffix\_id\_MCPCalRes.root} which contains relevent histograms discussed in section 4.9.3 of RH thesis. 
\subsection{Geometry and Field Calibration with $^4$He Photoions} 

\subsection{Background Normalization and Subtraction}
Much of the background can be elliminated by recondstructing the total energy (Q-values) of the events assuming they belong to either charge state 1 or 2 and then cutting out events with Q-values that are not within a chosen range of the known endpoint of the decay (Q Cut). However, some events from untrapped $^6$He decays (with unknown initial state variables) are not removed from the TOF region of the trapped decays. The procedure to remove this ``background" involves measuring the triple coincidence TOF spectrum of untrapped $^6$He decays (at a low rate of about 30 Hz to avoid random coincidences backgrounds not present in data), normalizing it to the unphysical parts of the data TOF spectrum (10 ns < TOF < 110 ns) before the Q Cut is applied (Figure~\ref{fig:BkgNoQCut}), and subtracting the normalized spectrum after the Q Cut is applied to it and the data (Figure~\ref{fig:BkgQCut}).
\begin{figure}[ht]
	\begin{subfigure}[t]{0.45\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height=.90\textwidth]{Figures/FigBkgComparison1.pdf} 
		%\vspace{.1cm}
		\subcaption{\label{fig:BkgNoQCut}{\footnotesize Comparison of triple-coincidence TOF spectra from trapped and untrapped decays before the Q cut is applied. The untrapped spectrum is normalized to the trapped spectrum in the region 10 ns < TOF < 110 ns.}}
	\end{subfigure}\hfill
	%\hspace{.5 cm}
	\quad
	\begin{subfigure}[t]{0.45\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height= .90\textwidth]{Figures/FigBkgComparison2.pdf} 
		%\vskip 1cm
		\subcaption{\label{fig:BkgQCut}{\footnotesize Comparison of TOF spectra from trapped and untrapped decays after normalization and the Q cut is applied. The red curve constitutes the background to be subtracted from the blue curve.}}
	\end{subfigure}
	\caption{\label{fig:MCPCalibration2}}
\end{figure}
\subsubsection{Software Methods}
The analysis of the diffuse untrapped $^6$He data for background histogram construction must be done with all of the calibrations and conditions in place, as they would be for the data itself. As seen from the script below, the diffuse data will be processed twice: once with the QCut OFF and once with it ON. If histograms are ``combined", they must be ``put back" into the first element of the histogram array before the command \verb|ConstructBkgHistogram <flag>| is issued, where \verb|<flag>| indicates whether the Q cut is OFF or ON for that histogram. \verb|SaveBkgHistogram <id>| saves the histograms to root file \textbf{ExtBkgHistograms\_id.root}.   

\begin{verbatim}
ConfigHistogram TOFHist -1000 2000 3000
SetCondition TOFLow 0 -1000
SetCondition TOFHigh 0 2000
SetFileExpDim 4 0
SetFileIDListExp 0 1 1
#
DisableBkgHist All
#
#Process Input
CondFuncSwitch ExpCond_QValue OFF
ProcessInputExp 1 0 0
CombineExpHistograms 1 OFF
PutBackCombHistograms
DeleteCombHistList
#Save Histograms
SaveHistograms Diffuse_LowRate_He6_03092016_1500 Array
ConstructBkgHistogram All
CondFuncSwitch ExpCond_QValue ON
#ConfigHistogram TOFHist 100 500 400
ProcessInputExp 1 0 0
CombineExpHistograms 1 OFF
PutBackCombHistograms
ConstructBkgHistogram Q_Cut_ON
SaveBkgHistogram 23
exit
\end{verbatim}

Once the background histograms are constructed, they need to be normalized to the data in the non-physical region of the TOF before the Q Cut is implemented. Assuming the data histograms are loaded into the [0][0] unit (without Q Cut), the following commands load the background histograms, determine the norm, and output the root file \textbf{BkgComparison\_DMarAllBkgMar09.root}.
\begin{verbatim}
######Background#################################
LoadBkgHistogram 23
NormalizeBkg DMarAllBkgMar09
#SetBkgNorm  0.108804
#ExpBkgSubtraction MarRun2
#################################################
\end{verbatim}
Likewise, to apply the background subtraction, one must load the data histograms into the [0][0] unit (with the Q Cut applied) and run the following command sequence, replacing the number after \verb|SetBkgNorm| with the calculated norm in the previous step.
 \begin{verbatim}
######Background#################################
LoadBkgHistogram 23
#NormalizeBkg DMarAllBkgMar09
SetBkgNorm  0.108804
ExpBkgSubtraction MarRun2
#################################################
\end{verbatim}
Following this command, the histogram in the [0][0] TOFHist unit will be background subtracted. The root file \textbf{BkgSubtraction\_MarRun2.root} is output and contains the overlaid data, background, and background subtracted TOF spectra.

\subsection{MOT-MCP Distance Calibration Using Photion TOF}
Assuming the field is simulated correctly, the MOT-MCP distance (MOT Z Position) is extracted from a TOF measurement of $^4$He or $^6$He photoions. The atoms in the MOT are ionized using a pulsed nitrogen laser that is aligned with the MOT cloud. The laser pulse signal is fed into a photodiode to act as a trigger and is also picked up on the PMT. For an ion of zero initial velocity, the relation between the measured TOF and the field parameters is
 
\begin{equation}\label{eq:TOF}
 TOF(\alpha_0) - t_0= \sqrt{\frac{2m}{kq}}\sqrt{\alpha} \propto \frac{1}{\sqrt{k}}
\end{equation}

where in the case of a uniform field $\alpha\equiv \frac{Z}{E_0}$, $E_0 = \langle E_z \rangle$ is the electric field, $Z$ is the MOT-MCP distance, and $m$ and $q$ are the ion mass and charge respectively.
To determine the timing offset of the system $t_0$, the photion TOF is measured as a function of the field scaling $k$, the TOF peaks are fit to Gaussians to extract the centroids, and $t_0$ is fit in ~\ref{eq:TOF} (Figure ~\ref{fig:ScalingTOF}). To find the MOT-MCP distance for a given field configuration, a systematic simulation study is performed to determine the linear relation between the photion TOF and position of the MOT (Figure ~\ref{fig:dTOF/dZ}). This relation is used to solve for the MOT position, so that the experiment and simulation TOF peaks agree. 
\begin{figure}[ht]
	\begin{subfigure}[t]{0.45\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height=.90\textwidth]{Figures/He4TOFplot.png} 
		%\vspace{.1cm}
		\subcaption{\label{fig:ScalingTOF}{\footnotesize $^4$He photoion TOF peak location as a function of the field scaling parameter $k$ fit to ~\ref{eq:TOF}.}}
	\end{subfigure}\hfill
	%\hspace{.5 cm}
	\quad
	\begin{subfigure}[t]{0.45\textwidth}
		%\vspace{0pt}
		\centering
		\includegraphics[height= .90\textwidth]{Figures/March07He6ZvsTOFSysStudy.png} 
		%\vskip 1cm
		\subcaption{\label{fig:dTOF/dZ}{\footnotesize Systematic simulation study of the fit of the $^6$He ion TOF as a function of the MOT Position.}}
	\end{subfigure}
	%\caption{\label{fig:MCPCalibration2}}
\end{figure}
\subsubsection{Software Methods}
\subsubsection*{Experiment}
The TOF data can be analyzed in two modes: He4 mode if the the laser pulse in the photodiode is recorded and Doubles where the pulse is detected by the Scintillator-PMT detector. To perform the field scaling study on the experiment, an analysis script would look as follows:
\begin{verbatim}
SetPurpose 1 0
#SetMode He4
SetMode Double
SetExpFilePrefix He4_photoions_03252016
#SetParameter TOFShift 0 -1.17
#SetParameter TOFShift 0 83.4097
ConfigBackground OFF
ConfigHistogram TOFHist -100 500 5200
DisableBkgHist All
########################################
#Conditions
########################################
SetCondition MCP_R 0 40
SetCondition MWPC_R 0 14.0
SetCondition TXSumLow 0 80.0 
SetCondition TXSumHigh 0 86.0
SetCondition TYSumLow 0 88.0
SetCondition TYSumHigh 0 95.0
SetCondition ScintThreshold 0 20.0
CondFuncSwitch ExpCond_QValue OFF
SetCondition TOFLow 0 -100
SetCondition TOFHigh 0 500
#ConfigPushBeamVeto ON 250 40
########################################
SetFileExpDim 5 1
#FileIDs for User_PhotoIonTOFStudy
SetFileIDListExp 0 1 1
#Process Input
ProcessInputExp 1 0 0
#SaveHistograms He4_PhotoIon_03252016_Fields_Double Array
User_PhotoIonTOFStudy 1 -0.1625 1.5661 0.0 Mar25_FieldFits_Double
#User_CompareSpectra TOFHist 5 None 0 False 0 Mar25_Fields
exit
\end{verbatim}
where the argurments of the command \verb|User_PhotoIonTOFStudy| are \verb|<k_start>|, \verb|<k_increment>|, \verb|<E0>|, \verb|<E0_err>|, and \verb|<RootOutName>| (\verb|<E0>| and \verb|<E0_err>| are only relevent in the case of a known perfectly uniform field.) The root file \textbf{UserHist\_PhotoionTOFMar25\_FieldFits\_Double.root} would be generate by the script above, along with a fit of $t_0$ and the coefficient of $1/\sqrt{k}$ in Eq.~\ref{eq:TOF}. To implement the $t_0$ correction for subsequent analysis runs, issue the command \verb|SetParameter TOFShift <-t0>| before processing the experiment data, where the sign is opposite to the sign of $t_0$.

Fitting and monitoring of the $^6$He photoion TOF peaks as a function of data run can be by issuing the \verb|User_Monitor PhotoIonZ <OutName>| command after cutting on the 2D ScintE vs TOF region to isolate the photoion events, processing the data, and setting the fit configuration to Gaussian with the command \verb|FitConfig Gauss OFF 0|. The root file \textbf{UserMonitorHist\_PhotoIonZ\_OutName.root} is generated and contains plots like Figure ~\ref{fig:MonitorIons} for the TOF and TOF widths as a function of run number.
\begin{figure}[ht]
		%\vspace{0pt}
		\centering
		\includegraphics[height=.70\textwidth]{Figures/UserMonitor_PhotoIonZ_Mar08_TOF.png} 
		%\vspace{.1cm}
		\caption{\label{fig:MonitorIons}{\footnotesize $^6$He photoion TOF peak location as a function of run number.}}
\end{figure}
\subsubsection*{Simulation}
To perform a systematic study of the TOF with respect to the MOT Position, the following script could be used:
\begin{verbatim}
SetPurpose 0 0
SetMode He4
ConfigBackground OFF
ConfigHistogram TOFHist -100 500 5200
DisableBkgHist All
#######Conditions###############################
SetCondition MCP_R 0 40
SetCondition MWPC_R 0 14.0
SetCondition TOFLow 0 100
SetCondition TOFHigh 0 500
#############################################
SetFileSimDim 10 1
#SetFileIDListSim index ID-G ID-B ID-I ID-P Inc
SetFileIDListSim 0 0 86 26 1 Ion
#Process Input
FitConfig Gauss OFF 0
#SystematicStudy_File Dim fitrangelow fitrangehigh...
#...Purpose1 low1 interval1 XUnit Purpose2 low2 interval2 YUnit
SystematicStudy_File 1 100 500 ZPosition -5 .5 [mm]
#ProcessInputSim 1 0 0
#######Campare Simulation and Experiment############
SetPurpose 1 0
SetMode He4
LoadHistograms He4_PhotoIon_03252016_Fields_Double Array
User_CompareSpectra TOFHist 1 TOFHist 10 True 0 Mar25_Fields_ExpvsSim
exit
\end{verbatim}
The fit mode is set to Gaussian with the command \verb|FitConfig Gauss OFF 0| and the ususal command for a systematic TOF study using files is run: \verb|SystematicStudy_File| where the arguments are listed in a comment within the script.  
\end{document}



