//      Ran Hong
//      Aug 13th 2013
//
//	Convert .fast file into a root Tree within a root file
//	usage:
//	make He6_Faster2Root
//	./He6_Faster2Root filename_prefix(no number or .fast) number_of_file
//
// 	adjust nmax if only a sample is required
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TRandom.h"
#include "TGraphErrors.h"

const double nmax= 100000000;
using namespace std;

//Define data structures


typedef struct DataStruct_MWPC_X{
  double AX1;
  double AX2;
  double AX3;
  double AX4;
  double AX5;
  double AX6;
}DataStruct_MWPC_X;

typedef struct DataStruct_MWPC_Y{
  double AY1;
  double AY2;
  double AY3;
  double AY4;
  double AY5;
  double AY6;
}DataStruct_MWPC_Y;

typedef struct DataStruct_TMWPC_X{
  double MW_TX1;
  double MW_TX2;
  double MW_TX3;
  double MW_TX4;
  double MW_TX5;
  double MW_TX6;
}DataStruct_TMWPC_X;

typedef struct DataStruct_TMWPC_Y{
  double MW_TY1;
  double MW_TY2;
  double MW_TY3;
  double MW_TY4;
  double MW_TY5;
  double MW_TY6;
}DataStruct_TMWPC_Y;

typedef struct DataStruct_QMCP_anodes{
  double QX1;
  double QX2;
  double QY1;
  double QY2;
}DataStruct_QMCP_anodes;

typedef struct DataStruct_TMCP_anodes{
  double TX1;
  double TX2;
  double TY1;
  double TY2;
}DataStruct_TMCP_anodes;



int main (int argc, char** argv) {
  TH1F *dummy = new TH1F("dummy", "dummy", 10, 0, 1);
  TFile *dummy_file = new TFile("dummy.root","recreate");
  TTree *dummytree = new TTree("dummytree","dummytree",99);

  string inputfile;
  string spectrumfile;
  char filenamebuf[30];
  int NFile;                           
  //  double TMCP_ref;	//  Time ref for first event
  int ReadOutType;

  double EnergyThreshold = 100000;
  double EnergyUpperBound = 1400000;

  int n = 0;  //  nb of data read
  int nk = 0; //  nb of data kept

  double Tref = 0;
  double Tref_A = 0;
  double QPMT_A;
  double TPMT_A_rel;
  double QPMT_D;
  double TPMT_DA;
  double AMWPC_anode;
  double AMWPC_anode1;
  double AMWPC_anode2;
  double TMWPC_rel;
  double TBeta_diff;
  DataStruct_MWPC_X MWPC_X;
  DataStruct_TMWPC_X TMWPC_X;
  DataStruct_MWPC_Y MWPC_Y;
  DataStruct_TMWPC_Y TMWPC_Y;
  double QMCP;
  DataStruct_QMCP_anodes QMCP_anodes;
  DataStruct_TMCP_anodes TMCP_anodes;
  double TMCP_rel;
  double TOF;
  double LED_A;
  double T_LED;
  double T_LED_PMT;

  double GroupTime;

  unsigned short TriggerMap;

  long double TScint,TX1,TX2,TY1,TY2,TMCP;
  double TMWPC_anode;
  double TMWPC_X1,TMWPC_X2,TMWPC_X3,TMWPC_X4,TMWPC_X5,TMWPC_X6;
  double TMWPC_Y1,TMWPC_Y2,TMWPC_Y3,TMWPC_Y4,TMWPC_Y5,TMWPC_Y6;

  double TX1mTX2,TX1pTX2,TY1mTY2,TY1pTY2;
  double TMWPCX_rel,TMWPCY_rel,TMCP_anodes_rel;
  double MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY;

  if (argc < 2) {   //  command args & usage
    printf ("usage : \n");
    printf ("        %s  filename\n", argv[0]);
    printf ("\n");
    return EXIT_SUCCESS;
  }

  //Get Exp Data directory from environment
  char *ExpDataDirect;
  ExpDataDirect=getenv("HE6_EXPERIMENT_DATA_DIRECTORY");
  if (ExpDataDirect==NULL){
    cout << "The environment variable HE6_EXPERIMENT_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }

  // output root file
  TString fName = string(ExpDataDirect) + string("/") + string(argv[1]);
  fName += ".root";
  TFile *f = new TFile (fName.Data(), "read");

  // root tree
  //Trees for calibration
  TTree *Tree_Scint = (TTree*)f->Get("Tree_Scint");
  TTree *Tree_MWPC = (TTree*)f->Get("Tree_MWPC");	//Exclude Scint event
  TTree *Tree_Beta = (TTree*)f->Get("Tree_Beta");	//Exclude tripple trigger
/*
  TTree *Tree_MWPC_Iso = (TTree*)f->Get("Tree_MWPC_Iso");
  TTree *Tree_MWPCX_Iso = (TTree*)f->Get("Tree_MWPC_Iso");
  TTree *Tree_MWPCY_Iso = (TTree*)f->Get("Tree_MWPC_Iso");
*/
  // Branches  

  Tree_Scint->SetBranchAddress("Event_No",&n);
  Tree_Scint->SetBranchAddress("GroupTime",&GroupTime);
  Tree_Scint->SetBranchAddress("QPMT_A",&QPMT_A);
  Tree_Scint->SetBranchAddress("QPMT_D",&QPMT_D);
  Tree_Scint->SetBranchAddress("TPMT_A_rel",&TPMT_A_rel);
  Tree_Scint->SetBranchAddress("TPMT_DA",&TPMT_DA);
  Tree_Scint->SetBranchAddress("TriggerMap",&TriggerMap);

  Tree_MWPC->SetBranchAddress("Event_No",&n);
  Tree_MWPC->SetBranchAddress("GroupTime",&GroupTime);
  Tree_MWPC->SetBranchAddress("AMWPC_anode1",&AMWPC_anode1);
  Tree_MWPC->SetBranchAddress("AMWPC_anode2",&AMWPC_anode2);
  Tree_MWPC->SetBranchAddress("TMWPC_rel",&TMWPC_rel);
  Tree_MWPC->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_MWPC->SetBranchAddress("TMWPC_X",&TMWPC_X);
  Tree_MWPC->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_MWPC->SetBranchAddress("TMWPC_Y",&TMWPC_Y);
  Tree_MWPC->SetBranchAddress("TriggerMap",&TriggerMap);

  Tree_Beta->SetBranchAddress("Event_No",&n);
  Tree_Beta->SetBranchAddress("GroupTime",&GroupTime);
  Tree_Beta->SetBranchAddress("QPMT_A",&QPMT_A);
  Tree_Beta->SetBranchAddress("TPMT_A_rel",&TPMT_A_rel);
  Tree_Beta->SetBranchAddress("AMWPC_anode1",&AMWPC_anode1);
  Tree_Beta->SetBranchAddress("AMWPC_anode2",&AMWPC_anode2);
  Tree_Beta->SetBranchAddress("TBeta_diff",&TBeta_diff);
  Tree_Beta->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_Beta->SetBranchAddress("TMWPC_X",&TMWPC_X);
  Tree_Beta->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_Beta->SetBranchAddress("TMWPC_Y",&TMWPC_Y);
  Tree_Beta->SetBranchAddress("TriggerMap",&TriggerMap);

/*
  Tree_MWPC_Iso->SetBranchAddress("Event_No",&n);  
  Tree_MWPC_Iso->SetBranchAddress("AMWPC_anode",&AMWPC_anode);
  Tree_MWPC_Iso->SetBranchAddress("TMWPC_rel",&TMWPC_rel);

  Tree_MWPCX_Iso->SetBranchAddress("Event_No",&n);
  Tree_MWPCX_Iso->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_MWPCX_Iso->SetBranchAddress("TMWPCX_rel",&TMWPCX_rel);

  Tree_MWPCY_Iso->SetBranchAddress("Event_No",&n);
  Tree_MWPCY_Iso->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_MWPCY_Iso->SetBranchAddress("TMWPCY_rel",&TMWPCY_rel);
*/
  // root histos
  TH1D *h_Scint = new TH1D("h_Scint","Scint. alone spectrum",1024,0,2048000);
  TH1D *h_MWPC  = new TH1D("h_MWPC","MWPC Anode alone spectrum",1024,0,256000);
  TH1D *h_Scint_Coin = new TH1D("h_Scint_Coin","Scint. coincidence spectrum",1024,0,2048000);
  TH1D *h_MWPC_Coin  = new TH1D("h_MWPC_Coin","MWPC Anode coincidence spectrum",1024,0,256000);
  TH2D *h_MWPC_Scint = new TH2D("h_MWPC_Scint","MWPC vs Scint 2D",1024,0,65536,1024,0,2048000);
  TH1 *h_FiredWireX = new TH1D("h_FiredWireX","# of fired stripes X",10,0,10);
  TH1 *h_FiredWireY = new TH1D("h_FiredWireY","# of fired stripes Y",10,0,10);

  TH1D *h_Cathode_X = new TH1D("h_Cathode_X","MWPC CathodeX position spectrum",100,-25,25);
  TH1D *h_Cathode_Y = new TH1D("h_Cathode_Y","MWPC CathodeY position spectrum",100,-25,25);
  TH2D *h_Cathode_Image = new TH2D("h_Cathode_Image","2D image of MWPC",100,-25,25,100,-25,25);
  TH2D *h_Cathode_Image_Coin = new TH2D("h_Cathode_Image_Coin","2D image of MWPC in coincidence with Scint",100,-25,25,100,-25,25);
  TH1D *h_Charge_X = new TH1D("h_Charge_X","Total Charge on MWPC X",1024,0,51200);
  TH1D *h_Charge_Y = new TH1D("h_Charge_Y","Total Charge on MWPC Y",1024,0,51200);

  TH2D *h_ChargeX_vs_Anode = new TH2D("h_ChargeX_vs_Anode","ChargeX vs Anode",1024,0,51200,1024,0,256000);
  TH2D *h_ChargeY_vs_Anode = new TH2D("h_ChargeY_vs_Anode","ChargeY vs Anode",1024,0,51200,1024,0,256000);
  TH2D *h_ChargeX_vs_ChargeY = new TH2D("h_ChargeX_vs_ChargeY","ChargeX vs ChargeY",1024,0,204800,1024,0,204800);
  TH2D *h_TotCharge_vs_Anode = new TH2D("h_TotCharge_vs_Anode","Total Charge vs Anode",1024,0,204800,1024,0,256000);
  //Time spectrum
  TH1D *h_Scint_T = new TH1D("h_Scint_T","Scint. alone time spectrum",900,0,900);
  TH1D *h_Coin_T = new TH1D("h_Coin_T","Coincidence time spectrum",900,0,900);
  TH1D *h_Eff_T = new TH1D("h_Eff_T","MWPC Efficiency time spectrum",900,0,900);

  //Graphs
//  TGraphErrors* LEDGraph = new TGraphErrors();

//  LEDGraph->SetTitle("Photo Diod vs time");
  /////////////////////////////////////////////End Root Definition//////////////////////////////////////
  //SR90 Calibration
  double StartTime = 0;
  int N_Scint = Tree_Scint->GetEntries();
  for(int i=0;i<N_Scint;i++){
    //Read Tree
    Tree_Scint->GetEntry(i);
    if (i==0)StartTime = GroupTime;
    h_Scint->Fill(QPMT_A);
    if(QPMT_A>EnergyThreshold && QPMT_A<EnergyUpperBound) h_Scint_T->Fill((GroupTime-StartTime)/60.0);
    /*    int hour = int(GroupTime/3600);
	  int minute = int((GroupTime - hour*3600)/60);
	  int second = GroupTime - hour*3600 - 60*minute;
     */
  }//End file loop
  cout << N_Scint << " Scint events"<<endl;

  int N_Beta = Tree_Beta->GetEntries();
//  int N_Beta = 600000;

  for(int i=0;i<N_Beta;i++){
    //Read Tree
    Tree_Beta->GetEntry(i);
    AMWPC_anode = AMWPC_anode1 +AMWPC_anode2; 
    if (i==0)StartTime = GroupTime;
    h_Scint_Coin->Fill(QPMT_A);
    if(QPMT_A>EnergyThreshold && QPMT_A<EnergyUpperBound) h_Coin_T->Fill((GroupTime-StartTime)/60.0);
    //MWPC Anode and Image
    h_MWPC_Coin->Fill(AMWPC_anode);
    //Count # of fired wires
    int NFireX=0;
    int NFireY=0;
    if (MWPC_X.AX1!=0) NFireX++;
    if (MWPC_X.AX2!=0) NFireX++;
    if (MWPC_X.AX3!=0) NFireX++;
    if (MWPC_X.AX4!=0) NFireX++;
    if (MWPC_X.AX5!=0) NFireX++;
    if (MWPC_X.AX6!=0) NFireX++;
    if (MWPC_Y.AY1!=0) NFireY++;
    if (MWPC_Y.AY2!=0) NFireY++;
    if (MWPC_Y.AY3!=0) NFireY++;
    if (MWPC_Y.AY4!=0) NFireY++;
    if (MWPC_Y.AY5!=0) NFireY++;
    if (MWPC_Y.AY6!=0) NFireY++;
    MWPC_ChargeX = MWPC_X.AX1+MWPC_X.AX2+MWPC_X.AX3+MWPC_X.AX4+MWPC_X.AX5+MWPC_X.AX6;
    MWPC_PosX = (-20.*MWPC_X.AX1 -12.*MWPC_X.AX2-4.*MWPC_X.AX3+4.*MWPC_X.AX4+12.*MWPC_X.AX5+20.*MWPC_X.AX6)/MWPC_ChargeX;
    MWPC_ChargeY = MWPC_Y.AY1+MWPC_Y.AY2+MWPC_Y.AY3+MWPC_Y.AY4+MWPC_Y.AY5+MWPC_Y.AY6;
    MWPC_PosY = (-20.*MWPC_Y.AY1 -12.*MWPC_Y.AY2-4.*MWPC_Y.AY3+4.*MWPC_Y.AY4+12.*MWPC_Y.AY5+20.*MWPC_Y.AY6)/MWPC_ChargeY;
    h_Cathode_Image_Coin->Fill(MWPC_PosX,MWPC_PosY);
    h_FiredWireX->Fill(NFireX);
    h_FiredWireY->Fill(NFireY);
      
  }//End file loop

  for (int i=1;i<=900;i++){
    if (h_Scint_T->GetBinContent(i)!=0)
      h_Eff_T->Fill(i,h_Coin_T->GetBinContent(i)/h_Scint_T->GetBinContent(i)*100.0);
  } 
  h_FiredWireX->Scale(100.0/N_Beta);
  h_FiredWireY->Scale(100.0/N_Beta);

  cout << N_Beta << " Scint events"<<endl;

  TCanvas *c1 = new TCanvas("c1","Scint");
  c1->Divide(2,2);
  c1->cd(1);
  h_Scint->Draw();
  c1->cd(2);
  h_Scint_Coin->Draw();
  c1->cd(3);
  h_Scint_T->GetXaxis()->SetTitle("Time (min)");
  h_Scint_T->Draw();
  c1->cd(4);
  h_Coin_T->GetXaxis()->SetTitle("Time (min)");
  h_Coin_T->Draw();
  spectrumfile = argv[1];
  sprintf(filenamebuf,"_Beta_spectra.pdf(");
  spectrumfile += filenamebuf;	
  c1->SaveAs(spectrumfile.c_str());

  TCanvas *c2 = new TCanvas("c2","Efficiency");
  h_Eff_T->Draw();
  h_Eff_T->GetXaxis()->SetTitle("Time (min)");
  h_Eff_T->GetYaxis()->SetTitle("%");
  spectrumfile = argv[1];
  sprintf(filenamebuf,"_Beta_spectra.pdf");
  spectrumfile += filenamebuf;	
  c2->SaveAs(spectrumfile.c_str());

  TCanvas *c3 = new TCanvas("c3","MWPC");
  c3->Divide(2,2);
  c3->cd(1);
  h_MWPC_Coin->Draw();
//  c3->cd(2)->SetLogz();
  c3->cd(2);
  h_Cathode_Image_Coin->Draw("colz");
  c3->cd(3);
  h_FiredWireX->GetXaxis()->SetTitle("# of Fired Stripe");
  h_FiredWireX->GetYaxis()->SetTitle("%");
  h_FiredWireX->Draw();
  c3->cd(4);
  h_FiredWireY->Draw();
  spectrumfile = argv[1];
  sprintf(filenamebuf,"_Beta_spectra.pdf)");
  spectrumfile += filenamebuf;	
  c3->SaveAs(spectrumfile.c_str());

  delete f;
  return EXIT_SUCCESS;
}


