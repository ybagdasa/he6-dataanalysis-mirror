//      Ran Hong
//      Aug 13th 2013
//
//	Convert .fast file into a root Tree within a root file
//	usage:
//	make He6_Faster2Root
//	./He6_Faster2Root filename_prefix(no number or .fast) number_of_file
//
// 	adjust nmax if only a sample is required
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>

#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TRandom.h"
#include "TGraphErrors.h"

const double nmax= 100000000;
using namespace std;

//Define data structures


typedef struct DataStruct_MWPC_X{
  double AX1;
  double AX2;
  double AX3;
  double AX4;
  double AX5;
  double AX6;
}DataStruct_MWPC_X;

typedef struct DataStruct_MWPC_Y{
  double AY1;
  double AY2;
  double AY3;
  double AY4;
  double AY5;
  double AY6;
}DataStruct_MWPC_Y;

typedef struct DataStruct_TMWPC_X{
  double MW_TX1;
  double MW_TX2;
  double MW_TX3;
  double MW_TX4;
  double MW_TX5;
  double MW_TX6;
}DataStruct_TMWPC_X;

typedef struct DataStruct_TMWPC_Y{
  double MW_TY1;
  double MW_TY2;
  double MW_TY3;
  double MW_TY4;
  double MW_TY5;
  double MW_TY6;
}DataStruct_TMWPC_Y;

typedef struct DataStruct_QMCP_anodes{
  double QX1;
  double QX2;
  double QY1;
  double QY2;
}DataStruct_QMCP_anodes;

typedef struct DataStruct_TMCP_anodes{
  double TX1;
  double TX2;
  double TY1;
  double TY2;
}DataStruct_TMCP_anodes;



int main (int argc, char** argv) {
  TH1F *dummy = new TH1F("dummy", "dummy", 10, 0, 1);
  TFile *dummy_file = new TFile("dummy.root","recreate");
  TTree *dummytree = new TTree("dummytree","dummytree",99);

  string inputfile;
  string spectrumfile;
  char filenamebuf[30];
  int NFile;                           
  //  double TMCP_ref;	//  Time ref for first event
  int ReadOutType;

  double EnergyThreshold = 100000;
  double EnergyUpperBound = 1400000;

  int n = 0;  //  nb of data read
  int nk = 0; //  nb of data kept

  double Tref = 0;
  double Tref_A = 0;
  double QScint;
  double TScint_rel;
  double AMWPC_anode1;
  double AMWPC_anode2;
  double TMWPC_rel;
  double TBeta_diff;
  DataStruct_MWPC_X MWPC_X;
  DataStruct_TMWPC_X TMWPC_X;
  DataStruct_MWPC_Y MWPC_Y;
  DataStruct_TMWPC_Y TMWPC_Y;
  double QMCP;
  DataStruct_QMCP_anodes QMCP_anodes;
  DataStruct_TMCP_anodes TMCP_anodes;
  double TMCP_rel;
  double TOF;

  double GroupTime;

  unsigned short TriggerMap;

  long double TScint,TX1,TX2,TY1,TY2,TMCP;
  double TMWPC_anode;
  double TMWPC_X1,TMWPC_X2,TMWPC_X3,TMWPC_X4,TMWPC_X5,TMWPC_X6;
  double TMWPC_Y1,TMWPC_Y2,TMWPC_Y3,TMWPC_Y4,TMWPC_Y5,TMWPC_Y6;

  double TX1mTX2,TX1pTX2,TY1mTY2,TY1pTY2;
  double TMWPCX_rel,TMWPCY_rel,TMCP_anodes_rel;
  double MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY;
  
  ofstream fileout;
  fileout.open("AmpCalibration.txt",ios::out);

  if (argc < 2) {   //  command args & usage
    printf ("usage : \n");
    printf ("        %s  filename\n", argv[0]);
    printf ("\n");
    return EXIT_SUCCESS;
  }

  //Get Exp Data directory from environment
  char *ExpDataDirect;
  ExpDataDirect=getenv("HE6_EXPERIMENT_DATA_DIRECTORY");
  if (ExpDataDirect==NULL){
    cout << "The environment variable HE6_EXPERIMENT_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }

  // output root file
  TString fName = string(ExpDataDirect) + string("/") + string(argv[1]);
  fName += ".root";
  TFile *f = new TFile (fName.Data(), "read");

  // root tree
  //Trees for calibration
  TTree *Tree_MWPC = (TTree*)f->Get("Tree_MWPC");	//Exclude Scint event
//  TTree *Tree_Beta = (TTree*)f->Get("Tree_Beta");	//Exclude tripple trigger

//  TTree *Tree_MWPC_Iso = (TTree*)f->Get("Tree_MWPC_Iso");
  TTree *Tree_MWPCX_Iso = (TTree*)f->Get("Tree_MWPCX_Iso");
  TTree *Tree_MWPCY_Iso = (TTree*)f->Get("Tree_MWPCY_Iso");

  // Branches  
/*
  Tree_Scint->SetBranchAddress("Event_No",&n);
  Tree_Scint->SetBranchAddress("GroupTime",&GroupTime);
  Tree_Scint->SetBranchAddress("QScint",&QScint);
  Tree_Scint->SetBranchAddress("TScint_rel",&TScint_rel);
  Tree_Scint->SetBranchAddress("TriggerMap",&TriggerMap);
*/
  Tree_MWPC->SetBranchAddress("Event_No",&n);  
  Tree_MWPC->SetBranchAddress("GroupTime",&GroupTime);
  Tree_MWPC->SetBranchAddress("AMWPC_anode1",&AMWPC_anode1);
  Tree_MWPC->SetBranchAddress("AMWPC_anode2",&AMWPC_anode2);
  Tree_MWPC->SetBranchAddress("TMWPC_rel",&TMWPC_rel);
  Tree_MWPC->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_MWPC->SetBranchAddress("TMWPC_X",&TMWPC_X);
  Tree_MWPC->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_MWPC->SetBranchAddress("TMWPC_Y",&TMWPC_Y);
  Tree_MWPC->SetBranchAddress("TriggerMap",&TriggerMap);

/*  Tree_Beta->SetBranchAddress("Event_No",&n);
  Tree_Beta->SetBranchAddress("GroupTime",&GroupTime);
  Tree_Beta->SetBranchAddress("QScint",&QScint);
  Tree_Beta->SetBranchAddress("TScint_rel",&TScint_rel);
  Tree_Beta->SetBranchAddress("AMWPC_anode",&AMWPC_anode);
  Tree_Beta->SetBranchAddress("TBeta_diff",&TBeta_diff);
  Tree_Beta->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_Beta->SetBranchAddress("TMWPC_X",&TMWPC_X);
  Tree_Beta->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_Beta->SetBranchAddress("TMWPC_Y",&TMWPC_Y);
  Tree_Beta->SetBranchAddress("TriggerMap",&TriggerMap);

  Tree_MWPC_Iso->SetBranchAddress("Event_No",&n);  
  Tree_MWPC_Iso->SetBranchAddress("AMWPC_anode",&AMWPC_anode);
  Tree_MWPC_Iso->SetBranchAddress("TMWPC_rel",&TMWPC_rel);
*/
  Tree_MWPCX_Iso->SetBranchAddress("Event_No",&n);
  Tree_MWPCX_Iso->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_MWPCX_Iso->SetBranchAddress("TMWPCX_rel",&TMWPCX_rel);

  Tree_MWPCY_Iso->SetBranchAddress("Event_No",&n);
  Tree_MWPCY_Iso->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_MWPCY_Iso->SetBranchAddress("TMWPCY_rel",&TMWPCY_rel);

  // root histos
  TH1D *h_MWPC1  = new TH1D("h_MWPC1","MWPC Anode1 alone spectrum",1024,0,2048000);
  TH1D *h_MWPC2  = new TH1D("h_MWPC2","MWPC Anode2 alone spectrum",1024,0,2048000);
  TH1D *h_A1 = new TH1D("h_ADC1","ADC1. alone spectrum",1024,0,819200);
  TH1D *h_A2 = new TH1D("h_ADC2","ADC2. alone spectrum",1024,0,819200);
  TH1D *h_A3 = new TH1D("h_ADC3","ADC3. alone spectrum",1024,0,819200);
  TH1D *h_A4 = new TH1D("h_ADC4","ADC4. alone spectrum",1024,0,819200);
  TH1D *h_A5 = new TH1D("h_ADC5","ADC5. alone spectrum",1024,0,819200);
  TH1D *h_A6 = new TH1D("h_ADC6","ADC6. alone spectrum",1024,0,819200);
  TH1D *h_A7 = new TH1D("h_ADC7","ADC7. alone spectrum",1024,0,819200);
  TH1D *h_A8 = new TH1D("h_ADC8","ADC8. alone spectrum",1024,0,819200);
  TH1D *h_A9 = new TH1D("h_ADC9","ADC9. alone spectrum",1024,0,819200);
  TH1D *h_A10 = new TH1D("h_ADC10","ADC10. alone spectrum",1024,0,819200);
  TH1D *h_A11 = new TH1D("h_ADC11","ADC11. alone spectrum",1024,0,819200);
  TH1D *h_A12 = new TH1D("h_ADC12","ADC12. alone spectrum",1024,0,819200);
  //Time spectrum

  //Graphs

  /////////////////////////////////////////////End Root Definition//////////////////////////////////////
  //Pulser Calibration
  double StartTime = 0;
  int N_Event = Tree_MWPC->GetEntries();
  double DataArray[14];
  int CountArray[14];

  for (int i=0;i<14;i++){
    DataArray[i]=0;
    CountArray[i]=0;
  }
  for(int i=0;i<N_Event;i++){
    //Read Tree
    Tree_MWPC->GetEntry(i);
    if (i==0)StartTime = GroupTime;
    h_MWPC1->Fill(AMWPC_anode1);
    h_MWPC2->Fill(AMWPC_anode2);
    h_A1->Fill(MWPC_X.AX1);
    h_A2->Fill(MWPC_X.AX2);
    h_A3->Fill(MWPC_X.AX3);
    h_A4->Fill(MWPC_X.AX4);
    h_A5->Fill(MWPC_X.AX5);
    h_A6->Fill(MWPC_X.AX6);
    h_A7->Fill(MWPC_Y.AY1);
    h_A8->Fill(MWPC_Y.AY2);
    h_A9->Fill(MWPC_Y.AY3);
    h_A10->Fill(MWPC_Y.AY4);
    h_A11->Fill(MWPC_Y.AY5);
    h_A12->Fill(MWPC_Y.AY6);
    if (MWPC_X.AX1 > 1000){
      DataArray[0]+=MWPC_X.AX1;
      CountArray[0]++;
    }
    if (MWPC_X.AX2 > 1000){
      DataArray[1]+=MWPC_X.AX2;
      CountArray[1]++;
    }
    if (MWPC_X.AX3 > 1000){
      DataArray[2]+=MWPC_X.AX3;
      CountArray[2]++;
    }
    if (MWPC_X.AX4 > 1000){
      DataArray[3]+=MWPC_X.AX4;
      CountArray[3]++;
    }
    if (MWPC_X.AX5 > 1000){
      DataArray[4]+=MWPC_X.AX5;
      CountArray[4]++;
    }
    if (MWPC_X.AX6 > 1000){
      DataArray[5]+=MWPC_X.AX6;
      CountArray[5]++;
    }
    if (MWPC_Y.AY1 > 1000){
      DataArray[6]+=MWPC_Y.AY1;
      CountArray[6]++;
    }
    if (MWPC_Y.AY2 > 1000){
      DataArray[7]+=MWPC_Y.AY2;
      CountArray[7]++;
    }
    if (MWPC_Y.AY3 > 1000){
      DataArray[8]+=MWPC_Y.AY3;
      CountArray[8]++;
    }
    if (MWPC_Y.AY4 > 1000){
      DataArray[9]+=MWPC_Y.AY4;
      CountArray[9]++;
    }
    if (MWPC_Y.AY5 > 1000){
      DataArray[10]+=MWPC_Y.AY5;
      CountArray[10]++;
    }
    if (MWPC_Y.AY6 > 1000){
      DataArray[11]+=MWPC_Y.AY6;
      CountArray[11]++;
    }
    if (AMWPC_anode1 > 1000){
      DataArray[12]+=AMWPC_anode1;
      CountArray[12]++;
    }
    if (AMWPC_anode2 > 1000){
      DataArray[13]+=AMWPC_anode2;
      CountArray[13]++;
    }
  }//End file loop
  cout << N_Event << " events"<<endl;

  for (int i=0;i<14;i++){
    DataArray[i]/=double(CountArray[i]);
  }
  fileout <<"1"<<endl;
  for (int i=1;i<6;i++){
    DataArray[i]/=DataArray[0];
    fileout << DataArray[i]<<endl;
  }
  fileout <<"1"<<endl;
  for (int i=7;i<12;i++){
    DataArray[i]/=DataArray[6];
    fileout << DataArray[i]<<endl;
  }
  fileout << DataArray[12]<<endl;
  fileout << DataArray[13]<<endl;

  TCanvas *c1 = new TCanvas("c1","cathode1");
  c1->Divide(3,2);
  c1->cd(1);
  h_A1->Draw();
  c1->cd(2);
  h_A2->Draw();
  c1->cd(3);
  h_A3->Draw();
  c1->cd(4);
  h_A4->Draw();
  c1->cd(5);
  h_A5->Draw();
  c1->cd(6);
  h_A6->Draw();
  spectrumfile = argv[1];
  sprintf(filenamebuf,"_CathodeCalibration.pdf(");
  spectrumfile += filenamebuf;	
  c1->SaveAs(spectrumfile.c_str());

  TCanvas *c2 = new TCanvas("c2","cathode2");
  c2->Divide(3,2);
  c2->cd(1);
  h_A7->Draw();
  c2->cd(2);
  h_A8->Draw();
  c2->cd(3);
  h_A9->Draw();
  c2->cd(4);
  h_A10->Draw();
  c2->cd(5);
  h_A11->Draw();
  c2->cd(6);
  h_A12->Draw();
  spectrumfile = argv[1];
  sprintf(filenamebuf,"_CathodeCalibration.pdf");
  spectrumfile += filenamebuf;	
  c2->SaveAs(spectrumfile.c_str());

  TCanvas *c3 = new TCanvas("c2","Anode");
  c3->Divide(1,2);
  c3->cd(1);
  h_MWPC1->Draw();
  c3->cd(2);
  h_MWPC2->Draw();
  spectrumfile = argv[1];
  sprintf(filenamebuf,"_CathodeCalibration.pdf)");
  spectrumfile += filenamebuf;	
  c3->SaveAs(spectrumfile.c_str());

  delete f;
  fileout.close();
  return EXIT_SUCCESS;
}


