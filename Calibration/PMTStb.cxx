//      Ran Hong
//      Aug 13th 2013
//
//	Convert .fast file into a root Tree within a root file
//	usage:
//	make He6_Faster2Root
//	./He6_Faster2Root filename_prefix(no number or .fast) number_of_file
//
// 	adjust nmax if only a sample is required
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TRandom.h"
#include "TGraphErrors.h"

const double nmax= 100000000;
using namespace std;

//Define data structures


typedef struct DataStruct_MWPC_X{
  double AX1;
  double AX2;
  double AX3;
  double AX4;
  double AX5;
  double AX6;
}DataStruct_MWPC_X;

typedef struct DataStruct_MWPC_Y{
  double AY1;
  double AY2;
  double AY3;
  double AY4;
  double AY5;
  double AY6;
}DataStruct_MWPC_Y;

typedef struct DataStruct_TMWPC_X{
  double MW_TX1;
  double MW_TX2;
  double MW_TX3;
  double MW_TX4;
  double MW_TX5;
  double MW_TX6;
}DataStruct_TMWPC_X;

typedef struct DataStruct_TMWPC_Y{
  double MW_TY1;
  double MW_TY2;
  double MW_TY3;
  double MW_TY4;
  double MW_TY5;
  double MW_TY6;
}DataStruct_TMWPC_Y;

typedef struct DataStruct_QMCP_anodes{
  double QX1;
  double QX2;
  double QY1;
  double QY2;
}DataStruct_QMCP_anodes;

typedef struct DataStruct_TMCP_anodes{
  double TX1;
  double TX2;
  double TY1;
  double TY2;
}DataStruct_TMCP_anodes;



int main (int argc, char** argv) {
  TH1F *dummy = new TH1F("dummy", "dummy", 10, 0, 1);
  TFile *dummy_file = new TFile("dummy.root","recreate");
  TTree *dummytree = new TTree("dummytree","dummytree",99);

  string inputfile;
  string spectrumfile;
  char filenamebuf[30];
  int NFile;                           
  //  double TMCP_ref;	//  Time ref for first event
  int ReadOutType;

  int n = 0;  //  nb of data read
  int nk = 0; //  nb of data kept

  double Tref = 0;
  double Tref_A = 0;
  double QPMT_A;
  double TPMT_A_rel;
  double QPMT_D;
  double TPMT_DA;
  double AMWPC_anode;
  double TMWPC_rel;
  double TBeta_diff;
  DataStruct_MWPC_X MWPC_X;
  DataStruct_TMWPC_X TMWPC_X;
  DataStruct_MWPC_Y MWPC_Y;
  DataStruct_TMWPC_Y TMWPC_Y;
  double QMCP;
  DataStruct_QMCP_anodes QMCP_anodes;
  DataStruct_TMCP_anodes TMCP_anodes;
  double TMCP_rel;
  double TOF;
  double LED_A;
  double T_LED;
  double T_LED_PMT;

  double GroupTime;

  unsigned short TriggerMap;

  long double TScint,TX1,TX2,TY1,TY2,TMCP;
  double TMWPC_anode;
  double TMWPC_X1,TMWPC_X2,TMWPC_X3,TMWPC_X4,TMWPC_X5,TMWPC_X6;
  double TMWPC_Y1,TMWPC_Y2,TMWPC_Y3,TMWPC_Y4,TMWPC_Y5,TMWPC_Y6;

  double TX1mTX2,TX1pTX2,TY1mTY2,TY1pTY2;
  double TMWPCX_rel,TMWPCY_rel,TMCP_anodes_rel;
  double MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY;

  if (argc < 2) {   //  command args & usage
    printf ("usage : \n");
    printf ("        %s  filename\n", argv[0]);
    printf ("\n");
    return EXIT_SUCCESS;
  }

  //Get Exp Data directory from environment
  char *ExpDataDirect;
  ExpDataDirect=getenv("HE6_EXPERIMENT_DATA_DIRECTORY");
  if (ExpDataDirect==NULL){
    cout << "The environment variable HE6_EXPERIMENT_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }

  // output root file
  TString fName = string(ExpDataDirect) + string("/") + string(argv[1]);
  fName += ".root";
  TFile *f = new TFile (fName.Data(), "read");

  // root tree
  //Trees for calibration
  TTree *Tree_PMT_STB = (TTree*)f->Get("Tree_PMT_STB");      //LED calibration of PMT stability
  TTree *Tree_Scint = (TTree*)f->Get("Tree_Scint");
  TTree *Tree_MWPC = (TTree*)f->Get("Tree_MWPC");	//Exclude Scint event
  TTree *Tree_Beta = (TTree*)f->Get("Tree_Beta");	//Exclude tripple trigger
/*
  TTree *Tree_MWPC_Iso = (TTree*)f->Get("Tree_MWPC_Iso");
  TTree *Tree_MWPCX_Iso = (TTree*)f->Get("Tree_MWPC_Iso");
  TTree *Tree_MWPCY_Iso = (TTree*)f->Get("Tree_MWPC_Iso");
*/
  // Branches  

  Tree_Scint->SetBranchAddress("Event_No",&n);
  Tree_Scint->SetBranchAddress("GroupTime",&GroupTime);
  Tree_Scint->SetBranchAddress("QPMT_A",&QPMT_A);
  Tree_Scint->SetBranchAddress("QPMT_D",&QPMT_D);
  Tree_Scint->SetBranchAddress("TPMT_A_rel",&TPMT_A_rel);
  Tree_Scint->SetBranchAddress("TPMT_DA",&TPMT_DA);
  Tree_Scint->SetBranchAddress("TriggerMap",&TriggerMap);

  Tree_MWPC->SetBranchAddress("Event_No",&n);  
  Tree_MWPC->SetBranchAddress("GroupTime",&GroupTime);
  Tree_MWPC->SetBranchAddress("AMWPC_anode",&AMWPC_anode);
  Tree_MWPC->SetBranchAddress("TMWPC_rel",&TMWPC_rel);
  Tree_MWPC->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_MWPC->SetBranchAddress("TMWPC_X",&TMWPC_X);
  Tree_MWPC->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_MWPC->SetBranchAddress("TMWPC_Y",&TMWPC_Y);
  Tree_MWPC->SetBranchAddress("TriggerMap",&TriggerMap);

  Tree_Beta->SetBranchAddress("Event_No",&n);
  Tree_Beta->SetBranchAddress("GroupTime",&GroupTime);
  Tree_Beta->SetBranchAddress("QPMT_A",&QPMT_A);
  Tree_Beta->SetBranchAddress("TPMT_A_rel",&TPMT_A_rel);
  Tree_Beta->SetBranchAddress("AMWPC_anode",&AMWPC_anode);
  Tree_Beta->SetBranchAddress("TBeta_diff",&TBeta_diff);
  Tree_Beta->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_Beta->SetBranchAddress("TMWPC_X",&TMWPC_X);
  Tree_Beta->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_Beta->SetBranchAddress("TMWPC_Y",&TMWPC_Y);
  Tree_Beta->SetBranchAddress("TriggerMap",&TriggerMap);
/*
  Tree_MWPC_Iso->SetBranchAddress("Event_No",&n);  
  Tree_MWPC_Iso->SetBranchAddress("AMWPC_anode",&AMWPC_anode);
  Tree_MWPC_Iso->SetBranchAddress("TMWPC_rel",&TMWPC_rel);

  Tree_MWPCX_Iso->SetBranchAddress("Event_No",&n);
  Tree_MWPCX_Iso->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_MWPCX_Iso->SetBranchAddress("TMWPCX_rel",&TMWPCX_rel);

  Tree_MWPCY_Iso->SetBranchAddress("Event_No",&n);
  Tree_MWPCY_Iso->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_MWPCY_Iso->SetBranchAddress("TMWPCY_rel",&TMWPCY_rel);
*/
  Tree_PMT_STB->SetBranchAddress("LED",&LED_A);
  Tree_PMT_STB->SetBranchAddress("LED_T",&T_LED);
  Tree_PMT_STB->SetBranchAddress("Scint",&QPMT_A);
  Tree_PMT_STB->SetBranchAddress("Timing",&T_LED_PMT);

  // root histos
  TH1D *h_Scint = new TH1D("h_Scint","Scint. alone spectrum",1024,0,2048000);
  TH1D *h_Scint_All = new TH1D("h_Scint_All","Scint. alone spectrum",1024,0,2048000);
  TH1D *h_MWPC  = new TH1D("h_MWPC","MWPC Anode alone spectrum",1024,0,65536);
  TH1D *h_Scint_Coin = new TH1D("h_Scint_Coin","Scint. coincidence spectrum",1024,0,2048000);
  TH1D *h_MWPC_Coin  = new TH1D("h_MWPC_Coin","MWPC Anode coincidence spectrum",1024,0,65536);
  TH2D *h_MWPC_Scint = new TH2D("h_MWPC_Scint","MWPC vs Scint 2D",1024,0,65536,1024,0,2048000);

  TH1D *h_Cathode_X = new TH1D("h_Cathode_X","MWPC CathodeX position spectrum",100,-25,25);
  TH1D *h_Cathode_Y = new TH1D("h_Cathode_Y","MWPC CathodeY position spectrum",100,-25,25);
  TH2D *h_Cathode_Image = new TH2D("h_Cathode_Image","2D image of MWPC",100,-25,25,100,-25,25);
  TH2D *h_Cathode_Image_Coin = new TH2D("h_Cathode_Image_Coin","2D image of MWPC in coincidence with Scint",100,-25,25,100,-25,25);
  TH1D *h_Charge_X = new TH1D("h_Charge_X","Total Charge on MWPC X",1024,0,51200);
  TH1D *h_Charge_Y = new TH1D("h_Charge_Y","Total Charge on MWPC Y",1024,0,51200);

  TH2D *h_ChargeX_vs_Anode = new TH2D("h_ChargeX_vs_Anode","ChargeX vs Anode",1024,0,51200,1024,0,65536);
  TH2D *h_ChargeY_vs_Anode = new TH2D("h_ChargeY_vs_Anode","ChargeY vs Anode",1024,0,51200,1024,0,65536);
  TH2D *h_ChargeX_vs_ChargeY = new TH2D("h_ChargeX_vs_ChargeY","ChargeX vs ChargeY",1024,0,204800,1024,0,204800);
  TH2D *h_TotCharge_vs_Anode = new TH2D("h_TotCharge_vs_Anode","Total Charge vs Anode",1024,0,204800,1024,0,65536);

  TH1 *LEDHist = new TH1D("LEDHist","LED Photo Diod",3000,80000,100000);
  TH1 *PMT_LED = new TH1D("PMT_LED","PMT in Coincidence with LED",3000,0,3000000);
  TH1 *LEDHist_All = new TH1D("LEDHist_All","LED Photo Diod",3000,80000,100000);
  TH1 *PMT_LED_All = new TH1D("PMT_LED_All","PMT in Coincidence with LED",3000,0,3000000);

  //Graphs
  TGraphErrors* LEDGraph = new TGraphErrors();
  TGraphErrors* PMTGraph = new TGraphErrors();
  TGraphErrors* PMTGraphNorm = new TGraphErrors();
  TGraphErrors* CorrPMTLED = new TGraphErrors();
  TGraphErrors* ScintGraph = new TGraphErrors();
  TGraphErrors* ScintLEDGraph = new TGraphErrors();
  TGraphErrors* CorrScintLED = new TGraphErrors();

  LEDGraph->SetTitle("Photo Diod vs time");
  PMTGraph->SetTitle("PMT vs time");
  PMTGraphNorm->SetTitle("PMT(Normalized) vs time");
  CorrPMTLED->SetTitle("Photo Diod vs PMT");
  ScintGraph->SetTitle("EndPoint vs time");
  ScintLEDGraph->SetTitle("LED-PMT vs time");
  CorrScintLED->SetTitle("EndPoint vs LED-PMT");
  /////////////////////////////////////////////End Root Definition//////////////////////////////////////
  int N_LED = Tree_PMT_STB->GetEntries();
  int NP_LED = N_LED/8;

  double IntegralTime = 600;//s
  int j=0;

  double StartTime = 0;
  for(int i=0;i<N_LED;i++){
    //Read Tree
    Tree_PMT_STB->GetEntry(i);
    if (i==0)StartTime = T_LED;
    LEDHist->Fill(LED_A);
    PMT_LED->Fill(QPMT_A);
    LEDHist_All->Fill(LED_A);
    PMT_LED_All->Fill(QPMT_A);
    if ((T_LED-StartTime)-j*IntegralTime>=IntegralTime){
      LEDHist->Print();
      //LED
      LEDHist->Fit("gaus","R","",80000,100000);
      TF1 * fitFunc1 = LEDHist->GetFunction("gaus");
      cout << fitFunc1->GetParameter(0)<<" ";
      cout << fitFunc1->GetParameter(1)<<" ";
      cout << fitFunc1->GetParameter(2)<<" ";
      cout << fitFunc1->GetChisquare()/fitFunc1->GetNDF()<<endl;
      LEDGraph->SetPoint(j,(j+1)*IntegralTime/60.0,fitFunc1->GetParameter(1));
      LEDGraph->SetPointError(j,0,fitFunc1->GetParError(1));
      //PMT
      PMT_LED->Fit("gaus","R","",1500000,2000000);
      TF1 * fitFunc2 = PMT_LED->GetFunction("gaus");
      cout << fitFunc2->GetParameter(0)<<" ";
      cout << fitFunc2->GetParameter(1)<<" ";
      cout << fitFunc2->GetParameter(2)<<" ";
      cout << fitFunc2->GetChisquare()/fitFunc2->GetNDF()<<endl;
      PMTGraph->SetPoint(j,(j+1)*IntegralTime/60.0,fitFunc2->GetParameter(1));
      PMTGraph->SetPointError(j,0,fitFunc2->GetParError(1));
      PMTGraphNorm->SetPoint(j,(j+1)*IntegralTime/60.0,fitFunc2->GetParameter(1)/fitFunc1->GetParameter(1));
      PMTGraphNorm->SetPointError(j,0,fitFunc2->GetParError(1)/fitFunc1->GetParameter(1));
      //Correlation
      CorrPMTLED->SetPoint(j,fitFunc1->GetParameter(1),fitFunc2->GetParameter(1));
      CorrPMTLED->SetPointError(j,fitFunc1->GetParError(1),fitFunc2->GetParError(1));
      LEDHist->Reset();
      PMT_LED->Reset();
      j++;
    }
  }//End file loop
  cout << N_LED << " LED events"<<endl;

  TCanvas *c1 = new TCanvas("c1","LED");
  c1->Divide(2,2);
  c1->cd(1);
  LEDGraph->Draw("APL");
  LEDGraph->GetXaxis()->SetTitle("Time [min]");
  c1->cd(2);
  PMTGraph->Draw("APL");
  PMTGraph->GetXaxis()->SetTitle("Time [min]");
  c1->cd(3);
  LEDHist_All->Draw();
  c1->cd(4);
  PMT_LED_All->Draw();

  spectrumfile = argv[1];
  sprintf(filenamebuf,"_spectra.pdf(");
  spectrumfile += filenamebuf;	
  c1->SaveAs(spectrumfile.c_str());

  TCanvas *c2 = new TCanvas("c2","Correlation");
  CorrPMTLED->Draw("APL");

  spectrumfile = argv[1];
  sprintf(filenamebuf,"_spectra.pdf");
  spectrumfile += filenamebuf;	
  c2->SaveAs(spectrumfile.c_str());

  TCanvas *c22 = new TCanvas("c22","PMTNorm");
  PMTGraphNorm->Draw("APL");
  PMTGraphNorm->GetXaxis()->SetTitle("Time [min]");

  spectrumfile = argv[1];
  sprintf(filenamebuf,"_spectra.pdf");
  spectrumfile += filenamebuf;	
  c22->SaveAs(spectrumfile.c_str());

  //SR90 Calibration
  int N_Scint = Tree_Scint->GetEntries();
  int NP_Scint = N_Scint/8;
  j=0;
  //Plot individual fit
//  TCanvas *ctest = new TCanvas("ctest","PMT");
//  ctest->SaveAs("Slides.pdf(");
  for(int i=0;i<N_Scint;i++){
    //Read Tree
    Tree_Scint->GetEntry(i);
    if (i==0)StartTime = GroupTime;
    h_Scint->Fill(QPMT_A);
    h_Scint_All->Fill(QPMT_A);
    if ((GroupTime-StartTime)-j*IntegralTime>=IntegralTime){
      TF1 * fitFunc = new TF1("fitFunc","[0]*(x-[1])",500000,1000000);
      fitFunc->SetParameters(-0.016,1200000);
      h_Scint->Fit(fitFunc,"R","",600000,1000000);
      cout << fitFunc->GetParameter(0)<<" ";
      cout << fitFunc->GetParameter(1)<<" ";
      cout << fitFunc->GetChisquare()/fitFunc->GetNDF()<<endl;
      ScintGraph->SetPoint(j,(j+1)*IntegralTime/60.0,fitFunc->GetParameter(1));
      ScintGraph->SetPointError(j,0,fitFunc->GetParError(1));
/*      if (j<5){
      h_Scint->Draw();
      ctest->SaveAs("Slides.pdf");
      }*/
      h_Scint->Fit("gaus","R","",1400000,2000000);
      TF1 * fitFunc2 = h_Scint->GetFunction("gaus");
      cout << fitFunc2->GetParameter(0)<<" ";
      cout << fitFunc2->GetParameter(1)<<" ";
      cout << fitFunc2->GetParameter(2)<<" ";
      cout << fitFunc2->GetChisquare()/fitFunc2->GetNDF()<<endl;
      ScintLEDGraph->SetPoint(j,(j+1)*IntegralTime/60.0,fitFunc2->GetParameter(1));
      ScintLEDGraph->SetPointError(j,0,fitFunc2->GetParError(1));
      //Correlation
      CorrScintLED->SetPoint(j,fitFunc->GetParameter(1),fitFunc2->GetParameter(1));
      CorrScintLED->SetPointError(j,fitFunc->GetParError(1),fitFunc2->GetParError(1));
      h_Scint->Reset();
      j++;
    }
  }//End file loop
  cout << N_Scint << " Scint events"<<endl;

//  ctest->SaveAs("Slides.pdf)");
//  delete ctest;

  TCanvas *c3 = new TCanvas("c3","Scint");
  c3->Divide(2,2);
  c3->cd(1);
  ScintGraph->Draw("APL");
  ScintGraph->GetXaxis()->SetTitle("Time [min]");
  c3->cd(2);
  ScintLEDGraph->Draw("APL");
  ScintLEDGraph->GetXaxis()->SetTitle("Time [min]");
  c3->cd(3);
  h_Scint_All->Draw();
  c3->cd(4);
  CorrScintLED->Draw("APL");
  spectrumfile = argv[1];
  sprintf(filenamebuf,"_spectra.pdf)");
  spectrumfile += filenamebuf;	
  c3->SaveAs(spectrumfile.c_str());


  delete f;
  return EXIT_SUCCESS;
}


