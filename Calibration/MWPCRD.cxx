//      Ran Hong
//      Aug 13th 2013
//
//	Convert .fast file into a root Tree within a root file
//	usage:
//	make He6_Faster2Root
//	./He6_Faster2Root filename_prefix(no number or .fast) number_of_file
//
// 	adjust nmax if only a sample is required
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>

#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TRandom.h"
#include "TGraphErrors.h"
#include "TGraph.h"

const double nmax= 100000000;
using namespace std;

//Define data structures


typedef struct DataStruct_MWPC_X{
  double AX1;
  double AX2;
  double AX3;
  double AX4;
  double AX5;
  double AX6;
}DataStruct_MWPC_X;

typedef struct DataStruct_MWPC_Y{
  double AY1;
  double AY2;
  double AY3;
  double AY4;
  double AY5;
  double AY6;
}DataStruct_MWPC_Y;

typedef struct DataStruct_TMWPC_X{
  double MW_TX1;
  double MW_TX2;
  double MW_TX3;
  double MW_TX4;
  double MW_TX5;
  double MW_TX6;
}DataStruct_TMWPC_X;

typedef struct DataStruct_TMWPC_Y{
  double MW_TY1;
  double MW_TY2;
  double MW_TY3;
  double MW_TY4;
  double MW_TY5;
  double MW_TY6;
}DataStruct_TMWPC_Y;

typedef struct DataStruct_QMCP_anodes{
  double QX1;
  double QX2;
  double QY1;
  double QY2;
}DataStruct_QMCP_anodes;

typedef struct DataStruct_TMCP_anodes{
  double TX1;
  double TX2;
  double TY1;
  double TY2;
}DataStruct_TMCP_anodes;



int main (int argc, char** argv) {
  TH1F *dummy = new TH1F("dummy", "dummy", 10, 0, 1);
  TFile *dummy_file = new TFile("dummy.root","recreate");
  TTree *dummytree = new TTree("dummytree","dummytree",99);

  string inputfile;
  string spectrumfile;
  char filenamebuf[30];
  int NFile;                           
  //  double TMCP_ref;	//  Time ref for first event
  int ReadOutType;

  double EnergyThreshold = 100000;
  double EnergyUpperBound = 1400000;

  int n = 0;  //  nb of data read
  int nk = 0; //  nb of data kept

  double Tref = 0;
  double Tref_A = 0;
  double QScint;
  double TScint_rel;
  double AMWPC_anode1;
  double AMWPC_anode2;
  double TMWPC_rel;
  double TBeta_diff;
  DataStruct_MWPC_X MWPC_X;
  DataStruct_TMWPC_X TMWPC_X;
  DataStruct_MWPC_Y MWPC_Y;
  DataStruct_TMWPC_Y TMWPC_Y;
  double QMCP;
  DataStruct_QMCP_anodes QMCP_anodes;
  DataStruct_TMCP_anodes TMCP_anodes;
  double TMCP_rel;
  double TOF;

  double GroupTime;

  unsigned short TriggerMap;

  long double TScint,TX1,TX2,TY1,TY2,TMCP;
  double TMWPC_anode;
  double TMWPC_X1,TMWPC_X2,TMWPC_X3,TMWPC_X4,TMWPC_X5,TMWPC_X6;
  double TMWPC_Y1,TMWPC_Y2,TMWPC_Y3,TMWPC_Y4,TMWPC_Y5,TMWPC_Y6;

  double TX1mTX2,TX1pTX2,TY1mTY2,TY1pTY2;
  double TMWPCX_rel,TMWPCY_rel,TMCP_anodes_rel;
  double MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY;
  double AnodePos;

  double CalParX[4];
  double CalParY[4];
  double CalParA[4];

  ifstream calfilein;
  calfilein.open("AmpCalibration.txt",ios::in);
  double AmpCalX[6];
  double AmpCalY[6];
  for (int i=0;i<6;i++){
    calfilein>>AmpCalX[i];
//    cout << AmpCalX[i]<<endl;
  }
  for (int i=0;i<6;i++){
    calfilein>>AmpCalY[i];
//    cout << AmpCalY[i]<<endl;
  }
  calfilein.close();

  // root histos
  TH1D *h_Scint = new TH1D("h_Scint","Scint. alone spectrum",1024,0,2048000);
  TH1D *h_MWPC1  = new TH1D("h_MWPC1","MWPC Anode1 spectrum",1024,0,655360);
  TH1D *h_MWPC2  = new TH1D("h_MWPC2","MWPC Anode2 spectrum",1024,0,655360);
  TH2D *h_MWPC_Corr  = new TH2D("h_MWPC1_2","MWPC Anode1-2 spectrum",1024,0,655360,1024,0,655360);
  TH1D *h_MWPC_tot  = new TH1D("h_MWPC_tot","MWPC Anode_tot spectrum",1024,0,655360);
  TH1D *h_Scint_Coin = new TH1D("h_Scint_Coin","Scint. coincidence spectrum",1024,0,2048000);
  TH1D *h_MWPC_Coin  = new TH1D("h_MWPC_Coin","MWPC Anode coincidence spectrum",1024,0,65536);
  TH2D *h_MWPC_Scint = new TH2D("h_MWPC_Scint","MWPC vs Scint 2D",1024,0,65536,1024,0,2048000);
  TH1 *h_FiredWireX = new TH1D("h_FiredWireX","# of fired stripes X",10,0,10);
  TH1 *h_FiredWireY = new TH1D("h_FiredWireY","# of fired stripes Y",10,0,10);

  TH1D *h_Cathode_X = new TH1D("h_Cathode_X","MWPC CathodeX position spectrum",200,-25,25);
  TH1D *h_Cathode_Y = new TH1D("h_Cathode_Y","MWPC CathodeY position spectrum",200,-25,25);
  TH1D *h_Cathode_X_cond = new TH1D("h_Cathode_X_cond","MWPC CathodeX position spectrum cond",200,-25.125,24.875);
  TH1D *h_Cathode_Y_cond = new TH1D("h_Cathode_Y_cond","MWPC CathodeY position spectrum cond",200,-25.125,24.875);
  TH1D *h_Cathode_X_cond2 = new TH1D("h_Cathode_X_cond2","MWPC CathodeX position spectrum cond2",200,-25,25);
  TH1D *h_Cathode_Y_cond2 = new TH1D("h_Cathode_Y_cond2","MWPC CathodeY position spectrum cond2",200,-25,25);
  TH1D *h_Anode_Pos = new TH1D("h_Anode_Pos","MWPC Anode position spectrum",200,-25.25,24.75);
  TH2D *h_Cathode_Image = new TH2D("h_Cathode_Image","2D image of MWPC",200,-25,25,200,-25,25);
  TH2D *h_Anode_Image = new TH2D("h_Anode_Image","2D Anode image of MWPC",200,-25,25,200,-25,25);
  TH2D *h_Cathode_Image_cond = new TH2D("h_Cathode_Image_cond","2D image of MWPC, more than 3 trigger",200,-25,25,200,-25,25);
  TH2D *h_Anode_Image_cond = new TH2D("h_Anode_Image_cond","2D Anode image of MWPC more than 3 trigger",200,-25,25,200,-25,25);
  TH2D *h_ACCorrelation = new TH2D("h_ACCorrelation","MWPC anode cathode_Y correlation",200,-25,25,200,-25,25);
  TH2D *h_Cathode_Image_Coin = new TH2D("h_Cathode_Image_Coin","2D image of MWPC in coincidence with Scint",200,-25,25,200,-25,25);
  TH1D *h_Charge_X = new TH1D("h_Charge_X","Total Charge on MWPC X",1024,0,51200);
  TH1D *h_Charge_Y = new TH1D("h_Charge_Y","Total Charge on MWPC Y",1024,0,51200);

  TH2D *h_ChargeX_vs_Anode = new TH2D("h_ChargeX_vs_Anode","ChargeX vs Anode",1024,0,51200,1024,0,65536);
  TH2D *h_ChargeY_vs_Anode = new TH2D("h_ChargeY_vs_Anode","ChargeY vs Anode",1024,0,51200,1024,0,65536);
  TH2D *h_ChargeX_vs_ChargeY = new TH2D("h_ChargeX_vs_ChargeY","ChargeX vs ChargeY",1024,0,204800,1024,0,204800);
  TH2D *h_TotCharge_vs_Anode = new TH2D("h_TotCharge_vs_Anode","Total Charge vs Anode",1024,0,204800,1024,0,65536);
  //Time spectrum
  TH1D *h_Scint_T = new TH1D("h_Scint_T","Scint. alone time spectrum",240,0,240);
  TH1D *h_Coin_T = new TH1D("h_Coin_T","Coincidence time spectrum",240,0,240);
  TH1D *h_Eff_T = new TH1D("h_Eff_T","MWPC Efficiency time spectrum",240,0,240);

  //Graphs
//  TGraphErrors* LEDGraph = new TGraphErrors();

//  LEDGraph->SetTitle("Photo Diod vs time");
  if (argc < 2) {   //  command args & usage
    printf ("usage : \n");
    printf ("        %s  filename\n", argv[0]);
    printf ("\n");
    return EXIT_SUCCESS;
  }

  //Get Exp Data directory from environment
  char *ExpDataDirect;
  ExpDataDirect=getenv("HE6_EXPERIMENT_DATA_DIRECTORY");
  if (ExpDataDirect==NULL){
    cout << "The environment variable HE6_EXPERIMENT_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }

  // output root file
  TString fName = string(ExpDataDirect) + string("/") + string(argv[1]);
  fName += ".root";
  TFile *f = new TFile (fName.Data(), "read");

  // root tree
  //Trees for calibration
//  TTree *Tree_Scint = (TTree*)f->Get("Tree_Scint");
  TTree *Tree_MWPC = (TTree*)f->Get("Tree_MWPC");	//Exclude Scint event
//  TTree *Tree_Beta = (TTree*)f->Get("Tree_Beta");	//Exclude tripple trigger
/*
  TTree *Tree_MWPC_Iso = (TTree*)f->Get("Tree_MWPC_Iso");
  TTree *Tree_MWPCX_Iso = (TTree*)f->Get("Tree_MWPC_Iso");
  TTree *Tree_MWPCY_Iso = (TTree*)f->Get("Tree_MWPC_Iso");
*/
  // Branches  
/*
  Tree_Scint->SetBranchAddress("Event_No",&n);
  Tree_Scint->SetBranchAddress("GroupTime",&GroupTime);
  Tree_Scint->SetBranchAddress("QScint",&QScint);
  Tree_Scint->SetBranchAddress("TScint_rel",&TScint_rel);
  Tree_Scint->SetBranchAddress("TriggerMap",&TriggerMap);
*/
  Tree_MWPC->SetBranchAddress("Event_No",&n);  
  Tree_MWPC->SetBranchAddress("GroupTime",&GroupTime);
  Tree_MWPC->SetBranchAddress("AMWPC_anode1",&AMWPC_anode1);
  Tree_MWPC->SetBranchAddress("AMWPC_anode2",&AMWPC_anode2);
  Tree_MWPC->SetBranchAddress("TMWPC_rel",&TMWPC_rel);
  Tree_MWPC->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_MWPC->SetBranchAddress("TMWPC_X",&TMWPC_X);
  Tree_MWPC->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_MWPC->SetBranchAddress("TMWPC_Y",&TMWPC_Y);
  Tree_MWPC->SetBranchAddress("TriggerMap",&TriggerMap);
/*
  Tree_Beta->SetBranchAddress("Event_No",&n);
  Tree_Beta->SetBranchAddress("GroupTime",&GroupTime);
  Tree_Beta->SetBranchAddress("QScint",&QScint);
  Tree_Beta->SetBranchAddress("TScint_rel",&TScint_rel);
  Tree_Beta->SetBranchAddress("AMWPC_anode1",&AMWPC_anode1);
  Tree_Beta->SetBranchAddress("TBeta_diff",&TBeta_diff);
  Tree_Beta->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_Beta->SetBranchAddress("TMWPC_X",&TMWPC_X);
  Tree_Beta->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_Beta->SetBranchAddress("TMWPC_Y",&TMWPC_Y);
  Tree_Beta->SetBranchAddress("TriggerMap",&TriggerMap);
*/
  /*
  Tree_MWPC_Iso->SetBranchAddress("Event_No",&n);  
  Tree_MWPC_Iso->SetBranchAddress("AMWPC_anode",&AMWPC_anode);
  Tree_MWPC_Iso->SetBranchAddress("TMWPC_rel",&TMWPC_rel);

  Tree_MWPCX_Iso->SetBranchAddress("Event_No",&n);
  Tree_MWPCX_Iso->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_MWPCX_Iso->SetBranchAddress("TMWPCX_rel",&TMWPCX_rel);

  Tree_MWPCY_Iso->SetBranchAddress("Event_No",&n);
  Tree_MWPCY_Iso->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_MWPCY_Iso->SetBranchAddress("TMWPCY_rel",&TMWPCY_rel);
*/
  /////////////////////////////////////////////End Root Definition//////////////////////////////////////
  //Fe55 Calibration
  double StartTime = 0;
  int N_MWPC = Tree_MWPC->GetEntries();
//  int N_Beta = 600000;

  //if calibrated
  ifstream poscalfilein;
  poscalfilein.open("CalibrationPos.txt",ios::in);
  for (int i=0;i<4;i++){
    poscalfilein>> CalParX[i];
  }
  for (int i=0;i<4;i++){
    poscalfilein>> CalParY[i];
  }
  for (int i=0;i<4;i++){
    poscalfilein>> CalParA[i];
  }
  poscalfilein.close();
  for(int i=0;i<N_MWPC;i++){
    //Read Tree
    Tree_MWPC->GetEntry(i);
    if (i==0)StartTime = GroupTime;
    //Count # of fired wires
    int NFireX=0;
    int NFireY=0;
    if (MWPC_X.AX1!=0) NFireX++;
    if (MWPC_X.AX2!=0) NFireX++;
    if (MWPC_X.AX3!=0) NFireX++;
    if (MWPC_X.AX4!=0) NFireX++;
    if (MWPC_X.AX5!=0) NFireX++;
    if (MWPC_X.AX6!=0) NFireX++;
    if (MWPC_Y.AY1!=0) NFireY++;
    if (MWPC_Y.AY2!=0) NFireY++;
    if (MWPC_Y.AY3!=0) NFireY++;
    if (MWPC_Y.AY4!=0) NFireY++;
    if (MWPC_Y.AY5!=0) NFireY++;
    if (MWPC_Y.AY6!=0) NFireY++;
    MWPC_X.AX1/=AmpCalX[0];
    MWPC_X.AX2/=AmpCalX[1];
    MWPC_X.AX3/=AmpCalX[2];
    MWPC_X.AX4/=AmpCalX[3];
    MWPC_X.AX5/=AmpCalX[4];
    MWPC_X.AX6/=AmpCalX[5];
    MWPC_Y.AY1/=AmpCalY[0];
    MWPC_Y.AY2/=AmpCalY[1];
    MWPC_Y.AY3/=AmpCalY[2];
    MWPC_Y.AY4/=AmpCalY[3];
    MWPC_Y.AY5/=AmpCalY[4];
    MWPC_Y.AY6/=AmpCalY[5];
//    if (AMWPC_anode1+AMWPC_anode2>1000){
    if (AMWPC_anode1>1500 && AMWPC_anode2>1000){
      //MWPC Anode and Image
      h_MWPC1->Fill(AMWPC_anode1/2.1*1.88);
      h_MWPC2->Fill(AMWPC_anode2);
      h_MWPC_tot->Fill(AMWPC_anode1/2.1*1.88+AMWPC_anode2);
      h_MWPC_Corr->Fill(AMWPC_anode1/2.1*1.88,AMWPC_anode2);
      AnodePos = 21.0*(AMWPC_anode1/2.1*1.88-AMWPC_anode2)/(AMWPC_anode1/2.1*1.88+AMWPC_anode2);
      MWPC_ChargeX = MWPC_X.AX1+MWPC_X.AX2+MWPC_X.AX3+MWPC_X.AX4+MWPC_X.AX5+MWPC_X.AX6;
      MWPC_PosX = (-20.*MWPC_X.AX1 -12.*MWPC_X.AX2-4.*MWPC_X.AX3+4.*MWPC_X.AX4+12.*MWPC_X.AX5+20.*MWPC_X.AX6)/MWPC_ChargeX;
      MWPC_ChargeY = MWPC_Y.AY1+MWPC_Y.AY2+MWPC_Y.AY3+MWPC_Y.AY4+MWPC_Y.AY5+MWPC_Y.AY6;
      MWPC_PosY = (-20.*MWPC_Y.AY1 -12.*MWPC_Y.AY2-4.*MWPC_Y.AY3+4.*MWPC_Y.AY4+12.*MWPC_Y.AY5+20.*MWPC_Y.AY6)/MWPC_ChargeY;
      //Calibrations
/*      MWPC_PosX = CalParX[0]+CalParX[1]*MWPC_PosX+CalParX[2]*pow(MWPC_PosX,2.0)+CalParX[3]*pow(MWPC_PosX,3.0);
      MWPC_PosY = CalParY[0]+CalParY[1]*MWPC_PosY+CalParY[2]*pow(MWPC_PosY,2.0)+CalParY[3]*pow(MWPC_PosY,3.0);
      AnodePos = CalParA[0]+CalParA[1]*AnodePos+CalParA[2]*pow(AnodePos,2.0)+CalParA[3]*pow(AnodePos,3.0);
*/      h_Anode_Pos->Fill(AnodePos);
      h_Cathode_Image->Fill(MWPC_PosX,MWPC_PosY);
      if(MWPC_ChargeY>0)h_ACCorrelation->Fill(MWPC_PosY,AnodePos);
      if (MWPC_ChargeX>0)h_Anode_Image->Fill(MWPC_PosX,AnodePos);
      h_FiredWireX->Fill(NFireX);
      h_FiredWireY->Fill(NFireY);

      if (NFireX>=2){
	h_Cathode_X->Fill(MWPC_PosX);
      }
      if (NFireY>=2){
	h_Cathode_Y->Fill(MWPC_PosY);
      }
      //Conditioned
      if (NFireX==3){
	h_Cathode_X_cond2->Fill(MWPC_PosX);
      }
      if (NFireY==3){
	h_Cathode_Y_cond2->Fill(MWPC_PosY);
      }
      int Nset=3;
      if (NFireX>=Nset){
//	h_Anode_Image_cond->Fill(MWPC_PosX,AnodePos);	
//	h_Cathode_Image_cond->Fill(MWPC_PosX,MWPC_PosY);
	h_Cathode_X_cond->Fill(MWPC_PosX);
      }
      if (NFireY>=Nset){
	h_Anode_Image_cond->Fill(AnodePos,MWPC_PosY);
	if (NFireX>=Nset)h_Cathode_Image_cond->Fill(MWPC_PosX,MWPC_PosY);
	h_Cathode_Y_cond->Fill(MWPC_PosY);
      }
    }
  }//End file loop

  //Calibration
  //CathodeX
  TGraph* CalCathodeXGraph = new TGraph();
  int CalCathodeXPosPlus[10];
  int CalCathodeXPosMinus[10];
  int kPlus,kMinus;
  kPlus=kMinus=0;
  h_Cathode_X_cond->Smooth(3);
  int kZero = h_Cathode_X_cond->FindBin(-0.5);
  int ii=0;
  int jj=0;
  while(ii<8){
    while(h_Cathode_X_cond->GetBinContent(kZero+kPlus+1)<h_Cathode_X_cond->GetBinContent(kZero+kPlus)*1.05){
      kPlus++;
    }
    while(h_Cathode_X_cond->GetBinContent(kZero+kPlus+1)>=h_Cathode_X_cond->GetBinContent(kZero+kPlus)){
      kPlus++;
    }
    CalCathodeXPosPlus[ii]=kZero+kPlus;
    ii++;
  }
  while(jj<7){
    while(h_Cathode_X_cond->GetBinContent(kZero-kMinus-1)<h_Cathode_X_cond->GetBinContent(kZero-kMinus)*1.05){
      kMinus++;
    }
    while(h_Cathode_X_cond->GetBinContent(kZero-kMinus-1)>=h_Cathode_X_cond->GetBinContent(kZero-kMinus)){
      kMinus++;
    }
    CalCathodeXPosMinus[jj]=kZero-kMinus;
    jj++;
  }
  for (int i=0;i<7;i++){
    CalCathodeXGraph->SetPoint(i,h_Cathode_X_cond->GetBinCenter(CalCathodeXPosMinus[6-i]),-16.0+i*2.0);
    cout <<h_Cathode_X_cond->GetBinCenter(CalCathodeXPosMinus[6-i]) << endl; 
  }
  for (int i=0;i<8;i++){
    CalCathodeXGraph->SetPoint(i+7,h_Cathode_X_cond->GetBinCenter(CalCathodeXPosPlus[i]),i*2.0);
    cout <<h_Cathode_X_cond->GetBinCenter(CalCathodeXPosPlus[i]) << endl; 
  }
  cout <<"XXXXXXXXXX"<<endl;
  //CathodeY
  TGraph* CalCathodeYGraph = new TGraph();
  int CalCathodeYPosPlus[10];
  int CalCathodeYPosMinus[10];
  kPlus=kMinus=0;
  h_Cathode_Y_cond->Smooth(3);
  kZero = h_Cathode_Y_cond->FindBin(-0.0);
  ii=0;
  jj=0;
  while(ii<8){
    while(h_Cathode_Y_cond->GetBinContent(kZero+kPlus+1)<h_Cathode_Y_cond->GetBinContent(kZero+kPlus)*1.05){
      kPlus++;
    }
    while(h_Cathode_Y_cond->GetBinContent(kZero+kPlus+1)>=h_Cathode_Y_cond->GetBinContent(kZero+kPlus)){
      kPlus++;
    }
    CalCathodeYPosPlus[ii]=kZero+kPlus;
    ii++;
  }
  while(jj<8){
    while(h_Cathode_Y_cond->GetBinContent(kZero-kMinus-1)<h_Cathode_Y_cond->GetBinContent(kZero-kMinus)*1.05){
      kMinus++;
    }
    while(h_Cathode_Y_cond->GetBinContent(kZero-kMinus-1)>=h_Cathode_Y_cond->GetBinContent(kZero-kMinus)){
      kMinus++;
    }
    CalCathodeYPosMinus[jj]=kZero-kMinus;
    jj++;
  }
  for (int i=0;i<8;i++){
    CalCathodeYGraph->SetPoint(i,h_Cathode_Y_cond->GetBinCenter(CalCathodeYPosMinus[7-i]),-15.0+i*2.0);
    cout <<h_Cathode_Y_cond->GetBinCenter(CalCathodeYPosMinus[7-i]) << endl; 
  }
  for (int i=0;i<8;i++){
    CalCathodeYGraph->SetPoint(i+8,h_Cathode_Y_cond->GetBinCenter(CalCathodeYPosPlus[i]),1+i*2.0);
    cout <<h_Cathode_Y_cond->GetBinCenter(CalCathodeYPosPlus[i]) << endl; 
  }
  cout <<"XXXXXXXXXX"<<endl;
  //Anode
  TGraph* CalAnodeGraph = new TGraph();
  int CalAnodePosPlus[10];
  int CalAnodePosMinus[10];
  kPlus=kMinus=0;
//  h_Anode_Pos->Smooth(2);
  kZero = h_Anode_Pos->FindBin(-0.5);
  ii=0;
  jj=0;
  while(ii<9){
    while(h_Anode_Pos->GetBinContent(kZero+kPlus+1)<h_Anode_Pos->GetBinContent(kZero+kPlus)*1.05){
      kPlus++;
    }
    while(h_Anode_Pos->GetBinContent(kZero+kPlus+1)>=h_Anode_Pos->GetBinContent(kZero+kPlus)){
      kPlus++;
    }
    CalAnodePosPlus[ii]=kZero+kPlus;
    ii++;
  }
  while(jj<8){
    while(h_Anode_Pos->GetBinContent(kZero-kMinus-1)<h_Anode_Pos->GetBinContent(kZero-kMinus)*1.05){
      kMinus++;
    }
    while(h_Anode_Pos->GetBinContent(kZero-kMinus-1)>=h_Anode_Pos->GetBinContent(kZero-kMinus)){
      kMinus++;
    }
    CalAnodePosMinus[jj]=kZero-kMinus;
    jj++;
  }
  for (int i=0;i<8;i++){
    CalAnodeGraph->SetPoint(i,h_Anode_Pos->GetBinCenter(CalAnodePosMinus[7-i]),-15.0+i*2.0);
    cout <<h_Anode_Pos->GetBinCenter(CalAnodePosMinus[7-i]) << endl; 
  }
  for (int i=0;i<8;i++){
    CalAnodeGraph->SetPoint(i+8,h_Anode_Pos->GetBinCenter(CalAnodePosPlus[i]),1+i*2.0);
    cout <<h_Anode_Pos->GetBinCenter(CalAnodePosPlus[i]) << endl; 
  }
  cout <<"XXXXXXXXXX"<<endl;

  h_FiredWireX->Scale(100.0/N_MWPC);
  h_FiredWireY->Scale(100.0/N_MWPC);

  cout << N_MWPC << " events"<<endl;

  TCanvas *c1 = new TCanvas("c1","MWPC");
  c1->Divide(2,2);
  c1->cd(1);
  h_Cathode_Image->Draw("colz");
  c1->cd(2);
  h_Anode_Image->Draw("colz");
  c1->cd(3);
  h_ACCorrelation->Draw("colz");
  c1->cd(4);
  h_Anode_Pos->Draw();
  spectrumfile = argv[1];
  sprintf(filenamebuf,"_Beta_spectra.pdf(");
  spectrumfile += filenamebuf;	
  c1->SaveAs(spectrumfile.c_str());

  TCanvas *c2 = new TCanvas("c2","MWPC2");
  c2->Divide(3,2);
  c2->cd(1);
  h_Cathode_X->Draw();
  c2->cd(2);
  h_Cathode_X_cond->Draw();
  c2->cd(3);
  h_Cathode_X_cond2->Draw();
  c2->cd(4);
  h_Cathode_Y->Draw();
  c2->cd(5);
  h_Cathode_Y_cond->Draw();
  c2->cd(6);
  h_Cathode_Y_cond2->Draw();
  spectrumfile = argv[1];
  sprintf(filenamebuf,"_Beta_spectra.pdf");
  spectrumfile += filenamebuf;	
  c2->SaveAs(spectrumfile.c_str());

  TCanvas *c4 = new TCanvas("c4","MWPC2");
  h_MWPC_tot->Draw();
  spectrumfile = argv[1];
  sprintf(filenamebuf,"_Beta_spectra.pdf");
  spectrumfile += filenamebuf;	
  c4->SaveAs(spectrumfile.c_str());

  TCanvas *c5 = new TCanvas("c5","Anodes");
  c5->Divide(2,2);
  c5->cd(1);
  h_MWPC_Corr->Draw("colz");
  c5->cd(2);
  h_MWPC1->Draw();
  c5->cd(3);
  h_MWPC2->Draw();
  c5->SaveAs(spectrumfile.c_str());

  TCanvas *c6 = new TCanvas("c6","Anodes");
//  ofstream calout;
//  calout.open("CalibrationPos.txt",ios::out);
  c6->Divide(2,2);
  c6->cd(1);
  CalCathodeXGraph->Draw("AP");
  CalCathodeXGraph->SetMarkerSize(20);
  CalCathodeXGraph->SetLineWidth(5);
  CalCathodeXGraph->SetMarkerColor(kBlue);
  CalCathodeXGraph->Fit("pol3");
  TF1 * fitFuncX = CalCathodeXGraph->GetFunction("pol3");
  for (int i=0;i<4;i++){
    CalParX[i] = fitFuncX->GetParameter(i);
//    calout<<CalParX[i]<<" ";
  }
//  calout<<endl;

  c6->cd(2);
  CalCathodeYGraph->Draw("AP");
  CalCathodeYGraph->SetMarkerSize(20);
  CalCathodeYGraph->SetMarkerColor(kBlue);
  CalCathodeYGraph->Fit("pol3");
  TF1 * fitFuncY = CalCathodeYGraph->GetFunction("pol3");
  for (int i=0;i<4;i++){
    CalParY[i] = fitFuncY->GetParameter(i);
//    calout<<CalParY[i]<<" ";
  }
//  calout<<endl;

  c6->cd(3);
  CalAnodeGraph->Draw("AP");
  CalAnodeGraph->SetMarkerSize(20);
  CalAnodeGraph->SetMarkerColor(kBlue);
  CalAnodeGraph->Fit("pol3");
  TF1 * fitFuncA = CalAnodeGraph->GetFunction("pol3");
  for (int i=0;i<4;i++){
    CalParA[i] = fitFuncA->GetParameter(i);
//    calout<<CalParA[i]<<" ";
  }
//  calout<<endl;
//  calout.close();
  c6->SaveAs(spectrumfile.c_str());

  TCanvas *c3 = new TCanvas("c3","MWPC3");
  c3->Divide(2,2);
  c3->cd(1);
  h_Anode_Image->Draw("colz");
  c3->cd(2);
  h_Cathode_Image->Draw("colz");
  c3->cd(3);
  h_Anode_Image_cond->Draw("colz");
//  h_FiredWireX->Draw();
  c3->cd(4);
  h_Cathode_Image_cond->Draw("colz");
//  h_FiredWireY->Draw();
  spectrumfile = argv[1];
  sprintf(filenamebuf,"_Beta_spectra.pdf)");
  spectrumfile += filenamebuf;	
  c3->SaveAs(spectrumfile.c_str());
  delete f;

  spectrumfile = argv[1];
  sprintf(filenamebuf,"_Hist.root");
  spectrumfile += filenamebuf;	
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");
  h_MWPC1->Write();
  h_MWPC2->Write();
  h_MWPC_tot->Write();
  h_Anode_Pos->Write();
  h_Cathode_Image->Write();
  h_ACCorrelation->Write();
  h_Anode_Image->Write();
  h_FiredWireX->Write();
  h_FiredWireY->Write();

  h_Cathode_X->Write();
  h_Cathode_Y->Write();
  h_Cathode_X_cond2->Write();
  h_Cathode_Y_cond2->Write();
  h_Anode_Image_cond->Write();
  h_Cathode_Image_cond->Write();
  h_Cathode_X_cond->Write();
  h_Cathode_Y_cond->Write();
  h_MWPC_Corr->Write();

  CalCathodeXGraph->Write();
  CalCathodeYGraph->Write();
  CalAnodeGraph->Write();
  histfile->Close();
  return EXIT_SUCCESS;
}


