
#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string>
#include <sstream> 
#include <vector>
#include <stdint.h>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TGraphErrors.h"
//Analyzer includes
#include "Analyzer.h"
#include "Calibration.h"

using namespace std;

/***********************************************************************************/
/*int Calibration::SetupMWPCCal(double A_Th,double C_Th)
{
  MWPCAnodeThreshold = A_Th;
  MWPCCathodeThreshold = C_Th;
  return 0;
}*/
int Calibration::ConfigCalMWPC(string Mode,bool Keep)
{
  CalibrationScheme = Mode;
  if (Mode.compare("Micro")!=0 && Mode.compare("Pol")!=0){
    cout << "Only Micro and Pol are allowed mode."<<endl;
    cout << "Use default Micro."<<endl;
    CalibrationScheme="Micro";
  }
  KeepLessTrigger = Keep;
  return 0;
}

/***********************************************************************************/
int Calibration::CalibrateMWPCPreview(string FilePrefix,int FileID,string Source)
{
  CurrentAnalyzer->SetMode("MWPC");
  CurrentAnalyzer->Calibrating = true;
  string spectrumfile;

  int n = 0;  //  n of data read
  double CathodeThreshold = CurrentAnalyzer->SysWParameters["MWPCC_Threshold"][0];

  double Tref = 0;
  double Tref_A = 0;
  double AMWPC_anode1;
  double AMWPC_anode2;
  double TMWPC_rel[2];
  DataStruct_MWPC_X MWPC_X;
  DataStruct_TMWPC_X TMWPC_X;
  DataStruct_MWPC_Y MWPC_Y;
  DataStruct_TMWPC_Y TMWPC_Y;
  //TFile* tfilein = NULL; //pointer to input root file
  //TTree * InTree = NULL; //input tree pointer

  double GroupTime;

  unsigned short TriggerMap;

  double TMWPC_anode1;
  double TMWPC_anode2;
  double TMWPC_X1,TMWPC_X2,TMWPC_X3,TMWPC_X4,TMWPC_X5,TMWPC_X6;
  double TMWPC_Y1,TMWPC_Y2,TMWPC_Y3,TMWPC_Y4,TMWPC_Y5,TMWPC_Y6;

  double MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY;
  double AnodePos;

  //For sorting Cathode signals
  int TriggerMultiplicity = 6;
  double* Xsig[6];
  double* Ysig[6];
  Xsig[0] = &(MWPC_X.AX1);
  Xsig[1] = &(MWPC_X.AX2);
  Xsig[2] = &(MWPC_X.AX3);
  Xsig[3] = &(MWPC_X.AX4);
  Xsig[4] = &(MWPC_X.AX5);
  Xsig[5] = &(MWPC_X.AX6);
  Ysig[0] = &(MWPC_Y.AY1);
  Ysig[1] = &(MWPC_Y.AY2);
  Ysig[2] = &(MWPC_Y.AY3);
  Ysig[3] = &(MWPC_Y.AY4);
  Ysig[4] = &(MWPC_Y.AY5);
  Ysig[5] = &(MWPC_Y.AY6);
  // root histos
  TH1D *h_MWPC1  = new TH1D("h_MWPC1","MWPC Anode1 spectrum",1024,0,655360);
  TH1D *h_MWPC2  = new TH1D("h_MWPC2","MWPC Anode2 spectrum",1024,0,655360);
  TH2* h_A1A2 = new TH2D("h_A1A2","A1 A2 correlation",1024,0,655360,1024,0,655360);
  TH2* h_PosX_Charge = new TH2D("h_PosX_Charge","X-TotalCharge correlation",800,-25.03125,24.96875,400,0,200);
  TH2* h_PosY_Charge = new TH2D("h_PosY_Charge","Y-TotalCharge correlation",800,-25.03125,24.96875,400,0,200);
  TH1D *h_MWPC_tot  = new TH1D("h_MWPC_tot","MWPC Anode_tot spectrum",1024,0,512);
  TH1D *h_MWPC_Cond  = new TH1D("h_MWPC_Cond","MWPC Anode_tot spectrum",1024,0,655360);
  TH1 *h_FiredWireX = new TH1D("h_FiredWireX","# of fired stripes X",10,0,10);
  TH1 *h_FiredWireY = new TH1D("h_FiredWireY","# of fired stripes Y",10,0,10);

  TH1D *h_Cathode_X = new TH1D("h_Cathode_X","MWPC CathodeX position spectrum",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y = new TH1D("h_Cathode_Y","MWPC CathodeY position spectrum",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond2 = new TH1D("h_Cathode_X_cond2","MWPC CathodeX position spectrum cond2",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond2 = new TH1D("h_Cathode_Y_cond2","MWPC CathodeY position spectrum cond2",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond3 = new TH1D("h_Cathode_X_cond3","MWPC CathodeX position spectrum cond3",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond3 = new TH1D("h_Cathode_Y_cond3","MWPC CathodeY position spectrum cond3",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond4 = new TH1D("h_Cathode_X_cond4","MWPC CathodeX position spectrum cond4",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond4 = new TH1D("h_Cathode_Y_cond4","MWPC CathodeY position spectrum cond4",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond5 = new TH1D("h_Cathode_X_cond5","MWPC CathodeX position spectrum cond5",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond5 = new TH1D("h_Cathode_Y_cond5","MWPC CathodeY position spectrum cond5",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond6 = new TH1D("h_Cathode_X_cond6","MWPC CathodeX position spectrum cond6",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond6 = new TH1D("h_Cathode_Y_cond6","MWPC CathodeY position spectrum cond6",800,-25.03125,24.96875);
  TH1D *h_Anode_Pos = new TH1D("h_Anode_Pos","MWPC Anode position spectrum",800,-25.03125,24.96875);
  TH2D *h_Cathode_Image = new TH2D("h_Cathode_Image","2D image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Anode_Image = new TH2D("h_Anode_Image","2D Anode image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_MWPC_Corr  = new TH2D("h_MWPC1_2","MWPC Anode1-2 spectrum",1024,0,655360,1024,0,655360);
  TH2D *h_Cathode_Image_cond = new TH2D("h_Cathode_Image_cond","2D image of MWPC, more than 3 trigger",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Anode_Image_cond = new TH2D("h_Anode_Image_cond","2D Anode image of MWPC more than 3 trigger",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_ACCorrelation = new TH2D("h_ACCorrelation","MWPC anode cathode x correlation",400,-25,25,400,-25,25);
  TH1D *h_Charge_X = new TH1D("h_Charge_X","Total Charge on MWPC X",1024,0,512);
  TH1D *h_Charge_Y = new TH1D("h_Charge_Y","Total Charge on MWPC Y",1024,0,512);
  TH1D *h_Charge_C = new TH1D("h_Charge_C","MWPC Cathode_tot spectrum",1024,0,512);
  TH2D *h_Charge_XY = new TH2D("h_Charge_XY","Cathode X-Y charge correlation",400,0,512,400,0,512);
  TH2D *h_Charge_AC = new TH2D("h_Charge_AC","Cathode-Anode charge correlation",400,0,512,400,0,512);

  TH1D *h_Charge_X1 = new TH1D("h_Charge_X1","Total Charge on MWPC X1",1000,0,20000);
  TH1D *h_Charge_X2 = new TH1D("h_Charge_X2","Total Charge on MWPC X2",1000,0,20000);
  TH1D *h_Charge_X3 = new TH1D("h_Charge_X3","Total Charge on MWPC X3",1000,0,20000);
  TH1D *h_Charge_X4 = new TH1D("h_Charge_X4","Total Charge on MWPC X4",1000,0,20000);
  TH1D *h_Charge_X5 = new TH1D("h_Charge_X5","Total Charge on MWPC X5",1000,0,20000);
  TH1D *h_Charge_X6 = new TH1D("h_Charge_X6","Total Charge on MWPC X6",1000,0,20000);

  //Histograms for calibration
  //X spectrum, using 5 triggered stripes
  TH1D *h_Cathode_X_Group0 = new TH1D("h_Cathode_X_Group0","MWPC CathodeX position spectrum Group0",800,-25.03125,24.96875);
  //X spectrum, using 4 triggered stripes
  TH1D *h_Cathode_X_Group1 = new TH1D("h_Cathode_X_Group1","MWPC CathodeX position spectrum Group1",800,-25.03125,24.96875);
  //X spectrum, using 3 triggered stripes
  TH1D *h_Cathode_X_Group2 = new TH1D("h_Cathode_X_Group2","MWPC CathodeX position spectrum Group2",800,-25.03125,24.96875);

  ostringstream convert;
  convert << setfill('0') << setw(4)<<FileID;
  TString fName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + FilePrefix + string("_") + convert.str();
  fName += ".root";
  //if(OpenExpInputFile(fName,tfilein)==-1) return -1;
  //SetExpInTreeAddress(tfilein,InTree,ExpData);
  TFile *f = new TFile (fName.Data(), "read");

  TTree *Tree_MWPC = (TTree*)f->Get("Tree_MWPC");       //Exclude Scint event

  Tree_MWPC->SetBranchAddress("Event_No",&n);
  Tree_MWPC->SetBranchAddress("GroupTime",&GroupTime);
  Tree_MWPC->SetBranchAddress("AMWPC_anode1",&AMWPC_anode1);
  Tree_MWPC->SetBranchAddress("AMWPC_anode2",&AMWPC_anode2);
  Tree_MWPC->SetBranchAddress("TMWPC_rel",&TMWPC_rel);
  Tree_MWPC->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_MWPC->SetBranchAddress("TMWPC_X",&TMWPC_X);
  Tree_MWPC->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_MWPC->SetBranchAddress("TMWPC_Y",&TMWPC_Y);
  Tree_MWPC->SetBranchAddress("TriggerMap",&TriggerMap);

  double StartTime = 0;
  int N_MWPC = Tree_MWPC->GetEntries();

  for(int i=0;i<N_MWPC;i++){
    //Read Tree
    Tree_MWPC->GetEntry(i);
    if (i==0)StartTime = GroupTime;
    if (Source.compare("Bi207")==0 && (TriggerMap&(1<<1))==1)continue;
    //Calibrate cathode amplifer signals
/*    MWPC_X.AX1/=CurrentAnalyzer->AmpCalX[0];
    MWPC_X.AX2/=CurrentAnalyzer->AmpCalX[1];
    MWPC_X.AX3/=CurrentAnalyzer->AmpCalX[2];
    MWPC_X.AX4/=CurrentAnalyzer->AmpCalX[3];
    MWPC_X.AX5/=CurrentAnalyzer->AmpCalX[4];
    MWPC_X.AX6/=CurrentAnalyzer->AmpCalX[5];
    MWPC_Y.AY1/=CurrentAnalyzer->AmpCalY[0];
    MWPC_Y.AY2/=CurrentAnalyzer->AmpCalY[1];
    MWPC_Y.AY3/=CurrentAnalyzer->AmpCalY[2];
    MWPC_Y.AY4/=CurrentAnalyzer->AmpCalY[3];
    MWPC_Y.AY5/=CurrentAnalyzer->AmpCalY[4];
    MWPC_Y.AY6/=CurrentAnalyzer->AmpCalY[5];*/
    //Fill each X cathode signal
    if (MWPC_X.AX1>0)h_Charge_X1->Fill(MWPC_X.AX1); 
    if (MWPC_X.AX2>0)h_Charge_X2->Fill(MWPC_X.AX2); 
    if (MWPC_X.AX3>0)h_Charge_X3->Fill(MWPC_X.AX3); 
    if (MWPC_X.AX4>0)h_Charge_X4->Fill(MWPC_X.AX4); 
    if (MWPC_X.AX5>0)h_Charge_X5->Fill(MWPC_X.AX5); 
    if (MWPC_X.AX6>0)h_Charge_X6->Fill(MWPC_X.AX6); 
    //Sort signals according to their amplitudes
    //X
    double *ptr;
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Xsig[k])<*(Xsig[k+1])){
	  ptr = Xsig[k];
	  Xsig[k]=Xsig[k+1];
	  Xsig[k+1]=ptr;
	}
      }
    }
    //Y
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Ysig[k])<*(Ysig[k+1])){
	  ptr = Ysig[k];
	  Ysig[k]=Ysig[k+1];
	  Ysig[k+1]=ptr;
	}
      }
    }

    TriggerMultiplicity = 6;
    //Set Smallest ones to zero, according to trigger multiplicity
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    //Count # of fired wires
    int NFireX=0;
    int NFireY=0;
    //Count Triggered Wires
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    //Calibrate relative amplitude (for future)
    AMWPC_anode2 = AMWPC_anode2*CurrentAnalyzer->AmpCalA[0]/CurrentAnalyzer->AmpCalA[1];
    h_A1A2->Fill(AMWPC_anode1,AMWPC_anode2);
    if ((AMWPC_anode1+AMWPC_anode2>1000)){
      //MWPC Anode and Image
      h_MWPC1->Fill(AMWPC_anode1);
      h_MWPC2->Fill(AMWPC_anode2);
      h_MWPC_tot->Fill((AMWPC_anode1+AMWPC_anode2)*0.001);
      h_MWPC_Corr->Fill(AMWPC_anode1,AMWPC_anode2);
      //Process MWPC Data, signal to position and charge
      ProcessMWPCData(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);

      h_Anode_Pos->Fill(AnodePos);
      h_Cathode_Image->Fill(MWPC_PosX,MWPC_PosY);
      if(MWPC_ChargeY>0)h_ACCorrelation->Fill(MWPC_PosY,AnodePos);
      if (MWPC_ChargeX>0)h_Anode_Image->Fill(MWPC_PosX,AnodePos);
      h_Charge_X->Fill(MWPC_ChargeX*0.001);
      h_Charge_Y->Fill(MWPC_ChargeY*0.001);
      h_Charge_C->Fill((MWPC_ChargeX+MWPC_ChargeY)*0.001);
      h_Charge_XY->Fill(MWPC_ChargeX*0.001,MWPC_ChargeY*0.001);
      h_Charge_AC->Fill((MWPC_ChargeX+MWPC_ChargeY)*0.001,(AMWPC_anode1+AMWPC_anode2)*0.001);

      h_FiredWireX->Fill(NFireX);
      h_FiredWireY->Fill(NFireY);

      h_Cathode_X->Fill(MWPC_PosX);
      h_Cathode_Y->Fill(MWPC_PosY);

      h_PosY_Charge->Fill(AnodePos,(AMWPC_anode1+AMWPC_anode2)*0.001);
//      h_PosY_Charge->Fill(MWPC_PosY,MWPC_ChargeY);
      h_PosX_Charge->Fill(MWPC_PosX,(AMWPC_anode1+AMWPC_anode2)*0.001);
      //Conditioned
      if (NFireX==2){
	h_Cathode_X_cond2->Fill(MWPC_PosX);
      }
      if (NFireY==2){
	h_Cathode_Y_cond2->Fill(MWPC_PosY);
      }
      if (NFireX==3){
	h_Cathode_X_cond3->Fill(MWPC_PosX);
      }
      if (NFireY==3){
	h_Cathode_Y_cond3->Fill(MWPC_PosY);
      }
      if (NFireX==4){
	h_Cathode_X_cond4->Fill(MWPC_PosX);
      }
      if (NFireY==4){
	h_Cathode_Y_cond4->Fill(MWPC_PosY);
      }
      if (NFireX==5){
	h_Cathode_X_cond5->Fill(MWPC_PosX);
      }
      if (NFireY==5){
	h_Cathode_Y_cond5->Fill(MWPC_PosY);
      }
      if (NFireX==6){
	h_Cathode_X_cond6->Fill(MWPC_PosX);
      }
      if (NFireY==6){
	h_Cathode_Y_cond6->Fill(MWPC_PosY);
      }
      if (AnodePos>-16 && AnodePos<16 && MWPC_PosX>-15 && MWPC_PosX<15){
	h_MWPC_Cond->Fill(AMWPC_anode1+AMWPC_anode2);
	h_Anode_Image_cond->Fill(MWPC_PosX,AnodePos);
      }
      if (MWPC_PosY>-16 && MWPC_PosY<16 && MWPC_PosX>-15 && MWPC_PosX<15){
	h_Cathode_Image_cond->Fill(MWPC_PosX,MWPC_PosY);
      }
    }
    //Create the 3 groups for calibration
    TriggerMultiplicity = 5;
    //Set Smallest ones to zero, according to trigger multiplicity
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    NFireX=0;
    NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>1000)){
      ProcessMWPCData(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
      if (NFireX==5){
	h_Cathode_X_Group0->Fill(MWPC_PosX);
      }
    }
    TriggerMultiplicity = 4;
    //Set Smallest ones to zero, according to trigger multiplicity
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    NFireX=0;
    NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>1000)){
      ProcessMWPCData(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
      if (NFireX==4){
	h_Cathode_X_Group1->Fill(MWPC_PosX);
      }
    }
    TriggerMultiplicity = 3;
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    NFireX=0;
    NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>1000)){
      ProcessMWPCData(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
      if (NFireX==3){
	h_Cathode_X_Group2->Fill(MWPC_PosX);
      }
    }
  }//End file loop
  h_FiredWireX->Scale(100.0/N_MWPC);
  h_FiredWireY->Scale(100.0/N_MWPC);
  cout << N_MWPC << " events"<<endl;

  //Calibration
  //Now the primitive peaks are constructed by hand.
  //In the future, computer needs to do this
//  ofstream calfileout;
//  char filename[100];
//  sprintf(filename,"%s/MWPCPosCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
//  calfileout.open(filename,ios::out);
  
    
  /************************************************************************************************/
  //Write to root file
  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_MWPCCalPreview.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");
  h_MWPC1->Write();
  h_MWPC2->Write();
  h_MWPC_tot->Write();
  h_Charge_X->Write();
  h_Charge_Y->Write();
  h_Charge_C->Write();
  h_Charge_XY->Write();
  h_Charge_AC->Write();
  h_MWPC_Cond->Write();
  h_Anode_Pos->Write();
  h_Cathode_Image->Write();
  h_ACCorrelation->Write();
  h_Anode_Image->Write();
  h_FiredWireX->Write();
  h_FiredWireY->Write();

  h_Charge_X1->Write();
  h_Charge_X2->Write();
  h_Charge_X3->Write();
  h_Charge_X4->Write();
  h_Charge_X5->Write();
  h_Charge_X6->Write();
  h_Cathode_X->Write();
  h_Cathode_Y->Write();
  h_Cathode_X_cond2->Write();
  h_Cathode_Y_cond2->Write();
  h_Cathode_X_cond3->Write();
  h_Cathode_Y_cond3->Write();
  h_Cathode_X_cond4->Write();
  h_Cathode_Y_cond4->Write();
  h_Cathode_X_cond5->Write();
  h_Cathode_Y_cond5->Write();
  h_Cathode_X_cond6->Write();
  h_Cathode_Y_cond6->Write();
  h_Cathode_X_Group0->Write();
  h_Cathode_X_Group1->Write();
  h_Cathode_X_Group2->Write();
  h_Anode_Image_cond->Write();
  h_Cathode_Image_cond->Write();
  h_MWPC_Corr->Write();
  h_A1A2->Write();
  h_PosX_Charge->Write();
  h_PosY_Charge->Write();
  
  histfile->Close();

  return 0;
}

/***********************************************************************************/
int Calibration::CalibrateMWPC(string FilePrefix,int FileID,int CalID,string Source)
{
  string spectrumfile;

  int n = 0;  //  n of data read
  double CathodeThreshold = CurrentAnalyzer->SysWParameters["MWPCC_Threshold"][0];

  double Tref = 0;
  double Tref_A = 0;
  double AMWPC_anode1;
  double AMWPC_anode2;
  double TMWPC_rel[2];
  DataStruct_MWPC_X MWPC_X;
  DataStruct_TMWPC_X TMWPC_X;
  DataStruct_MWPC_Y MWPC_Y;
  DataStruct_TMWPC_Y TMWPC_Y;

  double GroupTime;

  unsigned short TriggerMap;

  double TMWPC_anode1;
  double TMWPC_anode2;
  double TMWPC_X1,TMWPC_X2,TMWPC_X3,TMWPC_X4,TMWPC_X5,TMWPC_X6;
  double TMWPC_Y1,TMWPC_Y2,TMWPC_Y3,TMWPC_Y4,TMWPC_Y5,TMWPC_Y6;

  double MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY;
  double AnodePos;

  //For sorting Cathode signals
  int TriggerMultiplicity = 6;
  double* Xsig[6];
  double* Ysig[6];
  Xsig[0] = &(MWPC_X.AX1);
  Xsig[1] = &(MWPC_X.AX2);
  Xsig[2] = &(MWPC_X.AX3);
  Xsig[3] = &(MWPC_X.AX4);
  Xsig[4] = &(MWPC_X.AX5);
  Xsig[5] = &(MWPC_X.AX6);
  Ysig[0] = &(MWPC_Y.AY1);
  Ysig[1] = &(MWPC_Y.AY2);
  Ysig[2] = &(MWPC_Y.AY3);
  Ysig[3] = &(MWPC_Y.AY4);
  Ysig[4] = &(MWPC_Y.AY5);
  Ysig[5] = &(MWPC_Y.AY6);

  //Create Cathode Signal Calibration data structure
  double CathodeSignalMap[257][18][12];
  for (int i=0;i<257;i++){
    for (int j=0;j<18;j++){
      for (int k=0;k<12;k++){
	CathodeSignalMap[i][j][k] = 0;
      }
    }
  }

  //For Calibration
  /*
  double PrimitivePeaksX0[9] = {-7.875,-5.875,-3.9375,-2.0,0.0,2.0625,4.0,5.9375,7.9375};
  double PrimitivePeaksX1[15] = {-11.6875,-9.875,-8.0,-6.0,-4.1875,-3.75,-1.875,0.0,1.9375,3.8125,4.25,6.0625,8.0,10.0,11.75};
  double PrimitivePeaksX2[20] = {-15.3125,-13.85,-11.875,-10.25,-8.75,-7.375,-5.625,-3.9375,-2.3125,-0.55,0.56,2.32,4.0,5.65,7.45,8.56,10.3125,11.9375,13.9,15.4};
  double PrimitivePeaksY[18] = {-15.45,-13.85,-11.875,-10.25,-8.5,-7.45,-5.625,-3.0,-1.0,1.0,3.25,5.25,7.5,9.0,11.0,12.75,14.5,16.0};

  double PrimitivePeaksA[18] = {-15.9,-13.8,-11.85,-10.1,-8.15,-6.25,-4.45,-2.6,-0.75,1.0,2.7,4.6,6.4,8.2,10.15,12.05,14.1,16.0};

  double ExactPos0[9] = {-8.0,-6.0,-4.0,-2.0,0.0,2.0,4.0,6.0,8.0};
  double ExactPos1[15] = {-12.0,-10.0,-8.0,-6.0,-4.0,-4.0,-2.0,0.0,2.0,4.0,4.0,6.0,8.0,10.0,12.0};
  double ExactPos2[20] = {-16.0,-14.0,-12.0,-10.0,-8.0,-8.0,-6.0,-4.0,-2.0,0.0,0.0,2.0,4.0,6.0,8.0,8.0,10.0,12.0,14.0,16.0};
  double ExactPosA[18] = {-17.0,-15.0,-13.0,-11.0,-9.0,-7.0,-5.0,-3.0,-1.0,1.0,3.0,5.0,7.0,9.0,11.0,13.0,15.0,17.0};
*/
  //New version: read in peak positions from file
  double PrimitivePeaksX0[9];
  double PrimitivePeaksX1[15];
  double PrimitivePeaksX2[20];
  double PrimitivePeaksY[18];
  double PrimitivePeaksA[18];

  double ExactPos0[9];
  double ExactPos1[15]; 
  double ExactPos2[20];
  double ExactPosA[18]; 

  ifstream calpeakfilein;
  char filename[500];
  sprintf(filename,"%s/MWPCPosPeaks%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  calpeakfilein.open(filename,ios::in);
  if (!calpeakfilein.is_open()){
    cout <<"Error! file "<< filename<<" does not exist.\n"<<endl;
    return -1;
  }
  for (int i=0;i<9;i++){
    calpeakfilein>>ExactPos0[i]>>PrimitivePeaksX0[i];
  }
  for (int i=0;i<15;i++){
    calpeakfilein>>ExactPos1[i]>>PrimitivePeaksX1[i];
  }
  for (int i=0;i<20;i++){
    calpeakfilein>>ExactPos2[i]>>PrimitivePeaksX2[i];
  }
  for (int i=0;i<18;i++){
    calpeakfilein>>ExactPosA[i]>>PrimitivePeaksA[i];
  }
  calpeakfilein.close();

  //Peaks after scanning for maximum
  double PeaksX0[9];
  double PeaksX1[15];
  double PeaksX2[20];
  double PeaksA[18];

  TGraph* XGraph0;
  TGraph* XGraph1[3];
  TGraph* XGraph2[4];
  TGraph* AGraph;
//  XGraph1 = new (TGraph*)[3];
//  XGraph2 = new (TGraph*)[4];
  XGraph0 = new TGraph();
  AGraph = new TGraph();
  XGraph0->SetName("XGraph0");
  AGraph->SetName("AGraph");
  for (int i=0;i<3;i++){
    XGraph1[i] = new TGraph();
    XGraph1[i]->SetName(Form("XGraph1_%d",i));
  }
  for (int i=0;i<4;i++){
    XGraph2[i] = new TGraph();
    XGraph2[i]->SetName(Form("XGraph2_%d",i));
  }
  TF1* fitFuncA;
  TF1* fitFuncX0;
  TF1* fitFuncX1[3];
  TF1* fitFuncX2[4];
  double FitParametersA[4];
  double FitParametersX0[4];
  double FitParametersX1[3][4];
  double FitParametersX2[4][4];
/*  for (int j=0;j<6;j++){
    cout << CurrentAnalyzer->AmpCalX[j] << " ";
  }
  cout << endl;
  for (int j=0;j<6;j++){
    cout << CurrentAnalyzer->AmpCalY[j] << " ";
  }
  cout << endl;
  cout << CurrentAnalyzer->AmpCalA[0]<<" "<<CurrentAnalyzer->AmpCalA[1]<<endl;
  */
  // root histos
  TH1D *h_MWPC1  = new TH1D("h_MWPC1","MWPC Anode1 spectrum",1024,0,655360);
  TH1D *h_MWPC2  = new TH1D("h_MWPC2","MWPC Anode2 spectrum",1024,0,655360);
  TH2* h_A1A2 = new TH2D("h_A1A2","A1 A2 correlation",1024,0,655360,1024,0,655360);
  TH2* h_PosX_Charge = new TH2D("h_PosX_Charge","X-TotalCharge correlation",800,-25.03125,24.96875,400,0,200);
  TH2* h_PosY_Charge = new TH2D("h_PosY_Charge","Y-TotalCharge correlation",800,-25.03125,24.96875,400,0,200);
  TH1D *h_MWPC_tot  = new TH1D("h_MWPC_tot","MWPC Anode_tot spectrum",1024,0,512);
  TH1D *h_MWPC_Cond  = new TH1D("h_MWPC_Cond","MWPC Anode_tot spectrum",1024,0,655360);
  TH1 *h_FiredWireX = new TH1D("h_FiredWireX","# of fired stripes X",10,0,10);
  TH1 *h_FiredWireY = new TH1D("h_FiredWireY","# of fired stripes Y",10,0,10);

  TH1D *h_Cathode_X = new TH1D("h_Cathode_X","MWPC CathodeX position spectrum",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y = new TH1D("h_Cathode_Y","MWPC CathodeY position spectrum",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond2 = new TH1D("h_Cathode_X_cond2","MWPC CathodeX position spectrum cond2",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond2 = new TH1D("h_Cathode_Y_cond2","MWPC CathodeY position spectrum cond2",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond3 = new TH1D("h_Cathode_X_cond3","MWPC CathodeX position spectrum cond3",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond3 = new TH1D("h_Cathode_Y_cond3","MWPC CathodeY position spectrum cond3",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond4 = new TH1D("h_Cathode_X_cond4","MWPC CathodeX position spectrum cond4",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond4 = new TH1D("h_Cathode_Y_cond4","MWPC CathodeY position spectrum cond4",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond5 = new TH1D("h_Cathode_X_cond5","MWPC CathodeX position spectrum cond5",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond5 = new TH1D("h_Cathode_Y_cond5","MWPC CathodeY position spectrum cond5",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond6 = new TH1D("h_Cathode_X_cond6","MWPC CathodeX position spectrum cond6",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond6 = new TH1D("h_Cathode_Y_cond6","MWPC CathodeY position spectrum cond6",800,-25.03125,24.96875);
  TH1D *h_Anode_Pos = new TH1D("h_Anode_Pos","MWPC Anode position spectrum",800,-25.03125,24.96875);
  TH2D *h_Cathode_Image = new TH2D("h_Cathode_Image","2D image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Anode_Image = new TH2D("h_Anode_Image","2D Anode image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_MWPC_Corr  = new TH2D("h_MWPC1_2","MWPC Anode1-2 spectrum",1024,0,655360,1024,0,655360);
  TH2D *h_Cathode_Image_cond = new TH2D("h_Cathode_Image_cond","2D image of MWPC, more than 3 trigger",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Anode_Image_cond = new TH2D("h_Anode_Image_cond","2D Anode image of MWPC more than 3 trigger",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_ACCorrelation = new TH2D("h_ACCorrelation","MWPC anode cathode x correlation",400,-25,25,400,-25,25);
  TH1D *h_Charge_X = new TH1D("h_Charge_X","Total Charge on MWPC X",1024,0,512);
  TH1D *h_Charge_Y = new TH1D("h_Charge_Y","Total Charge on MWPC Y",1024,0,512);
  TH1D *h_Charge_C = new TH1D("h_Charge_C","MWPC Cathode_tot spectrum",1024,0,512);
  TH2D *h_Charge_XY = new TH2D("h_Charge_XY","Cathode X-Y charge correlation",400,0,512,400,0,512);
  TH2D *h_Charge_AC = new TH2D("h_Charge_AC","Cathode-Anode charge correlation",400,0,512,400,0,512);

  TH1D *h_Charge_X1 = new TH1D("h_Charge_X1","Total Charge on MWPC X1",1000,0,20000);
  TH1D *h_Charge_X2 = new TH1D("h_Charge_X2","Total Charge on MWPC X2",1000,0,20000);
  TH1D *h_Charge_X3 = new TH1D("h_Charge_X3","Total Charge on MWPC X3",1000,0,20000);
  TH1D *h_Charge_X4 = new TH1D("h_Charge_X4","Total Charge on MWPC X4",1000,0,20000);
  TH1D *h_Charge_X5 = new TH1D("h_Charge_X5","Total Charge on MWPC X5",1000,0,20000);
  TH1D *h_Charge_X6 = new TH1D("h_Charge_X6","Total Charge on MWPC X6",1000,0,20000);

  //Histograms for calibration
  //X spectrum, using 5 triggered stripes
  TH1D *h_Cathode_X_Group0 = new TH1D("h_Cathode_X_Group0","MWPC CathodeX position spectrum Group0",800,-25.03125,24.96875);
  //X spectrum, using 4 triggered stripes
  TH1D *h_Cathode_X_Group1 = new TH1D("h_Cathode_X_Group1","MWPC CathodeX position spectrum Group1",800,-25.03125,24.96875);
  //X spectrum, using 3 triggered stripes
  TH1D *h_Cathode_X_Group2 = new TH1D("h_Cathode_X_Group2","MWPC CathodeX position spectrum Group2",800,-25.03125,24.96875);

  TH2D *h_Cathode_Image_Cal = new TH2D("h_Cathode_Image_Cal","2D image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Anode_Image_Cal = new TH2D("h_Anode_Image_Cal","2D Anode image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_ACCorrelation_Cal = new TH2D("h_ACCorrelation_Cal","MWPC anode cathode x correlation",200,-25,25,200,-25,25);
  TH1D *h_Anode_Cal = new TH1D("h_Anode_Cal","MWPC Anode position spectrum",800,-25.0625,24.96875);
  TH1D *h_Cathode_X_Cal = new TH1D("h_Cathode_X_Cal","MWPC CathodeX position spectrum",800,-25.0625,24.96875);
  TH1D *h_Cathode_Y_Cal = new TH1D("h_Cathode_Y_Cal","MWPC CathodeY position spectrum",800,-25.0625,24.96875);
  TH1D *h_Cathode_X_cond3_Cal = new TH1D("h_Cathode_X_cond3_Cal","MWPC CathodeX position spectrum cond3",800,-25.0625,24.96875);
  TH1D *h_Cathode_X_cond4_Cal = new TH1D("h_Cathode_X_cond4_Cal","MWPC CathodeX position spectrum cond4",800,-25.0625,24.96875);
  TH1D *h_Cathode_X_cond5_Cal = new TH1D("h_Cathode_X_cond5_Cal","MWPC CathodeX position spectrum cond5",800,-25.0625,24.96875);
  TH1D *h_MWPCTot_Cal  = new TH1D("h_MWPCTot_Cal","MWPC Anode_tot calibrated spectrum",1024,0,18);
  TH1D *h_MWPCTot_Cal2  = new TH1D("h_MWPCTot_Cal2","MWPC Anode_tot calibrated spectrum",1024,0,18);
  TH1D *h_Charge_X_Cal = new TH1D("h_Charge_X_Cal","Total Charge on MWPC X calibrated",1024,0,18);
  TH1D *h_Charge_Y_Cal = new TH1D("h_Charge_Y_Cal","Total Charge on MWPC Y calibrated",1024,0,18);
  
  ostringstream convert;
  convert << setfill('0') << setw(4)<<FileID;
  TString fName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + FilePrefix + string("_") + convert.str();
  fName += ".root";
  TFile *f = new TFile (fName.Data(), "read");

  TTree *Tree_MWPC = (TTree*)f->Get("Tree_MWPC");       //Exclude Scint event

  Tree_MWPC->SetBranchAddress("Event_No",&n);
  Tree_MWPC->SetBranchAddress("GroupTime",&GroupTime);
  Tree_MWPC->SetBranchAddress("AMWPC_anode1",&AMWPC_anode1);
  Tree_MWPC->SetBranchAddress("AMWPC_anode2",&AMWPC_anode2);
  Tree_MWPC->SetBranchAddress("TMWPC_rel",&TMWPC_rel);
  Tree_MWPC->SetBranchAddress("MWPC_X",&MWPC_X);
  Tree_MWPC->SetBranchAddress("TMWPC_X",&TMWPC_X);
  Tree_MWPC->SetBranchAddress("MWPC_Y",&MWPC_Y);
  Tree_MWPC->SetBranchAddress("TMWPC_Y",&TMWPC_Y);
  Tree_MWPC->SetBranchAddress("TriggerMap",&TriggerMap);

  double StartTime = 0;
  int N_MWPC = Tree_MWPC->GetEntries();

  for(int i=0;i<N_MWPC;i++){
    //Read Tree
    Tree_MWPC->GetEntry(i);
    if (i==0)StartTime = GroupTime;
    if (Source.compare("Bi207")==0 && (TriggerMap&(1<<1))==1)continue;
    //Calibrate cathode amplifer signals
/*    MWPC_X.AX1/=CurrentAnalyzer->AmpCalX[0];
    MWPC_X.AX2/=CurrentAnalyzer->AmpCalX[1];
    MWPC_X.AX3/=CurrentAnalyzer->AmpCalX[2];
    MWPC_X.AX4/=CurrentAnalyzer->AmpCalX[3];
    MWPC_X.AX5/=CurrentAnalyzer->AmpCalX[4];
    MWPC_X.AX6/=CurrentAnalyzer->AmpCalX[5];
    MWPC_Y.AY1/=CurrentAnalyzer->AmpCalY[0];
    MWPC_Y.AY2/=CurrentAnalyzer->AmpCalY[1];
    MWPC_Y.AY3/=CurrentAnalyzer->AmpCalY[2];
    MWPC_Y.AY4/=CurrentAnalyzer->AmpCalY[3];
    MWPC_Y.AY5/=CurrentAnalyzer->AmpCalY[4];
    MWPC_Y.AY6/=CurrentAnalyzer->AmpCalY[5];*/
    //Fill each X cathode signal
    if (MWPC_X.AX1>0)h_Charge_X1->Fill(MWPC_X.AX1); 
    if (MWPC_X.AX2>0)h_Charge_X2->Fill(MWPC_X.AX2); 
    if (MWPC_X.AX3>0)h_Charge_X3->Fill(MWPC_X.AX3); 
    if (MWPC_X.AX4>0)h_Charge_X4->Fill(MWPC_X.AX4); 
    if (MWPC_X.AX5>0)h_Charge_X5->Fill(MWPC_X.AX5); 
    if (MWPC_X.AX6>0)h_Charge_X6->Fill(MWPC_X.AX6); 
    //Sort signals according to their amplitudes
    //X
    double *ptr;
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Xsig[k])<*(Xsig[k+1])){
	  ptr = Xsig[k];
	  Xsig[k]=Xsig[k+1];
	  Xsig[k+1]=ptr;
	}
      }
    }
    //Y
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Ysig[k])<*(Ysig[k+1])){
	  ptr = Ysig[k];
	  Ysig[k]=Ysig[k+1];
	  Ysig[k+1]=ptr;
	}
      }
    }

    TriggerMultiplicity = 6;
    //Set Smallest ones to zero, according to trigger multiplicity
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    //Count # of fired wires
    int NFireX=0;
    int NFireY=0;
    //Count Triggered Wires
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    //Calibrate relative amplitude (for future)
    AMWPC_anode2 = AMWPC_anode2*CurrentAnalyzer->AmpCalA[0]/CurrentAnalyzer->AmpCalA[1];
    h_A1A2->Fill(AMWPC_anode1,AMWPC_anode2);
    if ((AMWPC_anode1+AMWPC_anode2>1000)){
      //MWPC Anode and Image
      h_MWPC1->Fill(AMWPC_anode1);
      h_MWPC2->Fill(AMWPC_anode2);
      h_MWPC_tot->Fill((AMWPC_anode1+AMWPC_anode2)*0.001);
      h_MWPC_Corr->Fill(AMWPC_anode1,AMWPC_anode2);
      //Process MWPC Data, signal to position and charge
      ProcessMWPCData(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);

      h_Anode_Pos->Fill(AnodePos);
      h_Cathode_Image->Fill(MWPC_PosX,MWPC_PosY);
      if(MWPC_ChargeY>0)h_ACCorrelation->Fill(MWPC_PosY,AnodePos);
      if (MWPC_ChargeX>0)h_Anode_Image->Fill(MWPC_PosX,AnodePos);
      h_Charge_X->Fill(MWPC_ChargeX*0.001);
      h_Charge_Y->Fill(MWPC_ChargeY*0.001);
      h_Charge_C->Fill((MWPC_ChargeX+MWPC_ChargeY)*0.001);
      h_Charge_XY->Fill(MWPC_ChargeX*0.001,MWPC_ChargeY*0.001);
      h_Charge_AC->Fill((MWPC_ChargeX+MWPC_ChargeY)*0.001,(AMWPC_anode1+AMWPC_anode2)*0.001);

      h_FiredWireX->Fill(NFireX);
      h_FiredWireY->Fill(NFireY);

      h_Cathode_X->Fill(MWPC_PosX);
      h_Cathode_Y->Fill(MWPC_PosY);

      h_PosY_Charge->Fill(AnodePos,(AMWPC_anode1+AMWPC_anode2)*0.001);
//      h_PosY_Charge->Fill(MWPC_PosY,MWPC_ChargeY);
      h_PosX_Charge->Fill(MWPC_PosX,(AMWPC_anode1+AMWPC_anode2)*0.001);
      //Conditioned
      if (NFireX==2){
	h_Cathode_X_cond2->Fill(MWPC_PosX);
      }
      if (NFireY==2){
	h_Cathode_Y_cond2->Fill(MWPC_PosY);
      }
      if (NFireX==3){
	h_Cathode_X_cond3->Fill(MWPC_PosX);
      }
      if (NFireY==3){
	h_Cathode_Y_cond3->Fill(MWPC_PosY);
      }
      if (NFireX==4){
	h_Cathode_X_cond4->Fill(MWPC_PosX);
      }
      if (NFireY==4){
	h_Cathode_Y_cond4->Fill(MWPC_PosY);
      }
      if (NFireX==5){
	h_Cathode_X_cond5->Fill(MWPC_PosX);
      }
      if (NFireY==5){
	h_Cathode_Y_cond5->Fill(MWPC_PosY);
      }
      if (NFireX==6){
	h_Cathode_X_cond6->Fill(MWPC_PosX);
      }
      if (NFireY==6){
	h_Cathode_Y_cond6->Fill(MWPC_PosY);
      }
      if (AnodePos>-16 && AnodePos<16 && MWPC_PosX>-15 && MWPC_PosX<15){
	h_MWPC_Cond->Fill(AMWPC_anode1+AMWPC_anode2);
	h_Anode_Image_cond->Fill(MWPC_PosX,AnodePos);
      }
      if (MWPC_PosY>-16 && MWPC_PosY<16 && MWPC_PosX>-15 && MWPC_PosX<15){
	h_Cathode_Image_cond->Fill(MWPC_PosX,MWPC_PosY);
      }
    }
    //Create the 3 groups for calibration
    TriggerMultiplicity = 5;
    //Set Smallest ones to zero, according to trigger multiplicity
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    NFireX=0;
    NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>1000)){
      ProcessMWPCData(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
      if (NFireX==5){
	h_Cathode_X_Group0->Fill(MWPC_PosX);
      }
    }
    TriggerMultiplicity = 4;
    //Set Smallest ones to zero, according to trigger multiplicity
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    NFireX=0;
    NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>1000)){
      ProcessMWPCData(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
      if (NFireX==4){
	h_Cathode_X_Group1->Fill(MWPC_PosX);
      }
    }
    TriggerMultiplicity = 3;
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    NFireX=0;
    NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>1000)){
      ProcessMWPCData(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
      if (NFireX==3){
	h_Cathode_X_Group2->Fill(MWPC_PosX);
      }
    }
  }//End file loop
  h_FiredWireX->Scale(100.0/N_MWPC);
  h_FiredWireY->Scale(100.0/N_MWPC);
  cout << N_MWPC << " events"<<endl;

  //Calibration
  ofstream calfileout;
  sprintf(filename,"%s/MWPCPosCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  calfileout.open(filename,ios::out);
  
  /*
  //CathodeX
  //Find Peaks, maxima
  int Index = 0;
  double Max = 0;
  for (int i=0;i<9;i++){
    if (i==4){
      calfileout<<PeaksX0[i]<<endl;
      continue;
    }
    PeaksX0[i] = PrimitivePeaksX0[i];
    Index = h_Cathode_X_Group0->FindBin(PeaksX0[i]);
    Max = h_Cathode_X_Group0->GetBinContent(Index);
    for (int j=Index-2;j<=Index+2;j++){
      if (h_Cathode_X_Group0->GetBinContent(j)>Max){
	Max = h_Cathode_X_Group0->GetBinContent(j);
	PeaksX0[i] = h_Cathode_X_Group0->GetBinCenter(j);
      }
    }
    calfileout<<PeaksX0[i]<<endl;
  }
  for (int i=0;i<15;i++){
    PeaksX1[i] = PrimitivePeaksX1[i];
    Index = h_Cathode_X_Group1->FindBin(PeaksX1[i]);
    Max = h_Cathode_X_Group1->GetBinContent(Index);
    for (int j=Index-2;j<=Index+2;j++){
      if (h_Cathode_X_Group1->GetBinContent(j)>Max){
	Max = h_Cathode_X_Group1->GetBinContent(j);
	PeaksX1[i] = h_Cathode_X_Group1->GetBinCenter(j);
      }
    }
    calfileout<<PeaksX1[i]<<endl;
//    cout << PrimitivePeaksX1[i]<<" "<<PeaksX1[i]<<endl;
  }
  for (int i=0;i<20;i++){
    PeaksX2[i] = PrimitivePeaksX2[i];
    Index = h_Cathode_X_Group2->FindBin(PeaksX2[i]);
    Max = h_Cathode_X_Group2->GetBinContent(Index);
    for (int j=Index-2;j<=Index+2;j++){
      if (h_Cathode_X_Group2->GetBinContent(j)>Max){
	Max = h_Cathode_X_Group2->GetBinContent(j);
	PeaksX2[i] = h_Cathode_X_Group2->GetBinCenter(j);
      }
    }
    calfileout<<PeaksX2[i]<<endl;
  }
  //Temp
  //How to deal with double peaks?
  for (int i=0;i<18;i++){
    calfileout<<PrimitivePeaksY[i]<<endl;
  }
  //For anode
  for (int i=0;i<18;i++){
    PeaksA[i] = PrimitivePeaksA[i];
    Index = h_Anode_Pos->FindBin(PeaksA[i]);
    Max = h_Anode_Pos->GetBinContent(Index);
    for (int j=Index-2;j<=Index+2;j++){
      if (h_Anode_Pos->GetBinContent(j)>Max){
	Max = h_Anode_Pos->GetBinContent(j);
	PeaksA[i] = h_Anode_Pos->GetBinCenter(j);
      }
    }
    calfileout<<PeaksA[i]<<endl;
  }
  */
  //Need to develope a better way to find peaks automatically
  //For now, directly copy the primitve peaks determined by hand and output
  //CathodeX
  for (int i=0;i<9;i++){
    PeaksX0[i]=PrimitivePeaksX0[i];
    calfileout<<ExactPos0[i]<<" "<<PeaksX0[i]<<endl;
  }
  for (int i=0;i<15;i++){
    PeaksX1[i]=PrimitivePeaksX1[i];
    calfileout<<ExactPos1[i]<<" "<<PeaksX1[i]<<endl;
  }
  for (int i=0;i<20;i++){
    PeaksX2[i]=PrimitivePeaksX2[i];
    calfileout<<ExactPos2[i]<<" "<<PeaksX2[i]<<endl;
  }
  //For anode
  for (int i=0;i<18;i++){
    PeaksA[i]=PrimitivePeaksA[i];
    calfileout<<ExactPosA[i]<<" "<<PeaksA[i]<<endl;
  }

  calfileout.close();

  //Set Points
  for (int i=0;i<18;i++){
    AGraph->SetPoint(i,PeaksA[i],-17.0+2.0*i);
  }
  for (int i=0;i<9;i++){
    XGraph0->SetPoint(i,PeaksX0[i],-8.0+2.0*i);
  }
  for (int i=0;i<5;i++){
    for (int j=0;j<3;j++){
      XGraph1[j]->SetPoint(i,PeaksX1[i+j*5]+8.0*(1-j),-4.0+2.0*i);
    }
  }
  for (int i=0;i<5;i++){
    for (int j=0;j<4;j++){
      XGraph2[j]->SetPoint(i,PeaksX2[i+j*5]+8.0*(1-j)+4.0,-4.0+2.0*i);
    }
  }
  //Fit to pol[3]
  AGraph->Fit("pol3");
  fitFuncA = AGraph->GetFunction("pol3");
  for (int j=0;j<4;j++){
    FitParametersA[j] = fitFuncA->GetParameter(j);
  }
  XGraph0->Fit("pol3");
  fitFuncX0 = XGraph0->GetFunction("pol3");
  for (int j=0;j<4;j++){
    FitParametersX0[j] = fitFuncX0->GetParameter(j);
  }
  for (int i=0;i<3;i++){
    XGraph1[i]->Fit("pol3");
    fitFuncX1[i] = XGraph1[i]->GetFunction("pol3");
    for (int j=0;j<4;j++){
      FitParametersX1[i][j] = fitFuncX1[i]->GetParameter(j);
    }
  }
  for (int i=0;i<4;i++){
    XGraph2[i]->Fit("pol3");
    fitFuncX2[i] = XGraph2[i]->GetFunction("pol3");
    for (int j=0;j<4;j++){
      FitParametersX2[i][j] = fitFuncX2[i]->GetParameter(j);
    }
  }
  //Calibration Complete
  //Start testing calibration

  /*******************************Gain Calibration*************************************************/
  //This can be done with the testing
  TH1* CellSpec[17][18];
  double GainMap[17][18];
  string HistName;
  string HistTitle;
  for (int i=0;i<17;i++){
    for (int j=0;j<18;j++){
      ostringstream double2str;
      double2str << setfill('0') << setw(2) <<i<<"_"<< setw(2) <<j;
      HistName="Cell_" + double2str.str();
      HistTitle="Spectrum for cell " + double2str.str();
      CellSpec[i][j]=new TH1D(HistName.c_str(),HistTitle.c_str(),512,0,163840);
      GainMap[i][j] = 0.0;
    }
  }
  vector<double> Gain;
  vector<double> GainX;
  vector<double> GainY;
  vector<double> GainChargeX;
  vector<double> GainChargeY;
  /*******************************Gain Calibration*************************************************/
  TH1* CathodeXSig = new TH1D("CathodeXSig","CathodeX Signal distribution at X=-2mm",6,-24,24);

  double TempMWPC_X[6];
  double TempMWPC_Y[6];
  for(int i=0;i<N_MWPC;i++){
    //Read Tree
    Tree_MWPC->GetEntry(i);
    if (i==0)StartTime = GroupTime;
    //Save temp cathode data
    TempMWPC_X[0] = MWPC_X.AX1;
    TempMWPC_X[1] = MWPC_X.AX2;
    TempMWPC_X[2] = MWPC_X.AX3;
    TempMWPC_X[3] = MWPC_X.AX4;
    TempMWPC_X[4] = MWPC_X.AX5;
    TempMWPC_X[5] = MWPC_X.AX6;
    TempMWPC_Y[0] = MWPC_Y.AY1;
    TempMWPC_Y[1] = MWPC_Y.AY2;
    TempMWPC_Y[2] = MWPC_Y.AY3;
    TempMWPC_Y[3] = MWPC_Y.AY4;
    TempMWPC_Y[4] = MWPC_Y.AY5;
    TempMWPC_Y[5] = MWPC_Y.AY6;
    //Sort signals according to their amplitudes
    //X
    double *ptr;
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Xsig[k])<*(Xsig[k+1])){
	  ptr = Xsig[k];
	  Xsig[k]=Xsig[k+1];
	  Xsig[k+1]=ptr;
	}
      }
    }
    //Y
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Ysig[k])<*(Ysig[k+1])){
	  ptr = Ysig[k];
	  Ysig[k]=Ysig[k+1];
	  Ysig[k+1]=ptr;
	}
      }
    }

    double MWPC_ChargeX6 = MWPC_X.AX1+MWPC_X.AX2+MWPC_X.AX3+MWPC_X.AX4+MWPC_X.AX5+MWPC_X.AX6;
    double MWPC_ChargeY6 = MWPC_Y.AY1+MWPC_Y.AY2+MWPC_Y.AY3+MWPC_Y.AY4+MWPC_Y.AY5+MWPC_Y.AY6;

    AMWPC_anode2 = AMWPC_anode2*CurrentAnalyzer->AmpCalA[0]/CurrentAnalyzer->AmpCalA[1];
    int NFireX=0;
    int NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>1000)){
      //Calibrations
      if (CalibrationScheme.compare("Pol")==0){
	ConstructCorrectedMWPCPosPol(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,Xsig,Ysig,FitParametersA,FitParametersX0,FitParametersX1,FitParametersX2,KeepLessTrigger,CathodeThreshold);
      }else if(CalibrationScheme.compare("Micro")==0){
	ConstructCorrectedMWPCPosMicro(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,Xsig,Ysig,PeaksX0,PeaksX1,PeaksX2,PeaksA,ExactPos0,ExactPos1,ExactPos2,ExactPosA,KeepLessTrigger,CathodeThreshold);
      }else{
	cout<<"Uknown scheme "<<CalibrationScheme<<endl;
	return -1;
      }
            
      if (NFireX>=3){
	h_Cathode_Image_Cal->Fill(MWPC_PosX,MWPC_PosY);
	if(MWPC_ChargeY>0)h_ACCorrelation_Cal->Fill(MWPC_PosY,AnodePos);
	if (MWPC_ChargeX>0)h_Anode_Image_Cal->Fill(MWPC_PosX,AnodePos);
	h_Anode_Cal->Fill(AnodePos);
	h_Cathode_X_Cal->Fill(MWPC_PosX);
	h_Cathode_Y_Cal->Fill(MWPC_PosY);
	/*******************************Gain Calibration*************************************************/
	Gain.push_back(AMWPC_anode1+AMWPC_anode2);
	GainChargeX.push_back(MWPC_ChargeX6);
	GainChargeY.push_back(MWPC_ChargeY6);
	GainX.push_back(MWPC_PosX);
	GainY.push_back(AnodePos);
	int ii = int((MWPC_PosX+17.0)/2.0);
	int jj = int((AnodePos+18.0)/2.0);
	if (ii>=0 && ii<17 && jj>=0 && jj<18){
	  CellSpec[ii][jj]->Fill(AMWPC_anode1+AMWPC_anode2);
	}
      }

      //Conditioned
      if (NFireX==3){
	h_Cathode_X_cond3_Cal->Fill(MWPC_PosX);
      }
      if (NFireX==4){
	h_Cathode_X_cond4_Cal->Fill(MWPC_PosX);
      }
      if (NFireX==5){
	h_Cathode_X_cond5_Cal->Fill(MWPC_PosX);
      }

      //Cathode signal calibration
      //Fill Cathode Signal Map
      //Only calibrate the central region
      if (MWPC_PosX>-16.0 && MWPC_PosX<16.0 && AnodePos>-17.0 && AnodePos<17.0){
	int XIndex = int((MWPC_PosX+16.0625)/0.125);
	int YIndex = int((AnodePos+18.0)/2.0);
	for (int kk=0;kk<6;kk++){
	  CathodeSignalMap[XIndex][YIndex][kk]+=TempMWPC_X[kk];
	  CathodeSignalMap[XIndex][YIndex][kk+6]+=TempMWPC_Y[kk];
	}
      }
    }
  }

  //Save Cathode Signal Map to file
  //Normalize
  double CathodeSignalSumX[257][18];
  for (int i=0;i<257;i++){
    for (int j=0;j<18;j++){
      CathodeSignalSumX[i][j]=0.0;
    }
  }
  double CathodeSignalSumY[257][18];
  for (int i=0;i<257;i++){
    for (int j=0;j<18;j++){
      CathodeSignalSumY[i][j]=0.0;
    }
  }
  for (int i=0;i<257;i++){
    for (int j=0;j<18;j++){
      for (int k=0;k<6;k++){
	CathodeSignalSumX[i][j]+=CathodeSignalMap[i][j][k];
      }
      for (int k=6;k<12;k++){
	CathodeSignalSumY[i][j]+=CathodeSignalMap[i][j][k];
      }
    }
  }
  //Fill inspection histogram
  for (int ii=0;ii<6;ii++){
    CathodeXSig->Fill(-20+ii*8,CathodeSignalMap[119][8][ii]/CathodeSignalSumX[119][8]);
  }
  ofstream CathodeSigCalFileOut;
  sprintf(filename,"%s/MWPCCathodeSignalMap%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  CathodeSigCalFileOut.open(filename,ios::out);
  for (int i=0;i<257;i++){
    for (int j=0;j<18;j++){
      CathodeSigCalFileOut<<-16.0+i*0.125<<" "<<-17.0+j*2.0<<" ";
      for (int k=0;k<6;k++){
	if (CathodeSignalSumX[i][j]!=0){
	  CathodeSigCalFileOut<< CathodeSignalMap[i][j][k]/CathodeSignalSumX[i][j]<<" ";
	}else{
	  CathodeSigCalFileOut<< 0 <<" ";
	}
      }
      for (int k=6;k<12;k++){
	if (CathodeSignalSumY[i][j]!=0){
	  CathodeSigCalFileOut<< CathodeSignalMap[i][j][k]/CathodeSignalSumY[i][j]<<" ";
	}else{
	  CathodeSigCalFileOut<< 0 <<" ";
	}
      }
      CathodeSigCalFileOut<<endl;
    }
  }
  CathodeSigCalFileOut.close();


  //Compare to standard positions
  TGraphErrors* DifferenceX = new TGraphErrors();
  DifferenceX->SetName("DifferenceX");
  DifferenceX->SetTitle("DifferenceX");
  for (int i=0;i<17;i++){
    h_Cathode_X_Cal->Fit("gaus","","",-16.0+i*2.0-0.125,-16.0+i*2.0+0.125);
    TF1* fitFuncGaus = h_Cathode_X_Cal->GetFunction("gaus");
    DifferenceX->SetPoint(i,-16.0+i*2.0,fitFuncGaus->GetParameter(1)-(-16.0+i*2.0));
    DifferenceX->SetPointError(i,0,fitFuncGaus->GetParameter(2));
  }

  TGraphErrors* DifferenceA = new TGraphErrors();
  DifferenceA->SetName("DifferenceA");
  DifferenceA->SetTitle("DifferenceA");
  for (int i=0;i<18;i++){
    h_Anode_Cal->Fit("gaus","","",-17.0+i*2.0-0.125,-17.0+i*2.0+0.125);
    TF1* fitFuncGaus = h_Anode_Cal->GetFunction("gaus");
    DifferenceA->SetPoint(i,-17.0+i*2.0,fitFuncGaus->GetParameter(1)-(-17.0+i*2.0));
    DifferenceA->SetPointError(i,0,fitFuncGaus->GetParameter(2));
  }
/*  for (int i=0;i<18;i++){
    double Ay = PrimitivePeaksA[i];
    DifferenceA->SetPoint(i,-17.0+i*2.0,FitParametersA[0]+FitParametersA[1]*Ay+FitParametersA[2]*pow(Ay,2.0)+FitParametersA[3]*pow(Ay,3.0)-(-17.0+i*2.0));
    DifferenceA->SetPointError(i,0,0);
  }*/



  /*******************************Gain Calibration*************************************************/
  TGraph2D *GainMapGraph = new TGraph2D();
  TGraph2D *GainMapChi2 = new TGraph2D();
  TCanvas* CellCanvas = new TCanvas("GainMap","GainMap",0,0,15000,16000);
  CellCanvas->Divide(17,18);
  //Fit [8][8] as the reference point
  CellCanvas->cd(8*17+8+1);
  CellSpec[8][8]->Rebin(8);
  int MaxBin = CellSpec[8][8]->GetMaximumBin();
  double Peak = CellSpec[8][8]->GetBinCenter(MaxBin);
  CellSpec[8][8]->Fit("gaus","","",Peak*0.8,Peak*1.2);
  TF1 * CellFitFunc = CellSpec[8][8]->GetFunction("gaus");
  double ReferenceGain = CellFitFunc->GetParameter(1);
  GainMap[8][8] = 1.0; 
  CellSpec[8][8]->Draw();
  for (int i=0;i<17;i++){
    for (int j=0;j<18;j++){
      if (i==8 && j==8){
	GainMapGraph->SetPoint(j*17+i,-16+i*2.0,-17+j*2.0,GainMap[i][j]);
	GainMapChi2->SetPoint(j*17+i,-16+i*2.0,-17+j*2.0,1.0);
	continue;
      }
      CellCanvas->cd(j*17+i+1);
      CellSpec[i][j]->Rebin(8);
      MaxBin = CellSpec[i][j]->GetMaximumBin();
      Peak = CellSpec[i][j]->GetBinCenter(MaxBin);
      TFitResultPtr r =  CellSpec[i][j]->Fit("gaus","","",Peak*0.8,Peak*1.2);
      CellFitFunc = CellSpec[i][j]->GetFunction("gaus");
      double Chi2 = CellFitFunc->GetChisquare();
      double NDF = CellFitFunc->GetNDF();
      if (NDF==0){
	Chi2=1;
      }else{
	Chi2/=double(NDF);
      }
      cout <<i<<" "<<j<<" Chi2 = "<<Chi2 <<endl;;
      //If failed, try to do it again, search for next maximum
      if (int(r)!=0){
	//Set the current maximum to zero
	for (int k=-2;k<=2;k++){
	  CellSpec[i][j]->SetBinContent(MaxBin+k,0.0);
	}
	MaxBin = CellSpec[i][j]->GetMaximumBin();
	Peak = CellSpec[i][j]->GetBinCenter(MaxBin);
	TFitResultPtr r =  CellSpec[i][j]->Fit("gaus","","",Peak*0.8,Peak*1.2);
	CellFitFunc = CellSpec[i][j]->GetFunction("gaus");
	Chi2 = CellFitFunc->GetChisquare();
	NDF = CellFitFunc->GetNDF();
	if (NDF==0){
	  Chi2=1;
	}else{
	  Chi2/=double(NDF);
	}
      }
      GainMap[i][j] = CellFitFunc->GetParameter(1)/ReferenceGain;
      GainMapGraph->SetPoint(j*17+i,-16+i*2.0,-17+j*2.0,GainMap[i][j]);
      GainMapChi2->SetPoint(j*17+i,-16+i*2.0,-17+j*2.0,Chi2);
      CellSpec[i][j]->Draw();
    }
  }
  string SpecFile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Spectra/") + FilePrefix + string("_") + convert.str() + string("_GainMap.pdf(");
  CellCanvas->SaveAs(SpecFile.c_str());
  delete CellCanvas;
  TCanvas* CellCanvas1 = new TCanvas("GainMap1","GainMap1",0,0,15000,16000);
  GainMapGraph->SetName("GainMap");
  GainMapGraph->SetTitle("GainMap");
  GainMapGraph->GetXaxis()->SetTitle("X");
  GainMapGraph->GetYaxis()->SetTitle("Y");
  GainMapGraph->GetZaxis()->SetTitle("Relative Gain");
  GainMapGraph->Draw("surf1");
  SpecFile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Spectra/") + FilePrefix + string("_") + convert.str() + string("_GainMap.pdf");
  CellCanvas1->SaveAs(SpecFile.c_str());
  GainMapChi2->SetName("Chi2");
  GainMapChi2->SetTitle("Chi2");
  GainMapChi2->Draw("surf1");
  SpecFile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Spectra/") + FilePrefix + string("_") + convert.str() + string("_GainMap.pdf)");
  CellCanvas1->SaveAs(SpecFile.c_str());
  delete CellCanvas1;
  //Load Data again, correct the gain non-uniform
  int NCal = Gain.size();
  for (int i=0;i<NCal;i++){
    int ii = int((GainX[i]+17.0)/2.0);
    int jj = int((GainY[i]+18.0)/2.0);
    if (ii>=0 && ii<17 && jj>=0 && jj<18){
      double GainFactor = GainMap[ii][jj];
      //Apply the fiducial cut, 16 mm as standard
      if (pow(GainX[i],2.0)+pow(GainY[i],2.0)<pow(16,2.0)){
	h_MWPCTot_Cal->Fill(Gain[i]/GainFactor/CurrentAnalyzer->MWPCRawCalA);
	h_Charge_X_Cal->Fill(GainChargeX[i]/GainFactor/CurrentAnalyzer->MWPCRawCalC);
	h_Charge_Y_Cal->Fill(GainChargeY[i]/GainFactor/CurrentAnalyzer->MWPCRawCalC);
      }
    }
  } 
  //Write to gain Calibration file
  ofstream GainCalFileOut;
  sprintf(filename,"%s/MWPCGainMap%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  GainCalFileOut.open(filename,ios::out);
  for (int i=0;i<17;i++){
    for (int j=0;j<18;j++){
      GainCalFileOut<<-16.0+i*2.0<<" "<<-17.0+j*2.0<<" "<< GainMap[i][j]<<endl;
    }
  }
  GainCalFileOut.close();
  /********************************Calibrate the main peak*****************************************/
  if (Source.compare("Fe55")==0){
    int HistMax = h_MWPCTot_Cal->GetMaximumBin();
    double HistMaxE = h_MWPCTot_Cal->GetBinCenter(HistMax);
    h_MWPCTot_Cal->Fit("gaus","","",HistMaxE*0.9,HistMaxE*1.1);
    double APeakPos =h_MWPCTot_Cal->GetFunction("gaus")->GetParameter(1);
    //Fit the Ar escape peak
    h_MWPCTot_Cal->Fit("gaus","","",HistMaxE*0.54*0.9,HistMaxE*0.54*1.1);
    double APeakPos2 = h_MWPCTot_Cal->GetFunction("gaus")->GetParameter(1);
    HistMax = h_Charge_X_Cal->GetMaximumBin();
    HistMaxE = h_Charge_X_Cal->GetBinCenter(HistMax);
    h_Charge_X_Cal->Fit("gaus","","",HistMaxE*0.9,HistMaxE*1.1);
    double CPeakPos =h_Charge_X_Cal->GetFunction("gaus")->GetParameter(1);
    //Try linear fit
    double Scale = 2.9577/(APeakPos-APeakPos2);
    double Shift = (APeakPos*2.9373-APeakPos2*5.895)/(APeakPos-APeakPos2);
    ofstream ECalFileOut;
    sprintf(filename,"%s/MWPCECalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
    ECalFileOut.open(filename,ios::out);
    cout <<"APeakPos1= "<<APeakPos<<endl;
    cout <<"APeakPos2= "<<APeakPos2<<endl;
    cout <<"CPeakPos= "<<CPeakPos<<endl;
    ECalFileOut<<5.895/APeakPos<<" "<<5.895/CPeakPos<<endl;
    ECalFileOut<<Scale<<" "<<Shift<<endl;	//New calibration method, still keep the old one for the moment
    ECalFileOut.close();
    //Load Data again,fine tune the gain 
    NCal = Gain.size();
    for (int i=0;i<NCal;i++){
      int ii = int((GainX[i]+17.0)/2.0);
      int jj = int((GainY[i]+18.0)/2.0);
      if (ii>=0 && ii<17 && jj>=0 && jj<18){
	double GainFactor = GainMap[ii][jj];
	//Apply the fiducial cut, 16 mm as standard
	if (pow(GainX[i],2.0)+pow(GainY[i],2.0)<pow(16,2.0)){
	  double aux = Gain[i]/GainFactor/CurrentAnalyzer->MWPCRawCalA;
	  h_MWPCTot_Cal2->Fill(Scale*aux+Shift);
	}
      }
    } 
    HistMax = h_MWPCTot_Cal2->GetMaximumBin();
    HistMaxE = h_MWPCTot_Cal2->GetBinCenter(HistMax);
    double MaxVal = h_MWPCTot_Cal2->GetMaximum();
    TF1* fitfunc = new TF1("TriGaus","[0]*exp(-pow(x-[1],2.0)/pow([2],2.0)/2)+[3]*exp(-pow(x-[4],2.0)/pow([5],2.0)/2)+[3]*0.1168*exp(-pow(x-[4]*1.102,2.0)/pow([6],2.0)/2)",0,12);
    fitfunc->SetParameters(MaxVal*0.135,HistMaxE*0.5,1.0,MaxVal,HistMaxE,HistMaxE*0.13,HistMaxE*0.13);
    h_MWPCTot_Cal2->Fit(fitfunc);
  }else if (Source.compare("Bi207")==0){
    int HistMax = h_MWPCTot_Cal->GetMaximumBin();
    double HistMaxE = h_MWPCTot_Cal->GetBinCenter(HistMax);
    h_MWPCTot_Cal->Fit("gaus","","",HistMaxE*0.9,HistMaxE*1.1);
    double APeakPos =h_MWPCTot_Cal->GetFunction("gaus")->GetParameter(1);
    HistMax = h_Charge_X_Cal->GetMaximumBin();
    HistMaxE = h_Charge_X_Cal->GetBinCenter(HistMax);
    h_Charge_X_Cal->Fit("gaus","","",HistMaxE*0.9,HistMaxE*1.1);
    double CPeakPos =h_Charge_X_Cal->GetFunction("gaus")->GetParameter(1);

    double Scale = 9.5/APeakPos;
    //Load Data again,fine tune the gain 
    NCal = Gain.size();
    for (int i=0;i<NCal;i++){
      int ii = int((GainX[i]+17.0)/2.0);
      int jj = int((GainY[i]+18.0)/2.0);
      if (ii>=0 && ii<17 && jj>=0 && jj<18){
	double GainFactor = GainMap[ii][jj];
	//Apply the fiducial cut, 16 mm as standard
	if (pow(GainX[i],2.0)+pow(GainY[i],2.0)<pow(16,2.0)){
	  double aux = Gain[i]/GainFactor/CurrentAnalyzer->MWPCRawCalA;
	  h_MWPCTot_Cal2->Fill(Scale*aux);
	}
      }
    } 
    HistMax = h_MWPCTot_Cal2->GetMaximumBin();
    HistMaxE = h_MWPCTot_Cal2->GetBinCenter(HistMax);
    double MaxVal = h_MWPCTot_Cal2->GetMaximum();
    h_MWPCTot_Cal2->Fit("gaus","","",HistMaxE*0.9,HistMaxE*1.1);
  }
  /************************************************************************************************/
  //Write to root file
  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_MWPCCal.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");
  h_MWPC1->Write();
  h_MWPC2->Write();
  h_MWPC_tot->Write();
  h_Charge_X->Write();
  h_Charge_Y->Write();
  h_Charge_C->Write();
  h_Charge_XY->Write();
  h_Charge_AC->Write();
  h_MWPC_Cond->Write();
  h_Anode_Pos->Write();
  h_Cathode_Image->Write();
  h_ACCorrelation->Write();
  h_Anode_Image->Write();
  h_FiredWireX->Write();
  h_FiredWireY->Write();

  h_Charge_X1->Write();
  h_Charge_X2->Write();
  h_Charge_X3->Write();
  h_Charge_X4->Write();
  h_Charge_X5->Write();
  h_Charge_X6->Write();
  h_Cathode_X->Write();
  h_Cathode_Y->Write();
  h_Cathode_X_cond2->Write();
  h_Cathode_Y_cond2->Write();
  h_Cathode_X_cond3->Write();
  h_Cathode_Y_cond3->Write();
  h_Cathode_X_cond4->Write();
  h_Cathode_Y_cond4->Write();
  h_Cathode_X_cond5->Write();
  h_Cathode_Y_cond5->Write();
  h_Cathode_X_cond6->Write();
  h_Cathode_Y_cond6->Write();
  h_Cathode_X_Group0->Write();
  h_Cathode_X_Group1->Write();
  h_Cathode_X_Group2->Write();
  h_Anode_Image_cond->Write();
  h_Cathode_Image_cond->Write();
  h_MWPC_Corr->Write();
  h_A1A2->Write();
  h_PosX_Charge->Write();
  h_PosY_Charge->Write();
  //Calibrations
  h_Cathode_Image_Cal->Write();
  h_ACCorrelation_Cal->Write();
  h_Anode_Image_Cal->Write();
  h_Anode_Cal->Write();
  h_Cathode_X_Cal->Write();
  h_Cathode_Y_Cal->Write();
  h_Cathode_X_cond3_Cal->Write();
  h_Cathode_X_cond4_Cal->Write();
  h_Cathode_X_cond5_Cal->Write();
  h_MWPCTot_Cal->Write();
  h_MWPCTot_Cal2->Write();
  h_Charge_X_Cal->Write();
  h_Charge_Y_Cal->Write();

  //Cathode Signal
  CathodeXSig->Write();

  DifferenceA->Write();
  DifferenceX->Write();
//  CalCathodeXGraph->Write();
//  CalCathodeYGraph->Write();
//  CalAnodeGraph->Write();
  AGraph->Write();
  XGraph0->Write();
  for (int i=0;i<3;i++){
    XGraph1[i]->Write();
//    fitFuncX1[i]->Write();
  }
  for (int i=0;i<4;i++){
    XGraph2[i]->Write();
//    fitFuncX2[i]->Write();
  }
  GainMapGraph->Write();
  GainMapChi2->Write();
  for (int i=0;i<17;i++){
    for (int j=0;j<18;j++){
      CellSpec[i][j]->Write();
    }
  }
  histfile->Close();
  delete GainMapGraph;
  delete GainMapChi2;
  delete AGraph;
  delete XGraph0;
  for (int i=0;i<3;i++){
    delete XGraph1[i];
  }
  for (int i=0;i<4;i++){
    delete XGraph2[i];
  }
  for (int i=0;i<17;i++){
    for (int j=0;j<18;j++){
      delete CellSpec[i][j];
    }
  }
  delete DifferenceA;
  delete DifferenceX;
//  delete[] XGraph1;
//  delete[] XGraph2;
  return 0;
}


/***********************************************************************************/
int Calibration::CalibrateMWPCPreviewSim(int FileID)
{
  CurrentAnalyzer->SetMode("MWPC");
  CurrentAnalyzer->Calibrating = true;
  string spectrumfile;

  int n = 0;  //  n of data read
  double CathodeThreshold = CurrentAnalyzer->SysWParameters["MWPCC_Threshold"][0];

  TFile* tfilein = NULL; //pointer to input root file
  TTree * InTree = NULL; //input tree pointer
  InDataStruct InData;  //input data unit
  InDataStructDouble InDataDouble;      //input data double format

  double& AMWPC_anode1 = InDataDouble.MWPCAnodeSignal1;
  double& AMWPC_anode2 = InDataDouble.MWPCAnodeSignal2;
  DataStruct_MWPC_X& MWPC_X = InDataDouble.MWPC_X;
  DataStruct_MWPC_Y& MWPC_Y = InDataDouble.MWPC_Y;

  double MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY;
  double AnodePos;

  //For sorting Cathode signals
  int TriggerMultiplicity = 6;
  double* Xsig[6];
  double* Ysig[6];
  Xsig[0] = &(MWPC_X.AX1);
  Xsig[1] = &(MWPC_X.AX2);
  Xsig[2] = &(MWPC_X.AX3);
  Xsig[3] = &(MWPC_X.AX4);
  Xsig[4] = &(MWPC_X.AX5);
  Xsig[5] = &(MWPC_X.AX6);
  Ysig[0] = &(MWPC_Y.AY1);
  Ysig[1] = &(MWPC_Y.AY2);
  Ysig[2] = &(MWPC_Y.AY3);
  Ysig[3] = &(MWPC_Y.AY4);
  Ysig[4] = &(MWPC_Y.AY5);
  Ysig[5] = &(MWPC_Y.AY6);
  // root histos
  TH1D *h_MWPC1  = new TH1D("h_MWPC1","MWPC Anode1 spectrum",1024,0,20);
  TH1D *h_MWPC2  = new TH1D("h_MWPC2","MWPC Anode2 spectrum",1024,0,20);
  TH2* h_A1A2 = new TH2D("h_A1A2","A1 A2 correlation",1024,0,20,1024,0,20);
  TH2* h_PosX_Charge = new TH2D("h_PosX_Charge","X-TotalCharge correlation",800,-25.03125,24.96875,400,0,200);
  TH2* h_PosY_Charge = new TH2D("h_PosY_Charge","Y-TotalCharge correlation",800,-25.03125,24.96875,400,0,200);
  TH1D *h_MWPC_tot  = new TH1D("h_MWPC_tot","MWPC Anode_tot spectrum",1024,0,20);
  TH1D *h_MWPC_Cond  = new TH1D("h_MWPC_Cond","MWPC Anode_tot spectrum",1024,0,20);
  TH1 *h_FiredWireX = new TH1D("h_FiredWireX","# of fired stripes X",10,0,10);
  TH1 *h_FiredWireY = new TH1D("h_FiredWireY","# of fired stripes Y",10,0,10);

  TH1D *h_Cathode_X = new TH1D("h_Cathode_X","MWPC CathodeX position spectrum",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y = new TH1D("h_Cathode_Y","MWPC CathodeY position spectrum",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond2 = new TH1D("h_Cathode_X_cond2","MWPC CathodeX position spectrum cond2",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond2 = new TH1D("h_Cathode_Y_cond2","MWPC CathodeY position spectrum cond2",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond3 = new TH1D("h_Cathode_X_cond3","MWPC CathodeX position spectrum cond3",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond3 = new TH1D("h_Cathode_Y_cond3","MWPC CathodeY position spectrum cond3",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond4 = new TH1D("h_Cathode_X_cond4","MWPC CathodeX position spectrum cond4",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond4 = new TH1D("h_Cathode_Y_cond4","MWPC CathodeY position spectrum cond4",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond5 = new TH1D("h_Cathode_X_cond5","MWPC CathodeX position spectrum cond5",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond5 = new TH1D("h_Cathode_Y_cond5","MWPC CathodeY position spectrum cond5",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond6 = new TH1D("h_Cathode_X_cond6","MWPC CathodeX position spectrum cond6",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond6 = new TH1D("h_Cathode_Y_cond6","MWPC CathodeY position spectrum cond6",800,-25.03125,24.96875);
  TH1D *h_Anode_Pos = new TH1D("h_Anode_Pos","MWPC Anode position spectrum",800,-25.03125,24.96875);
  TH2D *h_Cathode_Image = new TH2D("h_Cathode_Image","2D image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Anode_Image = new TH2D("h_Anode_Image","2D Anode image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_MWPC_Corr  = new TH2D("h_MWPC1_2","MWPC Anode1-2 spectrum",1024,0,655360,1024,0,655360);
  TH2D *h_Cathode_Image_cond = new TH2D("h_Cathode_Image_cond","2D image of MWPC, more than 3 trigger",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Anode_Image_cond = new TH2D("h_Anode_Image_cond","2D Anode image of MWPC more than 3 trigger",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Direct_Image = new TH2D("h_Direct_Image","2D Direct image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_ACCorrelation = new TH2D("h_ACCorrelation","MWPC anode cathode x correlation",400,-25,25,400,-25,25);
  TH1D *h_Charge_X = new TH1D("h_Charge_X","Total Charge on MWPC X",1024,0,20);
  TH1D *h_Charge_Y = new TH1D("h_Charge_Y","Total Charge on MWPC Y",1024,0,20);
  TH1D *h_Charge_C = new TH1D("h_Charge_C","MWPC Cathode_tot spectrum",1024,0,20);
  TH2D *h_Charge_XY = new TH2D("h_Charge_XY","Cathode X-Y charge correlation",400,0,20,400,0,20);
  TH2D *h_Charge_AC = new TH2D("h_Charge_AC","Cathode-Anode charge correlation",400,0,20,400,0,20);

  TH1D *h_Charge_X1 = new TH1D("h_Charge_X1","Total Charge on MWPC X1",1000,0,20);
  TH1D *h_Charge_X2 = new TH1D("h_Charge_X2","Total Charge on MWPC X2",1000,0,20);
  TH1D *h_Charge_X3 = new TH1D("h_Charge_X3","Total Charge on MWPC X3",1000,0,20);
  TH1D *h_Charge_X4 = new TH1D("h_Charge_X4","Total Charge on MWPC X4",1000,0,20);
  TH1D *h_Charge_X5 = new TH1D("h_Charge_X5","Total Charge on MWPC X5",1000,0,20);
  TH1D *h_Charge_X6 = new TH1D("h_Charge_X6","Total Charge on MWPC X6",1000,0,20);

  //Histograms for calibration
  //X spectrum, using 5 triggered stripes
  TH1D *h_Cathode_X_Group0 = new TH1D("h_Cathode_X_Group0","MWPC CathodeX position spectrum Group0",800,-25.03125,24.96875);
  //X spectrum, using 4 triggered stripes
  TH1D *h_Cathode_X_Group1 = new TH1D("h_Cathode_X_Group1","MWPC CathodeX position spectrum Group1",800,-25.03125,24.96875);
  //X spectrum, using 3 triggered stripes
  TH1D *h_Cathode_X_Group2 = new TH1D("h_Cathode_X_Group2","MWPC CathodeX position spectrum Group2",800,-25.03125,24.96875);


  if(CurrentAnalyzer->SetupInputFile(FileID,tfilein,InTree)==-1) return -1;
  CurrentAnalyzer->SetInTreeAddress(InTree, InData);
  int returnVal = 0;  
  //int iEvent = 0;

  int N_MWPC=0;

  while(1){
    returnVal = InTree->GetEntry(N_MWPC);
    if (returnVal==0) break; //eof
    else if(returnVal==-1) return -1;
    CurrentAnalyzer->ConvertDataToDouble(InData,InDataDouble); 
//    InData.ScintEnergy = 0;
//    filein.read((char *)&(InData.ScintEnergy),sizeof(int));
////    filein.read((char *)&(InData.ScintEnergy),3);
//    if(filein.eof())break;
    N_MWPC++;
//    filein.read((char *)&(InData.MWPCEnergy),sizeof(unsigned short));
//    filein.read((char *)(InData.MWPCHitPos),2*sizeof(short));
//    filein.read((char *)&(InData.MWPCAnodeSignal1),sizeof(unsigned short));
//    filein.read((char *)&(InData.MWPCAnodeSignal2),sizeof(unsigned short));
//    filein.read((char *)(InData.MWPCCathodeSignal),12*sizeof(unsigned short));
//    //        filein.read((char *)(InData.MWPCCathodeSignal),12*sizeof(unsigned int));
//    filein.read((char *)&(InData.ExitAngle_Be),sizeof(unsigned short));
//    filein.read((char *)(InData.InitP),3*sizeof(int));
//    filein.read((char *)(InData.DECPos),3*sizeof(short));
//    filein.read((char *)&(InData.Hits),sizeof(uint16_t));
//    filein.read((char *)&(InData.ParticleID),sizeof(uint8_t));

//    //Convert to double
////    InDataDouble.ScintEnergy = double(InData.ScintEnergy)/1000.0;
////    InDataDouble.MWPCEnergy = double(InData.MWPCEnergy)/1000.0;
//    AMWPC_anode1 = double(InData.MWPCAnodeSignal1)/2000.0;
//    AMWPC_anode2 = double(InData.MWPCAnodeSignal2)/2000.0;
//    MWPC_X.AX1 = double(InData.MWPCCathodeSignal[0])/8000.0;
//    MWPC_X.AX2 = double(InData.MWPCCathodeSignal[1])/8000.0;
//    MWPC_X.AX3 = double(InData.MWPCCathodeSignal[2])/8000.0;
//    MWPC_X.AX4 = double(InData.MWPCCathodeSignal[3])/8000.0;
//    MWPC_X.AX5 = double(InData.MWPCCathodeSignal[4])/8000.0;
//    MWPC_X.AX6 = double(InData.MWPCCathodeSignal[5])/8000.0;
//    MWPC_Y.AY1 = double(InData.MWPCCathodeSignal[6])/8000.0;
//    MWPC_Y.AY2 = double(InData.MWPCCathodeSignal[7])/8000.0;
//    MWPC_Y.AY3 = double(InData.MWPCCathodeSignal[8])/8000.0;
//    MWPC_Y.AY4 = double(InData.MWPCCathodeSignal[9])/8000.0;
//    MWPC_Y.AY5 = double(InData.MWPCCathodeSignal[10])/8000.0;
//    MWPC_Y.AY6 = double(InData.MWPCCathodeSignal[11])/8000.0;
////    InDataDouble.ExitAngle_Be = double(InData.ExitAngle_Be)/100.0;
//    for(int i=0;i<2;i++){
//      InDataDouble.MWPCHitPos[i] = double(InData.MWPCHitPos[i])/1000.0;
//    }

    //Fill each X cathode signal
    if (MWPC_X.AX1>0)h_Charge_X1->Fill(MWPC_X.AX1); 
    if (MWPC_X.AX2>0)h_Charge_X2->Fill(MWPC_X.AX2); 
    if (MWPC_X.AX3>0)h_Charge_X3->Fill(MWPC_X.AX3); 
    if (MWPC_X.AX4>0)h_Charge_X4->Fill(MWPC_X.AX4); 
    if (MWPC_X.AX5>0)h_Charge_X5->Fill(MWPC_X.AX5); 
    if (MWPC_X.AX6>0)h_Charge_X6->Fill(MWPC_X.AX6); 
    //Sort signals according to their amplitudes
    //X
    double *ptr;
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Xsig[k])<*(Xsig[k+1])){
	  ptr = Xsig[k];
	  Xsig[k]=Xsig[k+1];
	  Xsig[k+1]=ptr;
	}
      }
    }
    //Y
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Ysig[k])<*(Ysig[k+1])){
	  ptr = Ysig[k];
	  Ysig[k]=Ysig[k+1];
	  Ysig[k+1]=ptr;
	}
      }
    }

    TriggerMultiplicity = 6;
    //Set Smallest ones to zero, according to trigger multiplicity
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    //Count # of fired wires
    int NFireX=0;
    int NFireY=0;
    //Count Triggered Wires
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    //Calibrate relative amplitude (for future)
    AMWPC_anode2 = AMWPC_anode2*CurrentAnalyzer->AmpCalA[0]/CurrentAnalyzer->AmpCalA[1];
    h_A1A2->Fill(AMWPC_anode1,AMWPC_anode2);
    if ((AMWPC_anode1+AMWPC_anode2>0.1)){
      h_Direct_Image->Fill(InDataDouble.MWPCHitPos[0],InDataDouble.MWPCHitPos[1]);
      //MWPC Anode and Image
      h_MWPC1->Fill(AMWPC_anode1);
      h_MWPC2->Fill(AMWPC_anode2);
      h_MWPC_tot->Fill(AMWPC_anode1+AMWPC_anode2);
      h_MWPC_Corr->Fill(AMWPC_anode1,AMWPC_anode2);
      //Process MWPC Data, signal to position and charge
      //It is important that anode2 and anode1 are flipped in simulation!!!!
      ProcessMWPCData(AMWPC_anode2,AMWPC_anode1,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);

      h_Anode_Pos->Fill(AnodePos);
      h_Cathode_Image->Fill(MWPC_PosX,MWPC_PosY);
      if(MWPC_ChargeY>0)h_ACCorrelation->Fill(MWPC_PosY,AnodePos);
      if (MWPC_ChargeX>0)h_Anode_Image->Fill(MWPC_PosX,AnodePos);
      h_Charge_X->Fill(MWPC_ChargeX);
      h_Charge_Y->Fill(MWPC_ChargeY);
      h_Charge_C->Fill(MWPC_ChargeX+MWPC_ChargeY);
      h_Charge_XY->Fill(MWPC_ChargeX,MWPC_ChargeY);
      h_Charge_AC->Fill(MWPC_ChargeX+MWPC_ChargeY,AMWPC_anode1+AMWPC_anode2);

      h_FiredWireX->Fill(NFireX);
      h_FiredWireY->Fill(NFireY);

      h_Cathode_X->Fill(MWPC_PosX);
      h_Cathode_Y->Fill(MWPC_PosY);

      h_PosY_Charge->Fill(AnodePos,AMWPC_anode1+AMWPC_anode2);
//      h_PosY_Charge->Fill(MWPC_PosY,MWPC_ChargeY);
      h_PosX_Charge->Fill(MWPC_PosX,AMWPC_anode1+AMWPC_anode2);
      //Conditioned
      if (NFireX==2){
	h_Cathode_X_cond2->Fill(MWPC_PosX);
      }
      if (NFireY==2){
	h_Cathode_Y_cond2->Fill(MWPC_PosY);
      }
      if (NFireX==3){
	h_Cathode_X_cond3->Fill(MWPC_PosX);
      }
      if (NFireY==3){
	h_Cathode_Y_cond3->Fill(MWPC_PosY);
      }
      if (NFireX==4){
	h_Cathode_X_cond4->Fill(MWPC_PosX);
      }
      if (NFireY==4){
	h_Cathode_Y_cond4->Fill(MWPC_PosY);
      }
      if (NFireX==5){
	h_Cathode_X_cond5->Fill(MWPC_PosX);
      }
      if (NFireY==5){
	h_Cathode_Y_cond5->Fill(MWPC_PosY);
      }
      if (NFireX==6){
	h_Cathode_X_cond6->Fill(MWPC_PosX);
      }
      if (NFireY==6){
	h_Cathode_Y_cond6->Fill(MWPC_PosY);
      }
      if (AnodePos>-16 && AnodePos<16 && MWPC_PosX>-15 && MWPC_PosX<15){
	h_MWPC_Cond->Fill(AMWPC_anode1+AMWPC_anode2);
	h_Anode_Image_cond->Fill(MWPC_PosX,AnodePos);
      }
      if (MWPC_PosY>-16 && MWPC_PosY<16 && MWPC_PosX>-15 && MWPC_PosX<15){
	h_Cathode_Image_cond->Fill(MWPC_PosX,MWPC_PosY);
      }
    }
    //Create the 3 groups for calibration
    TriggerMultiplicity = 5;
    //Set Smallest ones to zero, according to trigger multiplicity
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    NFireX=0;
    NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>0.1)){
      //It is important that anode2 and anode1 are flipped in simulation!!!!
      ProcessMWPCData(AMWPC_anode2,AMWPC_anode1,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
      if (NFireX==5){
	h_Cathode_X_Group0->Fill(MWPC_PosX);
      }
    }
    TriggerMultiplicity = 4;
    //Set Smallest ones to zero, according to trigger multiplicity
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    NFireX=0;
    NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>0.1)){
      ProcessMWPCData(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
      if (NFireX==4){
	h_Cathode_X_Group1->Fill(MWPC_PosX);
      }
    }
    TriggerMultiplicity = 3;
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    NFireX=0;
    NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>0.1)){
      ProcessMWPCData(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
      if (NFireX==3){
	h_Cathode_X_Group2->Fill(MWPC_PosX);
      }
    }
  }//End file loop
  //close file
  if (tfilein!=NULL){
    //tfilein->Close(); done automatically upon deleting
    delete tfilein;
    tfilein =NULL;
  }
  h_FiredWireX->Scale(100.0/N_MWPC);
  h_FiredWireY->Scale(100.0/N_MWPC);
  cout << N_MWPC << " events"<<endl;

  //Calibration
  //Now the primitive peaks are constructed by hand.
  //In the future, computer needs to do this
//  ofstream calfileout;
//  sprintf(filename,"%s/MWPCPosCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
//  calfileout.open(filename,ios::out);
  
    
  /************************************************************************************************/
  //Write to root file
  //Write to root file
  ostringstream convert;
  convert << setfill('0') << setw(9)<<FileID;
  spectrumfile = CurrentAnalyzer->SIM_DATA_DIRECTORY + string("/../Histograms/") + "SimMWPCCalPreview" + string("_") + convert.str() + string(".root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");
  h_MWPC1->Write();
  h_MWPC2->Write();
  h_MWPC_tot->Write();
  h_Charge_X->Write();
  h_Charge_Y->Write();
  h_Charge_C->Write();
  h_Charge_XY->Write();
  h_Charge_AC->Write();
  h_MWPC_Cond->Write();
  h_Anode_Pos->Write();
  h_Cathode_Image->Write();
  h_ACCorrelation->Write();
  h_Anode_Image->Write();
  h_FiredWireX->Write();
  h_FiredWireY->Write();

  h_Charge_X1->Write();
  h_Charge_X2->Write();
  h_Charge_X3->Write();
  h_Charge_X4->Write();
  h_Charge_X5->Write();
  h_Charge_X6->Write();
  h_Cathode_X->Write();
  h_Cathode_Y->Write();
  h_Cathode_X_cond2->Write();
  h_Cathode_Y_cond2->Write();
  h_Cathode_X_cond3->Write();
  h_Cathode_Y_cond3->Write();
  h_Cathode_X_cond4->Write();
  h_Cathode_Y_cond4->Write();
  h_Cathode_X_cond5->Write();
  h_Cathode_Y_cond5->Write();
  h_Cathode_X_cond6->Write();
  h_Cathode_Y_cond6->Write();
  h_Cathode_X_Group0->Write();
  h_Cathode_X_Group1->Write();
  h_Cathode_X_Group2->Write();
  h_Anode_Image_cond->Write();
  h_Cathode_Image_cond->Write();
  h_MWPC_Corr->Write();
  h_A1A2->Write();
  h_PosX_Charge->Write();
  h_PosY_Charge->Write();
  delete histfile;
  
  CurrentAnalyzer->Calibrating = false;
  return 0;
}

/***********************************************************************************/
int Calibration::CalibrateMWPCSim(int FileID,int CalID)
{
  CurrentAnalyzer->SetMode("MWPC");
  CurrentAnalyzer->Calibrating = true;
  string spectrumfile;
  stringstream convert;
  convert << setfill('0') << setw(9)<<FileID;
  
  int n = 0;  //  n of data read
  double CathodeThreshold = CurrentAnalyzer->SysWParameters["MWPCC_Threshold"][0];

  TFile* tfilein = NULL; //pointer to input root file
  TTree * InTree = NULL; //input tree pointer
  InDataStruct InData;  //input data unit
  InDataStructDouble InDataDouble;      //input data double format

  double& AMWPC_anode1 = InDataDouble.MWPCAnodeSignal1;
  double& AMWPC_anode2 = InDataDouble.MWPCAnodeSignal2;
  DataStruct_MWPC_X& MWPC_X = InDataDouble.MWPC_X;
  DataStruct_MWPC_Y& MWPC_Y = InDataDouble.MWPC_Y;

  double MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY;
  double AnodePos;

  //For sorting Cathode signals
  int TriggerMultiplicity = 6;
  double* Xsig[6];
  double* Ysig[6];
  Xsig[0] = &(MWPC_X.AX1);
  Xsig[1] = &(MWPC_X.AX2);
  Xsig[2] = &(MWPC_X.AX3);
  Xsig[3] = &(MWPC_X.AX4);
  Xsig[4] = &(MWPC_X.AX5);
  Xsig[5] = &(MWPC_X.AX6);
  Ysig[0] = &(MWPC_Y.AY1);
  Ysig[1] = &(MWPC_Y.AY2);
  Ysig[2] = &(MWPC_Y.AY3);
  Ysig[3] = &(MWPC_Y.AY4);
  Ysig[4] = &(MWPC_Y.AY5);
  Ysig[5] = &(MWPC_Y.AY6);

  //Create Cathode Signal Calibration data structure
  double CathodeSignalMap[257][18][12];
  for (int i=0;i<257;i++){
    for (int j=0;j<18;j++){
      for (int k=0;k<12;k++){
	CathodeSignalMap[i][j][k] = 0;
      }
    }
  }

  //For Calibration
  /*
  double PrimitivePeaksX0[9] = {-7.875,-5.875,-3.9375,-2.0,0.0,2.0625,4.0,5.9375,7.9375};
  double PrimitivePeaksX1[15] = {-11.6875,-9.875,-8.0,-6.0,-4.1875,-3.75,-1.875,0.0,1.9375,3.8125,4.25,6.0625,8.0,10.0,11.75};
  double PrimitivePeaksX2[20] = {-15.3125,-13.85,-11.875,-10.25,-8.75,-7.375,-5.625,-3.9375,-2.3125,-0.55,0.56,2.32,4.0,5.65,7.45,8.56,10.3125,11.9375,13.9,15.4};
  double PrimitivePeaksY[18] = {-15.45,-13.85,-11.875,-10.25,-8.5,-7.45,-5.625,-3.0,-1.0,1.0,3.25,5.25,7.5,9.0,11.0,12.75,14.5,16.0};

  double PrimitivePeaksA[18] = {-15.9,-13.8,-11.85,-10.1,-8.15,-6.25,-4.45,-2.6,-0.75,1.0,2.7,4.6,6.4,8.2,10.15,12.05,14.1,16.0};

  double ExactPos0[9] = {-8.0,-6.0,-4.0,-2.0,0.0,2.0,4.0,6.0,8.0};
  double ExactPos1[15] = {-12.0,-10.0,-8.0,-6.0,-4.0,-4.0,-2.0,0.0,2.0,4.0,4.0,6.0,8.0,10.0,12.0};
  double ExactPos2[20] = {-16.0,-14.0,-12.0,-10.0,-8.0,-8.0,-6.0,-4.0,-2.0,0.0,0.0,2.0,4.0,6.0,8.0,8.0,10.0,12.0,14.0,16.0};
  double ExactPosA[18] = {-17.0,-15.0,-13.0,-11.0,-9.0,-7.0,-5.0,-3.0,-1.0,1.0,3.0,5.0,7.0,9.0,11.0,13.0,15.0,17.0};
*/
  //New version: read in peak positions from file
  double PrimitivePeaksX0[9];
  double PrimitivePeaksX1[15];
  double PrimitivePeaksX2[20];
  double PrimitivePeaksY[18];
  double PrimitivePeaksA[18];

  double ExactPos0[9];
  double ExactPos1[15]; 
  double ExactPos2[20];
  double ExactPosA[18]; 

  ifstream calpeakfilein;
  char filename[500];
  sprintf(filename,"%s/SimMWPCPosPeaks%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  calpeakfilein.open(filename,ios::in);
  if (!calpeakfilein.is_open()){
    cout <<"Error! file "<< filename<<" does not exist.\n"<<endl;
    return -1;
  }
  for (int i=0;i<9;i++){
    calpeakfilein>>ExactPos0[i]>>PrimitivePeaksX0[i];
  }
  for (int i=0;i<15;i++){
    calpeakfilein>>ExactPos1[i]>>PrimitivePeaksX1[i];
  }
  for (int i=0;i<20;i++){
    calpeakfilein>>ExactPos2[i]>>PrimitivePeaksX2[i];
  }
  for (int i=0;i<18;i++){
    calpeakfilein>>ExactPosA[i]>>PrimitivePeaksA[i];
  }
  calpeakfilein.close();

  //Peaks after scanning for maximum
  double PeaksX0[9];
  double PeaksX1[15];
  double PeaksX2[20];
  double PeaksA[18];

  TGraph* XGraph0;
  TGraph* XGraph1[3];
  TGraph* XGraph2[4];
  TGraph* AGraph;
//  XGraph1 = new (TGraph*)[3];
//  XGraph2 = new (TGraph*)[4];
  XGraph0 = new TGraph();
  AGraph = new TGraph();
  XGraph0->SetName("XGraph0");
  AGraph->SetName("AGraph");
  for (int i=0;i<3;i++){
    XGraph1[i] = new TGraph();
    XGraph1[i]->SetName(Form("XGraph1_%d",i));
  }
  for (int i=0;i<4;i++){
    XGraph2[i] = new TGraph();
    XGraph2[i]->SetName(Form("XGraph2_%d",i));
  }
  TF1* fitFuncA;
  TF1* fitFuncX0;
  TF1* fitFuncX1[3];
  TF1* fitFuncX2[4];
  double FitParametersA[4];
  double FitParametersX0[4];
  double FitParametersX1[3][4];
  double FitParametersX2[4][4];
/*  for (int j=0;j<6;j++){
    cout << CurrentAnalyzer->AmpCalX[j] << " ";
  }
  cout << endl;
  for (int j=0;j<6;j++){
    cout << CurrentAnalyzer->AmpCalY[j] << " ";
  }
  cout << endl;
  cout << CurrentAnalyzer->AmpCalA[0]<<" "<<CurrentAnalyzer->AmpCalA[1]<<endl;
  */
  // root histos
  TH1D *h_MWPC1  = new TH1D("h_MWPC1","MWPC Anode1 spectrum",1024,0,20);
  TH1D *h_MWPC2  = new TH1D("h_MWPC2","MWPC Anode2 spectrum",1024,0,20);
  TH2* h_A1A2 = new TH2D("h_A1A2","A1 A2 correlation",1024,0,20,1024,0,20);
  TH2* h_PosX_Charge = new TH2D("h_PosX_Charge","X-TotalCharge correlation",800,-25.03125,24.96875,400,0,200);
  TH2* h_PosY_Charge = new TH2D("h_PosY_Charge","Y-TotalCharge correlation",800,-25.03125,24.96875,400,0,200);
  TH1D *h_MWPC_tot  = new TH1D("h_MWPC_tot","MWPC Anode_tot spectrum",1024,0,20);
  TH1D *h_MWPC_Cond  = new TH1D("h_MWPC_Cond","MWPC Anode_tot spectrum",1024,0,20);
  TH1 *h_FiredWireX = new TH1D("h_FiredWireX","# of fired stripes X",10,0,10);
  TH1 *h_FiredWireY = new TH1D("h_FiredWireY","# of fired stripes Y",10,0,10);

  TH1D *h_Cathode_X = new TH1D("h_Cathode_X","MWPC CathodeX position spectrum",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y = new TH1D("h_Cathode_Y","MWPC CathodeY position spectrum",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond2 = new TH1D("h_Cathode_X_cond2","MWPC CathodeX position spectrum cond2",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond2 = new TH1D("h_Cathode_Y_cond2","MWPC CathodeY position spectrum cond2",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond3 = new TH1D("h_Cathode_X_cond3","MWPC CathodeX position spectrum cond3",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond3 = new TH1D("h_Cathode_Y_cond3","MWPC CathodeY position spectrum cond3",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond4 = new TH1D("h_Cathode_X_cond4","MWPC CathodeX position spectrum cond4",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond4 = new TH1D("h_Cathode_Y_cond4","MWPC CathodeY position spectrum cond4",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond5 = new TH1D("h_Cathode_X_cond5","MWPC CathodeX position spectrum cond5",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond5 = new TH1D("h_Cathode_Y_cond5","MWPC CathodeY position spectrum cond5",800,-25.03125,24.96875);
  TH1D *h_Cathode_X_cond6 = new TH1D("h_Cathode_X_cond6","MWPC CathodeX position spectrum cond6",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y_cond6 = new TH1D("h_Cathode_Y_cond6","MWPC CathodeY position spectrum cond6",800,-25.03125,24.96875);
  TH1D *h_Anode_Pos = new TH1D("h_Anode_Pos","MWPC Anode position spectrum",800,-25.03125,24.96875);
  TH2D *h_Cathode_Image = new TH2D("h_Cathode_Image","2D image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Anode_Image = new TH2D("h_Anode_Image","2D Anode image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_MWPC_Corr  = new TH2D("h_MWPC1_2","MWPC Anode1-2 spectrum",1024,0,20,1024,0,20);
  TH2D *h_Cathode_Image_cond = new TH2D("h_Cathode_Image_cond","2D image of MWPC, more than 3 trigger",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Anode_Image_cond = new TH2D("h_Anode_Image_cond","2D Anode image of MWPC more than 3 trigger",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Direct_Image = new TH2D("h_Direct_Image","2D Direct image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_ACCorrelation = new TH2D("h_ACCorrelation","MWPC anode cathode x correlation",400,-25,25,400,-25,25);
  TH1D *h_Charge_X = new TH1D("h_Charge_X","Total Charge on MWPC X",1024,0,20);
  TH1D *h_Charge_Y = new TH1D("h_Charge_Y","Total Charge on MWPC Y",1024,0,20);
  TH1D *h_Charge_C = new TH1D("h_Charge_C","MWPC Cathode_tot spectrum",1024,0,20);
  TH2D *h_Charge_XY = new TH2D("h_Charge_XY","Cathode X-Y charge correlation",400,0,20,400,0,20);
  TH2D *h_Charge_AC = new TH2D("h_Charge_AC","Cathode-Anode charge correlation",400,0,20,400,0,20);

  TH1D *h_Charge_X1 = new TH1D("h_Charge_X1","Total Charge on MWPC X1",1000,0,20);
  TH1D *h_Charge_X2 = new TH1D("h_Charge_X2","Total Charge on MWPC X2",1000,0,20);
  TH1D *h_Charge_X3 = new TH1D("h_Charge_X3","Total Charge on MWPC X3",1000,0,20);
  TH1D *h_Charge_X4 = new TH1D("h_Charge_X4","Total Charge on MWPC X4",1000,0,20);
  TH1D *h_Charge_X5 = new TH1D("h_Charge_X5","Total Charge on MWPC X5",1000,0,20);
  TH1D *h_Charge_X6 = new TH1D("h_Charge_X6","Total Charge on MWPC X6",1000,0,20);

  //Histograms for calibration
  //X spectrum, using 5 triggered stripes
  TH1D *h_Cathode_X_Group0 = new TH1D("h_Cathode_X_Group0","MWPC CathodeX position spectrum Group0",800,-25.03125,24.96875);
  //X spectrum, using 4 triggered stripes
  TH1D *h_Cathode_X_Group1 = new TH1D("h_Cathode_X_Group1","MWPC CathodeX position spectrum Group1",800,-25.03125,24.96875);
  //X spectrum, using 3 triggered stripes
  TH1D *h_Cathode_X_Group2 = new TH1D("h_Cathode_X_Group2","MWPC CathodeX position spectrum Group2",800,-25.03125,24.96875);

  TH2D *h_Cathode_Image_Cal = new TH2D("h_Cathode_Image_Cal","2D image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Anode_Image_Cal = new TH2D("h_Anode_Image_Cal","2D Anode image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_ACCorrelation_Cal = new TH2D("h_ACCorrelation_Cal","MWPC anode cathode x correlation",200,-25,25,200,-25,25);
  TH1D *h_Anode_Cal = new TH1D("h_Anode_Cal","MWPC Anode position spectrum",800,-25.0625,24.96875);
  TH1D *h_Cathode_X_Cal = new TH1D("h_Cathode_X_Cal","MWPC CathodeX position spectrum",800,-25.0625,24.96875);
  TH1D *h_Cathode_Y_Cal = new TH1D("h_Cathode_Y_Cal","MWPC CathodeY position spectrum",800,-25.0625,24.96875);
  TH1D *h_Cathode_X_cond3_Cal = new TH1D("h_Cathode_X_cond3_Cal","MWPC CathodeX position spectrum cond3",800,-25.0625,24.96875);
  TH1D *h_Cathode_X_cond4_Cal = new TH1D("h_Cathode_X_cond4_Cal","MWPC CathodeX position spectrum cond4",800,-25.0625,24.96875);
  TH1D *h_Cathode_X_cond5_Cal = new TH1D("h_Cathode_X_cond5_Cal","MWPC CathodeX position spectrum cond5",800,-25.0625,24.96875);
  TH1D *h_MWPCTot_Cal  = new TH1D("h_MWPCTot_Cal","MWPC Anode_tot calibrated spectrum",1024,0,18);
  TH1D *h_MWPCTot_Cal2  = new TH1D("h_MWPCTot_Cal2","MWPC Anode_tot calibrated spectrum",1024,0,18);
  TH1D *h_Charge_X_Cal = new TH1D("h_Charge_X_Cal","Total Charge on MWPC X calibrated",1024,0,18);
  TH1D *h_Charge_Y_Cal = new TH1D("h_Charge_Y_Cal","Total Charge on MWPC Y calibrated",1024,0,18);


  if(CurrentAnalyzer->SetupInputFile(FileID,tfilein,InTree)==-1) return -1;
  CurrentAnalyzer->SetInTreeAddress(InTree, InData);
  int returnVal = 0; 
  int N_MWPC=0;
  while(1){
    returnVal = InTree->GetEntry(N_MWPC);
    if (returnVal==0) break; //eof
    else if(returnVal==-1) return -1;
    CurrentAnalyzer->ConvertDataToDouble(InData,InDataDouble); 
    N_MWPC++;
//    filein.read((char *)&(InData.MWPCEnergy),sizeof(unsigned short));
//    filein.read((char *)(InData.MWPCHitPos),2*sizeof(short));
//    filein.read((char *)&(InData.MWPCAnodeSignal1),sizeof(unsigned short));
//    filein.read((char *)&(InData.MWPCAnodeSignal2),sizeof(unsigned short));
//    filein.read((char *)(InData.MWPCCathodeSignal),12*sizeof(unsigned short));
//    //        filein.read((char *)(InData.MWPCCathodeSignal),12*sizeof(unsigned int));
//    filein.read((char *)&(InData.ExitAngle_Be),sizeof(unsigned short));
//    filein.read((char *)(InData.InitP),3*sizeof(int));
//    filein.read((char *)(InData.DECPos),3*sizeof(short));
//    filein.read((char *)&(InData.Hits),sizeof(uint16_t));
//    filein.read((char *)&(InData.ParticleID),sizeof(uint8_t));

//    //Convert to double
//    //    InDataDouble.ScintEnergy = double(InData.ScintEnergy)/1000.0;
//    //    InDataDouble.MWPCEnergy = double(InData.MWPCEnergy)/1000.0;
//    AMWPC_anode1 = double(InData.MWPCAnodeSignal1)/2000.0;
//    AMWPC_anode2 = double(InData.MWPCAnodeSignal2)/2000.0;
//    MWPC_X.AX1 = double(InData.MWPCCathodeSignal[0])/8000.0;
//    MWPC_X.AX2 = double(InData.MWPCCathodeSignal[1])/8000.0;
//    MWPC_X.AX3 = double(InData.MWPCCathodeSignal[2])/8000.0;
//    MWPC_X.AX4 = double(InData.MWPCCathodeSignal[3])/8000.0;
//    MWPC_X.AX5 = double(InData.MWPCCathodeSignal[4])/8000.0;
//    MWPC_X.AX6 = double(InData.MWPCCathodeSignal[5])/8000.0;
//    MWPC_Y.AY1 = double(InData.MWPCCathodeSignal[6])/8000.0;
//    MWPC_Y.AY2 = double(InData.MWPCCathodeSignal[7])/8000.0;
//    MWPC_Y.AY3 = double(InData.MWPCCathodeSignal[8])/8000.0;
//    MWPC_Y.AY4 = double(InData.MWPCCathodeSignal[9])/8000.0;
//    MWPC_Y.AY5 = double(InData.MWPCCathodeSignal[10])/8000.0;
//    MWPC_Y.AY6 = double(InData.MWPCCathodeSignal[11])/8000.0;
//    //    InDataDouble.ExitAngle_Be = double(InData.ExitAngle_Be)/100.0;
//    for(int i=0;i<2;i++){
//      InDataDouble.MWPCHitPos[i] = double(InData.MWPCHitPos[i])/1000.0;
//    }

    //Fill each X cathode signal
    if (MWPC_X.AX1>0)h_Charge_X1->Fill(MWPC_X.AX1); 
    if (MWPC_X.AX2>0)h_Charge_X2->Fill(MWPC_X.AX2); 
    if (MWPC_X.AX3>0)h_Charge_X3->Fill(MWPC_X.AX3); 
    if (MWPC_X.AX4>0)h_Charge_X4->Fill(MWPC_X.AX4); 
    if (MWPC_X.AX5>0)h_Charge_X5->Fill(MWPC_X.AX5); 
    if (MWPC_X.AX6>0)h_Charge_X6->Fill(MWPC_X.AX6); 
    //Sort signals according to their amplitudes
    //X
    double *ptr;
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Xsig[k])<*(Xsig[k+1])){
	  ptr = Xsig[k];
	  Xsig[k]=Xsig[k+1];
	  Xsig[k+1]=ptr;
	}
      }
    }
    //Y
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Ysig[k])<*(Ysig[k+1])){
	  ptr = Ysig[k];
	  Ysig[k]=Ysig[k+1];
	  Ysig[k+1]=ptr;
	}
      }
    }

    TriggerMultiplicity = 6;
    //Set Smallest ones to zero, according to trigger multiplicity
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    //Count # of fired wires
    int NFireX=0;
    int NFireY=0;
    //Count Triggered Wires
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);

    h_A1A2->Fill(AMWPC_anode1,AMWPC_anode2);
    if ((AMWPC_anode1+AMWPC_anode2>0.1)){
      //MWPC Anode and Image
      h_Direct_Image->Fill(InDataDouble.MWPCHitPos[0],InDataDouble.MWPCHitPos[1]);
      h_MWPC1->Fill(AMWPC_anode1);
      h_MWPC2->Fill(AMWPC_anode2);
      h_MWPC_tot->Fill(AMWPC_anode1+AMWPC_anode2);
      h_MWPC_Corr->Fill(AMWPC_anode1,AMWPC_anode2);
      //Process MWPC Data, signal to position and charge
      //It is important that anode2 and anode1 are flipped in simulation!!!!
      ProcessMWPCData(AMWPC_anode2,AMWPC_anode1,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);

      h_Anode_Pos->Fill(AnodePos);
      h_Cathode_Image->Fill(MWPC_PosX,MWPC_PosY);
      if(MWPC_ChargeY>0)h_ACCorrelation->Fill(MWPC_PosY,AnodePos);
      if (MWPC_ChargeX>0)h_Anode_Image->Fill(MWPC_PosX,AnodePos);
      h_Charge_X->Fill(MWPC_ChargeX);
      h_Charge_Y->Fill(MWPC_ChargeY);
      h_Charge_C->Fill(MWPC_ChargeX+MWPC_ChargeY);
      h_Charge_XY->Fill(MWPC_ChargeX,MWPC_ChargeY);
      h_Charge_AC->Fill(MWPC_ChargeX+MWPC_ChargeY,AMWPC_anode1+AMWPC_anode2);

      h_FiredWireX->Fill(NFireX);
      h_FiredWireY->Fill(NFireY);

      h_Cathode_X->Fill(MWPC_PosX);
      h_Cathode_Y->Fill(MWPC_PosY);

      h_PosY_Charge->Fill(AnodePos,AMWPC_anode1+AMWPC_anode2);
//      h_PosY_Charge->Fill(MWPC_PosY,MWPC_ChargeY);
      h_PosX_Charge->Fill(MWPC_PosX,AMWPC_anode1+AMWPC_anode2);
      //Conditioned
      if (NFireX==2){
	h_Cathode_X_cond2->Fill(MWPC_PosX);
      }
      if (NFireY==2){
	h_Cathode_Y_cond2->Fill(MWPC_PosY);
      }
      if (NFireX==3){
	h_Cathode_X_cond3->Fill(MWPC_PosX);
      }
      if (NFireY==3){
	h_Cathode_Y_cond3->Fill(MWPC_PosY);
      }
      if (NFireX==4){
	h_Cathode_X_cond4->Fill(MWPC_PosX);
      }
      if (NFireY==4){
	h_Cathode_Y_cond4->Fill(MWPC_PosY);
      }
      if (NFireX==5){
	h_Cathode_X_cond5->Fill(MWPC_PosX);
      }
      if (NFireY==5){
	h_Cathode_Y_cond5->Fill(MWPC_PosY);
      }
      if (NFireX==6){
	h_Cathode_X_cond6->Fill(MWPC_PosX);
      }
      if (NFireY==6){
	h_Cathode_Y_cond6->Fill(MWPC_PosY);
      }
      if (AnodePos>-16 && AnodePos<16 && MWPC_PosX>-15 && MWPC_PosX<15){
	h_MWPC_Cond->Fill(AMWPC_anode1+AMWPC_anode2);
	h_Anode_Image_cond->Fill(MWPC_PosX,AnodePos);
      }
      if (MWPC_PosY>-16 && MWPC_PosY<16 && MWPC_PosX>-15 && MWPC_PosX<15){
	h_Cathode_Image_cond->Fill(MWPC_PosX,MWPC_PosY);
      }
    }
    //Create the 3 groups for calibration
    TriggerMultiplicity = 5;
    //Set Smallest ones to zero, according to trigger multiplicity
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    NFireX=0;
    NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>0.1)){
      //It is important that anode2 and anode1 are flipped in simulation!!!!
      ProcessMWPCData(AMWPC_anode2,AMWPC_anode1,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
      if (NFireX==5){
	h_Cathode_X_Group0->Fill(MWPC_PosX);
      }
    }
    TriggerMultiplicity = 4;
    //Set Smallest ones to zero, according to trigger multiplicity
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    NFireX=0;
    NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>0.1)){
      //It is important that anode2 and anode1 are flipped in simulation!!!!
      ProcessMWPCData(AMWPC_anode2,AMWPC_anode1,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
      if (NFireX==4){
	h_Cathode_X_Group1->Fill(MWPC_PosX);
      }
    }
    TriggerMultiplicity = 3;
    for (int j=TriggerMultiplicity;j<6;j++){
      *(Xsig[j])=0;
      *(Ysig[j])=0;
    }
    NFireX=0;
    NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>0.1)){
      //It is important that anode2 and anode1 are flipped in simulation!!!!
      ProcessMWPCData(AMWPC_anode2,AMWPC_anode1,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
      if (NFireX==3){
	h_Cathode_X_Group2->Fill(MWPC_PosX);
      }
    }
  }//End file loop
  //close file
  if (tfilein!=NULL){
    //tfilein->Close(); done automatically upon deleting
    delete tfilein;
    tfilein =NULL;
  }
  h_FiredWireX->Scale(100.0/N_MWPC);
  h_FiredWireY->Scale(100.0/N_MWPC);
  cout << N_MWPC << " events"<<endl;

  //Calibration
  ofstream calfileout;
  //char filename[500];
  sprintf(filename,"%s/SimMWPCPosCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  calfileout.open(filename,ios::out);
  
  /*
  //CathodeX
  //Find Peaks, maxima
  int Index = 0;
  double Max = 0;
  for (int i=0;i<9;i++){
    if (i==4){
      calfileout<<PeaksX0[i]<<endl;
      continue;
    }
    PeaksX0[i] = PrimitivePeaksX0[i];
    Index = h_Cathode_X_Group0->FindBin(PeaksX0[i]);
    Max = h_Cathode_X_Group0->GetBinContent(Index);
    for (int j=Index-2;j<=Index+2;j++){
      if (h_Cathode_X_Group0->GetBinContent(j)>Max){
	Max = h_Cathode_X_Group0->GetBinContent(j);
	PeaksX0[i] = h_Cathode_X_Group0->GetBinCenter(j);
      }
    }
    calfileout<<PeaksX0[i]<<endl;
  }
  for (int i=0;i<15;i++){
    PeaksX1[i] = PrimitivePeaksX1[i];
    Index = h_Cathode_X_Group1->FindBin(PeaksX1[i]);
    Max = h_Cathode_X_Group1->GetBinContent(Index);
    for (int j=Index-2;j<=Index+2;j++){
      if (h_Cathode_X_Group1->GetBinContent(j)>Max){
	Max = h_Cathode_X_Group1->GetBinContent(j);
	PeaksX1[i] = h_Cathode_X_Group1->GetBinCenter(j);
      }
    }
    calfileout<<PeaksX1[i]<<endl;
//    cout << PrimitivePeaksX1[i]<<" "<<PeaksX1[i]<<endl;
  }
  for (int i=0;i<20;i++){
    PeaksX2[i] = PrimitivePeaksX2[i];
    Index = h_Cathode_X_Group2->FindBin(PeaksX2[i]);
    Max = h_Cathode_X_Group2->GetBinContent(Index);
    for (int j=Index-2;j<=Index+2;j++){
      if (h_Cathode_X_Group2->GetBinContent(j)>Max){
	Max = h_Cathode_X_Group2->GetBinContent(j);
	PeaksX2[i] = h_Cathode_X_Group2->GetBinCenter(j);
      }
    }
    calfileout<<PeaksX2[i]<<endl;
  }
  //Temp
  //How to deal with double peaks?
  for (int i=0;i<18;i++){
    calfileout<<PrimitivePeaksY[i]<<endl;
  }
  //For anode
  for (int i=0;i<18;i++){
    PeaksA[i] = PrimitivePeaksA[i];
    Index = h_Anode_Pos->FindBin(PeaksA[i]);
    Max = h_Anode_Pos->GetBinContent(Index);
    for (int j=Index-2;j<=Index+2;j++){
      if (h_Anode_Pos->GetBinContent(j)>Max){
	Max = h_Anode_Pos->GetBinContent(j);
	PeaksA[i] = h_Anode_Pos->GetBinCenter(j);
      }
    }
    calfileout<<PeaksA[i]<<endl;
  }
  */
  //Need to develope a better way to find peaks automatically
  //For now, directly copy the primitve peaks determined by hand and output
  //CathodeX
  for (int i=0;i<9;i++){
    PeaksX0[i]=PrimitivePeaksX0[i];
    calfileout<<ExactPos0[i]<<" "<<PeaksX0[i]<<endl;
  }
  for (int i=0;i<15;i++){
    PeaksX1[i]=PrimitivePeaksX1[i];
    calfileout<<ExactPos1[i]<<" "<<PeaksX1[i]<<endl;
  }
  for (int i=0;i<20;i++){
    PeaksX2[i]=PrimitivePeaksX2[i];
    calfileout<<ExactPos2[i]<<" "<<PeaksX2[i]<<endl;
  }
  //For anode
  for (int i=0;i<18;i++){
    PeaksA[i]=PrimitivePeaksA[i];
    calfileout<<ExactPosA[i]<<" "<<PeaksA[i]<<endl;
  }

  calfileout.close();

  //Set Points
  for (int i=0;i<18;i++){
    AGraph->SetPoint(i,PeaksA[i],-17.0+2.0*i);
  }
  for (int i=0;i<9;i++){
    XGraph0->SetPoint(i,PeaksX0[i],-8.0+2.0*i);
  }
  for (int i=0;i<5;i++){
    for (int j=0;j<3;j++){
      XGraph1[j]->SetPoint(i,PeaksX1[i+j*5]+8.0*(1-j),-4.0+2.0*i);
    }
  }
  for (int i=0;i<5;i++){
    for (int j=0;j<4;j++){
      XGraph2[j]->SetPoint(i,PeaksX2[i+j*5]+8.0*(1-j)+4.0,-4.0+2.0*i);
    }
  }
  //Fit to pol[3]
  AGraph->Fit("pol3");
  fitFuncA = AGraph->GetFunction("pol3");
  for (int j=0;j<4;j++){
    FitParametersA[j] = fitFuncA->GetParameter(j);
  }
  XGraph0->Fit("pol3");
  fitFuncX0 = XGraph0->GetFunction("pol3");
  for (int j=0;j<4;j++){
    FitParametersX0[j] = fitFuncX0->GetParameter(j);
  }
  for (int i=0;i<3;i++){
    XGraph1[i]->Fit("pol3");
    fitFuncX1[i] = XGraph1[i]->GetFunction("pol3");
    for (int j=0;j<4;j++){
      FitParametersX1[i][j] = fitFuncX1[i]->GetParameter(j);
    }
  }
  for (int i=0;i<4;i++){
    XGraph2[i]->Fit("pol3");
    fitFuncX2[i] = XGraph2[i]->GetFunction("pol3");
    for (int j=0;j<4;j++){
      FitParametersX2[i][j] = fitFuncX2[i]->GetParameter(j);
    }
  }
  //Calibration Complete
  //Start testing calibration

  /*******************************Gain Calibration*************************************************/
  //This can be done with the testing
  TH1* CellSpec[17][18];
  double GainMap[17][18];
  string HistName;
  string HistTitle;
  for (int i=0;i<17;i++){
    for (int j=0;j<18;j++){
      ostringstream double2str;
      double2str << setfill('0') << setw(2) <<i<<"_"<< setw(2) <<j;
      HistName="Cell_" + double2str.str();
      HistTitle="Spectrum for cell " + double2str.str();
      CellSpec[i][j]=new TH1D(HistName.c_str(),HistTitle.c_str(),512,0,18.0);
      GainMap[i][j] = 0.0;
    }
  }
  vector<double> Gain;
  vector<double> GainX;
  vector<double> GainY;
  vector<double> GainChargeX;
  vector<double> GainChargeY;
  /*******************************Gain Calibration*************************************************/
  TH1* CathodeXSig = new TH1D("CathodeXSig","CathodeX Signal distribution at X=-2mm",6,-24,24);

  double TempMWPC_X[6];
  double TempMWPC_Y[6];

  if(CurrentAnalyzer->SetupInputFile(FileID,tfilein,InTree)==-1) return -1;
  CurrentAnalyzer->SetInTreeAddress(InTree, InData);
  N_MWPC=0;
  while(1){
    returnVal = InTree->GetEntry(N_MWPC);
    if (returnVal==0) break; //eof
    else if(returnVal==-1) return -1;
    CurrentAnalyzer->ConvertDataToDouble(InData,InDataDouble); 
    N_MWPC++;
//    filein.read((char *)&(InData.MWPCEnergy),sizeof(unsigned short));
//    filein.read((char *)(InData.MWPCHitPos),2*sizeof(short));
//    filein.read((char *)&(InData.MWPCAnodeSignal1),sizeof(unsigned short));
//    filein.read((char *)&(InData.MWPCAnodeSignal2),sizeof(unsigned short));
//    filein.read((char *)(InData.MWPCCathodeSignal),12*sizeof(unsigned short));
//    //        filein.read((char *)(InData.MWPCCathodeSignal),12*sizeof(unsigned int));
//    filein.read((char *)&(InData.ExitAngle_Be),sizeof(unsigned short));
//    filein.read((char *)(InData.InitP),3*sizeof(int));
//    filein.read((char *)(InData.DECPos),3*sizeof(short));
//    filein.read((char *)&(InData.Hits),sizeof(uint16_t));
//    filein.read((char *)&(InData.ParticleID),sizeof(uint8_t));

//    //Convert to double
//    //    InDataDouble.ScintEnergy = double(InData.ScintEnergy)/1000.0;
//    //    InDataDouble.MWPCEnergy = double(InData.MWPCEnergy)/1000.0;
//    AMWPC_anode1 = double(InData.MWPCAnodeSignal1)/2000.0;
//    AMWPC_anode2 = double(InData.MWPCAnodeSignal2)/2000.0;
//    MWPC_X.AX1 = double(InData.MWPCCathodeSignal[0])/8000.0;
//    MWPC_X.AX2 = double(InData.MWPCCathodeSignal[1])/8000.0;
//    MWPC_X.AX3 = double(InData.MWPCCathodeSignal[2])/8000.0;
//    MWPC_X.AX4 = double(InData.MWPCCathodeSignal[3])/8000.0;
//    MWPC_X.AX5 = double(InData.MWPCCathodeSignal[4])/8000.0;
//    MWPC_X.AX6 = double(InData.MWPCCathodeSignal[5])/8000.0;
//    MWPC_Y.AY1 = double(InData.MWPCCathodeSignal[6])/8000.0;
//    MWPC_Y.AY2 = double(InData.MWPCCathodeSignal[7])/8000.0;
//    MWPC_Y.AY3 = double(InData.MWPCCathodeSignal[8])/8000.0;
//    MWPC_Y.AY4 = double(InData.MWPCCathodeSignal[9])/8000.0;
//    MWPC_Y.AY5 = double(InData.MWPCCathodeSignal[10])/8000.0;
//    MWPC_Y.AY6 = double(InData.MWPCCathodeSignal[11])/8000.0;
//    //    InDataDouble.ExitAngle_Be = double(InData.ExitAngle_Be)/100.0;
//    for(int i=0;i<2;i++){
//      InDataDouble.MWPCHitPos[i] = double(InData.MWPCHitPos[i])/1000.0;
//    }

    //Save temp cathode data
    TempMWPC_X[0] = MWPC_X.AX1;
    TempMWPC_X[1] = MWPC_X.AX2;
    TempMWPC_X[2] = MWPC_X.AX3;
    TempMWPC_X[3] = MWPC_X.AX4;
    TempMWPC_X[4] = MWPC_X.AX5;
    TempMWPC_X[5] = MWPC_X.AX6;
    TempMWPC_Y[0] = MWPC_Y.AY1;
    TempMWPC_Y[1] = MWPC_Y.AY2;
    TempMWPC_Y[2] = MWPC_Y.AY3;
    TempMWPC_Y[3] = MWPC_Y.AY4;
    TempMWPC_Y[4] = MWPC_Y.AY5;
    TempMWPC_Y[5] = MWPC_Y.AY6;
    //Sort signals according to their amplitudes
    //X
    double *ptr;
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Xsig[k])<*(Xsig[k+1])){
	  ptr = Xsig[k];
	  Xsig[k]=Xsig[k+1];
	  Xsig[k+1]=ptr;
	}
      }
    }
    //Y
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Ysig[k])<*(Ysig[k+1])){
	  ptr = Ysig[k];
	  Ysig[k]=Ysig[k+1];
	  Ysig[k+1]=ptr;
	}
      }
    }

    double MWPC_ChargeX6 = MWPC_X.AX1+MWPC_X.AX2+MWPC_X.AX3+MWPC_X.AX4+MWPC_X.AX5+MWPC_X.AX6;
    double MWPC_ChargeY6 = MWPC_Y.AY1+MWPC_Y.AY2+MWPC_Y.AY3+MWPC_Y.AY4+MWPC_Y.AY5+MWPC_Y.AY6;

    AMWPC_anode2 = AMWPC_anode2*CurrentAnalyzer->AmpCalA[0]/CurrentAnalyzer->AmpCalA[1];
    int NFireX=0;
    int NFireY=0;
    CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
    if ((AMWPC_anode1+AMWPC_anode2>0.1)){
      //Calibrations
      if (CalibrationScheme.compare("Pol")==0){
	//It is important that anode2 and anode1 are flipped in simulation!!!!
	ConstructCorrectedMWPCPosPol(AMWPC_anode2,AMWPC_anode1,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,Xsig,Ysig,FitParametersA,FitParametersX0,FitParametersX1,FitParametersX2,KeepLessTrigger,CathodeThreshold);
      }else if(CalibrationScheme.compare("Micro")==0){
	ConstructCorrectedMWPCPosMicro(AMWPC_anode2,AMWPC_anode1,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,Xsig,Ysig,PeaksX0,PeaksX1,PeaksX2,PeaksA,ExactPos0,ExactPos1,ExactPos2,ExactPosA,KeepLessTrigger,CathodeThreshold);
      }else{
	cout<<"Uknown scheme "<<CalibrationScheme<<endl;
	return -1;
      }
            
      if (NFireX>=3){
	h_Cathode_Image_Cal->Fill(MWPC_PosX,MWPC_PosY);
	if(MWPC_ChargeY>0)h_ACCorrelation_Cal->Fill(MWPC_PosY,AnodePos);
	if (MWPC_ChargeX>0)h_Anode_Image_Cal->Fill(MWPC_PosX,AnodePos);
	h_Anode_Cal->Fill(AnodePos);
	h_Cathode_X_Cal->Fill(MWPC_PosX);
	h_Cathode_Y_Cal->Fill(MWPC_PosY);
	/*******************************Gain Calibration*************************************************/
	Gain.push_back(AMWPC_anode1+AMWPC_anode2);
	GainChargeX.push_back(MWPC_ChargeX6);
	GainChargeY.push_back(MWPC_ChargeY6);
	GainX.push_back(MWPC_PosX);
	GainY.push_back(AnodePos);
	int ii = int((MWPC_PosX+17.0)/2.0);
	int jj = int((AnodePos+18.0)/2.0);
	if (ii>=0 && ii<17 && jj>=0 && jj<18){
	  CellSpec[ii][jj]->Fill(AMWPC_anode1+AMWPC_anode2);
	}
      }

      //Conditioned
      if (NFireX==3){
	h_Cathode_X_cond3_Cal->Fill(MWPC_PosX);
      }
      if (NFireX==4){
	h_Cathode_X_cond4_Cal->Fill(MWPC_PosX);
      }
      if (NFireX==5){
	h_Cathode_X_cond5_Cal->Fill(MWPC_PosX);
      }

      //Cathode signal calibration
      //Fill Cathode Signal Map
      //Only calibrate the central region
      if (MWPC_PosX>-16.0 && MWPC_PosX<16.0 && AnodePos>-17.0 && AnodePos<17.0){
	int XIndex = int((MWPC_PosX+16.0625)/0.125);
	int YIndex = int((AnodePos+18.0)/2.0);
	for (int kk=0;kk<6;kk++){
	  CathodeSignalMap[XIndex][YIndex][kk]+=TempMWPC_X[kk];
	  CathodeSignalMap[XIndex][YIndex][kk+6]+=TempMWPC_Y[kk];
	}
      }
    }
  }
  //close file
  if (tfilein!=NULL){
    //tfilein->Close(); done automatically upon deleting
    delete tfilein;
    tfilein =NULL;
  }

  //Save Cathode Signal Map to file
  //Normalize
  double CathodeSignalSumX[257][18];
  for (int i=0;i<257;i++){
    for (int j=0;j<18;j++){
      CathodeSignalSumX[i][j]=0.0;
    }
  }
  for (int i=0;i<257;i++){
    for (int j=0;j<18;j++){
      for (int k=0;k<6;k++){
	CathodeSignalSumX[i][j]+=CathodeSignalMap[i][j][k];
      }
    }
  }
  //Fill inspection histogram
  for (int ii=0;ii<6;ii++){
    CathodeXSig->Fill(-20+ii*8,CathodeSignalMap[119][8][ii]/CathodeSignalSumX[119][8]);
  }
  //No signal map output for simulation

  //Compare to standard positions
  TGraphErrors* DifferenceX = new TGraphErrors();
  DifferenceX->SetName("DifferenceX");
  DifferenceX->SetTitle("DifferenceX");
  for (int i=0;i<17;i++){
    h_Cathode_X_Cal->Fit("gaus","","",-16.0+i*2.0-0.125,-16.0+i*2.0+0.125);
    TF1* fitFuncGaus = h_Cathode_X_Cal->GetFunction("gaus");
    DifferenceX->SetPoint(i,-16.0+i*2.0,fitFuncGaus->GetParameter(1)-(-16.0+i*2.0));
    DifferenceX->SetPointError(i,0,fitFuncGaus->GetParameter(2));
  }

  TGraphErrors* DifferenceA = new TGraphErrors();
  DifferenceA->SetName("DifferenceA");
  DifferenceA->SetTitle("DifferenceA");
  for (int i=0;i<18;i++){
    h_Anode_Cal->Fit("gaus","","",-17.0+i*2.0-0.125,-17.0+i*2.0+0.125);
    TF1* fitFuncGaus = h_Anode_Cal->GetFunction("gaus");
    DifferenceA->SetPoint(i,-17.0+i*2.0,fitFuncGaus->GetParameter(1)-(-17.0+i*2.0));
    DifferenceA->SetPointError(i,0,fitFuncGaus->GetParameter(2));
  }
/*  for (int i=0;i<18;i++){
    double Ay = PrimitivePeaksA[i];
    DifferenceA->SetPoint(i,-17.0+i*2.0,FitParametersA[0]+FitParametersA[1]*Ay+FitParametersA[2]*pow(Ay,2.0)+FitParametersA[3]*pow(Ay,3.0)-(-17.0+i*2.0));
    DifferenceA->SetPointError(i,0,0);
  }*/

  /*******************************Gain Calibration*************************************************/
  TGraph2D *GainMapGraph = new TGraph2D();
  TGraph2D *GainMapChi2 = new TGraph2D();
  TCanvas* CellCanvas = new TCanvas("GainMap","GainMap",0,0,15000,16000);
  CellCanvas->Divide(17,18);
  //Fit [7][7] as the reference point
  CellCanvas->cd(8*17+8+1);
  CellSpec[8][8]->Rebin(8);
  int MaxBin = CellSpec[8][8]->GetMaximumBin();
  double Peak = CellSpec[8][8]->GetBinCenter(MaxBin);
  CellSpec[8][8]->Fit("gaus","","",Peak*0.8,Peak*1.2);
  TF1 * CellFitFunc = CellSpec[8][8]->GetFunction("gaus");
  double ReferenceGain = CellFitFunc->GetParameter(1);
  GainMap[8][8] = 1.0; 
  CellSpec[8][8]->Draw();
  for (int i=0;i<17;i++){
    for (int j=0;j<18;j++){
      if (i==8 && j==8){
	GainMapGraph->SetPoint(j*17+i,-16+i*2.0,-17+j*2.0,GainMap[i][j]);
	GainMapChi2->SetPoint(j*17+i,-16+i*2.0,-17+j*2.0,1.0);
	continue;
      }
      CellCanvas->cd(j*17+i+1);
      CellSpec[i][j]->Rebin(8);
      MaxBin = CellSpec[i][j]->GetMaximumBin();
      Peak = CellSpec[i][j]->GetBinCenter(MaxBin);
      TFitResultPtr r =  CellSpec[i][j]->Fit("gaus","","",Peak*0.8,Peak*1.2);
      CellFitFunc = CellSpec[i][j]->GetFunction("gaus");
      double Chi2 = CellFitFunc->GetChisquare();
      double NDF = CellFitFunc->GetNDF();
      if (NDF==0){
	Chi2=1;
      }else{
	Chi2/=double(NDF);
      }
      cout <<i<<" "<<j<<" Chi2 = "<<Chi2 <<endl;;
      //If failed, try to do it again, search for next maximum
      if (int(r)!=0){
	//Set the current maximum to zero
	for (int k=-2;k<=2;k++){
	  CellSpec[i][j]->SetBinContent(MaxBin+k,0.0);
	}
	MaxBin = CellSpec[i][j]->GetMaximumBin();
	Peak = CellSpec[i][j]->GetBinCenter(MaxBin);
	TFitResultPtr r =  CellSpec[i][j]->Fit("gaus","","",Peak*0.8,Peak*1.2);
	CellFitFunc = CellSpec[i][j]->GetFunction("gaus");
	Chi2 = CellFitFunc->GetChisquare();
	NDF = CellFitFunc->GetNDF();
	if (NDF==0){
	  Chi2=1;
	}else{
	  Chi2/=double(NDF);
	}
      }
      GainMap[i][j] = CellFitFunc->GetParameter(1)/ReferenceGain;
      GainMapGraph->SetPoint(j*17+i,-16+i*2.0,-17+j*2.0,GainMap[i][j]);
      GainMapChi2->SetPoint(j*17+i,-16+i*2.0,-17+j*2.0,Chi2);
      CellSpec[i][j]->Draw();
    }
  }
  string SpecFile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Spectra/SimMWPCCal") + string("_") + convert.str() + string("_GainMap.pdf(");
  CellCanvas->SaveAs(SpecFile.c_str());
  delete CellCanvas;
  TCanvas* CellCanvas1 = new TCanvas("GainMap1","GainMap1",0,0,15000,16000);
  GainMapGraph->SetName("GainMap");
  GainMapGraph->SetTitle("GainMap");
  GainMapGraph->GetXaxis()->SetTitle("X");
  GainMapGraph->GetYaxis()->SetTitle("Y");
  GainMapGraph->GetZaxis()->SetTitle("Relative Gain");
  GainMapGraph->Draw("surf1");

  SpecFile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Spectra/SimMWPCCal") + string("_") + convert.str() + string("_GainMap.pdf");
  CellCanvas1->SaveAs(SpecFile.c_str());
  GainMapChi2->SetName("Chi2");
  GainMapChi2->SetTitle("Chi2");
  GainMapChi2->Draw("surf1");

  SpecFile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Spectra/SimMWPCCal") + string("_") + convert.str() + string("_GainMap.pdf)");
  CellCanvas1->SaveAs(SpecFile.c_str());
  delete CellCanvas1;
  //Load Data again, correct the gain non-uniform
  int NCal = Gain.size();
  for (int i=0;i<NCal;i++){
    int ii = int((GainX[i]+17.0)/2.0);
    int jj = int((GainY[i]+18.0)/2.0);
    if (ii>=0 && ii<17 && jj>=0 && jj<18){
//      cout << GainX[i]  <<" "<<GainY[i] <<" "<<GainMap[ii][jj] <<" "<<CurrentAnalyzer->SimMWPCGainMap[ii][jj]<<endl;
      //double GainFactor = GainMap[ii][jj];
      double GainFactor = CurrentAnalyzer->SimMWPCGainMap[ii][jj];  //Using the loaded gain map, more reliable
      //Apply the fiducial cut, 16 mm as standard
      if (pow(GainX[i],2.0)+pow(GainY[i],2.0)<pow(16,2.0)){
	h_MWPCTot_Cal->Fill(Gain[i]/GainFactor/1.1);
	h_Charge_X_Cal->Fill(GainChargeX[i]/GainFactor*3.6);
	h_Charge_Y_Cal->Fill(GainChargeY[i]/GainFactor*3.6);
      }
    }
  } 
  //Write to gain Calibration file
  ofstream GainCalFileOut;
  sprintf(filename,"%s/SimMWPCGainMap%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  GainCalFileOut.open(filename,ios::out);
  for (int i=0;i<17;i++){
    for (int j=0;j<18;j++){
      GainCalFileOut<<-16.0+i*2.0<<" "<<-17.0+j*2.0<<" "<< GainMap[i][j]<<endl;
    }
  }
  GainCalFileOut.close();
  /********************************Calibrate the main peak*****************************************/
  int HistMax = h_MWPCTot_Cal->GetMaximumBin();
  double HistMaxE = h_MWPCTot_Cal->GetBinCenter(HistMax);
  h_MWPCTot_Cal->Fit("gaus","","",HistMaxE*0.93,HistMaxE*1.07);
  double APeakPos =h_MWPCTot_Cal->GetFunction("gaus")->GetParameter(1);
  //Fit the Ar escape peak
  h_MWPCTot_Cal->Fit("gaus","","",HistMaxE*0.5*0.9,HistMaxE*0.5*1.1);
  double APeakPos2 = h_MWPCTot_Cal->GetFunction("gaus")->GetParameter(1);
  HistMax = h_Charge_X_Cal->GetMaximumBin();
  HistMaxE = h_Charge_X_Cal->GetBinCenter(HistMax);
  h_Charge_X_Cal->Fit("gaus","","",HistMaxE*0.93,HistMaxE*1.07);
  double CPeakPos =h_Charge_X_Cal->GetFunction("gaus")->GetParameter(1);
  //Try linear fit
  double Scale = 2.9577/(APeakPos-APeakPos2);
  double Shift = (APeakPos*2.9373-APeakPos2*5.895)/(APeakPos-APeakPos2);
  ofstream ECalFileOut;
  sprintf(filename,"%s/SimMWPCECalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  ECalFileOut.open(filename,ios::out);
  cout <<"APeakPos1= "<<APeakPos<<endl;
  cout <<"APeakPos2= "<<APeakPos2<<endl;
  cout <<"CPeakPos= "<<CPeakPos<<endl;
  ECalFileOut<<5.895/APeakPos<<" "<<5.895/CPeakPos<<endl;
  ECalFileOut<<Scale<<" "<<Shift<<endl;	//New calibration method, still keep the old one for the moment
  ECalFileOut.close();
  //Load Data again,fine tune the gain 
  NCal = Gain.size();
  for (int i=0;i<NCal;i++){
    int ii = int((GainX[i]+17.0)/2.0);
    int jj = int((GainY[i]+18.0)/2.0);
    if (ii>=0 && ii<17 && jj>=0 && jj<18){
      //double GainFactor = GainMap[ii][jj];
      double GainFactor = CurrentAnalyzer->SimMWPCGainMap[ii][jj];  //Using the loaded gain map, more reliable
      //Apply the fiducial cut, 16 mm as standard
      if (pow(GainX[i],2.0)+pow(GainY[i],2.0)<pow(16,2.0)){
	double aux = Gain[i]/GainFactor/1.1;
	h_MWPCTot_Cal2->Fill(Scale*aux+Shift);
      }
    }
  } 
  HistMax = h_MWPCTot_Cal2->GetMaximumBin();
  HistMaxE = h_MWPCTot_Cal2->GetBinCenter(HistMax);
  double MaxVal = h_MWPCTot_Cal2->GetMaximum();
  TF1* fitfunc = new TF1("TriGaus","[0]*exp(-pow(x-[1],2.0)/pow([2],2.0)/2)+[3]*exp(-pow(x-[4],2.0)/pow([5],2.0)/2)+[3]*0.1168*exp(-pow(x-[4]*1.102,2.0)/pow([6],2.0)/2)",0,12);
  fitfunc->SetParameters(MaxVal*0.135,HistMaxE*0.5,1.0,MaxVal,HistMaxE,HistMaxE*0.13,HistMaxE*0.13);
  h_MWPCTot_Cal2->Fit(fitfunc);
  /************************************************************************************************/
  //Write to root file
  spectrumfile = CurrentAnalyzer->SIM_DATA_DIRECTORY + string("/../Histograms/") + "SimMWPCCal" + string("_") + convert.str() + string(".root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");
  h_MWPC1->Write();
  h_MWPC2->Write();
  h_MWPC_tot->Write();
  h_Charge_X->Write();
  h_Charge_Y->Write();
  h_Charge_C->Write();
  h_Charge_XY->Write();
  h_Charge_AC->Write();
  h_MWPC_Cond->Write();
  h_Anode_Pos->Write();
  h_Cathode_Image->Write();
  h_ACCorrelation->Write();
  h_Anode_Image->Write();
  h_FiredWireX->Write();
  h_FiredWireY->Write();

  h_Charge_X1->Write();
  h_Charge_X2->Write();
  h_Charge_X3->Write();
  h_Charge_X4->Write();
  h_Charge_X5->Write();
  h_Charge_X6->Write();
  h_Cathode_X->Write();
  h_Cathode_Y->Write();
  h_Cathode_X_cond2->Write();
  h_Cathode_Y_cond2->Write();
  h_Cathode_X_cond3->Write();
  h_Cathode_Y_cond3->Write();
  h_Cathode_X_cond4->Write();
  h_Cathode_Y_cond4->Write();
  h_Cathode_X_cond5->Write();
  h_Cathode_Y_cond5->Write();
  h_Cathode_X_cond6->Write();
  h_Cathode_Y_cond6->Write();
  h_Cathode_X_Group0->Write();
  h_Cathode_X_Group1->Write();
  h_Cathode_X_Group2->Write();
  h_Anode_Image_cond->Write();
  h_Cathode_Image_cond->Write();
  h_MWPC_Corr->Write();
  h_A1A2->Write();
  h_PosX_Charge->Write();
  h_PosY_Charge->Write();
  //Calibrations
  h_Cathode_Image_Cal->Write();
  h_ACCorrelation_Cal->Write();
  h_Anode_Image_Cal->Write();
  h_Anode_Cal->Write();
  h_Cathode_X_Cal->Write();
  h_Cathode_Y_Cal->Write();
  h_Cathode_X_cond3_Cal->Write();
  h_Cathode_X_cond4_Cal->Write();
  h_Cathode_X_cond5_Cal->Write();
  h_MWPCTot_Cal->Write();
  h_MWPCTot_Cal2->Write();
  h_Charge_X_Cal->Write();
  h_Charge_Y_Cal->Write();

  //Cathode Signal
  CathodeXSig->Write();

  DifferenceA->Write();
  DifferenceX->Write();
//  CalCathodeXGraph->Write();
//  CalCathodeYGraph->Write();
//  CalAnodeGraph->Write();
  AGraph->Write();
  XGraph0->Write();
  for (int i=0;i<3;i++){
    XGraph1[i]->Write();
//    fitFuncX1[i]->Write();
  }
  for (int i=0;i<4;i++){
    XGraph2[i]->Write();
//    fitFuncX2[i]->Write();
  }
  GainMapGraph->Write();
  GainMapChi2->Write();
  for (int i=0;i<17;i++){
    for (int j=0;j<18;j++){
      CellSpec[i][j]->Write();
    }
  }
  histfile->Close();
  delete GainMapGraph;
  delete GainMapChi2;
  delete AGraph;
  delete XGraph0;
  for (int i=0;i<3;i++){
    delete XGraph1[i];
  }
  for (int i=0;i<4;i++){
    delete XGraph2[i];
  }
  for (int i=0;i<17;i++){
    for (int j=0;j<18;j++){
      delete CellSpec[i][j];
    }
  }
  delete DifferenceA;
  delete DifferenceX;
//  delete[] XGraph1;
//  delete[] XGraph2;
  CurrentAnalyzer->Calibrating = false;
  return 0;
}

/********************************************************************/
void CountTrigger(DataStruct_MWPC_X MWPC_X, DataStruct_MWPC_Y MWPC_Y, int & CountX, int & CountY, double CathodeThreshold)
{
  int NFireX=0;
  int NFireY=0;
  if (MWPC_X.AX1>CathodeThreshold) NFireX++;
  else MWPC_X.AX1=0;
  if (MWPC_X.AX2>CathodeThreshold) NFireX++;
  else MWPC_X.AX2=0;
  if (MWPC_X.AX3>CathodeThreshold) NFireX++;
  else MWPC_X.AX3=0;
  if (MWPC_X.AX4>CathodeThreshold) NFireX++;
  else MWPC_X.AX4=0;
  if (MWPC_X.AX5>CathodeThreshold) NFireX++;
  else MWPC_X.AX5=0;
  if (MWPC_X.AX6>CathodeThreshold) NFireX++;
  else MWPC_X.AX6=0;
  if (MWPC_Y.AY1>CathodeThreshold) NFireY++;
  else MWPC_Y.AY1=0;
  if (MWPC_Y.AY2>CathodeThreshold) NFireY++;
  else MWPC_Y.AY2=0;
  if (MWPC_Y.AY3>CathodeThreshold) NFireY++;
  else MWPC_Y.AY3=0;
  if (MWPC_Y.AY4>CathodeThreshold) NFireY++;
  else MWPC_Y.AY4=0;
  if (MWPC_Y.AY5>CathodeThreshold) NFireY++;
  else MWPC_Y.AY5=0;
  if (MWPC_Y.AY6>CathodeThreshold) NFireY++;
  else MWPC_Y.AY6=0;
  CountX=NFireX;
  CountY=NFireY;
}
/********************************************************************/
int ProcessMWPCData(double AMWPC_anode1,double AMWPC_anode2,DataStruct_MWPC_X MWPC_X, DataStruct_MWPC_Y MWPC_Y, double & AnodePos, double & MWPC_PosX, double & MWPC_PosY, double & MWPC_ChargeX, double & MWPC_ChargeY)
{
  double AnodeCharge = AMWPC_anode1+AMWPC_anode2;
  MWPC_ChargeX = MWPC_X.AX1+MWPC_X.AX2+MWPC_X.AX3+MWPC_X.AX4+MWPC_X.AX5+MWPC_X.AX6;
  MWPC_ChargeY = MWPC_Y.AY1+MWPC_Y.AY2+MWPC_Y.AY3+MWPC_Y.AY4+MWPC_Y.AY5+MWPC_Y.AY6;
  if (AnodeCharge>0.0 && MWPC_ChargeX>0.0 && MWPC_ChargeY>0.0){
    AnodePos = -21.0*(AMWPC_anode1-AMWPC_anode2)/(AMWPC_anode1+AMWPC_anode2);
    MWPC_PosX = (-20.*MWPC_X.AX1 -12.*MWPC_X.AX2-4.*MWPC_X.AX3+4.*MWPC_X.AX4+12.*MWPC_X.AX5+20.*MWPC_X.AX6)/MWPC_ChargeX;
    MWPC_PosY = -(-20.*MWPC_Y.AY1 -12.*MWPC_Y.AY2-4.*MWPC_Y.AY3+4.*MWPC_Y.AY4+12.*MWPC_Y.AY5+20.*MWPC_Y.AY6)/MWPC_ChargeY;
  }else{
    AnodePos = MWPC_PosX = MWPC_PosY = -500;
    return -1;
  }
  return 0;
}
/********************************************************************/
int ConstructCorrectedMWPCPosPol(double AMWPC_anode1,double AMWPC_anode2,DataStruct_MWPC_X MWPC_X, DataStruct_MWPC_Y MWPC_Y, double & AnodePos, double & MWPC_PosX, double & MWPC_PosY,double **Xsig,double **Ysig,double FitParametersA[],double FitParametersX0[],double FitParametersX1[][4],double FitParametersX2[][4],bool Keep, double CathodeThreshold)
{
  //Count # of fired wires
  int TriggerMultiplicity = 5;
  //Set Smallest ones to zero, according to trigger multiplicity
  for (int j=TriggerMultiplicity;j<6;j++){
    *(Xsig[j])=0;
  }
  int NFireX=0;
  int NFireY=0;
  CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
  //Process MWPC Data, signal to position and charge
  double MWPC_ChargeX;
  double MWPC_ChargeY;
  int returnval = ProcessMWPCData(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
  if (returnval==-1){
    MWPC_PosX=MWPC_PosY=AnodePos=-500;
    return -1;
  }

  double xx;
  if (NFireX==5){
    //	MWPC_PosX = FitParametersX0[0]+FitParametersX0[1]*MWPC_PosX+FitParametersX0[2]*pow(MWPC_PosX,2.0)+FitParametersX0[3]*pow(MWPC_PosX,3.0);
    if (MWPC_PosX>-9.0 && MWPC_PosX<9.0){
      MWPC_PosX = FitParametersX0[0]+FitParametersX0[1]*MWPC_PosX+FitParametersX0[2]*pow(MWPC_PosX,2.0)+FitParametersX0[3]*pow(MWPC_PosX,3.0);
    }else{
      //Downgrade to 4-trigger
      *(Xsig[4])=0;
      NFireX--;
      MWPC_ChargeX = MWPC_X.AX1+MWPC_X.AX2+MWPC_X.AX3+MWPC_X.AX4+MWPC_X.AX5+MWPC_X.AX6;
      MWPC_PosX = (-20.*MWPC_X.AX1 -12.*MWPC_X.AX2-4.*MWPC_X.AX3+4.*MWPC_X.AX4+12.*MWPC_X.AX5+20.*MWPC_X.AX6)/MWPC_ChargeX;
    }
  }
  if (NFireX==4){
    if (MWPC_PosX>-13.0 && MWPC_PosX<13.0){
      if (MWPC_PosX<=-4.0){
	xx = MWPC_PosX + 8.0;
	MWPC_PosX = FitParametersX1[0][0]+FitParametersX1[0][1]*xx+FitParametersX1[0][2]*pow(xx,2.0)+FitParametersX1[0][3]*pow(xx,3.0) - 8.0;
	//	  if (MWPC_PosX>-3.875)MWPC_PosX = -3.875;
      }
      //	if (MWPC_PosX>-4.1875 && MWPC_PosX<=-3.75) MWPC_PosX = -4.0;
      if (MWPC_PosX>-4.0 && MWPC_PosX<=4.0){
	MWPC_PosX = FitParametersX1[1][0]+FitParametersX1[1][1]*MWPC_PosX+FitParametersX1[1][2]*pow(MWPC_PosX,2.0)+FitParametersX1[1][3]*pow(MWPC_PosX,3.0);
	//	  if (MWPC_PosX<-4.125)MWPC_PosX = -4.125;
	//	  if (MWPC_PosX>4.0625)MWPC_PosX = 4.0;
      }
      //	if (MWPC_PosX>3.8125 && MWPC_PosX<=4.25) MWPC_PosX = 4.0;
      if (MWPC_PosX>4.0){
	xx = MWPC_PosX - 8.0;
	MWPC_PosX = FitParametersX1[2][0]+FitParametersX1[2][1]*xx+FitParametersX1[2][2]*pow(xx,2.0)+FitParametersX1[2][3]*pow(xx,3.0) + 8.0;
	//	  MWPC_PosX = 22.0;
	//	  if (MWPC_PosX<4.0)MWPC_PosX = 4.0;
      }
    }else{
      //Downgrade to 3-trigger
      *(Xsig[3])=0;
      NFireX--;
      MWPC_ChargeX = MWPC_X.AX1+MWPC_X.AX2+MWPC_X.AX3+MWPC_X.AX4+MWPC_X.AX5+MWPC_X.AX6;
      MWPC_PosX = (-20.*MWPC_X.AX1 -12.*MWPC_X.AX2-4.*MWPC_X.AX3+4.*MWPC_X.AX4+12.*MWPC_X.AX5+20.*MWPC_X.AX6)/MWPC_ChargeX;
    }
  }
  if (NFireX==3){
    if (MWPC_PosX<=-8.0){
      xx = MWPC_PosX + 12.0;
      MWPC_PosX = FitParametersX2[0][0]+FitParametersX2[0][1]*xx+FitParametersX2[0][2]*pow(xx,2.0)+FitParametersX2[0][3]*pow(xx,3.0) - 12.0;
      //	  if (MWPC_PosX>-8.0)MWPC_PosX = -8.0;
    }
    //	if (MWPC_PosX>-8.75 && MWPC_PosX<=-7.375) MWPC_PosX = -8.0;
    if (MWPC_PosX>-8.0 && MWPC_PosX<=0.0){
      xx = MWPC_PosX + 4.0;
      MWPC_PosX = FitParametersX2[1][0]+FitParametersX2[1][1]*xx+FitParametersX2[1][2]*pow(xx,2.0)+FitParametersX2[1][3]*pow(xx,3.0) - 4.0;
      //	  if (MWPC_PosX<-8.0)MWPC_PosX = -8.0;
      //	  if (MWPC_PosX>0.0)MWPC_PosX = 0.0;
    }
    //	if (MWPC_PosX>-0.75 && MWPC_PosX<=0.6875) MWPC_PosX = 4.0;
    if (MWPC_PosX>0.0 && MWPC_PosX<=8.0){
      xx = MWPC_PosX - 4.0;
      MWPC_PosX = FitParametersX2[2][0]+FitParametersX2[2][1]*xx+FitParametersX2[2][2]*pow(xx,2.0)+FitParametersX2[2][3]*pow(xx,3.0) + 4.0;
      //	  if (MWPC_PosX<0.0)MWPC_PosX = 0.0;
      //	  if (MWPC_PosX>8.0)MWPC_PosX = 8.0;
    }
    //	if (MWPC_PosX>7.3125 && MWPC_PosX<=8.75) MWPC_PosX = 4.0;
    if (MWPC_PosX>8.0){
      xx = MWPC_PosX - 12.0;
      MWPC_PosX = FitParametersX2[3][0]+FitParametersX2[3][1]*xx+FitParametersX2[3][2]*pow(xx,2.0)+FitParametersX2[3][3]*pow(xx,3.0) + 12.0;
      //	  if (MWPC_PosX<8.0)MWPC_PosX = 8.0;
    }
  }
  if (NFireX<3){
    if(Keep){
      //No corrections for these cases
      MWPC_PosX = (-20.*MWPC_X.AX1 -12.*MWPC_X.AX2-4.*MWPC_X.AX3+4.*MWPC_X.AX4+12.*MWPC_X.AX5+20.*MWPC_X.AX6)/MWPC_ChargeX;
    }else{
      //Through away these events
      MWPC_PosX=-500;
    }
  }
  //no corrections for Cathode Y
  MWPC_PosY = -(-20.*MWPC_Y.AY1 -12.*MWPC_Y.AY2-4.*MWPC_Y.AY3+4.*MWPC_Y.AY4+12.*MWPC_Y.AY5+20.*MWPC_Y.AY6)/MWPC_ChargeY;
  //Calibrate Anode Position
  AnodePos = -21.0*(AMWPC_anode1-AMWPC_anode2)/(AMWPC_anode1+AMWPC_anode2);
  AnodePos = FitParametersA[0]+FitParametersA[1]*AnodePos+FitParametersA[2]*pow(AnodePos,2.0)+FitParametersA[3]*pow(AnodePos,3.0);
  return 0;
}
/********************************************************************/
int ConstructCorrectedMWPCPosMicro(double AMWPC_anode1,double AMWPC_anode2,DataStruct_MWPC_X MWPC_X, DataStruct_MWPC_Y MWPC_Y, double & AnodePos, double & MWPC_PosX, double & MWPC_PosY,double **Xsig,double **Ysig,double PeaksX0[],double PeaksX1[],double PeaksX2[],double PeaksA[],double ExactPos0[],double ExactPos1[],double ExactPos2[],double ExactPosA[],bool Keep, double CathodeThreshold)
{
  //Count # of fired wires
  int TriggerMultiplicity = 5;
  //Set Smallest ones to zero, according to trigger multiplicity
  for (int j=TriggerMultiplicity;j<6;j++){
    *(Xsig[j])=0;
  }
  int NFireX=0;
  int NFireY=0;
  CountTrigger(MWPC_X,MWPC_Y,NFireX,NFireY,CathodeThreshold);
  //Process MWPC Data, signal to position and charge
  double MWPC_ChargeX;
  double MWPC_ChargeY;
  int returnval = ProcessMWPCData(AMWPC_anode1,AMWPC_anode2,MWPC_X,MWPC_Y,AnodePos,MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY);
  if (returnval==-1){
    MWPC_PosX=MWPC_PosY=AnodePos=-500;
    return -1;
  }

  double xx;
  int Index;
  if (NFireX==5){
    //	MWPC_PosX = FitParametersX0[0]+FitParametersX0[1]*MWPC_PosX+FitParametersX0[2]*pow(MWPC_PosX,2.0)+FitParametersX0[3]*pow(MWPC_PosX,3.0);
    //if (MWPC_PosX>-7.875 && MWPC_PosX<7.9375){
    if (MWPC_PosX>PeaksX0[0] && MWPC_PosX<PeaksX0[8]){
      xx = MWPC_PosX;
      Index = int((xx+8.0)/2.0);
      if (Index<0)Index=0;
      while(1){
	if (xx<PeaksX0[Index])Index--;
	if (xx>PeaksX0[Index+1])Index++;
	if (xx>=PeaksX0[Index] && xx<=PeaksX0[Index+1])break;
	if (Index<0 || Index>8){
	  cout << "Search peaks out of range! Index = "<<Index<<endl;
	  return -1;
	}
      }
      MWPC_PosX = ExactPos0[Index]+(ExactPos0[Index+1]-ExactPos0[Index])*(xx-PeaksX0[Index])/(PeaksX0[Index+1]-PeaksX0[Index]);
    }else{
      //Downgrade to 4-trigger
      *(Xsig[4])=0;
      NFireX--;
      MWPC_ChargeX = MWPC_X.AX1+MWPC_X.AX2+MWPC_X.AX3+MWPC_X.AX4+MWPC_X.AX5+MWPC_X.AX6;
      MWPC_PosX = (-20.*MWPC_X.AX1 -12.*MWPC_X.AX2-4.*MWPC_X.AX3+4.*MWPC_X.AX4+12.*MWPC_X.AX5+20.*MWPC_X.AX6)/MWPC_ChargeX;
    }
  }
  if (NFireX==4){
    //	  if (MWPC_PosX>-11.6875 && MWPC_PosX<11.75){
    if (MWPC_PosX>PeaksX1[0] && MWPC_PosX<PeaksX1[14]){
      xx = MWPC_PosX;
      Index = int((xx+12.0)/2.0);
      if (Index<0)Index=0;
      while(1){
	if (xx<PeaksX1[Index])Index--;
	if (xx>PeaksX1[Index+1])Index++;
	if (xx>=PeaksX1[Index] && xx<=PeaksX1[Index+1])break;
	if (Index<0 || Index>14){
	  cout << "Search peaks out of range! Index = "<<Index<<endl;
	  return -1;
	}
      }
      if (ExactPos1[Index+1]-ExactPos1[Index]>0){
	MWPC_PosX = ExactPos1[Index]+(ExactPos1[Index+1]-ExactPos1[Index])*(xx-PeaksX1[Index])/(PeaksX1[Index+1]-PeaksX1[Index]);
      }else{
	if (xx <= ExactPos1[Index]){
	  MWPC_PosX = ExactPos1[Index]+0.125*(xx-PeaksX1[Index])/(ExactPos1[Index]-PeaksX1[Index]);
	}
	if (xx > ExactPos1[Index]){
	  MWPC_PosX = ExactPos1[Index]-0.125*(PeaksX1[Index+1]-xx)/(PeaksX1[Index+1]-ExactPos1[Index]);
	}
      }
    }else{
      //Downgrade to 3-trigger
      *(Xsig[3])=0;
      NFireX--;
      MWPC_ChargeX = MWPC_X.AX1+MWPC_X.AX2+MWPC_X.AX3+MWPC_X.AX4+MWPC_X.AX5+MWPC_X.AX6;
      MWPC_PosX = (-20.*MWPC_X.AX1 -12.*MWPC_X.AX2-4.*MWPC_X.AX3+4.*MWPC_X.AX4+12.*MWPC_X.AX5+20.*MWPC_X.AX6)/MWPC_ChargeX;
    }
  }
  if (NFireX==3){
    //if (MWPC_PosX>-15.3125 && MWPC_PosX<15.3125){
    if (MWPC_PosX>PeaksX2[0] && MWPC_PosX<PeaksX2[19]){
      xx = MWPC_PosX;
      Index = int((xx+16.0)/2.0);
      if (Index<0)Index=0;
      while(1){
	if (xx<PeaksX2[Index])Index--;
	if (xx>PeaksX2[Index+1])Index++;
	if (xx>=PeaksX2[Index] && xx<=PeaksX2[Index+1])break;
	if (Index<0 || Index>19){
	  cout << "Search peaks out of range! Index = "<<Index<<endl;
	  return -1;
	}
      }
      if (ExactPos2[Index+1]-ExactPos2[Index]>0){
	MWPC_PosX = ExactPos2[Index]+(ExactPos2[Index+1]-ExactPos2[Index])*(xx-PeaksX2[Index])/(PeaksX2[Index+1]-PeaksX2[Index]);
      }else{
	if (xx <= ExactPos2[Index]){
	  MWPC_PosX = ExactPos2[Index]+0.125*(xx-PeaksX2[Index])/(ExactPos2[Index]-PeaksX2[Index]);
	}
	if (xx > ExactPos2[Index]){
	  MWPC_PosX = ExactPos2[Index]-0.125*(PeaksX2[Index+1]-xx)/(PeaksX2[Index+1]-ExactPos2[Index]);
	}
      }
    }else{
      //If out of range, treat it as low trigger
      if(Keep){
	//No corrections for these cases
	MWPC_PosX = (-20.*MWPC_X.AX1 -12.*MWPC_X.AX2-4.*MWPC_X.AX3+4.*MWPC_X.AX4+12.*MWPC_X.AX5+20.*MWPC_X.AX6)/MWPC_ChargeX;
      }else{
	//Through away these events
	MWPC_PosX=-500;
      }
    }
  }
  if (NFireX<3){
    if(Keep){
      //No corrections for these cases
      MWPC_PosX = (-20.*MWPC_X.AX1 -12.*MWPC_X.AX2-4.*MWPC_X.AX3+4.*MWPC_X.AX4+12.*MWPC_X.AX5+20.*MWPC_X.AX6)/MWPC_ChargeX;
    }else{
      //Through away these events
      MWPC_PosX=-500;
    }
  }
  //no corrections for Cathode Y
//  MWPC_PosY = -(-20.*MWPC_Y.AY1 -12.*MWPC_Y.AY2-4.*MWPC_Y.AY3+4.*MWPC_Y.AY4+12.*MWPC_Y.AY5+20.*MWPC_Y.AY6)/MWPC_ChargeY;
  //Already calculated 

  //Calibrate Anode Position
  //AnodePos = -21.0*(AMWPC_anode1-AMWPC_anode2)/(AMWPC_anode1+AMWPC_anode2);
  //  AnodePos = FitParametersA[0]+FitParametersA[1]*AnodePos+FitParametersA[2]*pow(AnodePos,2.0)+FitParametersA[3]*pow(AnodePos,3.0);
  if (AnodePos>PeaksA[0] && AnodePos<PeaksA[17]){
    xx = AnodePos;
    Index = int((xx+17.0)/2.0);
    if (Index<0)Index=0;
    while(1){
      if (xx<PeaksA[Index])Index--;
      if (xx>PeaksA[Index+1])Index++;
      if (xx>=PeaksA[Index] && xx<=PeaksA[Index+1])break;
      if (Index<0 || Index>17){
	cout << "Search peaks out of range! Index = "<<Index<<endl;
	return -1;
      }
    }
    AnodePos = ExactPosA[Index]+(ExactPosA[Index+1]-ExactPosA[Index])*(xx-PeaksA[Index])/(PeaksA[Index+1]-PeaksA[Index]);
  }else{
    //If out of range, treat it as low trigger
    if(Keep){
      //No corrections for these cases
      AnodePos = -21.0*(AMWPC_anode1-AMWPC_anode2)/(AMWPC_anode1+AMWPC_anode2);
    }else{
      //Through away these events
      AnodePos=-500;
    }
  }
  return 0;
}
  /********************************************************************/
