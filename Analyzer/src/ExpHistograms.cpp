//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>
#include <stdint.h>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
//Analyzer includes
#include "Analyzer.h"

using namespace std;

//Globals
extern int TOF_High;
extern int TOF_Low;
extern int TOF_N;

/***********************************************************************************/
int Analyzer::InitExpHistBox()
{
  ExpHistBox.clear();
  //Scint_EA
  ExpHistBox["Scint_EA"].Title = "Scint_EA";
  ExpHistBox["Scint_EA"].Dim = 1;
  ExpHistBox["Scint_EA"].MultiSlice = false;
  ExpHistBox["Scint_EA"].XTitle = "E_Scint_A [keV]";
  ExpHistBox["Scint_EA"].YTitle = "Counts";
  ExpHistBox["Scint_EA"].ZTitle = "";
  ExpHistBox["Scint_EA"].XBinNum = 400;
  ExpHistBox["Scint_EA"].XRange.low = 0.0;
  ExpHistBox["Scint_EA"].XRange.high = 8000.0;
  ExpHistBox["Scint_EA"].YBinNum = 0;
  ExpHistBox["Scint_EA"].YRange.low = 0.0;
  ExpHistBox["Scint_EA"].YRange.high = 0.0;
  ExpHistBox["Scint_EA"].Hist1DList = NULL;
  ExpHistBox["Scint_EA"].Hist2DList = NULL;
  ExpHistBox["Scint_EA"].Hist1DListSliced = NULL;
  ExpHistBox["Scint_EA"].Enabled = false;
  //Scint_EDA
  ExpHistBox["Scint_EDA"].Title = "Scint_EDA";
  ExpHistBox["Scint_EDA"].Dim = 2;
  ExpHistBox["Scint_EDA"].MultiSlice = false;
  ExpHistBox["Scint_EDA"].XTitle = "E_Scint_D [keV]";
  ExpHistBox["Scint_EDA"].YTitle = "E_Scint_A [keV]";
  ExpHistBox["Scint_EDA"].ZTitle = "Counts";
  ExpHistBox["Scint_EDA"].XBinNum = 400;
  ExpHistBox["Scint_EDA"].XRange.low = 0.0;
  ExpHistBox["Scint_EDA"].XRange.high = 8000.0;
  ExpHistBox["Scint_EDA"].YBinNum = 400;
  ExpHistBox["Scint_EDA"].YRange.low = 0.0;
  ExpHistBox["Scint_EDA"].YRange.high = 8000.0;
  ExpHistBox["Scint_EDA"].Hist1DList = NULL;
  ExpHistBox["Scint_EDA"].Hist2DList = NULL;
  ExpHistBox["Scint_EDA"].Hist1DListSliced = NULL;
  ExpHistBox["Scint_EDA"].Enabled = false;
  //Scint_Time
  ExpHistBox["Scint_Time"].Title = "Scint_Time";
  ExpHistBox["Scint_Time"].Dim = 1;
  ExpHistBox["Scint_Time"].MultiSlice = false;
  ExpHistBox["Scint_Time"].XTitle = "Scint_Time [ms]";
  ExpHistBox["Scint_Time"].YTitle = "Counts";
  ExpHistBox["Scint_Time"].ZTitle = "";
  ExpHistBox["Scint_Time"].XBinNum = 2000;
  ExpHistBox["Scint_Time"].XRange.low = 0.0;
  ExpHistBox["Scint_Time"].XRange.high = 20000.0;
  ExpHistBox["Scint_Time"].YBinNum = 0;
  ExpHistBox["Scint_Time"].YRange.low = 0.0;
  ExpHistBox["Scint_Time"].YRange.high = 0.0;
  ExpHistBox["Scint_Time"].Hist1DList = NULL;
  ExpHistBox["Scint_Time"].Hist2DList = NULL;
  ExpHistBox["Scint_Time"].Hist1DListSliced = NULL;
  ExpHistBox["Scint_Time"].Enabled = false;
  //MWPC_Anode
  ExpHistBox["MWPC_Anode"].Title = "MWPC_Anode";
  ExpHistBox["MWPC_Anode"].Dim = 1;
  ExpHistBox["MWPC_Anode"].MultiSlice = false;
  ExpHistBox["MWPC_Anode"].XTitle = "E_MWPC [keV]";
  ExpHistBox["MWPC_Anode"].YTitle = "Counts";
  ExpHistBox["MWPC_Anode"].ZTitle = "";
  ExpHistBox["MWPC_Anode"].XBinNum = 600;
  ExpHistBox["MWPC_Anode"].XRange.low = 0.0;
  ExpHistBox["MWPC_Anode"].XRange.high = 30.0;
  ExpHistBox["MWPC_Anode"].YBinNum = 0;
  ExpHistBox["MWPC_Anode"].YRange.low = 0.0;
  ExpHistBox["MWPC_Anode"].YRange.high = 0.0;
  ExpHistBox["MWPC_Anode"].Hist1DList = NULL;
  ExpHistBox["MWPC_Anode"].Hist2DList = NULL;
  ExpHistBox["MWPC_Anode"].Hist1DListSliced = NULL;
  ExpHistBox["MWPC_Anode"].Enabled = false;
  //Beta_Time_diff
  ExpHistBox["Beta_Time_diff"].Title = "Beta_Time_diff";
  ExpHistBox["Beta_Time_diff"].Dim = 1;
  ExpHistBox["Beta_Time_diff"].MultiSlice = false;
  ExpHistBox["Beta_Time_diff"].XTitle = "TMWPC-TScint [ns]";
  ExpHistBox["Beta_Time_diff"].YTitle = "Counts";
  ExpHistBox["Beta_Time_diff"].ZTitle = "";
  ExpHistBox["Beta_Time_diff"].XBinNum = 400;
  ExpHistBox["Beta_Time_diff"].XRange.low = -500.0;
  ExpHistBox["Beta_Time_diff"].XRange.high = 500.0;
  ExpHistBox["Beta_Time_diff"].YBinNum = 0;
  ExpHistBox["Beta_Time_diff"].YRange.low = 0.0;
  ExpHistBox["Beta_Time_diff"].YRange.high = 0.0;
  ExpHistBox["Beta_Time_diff"].Hist1DList = NULL;
  ExpHistBox["Beta_Time_diff"].Hist2DList = NULL;
  ExpHistBox["Beta_Time_diff"].Hist1DListSliced = NULL;
  ExpHistBox["Beta_Time_diff"].Enabled = false;
  //MWPC_Cathode_Anode
  ExpHistBox["MWPC_Cathode_Anode"].Title = "MWPC_Cathode_Anode";
  ExpHistBox["MWPC_Cathode_Anode"].Dim = 2;
  ExpHistBox["MWPC_Cathode_Anode"].MultiSlice = false;
  ExpHistBox["MWPC_Cathode_Anode"].XTitle = "E_MWPC_Cathode [keV]";
  ExpHistBox["MWPC_Cathode_Anode"].YTitle = "E_MWPC_Anode [keV]";
  ExpHistBox["MWPC_Cathode_Anode"].ZTitle = "";
  ExpHistBox["MWPC_Cathode_Anode"].XBinNum = 128;
  ExpHistBox["MWPC_Cathode_Anode"].XRange.low = 0.0;
  ExpHistBox["MWPC_Cathode_Anode"].XRange.high = 30.0;
  ExpHistBox["MWPC_Cathode_Anode"].YBinNum = 128;
  ExpHistBox["MWPC_Cathode_Anode"].YRange.low = 0.0;
  ExpHistBox["MWPC_Cathode_Anode"].YRange.high = 30.0;
  ExpHistBox["MWPC_Cathode_Anode"].Hist1DList = NULL;
  ExpHistBox["MWPC_Cathode_Anode"].Hist2DList = NULL;
  ExpHistBox["MWPC_Cathode_Anode"].Hist1DListSliced = NULL;
  ExpHistBox["MWPC_Cathode_Anode"].Enabled = false;
  //MWPC_Cathode_XY
  ExpHistBox["MWPC_Cathode_XY"].Title = "MWPC_Cathode_XY";
  ExpHistBox["MWPC_Cathode_XY"].Dim = 2;
  ExpHistBox["MWPC_Cathode_XY"].MultiSlice = false;
  ExpHistBox["MWPC_Cathode_XY"].XTitle = "E_MWPC_Cathode_X [keV]";
  ExpHistBox["MWPC_Cathode_XY"].YTitle = "E_MWPC_Cathode_Y [keV]";
  ExpHistBox["MWPC_Cathode_XY"].ZTitle = "Counts";
  ExpHistBox["MWPC_Cathode_XY"].XBinNum = 128;
  ExpHistBox["MWPC_Cathode_XY"].XRange.low = 0.0;
  ExpHistBox["MWPC_Cathode_XY"].XRange.high = 30.0;
  ExpHistBox["MWPC_Cathode_XY"].YBinNum = 128;
  ExpHistBox["MWPC_Cathode_XY"].YRange.low = 0.0;
  ExpHistBox["MWPC_Cathode_XY"].YRange.high = 30.0;
  ExpHistBox["MWPC_Cathode_XY"].Hist1DList = NULL;
  ExpHistBox["MWPC_Cathode_XY"].Hist2DList = NULL;
  ExpHistBox["MWPC_Cathode_XY"].Hist1DListSliced = NULL;
  ExpHistBox["MWPC_Cathode_XY"].Enabled = false;
  //MWPC_Vs_Scint
  ExpHistBox["MWPC_vs_Scint"].Title = "MWPC_vs_Scint";
  ExpHistBox["MWPC_vs_Scint"].Dim = 2;
  ExpHistBox["MWPC_vs_Scint"].MultiSlice = false;
  ExpHistBox["MWPC_vs_Scint"].XTitle = "MWPC_E [keV]";
  ExpHistBox["MWPC_vs_Scint"].YTitle = "Scint_E [keV]";
  ExpHistBox["MWPC_vs_Scint"].ZTitle = "Counts";
  ExpHistBox["MWPC_vs_Scint"].XBinNum = 128;
  ExpHistBox["MWPC_vs_Scint"].XRange.low = 0.0;
  ExpHistBox["MWPC_vs_Scint"].XRange.high = 30.0;
  ExpHistBox["MWPC_vs_Scint"].YBinNum = 128;
  ExpHistBox["MWPC_vs_Scint"].YRange.low = 0.0;
  ExpHistBox["MWPC_vs_Scint"].YRange.high = 4000.0;
  ExpHistBox["MWPC_vs_Scint"].Hist1DList = NULL;
  ExpHistBox["MWPC_vs_Scint"].Hist2DList = NULL;
  ExpHistBox["MWPC_vs_Scint"].Hist1DListSliced = NULL;
  ExpHistBox["MWPC_vs_Scint"].Enabled = false;
  //MWPC_Image
  ExpHistBox["MWPC_Image"].Title = "MWPC_Image";
  ExpHistBox["MWPC_Image"].Dim = 2;
  ExpHistBox["MWPC_Image"].MultiSlice = false;
  ExpHistBox["MWPC_Image"].XTitle = "X [mm]";
  ExpHistBox["MWPC_Image"].YTitle = "Y [mm]";
  ExpHistBox["MWPC_Image"].ZTitle = "Counts";
  ExpHistBox["MWPC_Image"].XBinNum = 200;
  ExpHistBox["MWPC_Image"].XRange.low = -25.0;
  ExpHistBox["MWPC_Image"].XRange.high = 25.0;
  ExpHistBox["MWPC_Image"].YBinNum = 200;
  ExpHistBox["MWPC_Image"].YRange.low = -25.0;
  ExpHistBox["MWPC_Image"].YRange.high = 25.0;
  ExpHistBox["MWPC_Image"].Hist1DList = NULL;
  ExpHistBox["MWPC_Image"].Hist2DList = NULL;
  ExpHistBox["MWPC_Image"].Hist1DListSliced = NULL;
  ExpHistBox["MWPC_Image"].Enabled = false;
  //MWPC_Cathode-Anode_YCorrelation
  ExpHistBox["MWPC_CAY"].Title = "MWPC_CAY";
  ExpHistBox["MWPC_CAY"].Dim = 2;
  ExpHistBox["MWPC_CAY"].MultiSlice = false;
  ExpHistBox["MWPC_CAY"].XTitle = "CY [mm]";
  ExpHistBox["MWPC_CAY"].YTitle = "AY [mm]";
  ExpHistBox["MWPC_CAY"].ZTitle = "Counts";
  ExpHistBox["MWPC_CAY"].XBinNum = 200;
  ExpHistBox["MWPC_CAY"].XRange.low = -25.0;
  ExpHistBox["MWPC_CAY"].XRange.high = 25.0;
  ExpHistBox["MWPC_CAY"].YBinNum = 200;
  ExpHistBox["MWPC_CAY"].YRange.low = -25.0;
  ExpHistBox["MWPC_CAY"].YRange.high = 25.0;
  ExpHistBox["MWPC_CAY"].Hist1DList = NULL;
  ExpHistBox["MWPC_CAY"].Hist2DList = NULL;
  ExpHistBox["MWPC_CAY"].Hist1DListSliced = NULL;
  ExpHistBox["MWPC_CAY"].Enabled = false;
  //MCP_TX1vQX1
  ExpHistBox["MCP_TX1vQX1"].Title = "MCP_TX1vQX1";
  ExpHistBox["MCP_TX1vQX1"].Dim = 2;
  ExpHistBox["MCP_TX1vQX1"].MultiSlice = false;
  ExpHistBox["MCP_TX1vQX1"].XTitle = "QX1 [au]";
  ExpHistBox["MCP_TX1vQX1"].YTitle = "TX1 [ns]";
  ExpHistBox["MCP_TX1vQX1"].ZTitle = "";
  ExpHistBox["MCP_TX1vQX1"].XBinNum = 400;
  ExpHistBox["MCP_TX1vQX1"].XRange.low = 0.0;
  ExpHistBox["MCP_TX1vQX1"].XRange.high = 16000.0;
  ExpHistBox["MCP_TX1vQX1"].YBinNum = 400;
  ExpHistBox["MCP_TX1vQX1"].YRange.low = 34.0;
  ExpHistBox["MCP_TX1vQX1"].YRange.high = 48.0;
  ExpHistBox["MCP_TX1vQX1"].Hist1DList = NULL;
  ExpHistBox["MCP_TX1vQX1"].Hist2DList = NULL;
  ExpHistBox["MCP_TX1vQX1"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_TX1vQX1"].Enabled = false;
  //MCP_TX2vQX2
  ExpHistBox["MCP_TX2vQX2"].Title = "MCP_TX2vQX2";
  ExpHistBox["MCP_TX2vQX2"].Dim = 2;
  ExpHistBox["MCP_TX2vQX2"].MultiSlice = false;
  ExpHistBox["MCP_TX2vQX2"].XTitle = "QX2 [au]";
  ExpHistBox["MCP_TX2vQX2"].YTitle = "TX2 [ns]";
  ExpHistBox["MCP_TX2vQX2"].ZTitle = "";
  ExpHistBox["MCP_TX2vQX2"].XBinNum = 400;
  ExpHistBox["MCP_TX2vQX2"].XRange.low = 0.0;
  ExpHistBox["MCP_TX2vQX2"].XRange.high = 16000.0;
  ExpHistBox["MCP_TX2vQX2"].YBinNum = 400;
  ExpHistBox["MCP_TX2vQX2"].YRange.low = 34.0;
  ExpHistBox["MCP_TX2vQX2"].YRange.high = 48.0;
  ExpHistBox["MCP_TX2vQX2"].Hist1DList = NULL;
  ExpHistBox["MCP_TX2vQX2"].Hist2DList = NULL;
  ExpHistBox["MCP_TX2vQX2"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_TX2vQX2"].Enabled = false;
  //MCP_TY1vQY1
  ExpHistBox["MCP_TY1vQY1"].Title = "MCP_TY1vQY1";
  ExpHistBox["MCP_TY1vQY1"].Dim = 2;
  ExpHistBox["MCP_TY1vQY1"].MultiSlice = false;
  ExpHistBox["MCP_TY1vQY1"].XTitle = "QY1 [au]";
  ExpHistBox["MCP_TY1vQY1"].YTitle = "TY1 [ns]";
  ExpHistBox["MCP_TY1vQY1"].ZTitle = "";
  ExpHistBox["MCP_TY1vQY1"].XBinNum = 400;
  ExpHistBox["MCP_TY1vQY1"].XRange.low = 0.0;
  ExpHistBox["MCP_TY1vQY1"].XRange.high = 16000.0;
  ExpHistBox["MCP_TY1vQY1"].YBinNum = 400;
  ExpHistBox["MCP_TY1vQY1"].YRange.low = 34.0;
  ExpHistBox["MCP_TY1vQY1"].YRange.high = 48.0;
  ExpHistBox["MCP_TY1vQY1"].Hist1DList = NULL;
  ExpHistBox["MCP_TY1vQY1"].Hist2DList = NULL;
  ExpHistBox["MCP_TY1vQY1"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_TY1vQY1"].Enabled = false;
  //MCP_TY2vQY2
  ExpHistBox["MCP_TY2vQY2"].Title = "MCP_TY2vQY2";
  ExpHistBox["MCP_TY2vQY2"].Dim = 2;
  ExpHistBox["MCP_TY2vQY2"].MultiSlice = false;
  ExpHistBox["MCP_TY2vQY2"].XTitle = "QY2 [au]";
  ExpHistBox["MCP_TY2vQY2"].YTitle = "TY2 [ns]";
  ExpHistBox["MCP_TY2vQY2"].ZTitle = "";
  ExpHistBox["MCP_TY2vQY2"].XBinNum = 400;
  ExpHistBox["MCP_TY2vQY2"].XRange.low = 0.0;
  ExpHistBox["MCP_TY2vQY2"].XRange.high = 16000.0;
  ExpHistBox["MCP_TY2vQY2"].YBinNum = 400;
  ExpHistBox["MCP_TY2vQY2"].YRange.low = 34.0;
  ExpHistBox["MCP_TY2vQY2"].YRange.high = 48.0;
  ExpHistBox["MCP_TY2vQY2"].Hist1DList = NULL;
  ExpHistBox["MCP_TY2vQY2"].Hist2DList = NULL;
  ExpHistBox["MCP_TY2vQY2"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_TY2vQY2"].Enabled = false;
  //MCP_TX1pTX2
  ExpHistBox["MCP_TX1pTX2"].Title = "MCP_TX1pTX2";
  ExpHistBox["MCP_TX1pTX2"].Dim = 1;
  ExpHistBox["MCP_TX1pTX2"].MultiSlice = false;
  ExpHistBox["MCP_TX1pTX2"].XTitle = "TX1+TX2 [ns]";
  ExpHistBox["MCP_TX1pTX2"].YTitle = "Counts";
  ExpHistBox["MCP_TX1pTX2"].ZTitle = "";
  ExpHistBox["MCP_TX1pTX2"].XBinNum = 400;
  ExpHistBox["MCP_TX1pTX2"].XRange.low = 0.0;
  ExpHistBox["MCP_TX1pTX2"].XRange.high = 100.0;
  ExpHistBox["MCP_TX1pTX2"].YBinNum = 0;
  ExpHistBox["MCP_TX1pTX2"].YRange.low = 0.0;
  ExpHistBox["MCP_TX1pTX2"].YRange.high = 0.0;
  ExpHistBox["MCP_TX1pTX2"].Hist1DList = NULL;
  ExpHistBox["MCP_TX1pTX2"].Hist2DList = NULL;
  ExpHistBox["MCP_TX1pTX2"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_TX1pTX2"].Enabled = false;
  //MCP_TXSumPos
  ExpHistBox["MCP_TXSumPos"].Title = "MCP_TXSumPos";
  ExpHistBox["MCP_TXSumPos"].Dim = 2;
  ExpHistBox["MCP_TXSumPos"].MultiSlice = false;
  ExpHistBox["MCP_TXSumPos"].XTitle = "TX1+TX2 [ns]";
  ExpHistBox["MCP_TXSumPos"].YTitle = "X [mm]";
  ExpHistBox["MCP_TXSumPos"].ZTitle = "Count";
  ExpHistBox["MCP_TXSumPos"].XBinNum = 400;
  ExpHistBox["MCP_TXSumPos"].XRange.low = 0.0;
  ExpHistBox["MCP_TXSumPos"].XRange.high = 100.0;
  ExpHistBox["MCP_TXSumPos"].YBinNum = 100;
  ExpHistBox["MCP_TXSumPos"].YRange.low = -40.0;
  ExpHistBox["MCP_TXSumPos"].YRange.high = 40.0;
  ExpHistBox["MCP_TXSumPos"].Hist1DList = NULL;
  ExpHistBox["MCP_TXSumPos"].Hist2DList = NULL;
  ExpHistBox["MCP_TXSumPos"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_TXSumPos"].Enabled = false;
  //MCP_TY1pTY2
  ExpHistBox["MCP_TY1pTY2"].Title = "MCP_TY1pTY2";
  ExpHistBox["MCP_TY1pTY2"].Dim = 1;
  ExpHistBox["MCP_TY1pTY2"].MultiSlice = false;
  ExpHistBox["MCP_TY1pTY2"].XTitle = "TY1+TY2 [ns]";
  ExpHistBox["MCP_TY1pTY2"].YTitle = "Counts";
  ExpHistBox["MCP_TY1pTY2"].ZTitle = "";
  ExpHistBox["MCP_TY1pTY2"].XBinNum = 400;
  ExpHistBox["MCP_TY1pTY2"].XRange.low = 0.0;
  ExpHistBox["MCP_TY1pTY2"].XRange.high = 100.0;
  ExpHistBox["MCP_TY1pTY2"].YBinNum = 0;
  ExpHistBox["MCP_TY1pTY2"].YRange.low = 0.0;
  ExpHistBox["MCP_TY1pTY2"].YRange.high = 0.0;
  ExpHistBox["MCP_TY1pTY2"].Hist1DList = NULL;
  ExpHistBox["MCP_TY1pTY2"].Hist2DList = NULL;
  ExpHistBox["MCP_TY1pTY2"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_TY1pTY2"].Enabled = false;
  //MCP_TYSumPos
  ExpHistBox["MCP_TYSumPos"].Title = "MCP_TYSumPos";
  ExpHistBox["MCP_TYSumPos"].Dim = 2;
  ExpHistBox["MCP_TYSumPos"].MultiSlice = false;
  ExpHistBox["MCP_TYSumPos"].XTitle = "TY1+TY2 [ns]";
  ExpHistBox["MCP_TYSumPos"].YTitle = "Y [mm]";
  ExpHistBox["MCP_TYSumPos"].ZTitle = "Count";
  ExpHistBox["MCP_TYSumPos"].XBinNum = 400;
  ExpHistBox["MCP_TYSumPos"].XRange.low = 0.0;
  ExpHistBox["MCP_TYSumPos"].XRange.high = 100.0;
  ExpHistBox["MCP_TYSumPos"].YBinNum = 100;
  ExpHistBox["MCP_TYSumPos"].YRange.low = -40.0;
  ExpHistBox["MCP_TYSumPos"].YRange.high = 40.0;
  ExpHistBox["MCP_TYSumPos"].Hist1DList = NULL;
  ExpHistBox["MCP_TYSumPos"].Hist2DList = NULL;
  ExpHistBox["MCP_TYSumPos"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_TYSumPos"].Enabled = false;
  //MCP_Image
  ExpHistBox["MCP_Image"].Title = "MCP_Image";
  ExpHistBox["MCP_Image"].Dim = 2;
  ExpHistBox["MCP_Image"].MultiSlice = false;
  ExpHistBox["MCP_Image"].XTitle = "X [mm]";
  ExpHistBox["MCP_Image"].YTitle = "Y [mm]";
  ExpHistBox["MCP_Image"].ZTitle = "Counts";
  ExpHistBox["MCP_Image"].XBinNum = 400;
  ExpHistBox["MCP_Image"].XRange.low = -40.0;
  ExpHistBox["MCP_Image"].XRange.high = 40.0;
  ExpHistBox["MCP_Image"].YBinNum = 400;
  ExpHistBox["MCP_Image"].YRange.low = -40.0;
  ExpHistBox["MCP_Image"].YRange.high = 40.0;
  ExpHistBox["MCP_Image"].Hist1DList = NULL;
  ExpHistBox["MCP_Image"].Hist2DList = NULL;
  ExpHistBox["MCP_Image"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_Image"].Enabled = false;
  //MCP_Image_Zoom
  ExpHistBox["MCP_Image_Zoom"].Title = "MCP_Image_Zoom";
  ExpHistBox["MCP_Image_Zoom"].Dim = 2;
  ExpHistBox["MCP_Image_Zoom"].MultiSlice = false;
  ExpHistBox["MCP_Image_Zoom"].XTitle = "X [mm]";
  ExpHistBox["MCP_Image_Zoom"].YTitle = "Y [mm]";
  ExpHistBox["MCP_Image_Zoom"].ZTitle = "Counts";
  ExpHistBox["MCP_Image_Zoom"].XBinNum = 100;
  ExpHistBox["MCP_Image_Zoom"].XRange.low = -5.0;
  ExpHistBox["MCP_Image_Zoom"].XRange.high = 5.0;
  ExpHistBox["MCP_Image_Zoom"].YBinNum = 100;
  ExpHistBox["MCP_Image_Zoom"].YRange.low = -5.0;
  ExpHistBox["MCP_Image_Zoom"].YRange.high = 5.0;
  ExpHistBox["MCP_Image_Zoom"].Hist1DList = NULL;
  ExpHistBox["MCP_Image_Zoom"].Hist2DList = NULL;
  ExpHistBox["MCP_Image_Zoom"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_Image_Zoom"].Enabled = false;
  //MCP_R
  ExpHistBox["MCP_R"].Title = "MCP_R";
  ExpHistBox["MCP_R"].Dim = 1;
  ExpHistBox["MCP_R"].MultiSlice = false;
  ExpHistBox["MCP_R"].XTitle = "R [mm]";
  ExpHistBox["MCP_R"].YTitle = "Counts";
  ExpHistBox["MCP_R"].ZTitle = "";
  ExpHistBox["MCP_R"].XBinNum = 400;
  ExpHistBox["MCP_R"].XRange.low = 0.0;
  ExpHistBox["MCP_R"].XRange.high = 40.0;
  ExpHistBox["MCP_R"].YBinNum = 0;
  ExpHistBox["MCP_R"].YRange.low = 0.0;
  ExpHistBox["MCP_R"].YRange.high = 0.0;
  ExpHistBox["MCP_R"].Hist1DList = NULL;
  ExpHistBox["MCP_R"].Hist2DList = NULL;
  ExpHistBox["MCP_R"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_R"].Enabled = false;
  //MCP_Charge
  ExpHistBox["MCP_Charge"].Title = "MCP_Charge";
  ExpHistBox["MCP_Charge"].Dim = 1;
  ExpHistBox["MCP_Charge"].MultiSlice = false;
  ExpHistBox["MCP_Charge"].XTitle = "MCP_Charge [QDC_amp]";
  ExpHistBox["MCP_Charge"].YTitle = "Counts";
  ExpHistBox["MCP_Charge"].ZTitle = "";
  ExpHistBox["MCP_Charge"].XBinNum = 400;
  ExpHistBox["MCP_Charge"].XRange.low = 0.0;
  ExpHistBox["MCP_Charge"].XRange.high = 160000.0;
  ExpHistBox["MCP_Charge"].YBinNum = 0;
  ExpHistBox["MCP_Charge"].YRange.low = 0.0;
  ExpHistBox["MCP_Charge"].YRange.high = 0.0;
  ExpHistBox["MCP_Charge"].Hist1DList = NULL;
  ExpHistBox["MCP_Charge"].Hist2DList = NULL;
  ExpHistBox["MCP_Charge"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_Charge"].Enabled = false;
  //MCP_Time
  ExpHistBox["MCP_Time"].Title = "MCP_Time";
  ExpHistBox["MCP_Time"].Dim = 1;
  ExpHistBox["MCP_Time"].MultiSlice = false;
  ExpHistBox["MCP_Time"].XTitle = "MCP_Time [ms]";
  ExpHistBox["MCP_Time"].YTitle = "Counts";
  ExpHistBox["MCP_Time"].ZTitle = "";
  ExpHistBox["MCP_Time"].XBinNum = 500;
  ExpHistBox["MCP_Time"].XRange.low = 0.0;
  ExpHistBox["MCP_Time"].XRange.high = 250.0;
  ExpHistBox["MCP_Time"].YBinNum = 0;
  ExpHistBox["MCP_Time"].YRange.low = 0.0;
  ExpHistBox["MCP_Time"].YRange.high = 0.0;
  ExpHistBox["MCP_Time"].Hist1DList = NULL;
  ExpHistBox["MCP_Time"].Hist2DList = NULL;
  ExpHistBox["MCP_Time"].Hist1DListSliced = NULL;
  ExpHistBox["MCP_Time"].Enabled = false;
  //TOFHist
  ExpHistBox["TOFHist"].Title = "TOFHist";
  ExpHistBox["TOFHist"].Dim = 1;
  ExpHistBox["TOFHist"].MultiSlice = false;
  ExpHistBox["TOFHist"].XTitle = "TOF [ns]";
  ExpHistBox["TOFHist"].YTitle = "Counts";
  ExpHistBox["TOFHist"].ZTitle = "";
  ExpHistBox["TOFHist"].XBinNum = TOF_N;
  ExpHistBox["TOFHist"].XRange.low = TOF_Low;
  ExpHistBox["TOFHist"].XRange.high = TOF_High;
  ExpHistBox["TOFHist"].YBinNum = 0;
  ExpHistBox["TOFHist"].YRange.low = 0.0;
  ExpHistBox["TOFHist"].YRange.high = 0.0;
  ExpHistBox["TOFHist"].Hist1DList = NULL;
  ExpHistBox["TOFHist"].Hist2DList = NULL;
  ExpHistBox["TOFHist"].Hist1DListSliced = NULL;
  ExpHistBox["TOFHist"].Enabled = false;
  //TOF_vs_Scint
  ExpHistBox["TOF_vs_Scint"].Title = "TOF_vs_Scint";
  ExpHistBox["TOF_vs_Scint"].Dim = 2;
  ExpHistBox["TOF_vs_Scint"].MultiSlice = false;
  ExpHistBox["TOF_vs_Scint"].XTitle = "TOF [ns]";
  ExpHistBox["TOF_vs_Scint"].YTitle = "E_Scint [keV]";
  ExpHistBox["TOF_vs_Scint"].ZTitle = "Counts";
  ExpHistBox["TOF_vs_Scint"].XBinNum = TOF_N;
  ExpHistBox["TOF_vs_Scint"].XRange.low = TOF_Low;
  ExpHistBox["TOF_vs_Scint"].XRange.high = TOF_High;
  ExpHistBox["TOF_vs_Scint"].YBinNum = 200;
  ExpHistBox["TOF_vs_Scint"].YRange.low = 0.0;
  ExpHistBox["TOF_vs_Scint"].YRange.high = 4000.0;
  ExpHistBox["TOF_vs_Scint"].Hist1DList = NULL;
  ExpHistBox["TOF_vs_Scint"].Hist2DList = NULL;
  ExpHistBox["TOF_vs_Scint"].Hist1DListSliced = NULL;
  ExpHistBox["TOF_vs_Scint"].Enabled = false;
  //TOF_vs_QMCP
  ExpHistBox["TOF_vs_QMCP"].Title = "TOF_vs_QMCP";
  ExpHistBox["TOF_vs_QMCP"].Dim = 2;
  ExpHistBox["TOF_vs_QMCP"].MultiSlice = false;
  ExpHistBox["TOF_vs_QMCP"].XTitle = "TOF [ns]";
  ExpHistBox["TOF_vs_QMCP"].YTitle = "E_QMCP [keV]";
  ExpHistBox["TOF_vs_QMCP"].ZTitle = "Counts";
  ExpHistBox["TOF_vs_QMCP"].XBinNum = TOF_N;
  ExpHistBox["TOF_vs_QMCP"].XRange.low = TOF_Low;
  ExpHistBox["TOF_vs_QMCP"].XRange.high = TOF_High;
  ExpHistBox["TOF_vs_QMCP"].YBinNum = 100;
  ExpHistBox["TOF_vs_QMCP"].YRange.low = 0.0;
  ExpHistBox["TOF_vs_QMCP"].YRange.high = 160000.0;
  ExpHistBox["TOF_vs_QMCP"].Hist1DList = NULL;
  ExpHistBox["TOF_vs_QMCP"].Hist2DList = NULL;
  ExpHistBox["TOF_vs_QMCP"].Hist1DListSliced = NULL;
  ExpHistBox["TOF_vs_QMCP"].Enabled = false;

  //N2_Charge
  ExpHistBox["N2_Charge"].Title = "N2_Charge";
  ExpHistBox["N2_Charge"].Dim = 1;
  ExpHistBox["N2_Charge"].MultiSlice = false;
  ExpHistBox["N2_Charge"].XTitle = "N2_Charge [QDC_amp]";
  ExpHistBox["N2_Charge"].YTitle = "Counts";
  ExpHistBox["N2_Charge"].ZTitle = "";
  ExpHistBox["N2_Charge"].XBinNum = 400;
  ExpHistBox["N2_Charge"].XRange.low = 0.0;
  ExpHistBox["N2_Charge"].XRange.high = 160000.0;
  ExpHistBox["N2_Charge"].YBinNum = 0;
  ExpHistBox["N2_Charge"].YRange.low = 0.0;
  ExpHistBox["N2_Charge"].YRange.high = 0.0;
  ExpHistBox["N2_Charge"].Hist1DList = NULL;
  ExpHistBox["N2_Charge"].Hist2DList = NULL;
  ExpHistBox["N2_Charge"].Hist1DListSliced = NULL;
  ExpHistBox["N2_Charge"].Enabled = false;
  //N2_Time
  ExpHistBox["N2_Time"].Title = "N2_Time";
  ExpHistBox["N2_Time"].Dim = 1;
  ExpHistBox["N2_Time"].MultiSlice = false;
  ExpHistBox["N2_Time"].XTitle = "N2_Time [s]";
  ExpHistBox["N2_Time"].YTitle = "Counts";
  ExpHistBox["N2_Time"].ZTitle = "";
  ExpHistBox["N2_Time"].XBinNum = 28800;
  ExpHistBox["N2_Time"].XRange.low = 0.0;
  ExpHistBox["N2_Time"].XRange.high = 7200.0;
  ExpHistBox["N2_Time"].YBinNum = 0;
  ExpHistBox["N2_Time"].YRange.low = 0.0;
  ExpHistBox["N2_Time"].YRange.high = 0.0;
  ExpHistBox["N2_Time"].Hist1DList = NULL;
  ExpHistBox["N2_Time"].Hist2DList = NULL;
  ExpHistBox["N2_Time"].Hist1DListSliced = NULL;
  ExpHistBox["N2_Time"].Enabled = false;

  //Q value for charge 1 and 2
  ExpHistBox["Q_Values"].Title = "Q_Values";
  ExpHistBox["Q_Values"].Dim = 2;
  ExpHistBox["Q_Values"].MultiSlice = false;
  ExpHistBox["Q_Values"].XTitle = "Q1 [keV]";
  ExpHistBox["Q_Values"].YTitle = "Q2 [keV]";
  ExpHistBox["Q_Values"].ZTitle = "Counts";
  ExpHistBox["Q_Values"].XBinNum = 500;
  ExpHistBox["Q_Values"].XRange.low = 0.0;
  ExpHistBox["Q_Values"].XRange.high = 12000;
  ExpHistBox["Q_Values"].YBinNum = 500;
  ExpHistBox["Q_Values"].YRange.low = 0.0;
  ExpHistBox["Q_Values"].YRange.high = 12000;
  ExpHistBox["Q_Values"].Hist1DList = NULL;
  ExpHistBox["Q_Values"].Hist2DList = NULL;
  ExpHistBox["Q_Values"].Hist1DListSliced = NULL;
  ExpHistBox["Q_Values"].Enabled = false;
  //Q value for charge 2
/*  ExpHistBox["Q_Value2"].Title = "Q_Value2";
  ExpHistBox["Q_Value2"].Dim = 1;
  ExpHistBox["Q_Value2"].MultiSlice = false;
  ExpHistBox["Q_Value2"].XTitle = "Q [keV]";
  ExpHistBox["Q_Value2"].YTitle = "Counts";
  ExpHistBox["Q_Value2"].ZTitle = "";
  ExpHistBox["Q_Value2"].XBinNum = 500;
  ExpHistBox["Q_Value2"].XRange.low = 0.0;
  ExpHistBox["Q_Value2"].XRange.high = 5000;
  ExpHistBox["Q_Value2"].YBinNum = 0;
  ExpHistBox["Q_Value2"].YRange.low = 0.0;
  ExpHistBox["Q_Value2"].YRange.high = 0.0;
  ExpHistBox["Q_Value2"].Hist1DList = NULL;
  ExpHistBox["Q_Value2"].Hist2DList = NULL;
  ExpHistBox["Q_Value2"].Hist1DListSliced = NULL;
  ExpHistBox["Q_Value2"].Enabled = false;*/
  //E-Nu Correlation
  ExpHistBox["CosThetaENu"].Title = "E-Nu Correlation";
  ExpHistBox["CosThetaENu"].Dim = 1;
  ExpHistBox["CosThetaENu"].MultiSlice = false;
  ExpHistBox["CosThetaENu"].XTitle = "CosThetaENu";
  ExpHistBox["CosThetaENu"].YTitle = "Counts";
  ExpHistBox["CosThetaENu"].ZTitle = "";
  ExpHistBox["CosThetaENu"].XBinNum = 400;
  ExpHistBox["CosThetaENu"].XRange.low = -1.0;
  ExpHistBox["CosThetaENu"].XRange.high = 1.0;
  ExpHistBox["CosThetaENu"].YBinNum = 0;
  ExpHistBox["CosThetaENu"].YRange.low = 0.0;
  ExpHistBox["CosThetaENu"].YRange.high = 0.0;
  ExpHistBox["CosThetaENu"].Hist1DList = NULL;
  ExpHistBox["CosThetaENu"].Hist2DList = NULL;
  ExpHistBox["CosThetaENu"].Hist1DListSliced = NULL;
  ExpHistBox["CosThetaENu"].Enabled = false;

  //EIon1 Correlation
  ExpHistBox["EIon1"].Title = "EIon1";
  ExpHistBox["EIon1"].Dim = 1;
  ExpHistBox["EIon1"].MultiSlice = false;
  ExpHistBox["EIon1"].XTitle = "EIon1 [keV]";
  ExpHistBox["EIon1"].YTitle = "Counts";
  ExpHistBox["EIon1"].ZTitle = "";
  ExpHistBox["EIon1"].XBinNum = 400;
  ExpHistBox["EIon1"].XRange.low = 0.0;
  ExpHistBox["EIon1"].XRange.high = 1.6;
  ExpHistBox["EIon1"].YBinNum = 0;
  ExpHistBox["EIon1"].YRange.low = 0.0;
  ExpHistBox["EIon1"].YRange.high = 0.0;
  ExpHistBox["EIon1"].Hist1DList = NULL;
  ExpHistBox["EIon1"].Hist2DList = NULL;
  ExpHistBox["EIon1"].Hist1DListSliced = NULL;
  ExpHistBox["EIon1"].Enabled = false;

  //EIon2 Correlation
  ExpHistBox["EIon2"].Title = "EIon2";
  ExpHistBox["EIon2"].Dim = 1;
  ExpHistBox["EIon2"].MultiSlice = false;
  ExpHistBox["EIon2"].XTitle = "EIon2 [keV]";
  ExpHistBox["EIon2"].YTitle = "Counts";
  ExpHistBox["EIon2"].ZTitle = "";
  ExpHistBox["EIon2"].XBinNum = 400;
  ExpHistBox["EIon2"].XRange.low = 0.0;
  ExpHistBox["EIon2"].XRange.high = 1.6;
  ExpHistBox["EIon2"].YBinNum = 0;
  ExpHistBox["EIon2"].YRange.low = 0.0;
  ExpHistBox["EIon2"].YRange.high = 0.0;
  ExpHistBox["EIon2"].Hist1DList = NULL;
  ExpHistBox["EIon2"].Hist2DList = NULL;
  ExpHistBox["EIon2"].Hist1DListSliced = NULL;
  ExpHistBox["EIon2"].Enabled = false;
  
  //Group Time (group rate vs time)
  ExpHistBox["GroupTime"].Title = "GroupTime";
  ExpHistBox["GroupTime"].Dim = 1;
  ExpHistBox["GroupTime"].MultiSlice = false;
  ExpHistBox["GroupTime"].XTitle = "GroupTime [s]";
  ExpHistBox["GroupTime"].YTitle = "Counts";
  ExpHistBox["GroupTime"].ZTitle = "";
  ExpHistBox["GroupTime"].XBinNum = 40000;
  ExpHistBox["GroupTime"].XRange.low = 0.0;
  ExpHistBox["GroupTime"].XRange.high = 20000.0;
  ExpHistBox["GroupTime"].YBinNum = 0;
  ExpHistBox["GroupTime"].YRange.low = 0.0;
  ExpHistBox["GroupTime"].YRange.high = 0.0;
  ExpHistBox["GroupTime"].Hist1DList = NULL;
  ExpHistBox["GroupTime"].Hist2DList = NULL;
  ExpHistBox["GroupTime"].Hist1DListSliced = NULL;
  ExpHistBox["GroupTime"].Enabled = false;

  //Copy this map to bkg histogram conatainer
  BkgHistBox = ExpHistBox;
  //Copy these maps also to combined histogram containers
  CombExpHistBox = ExpHistBox;
  CombBkgHistBox = BkgHistBox;
  return 0;
}

/***********List allowed Histograms**************/
int Analyzer::LsExpHist()
{ 
  map <string,HistUnit>::iterator it;
  for (it=ExpHistBox.begin();it!=ExpHistBox.end();it++){
    cout << it->first<< " Dimension: " << it->second.Dim<<endl;
  }
  return 0;
}
/***********************************************************************************/
int Analyzer::InitExpFillFunctions(){
  if(ExpHistBox["Scint_EA"].Enabled) ExpFillFunctions["Scint_EA"] = &Analyzer::FillScint_EA;
  if(ExpHistBox["Scint_EDA"].Enabled) ExpFillFunctions["Scint_EDA"] = &Analyzer::FillScint_EDA;
  if(ExpHistBox["Scint_Time"].Enabled) ExpFillFunctions["Scint_Time"] = &Analyzer::FillScint_Time;
  if(ExpHistBox["MWPC_Anode"].Enabled) ExpFillFunctions["MWPC_Anode"] = &Analyzer::FillMWPC_Anode;
  if(ExpHistBox["Beta_Time_diff"].Enabled) ExpFillFunctions["Beta_Time_diff"] = &Analyzer::FillBeta_Time_diff;
  if(ExpHistBox["MWPC_Cathode_Anode"].Enabled) ExpFillFunctions["MWPC_Cathode_Anode"] = &Analyzer::FillMWPC_Cathode_Anode;
  if(ExpHistBox["MWPC_Cathode_XY"].Enabled) ExpFillFunctions["MWPC_Cathode_XY"] = &Analyzer::FillMWPC_Cathode_XY;
  if(ExpHistBox["MWPC_vs_Scint"].Enabled) ExpFillFunctions["MWPC_vs_Scint"] = &Analyzer::FillMWPC_vs_Scint;
  if(ExpHistBox["MWPC_Image"].Enabled) ExpFillFunctions["MWPC_Image"] = &Analyzer::FillMWPC_Image;
  if(ExpHistBox["MWPC_CAY"].Enabled) ExpFillFunctions["MWPC_CAY"] = &Analyzer::FillMWPC_CAY;
  if(ExpHistBox["MCP_TX1vQX1"].Enabled) ExpFillFunctions["MCP_TX1vQX1"] = &Analyzer::FillMCP_TX1vQX1;
  if(ExpHistBox["MCP_TX2vQX2"].Enabled) ExpFillFunctions["MCP_TX2vQX2"] = &Analyzer::FillMCP_TX2vQX2;
  if(ExpHistBox["MCP_TY1vQY1"].Enabled) ExpFillFunctions["MCP_TY1vQY1"] = &Analyzer::FillMCP_TY1vQY1;
  if(ExpHistBox["MCP_TY2vQY2"].Enabled) ExpFillFunctions["MCP_TY2vQY2"] = &Analyzer::FillMCP_TY2vQY2;
  if(ExpHistBox["MCP_TX1pTX2"].Enabled) ExpFillFunctions["MCP_TX1pTX2"] = &Analyzer::FillMCP_TX1pTX2;
  if(ExpHistBox["MCP_TXSumPos"].Enabled) ExpFillFunctions["MCP_TXSumPos"] = &Analyzer::FillMCP_TXSumPos;
  if(ExpHistBox["MCP_TY1pTY2"].Enabled) ExpFillFunctions["MCP_TY1pTY2"] = &Analyzer::FillMCP_TY1pTY2;
  if(ExpHistBox["MCP_TYSumPos"].Enabled) ExpFillFunctions["MCP_TYSumPos"] = &Analyzer::FillMCP_TYSumPos;
  if(ExpHistBox["MCP_Image"].Enabled) ExpFillFunctions["MCP_Image"] = &Analyzer::FillMCP_Image;
  if(ExpHistBox["MCP_Image_Zoom"].Enabled) ExpFillFunctions["MCP_Image_Zoom"] = &Analyzer::FillMCP_Image_Zoom;
  if(ExpHistBox["MCP_R"].Enabled) ExpFillFunctions["MCP_R"] = &Analyzer::FillMCP_R;
  if(ExpHistBox["MCP_Time"].Enabled) ExpFillFunctions["MCP_Time"] = &Analyzer::FillMCP_Time;
  if(ExpHistBox["MCP_Charge"].Enabled) ExpFillFunctions["MCP_Charge"] = &Analyzer::FillMCP_Charge;
  if(ExpHistBox["TOFHist"].Enabled) ExpFillFunctions["TOFHist"] = &Analyzer::FillTOFHist;
  if(ExpHistBox["TOF_vs_Scint"].Enabled) ExpFillFunctions["TOF_vs_Scint"] = &Analyzer::FillTOF_vs_Scint;
  if(ExpHistBox["TOF_vs_QMCP"].Enabled) ExpFillFunctions["TOF_vs_QMCP"] = &Analyzer::FillTOF_vs_QMCP;
  if(ExpHistBox["N2_Time"].Enabled) ExpFillFunctions["N2_Time"] = &Analyzer::FillN2_Time;
  if(ExpHistBox["N2_Charge"].Enabled) ExpFillFunctions["N2_Charge"] = &Analyzer::FillN2_Charge;
  //if(ExpHistBox["N2_TOFHist"].Enabled) ExpFillFunctions["N2_TOFHist"] = &Analyzer::FillN2_TOFHist;  
  if(ExpHistBox["Q_Values"].Enabled) ExpFillFunctions["Q_Values"] = &Analyzer::FillQ_Values;
  if(ExpHistBox["CosThetaENu"].Enabled) ExpFillFunctions["CosThetaENu"] = &Analyzer::FillCosThetaENu;
  if(ExpHistBox["EIon1"].Enabled) ExpFillFunctions["EIon1"] = &Analyzer::FillEIon1;
  if(ExpHistBox["EIon2"].Enabled) ExpFillFunctions["EIon2"] = &Analyzer::FillEIon2;
  if(ExpHistBox["GroupTime"].Enabled) ExpFillFunctions["GroupTime"] = &Analyzer::FillGroupTime;


  return 0;
}
/***********************************************************************************/
void Analyzer::InitToNullExp(){
  //Initialize histograms
  map <string,HistUnit>::iterator it;
  for (it = ExpHistBox.begin();it != ExpHistBox.end();++it){
    if((it->second).Dim==1){
      (it->second).Hist1DList = new TH1**[N_OF_HIST];
      for(int i=0;i<N_OF_HIST;i++){
	(it->second).Hist1DList[i] = new TH1*[N_OF_HIST];
	for(int j=0;j<N_OF_HIST;j++){
	  (it->second).Hist1DList[i][j] = NULL;
	}
      }
      if((it->second).MultiSlice){
	(it->second).Hist1DListSliced = new TH1***[N_OF_HIST];
	for(int i=0;i<N_OF_HIST;i++){
	  (it->second).Hist1DListSliced[i] = new TH1**[N_OF_HIST];
	  for(int j=0;j<N_OF_HIST;j++){
	    (it->second).Hist1DListSliced[i][j] = new TH1*[N_OF_SLICE];
	    for(int k=0;k<N_OF_SLICE;k++){
	      (it->second).Hist1DListSliced[i][j][k] = NULL;
	    }
	  }
	}
      }
    }else if((it->second).Dim==2){
      (it->second).Hist2DList = new TH2**[N_OF_HIST];
      for(int i=0;i<N_OF_HIST;i++){
	(it->second).Hist2DList[i] = new TH2*[N_OF_HIST];
	for(int j=0;j<N_OF_HIST;j++){
	  (it->second).Hist2DList[i][j] = NULL;
	}
      }
    }
  }

  for (it = BkgHistBox.begin();it != BkgHistBox.end();++it){
    if((it->second).Dim==1){
      (it->second).Hist1DList = new TH1**[N_OF_HIST];
      for(int i=0;i<N_OF_HIST;i++){
	(it->second).Hist1DList[i] = new TH1*[N_OF_HIST];
	for(int j=0;j<N_OF_HIST;j++){
	  (it->second).Hist1DList[i][j] = NULL;
	}
      }
      if((it->second).MultiSlice){
	(it->second).Hist1DListSliced = new TH1***[N_OF_HIST];
	for(int i=0;i<N_OF_HIST;i++){
	  (it->second).Hist1DListSliced[i] = new TH1**[N_OF_HIST];
	  for(int j=0;j<N_OF_HIST;j++){
	    (it->second).Hist1DListSliced[i][j] = new TH1*[N_OF_SLICE];
	    for(int k=0;k<N_OF_SLICE;k++){
	      (it->second).Hist1DListSliced[i][j][k] = NULL;
	    }
	  }
	}
      }
    }else if((it->second).Dim==2){
      (it->second).Hist2DList = new TH2**[N_OF_HIST];
      for(int i=0;i<N_OF_HIST;i++){
	(it->second).Hist2DList[i] = new TH2*[N_OF_HIST];
	for(int j=0;j<N_OF_HIST;j++){
	  (it->second).Hist2DList[i][j] = NULL;
	}
      }
    }
  }
}

/***********************************************************************************/
void Analyzer::InitToNullCombExp(){
  DelCombExpHistograms();
  //Initialize histograms
  map <string,HistUnit>::iterator it;
  for (it = CombExpHistBox.begin();it != CombExpHistBox.end();++it){
    if((it->second).Dim==1){
      (it->second).Hist1DList = new TH1**[1];
      for(int i=0;i<1;i++){
	(it->second).Hist1DList[i] = new TH1*[N_OF_HIST];
	for(int j=0;j<N_OF_HIST;j++){
	  (it->second).Hist1DList[i][j] = NULL;
	}
      }
      if((it->second).MultiSlice){
	(it->second).Hist1DListSliced = new TH1***[1];
	for(int i=0;i<1;i++){
	  (it->second).Hist1DListSliced[i] = new TH1**[N_OF_HIST];
	  for(int j=0;j<N_OF_HIST;j++){
	    (it->second).Hist1DListSliced[i][j] = new TH1*[N_OF_SLICE];
	    for(int k=0;k<N_OF_SLICE;k++){
	      (it->second).Hist1DListSliced[i][j][k] = NULL;
	    }
	  }
	}
      }
    }else if((it->second).Dim==2){
      (it->second).Hist2DList = new TH2**[1];
      for(int i=0;i<1;i++){
	(it->second).Hist2DList[i] = new TH2*[N_OF_HIST];
	for(int j=0;j<N_OF_HIST;j++){
	  (it->second).Hist2DList[i][j] = NULL;
	}
      }
    }
  }

  for (it = CombBkgHistBox.begin();it != CombBkgHistBox.end();++it){
    if((it->second).Dim==1){
      (it->second).Hist1DList = new TH1**[1];
      for(int i=0;i<1;i++){
	(it->second).Hist1DList[i] = new TH1*[N_OF_HIST];
	for(int j=0;j<N_OF_HIST;j++){
	  (it->second).Hist1DList[i][j] = NULL;
	}
      }
      if((it->second).MultiSlice){
	(it->second).Hist1DListSliced = new TH1***[1];
	for(int i=0;i<1;i++){
	  (it->second).Hist1DListSliced[i] = new TH1**[N_OF_HIST];
	  for(int j=0;j<N_OF_HIST;j++){
	    (it->second).Hist1DListSliced[i][j] = new TH1*[N_OF_SLICE];
	    for(int k=0;k<N_OF_SLICE;k++){
	      (it->second).Hist1DListSliced[i][j][k] = NULL;
	    }
	  }
	}
      }
    }else if((it->second).Dim==2){
      (it->second).Hist2DList = new TH2**[1];
      for(int i=0;i<1;i++){
	(it->second).Hist2DList[i] = new TH2*[N_OF_HIST];
	for(int j=0;j<N_OF_HIST;j++){
	  (it->second).Hist2DList[i][j] = NULL;
	}
      }
    }
  }
}

/***********************************************************************************/
int Analyzer::InitExpHistograms()
{
  if (ExpStatus!=0){
    cout << "Delete histograms first\n";
    return 1;
  }
  gROOT->cd();
  cout<<"Initializing exp histos...current directory: "<<gDirectory->GetPath()<<endl;
  InitToNullExp();
  string HistName;
  string HistTitle;
  //Initialize histograms
  map <string,HistUnit>::iterator it;
  for (it = ExpHistBox.begin();it != ExpHistBox.end();++it){
    if((it->second).Enabled){
      if((it->second).Dim==1){
	      for(int i=0;i<HistDim1;i++){
	        for(int j=0;j<HistDim2;j++){
	          HistName = "Exp" + it->first;
	          HistName+=Form("_Cond%d,%d",i,j);
	          HistTitle = (it->second).Title;
	          HistTitle+=Form(": Condition%d,%d",i,j);
	          (it->second).Hist1DList[i][j] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
	          (it->second).Hist1DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	          (it->second).Hist1DList[i][j]->GetXaxis()->SetTitleFont(60);
	          (it->second).Hist1DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	          (it->second).Hist1DList[i][j]->GetYaxis()->SetTitleFont(60);
	        }
	      }
	      if((it->second).MultiSlice){
	        for(int i=0;i<HistDim1;i++){
	          for(int j=0;j<HistDim2;j++){
	            for(int k=0;k<N_OF_SLICE;k++){
		            HistName = "Exp" + it->first;
		            HistName+=Form("_Cond%d,%d;Slice%d",i,j,k);
		            HistTitle = (it->second).Title;
		            HistTitle+=Form(": Condition%d,%d,Slice%d",i,j,k);
		            (it->second).Hist1DListSliced[i][j][k] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
		            (it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
		            (it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitleFont(60);
		            (it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
		            (it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitleFont(60);
	            }
	          }
	        }
	      }
      }else if((it->second).Dim==2){
	      for(int i=0;i<HistDim1;i++){
	        for(int j=0;j<HistDim2;j++){
	          HistName = "Exp" + it->first;
	          HistName+=Form("_Cond%d,%d",i,j);
	          HistTitle = (it->second).Title;
	          HistTitle+=Form(": Condition%d,%d",i,j);
	          (it->second).Hist2DList[i][j] = new TH2D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high,(it->second).YBinNum,(it->second).YRange.low,(it->second).YRange.high);
	          (it->second).Hist2DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	          (it->second).Hist2DList[i][j]->GetXaxis()->SetTitleFont(60);
	          (it->second).Hist2DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	          (it->second).Hist2DList[i][j]->GetYaxis()->SetTitleFont(60);
	          (it->second).Hist2DList[i][j]->GetZaxis()->SetTitle((it->second).ZTitle.c_str()); 
	          (it->second).Hist2DList[i][j]->GetZaxis()->SetTitleFont(60);
	        }
	      }
      }
    }
  }
  //Initialize Bkg histograms
  for (it = BkgHistBox.begin();it != BkgHistBox.end();++it){
    if((it->second).Enabled){
      if((it->second).Dim==1){
	      for(int i=0;i<HistDim1;i++){
	        for(int j=0;j<HistDim2;j++){
	          HistName = "Bkg" + it->first;
	          HistName+=Form("_Cond%d,%d",i,j);
	          HistTitle = (it->second).Title;
	          HistTitle+=Form(": Condition%d,%d",i,j);
	          (it->second).Hist1DList[i][j] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
	          (it->second).Hist1DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	          (it->second).Hist1DList[i][j]->GetXaxis()->SetTitleFont(60);
	          (it->second).Hist1DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	          (it->second).Hist1DList[i][j]->GetYaxis()->SetTitleFont(60);
	        }
	      }
	      if((it->second).MultiSlice){
	        for(int i=0;i<HistDim1;i++){
	          for(int j=0;j<HistDim2;j++){
	            for(int k=0;k<N_OF_SLICE;k++){
		            HistName = "Bkg" + it->first;
		            HistName+=Form("_Cond%d,%d;Slice%d",i,j,k);
		            HistTitle = (it->second).Title;
		            HistTitle+=Form(": Condition%d,%d,Slice%d",i,j,k);
		            (it->second).Hist1DListSliced[i][j][k] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
		            (it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
		            (it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitleFont(60);
		            (it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
		            (it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitleFont(60);
	            }
	          }
	        }
	      }
      }else if((it->second).Dim==2){
	    for(int i=0;i<HistDim1;i++){
	      for(int j=0;j<HistDim2;j++){
	        HistName = "Bkg" + it->first;
	        HistName+=Form("_Cond%d,%d",i,j);
	        HistTitle = (it->second).Title;
	        HistTitle+=Form(": Condition%d,%d",i,j);
	        (it->second).Hist2DList[i][j] = new TH2D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high,(it->second).YBinNum,(it->second).YRange.low,(it->second).YRange.high);
	        (it->second).Hist2DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	        (it->second).Hist2DList[i][j]->GetXaxis()->SetTitleFont(60);
	        (it->second).Hist2DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	        (it->second).Hist2DList[i][j]->GetYaxis()->SetTitleFont(60);
	        (it->second).Hist2DList[i][j]->GetZaxis()->SetTitle((it->second).ZTitle.c_str()); 
	        (it->second).Hist2DList[i][j]->GetZaxis()->SetTitleFont(60);
	      }
	    }
      }
    }
  }
  InitExpFillFunctions();
  ExpStatus=1;
  return 0;
}

/***********************************************************************************/
int Analyzer::InitCombExpHistograms()
{
  InitToNullCombExp();
  string HistName;
  string HistTitle;
  //Initialize histograms
  map <string,HistUnit>::iterator it;
  for (it = CombExpHistBox.begin();it != CombExpHistBox.end();++it){
    if((it->second).Enabled){
      if((it->second).Dim==1){
	for(int i=0;i<1;i++){
	  for(int j=0;j<N_OF_HIST;j++){
	    HistName = "CombExp" + it->first;
	    HistName+=Form("_Cond%d",j);
	    HistTitle = (it->second).Title;
	    HistTitle+=Form(": Condition%d",j);
	    (it->second).Hist1DList[i][j] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
	    (it->second).Hist1DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	    (it->second).Hist1DList[i][j]->GetXaxis()->SetTitleFont(60);
	    (it->second).Hist1DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	    (it->second).Hist1DList[i][j]->GetYaxis()->SetTitleFont(60);
	  }
	}
	if((it->second).MultiSlice){
	  for(int i=0;i<1;i++){
	    for(int j=0;j<N_OF_HIST;j++){
	      for(int k=0;k<N_OF_SLICE;k++){
		HistName = "CombExp" + it->first;
		HistName+=Form("_Cond%d;Slice%d",j,k);
		HistTitle = (it->second).Title;
		HistTitle+=Form(": Condition%d,Slice%d",j,k);
		(it->second).Hist1DListSliced[i][j][k] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
		(it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
		(it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitleFont(60);
		(it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
		(it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitleFont(60);
	      }
	    }
	  }
	}
      }else if((it->second).Dim==2){
	for(int i=0;i<1;i++){
	  for(int j=0;j<N_OF_HIST;j++){
	    HistName = "CombExp" + it->first;
	    HistName+=Form("_Cond%d",j);
	    HistTitle = (it->second).Title;
	    HistTitle+=Form(": Condition%d",j);
	    (it->second).Hist2DList[i][j] = new TH2D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high,(it->second).YBinNum,(it->second).YRange.low,(it->second).YRange.high);
	    (it->second).Hist2DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	    (it->second).Hist2DList[i][j]->GetXaxis()->SetTitleFont(60);
	    (it->second).Hist2DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	    (it->second).Hist2DList[i][j]->GetYaxis()->SetTitleFont(60);
	    (it->second).Hist2DList[i][j]->GetZaxis()->SetTitle((it->second).ZTitle.c_str()); 
	    (it->second).Hist2DList[i][j]->GetZaxis()->SetTitleFont(60);
	  }
	}
      }
    }
  }
  //Initialize Bkg histograms
  for (it = CombBkgHistBox.begin();it != CombBkgHistBox.end();++it){
    if((it->second).Enabled){
      if((it->second).Dim==1){
	for(int i=0;i<1;i++){
	  for(int j=0;j<N_OF_HIST;j++){
	    HistName = "CombBkg" + it->first;
	    HistName+=Form("_Cond%d",j);
	    HistTitle = (it->second).Title;
	    HistTitle+=Form(": Condition%d",j);
	    (it->second).Hist1DList[i][j] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
	    (it->second).Hist1DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	    (it->second).Hist1DList[i][j]->GetXaxis()->SetTitleFont(60);
	    (it->second).Hist1DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	    (it->second).Hist1DList[i][j]->GetYaxis()->SetTitleFont(60);
	  }
	}
	if((it->second).MultiSlice){
	  for(int i=0;i<1;i++){
	    for(int j=0;j<N_OF_HIST;j++){
	      for(int k=0;k<N_OF_SLICE;k++){
		HistName = "CombBkg" + it->first;
		HistName+=Form("_Cond%d;Slice%d",j,k);
		HistTitle = (it->second).Title;
		HistTitle+=Form(": Condition%d,Slice%d",j,k);
		(it->second).Hist1DListSliced[i][j][k] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
		(it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
		(it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitleFont(60);
		(it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
		(it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitleFont(60);
	      }
	    }
	  }
	}
      }else if((it->second).Dim==2){
	for(int i=0;i<1;i++){
	  for(int j=0;j<N_OF_HIST;j++){
	    HistName = "CombBkg" + it->first;
	    HistName+=Form("_Cond%d",j);
	    HistTitle = (it->second).Title;
	    HistTitle+=Form(": Condition%d",j);
	    (it->second).Hist2DList[i][j] = new TH2D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high,(it->second).YBinNum,(it->second).YRange.low,(it->second).YRange.high);
	    (it->second).Hist2DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	    (it->second).Hist2DList[i][j]->GetXaxis()->SetTitleFont(60);
	    (it->second).Hist2DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	    (it->second).Hist2DList[i][j]->GetYaxis()->SetTitleFont(60);
	    (it->second).Hist2DList[i][j]->GetZaxis()->SetTitle((it->second).ZTitle.c_str()); 
	    (it->second).Hist2DList[i][j]->GetZaxis()->SetTitleFont(60);
	  }
	}
      }
    }
  }
  CombStatus = 1;
  return 0;
}

/***********************************************************************************/
int Analyzer::DelExpHistograms()
{
  if (ExpStatus ==0 ){
    cout <<"Already deleted!\n";
    return 0;
  }
  //delete histograms
  map <string,HistUnit>::iterator it;
  for (it = ExpHistBox.begin();it != ExpHistBox.end();++it){
    if((it->second).Dim==1){
      for(int i=0;i<N_OF_HIST;i++){
	if ((it->second).Hist1DList[i]==NULL){break;}
	for(int j=0;j<N_OF_HIST;j++){
	  if ((it->second).Hist1DList[i][j]==NULL){break;}
	  else {delete (it->second).Hist1DList[i][j];} //deallocate memory for histogram 
	}
	delete[] (it->second).Hist1DList[i];
      }
      delete[] (it->second).Hist1DList;
      (it->second).Hist1DList = NULL;
      if((it->second).MultiSlice){
	for(int i=0;i<N_OF_HIST;i++){
	  if ((it->second).Hist1DListSliced[i]==NULL){break;}
	  for(int j=0;j<N_OF_HIST;j++){
	    if ((it->second).Hist1DListSliced[i][j]==NULL){break;}
	    for(int k=0;k<N_OF_SLICE;k++){
	      if ((it->second).Hist1DListSliced[i][j][k]==NULL){break;}
	      else {delete (it->second).Hist1DListSliced[i][j][k];}
	    }
	    delete[] (it->second).Hist1DListSliced[i][j];
	  }
	  delete[] (it->second).Hist1DListSliced[i];
	}
	delete[] (it->second).Hist1DListSliced;
	(it->second).Hist1DListSliced = NULL;
      }
    }else if((it->second).Dim==2){
      for(int i=0;i<N_OF_HIST;i++){
	if ((it->second).Hist2DList[i]==NULL){break;}
	for(int j=0;j<N_OF_HIST;j++){
	  if ((it->second).Hist2DList[i][j]==NULL){break;}
	  else {delete (it->second).Hist2DList[i][j];}
	}
	delete[] (it->second).Hist2DList[i];
      }
      delete[] (it->second).Hist2DList;
      (it->second).Hist2DList = NULL;
    }
  }
  //Delete Bkg histograms
  for (it = BkgHistBox.begin();it != BkgHistBox.end();++it){
    if((it->second).Dim==1){
      for(int i=0;i<N_OF_HIST;i++){
	if ((it->second).Hist1DList[i]==NULL){break;}
	for(int j=0;j<N_OF_HIST;j++){
	  if ((it->second).Hist1DList[i][j]==NULL){break;}
	  else {delete (it->second).Hist1DList[i][j];} //deallocate memory for histogram 
	}
	delete[] (it->second).Hist1DList[i];
      }
      delete[] (it->second).Hist1DList;
      (it->second).Hist1DList = NULL;
      if((it->second).MultiSlice){
	for(int i=0;i<N_OF_HIST;i++){
	  if ((it->second).Hist1DListSliced[i]==NULL){break;}
	  for(int j=0;j<N_OF_HIST;j++){
	    if ((it->second).Hist1DListSliced[i][j]==NULL){break;}
	    for(int k=0;k<N_OF_SLICE;k++){
	      if ((it->second).Hist1DListSliced[i][j][k]==NULL){break;}
	      else {delete (it->second).Hist1DListSliced[i][j][k];}
	    }
	    delete[] (it->second).Hist1DListSliced[i][j];
	  }
	  delete[] (it->second).Hist1DListSliced[i];
	}
	delete[] (it->second).Hist1DListSliced;
	(it->second).Hist1DListSliced = NULL;
      }
    }else if((it->second).Dim==2){
      for(int i=0;i<N_OF_HIST;i++){
	if ((it->second).Hist2DList[i]==NULL){break;}
	for(int j=0;j<N_OF_HIST;j++){
	  if ((it->second).Hist2DList[i][j]==NULL){break;}
	  else {delete (it->second).Hist2DList[i][j];}
	}
	delete[] (it->second).Hist2DList[i];
      }
      delete[] (it->second).Hist2DList;
      (it->second).Hist2DList = NULL;
    }
  }
  ExpStatus = 0;
  return 0;
}

/***********************************************************************************/
int Analyzer::DelCombExpHistograms()
{
  if(CombStatus == 0){
    return 0;
  }
  //delete histograms
  map <string,HistUnit>::iterator it;
  for (it = CombExpHistBox.begin();it != CombExpHistBox.end();++it){
    if((it->second).Dim==1){
      if ((it->second).Hist1DList==NULL){break;}
      for(int i=0;i<1;i++){
	if ((it->second).Hist1DList[i]==NULL){break;}
	for(int j=0;j<N_OF_HIST;j++){
	  if ((it->second).Hist1DList[i][j]==NULL){break;}
	  else {delete (it->second).Hist1DList[i][j];} //deallocate memory for histogram 
	}
	delete[] (it->second).Hist1DList[i];
      }
      delete[] (it->second).Hist1DList;
      (it->second).Hist1DList = NULL;
      if((it->second).MultiSlice){
	if ((it->second).Hist1DListSliced==NULL){break;}
	for(int i=0;i<1;i++){
	  if ((it->second).Hist1DListSliced[i]==NULL){break;}
	  for(int j=0;j<N_OF_HIST;j++){
	    if ((it->second).Hist1DListSliced[i][j]==NULL){break;}
	    for(int k=0;k<N_OF_SLICE;k++){
	      if ((it->second).Hist1DListSliced[i][j][k]==NULL){break;}
	      else {delete (it->second).Hist1DListSliced[i][j][k];}
	    }
	    delete[] (it->second).Hist1DListSliced[i][j];
	  }
	  delete[] (it->second).Hist1DListSliced[i];
	}
	delete[] (it->second).Hist1DListSliced;
	(it->second).Hist1DListSliced = NULL;
      }
    }else if((it->second).Dim==2){
      if ((it->second).Hist2DList==NULL){break;}
      for(int i=0;i<1;i++){
	if ((it->second).Hist2DList[i]==NULL){break;}
	for(int j=0;j<N_OF_HIST;j++){
	  if ((it->second).Hist2DList[i][j]==NULL){break;}
	  else {delete (it->second).Hist2DList[i][j];}
	}
	delete[] (it->second).Hist2DList[i];
      }
      delete[] (it->second).Hist2DList;
      (it->second).Hist2DList = NULL;
    }
  }
  //Delete Bkg histograms
  for (it = CombBkgHistBox.begin();it != CombBkgHistBox.end();++it){
    if((it->second).Dim==1){
      if ((it->second).Hist1DList==NULL){break;}
      for(int i=0;i<1;i++){
	if ((it->second).Hist1DList[i]==NULL){break;}
	for(int j=0;j<N_OF_HIST;j++){
	  if ((it->second).Hist1DList[i][j]==NULL){break;}
	  else {delete (it->second).Hist1DList[i][j];} //deallocate memory for histogram 
	}
	delete[] (it->second).Hist1DList[i];
      }
      delete[] (it->second).Hist1DList;
      (it->second).Hist1DList = NULL;
      if((it->second).MultiSlice){
	if ((it->second).Hist1DListSliced==NULL){break;}
	for(int i=0;i<1;i++){
	  if ((it->second).Hist1DListSliced[i]==NULL){break;}
	  for(int j=0;j<N_OF_HIST;j++){
	    if ((it->second).Hist1DListSliced[i][j]==NULL){break;}
	    for(int k=0;k<N_OF_SLICE;k++){
	      if ((it->second).Hist1DListSliced[i][j][k]==NULL){break;}
	      else {delete (it->second).Hist1DListSliced[i][j][k];}
	    }
	    delete[] (it->second).Hist1DListSliced[i][j];
	  }
	  delete[] (it->second).Hist1DListSliced[i];
	}
	delete[] (it->second).Hist1DListSliced;
	(it->second).Hist1DListSliced = NULL;
      }
    }else if((it->second).Dim==2){
      if ((it->second).Hist2DList==NULL){break;}
      for(int i=0;i<1;i++){
	if ((it->second).Hist2DList[i]==NULL){break;}
	for(int j=0;j<N_OF_HIST;j++){
	  if ((it->second).Hist2DList[i][j]==NULL){break;}
	  else {delete (it->second).Hist2DList[i][j];}
	}
	delete[] (it->second).Hist2DList[i];
      }
      delete[] (it->second).Hist2DList;
      (it->second).Hist2DList = NULL;
    }
  }
  CombStatus = 0;
  return 0;
}

/***********************************************************************************/
int Analyzer::RenewExpHistograms()
{
  //Delete histograms
  DelExpHistograms();
  //Redefine
  return InitExpHistograms();
}

/***********************************************************************************/
int Analyzer::RenewCombExpHistograms()
{
  //Delete histograms
  DelCombExpHistograms();
  //Redefine
  return InitCombExpHistograms();
}

/***********************************************************************************/
int Analyzer::SaveExpHistograms(string label,string Opt)
{
  if(ExpStatus!=2){
    cout<<"Histograms are not yet loaded!\n";
    return -1;
  }
  string filename;
  map <string,HistUnit>::iterator it;

  //Options
  if (Opt.compare("Both")==0 || Opt.compare("Array")==0){
    filename = HISTOGRAM_DIRECTORY + "/Histograms_";
    filename += label;
    filename += "_";
    filename += ReadMode;
    filename += ".root";
    TFile *OutFile = new TFile(filename.c_str(),"RECREATE");

    for (it = ExpHistBox.begin();it != ExpHistBox.end();++it){
      if((it->second).Enabled){
	if((it->second).Dim==1){
	  for(int i=0;i<HistDim1;i++){
	    for(int j=0;j<HistDim2;j++){
	      (it->second).Hist1DList[i][j]->Write(); 
	    }
	  }
	  if ((it->second).MultiSlice){
	    for(int i=0;i<HistDim1;i++){
	      for(int j=0;j<HistDim2;j++){
		for(int k=0;k<nSlice;k++){
		  (it->second).Hist1DListSliced[i][j][k]->Write(); 
		}
	      }
	    }
	  }
	}else if((it->second).Dim==2){
	  for(int i=0;i<HistDim1;i++){
	    for(int j=0;j<HistDim2;j++){
	      (it->second).Hist2DList[i][j]->Write(); 
	    }
	  }
	}
      }
    }
    //Save Bkg histograms
    for (it = BkgHistBox.begin();it != BkgHistBox.end();++it){
      if((it->second).Enabled){
	if((it->second).Dim==1){
	  for(int i=0;i<HistDim1;i++){
	    for(int j=0;j<HistDim2;j++){
	      (it->second).Hist1DList[i][j]->Write(); 
	    }
	  }
	  if ((it->second).MultiSlice){
	    for(int i=0;i<HistDim1;i++){
	      for(int j=0;j<HistDim2;j++){
		for(int k=0;k<nSlice;k++){
		  (it->second).Hist1DListSliced[i][j][k]->Write(); 
		}
	      }
	    }
	  }
	}else if((it->second).Dim==2){
	  for(int i=0;i<HistDim1;i++){
	    for(int j=0;j<HistDim2;j++){
	      (it->second).Hist2DList[i][j]->Write(); 
	    }
	  }
	}
      }
    }
    map <string,TGraphErrors *>::iterator ig;
    for (ig = SysGraphBox1D.begin();ig != SysGraphBox1D.end();++ig){
      (ig->second)->Write();
    }
    map <string,TGraph2DErrors *>::iterator igg;
    for (igg = SysGraphBox2D.begin();igg != SysGraphBox2D.end();++igg){
      (igg->second)->Write();
    }
    OutFile->Close();
    delete OutFile;
  }
  else if (Opt.compare("Both")==0 || Opt.compare("Combined")==0){
    filename = HISTOGRAM_DIRECTORY + "/CombHistograms_";
    filename += label;
    filename += ".root";
    TFile *OutFile = new TFile(filename.c_str(),"RECREATE");
    for (it = CombExpHistBox.begin();it != CombExpHistBox.end();++it){
      if((it->second).Enabled){
	if((it->second).Dim==1){
	  for(int i=0;i<1;i++){
	    for(int j=0;j<CombHistDim;j++){
	      (it->second).Hist1DList[i][j]->Write(); 
	    }
	  }
	  if ((it->second).MultiSlice){
	    for(int i=0;i<1;i++){
	      for(int j=0;j<CombHistDim;j++){
		for(int k=0;k<nSlice;k++){
		  (it->second).Hist1DListSliced[i][j][k]->Write(); 
		}
	      }
	    }
	  }
	}else if((it->second).Dim==2){
	  for(int i=0;i<1;i++){
	    for(int j=0;j<CombHistDim;j++){
	      (it->second).Hist2DList[i][j]->Write(); 
	    }
	  }
	}
      }
    }
    //Save Bkg histograms
    for (it = CombBkgHistBox.begin();it != CombBkgHistBox.end();++it){
      if((it->second).Enabled){
	if((it->second).Dim==1){
	  for(int i=0;i<1;i++){
	    for(int j=0;j<CombHistDim;j++){
	      (it->second).Hist1DList[i][j]->Write(); 
	    }
	  }
	  if ((it->second).MultiSlice){
	    for(int i=0;i<1;i++){
	      for(int j=0;j<CombHistDim;j++){
		for(int k=0;k<nSlice;k++){
		  (it->second).Hist1DListSliced[i][j][k]->Write(); 
		}
	      }
	    }
	  }
	}else if((it->second).Dim==2){
	  for(int i=0;i<1;i++){
	    for(int j=0;j<CombHistDim;j++){
	      (it->second).Hist2DList[i][j]->Write(); 
	    }
	  }
	}
      }
    }
    map <string,TGraphErrors *>::iterator ig;
    for (ig = SysGraphBox1D.begin();ig != SysGraphBox1D.end();++ig){
      (ig->second)->Write();
    }
    map <string,TGraph2DErrors *>::iterator igg;
    for (igg = SysGraphBox2D.begin();igg != SysGraphBox2D.end();++igg){
      (igg->second)->Write();
    }
    OutFile->Close();
    delete OutFile;
  }
  else{
    cout <<"Unidentified option. Saving failed.\n";
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::LoadExpHistograms(string label,string Opt)
{
  string filename;
  if (Opt.compare("Array")==0){
    filename = HISTOGRAM_DIRECTORY + "/Histograms_";
    filename += label;
    filename += ".root";
    //TCanvas *mydummycanvas=new TCanvas();

    TFile *InFile = new TFile(filename.c_str(),"READ");
    if (InFile->IsZombie()){
      cout<<"Fail when opening file " << filename.c_str()<<"!\n";
      return -1;
    }
    //Read Histograms
    if (ExpStatus!=0)DelExpHistograms();
    int i=0;
    int j=0;
    int k=0;
    TH1 * h = NULL;
    TH2 * hh = NULL;
    string HistName;

    gROOT->cd(); //all objects created are subsequently owned by gROOT (ie )

    map <string,HistUnit>::iterator it;
    //Enable All?
    InitToNullExp(); //Initialize array pointers for each hist unit to NULL.

    for (it = ExpHistBox.begin();it != ExpHistBox.end();++it){
      if (!(it->second).Enabled)continue;
      if((it->second).Dim==1){
	for(j=0;j<N_OF_HIST;j++){
	  for(i=0;i<N_OF_HIST;i++){
	    HistName = "Exp" + it->first;
	    HistName+=Form("_Cond%d,%d",i,j);
	    h = (TH1*)InFile->Get(HistName.c_str());
	    if (h==NULL){
	      if(i==0 && j!=0) HistDim2=j; //set the histogram dimension of the column to the number of the last successful load of the column
	      break;
	    }
	    else{
	      (it->second).Hist1DList[i][j] = (TH1*)h->Clone();//copy hist into gROOT and assign to Hist1DList pointer
	      delete h; //delete object pointed to from memory
	      HistDim1 = i+1;
	    }
	  }
	  if (i==0){
	    //if(i!=0) HistDim1=i;// if at least one histogram has been loaded, set the histogram dimension of the row to the number of the last successful load of the row                                 
	    break;
	  }
	  else {continue;}
	}
	//for multi slice
	i=j=k=0;
	if((it->second).MultiSlice){
	  for(i=0;i<HistDim1;i++){
	    for(j=0;j<HistDim2;j++){
	      for(k=0;k<N_OF_SLICE;k++){
		HistName = "Exp" + it->first;
		HistName+=Form("_Cond%d,%d;Slice%d",i,j,k);
		h = (TH1*)InFile->Get(HistName.c_str());
		if (h==NULL){
		  nSlice=k;
		  break;
		}
		(it->second).Hist1DListSliced[i][j][k] = (TH1*)h->Clone();
		delete h;
	      }
	      if (k==0)break;
	    }
	    if (j==0)break;
	  }
	}
      }else if((it->second).Dim==2){
	for(j=0;j<N_OF_HIST;j++){
	  for(i=0;i<N_OF_HIST;i++){
	    HistName = "Exp" + it->first;
	    HistName+=Form("_Cond%d,%d",i,j);
	    hh = (TH2*)InFile->Get(HistName.c_str());
	    if (hh==NULL){
	      if(i==0 && j!=0){HistDim2=j;} //set the histogram dimension of the column to the number of the last successful load of the column
	      break;
	    }
	    else{
	      (it->second).Hist2DList[i][j] = (TH2*)hh->Clone();
	      delete hh;
	      HistDim1 = i+1;
	    }
	  }
	  if (i==0){ //if the first histogram of the new column is not loaded
	    //if (i!=0) HistDim1=i; //set the histogram dimension of the row to the number of the last successful load of the row
	    break;
	  }
	  else{continue;}
	}
      }
    }
    //Load Bkg histograms
    for (it = BkgHistBox.begin();it != BkgHistBox.end();++it){
      if(!(it->second).Enabled)continue;
      if((it->second).Dim==1){
	for(j=0;j<N_OF_HIST;j++){
	  for(i=0;i<N_OF_HIST;i++){
	    HistName = "Bkg" + it->first;
	    HistName+=Form("_Cond%d,%d",i,j);
	    h = (TH1*)InFile->Get(HistName.c_str());
	    if (h==NULL){                 
	      if(i==0 && j!=0) HistDim2=j; //set the histogram dimension of the column to the number of the last successful load of the column
	      break;                              
	    }                             
	    else{                         
	      (it->second).Hist1DList[i][j] = (TH1*)h->Clone();
	      delete h; //delete object pointed to from memory
	      HistDim1 = i+1;
	    }  
	  }
	  if (i==0){//the first histogram of the new column is not loaded
	    //if(i!=0) HistDim1=i;// if at least one histogram has been loaded, set the histogram dimension of the row to the number of the last successful load of the row 
	    break;
	  }                               
	  else {continue;} 
	}
	//for multi slice
	i=j=k=0;
	if((it->second).MultiSlice){
	  for(i=0;i<HistDim1;i++){
	    for(j=0;j<HistDim2;j++){
	      for(k=0;k<N_OF_SLICE;k++){
		HistName = "Bkg" + it->first;
		HistName+=Form("_Cond%d,%d;Slice%d",i,j,k);
		h = (TH1*)InFile->Get(HistName.c_str());
		if (h==NULL){
		  nSlice=k;
		  break;
		}
		(it->second).Hist1DListSliced[i][j][k] = (TH1*)h->Clone();
		delete h;
	      }
	      if (k==0)break;
	    }
	    if (j==0)break;
	  }
	}
      }else if((it->second).Dim==2){
	for(j=0;j<N_OF_HIST;j++){
	  for(i=0;i<N_OF_HIST;i++){
	    HistName = "Bkg" + it->first;
	    HistName+=Form("_Cond%d,%d",i,j);
	    hh = (TH2*)InFile->Get(HistName.c_str());
	    if (hh==NULL){
	      if(i==0 && j!=0){HistDim2=j;} //set the histogram dimension of the column to the number of the last successful load of the column
	      break;
	      HistDim1 = i+1;
	    }
	    else{
	      (it->second).Hist2DList[i][j] = (TH2*)hh->Clone();
	      delete hh;
	    }
	  }
	  if (i==0){ //if the first histogram of the new column is not loaded
	   // if (i!=0) HistDim1=i; //set the histogram dimension of the row to the number of the last successful load of the row
	    break;
	  }
	  else{continue;}
	}
      }
    }

    ExpStatus=2;
    cout<<"Histograms Loaded: [HistDim1][HistDim2] = ["<<HistDim1<<"]["<<HistDim2<<"]"<<endl;
    InFile->Close();
    delete InFile;
  }
  else if (Opt.compare("Combined")==0){
    filename = HISTOGRAM_DIRECTORY + "/CombHistograms_";
    filename += label;
    filename += ".root";
    //TCanvas *mydummycanvas=new TCanvas();

    TFile *InFile = new TFile(filename.c_str(),"READ");
    if (InFile->IsZombie()){
      cout<<"Fail when opening file " << filename.c_str()<<"!\n";
      return -1;
    }
    //Read Histograms
    DelCombExpHistograms();
    int i=0;
    int j=0;
    int k=0;
    TH1 * h = NULL;
    TH2 * hh = NULL;
    string HistName;

    gROOT->cd();

    map <string,HistUnit>::iterator it;
    InitToNullCombExp(); //Initialize array pointers for each hist unit to NULL.
//    InitCombExpHistograms();

    for (it = CombExpHistBox.begin();it != CombExpHistBox.end();++it){
      if (!(it->second).Enabled)continue;
      if((it->second).Dim==1){
	for(j=0;j<N_OF_HIST;j++){
	  HistName = "CombExp" + it->first;
	  HistName+=Form("_Cond%d",j);
	  h = (TH1*)InFile->Get(HistName.c_str());
	  if (h==NULL){
	    CombHistDim=j; //set the Combined histogram dimension 
	    break;
	  }
	  else{
//	    delete (it->second).Hist1DList[0][j];
	    (it->second).Hist1DList[0][j] = (TH1*)h->Clone();
	    delete h; //delete object pointed to from memory
	  }
	}
	//for multi slice
	i=j=k=0;
	if((it->second).MultiSlice){
	  for(j=0;j<N_OF_HIST;j++){
	    for(k=0;k<N_OF_SLICE;k++){
	      HistName = "CombExp" + it->first;
	      HistName+=Form("_Cond%d;Slice%d",j,k);
	      h = (TH1*)InFile->Get(HistName.c_str());
	      if (h==NULL){
		nSlice=k;
		break;
	      }
//	      delete (it->second).Hist1DListSliced[0][j][k];
	      (it->second).Hist1DListSliced[0][j][k] = (TH1*)h->Clone();
	      delete h;
	    }
	    if (k==0)break;
	  }
	}
      }else if((it->second).Dim==2){
	for(j=0;j<N_OF_HIST;j++){
	  HistName = "CombExp" + it->first;
	  HistName+=Form("_Cond%d",j);
	  hh = (TH2*)InFile->Get(HistName.c_str());
	  if (hh==NULL){
	    CombHistDim=j; //set the Combined histogram dimension 
	    break;
	  }
	  else{
//	    delete (it->second).Hist2DList[0][j];
	    (it->second).Hist2DList[0][j] = (TH2*)hh->Clone();
	    delete hh;
	  }
	}
      }
    }
    //Load Bkg histograms
    for (it = CombBkgHistBox.begin();it != CombBkgHistBox.end();++it){
      if(!(it->second).Enabled)continue;
      if((it->second).Dim==1){
	for(j=0;j<N_OF_HIST;j++){
	  HistName = "CombBkg" + it->first;
	  HistName+=Form("_Cond%d",j);
	  h = (TH1*)InFile->Get(HistName.c_str());
	  if (h==NULL){                 
	    break;                              
	  }                             
	  else{                         
//	    delete (it->second).Hist1DList[0][j];
	    (it->second).Hist1DList[0][j] = (TH1*)h->Clone();
	    delete h; //delete object pointed to from memory
	  }  
	}
	//for multi slice
	i=j=k=0;
	if((it->second).MultiSlice){
	  for(j=0;j<N_OF_HIST;j++){
	    for(k=0;k<N_OF_SLICE;k++){
	      HistName = "CombBkg" + it->first;
	      HistName+=Form("_Cond%d;Slice%d",j,k);
	      h = (TH1*)InFile->Get(HistName.c_str());
	      if (h==NULL){
		nSlice=k;
		break;
	      }
//	      delete (it->second).Hist1DListSliced[0][j][k];
	      (it->second).Hist1DListSliced[0][j][k] = (TH1*)h->Clone();
	      delete h;
	    }
	    if (k==0)break;
	  }
	}
      }else if((it->second).Dim==2){
	for(j=0;j<N_OF_HIST;j++){
	  HistName = "CombBkg" + it->first;
	  HistName+=Form("_Cond%d",j);
	  hh = (TH2*)InFile->Get(HistName.c_str());
	  if (hh==NULL){
	    break;
	  }
	  else{
//	    delete (it->second).Hist2DList[0][j];
	    (it->second).Hist2DList[0][j] = (TH2*)hh->Clone();
	    delete hh;
	  }
	}
      }
    }

    CombStatus=2;
    InFile->Close();
    delete InFile;
  }

  return 0;
}

/***********************************************************************************/
int Analyzer::FillExpHistograms(int i,int j,bool cond,double Time,RecDataStruct& RecData)
{
  map <string,HistUnit> * ChosenHistBox;
  map <string,int (Analyzer::*)(int,int,bool,map <string,HistUnit>*,RecDataStruct& )>::iterator it;
  int returnval = 0;   
  if (cond){
    if (BkgSeparation && Time>BkgTime){
      ChosenHistBox = &BkgHistBox;
      BkgEvent_Count[i][j] += 1;
    }else{
      ChosenHistBox = &ExpHistBox;
      Event_Count[i][j] += 1;
    }
    for (it = ExpFillFunctions.begin();it != ExpFillFunctions.end();++it){
      //cout<< it->first<<endl;
      returnval += (this->*(it->second))(i,j,cond,ChosenHistBox,RecData);
    }
  }
  return returnval;
}

/***********************************************************************************/
//Filling functions
int Analyzer::FillScint_EA(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["Scint_EA"].Hist1DList)[i][j])->Fill(RecData.EScintA[0]);
  return 0;
}

int Analyzer::FillScint_EDA(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["Scint_EDA"].Hist2DList)[i][j])->Fill(RecData.EScintD[0],RecData.EScintA[0]);
  return 0;
}

int Analyzer::FillScint_Time(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["Scint_Time"].Hist1DList)[i][j])->Fill(RecData.TScint_A_rel);
  return 0;
}

int Analyzer::FillMWPC_Anode(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
//  (((*ChosenHistBox)["MWPC_Anode"].Hist1DList)[i][j])->Fill(RecData.EMWPC_anode1+RecData.EMWPC_anode2);
  (((*ChosenHistBox)["MWPC_Anode"].Hist1DList)[i][j])->Fill(RecData.EMWPC_anode);
  return 0;
}

int Analyzer::FillBeta_Time_diff(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["Beta_Time_diff"].Hist1DList)[i][j])->Fill(RecData.TBeta_diff[0]);
  return 0;
}

int Analyzer::FillMWPC_Cathode_Anode(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MWPC_Cathode_Anode"].Hist2DList)[i][j])->Fill(RecData.QMWPC_cathode_X,RecData.EMWPC_anode1+RecData.EMWPC_anode2);
  return 0;
}

int Analyzer::FillMWPC_Cathode_XY(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MWPC_Cathode_XY"].Hist2DList)[i][j])->Fill(RecData.QMWPC_cathode_X,RecData.QMWPC_cathode_Y);
  return 0;
}

int Analyzer::FillMWPC_vs_Scint(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MWPC_vs_Scint"].Hist2DList)[i][j])->Fill(RecData.EMWPC_anode1+RecData.EMWPC_anode2,RecData.EScintA[0]);
  return 0;
}

int Analyzer::FillMWPC_Image(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MWPC_Image"].Hist2DList)[i][j])->Fill(RecData.MWPCPos_X,RecData.MWPCPos_Y);
  return 0;
}

int Analyzer::FillMWPC_CAY(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MWPC_CAY"].Hist2DList)[i][j])->Fill(RecData.MWPCPos_Y_C,RecData.MWPCPos_Y);
  return 0;
}

int Analyzer::FillMCP_TX1vQX1(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MCP_TX1vQX1"].Hist2DList)[i][j])->Fill(RecData.QMCP_anodes.QX1,RecData.TMCP_anodes.TX1);
  return 0;
}

int Analyzer::FillMCP_TX2vQX2(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MCP_TX2vQX2"].Hist2DList)[i][j])->Fill(RecData.QMCP_anodes.QX2,RecData.TMCP_anodes.TX2);
  return 0;
}

int Analyzer::FillMCP_TY1vQY1(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MCP_TY1vQY1"].Hist2DList)[i][j])->Fill(RecData.QMCP_anodes.QY1,RecData.TMCP_anodes.TY1);
  return 0;
}

int Analyzer::FillMCP_TY2vQY2(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MCP_TY2vQY2"].Hist2DList)[i][j])->Fill(RecData.QMCP_anodes.QY2,RecData.TMCP_anodes.TY2);
  return 0;
}

int Analyzer::FillMCP_TX1pTX2(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MCP_TX1pTX2"].Hist1DList)[i][j])->Fill(RecData.MCP_TXSum);
  return 0;
}

int Analyzer::FillMCP_TXSumPos(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MCP_TXSumPos"].Hist2DList)[i][j])->Fill(RecData.MCP_TXSum,RecData.MCPPos_X);
  return 0;
}

int Analyzer::FillMCP_TY1pTY2(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MCP_TY1pTY2"].Hist1DList)[i][j])->Fill(RecData.MCP_TYSum);
  return 0;
}

int Analyzer::FillMCP_TYSumPos(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MCP_TYSumPos"].Hist2DList)[i][j])->Fill(RecData.MCP_TYSum,RecData.MCPPos_Y);
  return 0;
}

int Analyzer::FillMCP_Image(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MCP_Image"].Hist2DList)[i][j])->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
  return 0;
}

int Analyzer::FillMCP_Image_Zoom(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  double RotationAngle = -0.3927;       //22.5 deg
  double rotX = RecData.MCPPos_X*cos(RotationAngle)-RecData.MCPPos_Y*sin(RotationAngle);
  double rotY = RecData.MCPPos_Y*cos(RotationAngle)+RecData.MCPPos_X*sin(RotationAngle);
  (((*ChosenHistBox)["MCP_Image_Zoom"].Hist2DList)[i][j])->Fill(rotX,rotY);
  return 0;
}

int Analyzer::FillMCP_R(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MCP_R"].Hist1DList)[i][j])->Fill(sqrt(pow(RecData.MCPPos_X,2.0)+pow(RecData.MCPPos_Y,2.0)));
  return 0;
}

int Analyzer::FillMCP_Time(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MCP_Time"].Hist1DList)[i][j])->Fill(RecData.TMCP_rel);
  return 0;
}

int Analyzer::FillMCP_Charge(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["MCP_Charge"].Hist1DList)[i][j])->Fill(RecData.QMCP);
  return 0;
}

int Analyzer::FillTOFHist(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["TOFHist"].Hist1DList)[i][j])->Fill(RecData.TOF);
  return 0;
}

int Analyzer::FillTOF_vs_Scint(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["TOF_vs_Scint"].Hist2DList)[i][j])->Fill(RecData.TOF,RecData.EScintA[0]);
  return 0;
}

int Analyzer::FillTOF_vs_QMCP(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["TOF_vs_QMCP"].Hist2DList)[i][j])->Fill(RecData.TOF,RecData.QMCP);
  return 0;
}

int Analyzer::FillN2_Time(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["N2_Time"].Hist1DList)[i][j])->Fill(RecData.TN2Laser);
  return 0;
}

int Analyzer::FillN2_Charge(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["N2_Charge"].Hist1DList)[i][j])->Fill(RecData.QN2Laser);
  return 0;
}

//int Analyzer::FillN2_TOFHist(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
//  (((*ChosenHistBox)["N2_TOFHist"].Hist1DList)[i][j])->Fill(RecData.TOFN2Laser);
//  return 0;
//}

int Analyzer::FillQ_Values(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["Q_Values"].Hist2DList)[i][j])->Fill(RecData.Q_Value[0],RecData.Q_Value[1]);
  return 0;
}

int Analyzer::FillCosThetaENu(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["CosThetaENu"].Hist1DList)[i][j])->Fill(RecData.CosThetaENu);
  return 0;
}

int Analyzer::FillEIon1(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["EIon1"].Hist1DList)[i][j])->Fill(RecData.EIon[0]);
  return 0;
}

int Analyzer::FillEIon2(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["EIon2"].Hist1DList)[i][j])->Fill(RecData.EIon[1]);
  return 0;
}

/*int Analyzer::FillQ_Value2(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["Q_Value2"].Hist1DList)[i][j])->Fill(RecData.Q_Value[1]);
  return 0;
}*/

int Analyzer::FillGroupTime(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData){
  (((*ChosenHistBox)["GroupTime"].Hist1DList)[i][j])->Fill(RecData.GroupTime);
  return 0;
}

/***********************************************************************************/
int Analyzer::CombineExpHistograms(int CombineDim,string BkgOpt)
{
  if(ExpStatus!=2){
    cout<<"Histograms are not yet loaded!\n";
    return -1;
  }

  if(CombStatus==0){
    InitCombExpHistograms();
  }

  int i=0;
  int j=0;
  int k=0;
  int *iterator;
  if (CombineDim==0){
    iterator = &i;
    CombHistDim = HistDim1;
  }else if(CombineDim==1){
    iterator = &j;
    CombHistDim = HistDim2;
  }else if(CombineDim==2){
    iterator = &i;
    CombHistDim = HistDim1;
  }
  map <string,HistUnit>::iterator it;
  for (it = ExpHistBox.begin();it != ExpHistBox.end();++it){
    if((it->second).Enabled){
      if((it->second).Dim==1){
	for(i=0;i<HistDim1;i++){
	  for(j=0;j<HistDim2;j++){
	    if (CombineDim==0 && j>0)break;
	    CombExpHistBox[it->first].Hist1DList[0][*iterator]->Add((it->second).Hist1DList[i][j]);
	  }
	}
	if ((it->second).MultiSlice){
	  for(i=0;i<HistDim1;i++){
	    for(j=0;j<HistDim2;j++){
	      if (CombineDim==0 && j>0)break;
	      for(k=0;k<nSlice;k++){
		CombExpHistBox[it->first].Hist1DListSliced[0][*iterator][k]->Add((it->second).Hist1DListSliced[i][j][k]);
	      }
	    }
	  }
	}
      }else if((it->second).Dim==2){
	for(i=0;i<HistDim1;i++){
	  for(j=0;j<HistDim2;j++){
	    if (CombineDim==0 && j>0)break;
	    CombExpHistBox[it->first].Hist2DList[0][*iterator]->Add((it->second).Hist2DList[i][j]);
	  }
	}
      }
    }
  }
  //Combine Bkg histograms
  if (BkgOpt.compare("ON")==0){
    for (it = BkgHistBox.begin();it != BkgHistBox.end();++it){
      if((it->second).Enabled){
	if((it->second).Dim==1){
	  for(i=0;i<HistDim1;i++){
	    for(j=0;j<HistDim2;j++){
	      if (CombineDim==0 && j>0)break;
	      CombBkgHistBox[it->first].Hist1DList[0][*iterator]->Add((it->second).Hist1DList[i][j]);
	    }
	  }
	  if ((it->second).MultiSlice){
	    for(i=0;i<HistDim1;i++){
	      for(j=0;j<HistDim2;j++){
		if (CombineDim==0 && j>0)break;
		for(k=0;k<nSlice;k++){
		  CombBkgHistBox[it->first].Hist1DListSliced[0][*iterator][k]->Add((it->second).Hist1DListSliced[i][j][k]);
		}
	      }
	    }
	  }
	}else if((it->second).Dim==2){
	  for(i=0;i<HistDim1;i++){
	    for(j=0;j<HistDim2;j++){
	      if (CombineDim==0 && j>0)break;
	      CombBkgHistBox[it->first].Hist2DList[0][*iterator]->Add((it->second).Hist2DList[i][j]);
	    }
	  }
	}
      }
    }
  }
  CombStatus=2;
  return 0;
}

/***********************************************************************************/
int Analyzer::PutBackCombHistograms()
{
  if (ExpStatus!=0)DelExpHistograms();
  InitToNullExp(); //Initialize array pointers for each hist unit to NULL.

  int i=0;
  int j=0;
  int k=0;
  HistDim1=CombHistDim;
  HistDim2=1;
  map <string,HistUnit>::iterator it;
  string HistName;
  string HistTitle;
  for (it = ExpHistBox.begin();it != ExpHistBox.end();++it){
    if((it->second).Enabled){
      if((it->second).Dim==1){
	for(int i=0;i<CombHistDim;i++){
	  HistName = "Exp" + it->first;
	  HistName+=Form("_Cond%d,%d",i,j);
	  HistTitle = (it->second).Title;
	  HistTitle+=Form(": Condition%d,%d",i,j);
	  (it->second).Hist1DList[i][0] = (TH1*)CombExpHistBox[it->first].Hist1DList[0][i]->Clone();
	  (it->second).Hist1DList[i][0]->SetTitle(HistTitle.c_str());
	  (it->second).Hist1DList[i][0]->SetName(HistName.c_str());
	}
	if ((it->second).MultiSlice){
	  for(int i=0;i<CombHistDim;i++){
	    for(int k=0;k<nSlice;k++){
	      HistName = "Exp" + it->first;
	      HistName+=Form("_Cond%d,%d;Slice%d",i,j,k);
	      HistTitle = (it->second).Title;
	      HistTitle+=Form(": Condition%d,%d,Slice%d",i,j,k);
	      (it->second).Hist1DListSliced[i][0][k] = (TH1*)CombExpHistBox[it->first].Hist1DListSliced[0][i][k]->Clone();
	      (it->second).Hist1DListSliced[i][0][k]->SetTitle(HistTitle.c_str());
	      (it->second).Hist1DListSliced[i][0][k]->SetName(HistName.c_str());
	    }
	  }
	}
      }else if((it->second).Dim==2){
	for(int i=0;i<CombHistDim;i++){
	  HistName = "Exp" + it->first;
	  HistName+=Form("_Cond%d,%d",i,j);
	  HistTitle = (it->second).Title;
	  HistTitle+=Form(": Condition%d,%d",i,j);
	  (it->second).Hist2DList[i][0] = (TH2*)CombExpHistBox[it->first].Hist2DList[0][i]->Clone();
	  (it->second).Hist2DList[i][0]->SetTitle(HistTitle.c_str());
	  (it->second).Hist2DList[i][0]->SetName(HistName.c_str());
	}
      }
    }
  }
  //Put back Bkg histograms
  for (it = BkgHistBox.begin();it != BkgHistBox.end();++it){
    if((it->second).Enabled){
      if((it->second).Dim==1){
	for(int i=0;i<CombHistDim;i++){
	  HistName = "Bkg" + it->first;
	  HistName+=Form("_Cond%d,%d",i,j);
	  HistTitle = (it->second).Title;
	  HistTitle+=Form(": Condition%d,%d",i,j);
	  (it->second).Hist1DList[i][0] = (TH1*)CombBkgHistBox[it->first].Hist1DList[0][i]->Clone();
	  (it->second).Hist1DList[i][0]->SetTitle(HistTitle.c_str());
	  (it->second).Hist1DList[i][0]->SetName(HistName.c_str());
	}
	if ((it->second).MultiSlice){
	  for(int i=0;i<CombHistDim;i++){
	    for(int k=0;k<nSlice;k++){
	      HistName = "Bkg" + it->first;
	      HistName+=Form("_Cond%d,%d;Slice%d",i,j,k);
	      HistTitle = (it->second).Title;
	      HistTitle+=Form(": Condition%d,%d,Slice%d",i,j,k);
	      (it->second).Hist1DListSliced[i][0][k] = (TH1*)CombBkgHistBox[it->first].Hist1DListSliced[0][i][k]->Clone();
	      (it->second).Hist1DListSliced[i][0][k]->SetTitle(HistTitle.c_str());
	      (it->second).Hist1DListSliced[i][0][k]->SetName(HistName.c_str());
	    }
	  }
	}
      }else if((it->second).Dim==2){
	for(int i=0;i<CombHistDim;i++){
	  HistName = "Bkg" + it->first;
	  HistName+=Form("_Cond%d,%d",i,j);
	  HistTitle = (it->second).Title;
	  HistTitle+=Form(": Condition%d,%d",i,j);
	  (it->second).Hist2DList[i][0] = (TH2*)CombBkgHistBox[it->first].Hist2DList[0][i]->Clone();
	  (it->second).Hist2DList[i][0]->SetTitle(HistTitle.c_str());
	  (it->second).Hist2DList[i][0]->SetName(HistName.c_str());
	}
      }
    }
  }

  ExpStatus=2;
  return 0;
}

/***********************************************************************************/
int Analyzer::PutBackOneCombHistograms(int TargetIndex)
{
  if (ExpStatus==0){
    InitToNullExp(); //Initialize array pointers for each hist unit to NULL.
  }
  int i=0;
  int j=0;
  int k=0;
  HistDim1=TargetIndex+1;
  HistDim2=1;
  map <string,HistUnit>::iterator it;
  string HistName;
  string HistTitle;
  for (it = ExpHistBox.begin();it != ExpHistBox.end();++it){
    if((it->second).Enabled){//and if CombExpHistBox[it->first].Enabled
      if((it->second).Dim==1){
			HistName = "Exp" + it->first;
			HistName+=Form("_Cond%d,%d",TargetIndex,j);
			HistTitle = (it->second).Title;
			HistTitle+=Form(": Condition%d,%d",TargetIndex,j);
			(it->second).Hist1DList[TargetIndex][0] = (TH1*)CombExpHistBox[it->first].Hist1DList[0][0]->Clone();
			(it->second).Hist1DList[TargetIndex][0]->SetTitle(HistTitle.c_str());
			(it->second).Hist1DList[TargetIndex][0]->SetName(HistName.c_str());
      }else if((it->second).Dim==2){
			for(int i=0;i<CombHistDim;i++){
			  HistName = "Exp" + it->first;
			  HistName+=Form("_Cond%d,%d",TargetIndex,j);
			  HistTitle = (it->second).Title;
			  HistTitle+=Form(": Condition%d,%d",TargetIndex,j);
			  (it->second).Hist2DList[TargetIndex][0] = (TH2*)CombExpHistBox[it->first].Hist2DList[0][0]->Clone();
			  (it->second).Hist2DList[TargetIndex][0]->SetTitle(HistTitle.c_str());
			  (it->second).Hist2DList[TargetIndex][0]->SetName(HistName.c_str());
			}
      }
    }
  }
  //Put back Bkg histograms
  for (it = BkgHistBox.begin();it != BkgHistBox.end();++it){
    if((it->second).Enabled){
      if((it->second).Dim==1){
			HistName = "Bkg" + it->first;
			HistName+=Form("_Cond%d,%d",TargetIndex,j);
			HistTitle = (it->second).Title;
			HistTitle+=Form(": Condition%d,%d",TargetIndex,j);
			cout<<"Make it here."<<endl;
			(it->second).Hist1DList[TargetIndex][0] = (TH1*)CombBkgHistBox[it->first].Hist1DList[0][0]->Clone();
			cout<<"Make it here."<<endl;
			(it->second).Hist1DList[TargetIndex][0]->SetTitle(HistTitle.c_str());
			(it->second).Hist1DList[TargetIndex][0]->SetName(HistName.c_str());
      }else if((it->second).Dim==2){
			for(int i=0;i<CombHistDim;i++){
			  HistName = "Bkg" + it->first;
			  HistName+=Form("_Cond%d,%d",TargetIndex,j);
			  HistTitle = (it->second).Title;
			  HistTitle+=Form(": Condition%d,%d",TargetIndex,j);
			  (it->second).Hist2DList[TargetIndex][0] = (TH2*)CombBkgHistBox[it->first].Hist2DList[0][0]->Clone();
			  (it->second).Hist2DList[TargetIndex][0]->SetTitle(HistTitle.c_str());
			  (it->second).Hist2DList[TargetIndex][0]->SetName(HistName.c_str());
			}
      }
    }
  }
  ExpStatus=2;
  return 0;
}

/***********************************************************************************/
/***********************************************************************************/
int Analyzer::AddSlicedExpHist(string HistName,string ConditionName)
{
  if (SlicedExpHistList.find(HistName)!=SlicedExpHistList.end()){
    cout << HistName << " is already in the sliced histogram list.\n" <<endl;
    return 1;
  }
  if (ExpHistBox.find(HistName)==ExpHistBox.end()){
    cout << HistName << " is not an allowed histogram name. Check allowed histogram names first." <<endl;
    return -1;
  }
  SlicedExpHistList[HistName] = ConditionName;
  ExpHistBox[HistName].MultiSlice = true;
  if (ExpStatus!=0){
    cout << "Histograms are already initialized before this change. Please reinitialize them!\n";
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::ClearSlicedExpHistList()
{
  if (ExpStatus!=0){
    cout << "Histograms are not deleted. Please delete them first!\n";
    return -1;
  }
  map <string, string>::iterator it;
  for (it=SlicedExpHistList.begin();it!=SlicedExpHistList.end();++it){
    ExpHistBox[it->first].MultiSlice = false;
  }
  SlicedExpHistList.clear();
  return 0;
}

/***********************************************************************************
int Analyzer::FillSliceHistograms(int i,int j,InDataStructDouble InDataDouble)
{
  string HistName;
  string ConditionName;
  map<string,string>::iterator it;
  int k=0;
  for (it=SlicedExpHistList.begin();it!=SlicedExpHistList.end();++it){
    HistName = it->first;
    ConditionName = it->second;
    if (ConditionName.compare("MCP_Rings")==0){
      double R = sqrt(pow(InDataDouble.HitPos[0]-MCP_X,2.0) + pow(InDataDouble.HitPos[1]-MCP_Y,2.0));
      for (k=0;k<nSlice;k++){
	if (R<SliceBoundary[k+1])
	  break;
      }
    }else{
      cout << "Unidentified slice condition name "<<ConditionName<<endl;
      return -1;
    }
    if (HistName.compare("TOFHist")){
      ExpHistBox[HistName].Hist1DListSliced[i][j][k]->Fill(InDataDouble.TOF);
    }else{
      cout <<"Histogram "<<HistName<<" is not available for multi-slice fit yet."<<endl;
      return 0;
    }
  }
  return 0;
}

***********************************************************************************/
int Analyzer::ConfigExpHistogram(string Name,double xlow,double xhigh,int xNBin,double ylow,double yhigh,int yNBin)
{
  if (ExpHistBox.find(Name)==ExpHistBox.end()){
    cout << Name <<" is not found in histogram list!"<<endl;
    return -1;
  }
  ExpHistBox[Name].XBinNum = xNBin;
  ExpHistBox[Name].XRange.low = xlow;
  ExpHistBox[Name].XRange.high = xhigh;
  BkgHistBox[Name].XBinNum = xNBin;
  BkgHistBox[Name].XRange.low = xlow;
  BkgHistBox[Name].XRange.high = xhigh;
  CombExpHistBox[Name].XBinNum = xNBin;
  CombExpHistBox[Name].XRange.low = xlow;
  CombExpHistBox[Name].XRange.high = xhigh;
  CombBkgHistBox[Name].XBinNum = xNBin;
  CombBkgHistBox[Name].XRange.low = xlow;
  CombBkgHistBox[Name].XRange.high = xhigh;
  if (ExpHistBox[Name].Dim==2){
    ExpHistBox[Name].YBinNum = yNBin;
    ExpHistBox[Name].YRange.low = ylow;
    ExpHistBox[Name].YRange.high = yhigh;
    BkgHistBox[Name].YBinNum = yNBin;
    BkgHistBox[Name].YRange.low = ylow;
    BkgHistBox[Name].YRange.high = yhigh;
    CombExpHistBox[Name].YBinNum = yNBin;
    CombExpHistBox[Name].YRange.low = ylow;
    CombExpHistBox[Name].YRange.high = yhigh;
    CombBkgHistBox[Name].YBinNum = yNBin;
    CombBkgHistBox[Name].YRange.low = ylow;
    CombBkgHistBox[Name].YRange.high = yhigh;
  }
  if (Name.compare("TOFHist")==0){
    TOF_High = int(xhigh);
    TOF_Low =  int(xlow);
    TOF_N = xNBin;
  }
  return 0;
}
/************************************************************************************/









