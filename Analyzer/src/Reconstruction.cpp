/*
 * =====================================================================================
 *
 *       Filename:  Reconstruction.cpp
 *
 *    Description:  contains reconstruction methods, and parameter processings
 *
 *        Version:  1.0
 *        Created:  09/25/2014 16:09:50
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Ran Hong 
 *   Organization:  CENPA
 *
 * =====================================================================================
 */

//C++ includes
#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <stdint.h>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
#include "TRandom3.h"
#include "TTree.h"
#include "TGraphErrors.h"
#include "TH1.h"
#include "TF1.h"
#include "TRandom3.h"
//Analyzer includes
#include "Analyzer.h"
#include "Calibration.h"
#include "DataStruct.h"
#include "GlobalConstants.h"

/***********************************************************************************/
//Parameter related
int Analyzer::InitParameters()
{
  SysWParameters["MWPCA_Scale"].push_back(1.0);
  SysWParameters["MWPCC_Scale"].push_back(1.0);
  SysWParameters["MWPCX_Scale"].push_back(1.0);
  SysWParameters["MWPCY_Scale"].push_back(1.0);
  SysWParameters["MWPC_Res"].push_back(0.0);
  SysWParameters["MWPCC_Threshold"].push_back(0.008);
  SysWParameters["MCPEffMap_Scale"].push_back(1.0);
  SysWParameters["E_Field"].push_back(0.155);
  SysWParameters["Z_Pos"].push_back(89.008);
  SysWParameters["TOFShift"].push_back(0.0);
  SysWParameters["TOFBound12"].push_back(193.0);
  SysWParameters["TOFBound23"].push_back(149.0);
  //Configure loop
  ParameterLoopIndex["MWPCA_Scale"]=0;
  ParameterLoopIndex["MWPCC_Scale"]=0;
  ParameterLoopIndex["MWPCX_Scale"]=0;
  ParameterLoopIndex["MWPCY_Scale"]=0;
  ParameterLoopIndex["MWPC_Res"]=0;
  ParameterLoopIndex["MWPCC_Threshold"]=0;
  ParameterLoopIndex["MCPEffMap_Scale"]=0;
  ParameterLoopIndex["E_Field"]=0;
  ParameterLoopIndex["Z_Pos"]=0;
  ParameterLoopIndex["TOFShift"]=0;
  ParameterLoopIndex["TOFBound12"]=0;
  ParameterLoopIndex["TOFBound23"]=0;
  return 0;
}

/***********************************************************************************/
int Analyzer::ResetParameters()
{
  map <string,vector<double> >::iterator it;
  for (it=SysWParameters.begin();it!=SysWParameters.end();it++){
    (it->second).clear();
  }
  nParameter1=nParameter2=1;
  InitParameters();
  return 0;
}
/***********************************************************************************/
int Analyzer::InitParameterArray(string Name, int N, double low, double interval)
{
  if (SysWParameters.find(Name)==SysWParameters.end()){
    cout << "Unidentified parameter "<<Name<<endl;
    return -1;
  }else{
    SysWParameters[Name].clear();
  }
  for (int i=0;i<N;i++){
    SysWParameters[Name].push_back(low+i*interval);
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::SetParameter(string Name, int N, double val)
{
  int n;
  if (SysWParameters.find(Name)==SysWParameters.end()){
    cout << "Unidentified parameter "<<Name<<endl;
    return -1;
  }else{
    n = SysWParameters[Name].size();
    if (N>=n){
      cout << "The parameter array is not long enough to have index "<<N<<endl;
      return -1;
    }else{
      SysWParameters[Name][N]=val;
      return 0;
    }
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::SetParameterLoop(string Name, int index)
{
  if (ParameterLoopIndex.find(Name)==ParameterLoopIndex.end()){
    cout << "Unkown name "<<Name<<endl;
    return -1;
  }
  if (index!=1 && index!=2){
    cout <<"Index can only be 1 or 2."<<endl;
    return -1;
  }
  ParameterLoopIndex[Name]=index;
  return 0;
}

/***********************************************************************************/
int Analyzer::UnsetParameterLoop(string Name)
{
  if (ParameterLoopIndex.find(Name)==ParameterLoopIndex.end()){
    cout << "Unkown name "<<Name<<endl;
    return -1;
  }
  ParameterLoopIndex[Name]=0;
  return 0;
}

/***********************************************************************************/
int Analyzer::UnsetParameterLoops()
{
  map <string,int>::iterator it;
  for (it = ParameterLoopIndex.begin();it != ParameterLoopIndex.end();++it){
    it->second=0;
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::ConstructParameterList()
{
  //Check parameter structures, match dimensions
  int loop1 = 0;
  int loop2 = 0;
  map <string,int>::iterator it1;
  for (it1 = ParameterLoopIndex.begin();it1 != ParameterLoopIndex.end();++it1){
    if (SysWParameters[it1->first].size()==0){
      cout << "Parameter "<<it1->first<<" is not initialized!\n";
      return -1;
    }
    if (it1->second==1){
      loop1++;
      if (loop1==1){
	nParameter1 = SysWParameters[it1->first].size();
      }else if(loop1>1){
	if (SysWParameters[it1->first].size()!=nParameter1){
	  cout << "Linked parameter lists don't have same length."<<endl;
	  return -1;
	}
      }
    }
    if (it1->second==2){ 
      loop2++;
      if (loop2==1){
	nParameter2 = SysWParameters[it1->first].size();
      }else if(loop1>1){
	if (SysWParameters[it1->first].size()!=nParameter2){
	  cout << "Linked parameter lists don't have same length."<<endl;
	  return -1;
	}
      }
    }
  }
  if (nParameter1>N_OF_HIST){
    nParameter1=N_OF_HIST;
    cout << "Warning: nParameter1 is longer than N_OF_HIST. Set to N_OF_HIST."<<endl;
  }
  if (nParameter2>N_OF_HIST){
    nParameter2=N_OF_HIST;
    cout << "Warning: nParameter2 is longer than N_OF_HIST. Set to N_OF_HIST."<<endl;
  }

  //Fill the Parameter list
  for (int i=0;i<nParameter1;i++){
    for (int j=0;j<nParameter2;j++){
      if (ParameterLoopIndex["MWPCA_Scale"]==0)SimParameterList[i][j].MWPCA_Scale=SysWParameters["MWPCA_Scale"][0];
      else if (ParameterLoopIndex["MWPCA_Scale"]==1)SimParameterList[i][j].MWPCA_Scale=SysWParameters["MWPCA_Scale"][i];
      else if (ParameterLoopIndex["MWPCA_Scale"]==2)SimParameterList[i][j].MWPCA_Scale=SysWParameters["MWPCA_Scale"][j];

      if (ParameterLoopIndex["MWPCC_Scale"]==0)SimParameterList[i][j].MWPCC_Scale=SysWParameters["MWPCC_Scale"][0];
      else if (ParameterLoopIndex["MWPCC_Scale"]==1)SimParameterList[i][j].MWPCC_Scale=SysWParameters["MWPCC_Scale"][i];
      else if (ParameterLoopIndex["MWPCC_Scale"]==2)SimParameterList[i][j].MWPCC_Scale=SysWParameters["MWPCC_Scale"][j];

      if (ParameterLoopIndex["MWPCX_Scale"]==0)SimParameterList[i][j].MWPCX_Scale=SysWParameters["MWPCX_Scale"][0];
      else if (ParameterLoopIndex["MWPCX_Scale"]==1)SimParameterList[i][j].MWPCX_Scale=SysWParameters["MWPCX_Scale"][i];
      else if (ParameterLoopIndex["MWPCX_Scale"]==2)SimParameterList[i][j].MWPCX_Scale=SysWParameters["MWPCX_Scale"][j];

      if (ParameterLoopIndex["MWPCY_Scale"]==0)SimParameterList[i][j].MWPCY_Scale=SysWParameters["MWPCY_Scale"][0];
      else if (ParameterLoopIndex["MWPCY_Scale"]==1)SimParameterList[i][j].MWPCY_Scale=SysWParameters["MWPCY_Scale"][i];
      else if (ParameterLoopIndex["MWPCY_Scale"]==2)SimParameterList[i][j].MWPCY_Scale=SysWParameters["MWPCY_Scale"][j];

      if (ParameterLoopIndex["MWPC_Res"]==0)SimParameterList[i][j].MWPC_Res=SysWParameters["MWPC_Res"][0];
      else if (ParameterLoopIndex["MWPC_Res"]==1)SimParameterList[i][j].MWPC_Res=SysWParameters["MWPC_Res"][i];
      else if (ParameterLoopIndex["MWPC_Res"]==2)SimParameterList[i][j].MWPC_Res=SysWParameters["MWPC_Res"][j];

      if (ParameterLoopIndex["MWPCC_Threshold"]==0)SimParameterList[i][j].MWPCC_Threshold=SysWParameters["MWPCC_Threshold"][0];
      else if (ParameterLoopIndex["MWPCC_Threshold"]==1)SimParameterList[i][j].MWPCC_Threshold=SysWParameters["MWPCC_Threshold"][i];
      else if (ParameterLoopIndex["MWPCC_Threshold"]==2)SimParameterList[i][j].MWPCC_Threshold=SysWParameters["MWPCC_Threshold"][j];

      if (ParameterLoopIndex["MCPEffMap_Scale"]==0)SimParameterList[i][j].MCPEffMap_Scale=SysWParameters["MCPEffMap_Scale"][0];
      else if (ParameterLoopIndex["MCPEffMap_Scale"]==1)SimParameterList[i][j].MCPEffMap_Scale=SysWParameters["MCPEffMap_Scale"][i];
      else if (ParameterLoopIndex["MCPEffMap_Scale"]==2)SimParameterList[i][j].MCPEffMap_Scale=SysWParameters["MCPEffMap_Scale"][j];

      if (ParameterLoopIndex["E_Field"]==0)SimParameterList[i][j].E_Field=SysWParameters["E_Field"][0];
      else if (ParameterLoopIndex["E_Field"]==1)SimParameterList[i][j].E_Field=SysWParameters["E_Field"][i];
      else if (ParameterLoopIndex["E_Field"]==2)SimParameterList[i][j].E_Field=SysWParameters["E_Field"][j];

      if (ParameterLoopIndex["Z_Pos"]==0)SimParameterList[i][j].Z_Pos=SysWParameters["Z_Pos"][0];
      else if (ParameterLoopIndex["Z_Pos"]==1)SimParameterList[i][j].Z_Pos=SysWParameters["Z_Pos"][i];
      else if (ParameterLoopIndex["Z_Pos"]==2)SimParameterList[i][j].Z_Pos=SysWParameters["Z_Pos"][j];

      if (ParameterLoopIndex["TOFShift"]==0)SimParameterList[i][j].TOFShift=SysWParameters["TOFShift"][0];
      else if (ParameterLoopIndex["TOFShift"]==1)SimParameterList[i][j].TOFShift=SysWParameters["TOFShift"][i];
      else if (ParameterLoopIndex["TOFShift"]==2)SimParameterList[i][j].TOFShift=SysWParameters["TOFShift"][j];

      if (ParameterLoopIndex["TOFBound12"]==0)SimParameterList[i][j].TOFBound12=SysWParameters["TOFBound12"][0];
      else if (ParameterLoopIndex["TOFBound12"]==1)SimParameterList[i][j].TOFBound12=SysWParameters["TOFBound12"][i];
      else if (ParameterLoopIndex["TOFBound12"]==2)SimParameterList[i][j].TOFBound12=SysWParameters["TOFBound12"][j];

      if (ParameterLoopIndex["TOFBound23"]==0)SimParameterList[i][j].TOFBound23=SysWParameters["TOFBound23"][0];
      else if (ParameterLoopIndex["TOFBound23"]==1)SimParameterList[i][j].TOFBound23=SysWParameters["TOFBound23"][i];
      else if (ParameterLoopIndex["TOFBound23"]==2)SimParameterList[i][j].TOFBound23=SysWParameters["TOFBound23"][j];

    }
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::ReconstructSim(int iPara,int jPara,SimRecDataStruct& SimRecData,InDataStructDouble& InDataDouble,InDataStruct InData)
{
  //Static Random generator
  static TRandom3 Rand_Rec(0);
  double aux;
  //Reads in or define calibration data
  //  double Scint_Cal = 1.0;  //QDC to keV
  //  double MWPCAnode_Cal = 1.0;       //ADC to keV
  double* Xsig[6];
  double* Ysig[6];
  Xsig[0] = &(InDataDouble.MWPC_X.AX1);
  Xsig[1] = &(InDataDouble.MWPC_X.AX2);
  Xsig[2] = &(InDataDouble.MWPC_X.AX3);
  Xsig[3] = &(InDataDouble.MWPC_X.AX4);
  Xsig[4] = &(InDataDouble.MWPC_X.AX5);
  Xsig[5] = &(InDataDouble.MWPC_X.AX6);
  Ysig[0] = &(InDataDouble.MWPC_Y.AY1);
  Ysig[1] = &(InDataDouble.MWPC_Y.AY2);
  Ysig[2] = &(InDataDouble.MWPC_Y.AY3);
  Ysig[3] = &(InDataDouble.MWPC_Y.AY4);
  Ysig[4] = &(InDataDouble.MWPC_Y.AY5);
  Ysig[5] = &(InDataDouble.MWPC_Y.AY6);

  //Set KillEvent to false
  KillEvent = false;

  if (ReadMode.compare("Beta")==0 || ReadMode.compare("Triple")==0 ||ReadMode.compare("MWPC")==0){
    //MWPC
    //Calibrate position first 

    //Summing MWPC Cathode charge, reconstruct position
    double QMWPC_cathode_X = 0.0;
    double QMWPC_cathode_Y = 0.0;
    double Q_read = 0.0;
    int XTrigger = 0;
    int YTrigger = 0;
    int NFireX=0;
    int NFireY=0;

    //Process triggered stripes
    //Sort signals according to their amplitudes
    //X
    double *ptr;
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Xsig[k])<*(Xsig[k+1])){
	  ptr = Xsig[k];
	  Xsig[k]=Xsig[k+1];
	  Xsig[k+1]=ptr;
	}
      }
    }
    //Y
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Ysig[k])<*(Ysig[k+1])){
	  ptr = Ysig[k];
	  Ysig[k]=Ysig[k+1];
	  Ysig[k+1]=ptr;
	}
      }
    }

    //Count Triggered Wires
    CountTrigger(InDataDouble.MWPC_X,InDataDouble.MWPC_Y,NFireX,NFireY,SimParameterList[iPara][jPara].MWPCC_Threshold);

    QMWPC_cathode_X = InDataDouble.MWPC_X.AX1 + InDataDouble.MWPC_X.AX2 +InDataDouble.MWPC_X.AX3 +InDataDouble.MWPC_X.AX4 +InDataDouble.MWPC_X.AX5 +InDataDouble.MWPC_X.AX6;
    QMWPC_cathode_Y = InDataDouble.MWPC_Y.AY1 + InDataDouble.MWPC_Y.AY2 +InDataDouble.MWPC_Y.AY3 +InDataDouble.MWPC_Y.AY4 +InDataDouble.MWPC_Y.AY5 +InDataDouble.MWPC_Y.AY6;

    //For simulation, the order of anode1 and 2 are flipped!!!
    if (MWPCPosCalMode.compare("Pol")==0){
      ConstructCorrectedMWPCPosPol(InDataDouble.MWPCAnodeSignal2,InDataDouble.MWPCAnodeSignal1,InDataDouble.MWPC_X,InDataDouble.MWPC_Y,SimRecData.MWPCHitPos[1],SimRecData.MWPCHitPos[0],SimRecData.MWPC_CY,Xsig,Ysig,MWPCCalPar.FitParametersA,MWPCCalPar.FitParametersX0,MWPCCalPar.FitParametersX1,MWPCCalPar.FitParametersX2,KeepLessTrigger,ExpParameterList[iPara][jPara].MWPCC_Threshold);
    }else if(MWPCPosCalMode.compare("Micro")==0){
      ConstructCorrectedMWPCPosMicro(InDataDouble.MWPCAnodeSignal2,InDataDouble.MWPCAnodeSignal1,InDataDouble.MWPC_X,InDataDouble.MWPC_Y,SimRecData.MWPCHitPos[1],SimRecData.MWPCHitPos[0],SimRecData.MWPC_CY,Xsig,Ysig,MWPCPeaksX0,MWPCPeaksX1,MWPCPeaksX2,MWPCPeaksA,MWPCExactPos0,MWPCExactPos1,MWPCExactPos2,MWPCExactPosA,KeepLessTrigger,ExpParameterList[iPara][jPara].MWPCC_Threshold);
    }else if(MWPCPosCalMode.compare("Simple")==0){//Use the crossing point given by simulation
      SimRecData.MWPCHitPos[0] = InDataDouble.MWPCHitPos[0];
      SimRecData.MWPCHitPos[1] = InDataDouble.MWPCHitPos[1]; 
      SimRecData.MWPC_CY = InDataDouble.MWPCHitPos[1];
    }else{
      cout<<"Uknown scheme "<<MWPCPosCalMode<<endl;
      return -1;
    }

    if (SimRecData.MWPCHitPos[0]<-400.0 || SimRecData.MWPCHitPos[1]<-400.0){
      KillEvent=true;
    }

    SimRecData.MWPCCathodeSignalX = QMWPC_cathode_X*3.6;
    SimRecData.MWPCCathodeSignalY = QMWPC_cathode_Y*3.6;

    SimRecData.FiredWireX = NFireX;
    SimRecData.FiredWireY = NFireY;

    //Applying scaling
    SimRecData.MWPCHitPos[0]*=SimParameterList[iPara][jPara].MWPCX_Scale;
    SimRecData.MWPCHitPos[1]*=SimParameterList[iPara][jPara].MWPCY_Scale;

    //Taking care of the efficiency map
    int iii = int((SimRecData.MWPCHitPos[0]+15)/2.0);
    int jjj = int((SimRecData.MWPCHitPos[1]+16)/2.0);
    if (iii>=0 && iii<15 && jjj>=0 && jjj<16){
      double aux = Rand_Rec.Rndm();
      if (aux>SimMWPCEffMap[iii][jjj]){
	SimRecData.MWPCHitPos[0]=-500;
	SimRecData.MWPCHitPos[1]=-500;
      }
    }

    SimRecData.MWPCHitShift[0] = SimRecData.MWPCHitPos[0] - InDataDouble.MWPCHitPos[0];
    SimRecData.MWPCHitShift[1] = SimRecData.MWPCHitPos[1] - InDataDouble.MWPCHitPos[1];

    //Calculate the angle shift
    SimRecData.COSBetaShiftAngle =(InDataDouble.InitP[0]*SimRecData.MWPCHitPos[0]+InDataDouble.InitP[1]*SimRecData.MWPCHitPos[1]+InDataDouble.InitP[2]*3.331*25.4)/(sqrt(pow(InDataDouble.InitP[0],2.0)+pow(InDataDouble.InitP[1],2.0)+pow(InDataDouble.InitP[2],2.0))*sqrt(pow(SimRecData.MWPCHitPos[0],2.0)+pow(SimRecData.MWPCHitPos[1],2.0)+pow(3.331*25.4,2.0))); 

    //Apply additional resolution
    if (SimParameterList[iPara][jPara].MWPC_Res>0.0){
      SimRecData.MWPCHitPos[0] += Rand_Rec.Gaus(0.0,SimParameterList[iPara][jPara].MWPC_Res);
      SimRecData.MWPCHitPos[1] += Rand_Rec.Gaus(0.0,SimParameterList[iPara][jPara].MWPC_Res);
      SimRecData.MWPC_CY += Rand_Rec.Gaus(0.0,SimParameterList[iPara][jPara].MWPC_Res);
    }
    //    cout << Xpos <<" "<<Ypos<<endl;

    //Calibrate Amplitude
    InDataDouble.MWPCAnodeSignal1 = InDataDouble.MWPCAnodeSignal1*SimMWPCAnode_Cal*SimParameterList[iPara][jPara].MWPCA_Scale/1.1;
    InDataDouble.MWPCAnodeSignal2 = InDataDouble.MWPCAnodeSignal2*SimMWPCAnode_Cal*SimParameterList[iPara][jPara].MWPCA_Scale/1.1;
    SimRecData.MWPCEnergy=InDataDouble.MWPCAnodeSignal1+InDataDouble.MWPCAnodeSignal2;
//    SimRecData.MWPCEnergy=InDataDouble.MWPCEnergy;
/*    for (int i=0;i<12;i++){
      InDataDouble.MWPCCathodeSignal[i]*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    }*/
    InDataDouble.MWPC_X.AX1*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    InDataDouble.MWPC_X.AX2*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    InDataDouble.MWPC_X.AX3*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    InDataDouble.MWPC_X.AX4*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    InDataDouble.MWPC_X.AX5*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    InDataDouble.MWPC_X.AX6*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    InDataDouble.MWPC_Y.AY1*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    InDataDouble.MWPC_Y.AY2*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    InDataDouble.MWPC_Y.AY3*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    InDataDouble.MWPC_Y.AY4*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    InDataDouble.MWPC_Y.AY5*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    InDataDouble.MWPC_Y.AY6*=(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);

    SimRecData.MWPCCathodeSignalX = SimRecData.MWPCCathodeSignalX*(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);
    SimRecData.MWPCCathodeSignalY = SimRecData.MWPCCathodeSignalY*(SimMWPCCathode_Cal * SimParameterList[iPara][jPara].MWPCC_Scale);

    //Applying Gain Map
    int ii = int((SimRecData.MWPCHitPos[0]+17.0)/2.0);
    int jj = int((SimRecData.MWPCHitPos[1]+18.0)/2.0);
    if (ii>=0 && ii<17 && jj>=0 && jj<18){
      double GainFactor = SimMWPCGainMap[ii][jj];
      InDataDouble.MWPCAnodeSignal1/=GainFactor;
      InDataDouble.MWPCAnodeSignal2/=GainFactor;
      SimRecData.MWPCEnergy/=GainFactor;
      SimRecData.MWPCCathodeSignalX/=GainFactor;
      SimRecData.MWPCCathodeSignalY/=GainFactor;
    }
    //Rule out mode MWPC
    if (ReadMode.compare("MWPC")!=0){
      //Copy other data from InDataDouble, and calibrate
      SimRecData.ScintEnergy = InDataDouble.ScintEnergy;     //in keV
      SimRecData.ExitAngle_Be = InDataDouble.ExitAngle_Be;           // deg
      for (int i=0;i<3;i++){
	SimRecData.InitP[i] = InDataDouble.InitP[i]; //Initial momentum
      }
      for (int i=0;i<3;i++){
	SimRecData.DECPos[i] = InDataDouble.DECPos[i]; //Initial decay position
      }
      SimRecData.ParticleID = InData.ParticleID;   //0:electron 1:photon
      SimRecData.Hits = InData.Hits;                //Hits bitmap: 0 MCP Wall Ele Col Win MWPC Sci
    }
  }

  if (ReadMode.compare("MCP")==0 || ReadMode.compare("Triple")==0 ||ReadMode.compare("He4")==0){
    for (int i=0;i<3;i++){
      SimRecData.HitPos[i] = InDataDouble.HitPos[i];//in mm
    }

    //MCP acceptance map, square regions
    if (MCPPosCalMode.compare("Local")==0){
      int ii = int((SimRecData.HitPos[0]+34.0)/4.0);
      int jj = int((SimRecData.HitPos[1]+34.0)/4.0);
      if (SimRecData.HitPos[0]<-34.0)ii=-1;
      if (SimRecData.HitPos[1]<-34.0)jj=-1;
      if (ii<0 || ii>=MCPsizeX || jj<0 || jj>=MCPsizeY){
	KillEvent=true;
	for (int i=0;i<3;i++){
	  SimRecData.HitPos[i] = -100.0;	//some meaningless number
	}
      }else{
	if (MCPSquareArray[ii][jj].status!=1){
	  KillEvent=true;
	  for (int i=0;i<3;i++){
	    SimRecData.HitPos[i] = -100.0;	//some meaningless number
	  }
	}else{
	  if (SimCalSwitches["MCPEffMap"]){
	    aux = Rand_Rec.Rndm();
	  }else{
	    aux=0.0;
	  }
	  if (aux>1.0-(1.0-MCPEffMap[ii][jj])*SimParameterList[iPara][jPara].MCPEffMap_Scale){
	    KillEvent=true;
	    for (int i=0;i<3;i++){
	      SimRecData.HitPos[i] = -100.0;	//some meaningless number
	    }
	  }
	}

      }
    }
    //Copy other data from InDataDouble, and calibrate
    SimRecData.TOF = InDataDouble.TOF+SimParameterList[iPara][jPara].TOFShift;             //in ns
    SimRecData.HitAngle = InDataDouble.HitAngle;
    SimRecData.IonEnergy = InDataDouble.IonEnergy;
    SimRecData.IonStatus = InData.IonStatus;
    SimRecData.EIonInit = InDataDouble.EIonInit;
    //SimRecData.EIonInit = 0.5*(MASS_LI6 - MASS_E)*1000.0*(InDataDouble.V_Ion[0]*InDataDouble.V_Ion[0]+InDataDouble.V_Ion[1]*InDataDouble.V_Ion[1]+InDataDouble.V_Ion[2]*InDataDouble.V_Ion[2])/CSPEED/CSPEED;//keV 
  }
  //Reconstruct Full Kinematics and Q
  if (ReadMode.compare("Triple")==0){
    //double d_MWPC = 3.331*25.4;	//Distance from MOT to MWPC anode
    double d_MWPC = 80.94;	//in mm, New geometry, Distance from MOT to MWPC anode
    double M_Li = 1000*(MASS_LI6 - MASS_E);	//Li ion mass, in keV
    double m_e = 1000*MASS_E; //in keV
    double P_e[3];
    double P_R[3];
    double P_v[3];

    //Calculate P_e
    //Rotate back MWPC coordinate
    double MWPCRot = 30.0/180.0*PI;
    double X0 = SimRecData.MWPCHitPos[0]*cos(MWPCRot)+SimRecData.MWPCHitPos[1]*sin(MWPCRot);
    double Y0 = -SimRecData.MWPCHitPos[0]*sin(MWPCRot)+SimRecData.MWPCHitPos[1]*cos(MWPCRot);
    P_e[0] = X0;
    P_e[1] = Y0;
    P_e[2] = d_MWPC;

    double P_e_norm = sqrt(pow(P_e[0],2.0)+pow(P_e[1],2.0)+pow(P_e[2],2.0));
    for (int i=0;i<3;i++){
      P_e[i] = P_e[i]/P_e_norm*sqrt(pow(SimRecData.ScintEnergy,2.0)+2.0*m_e*SimRecData.ScintEnergy);
    }
    //Update P_e_norm
    P_e_norm = sqrt(pow(P_e[0],2.0)+pow(P_e[1],2.0)+pow(P_e[2],2.0));

    //Calculate P_R
    double c_light = CSPEED;	//mm/ns
    double Q_Charge = 1.0;
    double E_field = SimParameterList[iPara][jPara].E_Field;	//kV/mm
    double d_MCP = SimParameterList[iPara][jPara].Z_Pos;	//mm
    double TOFBound12 = SimParameterList[iPara][jPara].TOFBound12;
    //Rotate Back the MWPC
    double MCPRot = 22.5/180.0*PI;
    double XMCP0 = SimRecData.HitPos[0]*cos(MCPRot)+SimRecData.HitPos[1]*sin(MCPRot);
    double YMCP0 = -SimRecData.HitPos[0]*sin(MCPRot)+SimRecData.HitPos[1]*cos(MCPRot);
    P_R[0] = M_Li*XMCP0/(SimRecData.TOF+0.450)/c_light;
    P_R[1] = M_Li*YMCP0/(SimRecData.TOF+0.450)/c_light;
    P_R[2] = 0.5*E_field*Q_Charge*(SimRecData.TOF+0.450)*c_light-M_Li*d_MCP/(SimRecData.TOF+0.450)/c_light;

    if (SimRecData.TOF>TOFBound12)SimRecData.EIon[0] = (P_R[0]*P_R[0]+P_R[1]*P_R[1]+P_R[2]*P_R[2])/2.0/M_Li;
    else SimRecData.EIon[0]=-100;

    //  cout << P_R[0]<<endl;
    //Reconstruct Neutrino momentum
    for (int i=0;i<3;i++){
      P_v[i] = -P_e[i] - P_R[i];
    }
    double E_v = sqrt(pow(P_v[0],2.0)+pow(P_v[1],2.0)+pow(P_v[2],2.0));
    SimRecData.Q_Value[0] = E_v+SimRecData.ScintEnergy;
    //Reconstruct Correlation
    double CosTheta1 = (P_e[0]*P_v[0]+P_e[1]*P_v[1]+P_e[2]*P_v[2])/E_v/P_e_norm;

    //Charge State 2
    Q_Charge = 2.0;
    P_R[0] = M_Li*SimRecData.HitPos[0]/(SimRecData.TOF+0.450)/c_light;
    P_R[1] = M_Li*SimRecData.HitPos[1]/(SimRecData.TOF+0.450)/c_light;
    P_R[2] = 0.5*E_field*Q_Charge*(SimRecData.TOF+0.450)*c_light-M_Li*d_MCP/(SimRecData.TOF+0.450)/c_light;

    if (SimRecData.TOF<TOFBound12)SimRecData.EIon[1] = (P_R[0]*P_R[0]+P_R[1]*P_R[1]+P_R[2]*P_R[2])/2.0/M_Li;
    else SimRecData.EIon[1]=-100;

    //  cout <<RecData.MCPPos_X<<" "<<RecData.TOF<<" ";
    //  cout <<P_R[0]<<endl;
    //Reconstruct Neutrino momentum
    for (int i=0;i<3;i++){
      P_v[i] = -P_e[i] - P_R[i];
    }
    E_v = sqrt(pow(P_v[0],2.0)+pow(P_v[1],2.0)+pow(P_v[2],2.0));
    SimRecData.Q_Value[1] = E_v+SimRecData.ScintEnergy;
    //Reconstruct Correlation
    double CosTheta2 = (P_e[0]*P_v[0]+P_e[1]*P_v[1]+P_e[2]*P_v[2])/E_v/P_e_norm;
    
    //Charge State 3
    Q_Charge = 3.0;
    P_R[0] = M_Li*SimRecData.HitPos[0]/(SimRecData.TOF+0.450)/c_light;
    P_R[1] = M_Li*SimRecData.HitPos[1]/(SimRecData.TOF+0.450)/c_light;
    P_R[2] = 0.5*E_field*Q_Charge*(SimRecData.TOF+0.450)*c_light-M_Li*d_MCP/(SimRecData.TOF+0.450)/c_light;

    //  cout <<RecData.MCPPos_X<<" "<<RecData.TOF<<" ";
    //  cout <<P_R[0]<<endl;
    //Reconstruct Neutrino momentum
    for (int i=0;i<3;i++){
      P_v[i] = -P_e[i] - P_R[i];
    }
    E_v = sqrt(pow(P_v[0],2.0)+pow(P_v[1],2.0)+pow(P_v[2],2.0));
    SimRecData.Q_Value[2] = E_v+SimRecData.ScintEnergy;

    //Make a condition for TOF, deciding if it is in the charge state1
    if (SimRecData.TOF>TOFBound12)SimRecData.CosThetaENu=CosTheta1;
    else SimRecData.CosThetaENu=CosTheta2;
    //  cout <<RecData.Q_Value[0]<<" "<<RecData.Q_Value[1]<<endl;
  }else if(ReadMode.compare("Double")==0){
    double d_MWPC = 3.331*25.4;	//Distance from MOT to MWPC anode
    double M_Li = 1000*(MASS_LI6 - MASS_E);	//Li ion mass, in keV
    double m_e = 1000*MASS_E; //in keV
    double P_e[3];
    double P_R[3];
    double P_v[3];

    //Calculate P_e
    //Rotate back MWPC coordinate
//    double MWPCRot = 30.0/180.0*PI;
//    double X0 = SimRecData.MWPCHitPos[0]*cos(MWPCRot)+SimRecData.MWPCHitPos[1]*sin(MWPCRot);
//    double Y0 = -SimRecData.MWPCHitPos[0]*sin(MWPCRot)+SimRecData.MWPCHitPos[1]*cos(MWPCRot);
    P_e[0] = 0;
    P_e[1] = 0;
    P_e[2] = d_MWPC;

    double P_e_norm = sqrt(pow(P_e[0],2.0)+pow(P_e[1],2.0)+pow(P_e[2],2.0));
    for (int i=0;i<3;i++){
      P_e[i] = P_e[i]/P_e_norm*sqrt(pow(SimRecData.ScintEnergy,2.0)+2.0*m_e*SimRecData.ScintEnergy);
    }
    //Update P_e_norm
    P_e_norm = sqrt(pow(P_e[0],2.0)+pow(P_e[1],2.0)+pow(P_e[2],2.0));

    //Calculate P_R
    double c_light = CSPEED;	//mm/ns
    double Q_Charge = 1.0;
    double E_field = SimParameterList[iPara][jPara].E_Field;	//kV/mm
    double d_MCP = SimParameterList[iPara][jPara].Z_Pos;	//mm
    double TOFBound12 = SimParameterList[iPara][jPara].TOFBound12;
    //Rotate Back the MWPC
    double MCPRot = 22.5/180.0*PI;
    double XMCP0 = SimRecData.HitPos[0]*cos(MCPRot)+SimRecData.HitPos[1]*sin(MCPRot);
    double YMCP0 = -SimRecData.HitPos[0]*sin(MCPRot)+SimRecData.HitPos[1]*cos(MCPRot);
    P_R[0] = M_Li*XMCP0/(SimRecData.TOF+0.450)/c_light;
    P_R[1] = M_Li*YMCP0/(SimRecData.TOF+0.450)/c_light;
    P_R[2] = 0.5*E_field*Q_Charge*(SimRecData.TOF+0.450)*c_light-M_Li*d_MCP/(SimRecData.TOF+0.450)/c_light;

    if (SimRecData.TOF>TOFBound12)SimRecData.EIon[0] = (P_R[0]*P_R[0]+P_R[1]*P_R[1]+P_R[2]*P_R[2])/2.0/M_Li;
    else SimRecData.EIon[0]=-100;

    //  cout << P_R[0]<<endl;
    //Reconstruct Neutrino momentum
    for (int i=0;i<3;i++){
      P_v[i] = -P_e[i] - P_R[i];
    }
    double E_v = sqrt(pow(P_v[0],2.0)+pow(P_v[1],2.0)+pow(P_v[2],2.0));
    SimRecData.Q_Value[0] = E_v+SimRecData.ScintEnergy;
    //Reconstruct Correlation
    double CosTheta1 = (P_e[0]*P_v[0]+P_e[1]*P_v[1]+P_e[2]*P_v[2])/E_v/P_e_norm;

    //Charge State 2
    Q_Charge = 2.0;
    P_R[0] = M_Li*SimRecData.HitPos[0]/(SimRecData.TOF+0.450)/c_light;
    P_R[1] = M_Li*SimRecData.HitPos[1]/(SimRecData.TOF+0.450)/c_light;
    P_R[2] = 0.5*E_field*Q_Charge*(SimRecData.TOF+0.450)*c_light-M_Li*d_MCP/(SimRecData.TOF+0.450)/c_light;

    if (SimRecData.TOF<TOFBound12)SimRecData.EIon[1] = (P_R[0]*P_R[0]+P_R[1]*P_R[1]+P_R[2]*P_R[2])/2.0/M_Li;
    else SimRecData.EIon[1]=-100;

    //  cout <<RecData.MCPPos_X<<" "<<RecData.TOF<<" ";
    //  cout <<P_R[0]<<endl;
    //Reconstruct Neutrino momentum
    for (int i=0;i<3;i++){
      P_v[i] = -P_e[i] - P_R[i];
    }
    E_v = sqrt(pow(P_v[0],2.0)+pow(P_v[1],2.0)+pow(P_v[2],2.0));
    SimRecData.Q_Value[1] = E_v+SimRecData.ScintEnergy;
    //Reconstruct Correlation
    double CosTheta2 = (P_e[0]*P_v[0]+P_e[1]*P_v[1]+P_e[2]*P_v[2])/E_v/P_e_norm;
    
    //Charge State 3
    Q_Charge = 3.0;
    P_R[0] = M_Li*SimRecData.HitPos[0]/(SimRecData.TOF+0.450)/c_light;
    P_R[1] = M_Li*SimRecData.HitPos[1]/(SimRecData.TOF+0.450)/c_light;
    P_R[2] = 0.5*E_field*Q_Charge*(SimRecData.TOF+0.450)*c_light-M_Li*d_MCP/(SimRecData.TOF+0.450)/c_light;

    //  cout <<RecData.MCPPos_X<<" "<<RecData.TOF<<" ";
    //  cout <<P_R[0]<<endl;
    //Reconstruct Neutrino momentum
    for (int i=0;i<3;i++){
      P_v[i] = -P_e[i] - P_R[i];
    }
    E_v = sqrt(pow(P_v[0],2.0)+pow(P_v[1],2.0)+pow(P_v[2],2.0));
    SimRecData.Q_Value[2] = E_v+SimRecData.ScintEnergy;

    //Make a condition for TOF, deciding if it is in the charge state1
    if (SimRecData.TOF>TOFBound12)SimRecData.CosThetaENu=CosTheta1;
    else SimRecData.CosThetaENu=CosTheta2;
      cout <<SimRecData.Q_Value[0]<<" "<<SimRecData.Q_Value[1]<<endl;

  }else{
    SimRecData.Q_Value[0]=3500;
    SimRecData.Q_Value[1]=3500;
  }
  return 0;
}

/*****************Experiment parameters************************************/
//Parameter related
int Analyzer::InitExpParameters()
{
  ExpParameters["ScintA_Scale"].push_back(1.0);
  ExpParameters["MWPCA_Scale"].push_back(1.0);
  ExpParameters["MWPCC_Scale"].push_back(1.0);
  ExpParameters["MWPCX_Scale"].push_back(1.0);
  ExpParameters["MWPCY_Scale"].push_back(1.0);
  ExpParameters["MWPCC_Threshold"].push_back(0.02);
  ExpParameters["TOFShift"].push_back(0.0);
  ExpParameters["MCPXShift"].push_back(0.0);
  ExpParameters["MCPYShift"].push_back(0.0);
  ExpParameters["E_Field"].push_back(0.155);
  ExpParameters["Z_Pos"].push_back(89.008);
  ExpParameters["TOFBound12"].push_back(191.0);
  ExpParameters["TOFBound23"].push_back(149.0);
  //Configure loop
  ExpParameterLoopIndex["ScintA_Scale"]=0;
  ExpParameterLoopIndex["MWPCA_Scale"]=0;
  ExpParameterLoopIndex["MWPCC_Scale"]=0;
  ExpParameterLoopIndex["MWPCX_Scale"]=0;
  ExpParameterLoopIndex["MWPCY_Scale"]=0;
  ExpParameterLoopIndex["MWPCC_Threshold"]=0;
  ExpParameterLoopIndex["TOFShift"]=0;
  ExpParameterLoopIndex["MCPXShift"]=0;
  ExpParameterLoopIndex["MCPYShift"]=0;
  ExpParameterLoopIndex["E_Field"]=0;
  ExpParameterLoopIndex["Z_Pos"]=0;
  ExpParameterLoopIndex["TOFBound12"]=0;
  ExpParameterLoopIndex["TOFBound23"]=0;
  return 0;
}

/***********************************************************************************/
int Analyzer::ResetExpParameters()
{
  map <string,vector<double> >::iterator it;
  for (it=ExpParameters.begin();it!=ExpParameters.end();it++){
    (it->second).clear();
  }
  nParameterExp1=nParameterExp2=1;
  InitParameters();
  return 0;
}
/***********************************************************************************/
int Analyzer::InitExpParameterArray(string Name, int N, double low, double interval)
{
  if (ExpParameters.find(Name)==ExpParameters.end()){
    cout << "Unidentified parameter "<<Name<<endl;
    return -1;
  }else{
    ExpParameters[Name].clear();
  }
  for (int i=0;i<N;i++){
    ExpParameters[Name].push_back(low+i*interval);
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::SetExpParameter(string Name, int N, double val)
{
  int n;
  if (ExpParameters.find(Name)==ExpParameters.end()){
    cout << "Unidentified parameter "<<Name<<endl;
    return -1;
  }else{
    n = ExpParameters[Name].size();
    if (N>=n){
      cout << "The parameter array is not long enough to have index "<<N<<endl;
      return -1;
    }else{
      ExpParameters[Name][N]=val;
      return 0;
    }
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::SetExpParameterLoop(string Name, int index)
{
  if (ExpParameterLoopIndex.find(Name)==ExpParameterLoopIndex.end()){
    cout << "Unkown name "<<Name<<endl;
    return -1;
  }
  if (index!=1 && index!=2){
    cout <<"Index can only be 1 or 2."<<endl;
    return -1;
  }
  ExpParameterLoopIndex[Name]=index;
  return 0;
}

/***********************************************************************************/
int Analyzer::UnsetExpParameterLoop(string Name)
{
  if (ExpParameterLoopIndex.find(Name)==ExpParameterLoopIndex.end()){
    cout << "Unkown name "<<Name<<endl;
    return -1;
  }
  ExpParameterLoopIndex[Name]=0;
  return 0;
}

/***********************************************************************************/
int Analyzer::UnsetExpParameterLoops()
{
  map <string,int>::iterator it;
  for (it = ExpParameterLoopIndex.begin();it != ExpParameterLoopIndex.end();++it){
    it->second=0;
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::ConstructExpParameterList()
{
  //Check parameter structures, match dimensions
  int loop1 = 0;
  int loop2 = 0;
  map <string,int>::iterator it1;
  for (it1 = ExpParameterLoopIndex.begin();it1 != ExpParameterLoopIndex.end();++it1){
    if (ExpParameters[it1->first].size()==0){
      cout << "Parameter "<<it1->first<<" is not initialized!\n";
      return -1;
    }
    if (it1->second==1){ 
      loop1++;
      if (loop1==1){
	nParameterExp1 = ExpParameters[it1->first].size();
      }else if(loop1>1){
	if (ExpParameters[it1->first].size()!=nParameterExp1){
	  cout << "Linked parameter lists don't have same length."<<endl;
	  return -1;
	}
      }
    }
    if (it1->second==2){
      loop2++;
      if (loop2==1){
	nParameterExp2 = ExpParameters[it1->first].size();
      }else if(loop2>1){
	if (ExpParameters[it1->first].size()!=nParameterExp2){
	  cout << "Linked parameter lists don't have same length."<<endl;
	  return -1;
	}
      }
    }
  }
  if (nParameterExp1>N_OF_HIST){
    nParameterExp1=N_OF_HIST;
    cout << "Warning: nParameterExp1 is longer than N_OF_HIST. Set to N_OF_HIST."<<endl;
  }
  if (nParameterExp2>N_OF_HIST){
    nParameterExp2=N_OF_HIST;
    cout << "Warning: nParameterExp2 is longer than N_OF_HIST. Set to N_OF_HIST."<<endl;
  }

  //Fill the Parameter list
  for (int i=0;i<nParameterExp1;i++){
    for (int j=0;j<nParameterExp2;j++){
      if (ExpParameterLoopIndex["ScintA_Scale"]==0)ExpParameterList[i][j].ScintA_Scale=ExpParameters["ScintA_Scale"][0];
      else if (ExpParameterLoopIndex["ScintA_Scale"]==1)ExpParameterList[i][j].ScintA_Scale=ExpParameters["ScintA_Scale"][i];
      else if (ExpParameterLoopIndex["ScintA_Scale"]==2)ExpParameterList[i][j].ScintA_Scale=ExpParameters["ScintA_Scale"][j];

      if (ExpParameterLoopIndex["MWPCA_Scale"]==0)ExpParameterList[i][j].MWPCA_Scale=ExpParameters["MWPCA_Scale"][0];
      else if (ExpParameterLoopIndex["MWPCA_Scale"]==1)ExpParameterList[i][j].MWPCA_Scale=ExpParameters["MWPCA_Scale"][i];
      else if (ExpParameterLoopIndex["MWPCA_Scale"]==2)ExpParameterList[i][j].MWPCA_Scale=ExpParameters["MWPCA_Scale"][j];

      if (ExpParameterLoopIndex["MWPCC_Scale"]==0)ExpParameterList[i][j].MWPCC_Scale=ExpParameters["MWPCC_Scale"][0];
      else if (ExpParameterLoopIndex["MWPCC_Scale"]==1)ExpParameterList[i][j].MWPCC_Scale=ExpParameters["MWPCC_Scale"][i];
      else if (ExpParameterLoopIndex["MWPCC_Scale"]==2)ExpParameterList[i][j].MWPCC_Scale=ExpParameters["MWPCC_Scale"][j];

      if (ExpParameterLoopIndex["MWPCX_Scale"]==0)ExpParameterList[i][j].MWPCX_Scale=ExpParameters["MWPCX_Scale"][0];
      else if (ExpParameterLoopIndex["MWPCX_Scale"]==1)ExpParameterList[i][j].MWPCX_Scale=ExpParameters["MWPCX_Scale"][i];
      else if (ExpParameterLoopIndex["MWPCX_Scale"]==2)ExpParameterList[i][j].MWPCX_Scale=ExpParameters["MWPCX_Scale"][j];

      if (ExpParameterLoopIndex["MWPCY_Scale"]==0){
	ExpParameterList[i][j].MWPCY_Scale=ExpParameters["MWPCY_Scale"][0];
      }
      else if (ExpParameterLoopIndex["MWPCY_Scale"]==1)ExpParameterList[i][j].MWPCY_Scale=ExpParameters["MWPCY_Scale"][i];
      else if (ExpParameterLoopIndex["MWPCY_Scale"]==2)ExpParameterList[i][j].MWPCY_Scale=ExpParameters["MWPCY_Scale"][j];

      if (ExpParameterLoopIndex["MWPCC_Threshold"]==0)ExpParameterList[i][j].MWPCC_Threshold=ExpParameters["MWPCC_Threshold"][0];
      else if (ExpParameterLoopIndex["MWPCC_Threshold"]==1)ExpParameterList[i][j].MWPCC_Threshold=ExpParameters["MWPCC_Threshold"][i];
      else if (ExpParameterLoopIndex["MWPCC_Threshold"]==2)ExpParameterList[i][j].MWPCC_Threshold=ExpParameters["MWPCC_Threshold"][j];

      if (ExpParameterLoopIndex["TOFShift"]==0)ExpParameterList[i][j].TOFShift=ExpParameters["TOFShift"][0];
      else if (ExpParameterLoopIndex["TOFShift"]==1)ExpParameterList[i][j].TOFShift=ExpParameters["TOFShift"][i];
      else if (ExpParameterLoopIndex["TOFShift"]==2)ExpParameterList[i][j].TOFShift=ExpParameters["TOFShift"][j];

      if (ExpParameterLoopIndex["MCPXShift"]==0)ExpParameterList[i][j].MCPXShift=ExpParameters["MCPXShift"][0];
      else if (ExpParameterLoopIndex["MCPXShift"]==1)ExpParameterList[i][j].MCPXShift=ExpParameters["MCPXShift"][i];
      else if (ExpParameterLoopIndex["MCPXShift"]==2)ExpParameterList[i][j].MCPXShift=ExpParameters["MCPXShift"][j];

      if (ExpParameterLoopIndex["MCPYShift"]==0)ExpParameterList[i][j].MCPYShift=ExpParameters["MCPYShift"][0];
      else if (ExpParameterLoopIndex["MCPYShift"]==1)ExpParameterList[i][j].MCPYShift=ExpParameters["MCPYShift"][i];
      else if (ExpParameterLoopIndex["MCPYShift"]==2)ExpParameterList[i][j].MCPYShift=ExpParameters["MCPYShift"][j];

      if (ExpParameterLoopIndex["E_Field"]==0)ExpParameterList[i][j].E_Field=ExpParameters["E_Field"][0];
      else if (ExpParameterLoopIndex["E_Field"]==1)ExpParameterList[i][j].E_Field=ExpParameters["E_Field"][i];
      else if (ExpParameterLoopIndex["E_Field"]==2)ExpParameterList[i][j].E_Field=ExpParameters["E_Field"][j];

      if (ExpParameterLoopIndex["Z_Pos"]==0)ExpParameterList[i][j].Z_Pos=ExpParameters["Z_Pos"][0];
      else if (ExpParameterLoopIndex["Z_Pos"]==1)ExpParameterList[i][j].Z_Pos=ExpParameters["Z_Pos"][i];
      else if (ExpParameterLoopIndex["Z_Pos"]==2)ExpParameterList[i][j].Z_Pos=ExpParameters["Z_Pos"][j];

      if (ExpParameterLoopIndex["TOFBound12"]==0)ExpParameterList[i][j].TOFBound12=ExpParameters["TOFBound12"][0];
      else if (ExpParameterLoopIndex["TOFBound12"]==1)ExpParameterList[i][j].TOFBound12=ExpParameters["TOFBound12"][i];
      else if (ExpParameterLoopIndex["TOFBound12"]==2)ExpParameterList[i][j].TOFBound12=ExpParameters["TOFBound12"][j];

      if (ExpParameterLoopIndex["TOFBound23"]==0)ExpParameterList[i][j].TOFBound23=ExpParameters["TOFBound23"][0];
      else if (ExpParameterLoopIndex["TOFBound23"]==1)ExpParameterList[i][j].TOFBound23=ExpParameters["TOFBound23"][i];
      else if (ExpParameterLoopIndex["TOFBound23"]==2)ExpParameterList[i][j].TOFBound23=ExpParameters["TOFBound23"][j];

    }
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::Reconstruct(int iPara,int jPara,RecDataStruct& RecData,ExpDataStruct& ExpData)
{
  double* Xsig[6];
  double* Ysig[6];
  Xsig[0] = &(ExpData.MWPC_X.AX1);
  Xsig[1] = &(ExpData.MWPC_X.AX2);
  Xsig[2] = &(ExpData.MWPC_X.AX3);
  Xsig[3] = &(ExpData.MWPC_X.AX4);
  Xsig[4] = &(ExpData.MWPC_X.AX5);
  Xsig[5] = &(ExpData.MWPC_X.AX6);
  Ysig[0] = &(ExpData.MWPC_Y.AY1);
  Ysig[1] = &(ExpData.MWPC_Y.AY2);
  Ysig[2] = &(ExpData.MWPC_Y.AY3);
  Ysig[3] = &(ExpData.MWPC_Y.AY4);
  Ysig[4] = &(ExpData.MWPC_Y.AY5);
  Ysig[5] = &(ExpData.MWPC_Y.AY6);
  //Reads in or define calibration data
  // double ScintA_Cal = 1.0;  //QDC to keV
  // double ScintD_Cal = 1.0;  //QDC to keV
  // double MWPCAnode1_Cal = 3.0/200000.0;       //ADC to keV
  // double MWPCAnode2_Cal = 3.0/200000.0;       //ADC to keV
  double nsTomm = 40.0/80.0;	//80ns corresponds to 40mm
  double LEDScale;
  
  //Set KillEvent to false
  KillEvent = false;
  //Calibrate MWPC anode
  if (ReadMode.compare("Double")!=0 && ReadMode.compare("Scint")!=0 && ReadMode.compare("MCP")!=0 && ReadMode.compare("He4")!=0){
    RecData.EMWPC_anode1 = MWPCAnode_Cal * ExpData.AMWPC_anode1/MWPCRawCalA;
    RecData.EMWPC_anode2 = MWPCAnode_Cal * ExpData.AMWPC_anode2/MWPCRawCalA/AmpCalA[1]*AmpCalA[0];
    //    cout << RecData.EMWPC_anode1<<" "<<RecData.EMWPC_anode2<<endl;
    //    cout << RecData.EMWPC_anode1<<endl;
    //Calibrate MWPC Cathode
    //Need carefully inspection, remove  cathode calibration for the moment
    /*
    ExpData.MWPC_X.AX1/=AmpCalX[0];
    ExpData.MWPC_X.AX2/=AmpCalX[1];
    ExpData.MWPC_X.AX3/=AmpCalX[2];
    ExpData.MWPC_X.AX4/=AmpCalX[3];
    ExpData.MWPC_X.AX5/=AmpCalX[4];
    ExpData.MWPC_X.AX6/=AmpCalX[5];
    ExpData.MWPC_Y.AY1/=AmpCalY[0];
    ExpData.MWPC_Y.AY2/=AmpCalY[1];
    ExpData.MWPC_Y.AY3/=AmpCalY[2];
    ExpData.MWPC_Y.AY4/=AmpCalY[3];
    ExpData.MWPC_Y.AY5/=AmpCalY[4];
    ExpData.MWPC_Y.AY6/=AmpCalY[5];
     */
    //Process triggered stripes
    //Sort signals according to their amplitudes
    //X
    double *ptr;
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Xsig[k])<*(Xsig[k+1])){
	  ptr = Xsig[k];
	  Xsig[k]=Xsig[k+1];
	  Xsig[k+1]=ptr;
	}
      }
    }
    //Y
    for (int j=0;j<5;j++){
      for (int k=0;k<5-j;k++){
	if (*(Ysig[k])<*(Ysig[k+1])){
	  ptr = Ysig[k];
	  Ysig[k]=Ysig[k+1];
	  Ysig[k+1]=ptr;
	}
      }
    }

    int NFireX=0;
    int NFireY=0;
    //Count Triggered Wires
    CountTrigger(ExpData.MWPC_X,ExpData.MWPC_Y,NFireX,NFireY,ExpParameterList[iPara][jPara].MWPCC_Threshold);

    RecData.FiredWireX=NFireX;
    RecData.FiredWireY=NFireY;

    RecData.QMWPC_cathode_X = ExpData.MWPC_X.AX1 + ExpData.MWPC_X.AX2 +ExpData.MWPC_X.AX3 +ExpData.MWPC_X.AX4 +ExpData.MWPC_X.AX5 +ExpData.MWPC_X.AX6;
    RecData.QMWPC_cathode_Y = ExpData.MWPC_Y.AY1 + ExpData.MWPC_Y.AY2 +ExpData.MWPC_Y.AY3 +ExpData.MWPC_Y.AY4 +ExpData.MWPC_Y.AY5 +ExpData.MWPC_Y.AY6;

    if (MWPCPosCalMode.compare("Pol")==0){
      ConstructCorrectedMWPCPosPol(ExpData.AMWPC_anode1,ExpData.AMWPC_anode2,ExpData.MWPC_X,ExpData.MWPC_Y,RecData.MWPCPos_Y,RecData.MWPCPos_X,RecData.MWPCPos_Y_C,Xsig,Ysig,MWPCCalPar.FitParametersA,MWPCCalPar.FitParametersX0,MWPCCalPar.FitParametersX1,MWPCCalPar.FitParametersX2,KeepLessTrigger,ExpParameterList[iPara][jPara].MWPCC_Threshold);
    }else if(MWPCPosCalMode.compare("Micro")==0){
      ConstructCorrectedMWPCPosMicro(ExpData.AMWPC_anode1,ExpData.AMWPC_anode2,ExpData.MWPC_X,ExpData.MWPC_Y,RecData.MWPCPos_Y,RecData.MWPCPos_X,RecData.MWPCPos_Y_C,Xsig,Ysig,MWPCPeaksX0,MWPCPeaksX1,MWPCPeaksX2,MWPCPeaksA,MWPCExactPos0,MWPCExactPos1,MWPCExactPos2,MWPCExactPosA,KeepLessTrigger,ExpParameterList[iPara][jPara].MWPCC_Threshold);
    }else{
      cout<<"Uknown scheme "<<MWPCPosCalMode<<endl;
      return -1;
    }

    if (RecData.MWPCPos_X<-400.0 || RecData.MWPCPos_Y<-400.0){
      KillEvent=true;
    }


    //Applying scaling
    RecData.MWPCPos_X*=ExpParameterList[iPara][jPara].MWPCX_Scale;
    RecData.MWPCPos_Y*=ExpParameterList[iPara][jPara].MWPCY_Scale;

    //Applying Gain Map
    int ii = int((RecData.MWPCPos_X+17.0)/2.0);
    int jj = int((RecData.MWPCPos_Y+18.0)/2.0);
    if (ii>=0 && ii<17 && jj>=0 && jj<18){
      double GainFactor = MWPCGainMap[ii][jj];
      RecData.EMWPC_anode1/=GainFactor;
      RecData.EMWPC_anode2/=GainFactor;
      RecData.QMWPC_cathode_X=RecData.QMWPC_cathode_X/MWPCRawCalC/GainFactor;
      RecData.QMWPC_cathode_Y=RecData.QMWPC_cathode_Y/MWPCRawCalC/GainFactor;
    }
    double aux = (RecData.EMWPC_anode1+RecData.EMWPC_anode2)/MWPCAnode_Cal;
    RecData.EMWPC_anode = MWPCEScale*aux+MWPCEShift;
  }
  //Calibrate Scint
  if (ReadMode.compare("MWPC")!=0 && ReadMode.compare("MCP")!=0 && ReadMode.compare("He4")!=0){
    if (Calibrating){
      RecData.EScintA[0] = ExpData.QScint_A[0] * ScintRawCal;
      RecData.EScintA[1] = ExpData.QScint_A[1] * ScintRawCal;
      RecData.EScintD[0] = ExpData.QScint_D[0] * ScintRawCal;
      RecData.EScintD[1] = ExpData.QScint_D[1] * ScintRawCal;
    }else{
      LEDScale = 1.0;
      if (LEDStablizer){
	int j = int(ExpData.GroupTime/LEDPeriod);
	if (j>=LEDGainCorrectionSize)j=LEDGainCorrectionSize-1;
	LEDScale = (LEDScint_Cal/LED_Cal)/LEDGainCorrection[j];
      }
      int ii = int((RecData.MWPCPos_X+15.0)/2.0);
      int jj = int((RecData.MWPCPos_Y+16.0)/2.0);
      double GainFactor = 1.0;
      if (ii>=0 && ii<15 && jj>=0 && jj<16){
	if (ReadMode.compare("Double")!=0)GainFactor = ScintGainMap[ii][jj];
      }
      RecData.EScintA[0] = LEDScale * (ScintCalScale * ExpData.QScint_A[0] * ScintRawCal / GainFactor + ScintCalShift);
      RecData.EScintA[1] = LEDScale * (ScintCalScale * ExpData.QScint_A[1] * ScintRawCal / GainFactor + ScintCalShift);
      RecData.EScintD[0] = LEDScale * (ScintCalScale * ExpData.QScint_D[0] * ScintRawCal / GainFactor + ScintCalShift);
      RecData.EScintD[1] = LEDScale * (ScintCalScale * ExpData.QScint_D[1] * ScintRawCal / GainFactor + ScintCalShift);
//      cout <<LEDScale<<endl;
    }
    //  cout <<RecData.EScintA<<endl;
  }

  //Calibrate MCP
  if (ReadMode.compare("MWPC")!=0 && ReadMode.compare("Scint")!=0 && ReadMode.compare("Beta")!=0){
    if((ExpData.TriggerMap&(1<<4))!=0) {
      //Summing MCP Timing
      RecData.MCP_TXSum = ExpData.TMCP_anodes.TX1 + ExpData.TMCP_anodes.TX2;
      RecData.MCP_TYSum = ExpData.TMCP_anodes.TY1 + ExpData.TMCP_anodes.TY2;
      //Reconstruct MCP Position
      if (Calibrating){
	RecData.MCPPos_X = ((ExpData.TMCP_anodes.TX2-ExpData.TMCP_anodes.TX1)*nsTomm+MCPShiftX)*MCPScaleX;
	RecData.MCPPos_Y = ((ExpData.TMCP_anodes.TY1-ExpData.TMCP_anodes.TY2)*nsTomm+MCPShiftY)*MCPScaleY;
	//Y is flipped to match the experiment //////////////////////////////////////////////////////////

	//RecData.MCPPos_X = ((ExpData.TMCP_anodes.TX2-ExpData.TMCP_anodes.TX1)*nsTomm+MCPShiftX)*MCPScaleX + ExpParameterList[iPara][jPara].MCPXShift;
	//RecData.MCPPos_Y = ((ExpData.TMCP_anodes.TY2-ExpData.TMCP_anodes.TY1)*nsTomm+MCPShiftY)*MCPScaleY + ExpParameterList[iPara][jPara].MCPYShift;
      }else{
	double X = ((ExpData.TMCP_anodes.TX2-ExpData.TMCP_anodes.TX1)*nsTomm+MCPShiftX)*MCPScaleX;
	double Y = ((ExpData.TMCP_anodes.TY1-ExpData.TMCP_anodes.TY2)*nsTomm+MCPShiftY)*MCPScaleY;
	double XCorrected;
	double YCorrected;
	if (MCPPosCalMode.compare("Global")==0){
	  XCorrected = XFitPar[0]*X*X+XFitPar[1]*X*Y+XFitPar[2]*Y*Y+XFitPar[3]*X+XFitPar[4]*Y+XFitPar[5];
	  YCorrected = YFitPar[0]*X*X+YFitPar[1]*X*Y+YFitPar[2]*Y*Y+YFitPar[3]*X+YFitPar[4]*Y+YFitPar[5];
	}else if (MCPPosCalMode.compare("Local")==0){
	  int returnval=0;
	  int IndexX=0;
	  int IndexY=0;
	  double LocalXFitPar[6];
	  double LocalYFitPar[6];
	  double startX = -2.0*(MCPsizeX+1)+2.0;
	  double startY = -2.0*(MCPsizeY+1)+2.0;
	  returnval = FindSquare(MCPSquareArray,MCPsizeX,MCPsizeY,X,Y,IndexX,IndexY,MCPNPointEdge);
	  if (returnval==1){
	    for (int ii=0;ii<6;ii++){
	      LocalXFitPar[ii]=MCPSquareArray[IndexX][IndexY].XFitPar[ii];
	      LocalYFitPar[ii]=MCPSquareArray[IndexX][IndexY].YFitPar[ii];
	    }
	    X -= (startX+4.0*IndexX+2.0);
	    Y -= (startY+4.0*IndexY+2.0);
	    XCorrected = LocalXFitPar[0]*X*X+LocalXFitPar[1]*X*Y+LocalXFitPar[2]*Y*Y+LocalXFitPar[3]*X+LocalXFitPar[4]*Y+LocalXFitPar[5] + startX+4.0*IndexX+2.0;
	    YCorrected = LocalYFitPar[0]*X*X+LocalYFitPar[1]*X*Y+LocalYFitPar[2]*Y*Y+LocalYFitPar[3]*X+LocalYFitPar[4]*Y+LocalYFitPar[5] + startY+4.0*IndexY+2.0;
	  }else{
	    //Assign very far away points to make sure that they are vetoed
	    KillEvent=true;
	    XCorrected = -500;
	    YCorrected = -500;
	  }
	}
	RecData.MCPPos_X = XCorrected + ExpParameterList[iPara][jPara].MCPXShift;      
	RecData.MCPPos_Y = YCorrected + ExpParameterList[iPara][jPara].MCPYShift;
      }
    }
  }
  
  //Apply TOF shift and correction
  RecData.TOF = ExpData.TOF+ExpParameterList[iPara][jPara].TOFShift;     
  if (ExpCalSwitches["T0Corr"]){
    if(T0CorrMode.compare("Local")==0) RecData.TOF += t0cell->GetRelT0Corr(RecData.MCPPos_X,RecData.MCPPos_Y);
    else if(T0CorrMode.compare("Global")==0) RecData.TOF += t0cell->GetRelT0CorrPoly(RecData.MCPPos_X,RecData.MCPPos_Y);
    else if(T0CorrMode.compare("Distance")==0) RecData.TOF += t0cell->GetRelT0CorrDist(RecData.MCPPos_X,RecData.MCPPos_Y);
    else{
       cout<< "Unrecognizable T0CorrMode "<<T0CorrMode<<endl;
       exit(1);
    } 
  }
  if (ExpCalSwitches["T0CorrQMCP"]) RecData.TOF -= t0vsQMCPpoly1->Eval(ExpData.QMCP);
  //Copy the rest
  RecData.Event_Num = ExpData.Event_Num; 
  RecData.GroupTime = ExpData.GroupTime;
  RecData.TScint_A_rel = ExpData.TScint_A_rel;
  RecData.TScint_DA = ExpData.TScint_DA;  
  RecData.TMWPC_rel[0] = ExpData.TMWPC_rel[0];
  RecData.TMWPC_rel[1] = ExpData.TMWPC_rel[1];
  RecData.TBeta_diff[0] = ExpData.TBeta_diff[0];
  RecData.TBeta_diff[1] = ExpData.TBeta_diff[1];
  //  DataStruct_TMWPC_X TMWPC_X;   //MWPC cathode X TDCs
  //  DataStruct_TMWPC_Y TMWPC_Y;   //MWPC cathode Y TDCs
  RecData.QMCP_anodes.QX1 = ExpData.QMCP_anodes.QX1;   //MCP anodes
  RecData.QMCP_anodes.QX2 = ExpData.QMCP_anodes.QX2;   //MCP anodes
  RecData.QMCP_anodes.QY1 = ExpData.QMCP_anodes.QY1;   //MCP anodes
  RecData.QMCP_anodes.QY2 = ExpData.QMCP_anodes.QY2;   //MCP anodes
  RecData.TMCP_anodes.TX1 = ExpData.TMCP_anodes.TX1;   //MCP anodes
  RecData.TMCP_anodes.TX2 = ExpData.TMCP_anodes.TX2;   //MCP anodes
  RecData.TMCP_anodes.TY1 = ExpData.TMCP_anodes.TY1;   //MCP anodes
  RecData.TMCP_anodes.TY2 = ExpData.TMCP_anodes.TY2;   //MCP anodes
  RecData.TMCP_rel = ExpData.TMCP_rel;    
  RecData.TriggerMap = ExpData.TriggerMap; 
  RecData.QMCP = ExpData.QMCP;      
  RecData.LED = ExpData.LED;
  RecData.LED_T = ExpData.LED_T;
  RecData.QN2Laser = ExpData.QN2Laser;
  RecData.TN2Laser = ExpData.TN2Laser;

  //Reconstruct Full Kinematics and Q
  if (ReadMode.compare("Triple")==0){
    //double d_MWPC = 3.331*25.4;	//Distance from MOT to MWPC anode
    double d_MWPC = 80.94;	//in mm, New geometry, Distance from MOT to MWPC anode
    double M_Li = 1000*(MASS_LI6 - MASS_E);	//Li ion mass, in keV
    double m_e = 1000*MASS_E;
    double P_e[3];
    double P_R[3];
    double P_v[3];

    //Calculate P_e
    double MWPCRot = 30.0/180.0*PI;
    double X0 = RecData.MWPCPos_X*cos(MWPCRot)+RecData.MWPCPos_Y*sin(MWPCRot);
    double Y0 = -RecData.MWPCPos_X*sin(MWPCRot)+RecData.MWPCPos_Y*cos(MWPCRot);
    P_e[0] = X0;
    P_e[1] = Y0;
    P_e[2] = d_MWPC;

    double P_e_norm = sqrt(pow(P_e[0],2.0)+pow(P_e[1],2.0)+pow(P_e[2],2.0));
    for (int i=0;i<3;i++){
      P_e[i] = P_e[i]/P_e_norm*sqrt(pow(RecData.EScintA[0],2.0)+2.0*m_e*RecData.EScintA[0]);
    }
    //update P_e_norm
    P_e_norm = sqrt(pow(P_e[0],2.0)+pow(P_e[1],2.0)+pow(P_e[2],2.0));

    //Calculate P_R
    double c_light = CSPEED;	//mm/ns
    double Q_Charge = 1.0;
    double E_field = ExpParameterList[iPara][jPara].E_Field;	//kV/mm
    double d_MCP = ExpParameterList[iPara][jPara].Z_Pos;	//mm
    double MCPRot = 22.5/180.0*PI;
    double TOFBound12 = ExpParameterList[iPara][jPara].TOFBound12;
    double XMCP0 = RecData.MCPPos_X*cos(MCPRot)+RecData.MCPPos_Y*sin(MCPRot);
    double YMCP0 = -RecData.MCPPos_X*sin(MCPRot)+RecData.MCPPos_Y*cos(MCPRot);
    P_R[0] = M_Li*XMCP0/(RecData.TOF+0.450)/c_light;
    P_R[1] = M_Li*YMCP0/(RecData.TOF+0.450)/c_light;
    P_R[2] = 0.5*E_field*Q_Charge*(RecData.TOF+0.450)*c_light-M_Li*d_MCP/(RecData.TOF+0.450)/c_light;
    if (RecData.TOF>TOFBound12)RecData.EIon[0] = (P_R[0]*P_R[0]+P_R[1]*P_R[1]+P_R[2]*P_R[2])/2.0/M_Li;
    else RecData.EIon[0]=-100;

    //  cout << P_R[0]<<endl;
    //Reconstruct Neutrino momentum
    for (int i=0;i<3;i++){
      P_v[i] = -P_e[i] - P_R[i];
    }
    double E_v = sqrt(pow(P_v[0],2.0)+pow(P_v[1],2.0)+pow(P_v[2],2.0));
    RecData.Q_Value[0] = E_v+RecData.EScintA[0];

    //Reconstruct Correlation
    double CosTheta1 = (P_e[0]*P_v[0]+P_e[1]*P_v[1]+P_e[2]*P_v[2])/E_v/P_e_norm;

    //Charge State 2
    Q_Charge = 2.0;
    P_R[0] = M_Li*RecData.MCPPos_X/(RecData.TOF+0.450)/c_light;
    P_R[1] = M_Li*RecData.MCPPos_Y/(RecData.TOF+0.450)/c_light;
    P_R[2] = 0.5*E_field*Q_Charge*(RecData.TOF+0.450)*c_light-M_Li*d_MCP/(RecData.TOF+0.450)/c_light;
    RecData.EIon[1] = (P_R[0]*P_R[0]+P_R[1]*P_R[1]+P_R[2]*P_R[2])/2.0/M_Li;
    if (RecData.TOF<TOFBound12)RecData.EIon[1] = (P_R[0]*P_R[0]+P_R[1]*P_R[1]+P_R[2]*P_R[2])/2.0/M_Li;
    else RecData.EIon[1]=-100;

    //  cout <<RecData.MCPPos_X<<" "<<RecData.TOF<<" ";
    //  cout <<P_R[0]<<endl;
    //Reconstruct Neutrino momentum
    for (int i=0;i<3;i++){
      P_v[i] = -P_e[i] - P_R[i];
    }
    E_v = sqrt(pow(P_v[0],2.0)+pow(P_v[1],2.0)+pow(P_v[2],2.0));
    RecData.Q_Value[1] = E_v+RecData.EScintA[0];

    //Reconstruct Correlation
    double CosTheta2 = (P_e[0]*P_v[0]+P_e[1]*P_v[1]+P_e[2]*P_v[2])/E_v/P_e_norm;

    //Charge State 3
    Q_Charge = 3.0;
    P_R[0] = M_Li*RecData.MCPPos_X/(RecData.TOF+0.450)/c_light;
    P_R[1] = M_Li*RecData.MCPPos_Y/(RecData.TOF+0.450)/c_light;
    P_R[2] = 0.5*E_field*Q_Charge*(RecData.TOF+0.450)*c_light-M_Li*d_MCP/(RecData.TOF+0.450)/c_light;

    //  cout <<RecData.MCPPos_X<<" "<<RecData.TOF<<" ";
    //  cout <<P_R[0]<<endl;
    //Reconstruct Neutrino momentum
    for (int i=0;i<3;i++){
      P_v[i] = -P_e[i] - P_R[i];
    }
    E_v = sqrt(pow(P_v[0],2.0)+pow(P_v[1],2.0)+pow(P_v[2],2.0));
    RecData.Q_Value[2] = E_v+RecData.EScintA[0];
    //For the moment, no construction of CosTheta3

    //Make a condition for TOF, deciding if it is in the charge state1
    if (RecData.TOF>TOFBound12)RecData.CosThetaENu=CosTheta1;
    else RecData.CosThetaENu=CosTheta2;
    //  cout <<RecData.Q_Value[0]<<" "<<RecData.Q_Value[1]<<endl;
  }else if(ReadMode.compare("Double")==0){
    //double d_MWPC = 3.331*25.4;	//Distance from MOT to MWPC anode
    double d_MWPC = 80.94;	//in mm, New geometry, Distance from MOT to MWPC anode
    double M_Li = 1000*(MASS_LI6 - MASS_E);	//Li ion mass, in keV
    double m_e = 1000*MASS_E;
    double P_e[3];
    double P_R[3];
    double P_v[3];

    //Calculate P_e
//    double MWPCRot = 30.0/180.0*PI;
//    double X0 = RecData.MWPCPos_X*cos(MWPCRot)+RecData.MWPCPos_Y*sin(MWPCRot);
//    double Y0 = -RecData.MWPCPos_X*sin(MWPCRot)+RecData.MWPCPos_Y*cos(MWPCRot);
    P_e[0] = 0;
    P_e[1] = 0;
    P_e[2] = d_MWPC;

    double P_e_norm = sqrt(pow(P_e[0],2.0)+pow(P_e[1],2.0)+pow(P_e[2],2.0));
    for (int i=0;i<3;i++){
      P_e[i] = P_e[i]/P_e_norm*sqrt(pow(RecData.EScintA[0],2.0)+2.0*m_e*RecData.EScintA[0]);
    }
    //update P_e_norm
    P_e_norm = sqrt(pow(P_e[0],2.0)+pow(P_e[1],2.0)+pow(P_e[2],2.0));

    //Calculate P_R
    double c_light = CSPEED;	//mm/ns
    double Q_Charge = 1.0;
    double E_field = ExpParameterList[iPara][jPara].E_Field;	//kV/mm
    double d_MCP = ExpParameterList[iPara][jPara].Z_Pos;	//mm
    double MCPRot = 22.5/180.0*PI;
    double TOFBound12 = ExpParameterList[iPara][jPara].TOFBound12;
    double XMCP0 = RecData.MCPPos_X*cos(MCPRot)+RecData.MCPPos_Y*sin(MCPRot);
    double YMCP0 = -RecData.MCPPos_X*sin(MCPRot)+RecData.MCPPos_Y*cos(MCPRot);
    P_R[0] = M_Li*XMCP0/(RecData.TOF+0.450)/c_light;
    P_R[1] = M_Li*YMCP0/(RecData.TOF+0.450)/c_light;
    P_R[2] = 0.5*E_field*Q_Charge*(RecData.TOF+0.450)*c_light-M_Li*d_MCP/(RecData.TOF+0.450)/c_light;
    if (RecData.TOF>TOFBound12)RecData.EIon[0] = (P_R[0]*P_R[0]+P_R[1]*P_R[1]+P_R[2]*P_R[2])/2.0/M_Li;
    else RecData.EIon[0]=-100;

    //  cout << P_R[0]<<endl;
    //Reconstruct Neutrino momentum
    for (int i=0;i<3;i++){
      P_v[i] = -P_e[i] - P_R[i];
    }
    double E_v = sqrt(pow(P_v[0],2.0)+pow(P_v[1],2.0)+pow(P_v[2],2.0));
    RecData.Q_Value[0] = E_v+RecData.EScintA[0];

    //Reconstruct Correlation
    double CosTheta1 = (P_e[0]*P_v[0]+P_e[1]*P_v[1]+P_e[2]*P_v[2])/E_v/P_e_norm;

    //Charge State 2
    Q_Charge = 2.0;
    P_R[0] = M_Li*RecData.MCPPos_X/(RecData.TOF+0.450)/c_light;
    P_R[1] = M_Li*RecData.MCPPos_Y/(RecData.TOF+0.450)/c_light;
    P_R[2] = 0.5*E_field*Q_Charge*(RecData.TOF+0.450)*c_light-M_Li*d_MCP/(RecData.TOF+0.450)/c_light;
    RecData.EIon[1] = (P_R[0]*P_R[0]+P_R[1]*P_R[1]+P_R[2]*P_R[2])/2.0/M_Li;
    if (RecData.TOF<TOFBound12)RecData.EIon[1] = (P_R[0]*P_R[0]+P_R[1]*P_R[1]+P_R[2]*P_R[2])/2.0/M_Li;
    else RecData.EIon[1]=-100;

    //  cout <<RecData.MCPPos_X<<" "<<RecData.TOF<<" ";
    //  cout <<P_R[0]<<endl;
    //Reconstruct Neutrino momentum
    for (int i=0;i<3;i++){
      P_v[i] = -P_e[i] - P_R[i];
    }
    E_v = sqrt(pow(P_v[0],2.0)+pow(P_v[1],2.0)+pow(P_v[2],2.0));
    RecData.Q_Value[1] = E_v+RecData.EScintA[0];

    //Reconstruct Correlation
    double CosTheta2 = (P_e[0]*P_v[0]+P_e[1]*P_v[1]+P_e[2]*P_v[2])/E_v/P_e_norm;

    //Charge State 3
    Q_Charge = 3.0;
    P_R[0] = M_Li*RecData.MCPPos_X/(RecData.TOF+0.450)/c_light;
    P_R[1] = M_Li*RecData.MCPPos_Y/(RecData.TOF+0.450)/c_light;
    P_R[2] = 0.5*E_field*Q_Charge*(RecData.TOF+0.450)*c_light-M_Li*d_MCP/(RecData.TOF+0.450)/c_light;

    //  cout <<RecData.MCPPos_X<<" "<<RecData.TOF<<" ";
    //  cout <<P_R[0]<<endl;
    //Reconstruct Neutrino momentum
    for (int i=0;i<3;i++){
      P_v[i] = -P_e[i] - P_R[i];
    }
    E_v = sqrt(pow(P_v[0],2.0)+pow(P_v[1],2.0)+pow(P_v[2],2.0));
    RecData.Q_Value[2] = E_v+RecData.EScintA[0];
    //For the moment, no construction of CosTheta3

    //Make a condition for TOF, deciding if it is in the charge state1
    if (RecData.TOF>TOFBound12)RecData.CosThetaENu=CosTheta1;
    else RecData.CosThetaENu=CosTheta2;
    //  cout <<RecData.Q_Value[0]<<" "<<RecData.Q_Value[1]<<endl;
  }else{
    RecData.Q_Value[0] = 3500; 
    RecData.Q_Value[1] = 3500;
  }

  //if (!KillEvent)cout <<"Not Killed"<<endl;
  return 0;
}

/***********************************************************************************/
int Analyzer::GenerateLEDStablizer(int iFile,int jFile)
{
  LEDGainCorrection.clear();
  if (!LEDStablizer){
    return 1;
  }
  Calibrating = true;

  char filename[100];
  sprintf(filename,"%s/%s_%04d.root",EXP_DATA_DIRECTORY.c_str(),ExpFilePrefix.c_str(),FileIDListExp[iFile][jFile]);

  ExpDataStruct ExpData;      //Data struct for exp data reading
  RecDataStruct RecData;

  //Open root file for writing
  ostringstream convert;
  convert << setfill('0') << setw(4)<<FileIDListExp[iFile][jFile];
  string spectrumfile = EXP_DATA_DIRECTORY + string("/../Histograms/") + ExpFilePrefix + string("_") + convert.str() + string("_LEDMonitor.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");
  
  //Histograms for calibration
  TH1D *h_LED_tot = new TH1D("h_LED_tot","LED signal total",1400,0,140000);
  TH1D *h_Scint_LED_tot  = new TH1D("h_Scint_LED_tot","Scintillator spectrum (LED)",1024,0,20000);
  TH1D *h_LED = new TH1D("h_LED","LED signal",1400,0,140000);
  TH1D *h_Scint_LED  = new TH1D("h_Scint_LED","Scintillator spectrum",1024,0,20000);

  //Graph
  TGraphErrors* TLEDScint = new TGraphErrors;
  TLEDScint->SetName("TLEDScint");
  TLEDScint->SetTitle("Time spectrum TLEDScint");

  TGraphErrors* TLED = new TGraphErrors;
  TLED->SetName("TLED");
  TLED->SetTitle("Time spectrum TLED");

  TGraphErrors* TLEDCalibration = new TGraphErrors;
  TLEDCalibration->SetName("TLEDCalibration");
  TLEDCalibration->SetTitle("Time spectrum TLEDCalibration");

  TFile* filein = new TFile(filename,"READ");//input data file
  //Tree
  TTree *Tree_PMT_STB = (TTree*)filein->Get("Tree_PMT_STB");
  Tree_PMT_STB->SetBranchAddress("Scint",&ExpData.QScint_A);
  Tree_PMT_STB->SetBranchAddress("LED",&ExpData.LED);
  Tree_PMT_STB->SetBranchAddress("LED_T",&ExpData.LED_T);
  int N_LED = Tree_PMT_STB->GetEntries();

  double LEDVal;
  double ScintLEDVal;
  double LEDCalibration;

  int jj=0;
  for(int i=0;i<N_LED;i++){
    //Read Tree
    ClearTempData(ExpData,RecData);
    Tree_PMT_STB->GetEntry(i);
    Reconstruct(0,0,RecData,ExpData);
    //Still need to do this calibration, by hand....
    RecData.EScintA[0] = ScintCalScale * ExpData.QScint_A[0] * ScintRawCal + ScintCalShift;

    if (RecData.LED>100 && RecData.EScintA[0]>10 && (RecData.TriggerMap&(1<<8))==0){
      h_LED_tot->Fill(RecData.LED);
      h_Scint_LED_tot->Fill(RecData.EScintA[0]);
      h_LED->Fill(RecData.LED);
      h_Scint_LED->Fill(RecData.EScintA[0]);
      if (RecData.LED_T-jj*LEDPeriod>LEDPeriod){
	double LED_Peak = h_LED->GetBinCenter(h_LED->GetMaximumBin());
	h_LED->Fit("gaus","","",LED_Peak-1000,LED_Peak+1000);
	TF1* LEDFit = h_LED->GetFunction("gaus");
	LEDVal = LEDFit->GetParameter(1);
	double LEDError = LEDFit->GetParError(1);
	double Chi2 = LEDFit->GetChisquare();
	double NDF = LEDFit->GetNDF();
	cout << "LED Fit Chi2= "<<Chi2/NDF<<endl;
	TLED->SetPoint(jj,jj*LEDPeriod/60.0,LEDVal);
	TLED->SetPointError(jj,0,LEDError);

	double ScintLED_Peak = h_Scint_LED->GetBinCenter(h_Scint_LED->GetMaximumBin());
	h_Scint_LED->Fit("gaus","","",ScintLED_Peak-500,ScintLED_Peak+500);
	TF1* ScintLEDFit = h_Scint_LED->GetFunction("gaus");
	ScintLEDVal = ScintLEDFit->GetParameter(1);
	//Calibrate this peak,too
	double ScintLEDError = ScintLEDFit->GetParError(1);
	double ScintChi2 = ScintLEDFit->GetChisquare();
	double ScintNDF = ScintLEDFit->GetNDF();
	cout << "ScintLED Fit Chi2= "<<ScintChi2/ScintNDF<<endl;
	TLEDScint->SetPoint(jj,jj*LEDPeriod/60.0,ScintLEDVal);
	TLEDScint->SetPointError(jj,0,ScintLEDError);

	TLEDCalibration->SetPoint(jj,jj*LEDPeriod/60.0,ScintLEDVal/LEDVal);
	TLEDCalibration->SetPointError(jj,0,ScintLEDVal/LEDVal*sqrt(pow(ScintLEDError/ScintLEDVal,2.0)+pow(LEDError/LEDVal,2.0)));

	LEDGainCorrection.push_back(ScintLEDVal/LEDVal);

	jj++;

	delete h_LED;
	delete h_Scint_LED;
	h_LED = new TH1D("h_LED","LED signal",1400,0,140000);
	h_Scint_LED  = new TH1D("h_Scint_LED","Scintillator spectrum",1024,0,20000);
      }
    }
  }

  filein->Close();
  delete filein;
  Calibrating = false;
  LEDGainCorrectionSize=LEDGainCorrection.size();



  h_LED_tot->Write();
  h_Scint_LED_tot->Write();
//  h_LED->Write();
//  h_Scint_LED->Write();

  TLED->Write();
  TLEDScint->Write();
  TLEDCalibration->Write();
  histfile->Close();
  delete histfile;

  return 0;
}

