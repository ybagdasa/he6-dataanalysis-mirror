//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>
#include <stdint.h>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
//Analyzer includes
#include "Analyzer.h"

using namespace std;

/************************************************************************************/
int Analyzer::OpenSpecFile(string SpecFileName)
{
  SpectrumFileName = SPECTRA_DIRECTORY + "/Spectra_"+ SpecFileName + ".pdf(";
  TH1 * DummyHist = new TH1D("Dummy","Dummy Hist",10,0,10);
  TCanvas * DummyCanv = new TCanvas("Dummy","Dummy",0,0,800,600);
  DummyHist->Draw();
  DummyCanv->SaveAs(SpectrumFileName.c_str());
  SpectrumFileName = SPECTRA_DIRECTORY + "/Spectra_"+ SpecFileName + ".pdf";
  SpectrumStatus = 1;
  //Log
  AnalysisLog << "Open Spectrum file : "<< SpectrumFileName << "\n";
  delete DummyHist;
  delete DummyCanv;
  return 0;
}

/************************************************************************************/
int Analyzer::CloseSpecFile()
{
  //Log
  AnalysisLog << "Close Spectrum file : "<< SpectrumFileName << "\n";
  SpectrumFileName += ")";
  TH1 * DummyHist = new TH1D("Dummy","Dummy Hist",10,0,10);
  TCanvas * DummyCanv = new TCanvas("Dummy","Dummy",0,0,800,600);
  DummyHist->Draw();
  DummyCanv->SaveAs(SpectrumFileName.c_str());
  SpectrumFileName = SPECTRA_DIRECTORY + "/Spectra_0000";
  SpectrumStatus = 0;
  delete DummyHist;
  delete DummyCanv;
  return 0;
}

/************************************************************************************/
int Analyzer::PlotHistogram(string HistName,int i, int j)
{
  //Depend on purpose, choose the right histogram box
  map <string,HistUnit>*  ChosenHistBox;
  if (Purpose==0){
    if(Status != 2){
      cout <<"Histograms are not initialized,filled or loaded!\n";
      return -1;
    }
    ChosenHistBox = &SimHistBox;
  }
  if (Purpose==1){
    if(ExpStatus != 2){
      cout <<"Histograms are not initialized,filled or loaded!\n";
      return -1;
    }
    ChosenHistBox = &ExpHistBox;
  }

  if ((*ChosenHistBox).find(HistName)==(*ChosenHistBox).end()){
    cout << "Histogram "<<HistName<<" is not found!\n";
    return -1;
  }
  //Styles
  gStyle->SetOptStat("");
  TCanvas *canv = new TCanvas("canv","Spectra",0,0,800,600);
  if ((*ChosenHistBox)[HistName].Dim==1){
    (((*ChosenHistBox)[HistName].Hist1DList)[i][j])->Draw();
  } else if ((*ChosenHistBox)[HistName].Dim==2){
    (((*ChosenHistBox)[HistName].Hist2DList)[i][j])->Draw(Option2DHist.c_str());
  }
  canv->SaveAs(SpectrumFileName.c_str());
  delete canv;
  return 0;
}

/************************************************************************************/
int Analyzer::PlotCombHistogram(string HistName,int i,string Select)
{
  //Depend on purpose, choose the right histogram box
  map <string,HistUnit>*  ChosenHistBox;
  if (Select.compare("Exp")==0){
    ChosenHistBox = &CombExpHistBox;
  }
  else if (Select.compare("Bkg")==0){
    ChosenHistBox = &CombBkgHistBox;
  }
  else{
    cout << "Please choose Exp or Bkg.\n";
  }

  if ((*ChosenHistBox).find(HistName)==(*ChosenHistBox).end()){
    cout << "Histogram "<<HistName<<" is not found!\n";
    return -1;
  }
  //Styles
  gStyle->SetOptStat("");
  TCanvas *canv = new TCanvas("canv","Spectra",0,0,800,600);
  if ((*ChosenHistBox)[HistName].Dim==1){
    (((*ChosenHistBox)[HistName].Hist1DList)[0][i])->Draw();
  } else if ((*ChosenHistBox)[HistName].Dim==2){
    (((*ChosenHistBox)[HistName].Hist2DList)[0][i])->Draw(Option2DHist.c_str());
  }
  canv->SaveAs(SpectrumFileName.c_str());
  delete canv;
  return 0;
}

/************************************************************************************/
int Analyzer::OutputSpectraGeneral(int i, int j)
{
  if (Purpose==0){
    if(Status != 2){
      cout <<"Histograms are not initialized,filled or loaded!\n";
      return -1;
    }
    //  string filename;
    //Styles
    gStyle->SetOptStat("");
    //  gStyle->SetTitleXSize(0.05);

    TCanvas *canv = new TCanvas("canv","Canv 1",0,0,800,600);
    canv->Divide(3,2);
    canv->cd(1);
    ((SimHistBox["RawScint"].Hist1DList)[i][j])->Draw();
    canv->cd(4);
    ((SimHistBox["CoinScint"].Hist1DList)[i][j])->Draw();
    canv->cd(2);
    ((SimHistBox["RawMWPC"].Hist1DList)[i][j])->Draw();
    canv->cd(5);
    ((SimHistBox["CoinMWPC"].Hist1DList)[i][j])->Draw();
    canv->cd(3);
    ((SimHistBox["MWPC_vs_Scint"].Hist2DList)[i][j])->Draw(Option2DHist.c_str());
    canv->cd(6);
    ((SimHistBox["MWPC_Image"].Hist2DList)[i][j])->Draw(Option2DHist.c_str());
    canv->SaveAs(SpectrumFileName.c_str());

    TCanvas *canv2 = new TCanvas("canv2","Canv 2",0,0,660,600);
    canv2->Divide(2,2);
    canv2->cd(1);
    ((SimHistBox["MCP_Image"].Hist2DList)[i][j])->Draw(Option2DHist.c_str());
    canv2->cd(4);
    ((SimHistBox["TOFHist"].Hist1DList)[i][j])->Draw();
    canv2->cd(2);
    ((SimHistBox["TOF_vs_Scint"].Hist2DList)[i][j])->Draw(Option2DHist.c_str());
    canv2->cd(3);
    ((SimHistBox["ExitAngleHist"].Hist1DList)[i][j])->Draw();

    canv2->SaveAs(SpectrumFileName.c_str());
    delete canv;
    delete canv2;
  }
  if (Purpose==1){
    if(ExpStatus != 2){
      cout <<"Histograms are not initialized,filled or loaded!\n";
      return -1;
    }
    //  string filename;
    //Styles
    gStyle->SetOptStat("");
    //  gStyle->SetTitleXSize(0.05);

    TCanvas *canv = new TCanvas("canv","Canv 1",0,0,800,600);
    canv->Divide(3,2);
    canv->cd(1);
    ((ExpHistBox["Scint_EA"].Hist1DList)[i][j])->Draw();
    canv->cd(2);
    ((ExpHistBox["MWPC_Anode"].Hist1DList)[i][j])->Draw();
    canv->cd(3);
    ((ExpHistBox["MWPC_vs_Scint"].Hist2DList)[i][j])->Draw(Option2DHist.c_str());
    canv->cd(4);
    ((ExpHistBox["Beta_Time_diff"].Hist1DList)[i][j])->Draw();
    canv->cd(5);
    ((ExpHistBox["MWPC_Cathode_Anode"].Hist2DList)[i][j])->Draw(Option2DHist.c_str());
    canv->cd(6);
    ((ExpHistBox["MWPC_Image"].Hist2DList)[i][j])->Draw(Option2DHist.c_str());
    canv->SaveAs(SpectrumFileName.c_str());

    TCanvas *canv2 = new TCanvas("canv2","Canv 2",0,0,660,600);
    canv2->Divide(3,2);
    canv2->cd(1);
    ((ExpHistBox["MCP_Image"].Hist2DList)[i][j])->Draw(Option2DHist.c_str());
    canv2->cd(2);
    ((ExpHistBox["MCP_Charge"].Hist1DList)[i][j])->Draw();
    canv2->cd(4);
    ((ExpHistBox["TOF_vs_Scint"].Hist2DList)[i][j])->Draw(Option2DHist.c_str());
    canv2->cd(5);
    ((ExpHistBox["TOFHist"].Hist1DList)[i][j])->Draw();
    canv2->cd(3);
    ((ExpHistBox["MCP_TX1pTX2"].Hist1DList)[i][j])->Draw();
    canv2->cd(6);
    ((ExpHistBox["MCP_TY1pTY2"].Hist1DList)[i][j])->Draw();

    canv2->SaveAs(SpectrumFileName.c_str());
    delete canv;
    delete canv2;
  }
  return 0;
}

/************************************************************************************/
int Analyzer::PlotHistogramList(string HistName,int iStart,int iEnd,int jStart,int jEnd)
{
  //Depend on purpose, choose the right histogram box
  map <string,HistUnit>*  ChosenHistBox;
  if (Purpose==0){
    if(Status != 2){
      cout <<"Histograms are not initialized,filled or loaded!\n";
      return -1;
    }
    ChosenHistBox = &SimHistBox;
  }
  if (Purpose==1){
    if(ExpStatus != 2){
      cout <<"Histograms are not initialized,filled or loaded!\n";
      return -1;
    }
    ChosenHistBox = &ExpHistBox;
  }

  //Test Dimensions
  if (iEnd > HistDim1){
    cout <<"Index i out of range, set to "<<HistDim1<<endl;
    iEnd = HistDim1;
    if (iStart>=iEnd) iStart=iEnd-1;
  }
  if (jEnd > HistDim2){
    cout <<"Index j out of range, set to "<<HistDim2<<endl;
    jEnd = HistDim2;
    if (jStart>=jEnd) jStart=jEnd-1;
  }

  int dim1 = iEnd - iStart + 1;
  int dim2 = jEnd - jStart + 1;
//  cout << "dim1 = "<<dim1<<endl;
  //Limit to 10 X 5
  if (dim1 > 10){
    dim1 = 10;
    iEnd = iStart + 9;
    cout << "Limit dim1 to 10\n";
  }
  if (dim2 > 10){
    dim2 = 10;
    jEnd = jStart + 9;
    cout << "Limit dim2 to 10\n";
  }
  //Find histogram first
  if ((*ChosenHistBox).find(HistName)==(*ChosenHistBox).end()){
    cout << "Histogram "<<HistName<<" is not found!\n";
    return -1;
  }

  //Styles
  gStyle->SetOptStat("");

  TCanvas *canv = new TCanvas("canv","Spectra",0,0,300*dim1,300*dim2);
  canv->Divide(dim1,dim2);

  for (int i=1;i<=dim1;i++){
    for (int j=1;j<=dim2;j++){
      canv->cd((j-1)*dim1+i);
      if ((*ChosenHistBox)[HistName].Dim==1){
        (((*ChosenHistBox)[HistName].Hist1DList)[iStart+i-1][jStart+j-1])->Draw();
      }
      if ((*ChosenHistBox)[HistName].Dim==2){
        (((*ChosenHistBox)[HistName].Hist2DList)[iStart+i-1][jStart+j-1])->Draw(Option2DHist.c_str());
      }
    }
  }

  canv->SaveAs(SpectrumFileName.c_str());

  delete canv;
  return 0;
}

/***********************************************************************************/
int Analyzer::ShowTitle(bool Opt)
{
  TitleOn = Opt;
  return 0;
}

int Analyzer::ShowAxisTitle(bool Opt)
{
  AxisTitleOn = Opt;
  return 0;
}

int Analyzer::ShowGrid(bool Opt)
{
  GridOn = Opt;
  return 0;
}

int Analyzer::SetDrawOption2DHist(string Opt)
{
  Option2DHist = Opt;
  return 0;
}

int Analyzer::SetDrawOption2DGraph(string Opt)
{
  Option2DGraph = Opt;
  return 0;
}
/***********************************************************************************/
