/********************************************************************
  He6 experiment analyzer fit module
  Author:	Ran Hong
  Date:		May. 28th. 2013
  Version:	1.02

  Fit TOF and Energy spectrum, get 'a' and 'b'

********************************************************************/

//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>

//ROOT includes
/*
#include "Fit/Fitter.h"
#include "Fit/BinData.h"
#include "Fit/Chi2FCN.h"
#include "TList.h"
#include "Math/WrappedMultiTF1.h"
#include "HFitInterface.h"*/
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TF1.h"
#include "TF2.h"
#include "TCanvas.h"
#include "TGraph.h"
//#include "TSpline.h"
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"

//Analyzer includes
#include "Analyzer.h"

//Globals
extern int TOF_High;
extern int TOF_Low;
extern int TOF_N;

double MultiSlice(int floatTime, int N_Slice,double FitRange_low,double FitRange_high);

/***********************************************************************************/
//Function definations

using namespace std;

double spline_func(double *x, double parm[]);

double Std_First_N=0;  //Normalization of STD histograms
double Std_Second_N=0;
double Fit_N=0;
double GlobalRangeLow=0.0;
double GlobalRangeHigh=0.0;

//TSpline5 *spline_plus=NULL;
//TSpline5 *spline_minus=NULL;
TH1* Hist_plus=NULL;
TH1* Hist_minus=NULL;
TH1* Hist_Fit=NULL;
TH1* Hist_Bkg=NULL;

/*
//For Sliced Fit
double Std_First_Slice[N_OF_SLICE];
double Std_Second_Slice[N_OF_SLICE];

TSpline5 *spline_plus_slice[N_OF_SLICE];
TSpline5 *spline_minus_slice[N_OF_SLICE];
*/

/***********************************************************************************/
int Analyzer::FitConfig(string Type,string AllowTimeFloat,double Time_Shift)
{
  FitType = Type;
  if (FitType.compare("MINUIT")!=0 && FitType.compare("ROOT")!=0 && FitType.compare("HONG")!=0 && FitType.compare("MultiSlice")!=0 && FitType.compare("Gauss")!=0){
    cout << "Unknow FitType. FitType is set to default MINUIT.\n";
    FitType = "MINUIT";
  }
  if (AllowTimeFloat.compare("ON")==0)
    floatTime = 1;
  else if (AllowTimeFloat.compare("OFF")==0)
    floatTime = 0;
  else{
    cout << "Unknown option "<<AllowTimeFloat <<" for float time. Set to OFF by default.\n";
    floatTime = 0;
  }
  TimeShift = Time_Shift;
  return 0;
}

/***********************************************************************************/
int Analyzer::InitFit(string FitHistName)
{
  //Depend on purpose, choose the right histogram box
  map <string,HistUnit>*  ChosenHistBox;
  if (Purpose==0){
    ChosenHistBox = &SimHistBox;
  }
  if (Purpose==1){
    ChosenHistBox = &ExpHistBox;
  }

  //Test whether they are already initialized
  if (FitHistBox.find(FitHistName)!=FitHistBox.end()){
    if(FitHistBox[FitHistName].Dim==1){
      for(int i=0;i<N_OF_HIST;i++){
	for(int j=0;j<N_OF_HIST;j++){
	  delete (FitHistBox[FitHistName].Hist1DList)[i][j];
	}
	delete[] (FitHistBox[FitHistName].Hist1DList)[i];
      }
      delete[] FitHistBox[FitHistName].Hist1DList;
      if(FitHistBox[FitHistName].MultiSlice){
	for(int i=0;i<N_OF_HIST;i++){
	  for(int j=0;j<N_OF_HIST;j++){
	    for(int k=0;k<N_OF_SLICE;k++){
	      delete FitHistBox[FitHistName].Hist1DListSliced[i][j][k];
	    }
	    delete[] FitHistBox[FitHistName].Hist1DListSliced[i][j];
	  }
	  delete[] FitHistBox[FitHistName].Hist1DListSliced[i];
	}
	delete[] FitHistBox[FitHistName].Hist1DListSliced;
      }
    }else if((FitHistBox[FitHistName]).Dim==2){
      for(int i=0;i<N_OF_HIST;i++){
	for(int j=0;j<N_OF_HIST;j++){
	  delete (FitHistBox[FitHistName].Hist2DList)[i][j];
	}
	delete[] (FitHistBox[FitHistName].Hist2DList)[i];
      }
      delete[] FitHistBox[FitHistName].Hist2DList;
    }
  }else{
    FitHistBox[FitHistName].Title = (*ChosenHistBox)[FitHistName].Title + "_Fit"; 
    FitHistBox[FitHistName].Dim = (*ChosenHistBox)[FitHistName].Dim;
    FitHistBox[FitHistName].MultiSlice = (*ChosenHistBox)[FitHistName].MultiSlice;
    FitHistBox[FitHistName].XTitle = (*ChosenHistBox)[FitHistName].XTitle;
    FitHistBox[FitHistName].YTitle = (*ChosenHistBox)[FitHistName].YTitle;
    FitHistBox[FitHistName].ZTitle = (*ChosenHistBox)[FitHistName].ZTitle;
    FitHistBox[FitHistName].XBinNum = (*ChosenHistBox)[FitHistName].XBinNum; 
    FitHistBox[FitHistName].XRange.low = (*ChosenHistBox)[FitHistName].XRange.low;
    FitHistBox[FitHistName].XRange.high = (*ChosenHistBox)[FitHistName].XRange.high;
    FitHistBox[FitHistName].YBinNum = (*ChosenHistBox)[FitHistName].YBinNum;
    FitHistBox[FitHistName].YRange.low = (*ChosenHistBox)[FitHistName].YRange.low;
    FitHistBox[FitHistName].YRange.high = (*ChosenHistBox)[FitHistName].YRange.high;
    FitHistBox[FitHistName].Hist1DList = NULL;
    FitHistBox[FitHistName].Hist2DList = NULL;
    FitHistBox[FitHistName].Hist1DListSliced = NULL;
  }
  //Clear the Residual histograms
  string FitHistNameResidual = FitHistName + "_Residual";
  if (FitHistBox.find(FitHistNameResidual)!=FitHistBox.end()){
    if(FitHistBox[FitHistNameResidual].Dim==1){
      for(int i=0;i<N_OF_HIST;i++){
	for(int j=0;j<N_OF_HIST;j++){
	  delete (FitHistBox[FitHistNameResidual].Hist1DList)[i][j];
	}
	delete[] (FitHistBox[FitHistNameResidual].Hist1DList)[i];
      }
      delete[] FitHistBox[FitHistNameResidual].Hist1DList;
      if(FitHistBox[FitHistNameResidual].MultiSlice){
	for(int i=0;i<N_OF_HIST;i++){
	  for(int j=0;j<N_OF_HIST;j++){
	    for(int k=0;k<N_OF_SLICE;k++){
	      delete FitHistBox[FitHistNameResidual].Hist1DListSliced[i][j][k];
	    }
	    delete[] FitHistBox[FitHistNameResidual].Hist1DListSliced[i][j];
	  }
	  delete[] FitHistBox[FitHistNameResidual].Hist1DListSliced[i];
	}
	delete[] FitHistBox[FitHistNameResidual].Hist1DListSliced;
      }
    }else if((FitHistBox[FitHistNameResidual]).Dim==2){
      for(int i=0;i<N_OF_HIST;i++){
	for(int j=0;j<N_OF_HIST;j++){
	  delete (FitHistBox[FitHistNameResidual].Hist2DList)[i][j];
	}
	delete[] (FitHistBox[FitHistNameResidual].Hist2DList)[i];
      }
      delete[] FitHistBox[FitHistNameResidual].Hist2DList;
    }
  }else{
    FitHistBox[FitHistNameResidual].Title = (*ChosenHistBox)[FitHistName].Title + "_Residual"; 
    FitHistBox[FitHistNameResidual].Dim = (*ChosenHistBox)[FitHistName].Dim;
    FitHistBox[FitHistNameResidual].MultiSlice = (*ChosenHistBox)[FitHistName].MultiSlice;
    FitHistBox[FitHistNameResidual].XTitle = (*ChosenHistBox)[FitHistName].XTitle;
    FitHistBox[FitHistNameResidual].YTitle = (*ChosenHistBox)[FitHistName].YTitle;
    FitHistBox[FitHistNameResidual].ZTitle = (*ChosenHistBox)[FitHistName].ZTitle;
    FitHistBox[FitHistNameResidual].XBinNum = (*ChosenHistBox)[FitHistName].XBinNum; 
    FitHistBox[FitHistNameResidual].XRange.low = (*ChosenHistBox)[FitHistName].XRange.low;
    FitHistBox[FitHistNameResidual].XRange.high = (*ChosenHistBox)[FitHistName].XRange.high;
    FitHistBox[FitHistNameResidual].YBinNum = (*ChosenHistBox)[FitHistName].YBinNum;
    FitHistBox[FitHistNameResidual].YRange.low = (*ChosenHistBox)[FitHistName].YRange.low;
    FitHistBox[FitHistNameResidual].YRange.high = (*ChosenHistBox)[FitHistName].YRange.high;
    FitHistBox[FitHistNameResidual].Hist1DList = NULL;
    FitHistBox[FitHistNameResidual].Hist2DList = NULL;
    FitHistBox[FitHistNameResidual].Hist1DListSliced = NULL;
  }

  //Clear Results
  if (FitResBox.find(FitHistName)!=FitResBox.end()){
    for(int i=0;i<N_OF_HIST;i++){
      delete[] FitResBox[FitHistName][i];
    }
    delete[] FitResBox[FitHistName];
  }

  //Init Fit Histograms, residual histograms and results
  map <string,HistUnit>::iterator it;
  string HistName;
  string HistTitle;
  it = FitHistBox.find(FitHistName);
  if((it->second).Dim==1){
    (it->second).Hist1DList = new TH1**[N_OF_HIST];
    FitResBox[FitHistName] = new FitResult*[N_OF_HIST];
    for(int i=0;i<N_OF_HIST;i++){
      (it->second).Hist1DList[i] = new TH1*[N_OF_HIST];
      FitResBox[FitHistName][i] = new FitResult[N_OF_HIST];
      for(int j=0;j<N_OF_HIST;j++){
	HistName = it->first;
	HistName+=Form("_Fit_Cond%d,%d",i,j);
	HistTitle = (it->second).Title;
	HistTitle+=Form(": Condition%d,%d",i,j);
	(it->second).Hist1DList[i][j] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
	(it->second).Hist1DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	(it->second).Hist1DList[i][j]->GetXaxis()->SetTitleFont(60);
	(it->second).Hist1DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	(it->second).Hist1DList[i][j]->GetYaxis()->SetTitleFont(60);
	FitResBox[FitHistName][i][j].MultiSlice = false;
	FitResBox[FitHistName][i][j].Chi2 = 0;
	for (int k=0;k<10;k++){
	  FitResBox[FitHistName][i][j].fit_val[k] = 0.0;
	  FitResBox[FitHistName][i][j].fit_error[k] = 1.0;
	}
	FitResBox[FitHistName][i][j].FitFunc = NULL;
	FitResBox[FitHistName][i][j].Chi2Func = NULL;
      }
    }
    if((it->second).MultiSlice){
      (it->second).Hist1DListSliced = new TH1***[N_OF_HIST];
      for(int i=0;i<N_OF_HIST;i++){
	(it->second).Hist1DListSliced[i] = new TH1**[N_OF_HIST];
	for(int j=0;j<N_OF_HIST;j++){
	  (it->second).Hist1DListSliced[i][j] = new TH1*[N_OF_SLICE];
	  FitResBox[FitHistName][i][j].MultiSlice = true;
	  for(int k=0;k<N_OF_SLICE;k++){
	    HistName = it->first;
	    HistName+=Form("_Fit_Cond%d,%d;Slice%d",i,j,k);
	    HistTitle = (it->second).Title;
	    HistTitle+=Form(": Condition%d,%d,Slice%d",i,j,k);
	    (it->second).Hist1DListSliced[i][j][k] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
	    (it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitle((it->second).XTitle.c_str());
	    (it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitleFont(60);
	    (it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitle((it->second).YTitle.c_str());
	    (it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitleFont(60);
	    FitResBox[FitHistName][i][j].Chi2Slice[k] = 0.0;
	  }
	}
      }
    }
  }else if((it->second).Dim==2){
    (it->second).Hist2DList = new TH2**[N_OF_HIST];
    FitResBox[FitHistName] = new FitResult*[N_OF_HIST];
    for(int i=0;i<N_OF_HIST;i++){
      (it->second).Hist2DList[i] = new TH2*[N_OF_HIST];
      FitResBox[FitHistName][i] = new FitResult[N_OF_HIST];
      for(int j=0;j<N_OF_HIST;j++){
	HistName = it->first;
	HistName+=Form("_Fit_Cond%d,%d",i,j);
	HistTitle = (it->second).Title;
	HistTitle+=Form(": Condition%d,%d",i,j);
	(it->second).Hist2DList[i][j] = new TH2D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high,(it->second).YBinNum,(it->second).YRange.low,(it->second).YRange.high);
	(it->second).Hist2DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	(it->second).Hist2DList[i][j]->GetXaxis()->SetTitleFont(60);
	(it->second).Hist2DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	(it->second).Hist2DList[i][j]->GetYaxis()->SetTitleFont(60);
	(it->second).Hist2DList[i][j]->GetZaxis()->SetTitle((it->second).ZTitle.c_str()); 
	(it->second).Hist2DList[i][j]->GetZaxis()->SetTitleFont(60);
	for (int k=0;k<10;k++){
	  FitResBox[FitHistName][i][j].fit_val[k] = 0.0;
	  FitResBox[FitHistName][i][j].fit_error[k] = 1.0;
	}
	FitResBox[FitHistName][i][j].FitFunc = NULL;
	FitResBox[FitHistName][i][j].Chi2Func = NULL;
      }
    }
  }
  it  = FitHistBox.find(FitHistNameResidual);
  if((it->second).Dim==1){
    (it->second).Hist1DList = new TH1**[N_OF_HIST];
    for(int i=0;i<N_OF_HIST;i++){
      (it->second).Hist1DList[i] = new TH1*[N_OF_HIST];
      for(int j=0;j<N_OF_HIST;j++){
	HistName = it->first;
	HistName+=Form("_Cond%d,%d",i,j);
	HistTitle = (it->second).Title;
	HistTitle+=Form(": Condition%d,%d",i,j);
	(it->second).Hist1DList[i][j] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
	(it->second).Hist1DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	(it->second).Hist1DList[i][j]->GetXaxis()->SetTitleFont(60);
	(it->second).Hist1DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	(it->second).Hist1DList[i][j]->GetYaxis()->SetTitleFont(60);
      }
    }
    if((it->second).MultiSlice){
      (it->second).Hist1DListSliced = new TH1***[N_OF_HIST];
      for(int i=0;i<N_OF_HIST;i++){
	(it->second).Hist1DListSliced[i] = new TH1**[N_OF_HIST];
	for(int j=0;j<N_OF_HIST;j++){
	  (it->second).Hist1DListSliced[i][j] = new TH1*[N_OF_SLICE];
	  for(int k=0;k<N_OF_SLICE;k++){
	    HistName = it->first;
	    HistName+=Form("_Cond%d,%d;Slice%d",i,j,k);
	    HistTitle = (it->second).Title;
	    HistTitle+=Form(": Condition%d,%d,Slice%d",i,j,k);
	    (it->second).Hist1DListSliced[i][j][k] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
	    (it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitle((it->second).XTitle.c_str());
	    (it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitleFont(60);
	    (it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitle((it->second).YTitle.c_str());
	    (it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitleFont(60);
	  }
	}
      }
    }
  }else if((it->second).Dim==2){
    (it->second).Hist2DList = new TH2**[N_OF_HIST];
    for(int i=0;i<N_OF_HIST;i++){
      (it->second).Hist2DList[i] = new TH2*[N_OF_HIST];
      for(int j=0;j<N_OF_HIST;j++){
	HistName = it->first;
	HistName+=Form("_Cond%d,%d",i,j);
	HistTitle = (it->second).Title;
	HistTitle+=Form(": Condition%d,%d",i,j);
	(it->second).Hist2DList[i][j] = new TH2D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high,(it->second).YBinNum,(it->second).YRange.low,(it->second).YRange.high);
	(it->second).Hist2DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	(it->second).Hist2DList[i][j]->GetXaxis()->SetTitleFont(60);
	(it->second).Hist2DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	(it->second).Hist2DList[i][j]->GetYaxis()->SetTitleFont(60);
	(it->second).Hist2DList[i][j]->GetZaxis()->SetTitle((it->second).ZTitle.c_str()); 
	(it->second).Hist2DList[i][j]->GetZaxis()->SetTitleFont(60);
      }
    }
  }

  //Initialize global parameters
  Std_First_N=0;  //Normalization of STD histograms
  Std_Second_N=0;

//  spline_plus=NULL;
//  spline_minus=NULL;

  /*
  //For Sliced Fit
  for (int i=0;i<N_OF_SLICE;i++){
    Std_First_Slice[i]=0;
    Std_Second_Slice[i]=0;
    spline_plus_slice[i]=NULL;
    spline_minus_slice[i]=NULL;
  }*/
  return 0;
}

/***********************************************************************************/
int Analyzer::Fit_TOF(int i,int j,range &FitRange)
{	
  if (Purpose2 == 1) { //Calibration
    cout << "****** Using GAUSS Fitter ******" << endl;
    return Fit_TOF_Gauss(i,j, FitRange);
  }
  else{
    if(FitType.compare("HONG")==0){
      cout << "****** Using HONG Fitter ******" << endl;
      return Fit_TOF_Hong(i,j, FitRange);
    } else if(FitType.compare("MINUIT")==0){
      cout << "****** Using MINUIT Fitter ******" << endl;
      return Fit_TOF_Minuit(i,j,FitRange);
    } else if(FitType.compare("ROOT")==0){
      cout << "****** Using ROOT Fitter ******" << endl;
      return Fit_TOF_Root(i,j,FitRange);
    } else if(FitType.compare("MultiSlice")==0){
      cout << "****** Using MultiSlice Fitter ******" << endl;
      return Fit_TOF_MultiSlice(i,j,FitRange);
    } else if(FitType.compare("Gauss")==0){
      cout << "****** Using GAUSS Fitter ******" << endl;
      return Fit_TOF_Gauss(i,j, FitRange);
    } else{
      cout << "Unknown Fitter Requested --> Slecting Default Fitter" << endl;
      cout << "****** Using MINUIT Fitter ******" << endl;
      return Fit_TOF_Minuit(i,j,FitRange);
    }
  }
}

/***********************************************************************************/
double spline_func(double *x, double parms[]){
    //return parms[0]*( (-1.0/3.0+parms[1])*(-3.0/2.0)*(1.0/Std_Second_N)*spline_minus->Eval(x[0]+parms[2]) + (1.0/3.0+parms[1])*(3.0/2.0)*(1.0/Std_First_N)*spline_plus->Eval(x[0]+parms[2]) );
  int Bin = Hist_minus->FindBin(x[0]+parms[2]);
  int BinBkg = Hist_Bkg->FindBin(x[0]);
  double BinPos = Hist_minus->GetBinCenter(Bin);
  double BinVal_minus = Hist_minus->GetBinContent(Bin);
  double BinVal_plus = Hist_plus->GetBinContent(Bin);
  double BinPos2;
  double BinVal_minus2;
  double BinVal_plus2;
  if (x[0]+parms[2]>=BinPos){
    BinPos2 = Hist_minus->GetBinCenter(Bin+1);
    BinVal_minus2 = Hist_minus->GetBinContent(Bin+1);
    BinVal_plus2 = Hist_plus->GetBinContent(Bin+1);
  }else{
    BinPos2 = Hist_minus->GetBinCenter(Bin-1);
    BinVal_minus2 = Hist_minus->GetBinContent(Bin-1);
    BinVal_plus2 = Hist_plus->GetBinContent(Bin-1);
  }
  double spline_minus = BinVal_minus + (BinVal_minus2-BinVal_minus)*(x[0]+parms[2]-BinPos)/(BinPos2-BinPos);
  double spline_plus = BinVal_plus + (BinVal_plus2-BinVal_plus)*(x[0]+parms[2]-BinPos)/(BinPos2-BinPos);
  return parms[0]*((1.0/3.0-parms[1])*spline_minus + (1.0/3.0+parms[1])*spline_plus)/((1.0/3.0-parms[1])*Std_Second_N + (1.0/3.0+parms[1])*Std_First_N)+Hist_Bkg->GetBinContent(BinBkg);
}

double Chi2Function(const double *xx )
{
  double Chi2=0;
  int N = Hist_Fit->GetNbinsX();
  int NStd = Hist_minus->GetNbinsX();
  double a = xx[0];
  double dt = xx[1];
//  cout <<a<<" "<<dt<<endl;
  int iStart = Hist_Fit->FindBin(GlobalRangeLow);
  int iStop = Hist_Fit->FindBin(GlobalRangeHigh);
  for (int i=iStart;i<=iStop;i++){
    double x = Hist_Fit->GetBinCenter(i);
    double y = Hist_Fit->GetBinContent(i);
    double e = Hist_Fit->GetBinError(i);
    int Bin = Hist_minus->FindBin(x+dt);
    if (y<=0.0)continue;
    if (Bin<1)continue;
    if (Bin>=NStd)break;

//    cout << x<<" "<<y<<" "<<e<<endl;
    //Find Fit val
    double BinPos = Hist_minus->GetBinCenter(Bin);
    double BinVal_minus = Hist_minus->GetBinContent(Bin);
    double BinVal_plus = Hist_plus->GetBinContent(Bin);
    double BinPos2;
    double BinVal_minus2;
    double BinVal_plus2;
    if (x+dt>=BinPos){
      BinPos2 = Hist_minus->GetBinCenter(Bin+1);
      BinVal_minus2 = Hist_minus->GetBinContent(Bin+1);
      BinVal_plus2 = Hist_plus->GetBinContent(Bin+1);
    }else{
      BinPos2 = Hist_minus->GetBinCenter(Bin-1);
      BinVal_minus2 = Hist_minus->GetBinContent(Bin-1);
      BinVal_plus2 = Hist_plus->GetBinContent(Bin-1);
    }
    double spline_minus = BinVal_minus + (BinVal_minus2-BinVal_minus)*(x+dt-BinPos)/(BinPos2-BinPos);
    double spline_plus = BinVal_plus + (BinVal_plus2-BinVal_plus)*(x+dt-BinPos)/(BinPos2-BinPos);
    double y_fit = Fit_N*((1.0/3.0-a)*spline_minus + (1.0/3.0+a)*spline_plus)/((1.0/3.0-a)*Std_Second_N + (1.0/3.0+a)*Std_First_N);
    double e_fit_2 =pow(Fit_N/((1.0/3.0-a)*Std_Second_N + (1.0/3.0+a)*Std_First_N),2.0)*(pow((1.0/3.0-a),2.0)*spline_minus + pow((1.0/3.0+a),2.0)*spline_plus);
//    cout << y_fit<<" "<<e_fit<<endl;

    //Calculate Chi2
    if(e>0.0 || e_fit_2>0.0)Chi2+=(y-y_fit)*(y-y_fit)/(e_fit_2+e*e);
  }
  return Chi2;
}

double Chi2Function_p(const double *xx,const double *pp )
{
  double Chi2=0;
  int N = Hist_Fit->GetNbinsX();
  int NStd = Hist_minus->GetNbinsX();
  double a = xx[0];
  double dt = xx[1];
//  cout <<a<<" "<<dt<<endl;
  int iStart = Hist_Fit->FindBin(GlobalRangeLow);
  int iStop = Hist_Fit->FindBin(GlobalRangeHigh);
  for (int i=iStart;i<=iStop;i++){
    double x = Hist_Fit->GetBinCenter(i);
    double y = Hist_Fit->GetBinContent(i);
    double e = Hist_Fit->GetBinError(i);
    int Bin = Hist_minus->FindBin(x+dt);
    if (y<=0.0)continue;
    if (Bin<1)continue;
    if (Bin>=NStd)break;

//    cout << x<<" "<<y<<" "<<e<<endl;
    //Find Fit val
    double BinPos = Hist_minus->GetBinCenter(Bin);
    double BinVal_minus = Hist_minus->GetBinContent(Bin);
    double BinVal_plus = Hist_plus->GetBinContent(Bin);
    double BinPos2;
    double BinVal_minus2;
    double BinVal_plus2;
    if (x+dt>=BinPos){
      BinPos2 = Hist_minus->GetBinCenter(Bin+1);
      BinVal_minus2 = Hist_minus->GetBinContent(Bin+1);
      BinVal_plus2 = Hist_plus->GetBinContent(Bin+1);
    }else{
      BinPos2 = Hist_minus->GetBinCenter(Bin-1);
      BinVal_minus2 = Hist_minus->GetBinContent(Bin-1);
      BinVal_plus2 = Hist_plus->GetBinContent(Bin-1);
    }
    double spline_minus = BinVal_minus + (BinVal_minus2-BinVal_minus)*(x+dt-BinPos)/(BinPos2-BinPos);
    double spline_plus = BinVal_plus + (BinVal_plus2-BinVal_plus)*(x+dt-BinPos)/(BinPos2-BinPos);
    double y_fit = Fit_N*((1.0/3.0-a)*spline_minus + (1.0/3.0+a)*spline_plus)/((1.0/3.0-a)*Std_Second_N + (1.0/3.0+a)*Std_First_N);
    double e_fit_2 =pow(Fit_N/((1.0/3.0-a)*Std_Second_N + (1.0/3.0+a)*Std_First_N),2.0)*(pow((1.0/3.0-a),2.0)*spline_minus + pow((1.0/3.0+a),2.0)*spline_plus);
//    cout << y_fit<<" "<<e_fit<<endl;

    //Calculate Chi2
    if(e>0.0 || e_fit_2>0.0)Chi2+=(y-y_fit)*(y-y_fit)/(e_fit_2+e*e);
  }
  return Chi2;
}
/***********************************************************************************/
int Analyzer::Fit_TOF_Gauss(int i,int j,range &FitRange){
  //Depend on Purpose, choose histogram box
  map <string,HistUnit>* ChosenHistBox;
  if (Purpose==0){
    ChosenHistBox = &SimHistBox;
  }else if(Purpose==1){
    ChosenHistBox = &ExpHistBox;
  }

  TH1* TargetHist = ((*ChosenHistBox)["TOFHist"].Hist1DList)[i][j]; //Set the Fit target
  if (TargetHist==NULL){
  	cout << "ERROR: TOFHist["<<i<<"]["<<j<<"] was never initialized. Make sure histogram is enabled. ABORTING FIT!"<<endl;
  	return -1;
  }

  if (FitHistBox.find("TOFHist")==FitHistBox.end()) InitFit("TOFHist"); //Initialize fit result and histograms

  TF1 *fit_func = new TF1("fit_func","gaus",FitRange.low,FitRange.high);
  TargetHist->GetXaxis()->SetRangeUser(FitRange.low,FitRange.high);
  
  fit_func->SetParameter(0,TargetHist->GetEntries());
  cout<<"Mean: "<<TargetHist->GetMean()<<endl; 
  fit_func->SetParameter(1,TargetHist->GetMean()); //intial guess for TOF of He4 ion based on a mean field of 1.66 kV/cm 335
  fit_func->SetParameter(2,2); 
  //fit_func->SetParLimits(1,100,600);
  TFitResultPtr r = TargetHist->Fit("fit_func","RN0E","",FitRange.low,FitRange.high);
  Int_t fitStatus = r;
  if (r<0) {
  	cout << "Error in fit. Histogram maybe empty in fit range. Check timeshift fit range. ABORTING FIT!"<<endl;
  	return -1;
  }
  double low = fit_func->GetParameter(1) -40;
  double high = fit_func->GetParameter(1) +40;
  r = TargetHist->Fit("fit_func","N0","",low,high);
  if (r<0) {
  	cout << "Error in fit. Histogram maybe empty in fit range. Check timeshift fit range. ABORTING FIT!"<<endl;
  	return -1;
  }
  
  cout << "Chi2 = "<< fit_func->GetChisquare() <<" : Dof = "<<fit_func->GetNDF()<<endl;
  
  double mean, meanErr;
  
  mean = TargetHist->GetMean();
  meanErr = TargetHist->GetMeanError();
  cout<<"Mean = "<<mean<<"+/-"<<meanErr<<endl;

  FitResBox["TOFHist"][i][j].fit_val[0] = fit_func->GetParameter(1);
  FitResBox["TOFHist"][i][j].fit_error[0] = fit_func->GetParError(1);
  FitResBox["TOFHist"][i][j].fit_val[1] = fit_func->GetParameter(2);
  FitResBox["TOFHist"][i][j].fit_error[1] = fit_func->GetParError(2);
  FitResBox["TOFHist"][i][j].Chi2 = fit_func->GetChisquare()/fit_func->GetNDF();

  cout << "Fitting complete!"<<endl;
  cout << "TOF = " <<FitResBox["TOFHist"][i][j].fit_val[0] << " +/- "<<FitResBox["TOFHist"][i][j].fit_error[0]<<endl;
  AnalysisLog <<"**********Fit Result:**********\n";
  AnalysisLog <<"Histogram Indices: "<<i<<" "<<j<<endl;
  AnalysisLog <<"FitType:" <<FitType<<"; Fit range: ["<<FitRange.low<<","<<FitRange.high<<"]"<<endl;
  AnalysisLog << "TOF = " <<FitResBox["TOFHist"][i][j].fit_val[0] << " +/- "<<FitResBox["TOFHist"][i][j].fit_error[0]<<endl;
  AnalysisLog << "Chi2 = " << fit_func->GetChisquare()<<" : Dof = "<<fit_func->GetNDF() <<endl;

  //Construct histogram
  string newname = FitHistBox["TOFHist"].Hist1DList[i][j]->GetName();
  delete FitHistBox["TOFHist"].Hist1DList[i][j];
  FitHistBox["TOFHist"].Hist1DList[i][j]=NULL;

//  if (FitHistBox["TOFHist"].Hist1DList[i][j]!=NULL)cout <<"YYYYYYYYY"<<endl;
  FitHistBox["TOFHist"].Hist1DList[i][j] = (TH1*)(fit_func->GetHistogram()->Clone(newname.c_str()));
//  cout <<"XXXXXXXX"<<endl;
  //    TOF_Fit[i] = fit_func->GetHistogram(); 

  /*  if (TOF_Res[i] != NULL) {
      delete TOF_Res[i];
      TOF_Res[i]=NULL;
      }*/


  //   TOF_Res[i] = new TH1D(Form("TOF_Res_Cond%d",i),"TOF Residual",500,0,500);

  int N_start = TargetHist->FindBin(FitRange.low);
  int N_stop  = TargetHist->FindBin(FitRange.high);

  double h_error;

  for (int k=N_start; k<=N_stop; k++){
    h_error = TargetHist->GetBinError(k);
    if (h_error!=0)
      FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->SetBinContent(k,(TargetHist->GetBinContent(k)-fit_func->Eval(TargetHist->GetBinCenter(k)) )/h_error);
    else
      FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->SetBinContent(k,(TargetHist->GetBinContent(k)-fit_func->Eval(TargetHist->GetBinCenter(k)) ));
  }
  (*ChosenHistBox)["TOFHist"].Hist1DList[i][j]->GetXaxis()->SetRangeUser(FitRange.low,FitRange.high);
  FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->GetXaxis()->SetRangeUser(FitRange.low,FitRange.high);

  delete fit_func; 
  return 0;

}

/***********************************************************************************/
int Analyzer::Fit_TOF_Minuit(int i,int j,range &FitRange){
  //Depend on Purpose, choose histogram box
  map <string,HistUnit>* ChosenHistBox;
  if (Purpose==0){
    ChosenHistBox = &SimHistBox;
  }else if(Purpose==1){
    ChosenHistBox = &ExpHistBox;
  }

  TH1* TargetHist = ((*ChosenHistBox)["TOFHist"].Hist1DList)[i][j]; //Set the Fit target
  if (TargetHist==NULL){
  	cout << "ERROR: TOFHist["<<i<<"]["<<j<<"] was never initialized. Make sure histogram is enabled. ABORTING FIT!"<<endl;
  	return -1;
  }
  if (FitHistBox.find("TOFHist")==FitHistBox.end()) InitFit("TOFHist"); //Initialize fit result and histograms

  //Bin Range
  int Bin_Low = Std_First->FindBin(FitRange.low);
  int Bin_High = Std_First->FindBin(FitRange.high);
  int Bin_Low_Exp = TargetHist->FindBin(FitRange.low);
  int Bin_High_Exp = TargetHist->FindBin(FitRange.high);

  Std_First_N = Std_First->Integral(Bin_Low,Bin_High);
  Std_Second_N = Std_Second->Integral(Bin_Low,Bin_High);
  Fit_N = TargetHist->Integral(Bin_Low_Exp,Bin_High_Exp);
  GlobalRangeLow = FitRange.low;
  GlobalRangeHigh = FitRange.high;

//  spline_plus = new TSpline5((TH1*)Std_First,0,TOF_Low,TOF_High);
//  spline_minus = new TSpline5((TH1*)Std_Second,0,TOF_Low,TOF_High);
  Hist_plus = Std_First;
  Hist_minus = Std_Second;
  Hist_Fit = TargetHist;

  TF1 *fit_func = new TF1("fit_func",spline_func,TOF_Low,TOF_High,3);
  fit_func->SetNpx(TOF_N);
  fit_func->FixParameter(0,TargetHist->Integral(Bin_Low_Exp,Bin_High_Exp));//Normalization is fixed to "a" scaling, see definiation of the spline function
  fit_func->SetParameter(1,0);
  fit_func->SetParameter(2,0);

  //Use Function Fit first
  if(floatTime){
    fit_func->SetParLimits(2,-5.0,5.0);
    fit_func->SetParameter(2,TimeShift);
  } 
  else{
    fit_func->FixParameter(2,TimeShift);
  }
 // fit_func->SetParLimits(1,-0.333333333,0.333333333);
  TargetHist->Fit("fit_func","LRN0","",FitRange.low,FitRange.high);
  double func_a_fit = fit_func->GetParameter(1);
  double func_a_error = fit_func->GetParError(1);
  double func_t_fit = fit_func->GetParameter(2);
  double func_t_error = fit_func->GetParError(2);
  
  cout << "Use Function Fitter first"<<endl;
  cout << "Chi2 = "<< fit_func->GetChisquare() <<" : Dof = "<<fit_func->GetNDF()<<endl;
  cout << "    a     = " << func_a_fit  << " +/- "<<func_a_error << endl;
  cout << "TimeShift = " << func_t_fit  << " +/- "<<func_t_error << endl;

  /*
  FitResBox["TOFHist"][i][j].fit_val[0] = fit_func->GetParameter(1);
  FitResBox["TOFHist"][i][j].fit_error[0] = fit_func->GetParError(1);
  FitResBox["TOFHist"][i][j].fit_val[1] = fit_func->GetParameter(2);
  FitResBox["TOFHist"][i][j].fit_error[1] = fit_func->GetParError(2);
  FitResBox["TOFHist"][i][j].Chi2 = fit_func->GetChisquare()/fit_func->GetNDF();
*/

  //Use Chi2 minimizer
  ROOT::Math::Minimizer* Chi2Minimizer = ROOT::Math::Factory::CreateMinimizer("Minuit", "Combined");
  // set tolerance , etc...
  Chi2Minimizer->SetMaxFunctionCalls(1000000); // for Minuit/Minuit2 
  Chi2Minimizer->SetMaxIterations(10000);  // for GSL 
  Chi2Minimizer->SetTolerance(0.001);
  Chi2Minimizer->SetPrintLevel(-1);

  // create funciton wrapper for Chi2Minimizermizer
  // a IMultiGenFunction type 
  ROOT::Math::Functor f(&Chi2Function,2);
  double step[2] = {0.0001,0.001};

  Chi2Minimizer->SetFunction(f);

  // starting point
  double variable[2];// = { -1.,1.2};
  variable[0] = func_a_fit;
  variable[1] = func_t_fit;

  // Set the free variables to be minimized!
  Chi2Minimizer->SetVariable(0,"a",variable[0], step[0]);
  if(floatTime){
    cout << "Allowing leading edge to float.\n";
    Chi2Minimizer->SetVariable(1,"TimeShift",variable[1], step[1]);
  }
  else{
    cout << "Fixing leading edge of fits.\n";
    Chi2Minimizer->SetFixedVariable(1,"TimeShift",variable[1]);
  }

  // do the minimization
  Chi2Minimizer->Minimize();
  //Get Fit parameters
  const double *ParList = Chi2Minimizer->X();

  //update the function too
  fit_func->SetParameter(1,ParList[0]);
  fit_func->SetParameter(2,ParList[1]);

  //Construct the 2D Chi2 function
  TF2 *Chi2_func = new TF2("Chi2_func",Chi2Function_p,ParList[0]-5.0*func_a_error,ParList[0]+5.0*func_a_error,ParList[1]-5.0*func_t_error,ParList[1]+5.0*func_t_error,0);
  Chi2_func->SetNpx(200);
  Chi2_func->SetNpy(200);

  //TestOut
  TFile* testout = new TFile("FitTestOut.root","recreate");
  fit_func->Write();
  Chi2_func->Write();
  testout->Close();

  //Calculate NDF by hand
  int NDF=0;

  int N_start = TargetHist->FindBin(FitRange.low);
  int N_stop  = TargetHist->FindBin(FitRange.high);
  int NStd    = Hist_minus->GetNbinsX();
  for (int k=N_start; k<=N_stop; k++){
    if (TargetHist->GetBinContent(k)>0.0)NDF++;
  }
  if(floatTime){
    NDF-=2;
  }
  else{
    NDF-=1;
  }

  //Calculate uncertainties by hand
  //Scan over an area with delta_Chi2 = 2.30, get limits of a and dt
  double a_interval = 3.0*func_a_error/100.0;
  double t_interval = 3.0*func_t_error/100.0;
  TH2* LimitFinder = new TH2D("LimitFinder","LimitFinder",200,ParList[0]-100*a_interval,ParList[0]+100*a_interval,200,ParList[1]-100*t_interval,ParList[1]+100*t_interval);
  double MinVal = Chi2Minimizer->MinValue();
  for (int k=-100;k<100;k++){
    for (int l=-100;l<100;l++){
      double var[2] = {ParList[0]+(k+0.5)*a_interval,ParList[1]+(l+0.5)*t_interval};
      if (Chi2Function(var)<MinVal+1.0){
	//LimitFinder->SetBinContent(k+251,l+251,1);
	LimitFinder->Fill(ParList[0]+(k+0.5)*a_interval,ParList[1]+(l+0.5)*t_interval,1.0);
      }
    }
  }
  TH1* LimitFinderProjectX = (TH1*)(LimitFinder->ProjectionX()->Clone());
  TH1* LimitFinderProjectY = (TH1*)(LimitFinder->ProjectionY()->Clone());
  double a_lowerlimit=0.0;
  double a_upperlimit=0.0;
  double t_lowerlimit=0.0;
  double t_upperlimit=0.0;
  
  for (int k=1;k<=200;k++){
    if (LimitFinderProjectX->GetBinContent(k)>1.0){
      a_lowerlimit=LimitFinderProjectX->GetBinCenter(k);
      break;
    }
  }
  for (int k=200;k>=1;k--){
    if (LimitFinderProjectX->GetBinContent(k)>1.0){
      a_upperlimit=LimitFinderProjectX->GetBinCenter(k);
      break;
    }
  }
  for (int k=1;k<=200;k++){
    if (LimitFinderProjectY->GetBinContent(k)>0.0){
      t_lowerlimit=LimitFinderProjectX->GetBinCenter(k);
      break;
    }
  }
  for (int k=200;k>=1;k--){
    if (LimitFinderProjectY->GetBinContent(k)>0.0){
      t_upperlimit=LimitFinderProjectX->GetBinCenter(k);
      break;
    }
  }


  cout << "Chi2 = "<< Chi2Minimizer->MinValue() <<" : Dof = "<<NDF<<endl;
  cout << "TimeShift = " << ParList[1] << endl;

  FitResBox["TOFHist"][i][j].fit_val[0] = ParList[0];
  FitResBox["TOFHist"][i][j].fit_error[0] = (a_upperlimit-a_lowerlimit)/2.0;
  FitResBox["TOFHist"][i][j].fit_val[1] = ParList[1];
  FitResBox["TOFHist"][i][j].fit_error[1] = (t_upperlimit-t_lowerlimit)/2.0;
  FitResBox["TOFHist"][i][j].Chi2 = Chi2Minimizer->MinValue()/double(NDF);
  FitResBox["TOFHist"][i][j].FitFunc = fit_func;
  FitResBox["TOFHist"][i][j].Chi2Func = Chi2_func;

  cout << "Fitting complete!"<<endl;
  cout << "a = " <<FitResBox["TOFHist"][i][j].fit_val[0] << " +/- "<<FitResBox["TOFHist"][i][j].fit_error[0]<<endl;
  cout << "TShift = " <<FitResBox["TOFHist"][i][j].fit_val[1] << " +/- "<<FitResBox["TOFHist"][i][j].fit_error[1]<<endl;
  AnalysisLog <<"**********Fit Result:**********\n";
  AnalysisLog <<"Histogram Indices: "<<i<<" "<<j<<endl;
  AnalysisLog <<"FitType:" <<FitType<<"; Fit range: ["<<FitRange.low<<","<<FitRange.high<<"]"<<endl;
  AnalysisLog << "a = " <<FitResBox["TOFHist"][i][j].fit_val[0] << " +/- "<<FitResBox["TOFHist"][i][j].fit_error[0]<<endl;
  AnalysisLog << "TimeShift = " << FitResBox["TOFHist"][i][j].fit_val[1] << endl;
  AnalysisLog << "Chi2 = " << Chi2Minimizer->MinValue()<<" : Dof = "<<NDF <<endl;

  //Construct histogram
  string newname = FitHistBox["TOFHist"].Hist1DList[i][j]->GetName();
  delete FitHistBox["TOFHist"].Hist1DList[i][j];
  FitHistBox["TOFHist"].Hist1DList[i][j]=NULL;

  FitHistBox["TOFHist"].Hist1DList[i][j] = (TH1*)(fit_func->GetHistogram()->Clone(newname.c_str()));

  double h_error;

  for (int k=N_start; k<=N_stop; k++){
    double a = FitResBox["TOFHist"][i][j].fit_val[0];
    double dt = FitResBox["TOFHist"][i][j].fit_val[1];
    double x = TargetHist->GetBinCenter(k);
    double y = TargetHist->GetBinContent(k);
    double e = TargetHist->GetBinError(k);
    int Bin = Hist_minus->FindBin(x+dt);
    if (y<=0.0)continue;
    if (Bin<1)continue;
    if (Bin>=NStd)break;

    //Find Fit val
    double BinPos = Hist_minus->GetBinCenter(Bin);
    double BinVal_minus = Hist_minus->GetBinContent(Bin);
    double BinVal_plus = Hist_plus->GetBinContent(Bin);
    double BinPos2;
    double BinVal_minus2;
    double BinVal_plus2;
    if (x+dt>=BinPos){
      BinPos2 = Hist_minus->GetBinCenter(Bin+1);
      BinVal_minus2 = Hist_minus->GetBinContent(Bin+1);
      BinVal_plus2 = Hist_plus->GetBinContent(Bin+1);
    }else{
      BinPos2 = Hist_minus->GetBinCenter(Bin-1);
      BinVal_minus2 = Hist_minus->GetBinContent(Bin-1);
      BinVal_plus2 = Hist_plus->GetBinContent(Bin-1);
    }
    double spline_minus = BinVal_minus + (BinVal_minus2-BinVal_minus)*(x+dt-BinPos)/(BinPos2-BinPos);
    double spline_plus = BinVal_plus + (BinVal_plus2-BinVal_plus)*(x+dt-BinPos)/(BinPos2-BinPos);
    double y_fit = Fit_N*((1.0/3.0-a)*spline_minus + (1.0/3.0+a)*spline_plus)/((1.0/3.0-a)*Std_Second_N + (1.0/3.0+a)*Std_First_N);
    double e_fit_2 =pow(Fit_N/((1.0/3.0-a)*Std_Second_N + (1.0/3.0+a)*Std_First_N),2.0)*(pow((1.0/3.0-a),2.0)*spline_minus + pow((1.0/3.0+a),2.0)*spline_plus);

    //Calculate Chi2
    h_error = sqrt(e_fit_2+e*e);
    if (h_error!=0)
      FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->SetBinContent(k,(y-y_fit)/h_error);
    else
      FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->SetBinContent(k,y-y_fit);
  }
  (*ChosenHistBox)["TOFHist"].Hist1DList[i][j]->GetXaxis()->SetRangeUser(FitRange.low,FitRange.high);
  FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->GetXaxis()->SetRangeUser(FitRange.low,FitRange.high);

  //Calculate Chi2 by hand
  double Chi2User=0.0;
  for (int k=N_start; k<=N_stop; k++){
    Chi2User+=pow(FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->GetBinContent(k),2.0);
  }
  cout<<"Chi2 calculated by user: "<<Chi2User<<endl;

//  delete spline_plus;
//  delete spline_minus;
  delete LimitFinder;
  return 0;

}

//Using Root Fitter
int Analyzer::Fit_TOF_Root(int i,int j,range &FitRange){
  //Depend on Purpose, choose histogram box
  map <string,HistUnit>* ChosenHistBox;
  if (Purpose==0){
    ChosenHistBox = &SimHistBox;
  }else if(Purpose==1){
    ChosenHistBox = &ExpHistBox;
  }

  TH1* TargetHist = ((*ChosenHistBox)["TOFHist"].Hist1DList)[i][j]; //Set the Fit target
  if (TargetHist==NULL){
  	cout << "ERROR: TOFHist["<<i<<"]["<<j<<"] was never initialized. Make sure histogram is enabled. ABORTING FIT!"<<endl;
  	return -1;
  }
  TH1* histbkg;
  if (ExtBkgHistBox["Q_Cut_ON"]==NULL){
    cout <<"Warning: Bkg histogram not loaded."<<endl;
    cout <<"Zero bkg histogram is used."<<endl;
    histbkg= (TH1*)((*ChosenHistBox)["TOFHist"].Hist1DList)[i][j]->Clone(); 
    histbkg->Scale(0.0);
  }else{
    histbkg = (TH1*)(ExtBkgHistBox["Q_Cut_ON"]->Clone());//Set Bkg Hist
    histbkg->Scale(BkgNorm);
  }

  if (FitHistBox.find("TOFHist")==FitHistBox.end()) InitFit("TOFHist"); //Initialize fit result and histograms

  //Bin Range
  int Bin_Low = Std_First->FindBin(FitRange.low);
  int Bin_High = Std_First->FindBin(FitRange.high);
  int Bin_Low_Exp = TargetHist->FindBin(FitRange.low);
  int Bin_High_Exp = TargetHist->FindBin(FitRange.high);

  Std_First_N = Std_First->Integral(Bin_Low,Bin_High);
  Std_Second_N = Std_Second->Integral(Bin_Low,Bin_High);
  Fit_N = TargetHist->Integral(Bin_Low_Exp,Bin_High_Exp);
  GlobalRangeLow = FitRange.low;
  GlobalRangeHigh = FitRange.high;

//  spline_plus = new TSpline5((TH1*)Std_First,0,TOF_Low,TOF_High);
//  spline_minus = new TSpline5((TH1*)Std_Second,0,TOF_Low,TOF_High);
  Hist_plus = Std_First;
  Hist_minus = Std_Second;
  Hist_Fit = TargetHist;
  Hist_Bkg = histbkg;

  TF1 *fit_func = new TF1("fit_func",spline_func,TOF_Low,TOF_High,3);
  fit_func->SetNpx(TOF_N);
  fit_func->FixParameter(0,TargetHist->Integral(Bin_Low_Exp,Bin_High_Exp)-histbkg->Integral(Bin_Low_Exp,Bin_High_Exp));//Normalization is fixed to "a" scaling, see definiation of the spline function
  fit_func->SetParameter(1,0);
  fit_func->SetParameter(2,0);

  //Use Function Fit first
  if(floatTime){
    fit_func->SetParLimits(2,-5.0,5.0);
    fit_func->SetParameter(2,TimeShift);
  } 
  else{
    fit_func->FixParameter(2,TimeShift);
  }
 // fit_func->SetParLimits(1,-0.333333333,0.333333333);
//  fit_func->FixParameter(1,-0.300988);
  TargetHist->Fit("fit_func","LRN0","",FitRange.low,FitRange.high);
  double func_a_fit = fit_func->GetParameter(1);
  double func_a_error = fit_func->GetParError(1);
  double func_t_fit = fit_func->GetParameter(2);
  double func_t_error = fit_func->GetParError(2);

  //Construct the 2D Chi2 function
  TF2 *Chi2_func = new TF2("Chi2_func",Chi2Function_p,func_a_fit-5.0*func_a_error,func_a_fit+5.0*func_a_error,func_t_fit-5.0*func_t_error,func_t_fit+5.0*func_t_error,0);
  Chi2_func->SetNpx(200);
  Chi2_func->SetNpy(200);

  //TestOut
  TFile* testout = new TFile("FitTestOut.root","recreate");
  fit_func->Write();
  Chi2_func->Write();
  testout->Close();

  //Calculate NDF by hand
  int NDF=0;

  int N_start = TargetHist->FindBin(FitRange.low);
  int N_stop  = TargetHist->FindBin(FitRange.high);
  int NStd    = Hist_minus->GetNbinsX();
  for (int k=N_start; k<=N_stop; k++){
    if (TargetHist->GetBinContent(k)>0.0)NDF++;
  }
  if(floatTime){
    NDF-=2;
  }
  else{
    NDF-=1;
  }

  /*
  //Calculate uncertainties by hand
  //Scan over an area with delta_Chi2 = 2.30, get limits of a and dt
  double a_interval = 3.0*func_a_error/100.0;
  double t_interval = 3.0*func_t_error/100.0;
  TH2* LimitFinder = new TH2D("LimitFinder","LimitFinder",200,ParList[0]-100*a_interval,ParList[0]+100*a_interval,200,ParList[1]-100*t_interval,ParList[1]+100*t_interval);
  double MinVal = Chi2Minimizer->MinValue();
  for (int k=-100;k<100;k++){
    for (int l=-100;l<100;l++){
      double var[2] = {ParList[0]+(k+0.5)*a_interval,ParList[1]+(l+0.5)*t_interval};
      if (Chi2Function(var)<MinVal+1.0){
	//LimitFinder->SetBinContent(k+251,l+251,1);
	LimitFinder->Fill(ParList[0]+(k+0.5)*a_interval,ParList[1]+(l+0.5)*t_interval,1.0);
      }
    }
  }
  TH1* LimitFinderProjectX = (TH1*)(LimitFinder->ProjectionX()->Clone());
  TH1* LimitFinderProjectY = (TH1*)(LimitFinder->ProjectionY()->Clone());
  double a_lowerlimit=0.0;
  double a_upperlimit=0.0;
  double t_lowerlimit=0.0;
  double t_upperlimit=0.0;
  
  for (int k=1;k<=200;k++){
    if (LimitFinderProjectX->GetBinContent(k)>1.0){
      a_lowerlimit=LimitFinderProjectX->GetBinCenter(k);
      break;
    }
  }
  for (int k=200;k>=1;k--){
    if (LimitFinderProjectX->GetBinContent(k)>1.0){
      a_upperlimit=LimitFinderProjectX->GetBinCenter(k);
      break;
    }
  }
  for (int k=1;k<=200;k++){
    if (LimitFinderProjectY->GetBinContent(k)>0.0){
      t_lowerlimit=LimitFinderProjectX->GetBinCenter(k);
      break;
    }
  }
  for (int k=200;k>=1;k--){
    if (LimitFinderProjectY->GetBinContent(k)>0.0){
      t_upperlimit=LimitFinderProjectX->GetBinCenter(k);
      break;
    }
  }
*/  

  cout << "Use Function Fitter first"<<endl;
  cout << "Chi2 = "<< fit_func->GetChisquare() <<" : Dof = "<<fit_func->GetNDF()<<endl;
  cout << "    a     = " << func_a_fit  << " +/- "<<func_a_error << endl;
  cout << "TimeShift = " << func_t_fit  << " +/- "<<func_t_error << endl;

  
  FitResBox["TOFHist"][i][j].fit_val[0] = fit_func->GetParameter(1);
  FitResBox["TOFHist"][i][j].fit_error[0] = fit_func->GetParError(1);
  FitResBox["TOFHist"][i][j].fit_val[1] = fit_func->GetParameter(2);
  FitResBox["TOFHist"][i][j].fit_error[1] = fit_func->GetParError(2);
  FitResBox["TOFHist"][i][j].Chi2 = fit_func->GetChisquare()/fit_func->GetNDF();
  FitResBox["TOFHist"][i][j].FitFunc = fit_func;
  FitResBox["TOFHist"][i][j].Chi2Func = Chi2_func;

  cout << "Fitting complete!"<<endl;
//  cout << "a = " <<FitResBox["TOFHist"][i][j].fit_val[0] << " +/- "<<FitResBox["TOFHist"][i][j].fit_error[0]<<endl;
//  cout << "TShift = " <<FitResBox["TOFHist"][i][j].fit_val[1] << " +/- "<<FitResBox["TOFHist"][i][j].fit_error[1]<<endl;
  AnalysisLog <<"**********Fit Result:**********\n";
  AnalysisLog <<"Histogram Indices: "<<i<<" "<<j<<endl;
  AnalysisLog <<"FitType:" <<FitType<<"; Fit range: ["<<FitRange.low<<","<<FitRange.high<<"]"<<endl;
  AnalysisLog << "a = " <<FitResBox["TOFHist"][i][j].fit_val[0] << " +/- "<<FitResBox["TOFHist"][i][j].fit_error[0]<<endl;
  AnalysisLog << "TimeShift = " << FitResBox["TOFHist"][i][j].fit_val[1] << endl;
  AnalysisLog << "Chi2 = " << fit_func->GetChisquare()<<" : Dof = "<<fit_func->GetNDF() <<endl;

  //Construct histogram
  string newname = FitHistBox["TOFHist"].Hist1DList[i][j]->GetName();
  delete FitHistBox["TOFHist"].Hist1DList[i][j];
  FitHistBox["TOFHist"].Hist1DList[i][j]=NULL;

  FitHistBox["TOFHist"].Hist1DList[i][j] = (TH1*)(fit_func->GetHistogram()->Clone(newname.c_str()));

  double h_error;

  for (int k=N_start; k<=N_stop; k++){
    double a = FitResBox["TOFHist"][i][j].fit_val[0];
    double dt = FitResBox["TOFHist"][i][j].fit_val[1];
    double x = TargetHist->GetBinCenter(k);
    double y = TargetHist->GetBinContent(k);
    double e = TargetHist->GetBinError(k);
    int Bin = Hist_minus->FindBin(x+dt);
    if (y<=0.0)continue;
    if (Bin<1)continue;
    if (Bin>=NStd)break;

    //Find Fit val
    double BinPos = Hist_minus->GetBinCenter(Bin);
    double BinVal_minus = Hist_minus->GetBinContent(Bin);
    double BinVal_plus = Hist_plus->GetBinContent(Bin);
    double BinPos2;
    double BinVal_minus2;
    double BinVal_plus2;
    if (x+dt>=BinPos){
      BinPos2 = Hist_minus->GetBinCenter(Bin+1);
      BinVal_minus2 = Hist_minus->GetBinContent(Bin+1);
      BinVal_plus2 = Hist_plus->GetBinContent(Bin+1);
    }else{
      BinPos2 = Hist_minus->GetBinCenter(Bin-1);
      BinVal_minus2 = Hist_minus->GetBinContent(Bin-1);
      BinVal_plus2 = Hist_plus->GetBinContent(Bin-1);
    }
    double spline_minus = BinVal_minus + (BinVal_minus2-BinVal_minus)*(x+dt-BinPos)/(BinPos2-BinPos);
    double spline_plus = BinVal_plus + (BinVal_plus2-BinVal_plus)*(x+dt-BinPos)/(BinPos2-BinPos);
    double y_fit = Fit_N*((1.0/3.0-a)*spline_minus + (1.0/3.0+a)*spline_plus)/((1.0/3.0-a)*Std_Second_N + (1.0/3.0+a)*Std_First_N);
    double e_fit_2 =pow(Fit_N/((1.0/3.0-a)*Std_Second_N + (1.0/3.0+a)*Std_First_N),2.0)*(pow((1.0/3.0-a),2.0)*spline_minus + pow((1.0/3.0+a),2.0)*spline_plus);

    //Calculate Chi2
    h_error = sqrt(e_fit_2+e*e);
    if (h_error!=0)
      FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->SetBinContent(k,(y-y_fit)/h_error);
    else
      FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->SetBinContent(k,y-y_fit);
  }
  (*ChosenHistBox)["TOFHist"].Hist1DList[i][j]->GetXaxis()->SetRangeUser(FitRange.low,FitRange.high);
  FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->GetXaxis()->SetRangeUser(FitRange.low,FitRange.high);

  //Calculate Chi2 by hand
  double Chi2User=0.0;
  for (int k=N_start; k<=N_stop; k++){
    Chi2User+=pow(FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->GetBinContent(k),2.0);
  }
  cout<<"Chi2 calculated by user: "<<Chi2User<<endl;

//  delete spline_plus;
//  delete spline_minus;
  return 0;

}

//Fit TOF spectrum and extract a
int Analyzer::Fit_TOF_Hong(int i,int j,range &FitRange)
{
  if (Status!=2 || Std_First==0 || Std_Second==0){
    cout << "Histograms not loaded or Std-Histograms not defined!\n";
    return -1;
  }
  //Allow or not allow time shift
  int lowtimelimit;
  int hightimelimit;
  double timeshiftscale;

  if(floatTime){
    cout << "Allowing fits to float.\n";
    lowtimelimit = -100;
    hightimelimit = 100;
    timeshiftscale = 0.2;
  } else{
    cout << "Fixing leading edge of fits.\n";
    lowtimelimit = 0;
    hightimelimit = 0;
    timeshiftscale = 0.2;
  }

  //Depend on Purpose, choose histogram box
  map <string,HistUnit>* ChosenHistBox;
  if (Purpose==0){
    ChosenHistBox = &SimHistBox;
  }else if(Purpose==1){
    ChosenHistBox = &ExpHistBox;
  }

  TH1* h_Shift = new TH1D("TOF_Shift","TOF Shifted",TOF_N,TOF_Low,TOF_High);
  TH1* TargetHist = ((*ChosenHistBox)["TOFHist"].Hist1DList)[i][j]; //Set the Fit target
  if (TargetHist==NULL){
  	cout << "ERROR: TOFHist["<<i<<"]["<<j<<"] was never initialized. Make sure histogram is enabled. ABORTING FIT!"<<endl;
  	return -1;
  }
  if (FitHistBox.find("TOFHist")==FitHistBox.end()) InitFit("TOFHist"); //Initialize fit result and histograms

  int N_start = TargetHist->FindBin(FitRange.low);
  int N_stop  = TargetHist->FindBin(FitRange.high);
  double f_plus;
  double f_minus;
  double h_val;
  double h_error;

  double numerator = 0;
  double denominator = 0;

  //Calculate Norms
  double Norm = TargetHist->Integral();
  double Norm_std_aPlus = Std_First->Integral();
  double Norm_std_aMinus = Std_Second->Integral();
  double NormRatio_aPlus = Norm/Norm_std_aPlus;
  double NormRatio_aMinus = Norm/Norm_std_aMinus;

  //For time shift 
  double Chi2 = 0;
  //  double Chi2a = 0;
  //  double Chi2p = 0;
  double Chi2Min = 0;
  //  double Chi2aMin = 0;
  int TimeShift = 0;
  double Res[201];
  double ResErr[201];
  int MinShift = lowtimelimit;
  double binRatio=0;
  //  int Min_k = 0;
  //  int k=0;
  //  double a_instant;
  //  double Chi2Array[1001];
  //  double xArray[1001];

  //Calculate a_fit for linear fit
  for (TimeShift=lowtimelimit;TimeShift<=hightimelimit;TimeShift++){
    if (TimeShift<0){
      binRatio = - double(TimeShift)/100.0*timeshiftscale;
      for (int k=1;k<=TOF_N-1;k++){
	h_Shift->SetBinContent(k,TargetHist->GetBinContent(k)*(1-binRatio)+TargetHist->GetBinContent(k+1)*binRatio);
	h_Shift->SetBinError(k,sqrt(h_Shift->GetBinContent(k)));
      }
    }
    if (TimeShift>=0){
      binRatio = double(TimeShift)/100.0*timeshiftscale;
      for (int k=2;k<=TOF_N;k++){
	h_Shift->SetBinContent(k,TargetHist->GetBinContent(k)*(1-binRatio)+TargetHist->GetBinContent(k-1)*binRatio);
	h_Shift->SetBinError(k,sqrt(h_Shift->GetBinContent(k)));
      }
    }
    numerator=0;
    denominator=0;
    for (int j=N_start; j<=N_stop; j++){
      f_plus  = Std_First->GetBinContent(j) * NormRatio_aPlus;
      f_minus = Std_Second->GetBinContent(j) * NormRatio_aMinus;
      //      h_val   = TargetHist->GetBinContent(j);
      //      h_error = TargetHist->GetBinError(j);
      h_val   = h_Shift->GetBinContent(j);
      h_error = h_Shift->GetBinError(j)*(1.0+sqrt(NormRatio_aMinus));//assume that a is near -1/3
      if (h_error==0.0) h_error =1;    

      numerator += (f_plus - f_minus)*(h_val - 0.5*(f_plus+f_minus)) / pow(h_error,2.0);
      denominator += 1.5*pow((f_plus-f_minus)/h_error,2.0);
    }
    Res[TimeShift-lowtimelimit] = numerator / denominator;
    ResErr[TimeShift-lowtimelimit] = sqrt(1.0/denominator/1.5);

    //Calculate Chi2
    Chi2 = 0.0;
    for (int j=N_start; j<=N_stop; j++){
      f_plus  = Std_First->GetBinContent(j) * NormRatio_aPlus;
      f_minus = Std_Second->GetBinContent(j) * NormRatio_aMinus;

      h_error = sqrt(pow(h_Shift->GetBinError(j),2.0)+pow(Std_Second->GetBinError(j),2.0)*NormRatio_aMinus*1.5*(1.0/3.0-Res[TimeShift-lowtimelimit])+pow(Std_First->GetBinError(j),2.0)*NormRatio_aPlus*1.5*(Res[TimeShift-lowtimelimit]+1.0/3.0));
      if (h_error!=0)
	Chi2 += pow((h_Shift->GetBinContent(j) - 1.5*(((1.0/3.0-Res[TimeShift-lowtimelimit])*f_minus+(Res[TimeShift-lowtimelimit]+1.0/3.0)*f_plus)))/h_error,2.0);
      else
	Chi2 += pow((h_Shift->GetBinContent(j) - 1.5*(((1.0/3.0-Res[TimeShift-lowtimelimit])*f_minus+(Res[TimeShift-lowtimelimit]+1.0/3.0)*f_plus))),2.0);
    }
    //    cout << "Chi2 = "<<Chi2<<", TimeShift = "<<double(TimeShift)/100.0*0.2<<endl;
    if (TimeShift==lowtimelimit) Chi2Min = Chi2;
    else{
      if (Chi2<Chi2Min){
	Chi2Min = Chi2;
	MinShift = TimeShift;
      }
    }
  }

  cout << "Chi2 = "<< Chi2Min <<endl;
  cout << "TimeShift = " << MinShift << endl;

  FitResBox["TOFHist"][i][j].fit_val[0] = Res[MinShift-lowtimelimit];
  FitResBox["TOFHist"][i][j].fit_error[0] = ResErr[MinShift-lowtimelimit];
  FitResBox["TOFHist"][i][j].fit_val[1] = double(MinShift)/100.0*timeshiftscale;
  FitResBox["TOFHist"][i][j].fit_error[1] = 0.0;
  FitResBox["TOFHist"][i][j].Chi2 = Chi2Min;

  cout << "Fitting complete!"<<endl;
  cout << "a = " <<FitResBox["TOFHist"][i][j].fit_val[0] << " +/- "<<FitResBox["TOFHist"][i][j].fit_error[0]<<endl;
  AnalysisLog <<"**********Fit Result:**********\n";
  AnalysisLog <<"Histogram Indices: "<<i<<" "<<j<<endl;
  AnalysisLog <<"FitType:" <<FitType<<"; Fit range: ["<<FitRange.low<<","<<FitRange.high<<"]"<<endl;
  AnalysisLog << "a = " <<FitResBox["TOFHist"][i][j].fit_val[0] << " +/- "<<FitResBox["TOFHist"][i][j].fit_error[0]<<endl;
  AnalysisLog << "TimeShift = " << MinShift << endl;
  AnalysisLog << "Chi2 = " << Chi2Min <<endl;

  //Construct histogram
/*  if (TOF_Fit[i] != NULL) {
    delete TOF_Fit[i];
    TOF_Fit[i]=NULL;
  }
  TOF_Fit[i] = new TH1D(Form("TOF_Fit_Cond%d",i),"Fitted Time of Flight",500,0,500);

  if (TOF_Res[i] != NULL) {
    delete TOF_Res[i];
    TOF_Res[i]=NULL;
  }
  TOF_Res[i] = new TH1D(Form("TOF_Res_Cond%d",i),"TOF Residual",500,0,500);
*/

  if (TimeShift<0){
    binRatio = - double(TimeShift)/100.0*timeshiftscale;
    for (int k=1;k<=TOF_N-1;k++){
      h_Shift->SetBinContent(k,TargetHist->GetBinContent(k)*(1-binRatio)+TargetHist->GetBinContent(k+1)*binRatio);
      h_Shift->SetBinError(k,sqrt(h_Shift->GetBinContent(k)));
    }
  }
  if (TimeShift>=0){
    binRatio = double(TimeShift)/100.0*timeshiftscale;
    for (int k=2;k<=TOF_N;k++){
      h_Shift->SetBinContent(k,TargetHist->GetBinContent(k)*(1-binRatio)+TargetHist->GetBinContent(k-1)*binRatio);
      h_Shift->SetBinError(k,sqrt(h_Shift->GetBinContent(k)));
    }
  }

  for (int k=N_start; k<=N_stop; k++){
    f_plus  = Std_First->GetBinContent(k) * NormRatio_aPlus;
    f_minus = Std_Second->GetBinContent(k) * NormRatio_aMinus;
    FitHistBox["TOFHist"].Hist1DList[i][j]->SetBinContent(k,1.5*(((1.0/3.0-FitResBox["TOFHist"][i][j].fit_val[0])*f_minus+(FitResBox["TOFHist"][i][j].fit_val[0]+1.0/3.0)*f_plus)));

    h_error = FitHistBox["TOFHist"].Hist1DList[i][j]->GetBinError(k);
    if (h_error!=0)
      FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->SetBinContent(k,(h_Shift->GetBinContent(k)-FitHistBox["TOFHist"].Hist1DList[i][j]->GetBinContent(k))/h_error);
    else
      FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->SetBinContent(k,(h_Shift->GetBinContent(k)-FitHistBox["TOFHist"].Hist1DList[i][j]->GetBinContent(k)));
  }

  (*ChosenHistBox)["TOFHist"].Hist1DList[i][j]->GetXaxis()->SetRangeUser(FitRange.low,FitRange.high);
  FitHistBox["TOFHist_Residual"].Hist1DList[i][j]->GetXaxis()->SetRangeUser(FitRange.low,FitRange.high);
  delete h_Shift;
  return 0;
}

/***********************************************************************************/
int Analyzer::Fit_TOF_MultiSlice(int i,int j,range &FitRange){

  // *******************************************************************
  // *************** Construct Fit Functions & Wrappers ****************
  // *******************************************************************

  TFile *MultiSliceFile = new TFile("MultiSliceFile.root","RECREATE");

  //Depend on Purpose, choose histogram box
  map <string,HistUnit>* ChosenHistBox;
  if (Purpose==0){
    ChosenHistBox = &SimHistBox;
  }else if(Purpose==1){
    ChosenHistBox = &ExpHistBox;
  }
  for(int k=0;k<nSlice;k++){
    Std_First_Slice[k]->Write(Form("Std_First_Slice%d",k));
    Std_Second_Slice[k]->Write(Form("Std_Second_Slice%d",k));
    (*ChosenHistBox)["TOFHist"].Hist1DListSliced[i][j][k]->Write(Form("TOFHist_Slice%d",k));
  }

  MultiSliceFile->Write();
  MultiSliceFile->Close();
/*
  string root_mac;
  root_mac.resize(300);
  root_mac = ".L ";
  root_mac +=ANALYZER_DIRECTORY;
  root_mac +="/MultiSlice.C++";

  cout << "Process command: "<< root_mac << endl;

  gROOT->ProcessLine(root_mac.c_str());

  
  if(gROOT->ProcessLine(Form("MultiSlice(%d,%d,%d,%d)",i,j,floatTime,nSlice))==0){
    cout << "MultiSlice Fit Succesful" << endl;
  } else{
    cout << "*** WARNING: Abnormal termination of MultiSlice Fit ***" << endl;
  }*/
  MultiSlice(floatTime,nSlice,FitRange.low,FitRange.high);

  TFile *MultiSliceFile_Results = new TFile("MultiSliceFile_Results.root","READ");

  TF1 *fit_return = (TF1*)(MultiSliceFile_Results->Get("MultiSlice_FitFunc0"));

  FitResBox["TOFHist"][i][j].fit_val[0] = fit_return->GetParameter(1);
  FitResBox["TOFHist"][i][j].fit_error[0] = fit_return->GetParError(1);
  FitResBox["TOFHist"][i][j].fit_val[1] = fit_return->GetParameter(2);
  FitResBox["TOFHist"][i][j].fit_error[1] = fit_return->GetParError(2);

  for(int k=0;k<nSlice;k++){
    //Construct histogram
    string newname;
    newname = FitHistBox["TOFHist"].Hist1DListSliced[i][j][k]->GetName();
    delete FitHistBox["TOFHist"].Hist1DListSliced[i][j][k];
    FitHistBox["TOFHist"].Hist1DListSliced[i][j][k]=NULL;

    FitHistBox["TOFHist"].Hist1DListSliced[i][j][k] = (TH1*)((TH1*)(MultiSliceFile_Results->Get(Form("MultiSlice_TOFit%d",k))))->Clone(newname.c_str());

    newname = FitHistBox["TOFHist_Residual"].Hist1DListSliced[i][j][k]->GetName();
    delete FitHistBox["TOFHist_Residual"].Hist1DListSliced[i][j][k];
    FitHistBox["TOFHist_Residual"].Hist1DListSliced[i][j][k]=NULL;
    FitHistBox["TOFHist_Residual"].Hist1DListSliced[i][j][k] = (TH1*)((TH1*)(MultiSliceFile_Results->Get(Form("MultiSlice_TOFRes%d",j))))->Clone(newname.c_str());
  }

  //Getting Chi2
  TH1 * Chi2Hist;
  Chi2Hist = (TH1*)MultiSliceFile_Results->Get("Chi2Hist");
  //Logging
  AnalysisLog <<"**********Fit Result:**********\n";
  AnalysisLog <<"Histogram Indices: "<<i<<" "<<j<<endl;
  AnalysisLog <<"FitType:" <<FitType<<"; Fit range: ["<<FitRange.low<<","<<FitRange.high<<"]"<<endl;
  AnalysisLog << "a = " <<FitResBox["TOFHist"][i][j].fit_val[0] << " +/- "<<FitResBox["TOFHist"][i][j].fit_error[0]<<endl;
  AnalysisLog << "TimeShift = " << FitResBox["TOFHist"][i][j].fit_val[1] << endl;
  AnalysisLog << "Chi2 = " << Chi2Hist->GetBinContent(1)<<"; Dof = "<<Chi2Hist->GetBinContent(2) <<endl;
  return 0;
}

/***********************************************************************************/
//Fit E field and Z position
int Analyzer::FitWithEZ(string StdName,int NE,int NZ,range &FitRange,string label)
{
  //Depend on Purpose, choose histogram box
  map <string,HistUnit>* ChosenHistBox;
  if (Purpose==0){
    ChosenHistBox = &SimHistBox;
  }else if(Purpose==1){
    ChosenHistBox = &ExpHistBox;
  }

  TH1* TargetHist = ((*ChosenHistBox)["TOFHist"].Hist1DList)[0][0]; //Set the Fit target

  if (FitHistBox.find("TOFHist")==FitHistBox.end()) InitFit("TOFHist"); //Initialize fit result and histograms

  return 0;
}

/***********************************************************************************/
//Output Fitted histograms
int Analyzer::OutputFittedHists(string HistName,int iStart,int iEnd,int jStart,int jEnd)
{
  if(Status != 2 && ExpStatus != 2){
    cout <<"Histograms are not initialized,filled or loaded!\n";
    return -1;
  }
  //Test Dimensions
  if (iEnd > HistDim1){
    cout <<"Index i out of range, set to "<<HistDim1<<endl;
    iEnd = HistDim1;
    if (iStart>=iEnd) iStart=iEnd-1;
  }
  if (jEnd > HistDim2){
    cout <<"Index j out of range, set to "<<HistDim2<<endl;
    jEnd = HistDim2;
    if (jStart>=jEnd) jStart=jEnd-1;
  }

  int dim1 = iEnd - iStart + 1;
  int dim2 = jEnd - jStart + 1;
  //Limit to 10 X 5
  if (dim1 > 10){
    dim1 = 10;
    iEnd = iStart + 9;
    cout << "Limit dim1 to 10\n";
  }
  if (dim2 > 10){
    dim2 = 10;
    jEnd = jStart + 9;
    cout << "Limit dim2 to 10\n";
  }
  //Depend on Purpose, choose histogram box
  map <string,HistUnit>* ChosenHistBox;
  if (Purpose==0){
    ChosenHistBox = &SimHistBox;
  }else if(Purpose==1){
    ChosenHistBox = &ExpHistBox;
  }
  //Find histogram first
  if ((*ChosenHistBox).find(HistName)==(*ChosenHistBox).end()){
    cout << "Histogram "<<HistName<<" is not found!\n";
    return -1;
  }
  int TotalHistNum = dim1*dim2;
  int k=0;
  string HistNameResidual = HistName + "_Residual";

  //Styles
  gStyle->SetOptStat("");
  string HistFileName = HISTOGRAM_DIRECTORY + "/Fit_";
  if (Purpose==0){
    HistFileName+="Sim";
  }else if (Purpose==1){
    HistFileName+=ExpFilePrefix;
  }
  HistFileName+=HistName+".root";
  
  TFile* HistFile = new TFile(HistFileName.c_str(),"recreate");
  TCanvas *canv = new TCanvas("canv","Fit Spectra",0,0,800,600);
  if (FitType.compare("MultiSlice")!=0){
    if (TotalHistNum == 1){
      canv->Divide(1,2);
      canv->cd(1);
      (((*ChosenHistBox)[HistName].Hist1DList)[iStart][jStart])->SetLineColor(kBlue);;
      (((*ChosenHistBox)[HistName].Hist1DList)[iStart][jStart])->Draw();
      ((FitHistBox[HistName].Hist1DList)[iStart][jStart])->SetLineColor(kRed);
      ((FitHistBox[HistName].Hist1DList)[iStart][jStart])->Draw("same");
      canv->cd(2);
      ((FitHistBox[HistNameResidual].Hist1DList)[iStart][jStart])->Draw();
      canv->SaveAs(SpectrumFileName.c_str());
      canv->Write();
      (((*ChosenHistBox)[HistName].Hist1DList)[iStart][jStart])->Write();
      ((FitHistBox[HistName].Hist1DList)[iStart][jStart])->Write();
      ((FitHistBox[HistNameResidual].Hist1DList)[iStart][jStart])->Write();
      FitResBox[HistName][iStart][jStart].FitFunc->Write();
      FitResBox[HistName][iStart][jStart].Chi2Func->Write();
    }else{
      canv->Divide(5,4);
      for (int i=1;i<=dim1;i++){
	for (int j=1;j<=dim2;j++){
	  if (k%10<5) canv->cd(k%5+1);
	  else canv->cd(k%5+11);
	  if (FitHistBox[HistName].Dim==1){
	    (((*ChosenHistBox)[HistName].Hist1DList)[iStart+i-1][jStart+j-1])->SetLineColor(kBlue);;
	    (((*ChosenHistBox)[HistName].Hist1DList)[iStart+i-1][jStart+j-1])->Draw();
	    ((FitHistBox[HistName].Hist1DList)[iStart+i-1][jStart+j-1])->SetLineColor(kRed);
	    ((FitHistBox[HistName].Hist1DList)[iStart+i-1][jStart+j-1])->Draw("same");

	    (((*ChosenHistBox)[HistName].Hist1DList)[iStart+i-1][jStart+j-1])->Write();
	    ((FitHistBox[HistName].Hist1DList)[iStart+i-1][jStart+j-1])->Write();
	  }
	  if ((*ChosenHistBox)[HistName].Dim==2){
	    (((*ChosenHistBox)[HistName].Hist2DList)[iStart+i-1][jStart+j-1])->SetLineColor(kBlue);;
	    (((*ChosenHistBox)[HistName].Hist2DList)[iStart+i-1][jStart+j-1])->Draw();
	    ((FitHistBox[HistName].Hist2DList)[iStart+i-1][jStart+j-1])->SetLineColor(kRed);
	    ((FitHistBox[HistName].Hist2DList)[iStart+i-1][jStart+j-1])->Draw("same");

	    (((*ChosenHistBox)[HistName].Hist2DList)[iStart+i-1][jStart+j-1])->Write();
	    ((FitHistBox[HistName].Hist2DList)[iStart+i-1][jStart+j-1])->Write();
	  }
	  if (k%10<5) canv->cd(k%5+6);
	  else canv->cd(k%5+16);
	  if (FitHistBox[HistNameResidual].Dim==1){
	    //	((FitHistBox[HistNameResidual].Hist1DList)[iStart+i-1][jStart+j-1])->SetLineColor(kBlue);
	    ((FitHistBox[HistNameResidual].Hist1DList)[iStart+i-1][jStart+j-1])->Draw();
	    ((FitHistBox[HistNameResidual].Hist1DList)[iStart+i-1][jStart+j-1])->Write();
	  }
	  if ((*ChosenHistBox)[HistNameResidual].Dim==2){
	    //	((FitHistBox[HistName]Residual.Hist2DList)[iStart+i-1][jStart+j-1])->SetLineColor(kBlue);
	    ((FitHistBox[HistNameResidual].Hist2DList)[iStart+i-1][jStart+j-1])->Draw();
	    ((FitHistBox[HistNameResidual].Hist2DList)[iStart+i-1][jStart+j-1])->Write();
	  }
	  if ((k%10 == 9) || (i==dim1 && j==dim2)){
	    canv->SaveAs(SpectrumFileName.c_str());
	    canv->Write();
	  }
	  k++;
	}
      }
    }
  }else{
    for (int i=1;i<=dim1;i++){
      for (int j=1;j<=dim2;j++){
	canv->Divide(5,4);
	for (k=0;k<nSlice;k++){
	  if (k<5) canv->cd(k+1);
	  else  canv->cd(k+6);
	  (*ChosenHistBox)[HistName].Hist1DListSliced[iStart+i-1][jStart+j-1][k]->SetLineColor(kBlue);
	  (*ChosenHistBox)[HistName].Hist1DListSliced[iStart+i-1][jStart+j-1][k]->Draw();
	  FitHistBox[HistName].Hist1DListSliced[iStart+i-1][jStart+j-1][k]->SetLineColor(kRed);
	  FitHistBox[HistName].Hist1DListSliced[iStart+i-1][jStart+j-1][k]->Draw("same");;
	  if (k<5) canv->cd(k+6);
	  else  canv->cd(k+11);
	  ((FitHistBox[HistNameResidual].Hist1DListSliced)[iStart+i-1][jStart+j-1][k])->Draw();
	}
	canv->SaveAs(SpectrumFileName.c_str());
	canv->Write();
      }
    }
  }
  delete canv;
  HistFile->Close();
  delete HistFile;
  return 0;
} 






