//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>
#include <stdint.h>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TF1.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TGaxis.h"
#include "TFrame.h"
#include "TPaveText.h"
#include "TPaletteAxis.h"
#include "GlobalConstants.h"

//Analyzer includes
#include "Analyzer.h"

/***********************************************************************************/
//Function definations

using namespace std;

/***********************************************************************************/
//Systematic studies
/***********************************************************************************/
int Analyzer::SetSysAxis(int Dim,string Property1, string Name1,string Property2,string Name2)
{
  if (Dim==1){
    if(Property1.compare("Condition")==0){
      if(SysWConditions.find(Name1)==SysWConditions.end()){
	cout << "Condition list " << Name1 << " is not found!\n";
	return -1;
      }
      SysDataPtr1 = &(SysWConditions[Name1]);
    }else if (Property1.compare("File")==0){
      if(SysWFiles.find(Name1)==SysWFiles.end()){
	return -1;
      }
      SysDataPtr1 = &(SysWFiles[Name1]);
    }else if (Property1.compare("Parameter")==0){
      if(SysWParameters.find(Name1)==SysWParameters.end()){
	return -1;
      }
      SysDataPtr1 = &(SysWParameters[Name1]);
    }else{
      cout << "Only Condition,Parameter or File!\n";
      cout << "File-Condition list " << Name1 << " is not found!\n";
      return -1;
    }
  }else if (Dim==2){
    if(Property1.compare("Condition")==0){
      if(SysWConditions.find(Name1)==SysWConditions.end()){
	cout << "Condition list " << Name1 << " is not found!\n";
	return -1;
      }
      SysDataPtr1 = &(SysWConditions[Name1]);
    }else if (Property1.compare("File")==0){
      if(SysWFiles.find(Name1)==SysWFiles.end()){
	return -1;
      }
      SysDataPtr1 = &(SysWFiles[Name1]);
    }else if (Property1.compare("Parameter")==0){
      if(SysWParameters.find(Name1)==SysWParameters.end()){
	return -1;
      }
      SysDataPtr1 = &(SysWParameters[Name1]);
    }else{
      cout << "Only Condition,Parameter or File!\n";
      cout << "File-Condition list " << Name1 << " is not found!\n";
      return -1;
    }
    if(Property2.compare("Condition")==0){
      if(SysWConditions.find(Name2)==SysWConditions.end()){
	cout << "Condition list " << Name2 << " is not found!\n";
	return -1;
      }
      SysDataPtr2 = &(SysWConditions[Name2]);
    }else if (Property2.compare("File")==0){
      if(SysWFiles.find(Name2)==SysWFiles.end()){
	return -1;
      }
      SysDataPtr2 = &(SysWFiles[Name2]);
    }else if (Property1.compare("Parameter")==0){
      if(SysWParameters.find(Name2)==SysWParameters.end()){
	return -1;
      }
      SysDataPtr2 = &(SysWParameters[Name2]);
    }else{
      cout << "Only Condition or File!\n";
      cout << "File-Condition list " << Name2 << " is not found!\n";
      return -1;
    }
  }else{
    cout << "Incorrect number of Condition dimension.\n";
    return -1;
  }
  return 0;
}
/***********************************************************************************/
//Initialize graph
int Analyzer::InitGraph(int Dim,string GraphName)
{
  string GraphTitle1, GraphTitle2, GraphTitle3;
  string GraphName_Chi2=GraphName+"_Chi2";
  string GraphName_Res=GraphName+"_Res";
  
  if(Analyzer::Purpose2 == 1) {GraphTitle1 = Form("TOF_{fit}(") + GraphName + ")";}
  else{GraphTitle1 = Form("a_{fit}(") + GraphName + ")";}
  GraphTitle2 = Form("#chi^{2}(") + GraphName + ")";
  GraphTitle3 = "Residuals("+GraphName+")";
  if (Dim==1){
    if(SysGraphBox1D.find(GraphName)!=SysGraphBox1D.end() && SysGraphBox1D[GraphName]!=NULL) delete SysGraphBox1D[GraphName];
    SysGraphBox1D[GraphName] = new TGraphErrors();
    SysGraphBox1D[GraphName]->SetTitle(GraphTitle1.c_str());
    SysGraphBox1D[GraphName]->SetName("SysStudy1D");
    
    if(SysGraphBox1D.find(GraphName_Chi2)!=SysGraphBox1D.end() && SysGraphBox1D[GraphName_Chi2]!=NULL) delete SysGraphBox1D[GraphName_Chi2];
    SysGraphBox1D[GraphName_Chi2] = new TGraphErrors();
    SysGraphBox1D[GraphName_Chi2]->SetTitle(GraphTitle2.c_str());
    SysGraphBox1D[GraphName_Chi2]->SetName("SysStudy1D_Chi2");
    
    if(SysGraphBox1D.find(GraphName_Res)!=SysGraphBox1D.end() && SysGraphBox1D[GraphName_Res]!=NULL) delete SysGraphBox1D[GraphName_Res];
    SysGraphBox1D[GraphName_Res] = new TGraphErrors();
    SysGraphBox1D[GraphName_Res]->SetTitle(GraphTitle3.c_str());
    SysGraphBox1D[GraphName_Res]->SetName("SysStudy1D_Res");
    
  }else if (Dim==2){
    if(SysGraphBox2D.find(GraphName)!=SysGraphBox2D.end() && SysGraphBox2D[GraphName]!=NULL) delete SysGraphBox2D[GraphName];
    SysGraphBox2D[GraphName] = new TGraph2DErrors();
    SysGraphBox2D[GraphName]->SetTitle(GraphTitle1.c_str());
    SysGraphBox2D[GraphName]->SetName("SysStudy2D");
    
    if(SysGraphBox2D.find(GraphName_Chi2)!=SysGraphBox2D.end() && SysGraphBox2D[GraphName_Chi2]!=NULL) delete SysGraphBox2D[GraphName_Chi2];
    SysGraphBox2D[GraphName_Chi2] = new TGraph2DErrors();
    SysGraphBox2D[GraphName_Chi2]->SetTitle(GraphTitle2.c_str());
    SysGraphBox2D[GraphName_Chi2]->SetName("SysStudy2D_Chi2");
  }
  return 0;
}
/***********************************************************************************/
//Systematically fit a or TOF and fill graph
int Analyzer::SystematicFit_TOF(string GraphName, range &FitRange, int Dim, int iLim, int jLim)
{
  string GraphName_Chi2=GraphName+"_Chi2";
  
  if (Dim==1){
    if(SysGraphBox1D.find(GraphName)==SysGraphBox1D.end()){
      cout << "Graph "<<GraphName << " is not initialized!\n";
      return -1;
    }
    if (SysDataPtr1==NULL){
      cout << "Axis is not properly set!\n";
      return -1;
    }
    if(iLim>=HistDim1)iLim=HistDim1;
    for (int i=0;i<iLim;i++){
        
        if(SysDataPtr1 == &(SysWConditions["LeadingEdgeRange_High"]) ){
            LeadingEdgeRange_High = (*SysDataPtr1)[i];
        }
        if(SysDataPtr1 == &(SysWConditions["LeadingEdgeRange_Low"]) ){
            LeadingEdgeRange_Low = (*SysDataPtr1)[i];
        }
        
      Fit_TOF(i,0,FitRange);
      SysGraphBox1D[GraphName]->SetPoint(i,(*SysDataPtr1)[i],FitResBox["TOFHist"][i][0].fit_val[0]);
      SysGraphBox1D[GraphName]->SetPointError(i,0,FitResBox["TOFHist"][i][0].fit_error[0]);
      SysGraphBox1D[GraphName_Chi2]->SetPoint(i,(*SysDataPtr1)[i],FitResBox["TOFHist"][i][0].Chi2);
      SysGraphBox1D[GraphName_Chi2]->SetPointError(i,0,0);
      
    }
  }else if(Dim==2){
    if(SysGraphBox2D.find(GraphName)==SysGraphBox2D.end()){
      cout << "Graph "<<GraphName << " is not initialized!\n";
      return -1;
    }
    if (SysDataPtr1==NULL || SysDataPtr2==NULL){
      cout << "Axes are not properly set!\n";
      return -1;
    }
    if(iLim>=HistDim1)iLim=HistDim1;
    if(jLim>=HistDim2)jLim=HistDim2;
    int k=0;
    for (int i=0;i<iLim;i++){
      for (int j=0;j<jLim;j++){
	cout << "Index: " << i <<" "<<j<<endl;
          
        if(SysDataPtr1 == &(SysWConditions["LeadingEdgeRange_High"]) ){
           LeadingEdgeRange_High = (*SysDataPtr1)[i];
        }
        if(SysDataPtr1 == &(SysWConditions["LeadingEdgeRange_Low"]) ){
            LeadingEdgeRange_Low = (*SysDataPtr1)[i];
        }
        
        if(SysDataPtr2 == &(SysWConditions["LeadingEdgeRange_High"]) ){
            LeadingEdgeRange_High = (*SysDataPtr2)[j];
        }
        if(SysDataPtr2 == &(SysWConditions["LeadingEdgeRange_Low"]) ){
            LeadingEdgeRange_Low = (*SysDataPtr2)[j];
        }
          
        Fit_TOF(i,j,FitRange);
        SysGraphBox2D[GraphName]->SetPoint(k,(*SysDataPtr1)[i],(*SysDataPtr2)[j],FitResBox["TOFHist"][i][j].fit_val[0]);
        SysGraphBox2D[GraphName]->SetPointError(k,0,0,FitResBox["TOFHist"][i][j].fit_error[0]);
        SysGraphBox2D[GraphName_Chi2]->SetPoint(k,(*SysDataPtr1)[i],(*SysDataPtr2)[j],FitResBox["TOFHist"][i][j].Chi2);
        SysGraphBox2D[GraphName_Chi2]->SetPointError(k,0,0,0);
        k++;
      }
    }
  }else{
    cout << "Incorrect dimension!\n";
    return -1;
  }
  return 0;
}
/***********************************************************************************/
//Systematically fill graph with rate based on group time
int Analyzer::SystematicGraphRate(string GraphName, int Dim, int iLim, int jLim)
{
  double totCnts, err, totTime, rate;
  double xdum;
  //Logging
	AnalysisLog << "Systematic Rate Study: "<<endl;  
	AnalysisLog<<"Dim="<<Dim<<endl;
	AnalysisLog<<"Cond1\tCond2\tRate\tErr\n";
  if (Dim==1){
    if(SysGraphBox1D.find(GraphName)==SysGraphBox1D.end()){
      cout << "Graph "<<GraphName << " is not initialized!\n";
      return -1;
    }
    if (SysDataPtr1==NULL){
      cout << "Axis is not properly set!\n";
      return -1;
    }
    if(iLim>=HistDim1)iLim=HistDim1;
    for (int i=0;i<iLim;i++){
    	totCnts = ExpHistBox["GroupTime"].Hist1DList[i][0]->GetEntries();
    	totTime = ExpConditionListBox["GroupTimeHigh"][i]-ExpConditionListBox["GroupTimeLow"][i];
    	rate = totCnts/totTime;
    	err = sqrt(totCnts)/totTime;
      SysGraphBox1D[GraphName]->SetPoint(i,(*SysDataPtr1)[i],rate);
      SysGraphBox1D[GraphName]->SetPointError(i,0,err);
      //Logging
      AnalysisLog<<(*SysDataPtr1)[i]<<"\t"<<0<<"\t"<<rate<<"\t"<<err<<endl;     
    }
  }else if(Dim==2){
    if(SysGraphBox2D.find(GraphName)==SysGraphBox2D.end()){
      cout << "Graph "<<GraphName << " is not initialized!\n";
      return -1;
    }
    if (SysDataPtr1==NULL || SysDataPtr2==NULL){
      cout << "Axes are not properly set!\n";
      return -1;
    }
    if(iLim>=HistDim1)iLim=HistDim1;
    if(jLim>=HistDim2)jLim=HistDim2;
    int k=0;
    for (int i=0;i<iLim;i++){    	
      totTime = ExpConditionListBox["GroupTimeHigh"][i]-ExpConditionListBox["GroupTimeLow"][i];
      for (int j=0;j<jLim;j++){
        cout << "Index: " << i <<" "<<j<<endl;
        totCnts = ExpHistBox["GroupTime"].Hist1DList[i][j]->GetEntries();
        rate = totCnts/totTime;
        err = sqrt(totCnts)/totTime;
        SysGraphBox2D[GraphName]->SetPoint(k,(*SysDataPtr1)[i],(*SysDataPtr2)[j],rate);
        SysGraphBox2D[GraphName]->SetPointError(k,0,0,err);
        k++;
        //Logging
      	AnalysisLog<<(*SysDataPtr1)[i]<<"\t"<<(*SysDataPtr2)[j]<<"\t"<<rate<<"\t"<<err<<endl; 
      }
    }
  }else{
    cout << "Incorrect dimension!\n";
    return -1;
  }

	
  return 0;
}

/***********************************************************************************/
//fit graph of a fits or TOF fits with appropriate function
int Analyzer::SysPlotGraph(int Dim,string GraphName,string Purpose1,string XTitle, string YTitle,string Purpose2,string ZTitle)
{
  string GraphName_Chi2=GraphName+"_Chi2";
  string GraphName_Res=GraphName+"_Res";
  
  double Xbegin,Ybegin,Xend,Yend,Zbegin,Zend,Ymin,Ymax, residual;
  int IndexMax,IndexMin;
  if (Dim==1){
    if (SysGraphBox1D.find(GraphName)==SysGraphBox1D.end()){
      cout << "Graph "<<GraphName << " is not initialized!\n";
      return -1;
    }
    SysGraphBox1D[GraphName]->GetPoint(0,Xbegin,Ybegin);
    int N = SysGraphBox1D[GraphName]->GetN()-1;
    SysGraphBox1D[GraphName]->GetPoint(N,Xend,Yend);
    //Swap Xbegin and Xend if the order is reversed (Xend<Xbegin)
    if (Xend < Xbegin){
      double aux = Xbegin;
      Xbegin = Xend;
      Xend = aux;
    }
    //find Ymax and Ymin
    double dummyX,dummyY;
    SysGraphBox1D[GraphName]->GetPoint(0,dummyX,Ymin);
    Ymax=Ymin;
    IndexMax=IndexMin=0;
    for (int i=1;i<=N;i++){
      SysGraphBox1D[GraphName]->GetPoint(i,dummyX,dummyY);
      if (dummyY>Ymax){
				Ymax=dummyY;
				IndexMax = i;
      }
      if (dummyY<Ymin){
				Ymin=dummyY;
				IndexMin = i;
      }
    }
		TF1 *f;
		if(Analyzer::Purpose2==1){//if for calibration
			f = new TF1("InvSqrRt","[0]/TMath::Sqrt(x)+[1]",Xbegin,Xend);
			f->SetLineStyle(1);
			f->SetLineColor(4);
			f->SetParameters(0.0,(Ybegin+Yend)/2.0);
			//f->FixParameter(1,0); /////////////////Fix T0
			SysGraphBox1D[GraphName]->Fit(f,"RN0B","",Xbegin,Xend);
			alpha = f->GetParameter(0);
			alphaErr = f->GetParError(0);
			t0 = f->GetParameter(1);
			t0Err = f->GetParError(1);
			cout << "alpha = " << alpha <<"+/-"<<alphaErr <<endl;
			cout << "t0 = " << t0<<"+/-"<<t0Err <<endl;
			cout << "Chi2 = " << f->GetChisquare()/f->GetNDF() <<endl;
			//Logging
			AnalysisLog << "Systematic study inverse square root fit: "<<endl;
			AnalysisLog << "alpha = " <<alpha <<"+/-"<<alphaErr  <<endl;
			AnalysisLog << "t0 = " << t0<<"+/-"<<t0Err <<endl;
			AnalysisLog << "Chi2 = " << f->GetChisquare()/f->GetNDF() <<endl;

		}
		else{
			f = new TF1("Linear","[0]*x+[1]",Xbegin,Xend);
			f->SetLineStyle(1);
			f->SetLineColor(4);
			f->SetParameters(0.0,(Ybegin+Yend)/2.0);
			SysGraphBox1D[GraphName]->Fit(f,"RN0","",Xbegin,Xend);
			cout << "da/d(" <<Purpose1 <<") = " << f->GetParameter(0) <<endl;
			//Logging
			AnalysisLog << "Systematic study linear fit: "<<endl;
			AnalysisLog << "da/d(" <<Purpose1 <<") = " << f->GetParameter(0)<<"+/-"<< f->GetParError(0)<<endl;
			AnalysisLog << "a0 = " << f->GetParameter(1)<<"+/-"<< f->GetParError(1)<<endl;
			AnalysisLog << "RChi2 = " << f->GetChisquare()/f->GetNDF() <<endl;
			AnalysisLog << "Prob(Chi2) = "<<f->GetProb()<<endl;//the probability that an observed Chi-squared exceeds the value chi2 by chance, even for a correct model

		}
		
		//Calculate Residuals
		
		for (int i=0; i<=N; i++){
			SysGraphBox1D[GraphName]->GetPoint(i,dummyX,dummyY);
			residual = (dummyY - f->Eval(dummyX));
			//residual = (dummyY - f->Eval(dummyX))/SysGraphBox1D[GraphName]->GetErrorY(i); //Normalized by uncertainty of TOF fit
		  SysGraphBox1D[GraphName_Res]->SetPoint(i,dummyX,residual);
      SysGraphBox1D[GraphName_Res]->SetPointError(i,0,0);
		}
		
    //Graph drawing
    TCanvas *canv = new TCanvas("SysGraph","SysGraph",0,0,800,600);
    if(GridOn) canv->SetGrid();
    canv->cd();
    // draw a frame to define the range
    TH1F *hr = canv->DrawFrame(Xbegin-0.1*(Xend-Xbegin),Ymin-2.0*SysGraphBox1D[GraphName]->GetErrorY(IndexMin),Xend+0.1*(Xend-Xbegin),Ymax+2.0*SysGraphBox1D[GraphName]->GetErrorY(IndexMax));
    if(AxisTitleOn){
      hr->SetXTitle(XTitle.c_str());
      hr->SetYTitle(YTitle.c_str());
    }
    canv->GetFrame()->SetBorderSize(12);

    SysGraphBox1D[GraphName]->SetLineColor(1);
    SysGraphBox1D[GraphName]->SetMarkerColor(2);
    SysGraphBox1D[GraphName]->SetMarkerStyle(21);
    SysGraphBox1D[GraphName]->Draw("P");
    TPaveText* GraphTitle;
    if (TitleOn){
      GraphTitle = new TPaveText(0.1,0.92,0.6,0.98,"NDC");
      GraphTitle->AddText(SysGraphBox1D[GraphName]->GetTitle());
      GraphTitle->Draw();
    }

    f->Draw("same");
    double xmin = canv->GetUxmax();
    double ymin = canv->GetUymin();
    double xmax = canv->GetUxmax();
    double ymax = canv->GetUymax();
    TGaxis * RightAxis = new TGaxis(xmax,ymin,xmax, ymax,ymin*3.0+1,ymax*3.0+1,510,"+L"); 
    RightAxis->SetLineColor(kBlue);
    RightAxis->SetLabelColor(kBlue);
    if(AxisTitleOn){
      if (Analyzer::Purpose2 == 1)RightAxis->SetTitle("#Delta TOF/TOF");
    	else RightAxis->SetTitle("#Delta a/a");
    }
    RightAxis->SetTitleOffset(1.3);
    RightAxis->Draw();
    
    canv->SaveAs(SpectrumFileName.c_str());

    delete f;
    delete canv;
    delete RightAxis;
    if (TitleOn){
      delete GraphTitle;
    }

    //Draw Chi2 graph
    TCanvas *canv2 = new TCanvas("SysGraphCh2","SysGraphChi2",0,0,800,600);
    // draw a frame to define the range
    if (SysGraphBox1D.find(GraphName_Chi2)==SysGraphBox1D.end()){
      cout << "Graph "<<GraphName_Chi2 << " is not initialized!\n";
      return -1;
    }
    SysGraphBox1D[GraphName_Chi2]->SetLineColor(1);
    SysGraphBox1D[GraphName_Chi2]->SetMarkerColor(2);
    SysGraphBox1D[GraphName_Chi2]->SetMarkerStyle(21);
    SysGraphBox1D[GraphName_Chi2]->Draw("APL");
    SysGraphBox1D[GraphName_Chi2]->GetXaxis()->SetTitle(XTitle.c_str());
    SysGraphBox1D[GraphName_Chi2]->GetYaxis()->SetTitle("#chi^{2}");
    if(GridOn) canv2->SetGrid();
    canv2->SaveAs(SpectrumFileName.c_str());
    delete canv2;
    
    //Draw Residual graph
    TCanvas *canv3 = new TCanvas("SysGraphRes","SysGraphRes",0,0,800,600);
    // draw a frame to define the range
    SysGraphBox1D[GraphName_Res]->SetLineColor(1);
    SysGraphBox1D[GraphName_Res]->SetMarkerColor(2);
    SysGraphBox1D[GraphName_Res]->SetMarkerStyle(21);
    SysGraphBox1D[GraphName_Res]->Draw("APL");
    SysGraphBox1D[GraphName_Res]->GetXaxis()->SetTitle(XTitle.c_str());
    SysGraphBox1D[GraphName_Res]->GetYaxis()->SetTitle("Residuals");
    if(GridOn) canv3->SetGrid();
    canv3->SaveAs(SpectrumFileName.c_str());
    delete canv3;

  }else if (Dim==2){
    TCanvas *canv = new TCanvas("SysGraph","SysGraph",0,0,800,600);
    SysGraphBox2D[GraphName]->Draw("COLZ");
		gPad->SetRightMargin(0.15); //make room for legend
		gPad->Update();	//update to get access to color palette/legend
		TPaletteAxis* palette = (TPaletteAxis*)SysGraphBox2D[GraphName]->GetHistogram()->GetListOfFunctions()->FindObject("palette");
		palette->SetX1NDC(0.86); // Start the palette 86 % of the way across the image
   	palette->SetX2NDC(0.91); // End the palette 91% of the way across the image
   	gPad->Modified(); // Update with the new position
    SysGraphBox2D[GraphName]->GetXaxis()->SetTitle(XTitle.c_str());
    SysGraphBox2D[GraphName]->GetYaxis()->SetTitle(YTitle.c_str());
    SysGraphBox2D[GraphName]->GetZaxis()->SetTitle(ZTitle.c_str());
    if(GridOn) canv->SetGrid();
    canv->SaveAs(SpectrumFileName.c_str());
    delete canv;

    //Graph drawing
    TCanvas *canv2 = new TCanvas("SysGraphChi2","SysGraphChi2",0,0,800,600);
    SysGraphBox2D[GraphName_Chi2]->Draw("COLZ");
		gPad->SetRightMargin(0.15); //make room for legend
		gPad->Update();	//update to get access to color palette/legend
		TPaletteAxis* palette2 = (TPaletteAxis*)SysGraphBox2D[GraphName_Chi2]->GetHistogram()->GetListOfFunctions()->FindObject("palette");
		palette2->SetX1NDC(0.86); // Start the palette 86 % of the way across the image
   	palette2->SetX2NDC(0.91); // End the palette 91% of the way across the image
   	gPad->Modified(); // Update with the new position
    SysGraphBox2D[GraphName_Chi2]->GetXaxis()->SetTitle(XTitle.c_str());
    SysGraphBox2D[GraphName_Chi2]->GetYaxis()->SetTitle(YTitle.c_str());
    SysGraphBox2D[GraphName_Chi2]->GetZaxis()->SetTitle("Chi2");
    if(GridOn) canv2->SetGrid();
    canv2->SaveAs(SpectrumFileName.c_str());
    delete canv2;
  }else{
    cout << "Incorrect number of Condition dimension.\n";
    return -1;
  }

  return 0;
}
/***********************************************************************************/

//SystematicStudy_Condition
int Analyzer::SystematicStudy_Condition(int fileID,range &FitRange,int ConDim,string Purpose1,int N1, double low1, double interval1,string XUnit,string Purpose2,int N2,double low2, double interval2, string YUnit)
{
  //Initialization
  string GraphName;
  string XTitle,YTitle,ZTitle;

  //Unset condition loop pointers
  UnSetCondLoopPtrs();
  if (ConDim==1){
    cout << "1D-Condition Systematic Study with "<<Purpose1<<endl;
    InitCondition(Purpose1,N1,low1,interval1);
    AddCondLoopPtr(1,Purpose1);
    SetSysAxis(1,"Condition",Purpose1);
    //Init Graph
    XTitle = Purpose1 + " " + XUnit; 
		YTitle = "a_{fit}";
    GraphName = Purpose1;
    InitGraph(1,GraphName);
  }else if(ConDim==2){
    cout << "2D-Condition Systematic Study with "<<Purpose1<<" and "<<Purpose2<<endl;
    InitCondition(Purpose1,N1,low1,interval1);
    InitCondition(Purpose2,N2,low2,interval2);
    AddCondLoopPtr(1,Purpose1);
    AddCondLoopPtr(2,Purpose2);
    SetSysAxis(2,"Condition",Purpose1,"Condition",Purpose2);
    //Init Graph
    XTitle = Purpose1 + " " + XUnit; 
    YTitle = Purpose2 + " " + YUnit; 
		ZTitle = "a_{fit}";
    GraphName = Purpose1+"_"+Purpose2;
    InitGraph(2,GraphName);
  }else{
    cout << "Incorrect number of Condition dimension.\n";
    return -1;
  }
  SetFileSimDim(1,1);
  FileIDListSim[0][0] = fileID;
  //Process Input
  ProcessInputSim(0,ConDim,0);
  SaveHistograms(GraphName);
  //Fitting
  SystematicFit_TOF(GraphName,FitRange,ConDim,N1,N2);
  //Plot Histograms
  if (SpectrumStatus==0){
    char IDstr[100];
    sprintf(IDstr,"%09d",fileID);
    string SpecName = Purpose1 + "_" + Purpose2 + IDstr;
    OpenSpecFile(SpecName);
    OutputFittedHists("TOFHist",0,N1-1,0,N2-1);
    //Plot graph
    SysPlotGraph(ConDim,GraphName,Purpose1,XTitle,YTitle,Purpose2,ZTitle);
    CloseSpecFile();
  }
  OutputFittedHists("TOFHist",0,N1-1,0,N2-1);
  //Plot graph
  SysPlotGraph(ConDim,GraphName,Purpose1,XTitle,YTitle,Purpose2,ZTitle);
  SaveHistograms(GraphName);
  SaveGraphs(GraphName);
  //Unset condition loop pointers
  UnSetCondLoopPtrs();
  return 0;
}

//SystematicStudy_Parameter
int Analyzer::SystematicStudy_Parameter(int fileID,range &FitRange,int ParDim,string Purpose1,int N1, double low1, double interval1,string XUnit,string Purpose2,int N2,double low2, double interval2, string YUnit)
{
  //Initialization
  string GraphName;
  string XTitle,YTitle,ZTitle;

  //Unset condition loop pointers
  UnsetParameterLoops();
  if (ParDim==1){
    cout << "1D-Parameter Systematic Study with "<<Purpose1<<endl;
    InitParameterArray(Purpose1,N1,low1,interval1);
    SetParameterLoop(Purpose1,1);
    SetSysAxis(1,"Parameter",Purpose1);
    //Init Graph
    XTitle = Purpose1 + " " + XUnit; 
		YTitle = "a_{fit}";
    GraphName = Purpose1;
    InitGraph(1,GraphName);
  }else if(ParDim==2){
    cout << "2D-Parameter Systematic Study with "<<Purpose1<<" and "<<Purpose2<<endl;
    InitParameterArray(Purpose1,N1,low1,interval1);
    InitParameterArray(Purpose2,N2,low2,interval2);
    SetParameterLoop(Purpose1,1);
    SetParameterLoop(Purpose2,2);
    SetSysAxis(2,"Parameter",Purpose1,"Parameter",Purpose2);
    //Init Graph
    XTitle = Purpose1 + " " + XUnit; 
    YTitle = Purpose2 + " " + YUnit; 
		ZTitle = "a_{fit}";
    GraphName = Purpose1+"_"+Purpose2;
    InitGraph(2,GraphName);
  }else{
    cout << "Incorrect number of Parameter dimension.\n";
    return -1;
  }
  SetFileSimDim(1,1);
  FileIDListSim[0][0] = fileID;
  //Process Input
  ProcessInputSim(0,0,ParDim);
  SaveHistograms(GraphName);
  //Fitting
  SystematicFit_TOF(GraphName,FitRange,ParDim,N1,N2);
  //Plot Histograms
  if (SpectrumStatus==0){
    char IDstr[100];
    sprintf(IDstr,"%09d",fileID);
    string SpecName = Purpose1 + "_" + Purpose2 + IDstr;
    OpenSpecFile(SpecName);
    OutputFittedHists("TOFHist",0,N1-1,0,N2-1);
    //Plot graph
    SysPlotGraph(ParDim,GraphName,Purpose1,XTitle,YTitle,Purpose2,ZTitle);
    CloseSpecFile();
  }
  OutputFittedHists("TOFHist",0,N1-1,0,N2-1);
  //Plot graph
  SysPlotGraph(ParDim,GraphName,Purpose1,XTitle,YTitle,Purpose2,ZTitle);
  SaveHistograms(GraphName);
  SaveGraphs(GraphName);
  //Unset condition loop pointers
  UnsetParameterLoops();
  return 0;
}


//SystematicStudy_File
int Analyzer::SystematicStudy_Calibration(range &FitRange,double low1, double interval1, double E0, double E0err, string Purpose1)
{
  //Initialization
  string GraphName;
  string XTitle,YTitle,ZTitle;
  int nFile1,nFile2; 
  double z_fit;
  double mass = MASS_HE4;
  int charge_state = 1;
  double Q = (1e-6)*charge_state*CSPEED*CSPEED/mass; 			//q/m [mm/ns]^2/V
  E0 = E0*100; //convert to V/mm
  char IDstr[100];
  int returnVal;
  
  if(Analyzer::Purpose2 !=1){
  	cout<<"Cannot perform systematic calibration study. Purpose2 mismatch!"<<endl;
  	return -1;
  }
  


	if (Purpose == 0){
		if (Status == 2){
			cout<<"Histograms loaded."<<endl;
			if(HistDim2!=1){
				cout << "Error: There is more than one file dimension in the loaded histograms."<<endl;
				return -1;
			}
			SetFileSimDim(HistDim1,HistDim2);
		}
		else {
			cout << "FileID list should be initialized beforehand. Check whether you have done it.\n";
		}
		nFile1 = nFileSim1;
		nFile2 = nFileSim2;
		sprintf(IDstr,"%09d",FileIDListSim[0][0]); 
	  cout << "1D-File Systematic Study with Field Scaling for simulation."<<endl;
	}
	else if (Purpose == 1){
		if (ExpStatus == 2){
			cout<<"Histograms loaded."<<endl;
			if(HistDim2!=1){
				cout << "Error: There is more than one file dimension in the loaded histograms."<<endl;
				return -1;
			}
			SetFileExpDim(HistDim1,HistDim2);
		}
		else {
			cout << "FileID list should be initialized beforehand. Check whether you have done it.\n";
		}
		nFile1 = nFileExp1;
		nFile2 = nFileExp2;
		sprintf(IDstr,"%09d",FileIDListExp[0][0]); 
	  cout << "1D-File Systematic Study with Field Scaling for experiment."<<endl;		
	}
	else {
	 	cout << "Purpose " <<Purpose << " undefined!" <<endl;
  	return -1;
	}
	
	Purpose1 = "Field_Scaling_"+Purpose1;
  InitSysWFile(Purpose1,nFile1,low1,interval1);
  SetSysAxis(1,"File",Purpose1);

  GraphName = Purpose1; //!!!!!!!!!!!!!!!!!!!!!!!!!!



	if (Status !=2 || ExpStatus !=2){
	  //Process Input
	  if (Purpose == 0) returnVal = ProcessInputSim(1,0,0);
	  else if (Purpose == 1) returnVal = ProcessInputExp(1,0,0);
	  else {
	  	cout << "Purpose " <<Purpose << " undefined!" <<endl;
	  	returnVal = -1;
	  }
	  if(returnVal !=0) return -1;
	  SaveHistograms(GraphName);
	}
	//Init Graph

	XTitle = "Field_Scaling";
	YTitle = "TOF_{fit}";
	
	InitGraph(1,GraphName);
	
  //Fitting
	if(SystematicFit_TOF(GraphName,FitRange,1,nFile1,nFile2)!=0) return -1;

	//Plot Histograms

	string SpecName = Purpose1 + "_"  + IDstr;
	if (SpectrumStatus==0){
	  OpenSpecFile(SpecName);
	  OutputFittedHists("TOFHist",0,nFile1-1,0,nFile2-1);
	  //Plot graph
	  SysPlotGraph(1,GraphName,Purpose1,XTitle,YTitle,"",ZTitle);
	  CloseSpecFile();
	}
	OutputFittedHists("TOFHist",0,nFile1-1,0,nFile2-1);

  //Plot graph
  SysPlotGraph(1,GraphName,Purpose1,XTitle,YTitle,"",ZTitle);
  //GraphName+="PostFit";
  SaveHistograms(GraphName);
  SaveGraphs(GraphName);
  double z0_fit = .5*Q*E0*alpha*alpha;
  double z0_err = sqrt(pow(alphaErr*alpha*E0*Q,2) + pow(z0_fit*E0err,2));
  cout<<"Determined MCP-MOT distance = "<< z0_fit<<"+/-"<< z0_err<<" [mm]"<<endl;
  AnalysisLog << "z0_Fit = " << z0_fit<<"+/-"<< z0_err<<" [mm]"<<endl;
  AnalysisLog << "E0 = " << E0<<"+/-"<< E0err*E0<<" [V/mm]"<<endl; 

  return 0;
}
/***********************************************************************************/
int Analyzer::SystematicStudy_Rate(int ConDim, int N_GT, double GT_inc, double GT_lo, double GT_hi,string Purpose1,double low1, double interval1,string XUnit,string Purpose2,int N2,double low2, double interval2, string YUnit)
{
  //Initialization
  string GraphName;
  string XTitle,YTitle,ZTitle;
  int N1 = N_GT;
  
  //Unset condition loop pointers
  UnSetCondLoopPtrs();  
  
  if (ExpStatus == 2){
    cout<<"Expirement Histograms loaded."<<endl;
    if(ConDim==1 && HistDim2!=1){
      cout << "Error: There is more than one file dimension in the loaded histograms."<<endl;
      cout << "HistDim1 = "<<HistDim1<<"\t HistDim2 = "<<HistDim2<<endl;
      return -1;
    }
    else if (ConDim==2 && HistDim2==1){
      cout<< "Error: There is only one file dimension in the loaded histograms." <<endl;
      return -1;
    }
    SetFileExpDim(HistDim1,HistDim2); //include for simulation with multiple files
  }
  else {
    cout << "FileID list should be initialized beforehand. Check whether you have done it.\n";}  
  
  //Initialize GroupTime conditions and add to conditional loop pointer 1.
	InitExpCondition("GroupTimeLow", N_GT, GT_lo, GT_inc);
	InitExpCondition("GroupTimeHigh", N_GT, GT_hi, GT_inc);
	AddExpCondLoopPtr(1,"GroupTimeLow");
	AddExpCondLoopPtr(1,"GroupTimeHigh");


//include for simulation with multiple files????
//  if (nFileSim2==1 && FDim!=1){
//    cout << "Warning: there is actually only 1 file dimension. Set FDim to 1."<<endl;
//    FDim=1;
//  }
  


  if (ConDim==1){
    cout << "1D-Rate Systematic Study with "<<Purpose1<<endl;
    //Initialize external label and set the axes for the systematic graph  
    InitSysWFile(Purpose1,N1,low1,interval1);
    SetSysAxis(1,"File",Purpose1);
    //Init Graph
    XTitle = Purpose1 + " " + XUnit; 
		YTitle = "Rate [Hz]";
    GraphName = Purpose1;
    InitGraph(1,GraphName);
  }else if(ConDim==2){
    cout << "2D Rate vs Condition Systematic Study with "<<Purpose1<<" and "<<Purpose2<<endl;
    //Initialize Purpose2 condition
    InitExpCondition(Purpose2,N2,low2,interval2);
    AddExpCondLoopPtr(2,Purpose2);
    //Set Purpose1 label and Purpose2 condition as the axes for the systematic graph 
    SetSysAxis(2,"File",Purpose1,"Condition",Purpose2);
    //Init Graph
    XTitle = Purpose1 + " " + XUnit; 
    YTitle = Purpose2 + " " + YUnit; 
		ZTitle = "Rate [Hz]";
    GraphName = Purpose1+"_"+Purpose2;
    InitGraph(2,GraphName);
  }else{
    cout << "Incorrect number of Condition dimension.\n";
    return -1;
  }
  //SetFileSimDim(1,1); 

  
  if (ExpStatus !=2){
    //Process Input
    ProcessInputExp(0,ConDim,0);
    SaveHistograms(GraphName);
  }
  else LogConditionList();
  
  //Populate Graph with Rates
  SystematicGraphRate(GraphName, ConDim, N1, N2);
  //Plot Histograms
  if (SpectrumStatus==0){ //open spectrum file if not open and output graph
    char IDstr[100];
    sprintf(IDstr,"%09d",FileIDListSim[0][0]);
    string SpecName = Purpose1 + "_" + Purpose2 + IDstr;
    OpenSpecFile(SpecName);
    //Plot graph
    SysPlotGraph(ConDim,GraphName,Purpose1,XTitle,YTitle,Purpose2,ZTitle);
    CloseSpecFile();
  }else{
  //Plot graph
  SysPlotGraph(ConDim,GraphName,Purpose1,XTitle,YTitle,Purpose2,ZTitle);
  }
  SaveHistograms(GraphName);
  //SaveGraphs(GraphName);
  //Unset condition loop pointers
  UnSetCondLoopPtrs();
  return 0;
}

/***********************************************************************************/
//Systematic Filelist initialization
int Analyzer::SysFileListInit(int Dim, int BaseID1, int BaseID2, int BaseID3, int BaseID4, int dim1,string Inc1,int dim2,string Inc2)
{
  if (Dim==1){
    SetFileSimDim(dim1,1);
    SetFileIDListSim(0,BaseID1,BaseID2,BaseID3,BaseID4,Inc1);
  }else if(Dim==2){
    SetFileSimDim(dim1,dim2);
    SetFileIDListSim(0,BaseID1,BaseID2,BaseID3,BaseID4,Inc1);
    SetFileIDListSim2D(Inc2);
  }else{
    cout << "Incorrect number of Condition dimension.\n";
    return -1;
  }
  return 0;
}
/***********************************************************************************/
//SystematicStudy_File
int Analyzer::SystematicStudy_File(int FDim,range &FitRange,string Purpose1,double low1, double interval1,string XUnit,string Purpose2,double low2, double interval2, string YUnit)
{
  //Initialization
  string GraphName;
  string XTitle,YTitle,ZTitle;
  if (Status == 2){
    cout<<"Histograms loaded."<<endl;
    if(FDim==1 && HistDim2!=1){
      cout << "Error: There is more than one file dimension in the loaded histograms."<<endl;
      cout << "HistDim1 = "<<HistDim1<<"\t HistDim2 = "<<HistDim2<<endl;
      return -1;
    }
    else if (FDim==2 && HistDim2==1){
      cout<< "Error: There is only one file dimension in the loaded histograms." <<endl;
      return -1;
    }
    SetFileSimDim(HistDim1,HistDim2);
  }
  else {
    cout << "FileID list should be initialized beforehand. Check whether you have done it.\n";
  }


  if (nFileSim2==1 && FDim!=1){
    cout << "Warning: there is actually only 1 file dimension. Set FDim to 1."<<endl;
    FDim=1;
  }


  if (FDim==1){
    cout << "1D-File Systematic Study with "<<Purpose1<<endl;
    InitSysWFile(Purpose1,nFileSim1,low1,interval1);
    SetSysAxis(1,"File",Purpose1);
    //Init Graph
    XTitle = Purpose1 + " " + XUnit; 
    YTitle = "a_{fit}";
    GraphName = Purpose1;
    InitGraph(1,GraphName);
  }else if(FDim==2){
    cout << "2D-File Systematic Study with "<<Purpose1<<" and "<<Purpose2<<endl;
    InitSysWFile(Purpose1,nFileSim1,low1,interval1);
    InitSysWFile(Purpose2,nFileSim2,low2,interval2);
    SetSysAxis(2,"File",Purpose1,"File",Purpose2);
    //Init Graph
    XTitle = Purpose1 + " " + XUnit; 
    YTitle = Purpose2 + " " + YUnit; 
    ZTitle = "a_{fit}";
    GraphName = Purpose1+"_"+Purpose2;
    InitGraph(2,GraphName);
  }else{
    cout << "Incorrect number of Condition dimension.\n";
    return -1;
  }

  if (Status !=2){
    //Process Input
    ProcessInputSim(FDim,0,0);
    SaveHistograms(GraphName);
  }
  else LogConditionList();
  //Fitting
  SystematicFit_TOF(GraphName,FitRange,FDim,nFileSim1,nFileSim2);


  //Plot Histograms

  char IDstr[100];
  sprintf(IDstr,"%09d",FileIDListSim[0][0]);
  string SpecName = Purpose1 + "_" + Purpose2 + IDstr;
  if (SpectrumStatus==0){
    OpenSpecFile(SpecName);
    OutputFittedHists("TOFHist",0,nFileSim1-1,0,nFileSim2-1);
    //Plot graph
    SysPlotGraph(FDim,GraphName,Purpose1,XTitle,YTitle,Purpose2,ZTitle);
    CloseSpecFile();
  }
  OutputFittedHists("TOFHist",0,nFileSim1-1,0,nFileSim2-1);
  //Plot graph
  SysPlotGraph(FDim,GraphName,Purpose1,XTitle,YTitle,Purpose2,ZTitle);
  //GraphName+="PostFit";
  SaveHistograms(GraphName);
  SaveGraphs(GraphName);

  return 0;
}
/***********************************************************************************/
//SystematicStudy_Condition

int Analyzer::SystematicStudy_Mixed(range &FitRange,string PurposeCon,int N1, double low1, double interval1,string XUnit,string PurposeFile,double low2, double interval2, string YUnit)
{
  cout << "FileID list should be initialized beforehand. Check whether you have done it.\n";
  string GraphName;
  string XTitle,YTitle,ZTitle;
  //Initialization
  InitCondition(PurposeCon,N1,low1,interval1);
  AddCondLoopPtr(1,PurposeCon);
  cout << "2D-Mixed Systematic Study with "<<PurposeCon<<" and "<<PurposeFile<<endl;
  InitSysWFile(PurposeFile,nFileSim1,low2,interval2);
  SetSysAxis(2,"File",PurposeFile,"Condition",PurposeCon);
  //Init Graph
  XTitle = PurposeFile + " " + XUnit; 
  YTitle = PurposeCon + " " + YUnit; 
	ZTitle = "a_{fit}";
  GraphName = PurposeFile+"_"+PurposeCon;
  InitGraph(2,GraphName);


  //Process Input
  ProcessInputSim(1,1,0);
 	cout<< "made it out of the ProcessInputSim()"<<endl;
  SaveHistograms(GraphName);
  	cout<< "made it out of the SaveHistorgrams()"<<endl;
  //Fitting
  SystematicFit_TOF(GraphName,FitRange,2,nFileSim1,nCond1);
  //Plot Histograms
  char IDstr[100];
  sprintf(IDstr,"%09d",FileIDListSim[0][0]);
  string SpecName = PurposeFile + "_" + PurposeCon + IDstr;
  if (SpectrumStatus==0){
    OpenSpecFile(SpecName);
    OutputFittedHists("TOFHist",0,nFileSim1-1,0,nCond1-1);
    //Plot graph
    SysPlotGraph(2,GraphName,PurposeCon,XTitle,YTitle,PurposeFile,ZTitle);
    CloseSpecFile();
  }
  OutputFittedHists("TOFHist",0,nFileSim1-1,0,nCond1-1);
  //Plot graph
  SysPlotGraph(2,GraphName,PurposeCon,XTitle,YTitle,PurposeFile,ZTitle);
  SaveHistograms(GraphName);
  SaveGraphs(GraphName);
  return 0;
}

/***********************************************************************************/
/*****END*****/
