#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string>
#include <sstream> 
#include <vector>
#include <stdint.h>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TF2.h"
//Analyzer includes
#include "Analyzer.h"
#include "Calibration.h"

using namespace std;

/***********************************************************************************/
int Analyzer::LoadExpCalibrationData(string name,bool Switch){
  //Load Amplifier calibration data
  ifstream calfilein;
  char filename[500];
  if (name.compare("Amp")==0){
    if (Switch){
      sprintf(filename,"%s/AmpliferCalibration%02d.txt",CALIBRATION_DIRECTORY.c_str(),ExpCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
      	cout<<"Error: Cannot open file "<< filename<<endl;
      	return -1;
      }
      for (int i=0;i<6;i++){
	calfilein>>AmpCalX[i];
	//    cout << AmpCalX[i]<<endl;
      }
      for (int i=0;i<6;i++){
	calfilein>>AmpCalY[i];
	//    cout << AmpCalY[i]<<endl;
      }
      calfilein>>AmpCalA[0]>>AmpCalA[1];
      calfilein.close();
    }else{
      for (int i=0;i<6;i++){
	AmpCalX[i]=AmpCalY[i]=1.0;
	//    cout << AmpCalX[i]<<endl;
      }
      AmpCalA[0]=AmpCalA[1]=1.0;
    }
  }
  //Load Scintillator calibration data
  else if(name.compare("Scint")==0){
    if (Switch){
      sprintf(filename,"%s/ScintCalibration%02d.txt",CALIBRATION_DIRECTORY.c_str(),ExpCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
	cout<<"Error: Cannot open file "<< filename<<endl;
	return -1;
      }
      calfilein>>LED_Cal>>LEDScint_Cal;
      double aux;
      for (int i=0;i<3;i++){
	calfilein>>ExpCalPeaks[i*2]>>ExpCalPeaks[i*2+1]>>aux>>aux;
      }
      calfilein.close();
      if (SimScintCalLoaded){
	TGraphErrors *CalGraph = new TGraphErrors();
	CalGraph->SetName("CalGraph");
	CalGraph->SetTitle("CalGraph");
	for (int i=0;i<3;i++){
	  CalGraph->SetPoint(i,ExpCalPeaks[i*2],SimCalPeaks[i*2]);
	  CalGraph->SetPointError(i,ExpCalPeaks[i*2+1],SimCalPeaks[i*2+1]);
	}
	CalGraph->Fit("pol1");
	ScintCalShift = CalGraph->GetFunction("pol1")->GetParameter(0);
	ScintCalScale = CalGraph->GetFunction("pol1")->GetParameter(1);
        delete CalGraph;  
    }else{
	cout <<"WARNING: Simulation Scint Calibration is not loaded yet. Please load it first and then get the exp-sim correction term."<<endl;
      }
    }else{
      ScintCalScale=1.0;
      ScintCalShift=0.0;
      LED_Cal=121283;
      LEDScint_Cal=7641.64;
      for (int i=0;i<6;i++){
	ExpCalPeaks[i]=1.0;
      }
    }
  }
  else if (name.compare("ScintGainMap")==0){
    if (Switch){
      //Load Scint Gain Map calibration data
      sprintf(filename,"%s/ScintGainMap%02d.txt",CALIBRATION_DIRECTORY.c_str(),ExpCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
	cout<<"Error: Cannot open file "<< filename<<endl;
	return -1;
      }
      double dummyX;
      double dummyY;
      for (int i=0;i<15;i++){
	for (int j=0;j<16;j++){
	  calfilein>>dummyX>>dummyY>>ScintGainMap[i][j];
	}
      }
      calfilein.close();
    }else{
      for (int i=0;i<15;i++){
	for (int j=0;j<16;j++){
	  ScintGainMap[i][j]=1.0;
	}
      }
    }
  }
  //Load MWPC calibration data
  else if (name.compare("MWPCE")==0){
    if (Switch){
      //Load MWPC Energy calibration data
      sprintf(filename,"%s/MWPCECalibration%02d.txt",CALIBRATION_DIRECTORY.c_str(),ExpCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
	cout<<"Error: Cannot open file "<< filename<<endl;
	return -1;
      }
      calfilein>>MWPCAnode_Cal>>MWPCCathode_Cal;
      calfilein>>MWPCEScale>>MWPCEShift;
      calfilein.close();
    }else{
      MWPCAnode_Cal=MWPCCathode_Cal=1.0;
      MWPCEScale = 1.0;
      MWPCEShift = 0.0;
    }
  }
  else if (name.compare("MWPCGainMap")==0){
    if (Switch){
      //Load MWPC Gain Map calibration data
      sprintf(filename,"%s/MWPCGainMap%02d.txt",CALIBRATION_DIRECTORY.c_str(),ExpCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
	cout<<"Error: Cannot open file "<< filename<<endl;
	return -1;
      }
      double dummyX;
      double dummyY;
      for (int i=0;i<17;i++){
	for (int j=0;j<18;j++){
	  calfilein>>dummyX>>dummyY>>MWPCGainMap[i][j];
	}
      }
      calfilein.close();
    }else{
      for (int i=0;i<17;i++){
	for (int j=0;j<18;j++){
	  MWPCGainMap[i][j]=1.0;
	}
      }
    }
  }else if (name.compare("MWPCEffMap")==0){
    if (Switch){
      //Load MWPC Efficiency Map calibration data
      sprintf(filename,"%s/MWPCEffMap%02d.txt",CALIBRATION_DIRECTORY.c_str(),ExpCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
	cout<<"Error: Cannot open file "<< filename<<endl;
	return -1;
      }
      double dummyX;
      double dummyY;
      for (int i=0;i<15;i++){
	for (int j=0;j<16;j++){
	  calfilein>>MWPCEffMap[i][j];
	}
      }
      calfilein.close();
    }else{
      for (int i=0;i<15;i++){
	for (int j=0;j<16;j++){
	  MWPCEffMap[i][j]=1.0;
	}
      }
    }
  }else if (name.compare("MWPCPos")==0){
    cout <<"MWPCPos Calibration\n";	

    if (Switch){
      //Load MWPC Position calibration data
      TGraph* CalCathodeXGraph0;
      TGraph* CalCathodeXGraph1[3];
      TGraph* CalCathodeXGraph2[4];
      CalCathodeXGraph0 = new TGraph();
      CalCathodeXGraph0->SetName("CalCathodeXGraph0");
      for (int i=0;i<3;i++){
	CalCathodeXGraph1[i] = new TGraph();
	CalCathodeXGraph1[i]->SetName(Form("CalCathodeXGraph1_%d",i));
      }
      for (int i=0;i<4;i++){
	CalCathodeXGraph2[i] = new TGraph();
	CalCathodeXGraph2[i]->SetName(Form("CalCathodeXGraph2_%d",i));
      }
      TF1* fitFuncX0;
      TF1* fitFuncX1[3];
      TF1* fitFuncX2[4];

      TGraph* CalCathodeYGraph = new TGraph();
      TGraph* CalAnodeGraph = new TGraph();
      CalCathodeYGraph->SetName("CalCathodeYGraph");
      CalAnodeGraph->SetName("CalAnodeGraph");

      //      double InData = 0.0;
      sprintf(filename,"%s/MWPCPosCalibration%02d.txt",CALIBRATION_DIRECTORY.c_str(),ExpCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
	cout<<"Error: Cannot open file "<< filename<<endl;
	return -1;
      }else cout <<	"MWPCPos Calibration file open success!\n";	

      //Calibrate X
      for (int i=0;i<9;i++){
	calfilein>>MWPCExactPos0[i]>>MWPCPeaksX0[i];
	CalCathodeXGraph0->SetPoint(i,MWPCPeaksX0[i],-8.0+2.0*i);
      }
      int k=0;
      for (int j=0;j<3;j++){
	for (int i=0;i<5;i++){
	  calfilein>>MWPCExactPos1[k]>>MWPCPeaksX1[k];
	  CalCathodeXGraph1[j]->SetPoint(i,MWPCPeaksX1[k]+8.0*(1-j),-4.0+2.0*i);
	  k++;
	}
      }
      k=0;
      for (int j=0;j<4;j++){
	for (int i=0;i<5;i++){
	  calfilein>>MWPCExactPos2[k]>>MWPCPeaksX2[k];
	  CalCathodeXGraph2[j]->SetPoint(i,MWPCPeaksX2[k]+8.0*(1-j)+4.0,-4.0+2.0*i);
	  k++;
	}
      }

      //Fit to pol[3]
      CalCathodeXGraph0->Fit("pol3");
      fitFuncX0 = CalCathodeXGraph0->GetFunction("pol3");
      for (int i=0;i<4;i++){
	//	MWPCXCalPar[i] = fitFuncX0->GetParameter(i);
	MWPCCalPar.FitParametersX0[i] = fitFuncX0->GetParameter(i);
      }
      for (int i=0;i<3;i++){
	CalCathodeXGraph1[i]->Fit("pol3");
	fitFuncX1[i] = CalCathodeXGraph1[i]->GetFunction("pol3");
	for (int j=0;j<4;j++){
	  //	  MWPCXCalPar[i*4+j+4] = fitFuncX1[i]->GetParameter(j);
	  MWPCCalPar.FitParametersX1[i][j] = fitFuncX1[i]->GetParameter(j);
	}
      }
      for (int i=0;i<4;i++){
	CalCathodeXGraph2[i]->Fit("pol3");
	fitFuncX2[i] = CalCathodeXGraph2[i]->GetFunction("pol3");
	for (int j=0;j<4;j++){
	  //	  MWPCXCalPar[i*4+j+16] = fitFuncX2[i]->GetParameter(j);
	  MWPCCalPar.FitParametersX2[i][j] = fitFuncX2[i]->GetParameter(j);
	}
      }

      //Calibrate Y
      //For this moment, no calibration of cathode Y
      /*      for (int i=0;i<18;i++){
	      calfilein>>InData;
	      CalCathodeYGraph->SetPoint(i,InData,-17.0+i*2.0);
	      MWPCPeaksA[i]=InData;
      //	cout << InData<< endl;
      }
      CalCathodeYGraph->Fit("pol3");
      TF1 * fitFuncY = CalCathodeYGraph->GetFunction("pol3");
      for (int i=0;i<4;i++){
      MWPCYCalPar[i] = fitFuncY->GetParameter(i);
      }*/

      //Calibrate A 
      for (int i=0;i<18;i++){
	calfilein>>MWPCExactPosA[i]>>MWPCPeaksA[i];
	CalAnodeGraph->SetPoint(i,MWPCPeaksA[i],-17.0+i*2.0);
      }
      CalAnodeGraph->Fit("pol3");
      TF1 * fitFuncA = CalAnodeGraph->GetFunction("pol3");
      for (int i=0;i<4;i++){
	//	MWPCACalPar[i] = fitFuncA->GetParameter(i);
	MWPCCalPar.FitParametersA[i] = fitFuncA->GetParameter(i);
      }

      calfilein.close();
      /*
	 TFile * CalFileGraph = new TFile("Graphs.root","recreate");
	 CalAnodeGraph->Write();
	 CalCathodeXGraph->Write();
	 CalCathodeYGraph->Write();
	 CalFileGraph->Close();
	 delete CalFileGraph;
       */

      delete CalCathodeXGraph0;
      for (int i=0;i<3;i++) delete CalCathodeXGraph1[i];
      for (int i=0;i<4;i++) delete CalCathodeXGraph2[i];
      delete CalCathodeYGraph;
      delete CalAnodeGraph;
    }else{ //if Switch = false
      for (int i=0;i<4;i++){
	if (i==1) MWPCCalPar.FitParametersX0[i] = 1.0;
	else MWPCCalPar.FitParametersX0[i] = 0.0;
      }
      for (int i=0;i<3;i++){
	for (int j=0;j<4;j++){
	  if (j==1) MWPCCalPar.FitParametersX1[i][j] = 1.0;
	  else MWPCCalPar.FitParametersX1[i][j] = 0.0;
	}
      }
      for (int i=0;i<4;i++){
	for (int j=0;j<4;j++){
	  if (j==1) MWPCCalPar.FitParametersX2[i][j] = 1.0;
	  else MWPCCalPar.FitParametersX2[i][j] = 0.0;
	}
      }
      for (int i=0;i<4;i++){
	if (i==1) MWPCCalPar.FitParametersA[i] = 1.0;
	else  MWPCCalPar.FitParametersA[i] = 0.0;
      }

      for (int i=0;i<9;i++){
	MWPCExactPos0[i] = MWPCPeaksX0[i] = -8.0+2.0*i;
      }
      for (int i=0;i<15;i++){
	if (i<5) MWPCExactPos1[i] = MWPCPeaksX1[i] = -12.0+2.0*i;
	if (i>=5 && i<10) MWPCExactPos1[i] = MWPCPeaksX1[i] = -12.0+2.0*i-2.0;
	if (i>=10 && i<15) MWPCExactPos1[i] = MWPCPeaksX1[i] = -12.0+2.0*i-4.0;
      }
      for (int i=0;i<20;i++){
	if (i<5) MWPCExactPos2[i] = MWPCPeaksX2[i] = -16.0+2.0*i;
	if (i>=5 && i<10) MWPCExactPos2[i] = MWPCPeaksX2[i] = -12.0+2.0*i-2.0;
	if (i>=10 && i<15) MWPCExactPos2[i] = MWPCPeaksX2[i] = -12.0+2.0*i-4.0;
	if (i>=15 && i<20) MWPCExactPos2[i] = MWPCPeaksX2[i] = -12.0+2.0*i-6.0;
      }
      for (int i=0;i<18;i++){ 
	MWPCExactPosA[i] = MWPCPeaksA[i] = -17.0+2.0*i;
      }
    }
  }
  //Load MCP calibration data
  else if(name.compare("MCPPos")==0){
    if (Switch){
      if (MCPPosCalMode.compare("Global")==0){
	TGraph2D* XGraph2D = new TGraph2D();
	TGraph2D* YGraph2D = new TGraph2D();
	sprintf(filename,"%s/MCPPosGlobalCalibration%02d.txt",CALIBRATION_DIRECTORY.c_str(),ExpCalIDs[name]);
	calfilein.open(filename,ios::in);
	if (!calfilein.is_open()){
	  cout<<"Error: Cannot open file "<< filename<<endl;
	  return -1;
	}
	calfilein>>MCPShiftX>>MCPShiftY>>MCPScaleX>>MCPScaleY;
	int i,j,status;
	double X,Y;
	int NGrid=0;
	while (1){
	  calfilein>>i>>j>>status>>X>>Y;
	  if (calfilein.eof())break;
	  if (status==1){
	    XGraph2D->SetPoint(NGrid,X,Y,-34.0+4.0*i);
	    YGraph2D->SetPoint(NGrid,X,Y,-34.0+4.0*j);
	    NGrid++;
	  }
	}
	calfilein.close();
	//Define 2D quadratic functions
	TF2* XFitFunc = new TF2("XFitFunc","[0]*x*x+[1]*x*y+[2]*y*y+[3]*x+[4]*y+[5]",-40,40,-40,40);
	XFitFunc->SetParameters(0,0,0,1,0,0);
	TF2* YFitFunc = new TF2("YFitFunc","[0]*x*x+[1]*x*y+[2]*y*y+[3]*x+[4]*y+[5]",-40,40,-40,40);
	YFitFunc->SetParameters(0,0,0,0,1,0);

	XGraph2D->Fit(XFitFunc);
	YGraph2D->Fit(YFitFunc);

	for (int i=0;i<6;i++){
	  XFitPar[i] = XFitFunc->GetParameter(i);
	  YFitPar[i] = YFitFunc->GetParameter(i);
	}
	delete XGraph2D;
	delete YGraph2D;
	delete XFitFunc;
	delete YFitFunc;
	//clear the local calibration parameters
	MCPSquareArray=NULL;
	MCPNPointEdge=0;
	MCPsizeX=0;
	MCPsizeY=0;
      }else if (MCPPosCalMode.compare("Local")==0){
	//Loading calibration file
	sprintf(filename,"%s/MCPPosLocalCalibration%02d.txt",CALIBRATION_DIRECTORY.c_str(),ExpCalIDs[name]);
	calfilein.open(filename,ios::in);
	if (!calfilein.is_open()){
	  cout<<"Error: Cannot open file "<< filename<<endl;
	  return -1;
	}
	calfilein>>MCPShiftX>>MCPShiftY>>MCPScaleX>>MCPScaleY;
	calfilein>>MCPsizeX>>MCPsizeY>>MCPNPointEdge;
	MCPSquareArray = new GridSquare*[MCPsizeX];
	for (int i=0;i<MCPsizeX;i++){
	  MCPSquareArray[i] = new GridSquare[MCPsizeY];
	}
	int auxint1;
	int auxint2;
	int status;
	for (int i=0;i<MCPsizeX;i++){
	  for (int j=0;j<MCPsizeY;j++){
	    calfilein>>auxint1>>auxint2>>status;
	    if (status!=1){
	      MCPSquareArray[i][j].status=0;
	      continue;
	    }else{
	      MCPSquareArray[i][j].status=1;
	      for (int n=0;n<4;n++){
		calfilein>>MCPSquareArray[i][j].Corners[n].x>>MCPSquareArray[i][j].Corners[n].y;
	      }
	      for (int m=0;m<4;m++){
		for (int n=0;n<=MCPNPointEdge-1;n++){
		  calfilein>>MCPSquareArray[i][j].Edges[m].PointList[n].x>>MCPSquareArray[i][j].Edges[m].PointList[n].y;
		}
	      }
	      for (int n=0;n<6;n++){
		calfilein>>MCPSquareArray[i][j].XFitPar[n]>>MCPSquareArray[i][j].YFitPar[n];
	      }
	      calfilein >> MCPSquareArray[i][j].X_in_left;
	      calfilein >> MCPSquareArray[i][j].X_in_right;
	      calfilein >> MCPSquareArray[i][j].Y_in_bottom;
	      calfilein >> MCPSquareArray[i][j].Y_in_top;
	      calfilein >> MCPSquareArray[i][j].X_out_left;
	      calfilein >> MCPSquareArray[i][j].X_out_right;
	      calfilein >> MCPSquareArray[i][j].Y_out_bottom;
	      calfilein >> MCPSquareArray[i][j].Y_out_top;
	    }
	  }
	}
	calfilein.close();
	cout <<"MCP Position Local-Calibration data loaded."<<endl;
	//Delete global calibration parameters
	for (int i=0;i<6;i++){
	  XFitPar[i] = 0;
	  YFitPar[i] = 0;
	}
	XFitPar[3]=1.0;
	YFitPar[4]=1.0;
      }else{
	cout <<"Unidentified MCP calibration mode "<<MCPPosCalMode<<endl;
	return -1;
      }
    }else{
      MCPShiftX = 0.0;
      MCPShiftY = 0.0;
      MCPScaleX = 1.0;
      MCPScaleY = 1.0;
      for (int i=0;i<6;i++){
	XFitPar[i] = 0;
	YFitPar[i] = 0;
      }
      XFitPar[3]=1.0;
      YFitPar[4]=1.0;
      //Force Global calibration if the calibration is turned off
      SetExpMCPCalMode("Global");
      //clear the local calibration parameters
      MCPSquareArray=NULL;
      MCPNPointEdge=0;
      MCPsizeX=0;
      MCPsizeY=0;
    }
  }
  else if(name.compare("T0Corr")==0){
    if(Switch){
      sprintf(filename,"%s/T0CorrMap%04d.root",CALIBRATION_DIRECTORY.c_str(),ExpCalIDs[name]);
      if (t0cell == NULL) t0cell = new T0_Cell(filename);
      else {
        cout<< "A T0_Cell instance of size "<<t0cell->GetNCells()<<" is already loaded! Check your CalConfig switches..."<<endl;
        return -1; 
      }
    }
  } else if(name.compare("T0CorrQMCP")==0){
    if(Switch){
      sprintf(filename,"%s/T0CorrQMCP%04d.root",CALIBRATION_DIRECTORY.c_str(),ExpCalIDs[name]);
      if (t0vsQMCPpoly1 == NULL){
        TFile f(filename,"read");
        if (f.IsZombie()){
          cout << "ERROR: Could not open file "<<filename<<"!"<<endl;
          cout << "Check the full path!\n";
                exit(1);
        }
        t0vsQMCPpoly1 = (TF1*) f.Get("poly1")->Clone();
      }else {
        cout<< "A t0 vs QMCP polynomial is already loaded! Check your CalConfig switches..."<<endl;
        return -1; 
      }
    }
  }else{
    cout << "Unkown calibration name "<<name<<endl;
  }
  return 0;
}
/***********************************************************************************/
int Analyzer::LoadSimCalibrationData(string name,bool Switch){
  ifstream calfilein;
  char filename[100];
  //Load Scintillator calibration data
  if(name.compare("Scint")==0){
    if (Switch){
      sprintf(filename,"%s/SimScintCalibration%02d.txt",CALIBRATION_DIRECTORY.c_str(),SimCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
	cout<<"Error: Cannot open file "<< filename<<endl;
	return -1;
      }
      double aux;
      for (int i=0;i<3;i++){
	calfilein>>SimCalPeaks[i*2]>>SimCalPeaks[i*2+1]>>aux>>aux;
      }
      calfilein.close();
      SimScintCalLoaded=true;
    }else{
      for (int i=0;i<6;i++){
	SimCalPeaks[i]=1.0;
      }
      SimScintCalLoaded=false;
    }
  }
  //Load MWPC Energy calibration data
  else if (name.compare("MWPCE")==0){
    if (Switch){
      sprintf(filename,"%s/SimMWPCECalibration%02d.txt",CALIBRATION_DIRECTORY.c_str(),SimCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
	cout<<"Error: Cannot open file "<< filename<<endl;
	return -1;
      }
      calfilein>>SimMWPCAnode_Cal>>SimMWPCCathode_Cal;
      calfilein.close();
    }else{
      //Unload calibration data
      SimMWPCAnode_Cal=SimMWPCCathode_Cal=1.0;
    }
  }
  //Load MWPC Gain Map calibration data
  else if (name.compare("MWPCGainMap")==0){
    if (Switch){
      sprintf(filename,"%s/MWPCGainMap%02d.txt",CALIBRATION_DIRECTORY.c_str(),SimCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
	cout<<"Error: Cannot open file "<< filename<<endl;
	return -1;
      }
      double dummyX;
      double dummyY;
      for (int i=0;i<17;i++){
	for (int j=0;j<18;j++){
	  calfilein>>dummyX>>dummyY>>SimMWPCGainMap[i][j];
	}
      }
      calfilein.close();
    }else{
      for (int i=0;i<17;i++){
	for (int j=0;j<18;j++){
	  SimMWPCGainMap[i][j]=1.0;
	}
      }
    }
  }
  //Load MWPC Efficiency Map calibration data
  else if (name.compare("MWPCEffMap")==0){
    if (Switch){
      sprintf(filename,"%s/SimMWPCEffMap%02d.txt",CALIBRATION_DIRECTORY.c_str(),SimCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
	cout<<"Error: Cannot open file "<< filename<<endl;
	return -1;
      }
      double dummyX;
      double dummyY;
      for (int i=0;i<15;i++){
	for (int j=0;j<16;j++){
	  calfilein>>SimMWPCEffMap[i][j];
	}
      }
      calfilein.close();

      //Take the ratio between simulation and experiment
      double Max=0;
      for (int i=0;i<15;i++){
	for (int j=0;j<16;j++){
	  if(SimMWPCEffMap[i][j]>0)SimMWPCEffMap[i][j]=MWPCEffMap[i][j]/SimMWPCEffMap[i][j];
	  else SimMWPCEffMap[i][j]=MWPCEffMap[i][j];
	  if (SimMWPCEffMap[i][j]>Max)Max = SimMWPCEffMap[i][j];
	}
      }
      //Normalize
      for (int i=0;i<15;i++){
	for (int j=0;j<16;j++){
	  SimMWPCEffMap[i][j]/=Max;
	}
      }
    }else{
      for (int i=0;i<15;i++){
	for (int j=0;j<16;j++){
	  SimMWPCEffMap[i][j]=1.0;
	}
      }
      //Take the ratio between simulation and experiment
      double Max=0;
      for (int i=0;i<15;i++){
	for (int j=0;j<16;j++){
	  if(SimMWPCEffMap[i][j]>0)SimMWPCEffMap[i][j]=MWPCEffMap[i][j]/SimMWPCEffMap[i][j];
	  else SimMWPCEffMap[i][j]=MWPCEffMap[i][j];
	  if (SimMWPCEffMap[i][j]>Max)Max = SimMWPCEffMap[i][j];
	}
      }
      //Normalize
      for (int i=0;i<15;i++){
	for (int j=0;j<16;j++){
	  SimMWPCEffMap[i][j]/=Max;
	}
      }
    }
  }
  //Load MWPC Position calibration data
  else if (name.compare("MWPCPos")==0){
    cout <<"MWPCPos Calibration\n";	

    if (Switch){
      //Load MWPC Position calibration data
      TGraph* CalCathodeXGraph0;
      TGraph* CalCathodeXGraph1[3];
      TGraph* CalCathodeXGraph2[4];
      CalCathodeXGraph0 = new TGraph();
      CalCathodeXGraph0->SetName("CalCathodeXGraph0");
      for (int i=0;i<3;i++){
	CalCathodeXGraph1[i] = new TGraph();
	CalCathodeXGraph1[i]->SetName(Form("CalCathodeXGraph1_%d",i));
      }
      for (int i=0;i<4;i++){
	CalCathodeXGraph2[i] = new TGraph();
	CalCathodeXGraph2[i]->SetName(Form("CalCathodeXGraph2_%d",i));
      }
      TF1* fitFuncX0;
      TF1* fitFuncX1[3];
      TF1* fitFuncX2[4];

      TGraph* CalCathodeYGraph = new TGraph();
      TGraph* CalAnodeGraph = new TGraph();
      CalCathodeYGraph->SetName("CalCathodeYGraph");
      CalAnodeGraph->SetName("CalAnodeGraph");

      //      double InData = 0.0;
      sprintf(filename,"%s/SimMWPCPosCalibration%02d.txt",CALIBRATION_DIRECTORY.c_str(),SimCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
	cout<<"Error: Cannot open file "<< filename<<endl;
	return -1;
      }else cout <<	"MWPCPos Calibration file open success!\n";	

      //Calibrate X
      for (int i=0;i<9;i++){
	calfilein>>MWPCExactPos0[i]>>MWPCPeaksX0[i];
	CalCathodeXGraph0->SetPoint(i,MWPCPeaksX0[i],-8.0+2.0*i);
      }
      int k=0;
      for (int j=0;j<3;j++){
	for (int i=0;i<5;i++){
	  calfilein>>MWPCExactPos1[k]>>MWPCPeaksX1[k];
	  CalCathodeXGraph1[j]->SetPoint(i,MWPCPeaksX1[k]+8.0*(1-j),-4.0+2.0*i);
	  k++;
	}
      }
      k=0;
      for (int j=0;j<4;j++){
	for (int i=0;i<5;i++){
	  calfilein>>MWPCExactPos2[k]>>MWPCPeaksX2[k];
	  CalCathodeXGraph2[j]->SetPoint(i,MWPCPeaksX2[k]+8.0*(1-j)+4.0,-4.0+2.0*i);
	  k++;
	}
      }

      //Fit to pol[3]
      CalCathodeXGraph0->Fit("pol3");
      fitFuncX0 = CalCathodeXGraph0->GetFunction("pol3");
      for (int i=0;i<4;i++){
	//	MWPCXCalPar[i] = fitFuncX0->GetParameter(i);
	MWPCCalPar.FitParametersX0[i] = fitFuncX0->GetParameter(i);
      }
      for (int i=0;i<3;i++){
	CalCathodeXGraph1[i]->Fit("pol3");
	fitFuncX1[i] = CalCathodeXGraph1[i]->GetFunction("pol3");
	for (int j=0;j<4;j++){
	  //	  MWPCXCalPar[i*4+j+4] = fitFuncX1[i]->GetParameter(j);
	  MWPCCalPar.FitParametersX1[i][j] = fitFuncX1[i]->GetParameter(j);
	}
      }
      for (int i=0;i<4;i++){
	CalCathodeXGraph2[i]->Fit("pol3");
	fitFuncX2[i] = CalCathodeXGraph2[i]->GetFunction("pol3");
	for (int j=0;j<4;j++){
	  //	  MWPCXCalPar[i*4+j+16] = fitFuncX2[i]->GetParameter(j);
	  MWPCCalPar.FitParametersX2[i][j] = fitFuncX2[i]->GetParameter(j);
	}
      }

      //Calibrate Y
      //For this moment, no calibration of cathode Y
      /*      for (int i=0;i<18;i++){
	      calfilein>>InData;
	      CalCathodeYGraph->SetPoint(i,InData,-17.0+i*2.0);
	      MWPCPeaksA[i]=InData;
      //	cout << InData<< endl;
      }
      CalCathodeYGraph->Fit("pol3");
      TF1 * fitFuncY = CalCathodeYGraph->GetFunction("pol3");
      for (int i=0;i<4;i++){
      MWPCYCalPar[i] = fitFuncY->GetParameter(i);
      }*/

      //Calibrate A 
      for (int i=0;i<18;i++){
	calfilein>>MWPCExactPosA[i]>>MWPCPeaksA[i];
	CalAnodeGraph->SetPoint(i,MWPCPeaksA[i],-17.0+i*2.0);
      }
      CalAnodeGraph->Fit("pol3");
      TF1 * fitFuncA = CalAnodeGraph->GetFunction("pol3");
      for (int i=0;i<4;i++){
	//	MWPCACalPar[i] = fitFuncA->GetParameter(i);
	MWPCCalPar.FitParametersA[i] = fitFuncA->GetParameter(i);
      }

      calfilein.close();
      /*
	 TFile * CalFileGraph = new TFile("Graphs.root","recreate");
	 CalAnodeGraph->Write();
	 CalCathodeXGraph->Write();
	 CalCathodeYGraph->Write();
	 CalFileGraph->Close();
	 delete CalFileGraph;
       */

      delete CalCathodeXGraph0;
      for (int i=0;i<3;i++) delete CalCathodeXGraph1[i];
      for (int i=0;i<4;i++) delete CalCathodeXGraph2[i];
      delete CalCathodeYGraph;
      delete CalAnodeGraph;
    }else{ //if Switch = false
      for (int i=0;i<4;i++){
	if (i==1) MWPCCalPar.FitParametersX0[i] = 1.0;
	else MWPCCalPar.FitParametersX0[i] = 0.0;
      }
      for (int i=0;i<3;i++){
	for (int j=0;j<4;j++){
	  if (j==1) MWPCCalPar.FitParametersX1[i][j] = 1.0;
	  else MWPCCalPar.FitParametersX1[i][j] = 0.0;
	}
      }
      for (int i=0;i<4;i++){
	for (int j=0;j<4;j++){
	  if (j==1) MWPCCalPar.FitParametersX2[i][j] = 1.0;
	  else MWPCCalPar.FitParametersX2[i][j] = 0.0;
	}
      }
      for (int i=0;i<4;i++){
	if (i==1) MWPCCalPar.FitParametersA[i] = 1.0;
	else  MWPCCalPar.FitParametersA[i] = 0.0;
      }

      for (int i=0;i<9;i++){
	MWPCExactPos0[i] = MWPCPeaksX0[i] = -8.0+2.0*i;
      }
      for (int i=0;i<15;i++){
	if (i<5) MWPCExactPos1[i] = MWPCPeaksX1[i] = -12.0+2.0*i;
	if (i>=5 && i<10) MWPCExactPos1[i] = MWPCPeaksX1[i] = -12.0+2.0*i-2.0;
	if (i>=10 && i<15) MWPCExactPos1[i] = MWPCPeaksX1[i] = -12.0+2.0*i-4.0;
      }
      for (int i=0;i<20;i++){
	if (i<5) MWPCExactPos2[i] = MWPCPeaksX2[i] = -16.0+2.0*i;
	if (i>=5 && i<10) MWPCExactPos2[i] = MWPCPeaksX2[i] = -12.0+2.0*i-2.0;
	if (i>=10 && i<15) MWPCExactPos2[i] = MWPCPeaksX2[i] = -12.0+2.0*i-4.0;
	if (i>=15 && i<20) MWPCExactPos2[i] = MWPCPeaksX2[i] = -12.0+2.0*i-6.0;
      }
      for (int i=0;i<18;i++){ 
	MWPCExactPosA[i] = MWPCPeaksA[i] = -17.0+2.0*i;
      }
    }
  }
  else if (name.compare("MCPEffMap")==0){
    if (Switch){
      //Load MCP Efficiency Map calibration data
      sprintf(filename,"%s/MCPEfficiencyMap%02d.txt",CALIBRATION_DIRECTORY.c_str(),SimCalIDs[name]);
      calfilein.open(filename,ios::in);
      if (!calfilein.is_open()){
	cout<<"Error: Cannot open file "<< filename<<endl;
	return -1;
      }
      double dummyX;
      double dummyY;
      for (int i=0;i<17;i++){
	for (int j=0;j<17;j++){
	  calfilein>>MCPEffMap[i][j];
	}
      }
      calfilein.close();
    }else{
      for (int i=0;i<17;i++){
	for (int j=0;j<17;j++){
	  MCPEffMap[i][j]=1.0;
	}
      }
    }
  }
  else{
    cout << "Unkown calibration name "<<name<<endl;
  }
  return 0;
}
/***********************************************************************************/
int Analyzer::SetCalIDSim(string name,int val)
{
  if (SimCalSwitches.find(name)==SimCalSwitches.end()){
    cout << "Calibration name "<<name<<" not found!"<<endl;
    return -1;
  }
  SimCalIDs[name]=val;
  if (SimCalSwitches[name])LoadSimCalibrationData(name,true); 
  return 0;
}

/***********************************************************************************/
int Analyzer::CalConfigSim(string name,string status)
{
  if (SimCalSwitches.find(name)==SimCalSwitches.end()){
    cout << "Calibration name "<<name<<" not found!"<<endl;
    return -1;
  }
  if (status.compare("ON")==0){
    SimCalSwitches[name]=true;
    LoadSimCalibrationData(name,true);
  }else if(status.compare("OFF")==0){
    SimCalSwitches[name]=false;
    LoadSimCalibrationData(name,false);
  }else{
    cout << "Unkown status "<<status<<". Set to ON by default.\n";
    SimCalSwitches[name]=true;
    LoadSimCalibrationData(name,true);
  }
  return 0;
}
/***********************************************************************************/

int Analyzer::SetCalIDExp(string name,int val)
{
  int retVal = 0;
  if (ExpCalIDs.find(name)==ExpCalIDs.end()){
    cout << "Calibration name "<<name<<" not found!"<<endl;
    return -1;
  }
  ExpCalIDs[name]=val;
  if (ExpCalSwitches[name]) retVal = LoadExpCalibrationData(name,true); //if turned on, go ahead and try to load data
  return retVal;
}

/***********************************************************************************/
int Analyzer::CalConfigExp(string name,string status)
{
  int retVal = 0;
  if (ExpCalSwitches.find(name)==ExpCalSwitches.end()){
    cout << "Calibration name "<<name<<" not found!"<<endl;
    return -1;
  }
  if (status.compare("ON")==0){
    ExpCalSwitches[name]=true;
    retVal = LoadExpCalibrationData(name,true);
  }else if(status.compare("OFF")==0){
    ExpCalSwitches[name]=false;
    retVal = LoadExpCalibrationData(name,false);
  }else{
    cout << "Unkown status "<<status<<". Set to ON by default.\n";
    ExpCalSwitches[name]=true;
    retVal = LoadExpCalibrationData(name,true);
  }
  return retVal;
}
/***********************************************************************************/
int Analyzer::SetExpMCPCalMode(string Mode){
  MCPPosCalMode = Mode;
  return 0;
}
/***********************************************************************************/
int Analyzer::SetT0CorrMode(string Mode){
  T0CorrMode = Mode;
  return 0;
}
/***********************************************************************************/
int Analyzer::SetT0CorrStrength(double st){
 if(t0cell==NULL){
  cout<<"Error: T0 Correction not loaded. Load correction before setting the strength.";
  exit(1);
 }
 t0cell->SetRelCorrStrength(st); 
}
/***********************************************************************************/
int Analyzer::SetMWPCPosCalMode(string Mode){
  MWPCPosCalMode = Mode;
  return 0;
}
/***********************************************************************************/
int Analyzer::SetMWPCKeepLessTrigger(int Val){
  KeepLessTrigger = bool(Val);
  return 0;
}
/***********************************************************************************/
int Analyzer::CleanupCalibrationStructures(){
  if(MCPSquareArray != NULL){
    for (int i=0;i<MCPsizeX;i++){
      delete MCPSquareArray[i];
    }
    delete [] MCPSquareArray;
  }
  if(t0cell!=NULL) delete t0cell; 
}










