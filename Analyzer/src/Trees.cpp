//C++ includes
#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string>
#include <sstream> 
#include <vector>
#include <stdint.h>
#include <stdexcept>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
#include "TTree.h"
//Analyzer includes
#include "Analyzer.h"

using namespace std;

int Analyzer::SetupInputFile(int FileID, TFile* &tfilein, TTree* &InTree){
  string basename;
  ostringstream convert;
    
  if(ReadMode.compare("MCP")==0 || ReadMode.compare("He4")==0){basename = "IonOnly_EventFile";}
  else if(ReadMode.compare("Beta")==0 || ReadMode.compare("MWPC")==0){basename = "BetaOnly_EventFile";}
  else{basename = "EventFile";}

  convert << setfill('0') << setw(9)<<FileID;
  string fName = SIM_DATA_DIRECTORY + "/" + basename + convert.str();
  fName += ".root";
  
  tfilein = new TFile(fName.c_str(),"READ");
  if (tfilein->IsZombie()){
    cout << "No such file: "<<fName<<endl;
    cout << "Consider including the full path!\n";
	  return -1;
  }
  
  if(ReadMode.compare("Beta")==0 || ReadMode.compare("MWPC")==0){
    InTree = (TTree*) tfilein->Get("BetaTree");
  }else if(ReadMode.compare("MCP")==0 || ReadMode.compare("He4")==0){
    InTree = (TTree*) tfilein->Get("IonTree");
  }
  else {
    InTree = (TTree*) tfilein->Get("BetaTree");
    InTree->AddFriend("IonTree");
  }       
  InTree->AddFriend("ProcessorTree");      
   
  return 0;
}    
/********************************************************************/
int Analyzer::OpenExpInputFile(const char* fName, TFile* &tfilein){
  tfilein = new TFile(fName,"READ");
  if (tfilein->IsZombie()){
    cout << "No such file: "<<fName<<endl;
    cout << "Consider including the full path!\n";
	  return -1;
  }
}  
/********************************************************************/         
int Analyzer::InitTreesToNull(){
  OutTrees = new TTree**[N_OF_HIST];
  for(int i=0;i<N_OF_HIST;i++){
    OutTrees[i] = new TTree*[N_OF_HIST];
    for(int j=0;j<N_OF_HIST;j++){
      OutTrees[i][j] = NULL;
    }
  }
  return 0;
}
/********************************************************************/         
int Analyzer::LoadOutTrees(const char* fname){
  if(OutTreesInitialized){
    cout<<"OutTrees already initialized. Deleting and reinitializing."<<endl;
		DelOutTrees();
		//InitTreesToNull();    
		//exit(1);
  }
  string TreeName;
  char filename[200];
  InitTreesToNull(); ///////////Right here.
  string purpose;
  if (Purpose == 0){
    purpose = "Sim";
    OutTreesFilenameSim = fname;
  }else{
    purpose = "Exp";
    OutTreesFilenameExp = fname;
  }
  string mode = ReadMode;
  sprintf(filename,"%s/%s/OutTrees_%s.root",OUTPUT_TREE_DIRECTORY.c_str(),purpose.c_str(),fname);//,mode.c_str());
  cout<<"Loading trees from output file "<<filename<<"."<<endl;
  OutTreesFile = new TFile(filename,"READ");
  if (OutTreesFile->IsZombie()) exit(1); 
  int i,j;
  for(j=0;j<N_OF_HIST;j++){
    for(i=0;i<N_OF_HIST;i++){
      TreeName = "EventTree";
      TreeName+=Form("_Cond%d,%d",i,j);
      OutTrees[i][j] = (TTree*) OutTreesFile->Get(TreeName.c_str());
      if (OutTrees[i][j] ==NULL){
        if(i==0 && j!=0) HistDim2=j; //set the histogram dimension of the column to the number of the last successful load of the column
        break;
      }
      else HistDim1 = i+1;

      //OutTrees[i][j]->SetDirectory(OutTreesFile); //done automatically
    }
    if (i==0) break;
    else continue;
  }
  OutTreesInitialized = true;
  return 0;
}
/********************************************************************/  
int Analyzer::InitOutTrees(){
  if(OutTreesInitialized){
    cout<<"OutTrees already initialized. Deleting and reinitializing."<<endl;
		DelOutTrees();
		//InitTreesToNull();    
		//exit(1);
  }
  string TreeName;
  char filename[200];
  InitTreesToNull();
  string purpose, fname;
  if (Purpose == 0){
    purpose = "Sim";
    fname = OutTreesFilenameSim;
  }else{
    purpose = "Exp";
    fname = OutTreesFilenameExp;
  }
  string mode = ReadMode;
  sprintf(filename,"%s/%s/OutTrees_%s_%s.root",OUTPUT_TREE_DIRECTORY.c_str(),purpose.c_str(),fname.c_str(),mode.c_str());
  OutTreesFile = new TFile(filename,"RECREATE");
  if (OutTreesFile->IsZombie()) exit(1); 
	for(int i=0;i<HistDim1;i++){
	  for(int j=0;j<HistDim2;j++){
	    TreeName = "EventTree";
	    TreeName+=Form("_Cond%d,%d",i,j);
	    OutTrees[i][j] = new TTree(TreeName.c_str(),TreeName.c_str());
	    OutTrees[i][j]->SetDirectory(OutTreesFile); //done automatically
	  }
	}
	OutTreesInitialized = true;
	return 0;
}
/********************************************************************/ 
int Analyzer::DelOutTrees(){
  if(OutTrees!=NULL){
    for(int i=0;i<N_OF_HIST;i++){
	    for(int j=0;j<N_OF_HIST;j++){
	      if (OutTrees[i][j]==NULL){break;}		  
	      else {delete OutTrees[i][j];} //deallocate memory for tree
	    }
	    delete[] OutTrees[i]; //deallocate memory for every element (pointer OutTrees[i][j]) in array OutTrees[i]
    }
    delete[] OutTrees; //deallocate memory for every element (pointer OutTrees[i]) in array OutTrees
    OutTrees = NULL;
  }  
  OutTreesInitialized = false;
  
  //Tfile
  if(OutTreesFile!=NULL){
    OutTreesFile->Close(); //deleting a file automatically closes it
    delete OutTreesFile;
    OutTreesFile = NULL;
  }
  return 0;
}
/********************************************************************/ 
int Analyzer::SetOutTreesAddress(InDataStruct &InData, InDataStructDouble &InDataDouble, SimRecDataStruct &SimRecData){
	for(int i=0;i<HistDim1;i++){
	  for(int j=0;j<HistDim2;j++){
	    if(ReadMode.compare("Triple")==0||ReadMode.compare("Beta")==0 || ReadMode.compare("MWPC")==0 ){//BetaInfo
	    //Imported from BetaTree
	      OutTrees[i][j]->Branch("ScintEnergy",&InDataDouble.ScintEnergyBB,"ScintEnergy/D");
        OutTrees[i][j]->Branch("MWPCEnergy",&InDataDouble.MWPCEnergyBB,"MWPCEnergy/D");
        OutTrees[i][j]->Branch("ExitAngle_Be",&InDataDouble.ExitAngle_Be,"ExitAngle_Be/D");
        OutTrees[i][j]->Branch("P0e",&InDataDouble.InitP[0],"x/D:y/D:z/D");
        OutTrees[i][j]->Branch("Pos0",&InDataDouble.DECPos[0],"x/D:y/D:z/D");
        //OutTrees[i][j]->Branch("VIon0",&InData.V_Ion[0],"x/I:y/I:z/I");
        OutTrees[i][j]->Branch("Hits",&InData.Hits,"Hits/s");//Hits bitmap: 0 MCP Wall Ele Col Win MWPC Sci
        OutTrees[i][j]->Branch("ParticleID",&InData.ParticleID,"ParticleID/b");//0:electron 1:photon
        OutTrees[i][j]->Branch("MWPCPosEn",&InDataDouble.PosMWPCEntrance[0],"x/D:y/D:z/D"); //[mm]
        OutTrees[i][j]->Branch("MWPCPosEx",&InDataDouble.PosMWPCExit[0],"x/D:y/D:z/D"); //[mm]
        
  //      OutTrees[i][j]->Branch("MWPCFiredCell",&InData.MWPCFiredCell,"MWPCFiredCell/b");
  //      //for (int i=0;i<20;i++) InData.MWPCCellIndex[i] = (uint8_t )i; //just a check
  //      OutTrees[i][j]->Branch("MWPCCellIndex",&InData.MWPCCellIndex[0],"MWPCCellIndex[20]/b");
  //      OutTrees[i][j]->Branch("MWPCCellIonNumber",&InData.MWPCCellIonNumber[0],"MWPCCellIonNumber[20]/s");
  //      OutTrees[i][j]->Branch("MWPCCellXpos",&InData.MWPCCellXpos[0],"MWPCCellXpos[20]/S");
  //      OutTrees[i][j]->Branch("MWPCCellXvar",&InData.MWPCCellXvar[0],"MWPCCellXvar[20]/s");
  //      OutTrees[i][j]->Branch("ScintTime",&InData.ScintTime,"ScintTime/S");      
  //      OutTrees[i][j]->Branch("MCPTime",&InData.MCPTime,"MCPTime/S");

      //Imported from ProcessorTree
        OutTrees[i][j]->Branch("ScintEnergyPP",&InDataDouble.ScintEnergy,"ScintEnergyPP/D"); //Directly copied over as reconstructed value in Analyzer
	    	OutTrees[i][j]->Branch("MWPCEnergyPP",&InDataDouble.MWPCEnergy,"MWPCEnergyPP/D");
	    	OutTrees[i][j]->Branch("MWPCHitPos",&InDataDouble.MWPCHitPos[0],"X/D:Y/D");	 

	    	//OutTrees[i][j]->Branch("MWPCCathodeSignal",&InDataDouble.MWPCCathodeSignal[0] ,"MWPCCathodeSignal[12]/D"); <--MWPCCalDev	  	
	    	//OutTrees[i][j]->Branch("",&InData.TOFCorrection,"");
	    	
	    //Reconstruced Analyzer data
	    	OutTrees[i][j]->Branch("MWPCAnodeSignal1AA",&InDataDouble.MWPCAnodeSignal1 ,"MWPCAnodeSignal1/D"); //in keV //Altered in ReconstructSim
	    	OutTrees[i][j]->Branch("MWPCAnodeSignal2AA",&InDataDouble.MWPCAnodeSignal2 ,"MWPCAnodeSignal2/D"); //in keV //Altered in ReconstructSim 	
	    	OutTrees[i][j]->Branch("MWPCEnergyAA",&SimRecData.MWPCEnergy,"MWPCEnergyAA/D"); //in keV
    	  OutTrees[i][j]->Branch("MWPCCathodeSignalX",&SimRecData.MWPCCathodeSignalX,"MWPCCathodeSignalX/D"); //any unit
        OutTrees[i][j]->Branch("MWPCCathodeSignalY",&SimRecData.MWPCCathodeSignalY,"MWPCCathodeSignalY/D"); //any unit
        OutTrees[i][j]->Branch("FiredWireX",&SimRecData.FiredWireX,"FiredWireX/I");
        OutTrees[i][j]->Branch("FiredWireY",&SimRecData.FiredWireY,"FiredWireY/I");
        OutTrees[i][j]->Branch("MWPCHitPosAA",&SimRecData.MWPCHitPos[0],"X/D:Y/D");          //in mm
        OutTrees[i][j]->Branch("MWPC_CY",&SimRecData.MWPC_CY,"MWPC_CY/D");
        OutTrees[i][j]->Branch("MWPCHitShift",&SimRecData.MWPCHitShift[0],"X/D:Y/D");//Shifts from the reconstructed position to real position	  	      
      }
	    if(ReadMode.compare("Triple")==0||ReadMode.compare("MCP")==0 || ReadMode.compare("He4")==0){//IonInfo       
	    //Imported from IonTree
	      OutTrees[i][j]->Branch("TOF",&(InDataDouble.TOFII),"TOF/D");
        OutTrees[i][j]->Branch("MCPHitPos",&(InDataDouble.HitPosII[0]),"X/D:Y/D");
        OutTrees[i][j]->Branch("HitAngle",&InDataDouble.HitAngle,"HitAngle/D");
        OutTrees[i][j]->Branch("IonEnergy",&InDataDouble.IonEnergy,"IonEnergy/D");
        //OutTrees[i][j]->Branch("Status",&(InData.Status),"Status/b");
        OutTrees[i][j]->Branch("ChargeState",&(InData.ChargeState),"ChargeState/b");
        OutTrees[i][j]->Branch("Pos0",&InDataDouble.DECPos[0],"x/D:y/D:z/D"); 
        OutTrees[i][j]->Branch("VIon0",&InDataDouble.V_Ion[0],"x/D:y/D:z/D");
        OutTrees[i][j]->Branch("EIon0",&InDataDouble.EIonInit,"EIon0/D"); //in keV
                
      //Imported from ProcessorTree    	
	    	//OutTrees[i][j]->Branch("MCPTimePP",&InData.MCPTime,"MCPTimePP/S");
	    	OutTrees[i][j]->Branch("TOFPP",&InDataDouble.TOF ,"TOFPP/D");
	    	OutTrees[i][j]->Branch("MCPHitPosPP",&InDataDouble.HitPos[0] ,"X/D:Y/D");
	    	//OutTrees[i][j]->Branch("IonStatusPP",&InData.IonStatus ,"IonStatusPP/b");

        	
	    //Reconstruced Analyzer data
        OutTrees[i][j]->Branch("TOFAA",&SimRecData.TOF,"TOFAA/D");//in ns
	    	OutTrees[i][j]->Branch("MCPHitPosAA",&SimRecData.HitPos[0],"X/D:Y/D");//in mm

      }
      if(ReadMode.compare("Triple")==0){ //exclusive to Triples
        OutTrees[i][j]->Branch("RecIonEnergyAA",&SimRecData.EIon[0],"Q1/D:Q2/D:Q3/D");//in keV
        OutTrees[i][j]->Branch("RecQ_ValueAA",&SimRecData.Q_Value[0],"Q1/D:Q2/D:Q3/D");//in keV
        OutTrees[i][j]->Branch("RecCosThetaENuAA",&SimRecData.CosThetaENu,"CosThetaENuAA/D");//      
        OutTrees[i][j]->Branch("COSBetaShiftAngle",&SimRecData.COSBetaShiftAngle,"COSBetaShiftAngle/D"); //angle between initial momentum and reconstructed one (????) 
      }     
	  }
	}  
  return 0;
}

/***********************************************************************************/
int Analyzer::SetOutTreesAddress(RecDataStruct &ExpRecData){
	for(int i=0;i<HistDim1;i++){
	  for(int j=0;j<HistDim2;j++){
	      OutTrees[i][j]->Branch("GroupTime",&ExpRecData.GroupTime ,"GroupTime/D"); //in sec
	      OutTrees[i][j]->Branch("Time_rel",ExpRecData.Time_rel ,"Time_rel/D"); //in sec
	      OutTrees[i][j]->Branch("TriggerMap",&ExpRecData.TriggerMap ,"TriggerMap/s"); //Trigger Map
	    if(ReadMode.compare("Triple")==0||ReadMode.compare("Beta")==0 || ReadMode.compare("MWPC")==0 ){//MWPCInfo	    	
	      OutTrees[i][j]->Branch("MWPCAnodeSignal1AA",&ExpRecData.EMWPC_anode1 ,"MWPCAnodeSignal1/D"); //in keV
	    	OutTrees[i][j]->Branch("MWPCAnodeSignal2AA",&ExpRecData.EMWPC_anode2 ,"MWPCAnodeSignal2/D"); //in keV
    	  OutTrees[i][j]->Branch("MWPCEnergyAA",&ExpRecData.EMWPC_anode,"MWPCEnergyAA/D"); //in keV
    	  OutTrees[i][j]->Branch("MWPCCathodeSignalX",&ExpRecData.QMWPC_cathode_X,"MWPCCathodeSignalX/D"); //any unit
        OutTrees[i][j]->Branch("MWPCCathodeSignalY",&ExpRecData.QMWPC_cathode_Y,"MWPCCathodeSignalY/D"); //any unit
        OutTrees[i][j]->Branch("FiredWireX",&ExpRecData.FiredWireX,"FiredWireX/I");
        OutTrees[i][j]->Branch("FiredWireY",&ExpRecData.FiredWireY,"FiredWireY/I");
        OutTrees[i][j]->Branch("MWPCHitPosAA",&ExpRecData.MWPCPos_X,"X/D:Y/D");          //in mm
        //OutTrees[i][j]->Branch("MWPCHitPosAA/",&ExpRecData.MWPCPos_Y,"Y/D");          //in mm
        OutTrees[i][j]->Branch("MWPC_CY",&ExpRecData.MWPCPos_Y_C,"MWPC_CY/D");        //in mm, Cathode reconstruction of Y
        OutTrees[i][j]->Branch("TMWPCAnode1_rel",&ExpRecData.TMWPC_rel[0] ,"TMWPCAnode1_rel/D"); //MWPC time relative to Tref
	    	OutTrees[i][j]->Branch("TMWPCAnode2_rel",&ExpRecData.TMWPC_rel[1] ,"TMWPCAnode2_rel/D"); //MWPC time relative to Tref
	    }
	    if(ReadMode.compare("Triple")==0||ReadMode.compare("Double")==0||ReadMode.compare("Beta")==0 || ReadMode.compare("Scint")==0 ){//ScintInfo
	      OutTrees[i][j]->Branch("ScintEnergyAA",&ExpRecData.EScintA[0],"ScintEnergy/D");   //Only value used in kinematics reconsturction
	      OutTrees[i][j]->Branch("TScint_A_rel",&ExpRecData.TScint_A_rel,"TScint_A_rel/D"); //Scintillator time relative to Tref
	      OutTrees[i][j]->Branch("TScint_DA",&ExpRecData.TScint_DA,"TScint_DA/D");          //Scintillator anode dynode time difference
	      //OutTrees[i][j]->Branch("ScintEnergyAnode",&ExpRecData.EScintA[0],"1/D:2/D");<--available
	      //OutTrees[i][j]->Branch("ScintEnergyDynode",&ExpRecData.EScintD[0],"1/D:2/D");<--available
	    }
	    if(ReadMode.compare("Triple")==0||ReadMode.compare("Double")==0||ReadMode.compare("MCP")==0|| ReadMode.compare("He4")==0){//MCPInfo
	    	OutTrees[i][j]->Branch("MCPHitPosAA",&ExpRecData.MCPPos_X,"X/D:Y/D");//in mm
	    	//OutTrees[i][j]->Branch("MCPHitPosAA",&ExpRecData.MCPPos_Y,"Y/D");//in mm
	    	OutTrees[i][j]->Branch("MCP_TSum",&ExpRecData.MCP_TXSum,"X/D:Y/D"); //in ns //TX1 + TX2
	    	//OutTrees[i][j]->Branch("MCP_TSum",&ExpRecData.MCP_TYSum,"Y/D"); //in ns //TY1 + TY2
	    	OutTrees[i][j]->Branch("QMCP_anodes",&ExpRecData.QMCP_anodes,"QX1/D:QX2/D:QY1/D:QY2/D");
	    	OutTrees[i][j]->Branch("TMCP_anodes",&ExpRecData.TMCP_anodes,"TX1/D:TX2/D:TY1/D:TY2/D");
                OutTrees[i][j]->Branch("QMCP",&ExpRecData.QMCP,"QMCP/D"); //raw signal
	    	OutTrees[i][j]->Branch("TMCP_rel",&ExpRecData.TMCP_rel,"TMCP_rel/D"); //MCP time relative to Tref
      }
      if(ReadMode.compare("Triple")==0||ReadMode.compare("Beta")==0){//Scint and MWPC
	      OutTrees[i][j]->Branch("TBeta_diffA1",&ExpRecData.TMWPC_rel[0] ,"TBeta_diffA1/D"); //MWPC Anode1 time relative to Scint
	    	OutTrees[i][j]->Branch("TBeta_diffA2",&ExpRecData.TMWPC_rel[1] ,"TBeta_diffA2/D"); //MWPC Anode2 time relative to Scint
	    }
	    if(ReadMode.compare("Triple")==0||ReadMode.compare("Double")==0||ReadMode.compare("He4")==0){//Scint and MCP
	      OutTrees[i][j]->Branch("TOFAA",&ExpRecData.TOF,"TOFAA/D");//in ns //MCP time relative to Scint
	    }
	    if(ReadMode.compare("He4")==0){//N2 laser photodiode
        OutTrees[i][j]->Branch("QN2Laser",&ExpRecData.QN2Laser,"QN2Laser/D");//N2 Laser pulse charge on photodiode for event
        OutTrees[i][j]->Branch("TN2Laser",&ExpRecData.TN2Laser,"TN2Laser/D");//Timing of N2 Laser pulse on photodiode for event
      }
      if(ReadMode.compare("Triple")==0){ //exclusive to Triples
        OutTrees[i][j]->Branch("RecIonEnergyAA",&ExpRecData.EIon[0],"Q1/D:Q2/D:Q3/D");//in keV
        OutTrees[i][j]->Branch("RecQ_ValueAA",&ExpRecData.Q_Value[0],"Q1/D:Q2/D:Q3/D");//in keV
        OutTrees[i][j]->Branch("RecCosThetaENuAA",&ExpRecData.CosThetaENu,"CosThetaENuAA/D");//  
      } 
	  }
	}

}
/***********************************************************************************/
int Analyzer::FillOutTrees(int i,int j,bool cond){
  if(cond) OutTrees[i][j]->Fill();
  return 0;
}

int Analyzer::WriteOutTrees(){
  OutTreesFile->cd();
  TList *t_list = new TList;
  for(int i=0;i<HistDim1;i++){
    for(int j=0;j<HistDim2;j++){
      OutTrees[i][j]->Write("", TObject::kOverwrite);
      t_list->Add(OutTrees[i][j]);
    }
  }
  TTree *t_merged = TTree::MergeTrees(t_list);
  t_merged->SetName("EventTree_Combined");
  t_merged->SetTitle("Combined Event Tree");
  t_merged->Write();
  delete t_list;
  return 0;
}
/***********************************************************************************/
int Analyzer::SetInTreeAddress(TTree* InTree, InDataStruct &InData){
  if(ReadMode.compare("Triple")==0||ReadMode.compare("Beta")==0 || ReadMode.compare("MWPC")==0 ){//BetaInfo
    //BetaTree
    InTree->SetBranchAddress("ScintEnergy",&(InData.ScintEnergyBB));
    InTree->SetBranchAddress("MWPCEnergy",&(InData.MWPCEnergyBB));
    InTree->SetBranchAddress("ExitAngle_Be",&(InData.ExitAngle_Be));
    InTree->SetBranchAddress("P0e",&(InData.InitP[0]));
    InTree->SetBranchAddress("Pos0",&(InData.DECPos[0]));
    //InTree->SetBranchAddress("VIon0",&(InData.V_Ion[0]));
    InTree->SetBranchAddress("Hits",&(InData.Hits));
    InTree->SetBranchAddress("ParticleID",&(InData.ParticleID));  
    InTree->SetBranchAddress("PosEn",&(InData.PosMWPCEntrance[0]));
    InTree->SetBranchAddress("PosEx",&(InData.PosMWPCExit[0]));
    //InTree->SetBranchAddress("MWPCFiredCell",&(InData.MWPCFiredCell));
    //for (int i=0;i<20;i++) InData.MWPCCellIndex[i] = (uint8_t )i; //just a check
    //InTree->SetBranchAddress("MWPCCellIndex",&(InData.MWPCCellIndex[0]));
    //InTree->SetBranchAddress("MWPCCellIonNumber",&(InData.MWPCCellIonNumber[0]));
    //InTree->SetBranchAddress("MWPCCellXpos",&(InData.MWPCCellXpos[0]));
    //InTree->SetBranchAddress("MWPCCellXvar",&(InData.MWPCCellXvar[0]));

    //InTree->SetBranchAddress("ScintTime",&(InData.ScintTime));
    //InTree->SetBranchAddress("MCPTime",&(InData.MCPTime));  
    
    //ProcessorTree
	  InTree->SetBranchAddress("ScintEnergyPP",&InData.ScintEnergy);
	  InTree->SetBranchAddress("MWPCEnergyPP",&InData.MWPCEnergy);
	  InTree->SetBranchAddress("MWPCHitPos",&InData.MWPCHitPos[0]);	  	
	  InTree->SetBranchAddress("MWPCAnodeSignal1",&InData.MWPCAnodeSignal1);
	  InTree->SetBranchAddress("MWPCAnodeSignal2",&InData.MWPCAnodeSignal2);
	  InTree->SetBranchAddress("MWPCCathodeSignal",&InData.MWPCCathodeSignal[0]);	  
	  //InTree->SetBranchAddress("",&InData.TOFCorrection,"");
	}
	if(ReadMode.compare("Triple")==0||ReadMode.compare("MCP")==0 || ReadMode.compare("He4")==0){//IonInfo
    //IonTree
    InTree->SetBranchAddress("TOF",&(InData.TOFII));
    InTree->SetBranchAddress("HitPos",&(InData.HitPosII[0]));
    InTree->SetBranchAddress("HitAngle",&(InData.HitAngle));
    InTree->SetBranchAddress("IonEnergy",&(InData.IonEnergy));
    //InTree->SetBranchAddress("Status",&(InData.Status),"Status/b");
    InTree->SetBranchAddress("ChargeState",&(InData.ChargeState));
    InTree->SetBranchAddress("Pos0",&(InData.DECPos[0]));
    InTree->SetBranchAddress("VIon0",&(InData.V_Ion[0]));
    InTree->SetBranchAddress("EIon0",&(InData.EIonInit));

    //ProcessorTree 	
	  //InTree->SetBranchAddress("MCPTimePP",&InData.MCPTime,"MCPTimePP/S");
	  InTree->SetBranchAddress("TOFPP",&InData.TOF);
	  InTree->SetBranchAddress("HitPosPP",&InData.HitPos[0]);
	  InTree->SetBranchAddress("IonStatusPP",&InData.IonStatus);
  }
	return 0;
}
/***********************************************************************************/
int Analyzer::SetExpInTreeAddress(TFile* &tfilein, TTree* &InTree, ExpDataStruct &ExpData){
      if (ReadMode.compare("Triple")==0){
	InTree = (TTree*)tfilein->Get("Tree_Triple");
	InTree->SetBranchAddress("Event_No",&ExpData.Event_Num);
	InTree->SetBranchAddress("GroupTime",&ExpData.GroupTime);
	InTree->SetBranchAddress("QPMT_A",&ExpData.QScint_A);
	InTree->SetBranchAddress("QPMT_D",&ExpData.QScint_D);
	InTree->SetBranchAddress("TPMT_A_rel",&ExpData.TScint_A_rel);
	InTree->SetBranchAddress("TPMT_DA",&ExpData.TScint_DA);
	InTree->SetBranchAddress("AMWPC_anode1",&ExpData.AMWPC_anode1);
	InTree->SetBranchAddress("AMWPC_anode2",&ExpData.AMWPC_anode2);
	InTree->SetBranchAddress("TBeta_diff",&ExpData.TBeta_diff);
	InTree->SetBranchAddress("MWPC_X",&ExpData.MWPC_X);
	InTree->SetBranchAddress("TMWPC_X",&ExpData.TMWPC_X);
	InTree->SetBranchAddress("MWPC_Y",&ExpData.MWPC_Y);
	InTree->SetBranchAddress("TMWPC_Y",&ExpData.TMWPC_Y);
	InTree->SetBranchAddress("TMCP_rel",&ExpData.TMCP_rel);
	InTree->SetBranchAddress("QMCP",&ExpData.QMCP);
	InTree->SetBranchAddress("QMCP_anodes",&ExpData.QMCP_anodes);
	InTree->SetBranchAddress("TMCP_anodes",&ExpData.TMCP_anodes);
	InTree->SetBranchAddress("TOF",&ExpData.TOF);
	InTree->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
      }else if (ReadMode.compare("Double")==0){
	InTree = (TTree*)tfilein->Get("Tree_Double");
	InTree->SetBranchAddress("Event_No",&ExpData.Event_Num);
	InTree->SetBranchAddress("GroupTime",&ExpData.GroupTime);
	InTree->SetBranchAddress("QPMT_A",&ExpData.QScint_A);
	InTree->SetBranchAddress("QPMT_D",&ExpData.QScint_D);
	InTree->SetBranchAddress("TPMT_A_rel",&ExpData.TScint_A_rel);
	InTree->SetBranchAddress("TPMT_DA",&ExpData.TScint_DA);
	InTree->SetBranchAddress("TMCP_rel",&ExpData.TMCP_rel);
	InTree->SetBranchAddress("QMCP",&ExpData.QMCP);
	InTree->SetBranchAddress("QMCP_anodes",&ExpData.QMCP_anodes);
	InTree->SetBranchAddress("TMCP_anodes",&ExpData.TMCP_anodes);
	InTree->SetBranchAddress("TOF",&ExpData.TOF);
	InTree->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
	      }else if (ReadMode.compare("Beta")==0){
	InTree = (TTree*)tfilein->Get("Tree_Beta");
	InTree->SetBranchAddress("Event_No",&ExpData.Event_Num);
	InTree->SetBranchAddress("GroupTime",&ExpData.GroupTime);
	InTree->SetBranchAddress("QPMT_A",&ExpData.QScint_A);
	InTree->SetBranchAddress("QPMT_D",&ExpData.QScint_D);
	InTree->SetBranchAddress("TPMT_A_rel",&ExpData.TScint_A_rel);
	InTree->SetBranchAddress("TPMT_DA",&ExpData.TScint_DA);
	InTree->SetBranchAddress("AMWPC_anode1",&ExpData.AMWPC_anode1);
	InTree->SetBranchAddress("AMWPC_anode2",&ExpData.AMWPC_anode2);
	InTree->SetBranchAddress("TBeta_diff",&ExpData.TBeta_diff);
	InTree->SetBranchAddress("MWPC_X",&ExpData.MWPC_X);
	InTree->SetBranchAddress("TMWPC_X",&ExpData.TMWPC_X);
	InTree->SetBranchAddress("MWPC_Y",&ExpData.MWPC_Y);
	InTree->SetBranchAddress("TMWPC_Y",&ExpData.TMWPC_Y);
	InTree->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
	      }else if (ReadMode.compare("Scint")==0){
	InTree = (TTree*)tfilein->Get("Tree_Scint");
	InTree->SetBranchAddress("Event_No",&ExpData.Event_Num);
	InTree->SetBranchAddress("GroupTime",&ExpData.GroupTime);
	InTree->SetBranchAddress("QPMT_A",&ExpData.QScint_A);
	InTree->SetBranchAddress("QPMT_D",&ExpData.QScint_D);
	InTree->SetBranchAddress("TPMT_A_rel",&ExpData.TScint_A_rel);
	InTree->SetBranchAddress("TPMT_DA",&ExpData.TScint_DA);
	InTree->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
	      }else if (ReadMode.compare("MWPC")==0){
	InTree = (TTree*)tfilein->Get("Tree_MWPC");
	InTree->SetBranchAddress("Event_No",&ExpData.Event_Num);
	InTree->SetBranchAddress("GroupTime",&ExpData.GroupTime);
	InTree->SetBranchAddress("AMWPC_anode1",&ExpData.AMWPC_anode1);
	InTree->SetBranchAddress("AMWPC_anode2",&ExpData.AMWPC_anode2);
	InTree->SetBranchAddress("TMWPC_rel",&ExpData.TMWPC_rel);
	InTree->SetBranchAddress("MWPC_X",&ExpData.MWPC_X);
	InTree->SetBranchAddress("TMWPC_X",&ExpData.TMWPC_X);
	InTree->SetBranchAddress("MWPC_Y",&ExpData.MWPC_Y);
	InTree->SetBranchAddress("TMWPC_Y",&ExpData.TMWPC_Y);
	InTree->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
	     }else if (ReadMode.compare("MCP")==0){
	InTree = (TTree*)tfilein->Get("Tree_MCP");
	InTree->SetBranchAddress("Event_No",&ExpData.Event_Num);
	InTree->SetBranchAddress("GroupTime",&ExpData.GroupTime);
	InTree->SetBranchAddress("TMCP_rel",&ExpData.TMCP_rel);
	InTree->SetBranchAddress("QMCP",&ExpData.QMCP);
	InTree->SetBranchAddress("QMCP_anodes",&ExpData.QMCP_anodes);
	InTree->SetBranchAddress("TMCP_anodes",&ExpData.TMCP_anodes);
	InTree->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
	     }else if(ReadMode.compare("He4")==0){
	InTree = (TTree*)tfilein->Get("Tree_N2Laser");
	InTree->SetBranchAddress("QN2Laser",&ExpData.QN2Laser);
	InTree->SetBranchAddress("TN2Laser",&ExpData.TN2Laser);
	InTree->SetBranchAddress("TOFN2Laser",&ExpData.TOF); //TMCP_back - TN2Laser InTree->SetBranchAddress("TOF",&ExpData.TOF);
	InTree->SetBranchAddress("TMCP_rel",&ExpData.TMCP_rel);
	InTree->SetBranchAddress("QMCP",&ExpData.QMCP);
	InTree->SetBranchAddress("QMCP_anodes",&ExpData.QMCP_anodes);
	InTree->SetBranchAddress("TMCP_anodes",&ExpData.TMCP_anodes);
	InTree->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
	}
}
/***********************************************************************************/
//int Analyzer::SetRootInputSim (bool Switch){
//  RootInputSim = Switch;
//  return 0;
//}
/***********************************************************************************/
int Analyzer::SetRootOutputSim (bool Switch, string fname){
  RootOutputSim = Switch;
  OutTreesFilenameSim = fname;
  return 0;
}
/***********************************************************************************/
int Analyzer::SetRootOutputExp (bool Switch, string fname){
  RootOutputExp = Switch;
  OutTreesFilenameExp = fname;
  return 0;
}
/***********************************************************************************/
int Analyzer::ConvertDataToDouble(InDataStruct &InData, InDataStructDouble &InDataDouble){
	if(ReadMode.compare("Triple")==0||ReadMode.compare("Beta")==0 || ReadMode.compare("MWPC")==0 ){//BetaInfo
	  //Beta Info Convert to double
	  InDataDouble.ScintEnergy = double(InData.ScintEnergy)/1000.0;
	  InDataDouble.MWPCEnergy = double(InData.MWPCEnergy)/1000.0;
	  InDataDouble.ScintEnergyBB = double(InData.ScintEnergyBB)/1000.0;
	  InDataDouble.MWPCEnergyBB = double(InData.MWPCEnergyBB)/1000.0;
	  InDataDouble.MWPCAnodeSignal1 = double(InData.MWPCAnodeSignal1)/2000.0;
	  InDataDouble.MWPCAnodeSignal2 = double(InData.MWPCAnodeSignal2)/2000.0;
/*	  for (int i=0;i<12;i++){
	    InDataDouble.MWPCCathodeSignal[i] = double(InData.MWPCCathodeSignal[i])/8000.0; 
	  }*/
	  for(int i=0;i<3;i++){
	    if (i==2){
	     	InDataDouble.PosMWPCEntrance[i] = double(InData.PosMWPCEntrance[i])/100.0;  //mm
	      InDataDouble.PosMWPCExit[i] = double(InData.PosMWPCExit[i])/100.0;  //mm
	    }else{
	      InDataDouble.PosMWPCEntrance[i] = double(InData.PosMWPCEntrance[i])/1000.0;  //mm
	      InDataDouble.PosMWPCExit[i] = double(InData.PosMWPCExit[i])/1000.0;  //mm
	    }
	  }
	  InDataDouble.MWPC_X.AX1 = double(InData.MWPCCathodeSignal[0])/8000.0;
	  InDataDouble.MWPC_X.AX2 = double(InData.MWPCCathodeSignal[1])/8000.0;
	  InDataDouble.MWPC_X.AX3 = double(InData.MWPCCathodeSignal[2])/8000.0;
	  InDataDouble.MWPC_X.AX4 = double(InData.MWPCCathodeSignal[3])/8000.0;
	  InDataDouble.MWPC_X.AX5 = double(InData.MWPCCathodeSignal[4])/8000.0;
	  InDataDouble.MWPC_X.AX6 = double(InData.MWPCCathodeSignal[5])/8000.0;
	  InDataDouble.MWPC_Y.AY1 = double(InData.MWPCCathodeSignal[6])/8000.0;
	  InDataDouble.MWPC_Y.AY2 = double(InData.MWPCCathodeSignal[7])/8000.0;
	  InDataDouble.MWPC_Y.AY3 = double(InData.MWPCCathodeSignal[8])/8000.0;
	  InDataDouble.MWPC_Y.AY4 = double(InData.MWPCCathodeSignal[9])/8000.0;
	  InDataDouble.MWPC_Y.AY5 = double(InData.MWPCCathodeSignal[10])/8000.0;
	  InDataDouble.MWPC_Y.AY6 = double(InData.MWPCCathodeSignal[11])/8000.0;

	  for(int i=0;i<2;i++){
	    InDataDouble.MWPCHitPos[i] = double(InData.MWPCHitPos[i])/1000.0;
	  }
	  InDataDouble.ExitAngle_Be = double(InData.ExitAngle_Be)/100.0;
	  for(int i=0;i<3;i++){
	    InDataDouble.InitP[i] = double(InData.InitP[i])/1000.0;
	  }
	  for(int i=0;i<3;i++){
	    InDataDouble.DECPos[i] = double(InData.DECPos[i])/200.0;
	  }
	}
	
	if(ReadMode.compare("Triple")==0||ReadMode.compare("MCP")==0 || ReadMode.compare("He4")==0){
	  //Ion Info Convert to double
	  InDataDouble.TOF = double(InData.TOF)/1000.0;
	  for(int i=0;i<2;i++){
	    InDataDouble.HitPos[i] = double(InData.HitPos[i])/1000.0; //mm
	  }
	  InDataDouble.TOFII = double(InData.TOFII)/1000.0;
	  for(int i=0;i<2;i++){
	    InDataDouble.HitPosII[i] = double(InData.HitPosII[i])/1000.0; //mm
	  }
	  InDataDouble.HitAngle = double(InData.HitAngle)/100.0;
	  InDataDouble.IonEnergy = double(InData.IonEnergy)/1000.0; //keV
	  InDataDouble.EIonInit = double(InData.EIonInit)/1000.0;
	  for(int i=0;i<3;i++){
	    InDataDouble.V_Ion[i] = double(InData.V_Ion[i])/10000000.0;  //mm/ns
	  }
	}
	return 0;
}
/***********************************************************************************/
int Analyzer::OutputTreesToBin(vector<string> br,string fname){
  char filename[400];
  vector<double> var;
  vector<int> nleaves;
  string br_val; //branch name
  int n;
  ofstream fout;
  string purpose;  
  if (Purpose == 0){
    purpose = "Sim";
    OutTreesFilenameSim = fname;
  }else{
    purpose = "Exp";
    OutTreesFilenameExp = fname;
  }

  if (!OutTreesInitialized){
    cout<<"Error: Output trees are not initialized! Load before running OutputTreesToBin function."<<endl;
    return -1;
  }

  for (auto const& val: br){
    if(!OutTrees[0][0]->GetBranch(val.c_str())){
      cout<< "Error: No branch of name "<<val.c_str()<<" in OutTree."<<endl;
      return -1;
    }
  } 
  //make room for branch variables
  vector<int>::size_type n_br = br.size();
  vector<int>::size_type n_elements = 0;
  //nleaves.resize(n_br); //will store number of leaves in each branch
  for (auto const& val: br){
    TBranch *branch = OutTrees[0][0]->GetBranch(val.c_str());
    nleaves.push_back(branch->GetNleaves());
    n_elements += nleaves.back(); //last element
  }

    var.resize(n_elements);

    // TList *t_list = new TList;
    for(int i=0;i<HistDim1;i++){
      for(int j=0;j<HistDim2;j++){
        // t_list->Add(OutTrees[i][j]);

        n =OutTrees[i][j]->GetEntries();
        sprintf(filename,"%s/%s/OutTrees_%s_Cond%d,%d.bin",OUTPUT_TREE_DIRECTORY.c_str(),purpose.c_str(),fname.c_str(),i,j);
        fout.open(filename,ios::binary);

        if(fout.fail()){
          cout<< "Output file " <<filename<<" could not be opened for writing.\n";
          return -1;
        }

        cout<<"Writing OutTrees branches: ";
        for (auto const& val: br) cout<<val<<", ";
        cout<<" to file "<<filename<<"..."<<endl;

        //Set Tree branch address
        unsigned ii=0;
        for (unsigned k=0; k< n_br;k++){ 
          br_val = br.at(k);
          OutTrees[i][j]->SetBranchAddress(br_val.c_str(),&var[ii]);
          ii+=nleaves.at(k); //increment by number of leaves in each branch
          cout<<nleaves.at(k)<<"\t";
        }
        cout<<"\n";
        //Read in events and write to output file

        //write header entries
        fout.write((char*) &n_br,sizeof(int));//number of branches
        fout.write((char*) &nleaves.at(0),n_br*sizeof(int)); //number of leaves on each branch
        for (auto const& val: br){
          string towrite = val + "\n";
          fout.write(towrite.c_str(),towrite.length());
        }
        fout.write((char*) &n,sizeof(int));//number of entries

        for(int l=0;l<n;l++){
          OutTrees[i][j]->GetEntry(l);
          for (unsigned k=0; k< n_elements; k++){
            fout.write((char*) &var.at(k),sizeof(double)); 
          }
        }
        fout.close();
        OutTrees[i][j]->ResetBranchAddresses();  
      }
    }
    // TTree *t_merged = TTree::MergeTrees(t_list);
    // t_merged->SetName("EventTree_Combined");
    // t_merged->SetTitle("Combined Event Tree");
}
