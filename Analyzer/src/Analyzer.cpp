#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>
#include <stdint.h>
#include <time.h>
#include <sstream>
#include <iomanip>


//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
#include "TTree.h"
#include "TError.h"
//Analyzer includes
#include "Analyzer.h"


/***********************************************************************************/
//Function definations

using namespace std;

//Globals
int TOF_High = 500;
int TOF_Low = -200;
int TOF_N = 3500;

/***********************************************************************************/
//Constructor
Analyzer::Analyzer(char *SimDataDirect,char *ExpDataDirect,char *AnalyzerDirect,char *CalibrationDirect)
{
  time(&beginTime); //time at start of Analyzer
  time(&midTime);//time at start of constructor
  SIM_DATA_DIRECTORY = SimDataDirect;
  HISTOGRAM_DIRECTORY = SIM_DATA_DIRECTORY+"/../Histograms";
  SPECTRA_DIRECTORY = SIM_DATA_DIRECTORY+"/../Spectra";
  OUTPUT_TREE_DIRECTORY = SIM_DATA_DIRECTORY+"/../OutputTrees";
  EXP_DATA_DIRECTORY = ExpDataDirect;
  ANALYZER_DIRECTORY = AnalyzerDirect;
  CALIBRATION_DIRECTORY = CalibrationDirect;
  Purpose = 0;	//Default: simulation
  FitType = "MINUIT";
  nCond1 = 1;
  nCond2 = 1;
  nFileSim1 = 1;
  nFileSim2 = 1;
  nParameter1 = 1;
  nParameter2 = 1;
  nParameterExp1 = 1;
  nParameterExp2 = 1;
  nCondExp1 = 1;
  nCondExp2 = 1;
  nFileExp1 = 1;
  nFileExp2 = 1;
  floatTime = 0; //Fix fits by default
  HistDim1=1;	//Default hist dimentions are 1 and 1
  HistDim2=1;
  CombHistDim=1;
  nSlice=0;
  FileCondLnkd = false; //Exp file-condition correlation flag
  AutoT1T2 = false;//Auto T1T2 conditions
  //RootInputSim = true; //Read from root input file
  RootOutputSim = false; //Write output trees to root file
  RootOutputExp = false; //Write output trees to root file
  OutTreesFile = NULL;
  OutTrees = NULL;
  OutTreesInitialized = false;
  
  nExtPar=0;

  Configurations["MWPCRecScheme"] = "Full";

  //*************** Default TimeShift ***********
  TimeShift = 0.0;

  //**************** Load MCP Efficiency Map ******************

  MCPefficiencyMapSwitch = 0; //Default 0: MCP MAP OFF, 1: ON
/*
  ifstream MCP_Map_File("./EfficiencyMap.txt");

  double x_temp,  y_temp, ef_temp;
  int x_int, y_int;

  for(int i=0; i<8; i++){
    for(int j=0; j<8; j++){
      MCPefficiencyMap[i][j]=0.0;
    }
  }

  while(MCP_Map_File >> x_temp && MCP_Map_File >> y_temp && MCP_Map_File >> ef_temp){

    x_int = (int)floor(((x_temp/10.0)+7)/2.0);
    y_int = (int)floor(((y_temp/10.0)+7)/2.0);

    if(x_int>-1 && x_int <8 && y_int>-1 && y_int <8){
      MCPefficiencyMap[x_int][y_int] = 1.0-ef_temp;
    } else{
      cout << "WARNING: Dimensions of Efficiency MAP do not map dimensions of MAP Array" << endl;
      cout << "         Analysis Results May NOT Be Reliable" << endl;
    }
  }
*/
  //*************************************************************
  //Set allowed conditions first,and it is only set here.
  SetAllowedConditions();
  SetAllowedExpConditions();
  //Initialize parameters
  InitParameters();
  InitExpParameters();
  //Set scatt mode
  ScattMode = "Reject";
  //Set Histogram containers, and it is only set here
  InitSimHistBox();
  InitExpHistBox();
  //Initialize OutTrees to NULL. Needs to be done for DelHistograms in destructor
  //InitTreesToNull();

  //Initialize list of forbidden conditions (all by default)
  InitDefaultExpForbiddenCondFunctions();
  
  //Charge state 3 switch
  Charge3Switch = false;
  
  //Clean Systematic Maps first, the re-initialization is included int his function
  ResetSystematics();
  ResetExpConditions();

  Status = 0;
  ExpStatus = 0;
  CombStatus = 0;

  Std_First = NULL;
  Std_Second = NULL;

  SysDataPtr1=NULL;
  SysDataPtr2=NULL;

  //SpectrumFileName
  SpectrumFileName = SPECTRA_DIRECTORY + "/Spectra_0000";
  SpectrumStatus = 0;

  //Exp file prefix
  ExpFilePrefix = "He6Run";

  //Drawing Options
  GridOn = true;
  TitleOn = true;
  AxisTitleOn = true;
  Option2DHist = "COLZ";
  Option2DGraph = "";

  //Other Exp Configureations
  SetMode("Triple");
  BkgSeparation = false;  
  BkgRatio = 0.1;      
  BkgTime = 700.0;	//ms      

  PushBeamVeto = false;
  PushPeriod = 1000.0/3.0;	//ms
  PushLength = 30.0;		//ms

  LaserSwitch = false;
  LaserStatus = true;
  SwitchPeriod = 0.1;		//ms
  SwitchStart = 58.35;		//ms
  N_cycle = 1980;
  WaitTime = 0.0000; //ms

  //Calibration Data Init
/*  for (int i=0;i<6;i++){
    AmpCalX[i] = 1.0;
    AmpCalY[i] = 1.0;
  }
  AmpCalA[0] = AmpCalA[1] = 34000.0;*/
  //Load Calibration Data
  //Simulation
  SimCalSwitches["Scint"]=false;
  SimCalSwitches["MWPCE"]=false;
  SimCalSwitches["MWPCPos"]=false;
  SimCalSwitches["MWPCGainMap"]=false;
  SimCalSwitches["MWPCEffMap"]=false;
  SimCalSwitches["MCPEffMap"]=false;
  SimCalIDs["Scint"]=0;
  SimCalIDs["MWPCE"]=0;
  SimCalIDs["MWPCPos"]=0;
  SimCalIDs["MWPCGainMap"]=0;
  SimCalIDs["MWPCEffMap"]=0;
  SimCalIDs["MCPEffMap"]=0;
  LoadSimCalibrationData("Scint",false);
  LoadSimCalibrationData("MWPCE",false);
  LoadSimCalibrationData("MWPCPos",false);
  LoadSimCalibrationData("MWPCGainMap",false);
  LoadSimCalibrationData("MWPCEffMap",false);
  LoadSimCalibrationData("MCPEffMap",false);
  //LoadSimCalibrationData();
  //Experiment
  Calibrating = false;
  ExpCalSwitches["Scint"]=false;
  ExpCalSwitches["ScintGainMap"]=false;
  ExpCalSwitches["MWPCE"]=false;
  ExpCalSwitches["MWPCPos"]=false;
  ExpCalSwitches["MWPCGainMap"]=false;
  ExpCalSwitches["MWPCEffMap"]=false;
  ExpCalSwitches["Amp"]=false;
  ExpCalSwitches["MCPPos"]=false;
  ExpCalSwitches["T0Corr"]=false;
  ExpCalSwitches["T0CorrQMCP"]=false;  

  ExpCalIDs["Scint"]=0;
  ExpCalIDs["ScintGainMap"]=0;
  ExpCalIDs["MWPCE"]=0;
  ExpCalIDs["MWPCPos"]=0;
  ExpCalIDs["MWPCGainMap"]=0;
  ExpCalIDs["MWPCEffMap"]=0;
  ExpCalIDs["Amp"]=0;
  ExpCalIDs["MCPPos"]=0;
  ExpCalIDs["T0Corr"]=0;
  ExpCalIDs["T0CorrQMCP"]=0;
  t0cell = NULL; 
  t0vsQMCPpoly1 = NULL;

  LoadExpCalibrationData("Scint",false);
  LoadExpCalibrationData("ScintGainMap",false);
  LoadExpCalibrationData("MWPCE",false);
  SetExpMCPCalMode("Global");
  SetT0CorrMode("Global");
  SetMWPCPosCalMode("Micro");
  SetMWPCKeepLessTrigger(0);
  LoadExpCalibrationData("MWPCPos",false);
  LoadExpCalibrationData("MWPCGainMap",false);
  LoadExpCalibrationData("MWPCEffMap",false);
  LoadExpCalibrationData("Amp",false);
  LoadExpCalibrationData("MCPPos",false);

  LED_Cal = 1.0;       
  LEDScint_Cal = 1.0;  
  LEDStablizer = false; 
  LEDPeriod = 300;   
  LEDGainCorrection.clear();
  LEDGainCorrectionSize = 0;
  //Some Hard coded calibration parameters
  MWPCRawCalA = 14173.0;
  MWPCRawCalC = 9076.0;
  //ScintRawCal = 0.00225*1.5;
  ScintRawCal = 0.00225*1.5*1.5;

  //External Bkg handling
  ExtBkgStatus = 0;
  ExtBkgHistBox["All"]=NULL;
  ExtBkgHistBox["Q_Cut_ON"]=NULL;
  ExtBkgHistBox["QC_Cut_ON"]=NULL;
  ExtBkgHistBox["Scint_EA"]=NULL;
  ExtBkgHistBox["CosThetaENu"]=NULL;
  ExtBkgHistBox["EIon1"]=NULL;
  ExtBkgHistBox["EIon2"]=NULL;

  //Open Log stream
  AnalysisLog.open("AnalysisLog.log",ios::app);
  AnalysisLog << "********************************************************************************"<<endl<<endl;
  time_t startTime = time(0);
  struct tm tstruct;
  tstruct = *localtime(&startTime);
  char buf[80];
  strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
  AnalysisLog <<"Analysis starts at " << buf << endl;

}

/***********************************************************************************/
//Destructor
Analyzer::~Analyzer()
{
  //Delete histograms
  if (Status!=0)DelHistograms();
  DelStdHistograms();
  if (ExpStatus!=0)DelExpHistograms();
  DelCombExpHistograms();  
  DelBkgHistogram();
  
  //Delete output trees
  DelOutTrees();

  CleanupCalibrationStructures();
  time_t stopTime = time(0);
  struct tm tstruct;
  tstruct = *localtime(&stopTime);
  char buf[80];
  strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
  AnalysisLog <<"Analysis stops at " << buf << endl<<endl;
  AnalysisLog << "********************************************************************************"<<endl<<endl;
  AnalysisLog.close();

  time(&endTime);
  timeSec = difftime(endTime, beginTime);  
  printf("\n\n\nAnalyzer Runtime: %.3f s \n",timeSec);
  timeMin = floor(timeSec/60);
  printf("Analyzer Runtime: %02.0f:%02.0f \n\n\n",timeMin,fmod(timeSec,60));
}

/***********************************************************************************/
void Analyzer::SetPurpose(int val1, int val2){
  Purpose = val1;	//Set purpose: simulation or experiment
  Purpose2 = val2;	//Set purpose: decay or calibration
}
/***********************************************************************************/	
void Analyzer::ZeroCounters(){
  for(int i=0;i<N_OF_HIST;i++){
    for (int j=0;j<N_OF_HIST;j++){
      Event_Count[i][j] = 0;
      BkgEvent_Count[i][j] = 0;
      //Trigger_Count[i][j].stuff = 0; ????
      Hit_Count[i][j].Scint_Hit = 0;
      Hit_Count[i][j].MWPC_Hit = 0;
      Hit_Count[i][j].Window_Hit = 0;
      Hit_Count[i][j].Collimator_Hit = 0;
      Hit_Count[i][j].Electrode_Hit = 0;
      Hit_Count[i][j].Wall_Hit = 0;
      Hit_Count[i][j].MCP_Hit = 0;
      Hit_Count[i][j].BackScattering = 0;
    }
  }
}
/***********************************************************************************/	
int Analyzer::ProcessInputSim(int FileDim, int ConditionDim, int ParameterDim)
{
  if (FileDim+ConditionDim+ParameterDim>2){
    cout << "This program can only handle up to 2-dimension study of systematic effects."<<endl;
    return -1;
  }
  double timeRead, timeProcess, timeFill;
//  ifstream filein;	//input stream
  TFile* tfilein = NULL; //pointer to input root file
  InDataStruct InData;	//input data unit
  InDataStructDouble InDataDouble;	//input data double format
  SimRecDataStruct SimRecData;	//Reconstructed data
  TTree * InTree = NULL; //input tree pointer
  double PosMWPCCenter[3];//Position when the beta particle passes the MWPC center plane
  char filename[500];
  string basename;
  int returnVal = 0;

  if(Purpose!=0){
    cout <<"The purpose is not for simulation analysis.\n";
    return -1;
  }

  //Zero counters
  ZeroCounters();  
  cout << "Initializing... "<<endl;
  //Check Condition Settings
  int returnval;
  returnval = CheckConditionSettings();
  if (returnval!=0)
  {
    cout << "Condition setting problem!\n";
    return -1;
  }
  //Construct the parameter list
  returnval = ConstructParameterList();
  if (returnval!=0)
  {
    cout << "Parameter construction failed!\n";
    return -1;
  }

  //Loop Control
  int FileLim1;
  int FileLim2;
  int ConditionLim1;
  int ConditionLim2;
  int ParameterLim1;
  int ParameterLim2;
  int iFile;
  int jFile;
  int iCond;
  int jCond;
  int iPara;
  int jPara;
  int *Index1;
  int *Index2;
  int FileParDim = FileDim+ParameterDim;
  int NonLoopIndex = 0;

  //Treat file and parameter first
  if (FileParDim==2 && ConditionDim==0){//Two file/Par loops and zero condition loop
    if (FileDim==1 && ParameterDim==1){//One file loop and one Par loop
      FileLim1 = nFileSim1;
      FileLim2 = 1;
      ParameterLim1 = nParameter1;
      ParameterLim2 = 1;
      Index1 = &iFile;
      Index2 = &iPara;
      HistDim1 = nFileSim1;
      HistDim2 = nParameter1;
    }else if (FileDim==2 && ParameterDim==0){//Two file loops and zero Par loop
      FileLim1 = nFileSim1;
      FileLim2 = nFileSim2;
      ParameterLim1 = 1;
      ParameterLim2 = 1;
      Index1 = &iFile;
      Index2 = &jFile;
      HistDim1 = nFileSim1;
      HistDim2 = nFileSim2;
    }else if (FileDim==0 && ParameterDim==2){//Zero file loop and two par loops
      FileLim1 = 1;
      FileLim2 = 1;
      ParameterLim1 = nParameter1;
      ParameterLim2 = nParameter2;
      Index1 = &iPara;
      Index2 = &jPara;
      HistDim1 = nParameter1;
      HistDim2 = nParameter2;
    }else{
      cout << "Cannot handle dimension FileDim=" << FileDim <<" ParameterDim="<<ParameterDim <<" and ConditionDim=" << ConditionDim<< ".\n";
      return -1;
    } 
    ConditionLim1 = 1;
    ConditionLim2 = 1;
  }else if (FileParDim==1){
    if (FileDim==1 && ParameterDim==0){//One file loop and zero condition loop
      FileLim1 = nFileSim1;
      FileLim2 = 1;
      ParameterLim1 = 1;
      ParameterLim2 = 1;
      Index1 = &iFile;
      HistDim1 = nFileSim1;
    }else if (FileDim==0 && ParameterDim==1){//Zero file loop and one condition loop
      FileLim1 = 1;
      FileLim2 = 1;
      ParameterLim1 = nParameter1;
      ParameterLim2 = 1;
      Index1 = &iPara;
      HistDim1 = nParameter1;
    }else{
      cout << "Cannot handle dimension FileDim=" << FileDim <<" ParameterDim="<<ParameterDim <<" and ConditionDim=" << ConditionDim<< ".\n";
      return -1;
    } 
    if (ConditionDim==1){
      ConditionLim1 = nCond1;
      ConditionLim2 = 1;
      Index2 = &iCond;
      HistDim2 = nCond1;
    }else if (ConditionDim==0){
      ConditionLim1 = 1;
      ConditionLim2 = 1;
      Index2 = &NonLoopIndex;
      HistDim2 = 1;
    }else{
      cout << "Cannot handle dimension FileDim=" << FileDim <<" ParameterDim="<<ParameterDim <<" and ConditionDim=" << ConditionDim<< ".\n";
      return -1;
    } 
  }else if (FileParDim==0){
    FileLim1 = 1;
    FileLim2 = 1;
    ParameterLim1 = 1;
    ParameterLim2 = 1;
    if (ConditionDim==2){
      ConditionLim1 = nCond1;
      ConditionLim2 = nCond2;
      Index1 = &iCond;
      Index2 = &jCond;
      HistDim1 = nCond1;
      HistDim2 = nCond2;
    }else if(ConditionDim==1){
      ConditionLim1 = nCond1;
      ConditionLim2 = 1;
      Index1 = &iCond;
      Index2 = &NonLoopIndex;
      HistDim1 = nCond1;
      HistDim2 = 1;
    }else if(ConditionDim==0){
      ConditionLim1 = 1;
      ConditionLim2 = 1;
      Index1 = &NonLoopIndex;
      Index2 = &NonLoopIndex;
      HistDim1 = 1;
      HistDim2 = 1;
    }else{
      cout << "Cannot handle dimension FileDim=" << FileDim <<" ParameterDim="<<ParameterDim <<" and ConditionDim=" << ConditionDim<< ".\n";
      return -1;
    } 
  }else{
    cout << "Cannot handle dimension FileDim=" << FileDim <<" ParameterDim="<<ParameterDim <<" and ConditionDim=" << ConditionDim<< ".\n";
    return -1;
  } 

	LsEnabledCondFunc();
	
  //Log
  AnalysisLog<<"**********Simulation input processing info*********"<<endl;
  AnalysisLog<<"Histogram dimensions: " <<HistDim1<<" X "<<HistDim2<<endl;
  AnalysisLog<<"File dimension: " <<FileDim<<endl;
  LogSimFileIDList();
  AnalysisLog<<"Condition dimension: " <<ConditionDim<<endl;
  AnalysisLog<<"Parameter dimension: " <<ParameterDim<<endl;
  LogConditionList();

  //Renew histograms if Status is loaded
  if (Status == 0){
    cout<<"Initializing histograms."<<endl;
    InitHistograms();

  }
  else {
    cout<<"Histograms already loaded. Deleting and reinitialzing."<<endl;
    RenewHistograms();
  }
  

  
  //Initialize OutTrees
  if (RootOutputSim){
    cout<<"Initializing root output trees."<<endl;
    InitOutTrees();//needs to be done for DelOutTrees to work
    SetOutTreesAddress(InData,InDataDouble,SimRecData);

  }
  else {
    //cout<<"Histograms already loaded. Deleting and reinitialzing."<<endl;
    //RenewHistograms();
  }

  /*Processing**********************************************************/
  cout << "Processing... "<<endl;
  if(ReadMode.compare("MCP")==0 || ReadMode.compare("He4")==0){basename = "IonOnly_EventFile";}
  else if(ReadMode.compare("Beta")==0 || ReadMode.compare("MWPC")==0){basename = "BetaOnly_EventFile";}
  else{basename = "EventFile";}
  int FileID;
  for (iFile=0;iFile<FileLim1;iFile++){  //File loop i starts
    for (jFile=0;jFile<FileLim2;jFile++){//File loop j starts
      FileID = FileIDListSim[iFile][jFile];
      if(SetupInputFile(FileID,tfilein,InTree)==-1) return -1;
      SetInTreeAddress(InTree, InData);
      sprintf(filename,"%s/%s%09d.root",SIM_DATA_DIRECTORY.c_str(),basename.c_str(),FileIDListSim[iFile][jFile]);      
      //Reading loop
      int iEvent = 0;
      while(1){	
	      time(&iTime0);
	      returnVal = InTree->GetEntry(iEvent);
	      //else returnVal = ReadInSimEvent(filein,InData);//Read file
	      if (returnVal==0) break; //eof
	      else if(returnVal==-1) return -1;
//	      for (int j=0; j<12; j++) {
//				    cout << InData.MWPCCathodeSignal[j]<< " ";
//		    }
//			  cout << endl;
	      ConvertDataToDouble(InData,InDataDouble); 
	      time(&iTime1);
	      timeRead += difftime(iTime1, iTime0);
	      //Reconstruct events
	      for (iPara=0;iPara<ParameterLim1;iPara++){  //Para loop i starts
	        for (jPara=0;jPara<ParameterLim2;jPara++){//Para loop j starts
            time(&iTime0);
            ReconstructSim(iPara,jPara,SimRecData,InDataDouble,InData);
            //Process Conditions first
            ProcessConditions(SimRecData, iFile);
            time(&iTime1);
            timeProcess += difftime(iTime1, iTime0);
            //Loop over conditions
            time(&iTime0);
            for (iCond=0;iCond<ConditionLim1;iCond++){
              for (jCond=0;jCond<ConditionLim2;jCond++){
              //Fill histograms
                FillHistograms(*Index1,*Index2,Conditions[iCond][jCond],SimRecData);
                if(RootOutputSim) FillOutTrees(*Index1,*Index2,Conditions[iCond][jCond]);
                //FillTrees(*Index1,*Index2,Conditions[iCond][jCond]);
                if (Conditions[iCond][jCond]){
                  Event_Count[*Index1][*Index2] += 1;
                  //Count hits
                  if(((InData.Hits>>0) & 0x1)==1) Hit_Count[*Index1][*Index2].Scint_Hit += 1;
                  if(((InData.Hits>>1) & 0x1)==1) Hit_Count[*Index1][*Index2].MWPC_Hit += 1;
                  if(((InData.Hits>>2) & 0x1)==1) Hit_Count[*Index1][*Index2].Window_Hit += 1;
                  if(((InData.Hits>>3) & 0x1)==1) Hit_Count[*Index1][*Index2].Collimator_Hit += 1;
                  if(((InData.Hits>>4) & 0x1)==1) Hit_Count[*Index1][*Index2].Electrode_Hit += 1;
                  if(((InData.Hits>>5) & 0x1)==1) Hit_Count[*Index1][*Index2].Wall_Hit += 1;
                  if(((InData.Hits>>6) & 0x1)==1) Hit_Count[*Index1][*Index2].MCP_Hit += 1;
                  if(((InData.Hits>>7) & 0x1)==1) Hit_Count[*Index1][*Index2].BackScattering += 1;
                }
              }
            }
            time(&iTime1);
            timeFill += difftime(iTime1, iTime0);
          }       
        }
        iEvent++;
      }//Event loop ends
      time(&endTime);
      timeSec = difftime(endTime, midTime);  
      //printf("\n\n\nPostProcessing Runtime: %.3f s \n",timeSec);
      timeMin = floor(timeSec/60);
      printf("PostProcessing Cummulative Runtime: %02.0f:%02.0f \n",timeMin,fmod(timeSec,60));
      printf("Total read time: %.3f s \n",timeRead);
      printf("Total process time: %.3f s \n",timeProcess);
      printf("Total fill time: %.3f s \n",timeFill);
      //  Differ->Add(CoinScint,-1.0);

      //Output Hit information
      for (iCond=0;iCond<ConditionLim1;iCond++){
        for (jCond=0;jCond<ConditionLim2;jCond++){
          cout << "File "<<filename<<" Under Condition "<< iCond << " " << jCond <<":\n";
          cout << "Number of Hits on Scintillator = "<<Hit_Count[*Index1][*Index2].Scint_Hit<<" , "<<100*double(Hit_Count[*Index1][*Index2].Scint_Hit)/double(Event_Count[*Index1][*Index2])<<"%"<<" of detected events.\n";
          cout << "Number of Hits on MWPC = "<<Hit_Count[*Index1][*Index2].MWPC_Hit<<" , "<<100*double(Hit_Count[*Index1][*Index2].MWPC_Hit)/double(Event_Count[*Index1][*Index2])<<"%"<<" of detected events.\n";
          cout << "Number of Hits on Window = "<<Hit_Count[*Index1][*Index2].Window_Hit<<" , "<<100*double(Hit_Count[*Index1][*Index2].Window_Hit)/double(Event_Count[*Index1][*Index2])<<"%"<<" of detected events.\n";
          cout << "Number of Hits on Collimator = "<<Hit_Count[*Index1][*Index2].Collimator_Hit<<" , "<<100*double(Hit_Count[*Index1][*Index2].Collimator_Hit)/double(Event_Count[*Index1][*Index2])<<"%"<<" of detected events.\n";
          cout << "Number of Hits on Electrode = "<<Hit_Count[*Index1][*Index2].Electrode_Hit<<" , "<<100*double(Hit_Count[*Index1][*Index2].Electrode_Hit)/double(Event_Count[*Index1][*Index2])<<"%"<<" of detected events.\n";
          cout << "Number of Hits on Wall = "<<Hit_Count[*Index1][*Index2].Wall_Hit<<" , "<<100*double(Hit_Count[*Index1][*Index2].Wall_Hit)/double(Event_Count[*Index1][*Index2])<<"%"<<" of detected events.\n";
          cout << "Number of Hits on MCP = "<<Hit_Count[*Index1][*Index2].MCP_Hit<<" , "<<100*double(Hit_Count[*Index1][*Index2].MCP_Hit)/double(Event_Count[*Index1][*Index2])<<"%"<<" of detected events.\n";
          cout << "Number of BackScattering = "<<Hit_Count[*Index1][*Index2].BackScattering<<" , "<<100*double(Hit_Count[*Index1][*Index2].BackScattering)/double(Event_Count[*Index1][*Index2])<<"%"<<" of detected events.\n";
        }
      }
      //if (filein.is_open()) filein.close();
      if (tfilein!=NULL){
        tfilein->Close();
        delete tfilein;
        tfilein =NULL;
      }
      
    }//File loop ends j
  }//File loop ends i
  if(RootOutputSim) WriteOutTrees();
  //Set status to loaded
  Status = 2;
  cout <<"ProcessInputSim: Processing complete!\n";
  return 0;
}

/***********************************************************************************/



/***********************************************************************************/
int Analyzer::ProcessInputExp(int FileDim, int ConditionDim, int ParameterDim)
{
  //////////////////////////////////////////////////////////////////
  //This part of the code does not make sense
  //Just to prevent a damn segmentation fault, no one knows why
  //Something is wrong with root!
  //TH1F *dummy = new TH1F("dummy", "dummy", 10, 0, 1);
  //TFile *dummy_file = new TFile("dummy.root","recreate");
  //TTree *dummytree = new TTree("dummytree","dummytree",99);
  //Later version of root may not have this problem. 
  //Remove this if everyone's root is up-to-date
  //////////////////////////////////////////////////////////////////

  /*  string inputFileName = EXP_DATA_DIRECTORY+"/"+ExpInFileName+".root";
  //Open Input ROOT File
  TH1* h = NULL;
  TFile* Input = new TFile(inputFileName.c_str(),"READ");
  gROOT->cd();
  h = (TH1*)Input->Get("He6_TOF_Hist");
  //  Exp_TOFHist[0] = (TH1*)h->Clone();
  Input->Close();*/
  char filename[500];
  ExpDataStruct ExpData;      //Data struct for exp data reading
  RecDataStruct RecData;      //Reconstructed data struct
  SetExpTime_rel(RecData); //sets chooses time for push beam/laser veto according to ReadMode
  TFile* tfilein = NULL; //pointer to input root file
  TTree * InTree = NULL; //input tree pointer

  if(Purpose!=1){
    cout <<"The purpose is not for experiment analysis.\n";
    return -1;
  }

  //Zero counters  
  ZeroCounters();
  //May add quality monitor, to see which signal is missing

  cout << "Initializing... "<<endl;
  //Check Condition Settings
  int returnval;
  returnval = CheckExpConditionSettings();
  if (returnval!=0)
  {
    cout << "Exp-Condition setting problem!\n";
    return -1;
  }
  //Construct the parameter list
  returnval = ConstructExpParameterList();
  if (returnval!=0)
  {
    cout << "Parameter construction failed!\n";
    return -1;
  }

  //Loop Control
  int FileLim1;
  int FileLim2;
  int ConditionLim1;
  int ConditionLim2;
  int ParameterLim1;
  int ParameterLim2;
  int iFile;
  int jFile;
  int iCond;
  int jCond;
  int iPara;
  int jPara;
  int *Index1;
  int *Index2;
  int FileParDim = FileDim+ParameterDim;
  int NonLoopIndex = 0;

  //Treat file and parameter first
  if (FileParDim==2 && ConditionDim==0){//Two file/Par loops and zero condition loop
    if (FileDim==1 && ParameterDim==1){//One file loop and one Par loop
      FileLim1 = nFileExp1;
      FileLim2 = 1;
      ParameterLim1 = nParameterExp1;
      ParameterLim2 = 1;
      Index1 = &iFile;
      Index2 = &iPara;
      HistDim1 = nFileExp1;
      HistDim2 = nParameterExp1;
    }else if (FileDim==2 && ParameterDim==0){//Two file loops and zero Par loop
      FileLim1 = nFileExp1;
      FileLim2 = nFileExp2;
      ParameterLim1 = 1;
      ParameterLim2 = 1;
      Index1 = &iFile;
      Index2 = &jFile;
      HistDim1 = nFileExp1;
      HistDim2 = nFileExp2;
    }else if (FileDim==0 && ParameterDim==2){//Zero file loop and two par loops
      FileLim1 = 1;
      FileLim2 = 1;
      ParameterLim1 = nParameterExp1;
      ParameterLim2 = nParameterExp2;
      Index1 = &iPara;
      Index2 = &jPara;
      HistDim1 = nParameterExp1;
      HistDim2 = nParameterExp2;
    }else{
      cout << "Cannot handle dimension FileDim=" << FileDim <<" ParameterDim="<<ParameterDim <<" and ConditionDim=" << ConditionDim<< ".\n";
      return -1;
    } 
    ConditionLim1 = 1;
    ConditionLim2 = 1;
  }else if (FileParDim==1){
    if (FileDim==1 && ParameterDim==0){//One file loop and zero condition loop
      FileLim1 = nFileExp1;
      FileLim2 = 1;
      ParameterLim1 = 1;
      ParameterLim2 = 1;
      Index1 = &iFile;
      HistDim1 = nFileExp1;
    }else if (FileDim==0 && ParameterDim==1){//Zero file loop and one condition loop
      FileLim1 = 1;
      FileLim2 = 1;
      ParameterLim1 = nParameterExp1;
      ParameterLim2 = 1;
      Index1 = &iPara;
      HistDim1 = nParameterExp1;
    }else{
      cout << "Cannot handle dimension FileDim=" << FileDim <<" ParameterDim="<<ParameterDim <<" and ConditionDim=" << ConditionDim<< ".\n";
      return -1;
    } 
    if (ConditionDim==1){
      ConditionLim1 = nCondExp1;
      ConditionLim2 = 1;
      Index2 = &iCond;
      HistDim2 = nCondExp1;
    }else if (ConditionDim==0){
      ConditionLim1 = 1;
      ConditionLim2 = 1;
      Index2 = &NonLoopIndex;
      HistDim2 = 1;
    }else{
      cout << "Cannot handle dimension FileDim=" << FileDim <<" ParameterDim="<<ParameterDim <<" and ConditionDim=" << ConditionDim<< ".\n";
      return -1;
    } 
  }else if (FileParDim==0){
    FileLim1 = 1;
    FileLim2 = 1;
    ParameterLim1 = 1;
    ParameterLim2 = 1;
    if (ConditionDim==2){
      ConditionLim1 = nCondExp1;
      ConditionLim2 = nCondExp2;
      Index1 = &iCond;
      Index2 = &jCond;
      HistDim1 = nCondExp1;
      HistDim2 = nCondExp2;
    }else if(ConditionDim==1){
      ConditionLim1 = nCondExp1;
      ConditionLim2 = 1;
      Index1 = &iCond;
      Index2 = &NonLoopIndex;
      HistDim1 = nCondExp1;
      HistDim2 = 1;
    }else if(ConditionDim==0){
      ConditionLim1 = 1;
      ConditionLim2 = 1;
      Index1 = &NonLoopIndex;
      Index2 = &NonLoopIndex;
      HistDim1 = 1;
      HistDim2 = 1;
    }else{
      cout << "Cannot handle dimension FileDim=" << FileDim <<" ParameterDim="<<ParameterDim <<" and ConditionDim=" << ConditionDim<< ".\n";
      return -1;
    } 
  }else{
    cout << "Cannot handle dimension FileDim=" << FileDim <<" ParameterDim="<<ParameterDim <<" and ConditionDim=" << ConditionDim<< ".\n";
    return -1;
  } 

	LsEnabledExpCondFunc();
	
  //Log
  AnalysisLog<<"**********Experiment input processing info*********"<<endl;
  AnalysisLog<<"Exp-File prefix: "<<ExpFilePrefix<<endl;
  AnalysisLog<<"Histogram dimensions: " <<HistDim1<<" X "<<HistDim2<<endl;
  AnalysisLog<<"File dimension: " <<FileDim<<endl;
  LogExpFileIDList();
  AnalysisLog<<"Condition dimension: " <<ConditionDim<<endl;
  LogExpConditionList();

  //Renew histograms if Status is loaded
  if (ExpStatus == 0){
    InitExpHistograms(); 
  }
  else if (ExpStatus == 2){
    RenewExpHistograms();
    RenewCombExpHistograms();
  }

  //Initialize OutTrees
  if (RootOutputExp){
    cout<<"Initializing root output trees."<<endl;
    InitOutTrees();//needs to be done for DelOutTrees to work
    SetOutTreesAddress(RecData);

  }
  double Tref;
  double TotFGTime = 0;
  double TotBKGTime = 0;

  cout << "Calibration Data Loaded."<<endl;
  cout << "Processing Experiment Input... "<<endl;
  for (iFile=0;iFile<FileLim1;iFile++){  //File loop i starts
    for (jFile=0;jFile<FileLim2;jFile++){//File loop j starts
      double StartTime;
      double StopTime;
      sprintf(filename,"%s/%s_%04d.root",EXP_DATA_DIRECTORY.c_str(),ExpFilePrefix.c_str(),FileIDListExp[iFile][jFile]);
      if(OpenExpInputFile(filename,tfilein)==-1) return -1;
      SetExpInTreeAddress(tfilein,InTree,ExpData);
      cout << "Reading file "<<filename<<endl;
      //Generate LED gain correction
      GenerateLEDStablizer(iFile,jFile);

      //Reading loops
      //Check Mode

	    int NEvents = InTree->GetEntries();
	    for(int i=0;i<NEvents;i++){
	      ClearTempData(ExpData,RecData);  
	      //Read Tree
	      if(InTree->GetEntry(i)==-1) return -1;
	      if (i==0) StartTime=ExpData.GroupTime;
	      if (i==NEvents-1) StopTime=ExpData.GroupTime;
	      //Apply calibration and reconstruct data
	      for (iPara=0;iPara<ParameterLim1;iPara++){  //Para loop i starts
	        for (jPara=0;jPara<ParameterLim2;jPara++){//Para loop j starts
	          Reconstruct(iPara,jPara,RecData,ExpData);
	          //Process Conditions first
	          ProcessExpConditions(RecData, iFile);//<--push beam veto and laser veto applied here
	          //Loop over conditions
	          for (iCond=0;iCond<ConditionLim1;iCond++){
		          for (jCond=0;jCond<ConditionLim2;jCond++){
		            //Fill histograms
		            FillExpHistograms(*Index1,*Index2,Conditions[iCond][jCond],*RecData.Time_rel,RecData);//<--counts tallied here
		            if(RootOutputExp) FillOutTrees(*Index1,*Index2,Conditions[iCond][jCond]);//<--does not include bkg separation
		          }//ends jCond
	          }//end iCond
	        }//ends jPara
	      }//end iPara
      }//Event loop ends
      //double FGTime = (StopTime-StartTime)*(TotalTime-BkgTime)/TotalTime; 
      //double BKGTime = (StopTime-StartTime)*BkgTime/TotalTime;
      //TotFGTime +=  FGTime;
      //TotBKGTime +=  BKGTime;
      sprintf(filename,"%s_%04d.root",ExpFilePrefix.c_str(),FileIDListExp[iFile][jFile]);
      ostringstream stm;
      stm <<filename<<" "<<"Total Events: "<< Event_Count[*Index1][*Index2]<<" Total Runtime: "<< (StopTime-StartTime)<<" [s] Average Rate: "<<Event_Count[*Index1][*Index2]/(StopTime-StartTime);
//      stm <<filename<<" "<<"FGEvents "<< Event_Count[*Index1][*Index2]<<" "<<"BKGEvents "<< BkgEvent_Count[*Index1][*Index2]<<" FGRate " << Event_Count[*Index1][*Index2]/FGTime <<" BKGRate " << BkgEvent_Count[*Index1][*Index2]/BKGTime;
      cout<< stm.str() <<endl;
      AnalysisLog << stm.str() <<endl;
      if (tfilein!=NULL){
        tfilein->Close();
        delete tfilein;
        tfilein =NULL;
      }
    }//File loop ends
  }
  cout<<"TOTAL FGTime: "<<TotFGTime<<" [s]"<<endl;
  cout<<"TOTAL BKGTime: "<<TotBKGTime<<" [s]"<<endl;
  if(RootOutputExp) WriteOutTrees();
  //Set status to loaded
  ExpStatus = 2;
  cout <<"ProcessInputExp: Processing complete!\n";
  //  delete dummy;
  //  delete dummy_file;
  //  delete dummytree;
  return 0;
}

/***********************************************************************************/
int Analyzer::ConfigBackground(string Option,double bkgTime, double totTime, double ratio)
{
  if (Option.compare("ON")==0){
    BkgSeparation = true;
    cout <<"Background separation ON."<<endl;
  }else if (Option.compare("OFF")==0){
    BkgSeparation = false;
    cout <<"Background separation OFF."<<endl;
  }else{
    cout << "Incorrect option: "<<Option<<endl;
    cout  <<"By default set to OFF.\n";
    BkgSeparation = false;
  }
  BkgTime = bkgTime;
  BkgRatio = ratio;
  TotalTime = totTime;
  BkgNorm = 0.0;
  return 0;
}

/***********************************************************************************/
int Analyzer::ConfigPushBeamVeto(string Option,double Period, double Length)
{
  if (Option.compare("ON")==0){
    PushBeamVeto = true;
  }else if (Option.compare("OFF")==0){
    PushBeamVeto = false;
  }else{
    cout << "Incorrect option: "<<Option<<endl;
    cout  <<"By default set to OFF.\n";
    PushBeamVeto = false;
  }
  PushPeriod = Period;
  PushLength = Length;
  return 0;
}

/***********************************************************************************/
bool Analyzer::CheckPushBeamVeto(double Time) //returns true if vetoed
{
  bool veto = (Time - int(Time/PushPeriod)*PushPeriod)<PushLength;
  if (PushBeamVeto && veto) return true;
  else return false;
}
/***********************************************************************************/
int Analyzer::ConfigLaserSwitch(string StatusSelect, double PushBeamPeriod, double LaserPeriod, int Ncycle, double TWait)
{
 if(StatusSelect.compare("ON")==0){
   LaserStatus = true;
 }else if(StatusSelect.compare("OFF")==0){
   LaserStatus = false;
 }else{
   cout << "Incorrect option: " << StatusSelect << endl;
   cout << "By default set to ON.\n";
   LaserStatus = true;
 }

 PushPeriod = PushBeamPeriod;
 SwitchPeriod = LaserPeriod;
 //SwitchStart = Tstart;//this will be overwritten by the condition ExpCond_LaserSwitch if it is initiialized or set
 N_cycle = Ncycle;
 WaitTime = TWait;

}
/*int Analyzer::ConfigLaserSwitch(string Option1, string Option2, double PushBeamPeriod, double LaserPeriod, double Tstart, int Ncycle)
{
 if(Option1.compare("ON")==0){
   LaserSwitch = true;
 }else if(Option1.compare("OFF")==0){
   LaserSwitch = false;
 }else{
   cout << "Incorrect option: " << Option1 << endl;
   cout << "By default set to OFF.\n";
   LaserSwitch = false;
 }

 if(Option2.compare("ON")==0){
   LaserStatus = true;
 }else if(Option2.compare("OFF")==0){
   LaserStatus = false;
 }else{
   cout << "Incorrect option: " << Option2 << endl;
   cout << "By default set to ON.\n";
   LaserStatus = true;
 }

 PushPeriod = PushBeamPeriod;
 SwitchPeriod = LaserPeriod;
 SwitchStart = Tstart;//this will be overwritten by the condition ExpCond_LaserSwitch if it is initiialized or set
 N_cycle = Ncycle;

}
bool Analyzer::CheckLaserSwitchVeto(double Time) //returns true if vetoed
{
  bool laser_ON = ((Time<SwitchStart) && (Time>(SwitchStart+N_cycle*SwitchPeriod-PushPeriod))) || (Time>SwitchStart && fmod((Time-SwitchStart),SwitchPeriod)>SwitchPeriod/2.) || (Time<(SwitchStart+N_cycle*SwitchPeriod-PushPeriod) && fmod((Time+PushPeriod-SwitchStart),SwitchPeriod)>SwitchPeriod/2.);
  
  if ((LaserSwitch && LaserStatus && !laser_ON) || (LaserSwitch && !LaserStatus && laser_ON)) return true;
  else return false;
}*/
/***********************************************************************************/
int Analyzer::ConfigLEDCalibration(string Option, double Period)
{
  if (Option.compare("ON")==0){
    LEDStablizer = true;
  }else if (Option.compare("OFF")==0){
    LEDStablizer = false;
  }else{
    cout << "Incorrect option: "<<Option<<endl;
    cout  <<"By default set to OFF.\n";
    LEDStablizer = false;
  }
  LEDPeriod = Period;
  return 0;
}

/***********************************************************************************/
int Analyzer::SetMode(string mode)
{
  static string AllowedMode[7] = {"Triple","Double","Beta","Scint","MWPC","MCP","He4"};
  //Init switches,all false
  /*  map <string,HistUnit>::iterator it;
      for (it=ExpHistBox.begin();it!=ExpHistBox.end();it++){
      ModeSwitches[it->first] = false;
      }*/
  //Configure modes
  //Turn off the forbidden histograms
  if (mode.compare("Triple")==0){
    ReadMode = mode;
    ExpForbiddenCondFunctions.clear(); //no conditions are forbidden
    SimForbiddenCondFunctions.clear(); 
    //    ExpForbiddenCondFunctions.push_back("ExpCond_N2_Charge"); //replace with relevent condition name if any
    //    ExpForbiddenCondFunctions.push_back("ExpCond_N2_Time");//replace with relevent condition name if any

    //Enable all experimental histograms
    EnableDisableAll(true);
    //Disable N2LaserHists
    //Only For Experiment, not simulation
    if (Purpose==1){//Experiment
      DisableHist("N2_Charge");
      DisableHist("N2_Time");
    }
    cout << "Read Mode is set to: "<<mode<<endl;
  }else if (mode.compare("Double")==0){
    ReadMode = mode;
    //Remove MCP and Scint from the forbidden condition list
    InitDefaultExpForbiddenCondFunctions();
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MCPArea");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MCPT1T2");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_TOFRange");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_TOFScintE2D"); 
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_ScintE");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_TriggerBlock");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_GroupTime");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MCPCharge");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_QValue");
    //Enable MCP and Scint hists
    EnableHist("MCP_Charge");
    EnableHist("MCP_Image");
    EnableHist("MCP_Image_Zoom");
    EnableHist("MCP_R");
    EnableHist("MCP_TX1vQX1");
    EnableHist("MCP_TX2vQX2");
    EnableHist("MCP_TY1vQY1");
    EnableHist("MCP_TY2vQY2");
    EnableHist("MCP_TX1pTX2");
    EnableHist("MCP_TXSumPos");
    EnableHist("MCP_TY1pTY2");
    EnableHist("MCP_TYSumPos");
    EnableHist("MCP_Time");
    EnableHist("TOFHist");
    EnableHist("Scint_EA");
    EnableHist("Scint_EDA");
    EnableHist("Scint_Time");
    EnableHist("TOF_vs_Scint");    
    EnableHist("TOF_vs_QMCP");    
    EnableHist("Q_Values");    

    //Set Sim forbidden functions
    InitDefaultSimForbiddenCondFunctions();
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_MCPArea");
    //FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_MCPT1T2");
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_TOFRange");
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_TOFScintE2D"); 
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_ScintE");
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_QValue");

    cout << "Read Mode is set to: "<<mode<<endl;
  }else if (mode.compare("Beta")==0){
    ReadMode = mode;
    //Remove MWPC and Scint from the forbidden condition list
    InitDefaultExpForbiddenCondFunctions();
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MWPCE");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MWPCT");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MWPCArea");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_ScintE");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_TriggerBlock");

    //Enable MWPC and Scint  
    if (Purpose==1){//Experiment
      EnableHist("MWPC_Anode");
      EnableHist("MWPC_CAY");
      EnableHist("MWPC_Cathode_Anode");
      EnableHist("MWPC_Cathode_XY");
      EnableHist("MWPC_Image");
      EnableHist("MWPC_vs_Scint");
      EnableHist("Beta_Time_diff");
      EnableHist("Scint_EA");
      EnableHist("Scint_EDA");
      EnableHist("Scint_Time");
    }
    if (Purpose==0){//Simulation
      EnableHist("RawScint");
      EnableHist("RawMWPC");
      EnableHist("CoinScint");
      EnableHist("CoinMWPC");
      EnableHist("MWPCPosX");
      EnableHist("MWPCPosY");
      EnableHist("MWPC_vs_Scint");
      EnableHist("MWPC_Image");
      EnableHist("MWPC_CAY");
      EnableHist("MWPCHitShift");
      EnableHist("COSBetaShiftAngle");
    }

    //Set Sim forbidden functions
    InitDefaultSimForbiddenCondFunctions();
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_MWPCE");
    //FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_MWPCT");
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_MWPCArea");
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_ScintE");

    cout << "Read Mode is set to: "<<mode<<endl;
  }else if (mode.compare("Scint")==0){
    ReadMode = mode;
    //Remove Scint from the forbidden condition list
    InitDefaultExpForbiddenCondFunctions();
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_ScintE");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_TriggerBlock");

    //Enable scint   
    EnableHist("Scint_EA"); 
    EnableHist("Scint_EDA"); 
    EnableHist("Scint_Time");

    //Set Sim forbidden functions
    InitDefaultSimForbiddenCondFunctions();
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_ScintE");

    cout << "Read Mode is set to: "<<mode<<endl;
  }else if (mode.compare("MWPC")==0){
    ReadMode = mode;
    //Remove MWPC from the forbidden condition list
    InitDefaultExpForbiddenCondFunctions();
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MWPCE");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MWPCArea");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_TriggerBlock");

    //Enable MWPC

    if (Purpose==1){//Experiment
      EnableHist("MWPC_Anode");
      EnableHist("MWPC_CAY");
      EnableHist("MWPC_Cathode_Anode");
      EnableHist("MWPC_Cathode_XY");
      EnableHist("MWPC_Image");
    }
    if (Purpose==0){//Simulation
      EnableHist("RawMWPC");
      EnableHist("CoinMWPC");
      EnableHist("MWPCPosX");
      EnableHist("MWPCPosY");
      EnableHist("MWPC_Image");
      EnableHist("MWPC_CAY");
      EnableHist("MWPCHitShift");
      EnableHist("COSBetaShiftAngle");
    }
    //Set Sim forbidden functions
    InitDefaultSimForbiddenCondFunctions();
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_MWPCE");
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_MWPCArea");
    cout << "Read Mode is set to: "<<mode<<endl;
  }else if (mode.compare("MCP")==0){
    ReadMode = mode;
    //Remove MPC from the forbidden condition list
    InitDefaultExpForbiddenCondFunctions();
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MCPArea");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MCPT1T2");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_TriggerBlock");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_GroupTime");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MCPCharge");
    //Enable MCP
    if (Purpose==1){//Experiment
      EnableHist("MCP_Charge");
      EnableHist("MCP_Image");
      EnableHist("MCP_Image_Zoom");
      EnableHist("MCP_R");
      EnableHist("MCP_TX1vQX1");
      EnableHist("MCP_TX2vQX2");
      EnableHist("MCP_TY1vQY1");
      EnableHist("MCP_TY2vQY2");
      EnableHist("MCP_TXSumPos");
      EnableHist("MCP_TX1vQX1");
      EnableHist("MCP_TY1pTY2");
      EnableHist("MCP_TX1pTX2");
      EnableHist("MCP_TYSumPos");
      EnableHist("MCP_Time");
    }
    if (Purpose==0){//Simulation
      EnableHist("MCP_Image");
      EnableHist("MCP_R");
      EnableHist("HitAngle");
    }

    //Set Sim forbidden functions
    InitDefaultSimForbiddenCondFunctions();
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_MCPArea");
    //FindAndRemoveVector(ForbiddenCondFunctions, "Cond_MCPT1T2");
    cout << "Read Mode is set to: "<<mode<<endl;
  }else if (mode.compare("He4")==0){
    ReadMode = mode;
    //Remove MCP and N2Laser from the forbidden condition list
    InitDefaultExpForbiddenCondFunctions();
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MCPArea");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MCPT1T2");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_TOFRange");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_TriggerBlock");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_GroupTime");
    FindAndRemoveVector(ExpForbiddenCondFunctions, "ExpCond_MCPCharge");
    //Future N2Laser conditions go here...	
    //Enable N2Laser
    EnableHist("MCP_Charge");
    EnableHist("MCP_Image");
    EnableHist("MCP_Image_Zoom");
    EnableHist("MCP_R");
    EnableHist("MCP_TX1vQX1");
    EnableHist("MCP_TX1pTX2");
    EnableHist("MCP_TY1pTY2");
    EnableHist("N2_Charge");
    EnableHist("TOFHist");
    EnableHist("N2_Time");
    //EnableHist("MCP_Time");

    //Set Sim forbidden functions
    InitDefaultSimForbiddenCondFunctions();
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_MCPArea");
    //FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_MCPT1T2");
    FindAndRemoveVector(SimForbiddenCondFunctions, "Cond_TOFRange");
    cout << "Read Mode is set to: "<<mode<<endl;    
  }else{
    cout << "Mode: "<<mode<<" is not identified."<<endl;
    return -1;
  }
  //Reset all conditions, because some conditions are not allowed in some modes
  ResetExpConditions();
  ResetSystematics();
  return 0;
}

/***********************************************************************************/
void Analyzer::SetExpTime_rel(RecDataStruct& RecData){
  if (ReadMode.compare("Triple")==0 || ReadMode.compare("Double")==0 || ReadMode.compare("Beta")==0 || ReadMode.compare("Scint")==0){
    RecData.Time_rel = &RecData.TScint_A_rel;
  }else if(ReadMode.compare("MCP")==0 || ReadMode.compare("He4")==0){
    RecData.Time_rel = &RecData.TMCP_rel;
  }else if(ReadMode.compare("MWPC")==0){
    RecData.Time_rel = &RecData.GroupTime;
  }else {
    cout<< "Error: ReadMode "<<ReadMode<<" not known by function SetExpTime_rel()!"<<endl;
    exit(1);
  }
}
/***********************************************************************************/
int Analyzer::SetExpFilePrefix(string prefix)
{
  ExpFilePrefix = prefix;
  return 0;
}
/***********************************************************************************/
void Analyzer::ClearTempData(ExpDataStruct& ExpData,RecDataStruct& RecData)
{
  ExpData.Event_Num = 0;                //Event number
  ExpData.GroupTime = 0.0;             //Group time
  ExpData.QScint_A[0] = 0.0;                //Scintillator anode QDC
  ExpData.QScint_A[1] = 0.0;                //Scintillator anode QDC
  ExpData.QScint_D[0] = 0.0;                //Scintillator dynode QDC
  ExpData.QScint_D[1] = 0.0;                //Scintillator dynode QDC
  ExpData.TScint_A_rel = 0.0;            //Scintillator time relative to Tref
  ExpData.AMWPC_anode1 = 0.0;           //MWPC anode1 ADC
  ExpData.AMWPC_anode2 = 0.0;           //MWPC anode2 ADC
  ExpData.TMWPC_rel[0] = 0.0;             //MWPC1 time relative to Tref
  ExpData.TMWPC_rel[1] = 0.0;             //MWPC2 time relative to Tref
  ExpData.TBeta_diff[0] = 0.0;            //MWPC1 time relative to Scint
  ExpData.TBeta_diff[1] = 0.0;            //MWPC2 time relative to Scint
  ExpData.MWPC_X.AX1 = 0.0;     //MWPC cathode X ADCs
  ExpData.MWPC_X.AX2 = 0.0;     //MWPC cathode X ADCs
  ExpData.MWPC_X.AX3 = 0.0;     //MWPC cathode X ADCs
  ExpData.MWPC_X.AX4 = 0.0;     //MWPC cathode X ADCs
  ExpData.MWPC_X.AX5 = 0.0;     //MWPC cathode X ADCs
  ExpData.MWPC_X.AX6 = 0.0;     //MWPC cathode X ADCs
  ExpData.TMWPC_X.TX1 = 0.0;   //MWPC cathode X TDCs
  ExpData.TMWPC_X.TX2 = 0.0;   //MWPC cathode X TDCs
  ExpData.TMWPC_X.TX3 = 0.0;   //MWPC cathode X TDCs
  ExpData.TMWPC_X.TX4 = 0.0;   //MWPC cathode X TDCs
  ExpData.TMWPC_X.TX5 = 0.0;   //MWPC cathode X TDCs
  ExpData.TMWPC_X.TX6 = 0.0;   //MWPC cathode X TDCs
  ExpData.MWPC_Y.AY1 = 0.0;     //MWPC cathode Y ADCs
  ExpData.MWPC_Y.AY2 = 0.0;     //MWPC cathode Y ADCs
  ExpData.MWPC_Y.AY3 = 0.0;     //MWPC cathode Y ADCs
  ExpData.MWPC_Y.AY4 = 0.0;     //MWPC cathode Y ADCs
  ExpData.MWPC_Y.AY5 = 0.0;     //MWPC cathode Y ADCs
  ExpData.MWPC_Y.AY6 = 0.0;     //MWPC cathode Y ADCs
  ExpData.TMWPC_Y.TY1 = 0.0;   //MWPC cathode Y TDCs
  ExpData.TMWPC_Y.TY2 = 0.0;   //MWPC cathode Y TDCs
  ExpData.TMWPC_Y.TY3 = 0.0;   //MWPC cathode Y TDCs
  ExpData.TMWPC_Y.TY4 = 0.0;   //MWPC cathode Y TDCs
  ExpData.TMWPC_Y.TY5 = 0.0;   //MWPC cathode Y TDCs
  ExpData.TMWPC_Y.TY6 = 0.0;   //MWPC cathode Y TDCs
  ExpData.QMCP = 0.0;                  //MCP QDC
  ExpData.QMCP_anodes.QX1 = 0.0;   //MWPC anode QDCs
  ExpData.QMCP_anodes.QX2 = 0.0;   //MWPC anode QDCs
  ExpData.QMCP_anodes.QY1 = 0.0;   //MWPC anode QDCs
  ExpData.QMCP_anodes.QY2 = 0.0;   //MWPC anode QDCs
  ExpData.TMCP_anodes.TX1 = 0.0;   //MWPC anode TDCs
  ExpData.TMCP_anodes.TX2 = 0.0;   //MWPC anode TDCs
  ExpData.TMCP_anodes.TY1 = 0.0;   //MWPC anode TDCs
  ExpData.TMCP_anodes.TY2 = 0.0;   //MWPC anode TDCs
  ExpData.TMCP_rel = 0.0;              //MCP time relative to Tref
  ExpData.TOF = 0.0;                   //MCP time relative to Scint
  ExpData.LED = 0.0;
  ExpData.TriggerMap = 0;    //Trigger map
  ExpData.QN2Laser = 0;			//Charge of N2 Laser on photodiode
  ExpData.TN2Laser = 0;			//Trigger time of N2 Laser on photodiode
	//ExpData.TOFN2Laser = 0;		//MCP_back - TN2Laser
	
  RecData.EScintA[0] = 0.0;               //Scintillator QDC anode
  RecData.EScintA[1] = 0.0;               //Scintillator QDC anode
  RecData.EScintD[0] = 0.0;               //Scintillator QDC dynode
  RecData.EScintD[1] = 0.0;               //Scintillator QDC dynode
  RecData.EMWPC_anode1 = 0.0;          //MWPC anode1 ADC
  RecData.EMWPC_anode2 = 0.0;          //MWPC anode2 ADC
  RecData.QMWPC_cathode_X = 0.0;
  RecData.QMWPC_cathode_Y = 0.0;
  RecData.MWPCPos_X = -100.0;
  RecData.MWPCPos_Y = -100.0;
  RecData.MWPCPos_Y_C = -100.0;
  RecData.QMCP = 0.0;                  //MCP QDC
  RecData.MCP_TXSum = 0.0;             //TX1 + TX2
  RecData.MCP_TYSum = 0.0;             //TY1 + TY2
  RecData.MCPPos_X = -1000.0;
  RecData.MCPPos_Y = -1000.0;
  RecData.TOF = 0.0;                   //MCP time relative to Scint
  RecData.QN2Laser = 0;			//Charge of N2 Laser on photodiode
  RecData.TN2Laser = 0;			//Trigger time of N2 Laser on photodiode
  //RecData.TOFN2Laser = 0;		//MCP_back - TN2Laser
  RecData.Q_Value[0]=RecData.Q_Value[0]=0.0;
  RecData.CosThetaENu=-100;	//some unreasonable number
}
/***********************************************************************************/
int Analyzer::SetScattMode(string mode){
  if (mode.compare("Reject")==0 || mode.compare("Select")==0){
    ScattMode = mode;
    return 0;
  }
  cout << "Scatt mode "<<mode<<" is not acceptable."<<endl;
  return 1;
}
/***********************************************************************************/
















