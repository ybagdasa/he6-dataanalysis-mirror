//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>


//Analyzer includes
#include "Analyzer.h"
#include "AnalyzerUI.h"
#include "T0Correction.h"
/***********************************************************************************/
//Function definations

using namespace std;

/***********************************************************************************/
vector<string> split(const string& s, char delimiter)
{
   vector<string> tokens;
   string token;
   istringstream tokenStream(s);
   while (getline(tokenStream, token, delimiter))
   {
      tokens.push_back(token);
   }
   return tokens;
}
/***********************************************************************************/
//Constructor
AnalyzerUI::AnalyzerUI(Analyzer *Ana,Calibration *CalPtr){
  CommandLine = "";
  Purpose = "Simulation";
  CurrentAnalyzer = Ana;
  CurrentCalibration = CalPtr;
}

/***********************************************************************************/
//Destructor
AnalyzerUI::~AnalyzerUI(){
  ;
}

/***********************************************************************************/
//Initialize
int AnalyzerUI::Init(){ 
  return 0;
}

int AnalyzerUI::Start(int mode){
  int returnVal;
  cout<<"*********************************************************\n"
      <<"*****************He6 Experiment Analyzer*****************\n"
      <<"************************V3.10****************************\n"
      <<"*********************************************************\n"
      <<"*                 Ran Hong, 08-22-2014                  *\n"
      <<"*********************************************************\n";
  if (mode == 0)cout<<"Type your command after the prompt >\n";
  else cout <<"Running commands from a script file...\n";
  while(1){
    if (mode==0) cout<<"He6-Analyzer:>";	//Commandline mode
    cin>>CommandLine;
    returnVal = ProcessCommand(CommandLine);
    if (returnVal == 1){
    	cout << "Apparently OK to proceed.\n"<<endl;
      break;
    }
    if (returnVal == -1 || returnVal == 3){
      cout << "Error happens!\n";
      return -1;
    }
  }
  return 0;
}

int AnalyzerUI::ProcessCommand(string Cmd)
{
  int Value;
  int iValue,i1,i2,i3,i4,i5,i6,i7,i8;
  bool boolVal;
  double dValue;
  double dValue1,dValue2,dValue3,dValue4,dValue5,dValue6,dValue7,dValue8,dValue9,dValue10,dValue11,dValue12;
  string str1,str2,str3,str4,str5,str6;
  string buffer;
  string infilename;
  string label;
  string option;
  string histname;
  char   CharBuffer[5000];
  range FitRange;
  int returnVal = 0;

  /************************************Basic Controls*****************************************/
  if(Cmd.compare("exit")==0){		//Exit the program 
    return 1;
  }else if(Cmd.compare("ls")==0){	//List commands
    LS();
  }else if(Cmd.compare("lsHist")==0){	//List histograms
    returnVal = CurrentAnalyzer->LsHist();
  }else if(Cmd.compare("lsAllConditions")==0){	//List all availalbe conditions
    returnVal = CurrentAnalyzer->LsConditions();
  }else if(Cmd.compare("lsEnabledConditions")==0){	//List conditions
    returnVal = CurrentAnalyzer->LsEnabledCondFunc();  
  }else if(Cmd.compare("SetPurpose")==0){	//Set Purpose
    cin>>i1;
    cin>>i2;
    if((i1!=0 && i1!=1)||(i2!=0 && i2!=1)){
      cout<<"Wrong purpose Value! See command list for details.\n";
      return -1;
    }
    if (i1==0) Purpose = "Simulation";
    else if(i1==1) Purpose = "Experiment";
    CurrentAnalyzer->SetPurpose(i1,i2);
    cout<<"Purpose is set to "<< (i2==0 ? "He-6 Decay " : "Calibration ")<<(i1==0 ? "Simulation" : "Experiment") <<endl;
  }else if(Cmd.compare("ProcessInputSim")==0){	//Process input file of simulation
    cout << "Processing input files from Simulation"<<endl;
    cin>>i1>>i2>>i3;
    returnVal = CurrentAnalyzer->ProcessInputSim(i1,i2,i3);
  /************************************Histogram Operations*****************************************/
  }else if(Cmd.compare("EnableHist")==0){//Enable histogram filling
    cin>>label;
    returnVal = CurrentAnalyzer->EnableHist(label);
    if(returnVal !=0){
      cout<<"Defined Histograms:\n";
      CurrentAnalyzer->LsHist();
    }
  }else if(Cmd.compare("EnableBkgHist")==0){//Enable background histogram filling
    cin>>label;
    returnVal = CurrentAnalyzer->EnableBkgHist(label);
    if(returnVal !=0){
      cout<<"Defined Histograms:\n";
      CurrentAnalyzer->LsHist();
    }  
  }else if(Cmd.compare("DisableHist")==0){//Disable histogram filling
    cin>>label;
    returnVal = CurrentAnalyzer->DisableHist(label); 
    if(returnVal !=0){
      cout<<"Defined Histograms:\n";
      CurrentAnalyzer->LsHist();
    }
  }else if(Cmd.compare("DisableBkgHist")==0){//Disable backgroud histogram filling
    cin>>label;
    returnVal = CurrentAnalyzer->DisableBkgHist(label); 
    if(returnVal !=0){
      cout<<"Defined Histograms:\n";
      CurrentAnalyzer->LsHist();
    }    
  }else if(Cmd.compare("SaveHistograms")==0){	//Save histograms to a root file
    if (Purpose.compare("Simulation")==0){
      cin>>label;
      returnVal = CurrentAnalyzer->SaveHistograms(label);
    }
    else if (Purpose.compare("Experiment")==0){
      cin>>label>>option;
      returnVal = CurrentAnalyzer->SaveExpHistograms(label,option);
    }
    cout << "Histograms are saved to root files.\n";
  }else if(Cmd.compare("ConfigHistogram")==0){	//Config low hight and NBin
    if (Purpose.compare("Simulation")==0){
      cin>>str1>>dValue1>>dValue2>>i1;
      returnVal = CurrentAnalyzer->ConfigHistogram(str1,dValue1,dValue2,i1);
    }
    else if (Purpose.compare("Experiment")==0){
      cin>>str1>>dValue1>>dValue2>>i1;
      returnVal = CurrentAnalyzer->ConfigExpHistogram(str1,dValue1,dValue2,i1);
    }
  }else if(Cmd.compare("Config2DHistogram")==0){	//Config low hight and NBin
    if (Purpose.compare("Simulation")==0){
      cin>>str1>>dValue1>>dValue2>>i1>>dValue3>>dValue4>>i2;
      returnVal = CurrentAnalyzer->ConfigHistogram(str1,dValue1,dValue2,i1,dValue3,dValue4,i2);
    }
    else if (Purpose.compare("Experiment")==0){
      cin>>str1>>dValue1>>dValue2>>i1>>dValue3>>dValue4>>i2;
      returnVal = CurrentAnalyzer->ConfigExpHistogram(str1,dValue1,dValue2,i1,dValue3,dValue4,i2);
    }
  }else if(Cmd.compare("LoadHistograms")==0){	//Load histograms
    if (Purpose.compare("Simulation")==0){
      cin>>label;
      returnVal = CurrentAnalyzer->LoadHistograms(label);
    }
    else if (Purpose.compare("Experiment")==0){
      cin>>label>>option;
      returnVal = CurrentAnalyzer->LoadExpHistograms(label,option);
    }
    cout << "Histograms are loaded from root files.\n";
  }else if(Cmd.compare("SetStdHist")==0){	//Set to standard histogram
    cin>>histname>>option;
    returnVal = CurrentAnalyzer->SetStdHist(histname,option);
    cout << "Standard histogram for " <<option.c_str()<<" is set.\n";
  }else if(Cmd.compare("SaveStdHist")==0){	//Save standard histograms to a root file
    cin>>histname>>label;
    returnVal = CurrentAnalyzer->SaveStdHist(histname,label);
    cout << "Standard histograms are saved to root files.\n";
  }else if(Cmd.compare("LoadStdHist")==0){	//Load standard histograms
    cin>>histname>>label;
    returnVal = CurrentAnalyzer->LoadStdHist(histname,label);
    cout << "Standard histograms are loaded from root files.\n";
  }else if(Cmd.compare("AddSlicedHist")==0){	//Add a sliced histogram to the list
    cin>>histname>>str1;
    returnVal = CurrentAnalyzer->AddSlicedHist(histname,str1);
    cout << "Histogram "<<histname<<" is added to the sliced histogram list. Slice condition: "<<str1<<endl;
  }else if(Cmd.compare("ClearSlicedHistList")==0){	//Clear the sliced histogram list
    returnVal = CurrentAnalyzer->ClearSlicedHistList();
    cout << "Sliced histogram list is cleared.\n";
  }else if(Cmd.compare("CombineExpHistograms")==0){	//Delete the combined histogram list
    cin>>i1>>option;
    returnVal = CurrentAnalyzer->CombineExpHistograms(i1,option);
    cout << "Combining histograms completed.\n";
  }else if(Cmd.compare("PutBackCombHistograms")==0){	//Delete the combined histogram list
    returnVal = CurrentAnalyzer->PutBackCombHistograms();
    cout << "Combined histograms put back.\n";
  }else if(Cmd.compare("PutBackOneCombHistograms")==0){	//Delete the combined histogram list
    cin>>i1;
    returnVal = CurrentAnalyzer->PutBackOneCombHistograms(i1);
    cout << "One combined histograms put back.\n";
  }else if(Cmd.compare("DeleteCombHistList")==0){	//Delete the combined histogram list
    returnVal = CurrentAnalyzer->DelCombExpHistograms();
    cout << "Combined histogram list is deleted.\n";
  /************************************Condition Operations*****************************************/
  }else if (Cmd.compare("InitCondition")==0){	//Initialize condition list with length N, start from low with interval increment
    cin >> str1 >> i1 >> dValue1 >> dValue2;
    if (Purpose.compare("Simulation")==0)returnVal = CurrentAnalyzer->InitCondition(str1,i1,dValue1,dValue2);
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->InitExpCondition(str1,i1,dValue1,dValue2);
    cout << "Condition "<<str1<<" Initialized.\n";
  }else if (Cmd.compare("SetCondition")==0){	//Set condition list entry i1 to value 
    cin >> str1 >> i1 >> dValue1;
    if (Purpose.compare("Simulation")==0)returnVal = CurrentAnalyzer->SetCondition(str1,i1,dValue1);
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->SetExpCondition(str1,i1,dValue1);
    cout << "Condition "<<str1 <<" entry "<<i1<< " is set to "<<dValue1<<".\n";
  }else if (Cmd.compare("SetDefaultConditions")==0){	//Set all conditions to default values
    if (Purpose.compare("Simulation")==0)returnVal = CurrentAnalyzer->SetDefaultConditions();
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->SetDefaultExpConditions();
    cout << "All conditions set to default values\n"; 
  }else if (Cmd.compare("AddCondLoopPtr")==0){	//Loop condition with Name, index = 1 or 2
    cin >> i1 >> str1;
    if (Purpose.compare("Simulation")==0)returnVal = CurrentAnalyzer->AddCondLoopPtr(i1,str1);
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->AddExpCondLoopPtr(i1,str1);
    if (returnVal == 0)cout << "Condition "<<str1<< " is added to Loop "<<i1<<endl;
  }else if (Cmd.compare("UnSetCondLoopPtrs")==0){	//Unset the loopings
    if (Purpose.compare("Simulation")==0)returnVal = CurrentAnalyzer->UnSetCondLoopPtrs();
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->UnSetExpCondLoopPtrs();
    cout << "Loop pointers are unset\n";
  }else if (Cmd.compare("CondFuncSwitch")==0){	//Switch condition functions on and off
    cin >> str1 >>str2;
    if (Purpose.compare("Simulation")==0){
      if (str2.compare("ON")==0){
	returnVal = CurrentAnalyzer->SetCondFuncMap(str1,true);
      }else if (str2.compare("OFF")==0){
	returnVal = CurrentAnalyzer->SetCondFuncMap(str1,false);
      }
    }
    else if (Purpose.compare("Experiment")==0){
      if (str2.compare("ON")==0){
	returnVal = CurrentAnalyzer->SetExpCondFuncMap(str1,true);
      }else if (str2.compare("OFF")==0){
	returnVal = CurrentAnalyzer->SetExpCondFuncMap(str1,false);
      }
    }
    cout << "Condition function "<<str1<<" is turned "<<str2<<endl;;
  }else if (Cmd.compare("CheckConditionSettings")==0){	//Check Condition settings, set condition dimension correctly
    if (Purpose.compare("Simulation")==0)returnVal = CurrentAnalyzer->CheckConditionSettings();
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->CheckExpConditionSettings();
    cout << "Checking Conditions.\n";
  }else if (Cmd.compare("SetMCPefficiencyMap")==0){	//Turn on or off the MCP efficiency map, ON/OFF
    cin>>str1;
    if (str1.compare("ON")==0)
      returnVal = CurrentAnalyzer->SetMCPefficiencyMap(1);
    else if (str1.compare("OFF")==0)
      returnVal = CurrentAnalyzer->SetMCPefficiencyMap(0);
    else{
      cout << "Incorrect input value, MCP efficiency map is set to ON by default.\n";
      returnVal = CurrentAnalyzer->SetMCPefficiencyMap(1);
    }
    cout << "MCP efficiency Map is " <<str1<<endl;
  }else if (Cmd.compare("InitSliceBoundary")==0){	//Initialize the slice boundary list (N sections), start from low with interval increment
    cin>>i1>>dValue1>>dValue2;
    returnVal = CurrentAnalyzer->InitSliceBoundary(i1,dValue1,dValue2);
    cout << "Slice Boundary list initialized.\n";
  }else if (Cmd.compare("SetFileCondLnk")==0){ //Correlate conditions with FileID, ON/OFF
    cin>>str1;
    if (str1.compare("ON")==0) 
      returnVal = CurrentAnalyzer->SetFileCondLnk(true);
    else if (str1.compare("OFF")==0)
      returnVal = CurrentAnalyzer->SetFileCondLnk(false);
    else{ 
      cout << "Incorrect input value for "<<Cmd<<". (ON/OFF). Ignoring."<<endl;
    }  
  }else if (Cmd.compare("SetCharge3Switch")==0){ //Correlate conditions with FileID, ON/OFF
    cin>>str1;
    if (str1.compare("ON")==0) 
      returnVal = CurrentAnalyzer->SetCharge3Switch(true);
    else if (str1.compare("OFF")==0)
      returnVal = CurrentAnalyzer->SetCharge3Switch(false);
    else{ 
      cout << "Incorrect input value for "<<Cmd<<". (ON/OFF). Ignoring."<<endl;
    }  
  /************************************Parameter Operations*****************************************/
  }else if (Cmd.compare("ResetParameters")==0){	//Set all Parameters to default values
    if (Purpose.compare("Simulation")==0)returnVal = CurrentAnalyzer->ResetParameters();
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->ResetExpParameters();
    cout << "All parameterss set to default values\n"; 
  }else if (Cmd.compare("InitParameterArray")==0){	//Initialize parameter array with length N, start from low with interval increment
    cin >> str1 >> i1 >> dValue1 >> dValue2;
    if (Purpose.compare("Simulation")==0)returnVal = CurrentAnalyzer->InitParameterArray(str1,i1,dValue1,dValue2);
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->InitExpParameterArray(str1,i1,dValue1,dValue2);
    cout << "Parameter "<<str1<<" Initialized.\n";
  }else if (Cmd.compare("SetParameter")==0){	//Set parameter list entry i1 to value 
    cin >> str1 >> i1 >> dValue1;
    if (Purpose.compare("Simulation")==0)returnVal = CurrentAnalyzer->SetParameter(str1,i1,dValue1);
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->SetExpParameter(str1,i1,dValue1);
    cout << "Parameter "<<str1 <<" entry "<<i1<< " is set to "<<dValue1<<".\n";
  }else if (Cmd.compare("SetParameterLoop")==0){	//Parameter Loop condition with Name, index = 1 or 2
    cin >> str1 >>i1;
    if (Purpose.compare("Simulation")==0)returnVal = CurrentAnalyzer->SetParameterLoop(str1,i1);
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->SetExpParameterLoop(str1,i1);
    cout << "Parameter "<<str1<< " is added to Loop "<<i1<<endl;
  }else if (Cmd.compare("UnSetParameterLoop")==0){	//Unset the loopings
    cin >> str1;
    if (Purpose.compare("Simulation")==0)returnVal = CurrentAnalyzer->UnsetParameterLoop(str1);
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->UnsetExpParameterLoop(str1);
    cout << "Loop pointers are unset\n";
  }else if (Cmd.compare("ConstructParameterList")==0){	//Check Condition settings, set condition dimension correctly
    if (Purpose.compare("Simulation")==0)returnVal = CurrentAnalyzer->ConstructParameterList();
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->ConstructExpParameterList();
    cout << "Checking parameters and constructing ParameterLists.\n";
  /************************************File Operations*****************************************/
  }else if (Cmd.compare("SetFileSimDim")==0){	//Set FileID list dimensions
    cin >> i1 >>i2;
    returnVal = CurrentAnalyzer->SetFileSimDim(i1,i2);
    cout << "File dimensions are set to "<<i1<<" x "<<i2<<endl;
  }else if (Cmd.compare("SetFileIDEntrySim")==0){	//Set FileID list entry
    cin >> i1 >>i2 >>i3>>i4>>i5>>i6;;
    returnVal = CurrentAnalyzer->SetFileIDEntrySim(i1,i2,i3,i4,i5,i6);
    cout << "FileIDList["<<i1<<"]["<<i2<<"] is set to "<<i3<<" "<<i4<<" "<<i5<<" "<<i6<<endl;
  }else if (Cmd.compare("SetFileIDListSim")==0){	//Set FileID list with initial IDs in row (index), file dimensions should be set first, Inc specifies which ID to increase
    cin >> i1 >>i2 >>i3>>i4>>i5>>str1;
    returnVal = CurrentAnalyzer->SetFileIDListSim(i1,i2,i3,i4,i5,str1);
  }else if (Cmd.compare("SetFileIDListSim2D")==0){	//Set FileID 2D list using the first row
    cin >> str1;
    returnVal = CurrentAnalyzer->SetFileIDListSim2D(str1);
  }else if (Cmd.compare("InitSysWFile")==0){	//Initialize file condition list with length N, start from low with interval increment
    cin >> str1 >> i1 >> dValue1 >>dValue2;
    returnVal = CurrentAnalyzer->InitSysWFile(str1,i1,dValue1,dValue2);
    cout << "File Condition list "<< str1 << " is initialized\n";
  }else if (Cmd.compare("SetSysWFile")==0){	//Set one entry i of a file condition list with Name to value
    cin >> str1 >> i1 >> dValue1;
    returnVal = CurrentAnalyzer->SetSysWFile(str1,i1,dValue1);
    cout << "File Condition "<<str1 <<" entry "<<i1<< "is set.\n";
  }else if (Cmd.compare("SaveRootTrees")==0){
    cin >> str1;
    if (Purpose.compare("Simulation")==0) returnVal = CurrentAnalyzer->SetRootOutputSim(true,str1);
    else if (Purpose.compare("Experiment")==0)returnVal = CurrentAnalyzer->SetRootOutputExp(true,str1);
    cout<<"Will save data to root output file."<<endl;
  }else if (Cmd.compare("LoadOutTrees")==0){
    cin >> str1;
    returnVal = CurrentAnalyzer->LoadOutTrees(str1.c_str());
  }else if (Cmd.compare("OutputTreesToBin")==0){
    cin >> str1 >>str2;
    vector<string> br = split(str1,',');
    returnVal = CurrentAnalyzer->OutputTreesToBin(br,str2);
  /************************************Fit Operations*****************************************/
  }else if (Cmd.compare("FitConfig")==0){	//Configure Fit, Type = MINUIT or HONG,TimeShift = ON/OFF
    cin >> str1>>str2>>dValue1;
    returnVal = CurrentAnalyzer->FitConfig(str1,str2,dValue1);
    cout << "Configure Fit: Type "<<str1<<", TimeFloat "<<str2<<", TimeShift = "<<dValue1<<endl;;
  }else if (Cmd.compare("InitFit")==0){		//Initialize histograms and result array for fit
    cin >> str1;
    returnVal = CurrentAnalyzer->InitFit(str1);
    cout << "Initialize fit histograms with name "<<str1<<endl; 
  }else if(Cmd.compare("Fit_TOF")==0){		//Fit one histogram with index1 and index2 within a range
    cin>>i1>>i2>>FitRange.low>>FitRange.high;
    returnVal = CurrentAnalyzer->Fit_TOF(i1,i2,FitRange);
  }else if(Cmd.compare("FitWithEZ")==0){		//Fit E field and Z position
    cin>>str1>>i1>>i2>>FitRange.low>>FitRange.high>>str2;
    returnVal = CurrentAnalyzer->FitWithEZ(str1,i1,i2,FitRange,str2);
  /************************************Output Operations*****************************************/
  }else if(Cmd.compare("OpenSpecFile")==0){	//Open spectrum file with a name
    cin>>str1;
    returnVal = CurrentAnalyzer->OpenSpecFile(str1);
    cout << "Spectrum File "<<str1<<" opened."<<endl;
  }else if(Cmd.compare("CloseSpecFile")==0){	//Close the spectrum file
    returnVal = CurrentAnalyzer->CloseSpecFile();
    cout << "Spectrum File closed"<<endl;
  }else if(Cmd.compare("PlotHistogram")==0){	//Plot one histogram with index 1 and 2 on a page
    cin>>str1>>i1>>i2;
    returnVal = CurrentAnalyzer->PlotHistogram(str1,i1,i2);
    cout << "Histogram "<<str1<<" is plotted."<<endl;
  }else if(Cmd.compare("PlotCombHistogram")==0){	//Plot one combined histogram with index 1 and 2 on a page
    cin>>str1>>i1>>str2;
    returnVal = CurrentAnalyzer->PlotCombHistogram(str1,i1,str2);
    cout << "Combined Histogram "<<str1<<" is plotted."<<endl;
  }else if(Cmd.compare("PlotHistogramList")==0){	//Plot the whole list of histograms
    cin>>str1>>i1>>i2>>i3>>i4;
    returnVal = CurrentAnalyzer->PlotHistogramList(str1,i1,i2,i3,i4);
    cout << "Histogram list "<<str1<<" is plotted."<<endl;
  }else if(Cmd.compare("OutputSpectraGeneral")==0){	//Output general spectra for histogram[index1][index2]
    cin>> i1>>i2;
    returnVal = CurrentAnalyzer->OutputSpectraGeneral(i1,i2);
    cout << "Plot complete."<<endl;
  }else if(Cmd.compare("OutputFittedHists")==0){	//Output Fitted histograms
    cin>>str1>>i1>>i2>>i3>>i4;
    returnVal = CurrentAnalyzer->OutputFittedHists(str1,i1,i2,i3,i4);
    cout << "Fitted histograms "<< str1<<" are plotted."<<endl;
  }else if(Cmd.compare("ShowAxisTitle")==0){	//Show Axis Title on graph, Option = On/OFF
    cin>>str1;
    if (str1.compare("ON")==0)
      returnVal = CurrentAnalyzer->ShowAxisTitle(true); 
    else if (str1.compare("OFF")==0)
      returnVal = CurrentAnalyzer->ShowAxisTitle(false); 
    else{
      cout << "Incorrect input value,Axis Title ON by default.\n";
      returnVal = CurrentAnalyzer->ShowAxisTitle(true); 
    }
  }else if(Cmd.compare("ShowTitle")==0){	//Show Title on graph, Option = On/OFF
    cin>>str1;
    if (str1.compare("ON")==0)
      returnVal = CurrentAnalyzer->ShowTitle(true); 
    else if (str1.compare("OFF")==0)
      returnVal = CurrentAnalyzer->ShowTitle(false); 
    else{
      cout << "Incorrect input value,Title ON by default.\n";
      returnVal = CurrentAnalyzer->ShowTitle(true); 
    }
  }else if(Cmd.compare("ShowGrid")==0){	//Show Grid, Option = On/OFF
    cin>>str1;
    if (str1.compare("ON")==0)
      returnVal = CurrentAnalyzer->ShowGrid(true); 
    else if (str1.compare("OFF")==0)
      returnVal = CurrentAnalyzer->ShowGrid(false); 
    else{
      cout << "Incorrect input value,Grid ON by default.\n";
      returnVal = CurrentAnalyzer->ShowGrid(true); 
    }
  }else if(Cmd.compare("SetDrawOption2DHist")==0){	//Set DrawOptions for 2D histogram drawing
    cin>>str1;
    returnVal = CurrentAnalyzer->SetDrawOption2DHist(str1); 
  }else if(Cmd.compare("SetDrawOption2DGraph")==0){	//Set DrawOptions for 2D graph drawing
    cin>>str1;
    returnVal = CurrentAnalyzer->SetDrawOption2DGraph(str1); 
  }else if(Cmd.compare("OutputHistToTxt")==0){//Output histogram bin values and bin contents to text file, HistName, Filename_preffix
    cin>>str1>>str2;
    returnVal = CurrentAnalyzer->OutputHistogramToTxt(str1,str2);  
  }else if(Cmd.compare("MakeMovie")==0){//Make animated gif of histograms corresponding to str1, called str2, along hist dim i1 = 1 or 2 starting from index i2 = j or i, using draw option str3
    cin>>str1>>str2>>i1>>i2>>str3;
    cout<<"Making movie "<<str2<<" using histogram "<<str1<<endl;
    returnVal = CurrentAnalyzer->MakeMovie(str1, str2, i1, i2, str3);
  /*******************************Integrated Systematic Study**************************************/
  }else if(Cmd.compare("SetSysAxis")==0){	//Set Axis for the systematic graph, Property: Condition/File
    cin>>i1;
    if(i1==1){
      cin>>str1>>str2;
      returnVal = CurrentAnalyzer->SetSysAxis(i1,str1,str2);
    }else if(i1==2){
      cin>>str1>>str2>>str3>>str4;
      returnVal = CurrentAnalyzer->SetSysAxis(i1,str1,str2,str3,str4);
    }else{
      cout << "Incorrect dimension.\n";
      returnVal = -1;
    }
  }else if(Cmd.compare("InitGraph")==0){	//Initialize systematic graph, i1 = dim 1 or 2, str = GraphName, makes graph, chi2 graph, and res graph
    cin>>i1>>str1;
    returnVal = CurrentAnalyzer->InitGraph(i1,str1);
  }else if(Cmd.compare("SystematicFit_TOF")==0){	//Fit TOF and set the fit results to the graph
    cin>>str1>>FitRange.low>>FitRange.high>>i1>>i2;
    if(i1==1){
      returnVal = CurrentAnalyzer->SystematicFit_TOF(str1,FitRange,i1,i2);
    }else if(i1==2){
      cin>>i3;
      returnVal = CurrentAnalyzer->SystematicFit_TOF(str1,FitRange,i1,i2,i3);
    }else{
      cout << "Incorrect dimension.\n";
      returnVal = -1;
    }
    cout << "Fitting complete"<<endl;
  }else if(Cmd.compare("SysPlotGraph")==0){	//Plot the systematic graph
    cin>>i1>>str1>>str2>>str3>>str4;
    if(i1==1){
      returnVal = CurrentAnalyzer->SysPlotGraph(i1,str1,str2,str3,str4);
    }else if(i1==2){
      cin>>str5>>str6;
      returnVal = CurrentAnalyzer->SysPlotGraph(i1,str1,str2,str3,str4,str5,str6);
    }else{
      cout << "Incorrect dimension.\n";
      returnVal = -1;
    }
  }else if(Cmd.compare("SysFileListInit")==0){	//Initialize the systematic file list
    cin>>i1>>i2>>i3>>i4>>i5>>i6>>str1;
    if(i1==1){
      returnVal = CurrentAnalyzer->SysFileListInit(i1,i2,i3,i4,i5,i6,str1);
    }else if(i1==2){
      cin>>i7>>str2;
      returnVal = CurrentAnalyzer->SysFileListInit(i1,i2,i3,i4,i5,i6,str1,i7,str2);
    }else{
      cout << "Incorrect dimension.\n";
      returnVal = -1;
    }
  }else if(Cmd.compare("SystematicStudy_Condition")==0){	//Initialize Systematic study for 1 or 2 conditions
    cin>>i1>>FitRange.low>>FitRange.high>>i2>>str1>>i3>>dValue3>>dValue4>>str2;
    if(i2==1){
      returnVal = CurrentAnalyzer->SystematicStudy_Condition(i1,FitRange,i2,str1,i3,dValue3,dValue4,str2);
    }else if(i2==2){
      cin>>str3>>i4>>dValue5>>dValue6>>str4;
      returnVal = CurrentAnalyzer->SystematicStudy_Condition(i1,FitRange,i2,str1,i3,dValue3,dValue4,str2,str3,i4,dValue5,dValue6,str4);
    }else{
      cout << "Incorrect dimension.\n";
      returnVal = -1;
    }
    cout << "Systematic study with conditions complete."<<endl;
  }else if(Cmd.compare("SystematicStudy_Parameter")==0){	//Initialize Systematic study for 1 or 2 parameters
    cin>>i1>>FitRange.low>>FitRange.high>>i2>>str1>>i3>>dValue3>>dValue4>>str2;
    if(i2==1){
      returnVal = CurrentAnalyzer->SystematicStudy_Parameter(i1,FitRange,i2,str1,i3,dValue3,dValue4,str2);
    }else if(i2==2){
      cin>>str3>>i4>>dValue5>>dValue6>>str4;
      returnVal = CurrentAnalyzer->SystematicStudy_Parameter(i1,FitRange,i2,str1,i3,dValue3,dValue4,str2,str3,i4,dValue5,dValue6,str4);
    }else{
      cout << "Incorrect dimension.\n";
      returnVal = -1;
    }
    cout << "Systematic study with parameters complete."<<endl;
  }else if(Cmd.compare("SystematicStudy_File")==0){	//Initialize systematic study for 1 or 2 File conditions
    cin>>i1>>FitRange.low>>FitRange.high>>str1>>dValue3>>dValue4>>str2;
    if(i1==1){
      returnVal = CurrentAnalyzer->SystematicStudy_File(i1,FitRange,str1,dValue3,dValue4,str2);
    }else if(i1==2){
      cin>>str3>>dValue5>>dValue6>>str4;
      returnVal = CurrentAnalyzer->SystematicStudy_File(i1,FitRange,str1,dValue3,dValue4,str2,str3,dValue5,dValue6,str4);
    }else{
      cout << "Incorrect dimension.\n";
      returnVal = -1;
    }
    cout << "Systematic study with files complete."<<endl;
  }else if(Cmd.compare("SystematicStudy_Calibration")==0){	//Initialize systematic study for 1 File conditions
    cin>>FitRange.low>>FitRange.high>>dValue3>>dValue4>>dValue5>>dValue6>>str1;
    returnVal = CurrentAnalyzer->SystematicStudy_Calibration(FitRange,dValue3,dValue4,dValue5,dValue6,str1);
    cout << "Systematic calibration study."<<endl;
  }else if(Cmd.compare("SystematicStudy_Mixed")==0){	//Initialize systematic study for 1 file and 1 condition
    cin>>FitRange.low>>FitRange.high>>str1>>i1>>dValue3>>dValue4>>str2>>str3>>dValue5>>dValue6>>str4;
    returnVal = CurrentAnalyzer->SystematicStudy_Mixed(FitRange,str1,i1,dValue3,dValue4,str2,str3,dValue5,dValue6,str4);
    cout << "Systematic study with file and condition complete."<<endl;
  }else if(Cmd.compare("SystematicStudy_Rate")==0){  //Initialize systeamtic study for 1 file and 1 or 2 conditions
  	cin>>i1>>i2>>dValue1>>dValue2>>dValue3>>str1>>dValue4>>dValue5>>str2;
  	if(i1==1) returnVal = CurrentAnalyzer->SystematicStudy_Rate(i1,i2,dValue1,dValue2,dValue3,str1,dValue4,dValue5,str2);
  	else if (i1==2){
  		cin>>str3>>i3>>dValue6>>dValue7>>str4;
  		returnVal = CurrentAnalyzer->SystematicStudy_Rate(i1,i2,dValue1,dValue2,dValue3,str1,dValue4,dValue5,str2,str3,i3,dValue6,dValue7,str4);
    }else{
      cout << "Incorrect dimension.\n";
      returnVal = -1;
    }
    cout << "Systematic rate study complete."<<endl;
    /******************Experiment Related*******:****************/
  }else if(Cmd.compare("SetExpFilePrefix")==0){
    cin>>str1;
    returnVal = CurrentAnalyzer->SetExpFilePrefix(str1);
  }else if(Cmd.compare("SetMode")==0){
    cin>>str1;
    returnVal = CurrentAnalyzer->SetMode(str1);
  }else if(Cmd.compare("SetScattMode")==0){
    cin>>str1;
    returnVal = CurrentAnalyzer->SetScattMode(str1);
  }else if(Cmd.compare("ConfigBackground")==0){
    cin>>str1;
    if (str1.compare("OFF")==0)returnVal = CurrentAnalyzer->ConfigBackground(str1);
    else {
      cin>>dValue1>>dValue2>>dValue3;
      returnVal = CurrentAnalyzer->ConfigBackground(str1,dValue1,dValue2,dValue3);
    }
  }else if(Cmd.compare("ConfigPushBeamVeto")==0){
    cin>>str1;
    if (str1.compare("OFF")==0)returnVal = CurrentAnalyzer->ConfigPushBeamVeto(str1);
    else {
      cin>>dValue1>>dValue2;//Period, length of veto
      returnVal = CurrentAnalyzer->ConfigPushBeamVeto(str1,dValue1,dValue2);
    }
  }else if(Cmd.compare("ConfigLaserSwitch")==0){
    cin>>str1>>dValue1>>dValue2>>i1>>dValue3;
    returnVal = CurrentAnalyzer->ConfigLaserSwitch(str1,dValue1,dValue2,i1,dValue3);//LaserSelect (ON|OFF), PushPeriod, LaserPeriod,NCycles,WaitTime 
    //cin>>str1>>str2>>dValue1>>dValue2>>dValue3>>i1;
    //returnVal = CurrentAnalyzer->ConfigLaserSwitch(str1,str2,dValue1,dValue2,dValue3,i1);//IsSwitching (ON|OFF), LaserSelect (ON|OFF), PushPeriod, LaserPeriod, Tstart
  }else if(Cmd.compare("ConfigLEDCalibration")==0){
    cin>>str1;
    if (str1.compare("OFF")==0)returnVal = CurrentAnalyzer->ConfigLEDCalibration(str1);
    else {
      cin>>dValue1;
      returnVal = CurrentAnalyzer->ConfigLEDCalibration(str1,dValue1);
    }
  }else if (Cmd.compare("SetFileExpDim")==0){	//Set ExpFileID list dimensions
    cin >> i1 >>i2;
    returnVal = CurrentAnalyzer->SetFileExpDim(i1,i2);
    cout << "ExpFile dimensions are set to "<<i1<<" x "<<i2<<endl;
  }else if (Cmd.compare("SetFileIDEntryExp")==0){	//Set ExpFileID list entry
    cin >> i1 >>i2 >>i3;
    returnVal = CurrentAnalyzer->SetFileIDEntryExp(i1,i2,i3);
    cout << "FileIDList["<<i1<<"]["<<i2<<"] is set to "<<i3<<endl;
  }else if (Cmd.compare("SetFileIDListExp")==0){	//(Row Index, FileIDStart, IDInc) Set FileID list with initial IDs in row (index), file dimensions should be set first
    cin >> i1 >>i2 >>i3;
    returnVal = CurrentAnalyzer->SetFileIDListExp(i1,i2,i3);
  }else if(Cmd.compare("ProcessInputExp")==0){	//Process input file of experiment
    cout << "Processing input files from Experiment"<<endl;
    cin>>i1>>i2>>i3;
    returnVal = CurrentAnalyzer->ProcessInputExp(i1,i2,i3);
    /******************Background Functions*******:****************/
  }else if(Cmd.compare("SubtractRandomCoincidence")==0){
    cin>>str1;
    returnVal = CurrentAnalyzer->SubtractRandomCoincidence(str1);
    cout << "Random coincidence subtraction complete."<<endl;
  }else if(Cmd.compare("ConstructBkgHistogram")==0){
    cin>>str1;
    returnVal = CurrentAnalyzer->ConstructBkgHistogram(str1);
    cout << "Background histogram construction complete."<<endl;
  }else if(Cmd.compare("NormalizeBkg")==0){
    cin>>str1;
    returnVal = CurrentAnalyzer->NormalizeBkg(str1);
    cout << "Background histogram normalization complete."<<endl;
  }else if(Cmd.compare("ExpBkgSubtraction")==0){
    cin>>str1;
    returnVal = CurrentAnalyzer->ExpBkgSubtraction(str1);
    cout << "Background Subtraction complete."<<endl;
  }else if(Cmd.compare("SimBkgMixing")==0){
    cin>>str1;
    returnVal = CurrentAnalyzer->SimBkgMixing(str1);
    cout << "Background Mixing complete."<<endl;
  }else if(Cmd.compare("DelBkgHistogram")==0){
    returnVal = CurrentAnalyzer->DelBkgHistogram();
    cout << "Background histogram deleting complete."<<endl;
  }else if(Cmd.compare("SaveBkgHistogram")==0){
    cin>>i1;
    returnVal = CurrentAnalyzer->SaveBkgHistogram(i1);
    cout << "Background histogram saved."<<endl;
  }else if(Cmd.compare("LoadBkgHistogram")==0){
    cin>>i1;
    returnVal = CurrentAnalyzer->LoadBkgHistogram(i1);
    cout << "Background histogram loaded."<<endl;
  }else if(Cmd.compare("SetBkgNorm")==0){
    cin>>dValue1;
    returnVal = CurrentAnalyzer->SetBkgNorm(dValue1);
    cout << "BkgNorm set to " << dValue1<<endl;
  }else if(Cmd.compare("InitExtParList")==0){
    cin>>str1>>i1>>dValue1>>dValue2;
    if(i1>N_OF_HIST){
      cout<< "ERROR: External parameter length exceeds maximum N_OF_HIST: "<<N_OF_HIST<<endl;
      returnVal = -1;
    }
    returnVal = CurrentAnalyzer->InitExtParList(str1,i1,dValue1,dValue2);
    cout<< "External parameter list "<<str1<<" intitialized."<<endl;
  }else if(Cmd.compare("SetExtParList")==0){
    cin>>str1>>i1;
    if(i1>N_OF_HIST){
      cout<< "ERROR: External parameter length exceeds maximum N_OF_HIST: "<<N_OF_HIST<<endl;
      returnVal = -1;
    }
    double vals[N_OF_HIST];
    for(int i=0;i<i1;i++) cin>>vals[i];
    returnVal = CurrentAnalyzer->SetExtParList(str1,i1,vals);
    cout<< "External parameter list "<<str1<<" intitialized."<<endl;
    /******************User Functions*******:****************/
  }else if(Cmd.compare("User_MatchBetaSpectra")==0){
    cin>>i1>>str1>>dValue1>>dValue2>>str2;
    if(i1==1){
      returnVal = CurrentAnalyzer->User_MatchBetaSpectra(i1,str1,dValue1,dValue2,str2);
    }else if(i1==2){
      cin>>str3>>dValue3>>dValue4>>str4;
      returnVal = CurrentAnalyzer->User_MatchBetaSpectra(i1,str1,dValue1,dValue2,str2,str3,dValue3,dValue4,str4);
    }else{
      cout << "Incorrect dimension.\n";
      returnVal = -1;
    }
    cout << "User function MatchBetaSpectra complete."<<endl;
  }else if(Cmd.compare("User_CompareMCPMap")==0){
    cin>>str1>>i1;
    returnVal = CurrentAnalyzer->User_CompareMCPMap(str1,i1);
    cout << "User function CompareMCPMap complete."<<endl;
  }else if(Cmd.compare("User_MCPGainStudy")==0){
    cin>>str1>>dValue1>>dValue2;
    returnVal = CurrentAnalyzer->User_MCPGainStudy(str1,dValue1,dValue2);
    cout << "User function MCPGainStudy complete."<<endl;
  }else if(Cmd.compare("User_MWPCScatStudy")==0){
    cin>>dValue1>>str1;
    returnVal = CurrentAnalyzer->User_MWPCScatStudy(dValue1,str1);
    cout << "User function MWPCScatStudy complete."<<endl;
  }else if(Cmd.compare("User_CompareSpectra")==0){
    cin>>str1>>i1>>str2>>i2>>str3>>i3>>str4;
    bool bool1;
    if (str3.compare("True")==0)bool1=true;
    else if (str3.compare("False")==0)bool1=false;
    else{
      cout <<"Incorrect Boolean input "<<str3<<endl;
      cout  <<"Set to false by default."<<endl;
      bool1=false;
    }
    returnVal = CurrentAnalyzer->User_CompareSpectra(str1,i1,str2,i2,bool1,i3,str4);
    cout << "User function CompareSpectra complete."<<endl;
  }else if(Cmd.compare("User_PhotoIonTOFStudy")==0){
    cin>>dValue1>>dValue2>>dValue3>>dValue4>>str1;
    returnVal = CurrentAnalyzer->User_PhotoIonTOFStudy(dValue1,dValue2,dValue3,dValue4,str1);
    cout << "User function PhotoIonTOFStudy complete."<<endl;
  }else if(Cmd.compare("User_PhotoIonRateStudy")==0){
    cin>>str1>>str2>>str3;
    returnVal = CurrentAnalyzer->User_PhotoIonRateStudy(str1,str2,str3);
    cout << "User function PhotoIonRateStudy complete."<<endl; 
  }else if(Cmd.compare("User_QMCPStudy")==0){
    cin>>str1>>str2>>str3>>str4>>dValue1>>dValue2;
    returnVal = CurrentAnalyzer->User_QMCPStudy(str1,str2,str3,str4,dValue1,dValue2);
    cout << "User function QCMPStudy complete."<<endl; 
  }else if(Cmd.compare("User_LocalRateTOFStudy")==0){
    cin>>str1>>str2>>str3;
    returnVal = CurrentAnalyzer->User_LocalRateTOFStudy(str1,str2,str3);
    cout << "User function LocalRateTOFStudy complete."<<endl; 
  }else if(Cmd.compare("User_LaserSwitchStartStudy")==0){
    cin>>str1>>str2>>str3>>dValue1>>dValue2;
    returnVal = CurrentAnalyzer->User_LaserSwitchStartStudy(str1,str2,str3,dValue1,dValue2);
    cout << "User function User_LaserSwitchStartStudy complete."<<endl;
  }else if(Cmd.compare("User_TOFFit")==0){
    cin>>str1>>str2>>dValue1>>dValue2;
    returnVal = CurrentAnalyzer->User_TOFFit(str1,str2,dValue1,dValue2);
    cout << "User function User_TOFFit complete."<<endl;
  }else if(Cmd.compare("User_TOFFit_2DCond")==0){
    cin>>str1>>str2>>str3;
    returnVal = CurrentAnalyzer->User_TOFFit_2DCond(str1,str2,str3);
    cout << "User function User_TOFFit_2DCond complete."<<endl;
  }else if(Cmd.compare("User_Gauss1DFit")==0){
    cin>>str1>>str2;
    returnVal = CurrentAnalyzer->User_Gauss1DFit(str1,str2);
    cout << "User function User_Gauss1DFit complete."<<endl;
  }else if(Cmd.compare("User_MOT2DFit")==0){
    cin>>str1>>i1;
    returnVal = CurrentAnalyzer->User_MOT2DFit(str1,i1);
    cout << "User function User_MOT2DFit complete."<<endl;
  }else if(Cmd.compare("User_Monitor")==0){
  	 cout<<"User_Monitor function:"<<endl;
    cin>>str1>>str2;
    returnVal = CurrentAnalyzer->User_Monitor(str1,str2);
    cout << "User function Monitor complete."<<endl;
  }else if(Cmd.compare("User_MOTEvolution")==0){
    cin>>dValue1>>dValue2>>i1;
    returnVal = CurrentAnalyzer->User_MOTEvolution(dValue1,dValue2,i1);
    cout << "User function MOTEvolution complete."<<endl;
  }else if(Cmd.compare("User_Monitor_Penning")==0){
    cin>>str1>>dValue1>>dValue2;
    returnVal = CurrentAnalyzer->User_Monitor_Penning(str1,dValue1,dValue2);
    cout << "User function Monitor Penning complete."<<endl;
  }else if(Cmd.compare("User_ChargeState3Ana")==0){
    cin>>dValue1>>str1>>str2;
    returnVal = CurrentAnalyzer->User_ChargeState3Ana(dValue1,str1,str2);
    cout << "User function ChargeState3Ana complete."<<endl;
  }else if(Cmd.compare("User_ChargeState12Ana")==0){
    cin>>str1;
    returnVal = CurrentAnalyzer->User_ChargeState12Ana(str1);
    cout << "User function ChargeState3Ana complete."<<endl;
  }else if(Cmd.compare("User_RawDataStats")==0){
    returnVal = CurrentAnalyzer->User_RawDataStats();
    cout << "User function User_RawDataStats complete."<<endl;
    /**************Loading calibration data*****************************/
  }else if(Cmd.compare("CalConfigSim")==0){//Configure calibration for simulation
    cin >> str1>>str2;
    returnVal = CurrentAnalyzer->CalConfigSim(str1,str2);
  }else if(Cmd.compare("SetCalIDSim")==0){//Set simulation calibration ID
    cin >> str1>>i1;
    returnVal = CurrentAnalyzer->SetCalIDSim(str1,i1);
  }else if(Cmd.compare("CalConfigExp")==0){//Configure calibration for experiment
    cin >> str1>>str2;
    returnVal = CurrentAnalyzer->CalConfigExp(str1,str2);
  }else if(Cmd.compare("SetCalIDExp")==0){//Set experiment calibration ID
    cin >> str1>>i1;
    returnVal = CurrentAnalyzer->SetCalIDExp(str1,i1);
  }else if(Cmd.compare("SetMWPCPosCalMode")==0){//Set MWPC position calibration 
    cin >> str1;
    returnVal = CurrentAnalyzer->SetMWPCPosCalMode(str1);
  }else if(Cmd.compare("SetMWPCKeepLessTrigger")==0){//Decide whether to keep trigger less than 2
    cin >> i1;
    returnVal = CurrentAnalyzer->SetMWPCKeepLessTrigger(i1);
  }else if(Cmd.compare("SetExpMCPCalMode")==0){//Set MCP position calibration 
    cin >> str1;
    returnVal = CurrentAnalyzer->SetExpMCPCalMode(str1);
  }else if(Cmd.compare("SetT0CorrMode")==0){//Set T0Corr to local or global correction 
    cin >> str1;
    returnVal = CurrentAnalyzer->SetT0CorrMode(str1);
  }else if(Cmd.compare("SetT0CorrStrength")==0){//Set T0Corr magnitude multiplier
    cin >> dValue1;
    returnVal = CurrentAnalyzer->SetT0CorrStrength(dValue1);
    /**************Loading calibration data*****************************/
  }else if (Cmd.compare("Calibration/ConfigCalMWPC")==0){ //Configure Calibration of MWPC
    cin >> str1 >> i1;
    boolVal = bool(i1);
    returnVal = CurrentCalibration-> ConfigCalMWPC(str1,boolVal);
  }else if (Cmd.compare("Calibration/CalibrateMWPCPreview")==0){ //Calibration of MWPC preview, plot hisgotrams
    cin >> str1 >> i1 >> str2;
    returnVal = CurrentCalibration->CalibrateMWPCPreview(str1,i1,str2);
  }else if (Cmd.compare("Calibration/CalibrateMWPC")==0){ //Calibration of MWPC, plot hisgotrams
    cin >> str1 >> i1 >> i2 >> str2;
    returnVal = CurrentCalibration->CalibrateMWPC(str1,i1,i2,str2);
  }else if (Cmd.compare("Calibration/CalibrateBeta")==0){ //Calibration of Beta detectors, plot hisgotrams
    cin >> str1 >> i1 >> i2 >> i3 >> str2>>str3;
    returnVal = CurrentCalibration->CalibrateBeta(str1,i1,i2,i3,str2,str3);
  }else if (Cmd.compare("Calibration/CalibrateLED")==0){ //Calibration of Scint using LED, plot hisgotrams
    cin >> str1 >> i1 >> i2 >> str2 >> dValue1;
    returnVal = CurrentCalibration->CalibrateLED(str1,i1,i2,str2,dValue1);
  }else if (Cmd.compare("Calibration/CalibrateTimingZero")==0){ //Calibration of Timing Zero
    cin >> str1 >> i1 >> i2;
    returnVal = CurrentCalibration->CalibrateTimingZero(str1,i1,i2);
  }else if (Cmd.compare("Calibration/CalibrateTiming")==0){ //Calibration Timing
    // cin >> i1 >> i2;
    returnVal = CurrentCalibration->CalibrateTiming();
  }else if (Cmd.compare("Calibration/CalibrateMCPEff_Simple")==0){ //Calibration Timing
    cin >>str1 >>str2 >> i1;
    returnVal = CurrentCalibration->CalibrateMCPEff_Simple(str1,str2,i1);
  }else if (Cmd.compare("Calibration/CalibrateMCPEff")==0){ //Calibration Timing
    cin >>str1 >>i1 >> i2>>str2;
    returnVal = CurrentCalibration->CalibrateMCPEff(str1,i1,i2,str2);
  }else if (Cmd.compare("Calibration/ConfigCalMCP")==0){ //Configure Calibration of MCP
    cin >> str1 >> dValue1 >> dValue2 >> dValue3 >> dValue4 >> dValue5; 
    returnVal = CurrentCalibration->ConfigCalMCP(str1,dValue1,dValue2,dValue3,dValue4,dValue5);
  }else if (Cmd.compare("Calibration/CalibratePreviewMCP")==0){ //Preview MCP, plot hisgotrams
    cin >> str1 >> i1;;
    returnVal = CurrentCalibration->CalibratePreviewMCP(str1,i1);
  }else if (Cmd.compare("Calibration/CalibrateMCP")==0){ //Calibration of MCP, plot hisgotrams
    cin >> str1 >> i1 >> i2 >> str2;
    returnVal = CurrentCalibration->CalibrateMCP(str1,i1,i2,str2);
  }else if (Cmd.compare("Calibration/CalibrateMCPRes")==0){ //Calibration of MCP, plot hisgotrams
    cin >> str1 >> i1 >> i2;
    returnVal = CurrentCalibration->CalibrateMCPRes(str1,i1,i2);
  }else if (Cmd.compare("Calibration/CalibrateMOT")==0){ //Calibration of MCP, plot hisgotrams
    cin >> str1 >> i1 >> i2;
    returnVal = CurrentCalibration->CalibrateMOT(str1,i1,i2);
  }else if (Cmd.compare("Calibration/CalibrateMOTSimple")==0){ //Calibration of MCP, plot hisgotrams
    cin >> i1;
    returnVal = CurrentCalibration->CalibrateMOTSimple(i1);
  }else if (Cmd.compare("Calibration/CalibrateMOTPicture")==0){ //Calibration of MCP using the Picture
    cin >> str1>>str2>>str3>>i1;
    returnVal = CurrentCalibration->CalibrateMOTPicture(str1,str2,str3,i1);
  }else if (Cmd.compare("Calibration/CalibrateTimingRes")==0){ //Calibration Timing Resolution
    // cin >> i1 >> i2;
    returnVal = CurrentCalibration->CalibrateTimingRes();
  }else if (Cmd.compare("Calibration/CalibrateMWPCPreviewSim")==0){ //Calibration of MWPC Simulation, Preview
    cin >> i1;
    returnVal = CurrentCalibration->CalibrateMWPCPreviewSim(i1);
  }else if (Cmd.compare("Calibration/CalibrateMWPCSim")==0){ //Calibration of MWPC Simulation, plot hisgotrams
    cin >> i1 >> i2;
    returnVal = CurrentCalibration->CalibrateMWPCSim(i1,i2);
  }else if (Cmd.compare("Calibration/CalibrateBetaSim")==0){ //Calibration of MWPC, plot hisgotrams
    cin >> i1 >> i2 >> str2;
    returnVal = CurrentCalibration->CalibrateBetaSim(i1,i2,str2);
  }else if (Cmd.compare("Calibration/GenerateQMap")==0){ //Generate map of mean MCP charge for dValue1 by dValue2 cells circumscribed by MCP radius for outtrees file str1, file index i3 i4, output map to calid i1
    cin >> str1 >> i2 >> i3 >> dValue1 >> dValue2 >> dValue3 >> i1;
    T0Correction t0corr(str1.c_str(),i2,i3);
    returnVal = t0corr.GenerateQMap(dValue1,dValue2,dValue3,i1);
  }else if (Cmd.compare("Calibration/GenerateT0Map")==0){ //Generate T0 Correction map i2 from QMap calibration file i1 for outtrees file str1, file index i3 i4, dValue1 and dValue2 are TOF fit limits
    cin >> str1 >> i3 >> i4 >> i1 >> i2 >> dValue1 >> dValue2; 
    T0Correction t0corr(str1.c_str(),i3,i4);
    t0corr.InitQSlices(CurrentAnalyzer->GetExpCondition("MCP_QLow"),CurrentAnalyzer->GetExpCondition("MCP_QHigh"));
    returnVal = t0corr.GenerateT0CorrectionMap(i1,i2,dValue1,dValue2);
  /***********************************************************/
  }else if(Cmd.compare(0,1,"#")==0){		//Comment line, ignore up to 500 characters
    cin.getline(CharBuffer,500);
    return 2;
  }else{					//Command not found
    cout<<"Command "<<Cmd <<" is not found!\n";	
    return 3;
  }
  //cout<< returnVal<<endl;
  return returnVal;
}

void AnalyzerUI::LS()
{
  cout<<"List of Commands:\n"
    <<"****************Basic controls:****************\n"
    <<"exit------------------------------------------------------Quit\n"
    <<"ls--------------------------------------------------------List commands\n"
    <<"lsHist----------------------------------------------------List histograms\n"
    <<"lsAllConditions-----------------------------------------------List all available conditions\n"
    <<"lsEnabledConditions-------------------------------------------List enabled conditions \n"
    <<"SetPurpose int int----------------------------------------Set Purpose (0 for simulation,1 for experiment; 0 for He6 decay,1 for calibration)\n"
    <<"ProcessInputSim FileDim ConDim ParaDim--------------------Process input file from simulation, given file dimension, condition dimension and parameter dimension, 0 ,1 or 2, total up to 2\n"
    <<"****************Histogram Operations:****************\n"
    <<"EnableHist HistName-------------------------------------------Enable histogram filling. Name, \"None\" or \"All\"\n"
    <<"DisableHist HistName------------------------------------------Disable histogram filling. Name, \"None\" or \"All\"\n"
    <<"EnableBkgHist HistName----------------------------------------Enable background histogram filling. Name, \"None\" or \"All\"\n"
    <<"DisableBkgHist HistName---------------------------------------Disable background histogram filling. Name, \"None\" or \"All\"\n"
    <<"ConfigHistogram HistName xlow xhigh nxbins-----------------Configure 1D histogram range and binning.\n"
    <<"Config2DHistogram HistName xlow xhigh nxbins ylow yhigh nybins -----------------Configure 2D histogram range and binning.\n"
    <<"SaveHistograms FileName (option)--------------------------Save histograms to a root file,option(only for exp): Array Combined Both\n"
    <<"LoadHistograms FileName (option)--------------------------Load histograms from a root file,option(only for exp): Array Combined\n"
    <<"SetStdHist HistName option--------------------------------Set current histogram (HistName) to standard histogram, options = (aPlus or aMinus)\n"
    <<"SaveStdHist HistName FileName-----------------------------Save standard histograms to a root file\n"
    <<"LoadStdHist HistName FileName-----------------------------Load standard histograms from a root file\n"
    <<"AddSlicedHist HistName Slice_Condition--------------------Add a sliced histogram to the list\n"
    <<"ClearSlicedHistList---------------------------------------Clear the sliced histogram list\n"
    <<"DeleteCombHistList----------------------------------------Delete the combined histogram list\n"
    <<"CombineExpHistograms CombDim BkgOption--------------------Combine histograms\n"
    <<"****************Systematic Condition settings:****************\n"
    <<"InitCondition Name N low interval-------------------------Initialize condition list with length N, start from low with interval increment\n"
    <<"SetCondition Name i value---------------------------------Set one entry i of a condition list with Name to value\n"
    <<"SetDefaultConditions--------------------------------------Set all conditions to default values\n"
    <<"AddCondLoopPtr index Name---------------------------------Loop condition with Name, index = 1 or 2\n"
    <<"UnSetCondLoopPtrs-----------------------------------------Unset the loopings\n"
    <<"CheckConditionSettings------------------------------------Check Condition settings, set condition dimension correctly\n"
    <<"SetMCPefficiencyMap ON/OFF--------------------------------Turn on or off the MCP efficiency map, 0 Off, 1 On\n"
    <<"InitSliceBoundary N low interval--------------------------Initialize the slice boundary list (N sections), start from low with interval increment\n"
    <<"SetFileCondLnk ON/OFF-------------------------------------Correlate condition loops to File ID\n"
    <<"****************Parameter  settings:****************\n"
    <<"ResetParameters-------------------------------------------Set all Parameters to default values\n"
    <<"InitParameterArray Name N low interval--------------------Initialize Parameter list with length N, start from low with interval increment\n"
    <<"SetParameter Name i value---------------------------------Set one entry i of a Parameter list with Name to value\n"
    <<"SetParameterLoop Name index-------------------------------Parameter Loop condition with Name, index = 1 or 2\n"
    <<"UnsetParameterLoop Name-----------------------------------Unset Parameter loop\n"
    <<"ConstructParameterList------------------------------------Check Parameter settings,and construct parameter lists for reconstruction\n"
    <<"****************File settings:****************\n"
    <<"SetFileSimDim int int-------------------------------------Set FileID list dimensions\n"
    <<"SetFileIDEntrySim index1 index2 ID-G ID-B ID-I ID-P-------Set FileID list entry \n"
    <<"SetFileIDListSim index ID-G ID-B ID-I ID-P Inc------------Set FileID list with initial IDs in row (index), file dimensions should be set first, Inc specifies which ID to increase \n"
    <<"SetFileIDListSim2D Inc------------------------------------Set FileID 2D list using the first row\n"
    <<"InitSysWFile Name N low interval--------------------------Initialize file condition list with length N, start from low with interval increment\n"
    <<"SetSysWFile Name i val------------------------------------Set one entry i of a file condition list with Name to value\n"
    <<"****************Fit operations:****************\n"
    <<"FitConfig Type TimeShift----------------------------------Configure Fit, Type = MINUIT or ROOT or HONG,TimeShift = ON/OFF\n"
    <<"InitFit FitHistName---------------------------------------Initialize histograms and result array for fit\n"
    <<"Fit_TOF index1 index2 rangel_low range_high---------------Fit one histogram with index1 and index2 within a range\n"
    <<"****************Output operations:****************\n"
    <<"OpenSpecFile SpecFileName---------------------------------Open spectrum file with a name\n"
    <<"CloseSpecFile---------------------------------------------Close the spectrum file\n"
    <<"PlotHistogram HistName index1 index2----------------------Plot one histogram with index 1 and 2 on a page\n"
    <<"PlotCombHistogram HistName index Select-------------------Plot one combined histogram with index on a page,select Exp or Bkg\n"
    <<"PlotHistogramList HistName iStart iEnd jStart jEnd--------Plot the whole list of histograms\n"
    <<"OutputSpectraGeneral index1 index2------------------------Output general spectra for histogram[index1][index2]\n"
    <<"OutputFittedHists HistName iStart iEnd jStart jEnd--------Output Fitted histograms\n"
    <<"ShowTitle Option------------------------------------------Show Title on graph, Option = On/OFF\n"
    <<"ShowAxisTitle Option--------------------------------------Show Axis Title on graph, Option = On/OFF\n"
    <<"ShowGrid Option-------------------------------------------Show Grid, Option = On/OFF\n"
    <<"SetDrawOption2DHist Option--------------------------------Set DrawOptions for 2D histogram drawing\n"
    <<"SetDrawOption2DGraph Option-------------------------------Set DrawOptions for 2D graph drawing\n"
    <<"OutputHistToTxt HistName filename_preffix-----------------Output histogram bin centers and contents to tab-separated text file\n"
    <<"****************Integrated Systematic Study:****************\n"
    <<"SetSysAxis Dim Property1 Name1 Property2 Name2------------Set Axis for the systematic graph, Property: Condition/File\n"
    <<"InitGraph Dim GraphName XTitle YTitle ZTitle--------------Initialize systematic graph\n"
    <<"SystematicFit_TOF GraphName low high Dim iLim jLim--------Fit TOF and set the fit results to the graph\n"
    <<"SysPlotGraph Dim GraphName Purpose1 Purpose2--------------Plot the systematic graph\n"
    <<"SysFileListInit Dim BaseID-G BaseID-B BaseID-I BaseID-P dim1 Inc1 dim2 Inc2---------------------------------------------------Initialize the systematic file list\n"
    <<"SystematicStudy_Condition fileID Rangelow Rangehigh ConDim Purpose1 N1 low1 interval1 XUnit Purpose2 N2 low2 interval2 YUnit--Conduct Systematic study for 1 or 2 conditions\n"
    <<"SystematicStudy_Parameter fileID Rangelow Rangehigh ConDim Purpose1 N1 low1 interval1 XUnit Purpose2 N2 low2 interval2 YUnit--Conduct Systematic study for 1 or 2 parameters\n"
    <<"SystematicStudy_Calibration Rangelow Rangehigh StartScale interval1 E0 Purpose1-----------------------------------------------Conduct Systematic study for 1 file\n"
    <<"SystematicStudy_File Dim rangelow rangehigh Purpose1 low1 interval1 XUnit Purpose2 low2 interval2 YUnit-----------------------Conduct systematic study for 1 or 2 File conditions\n"
    <<"SystematicStudy_Mixed rangelow rangehigh PurposeCon N1 low1 interval1 XUnit PurposeFile low2 interval2 YUnit------------------Conduct systematic study for 1 file and 1 condition\n"
    <<"SystematicStudy_Rate CondDim N_GT GT_inc GT_lo GT_hi Purpose1 low1 interval1 XUnit Purpose2 N2 low2 interval2 YUnit--------Conduct systematic rate study for 1 file, N_GT time intergals, and N2 conditions.\n"
    <<"******************Experiment Related*******:****************\n"
    <<"SetExpFilePrefix NamePrefix-------------------------------Set the experiment data file name prefix\n"
    <<"SetMode mode----------------------------------------------Set Read mode\n"
    <<"SetScattMode mode-----------------------------------------Set Scatt mode\n"
    <<"ConfigBackground Switch BkgTime BkgRatio------------------Configure background treatment, Switch=On/OFF BkgTime:bkg start time BkgRatio:Bkg subtraction ratio\n"
    <<"SetFileExpDim int int-------------------------------------Set FileID list dimensions\n"
    <<"SetFileIDEntryExp index1 index2 ID------------------------Set FileID list entry \n"
    <<"SetFileIDListExp index startID Inc------------------------Set FileID list with initial IDs in row (j index), file dimensions should be set first, Inc specifies which ID to increase \n"
    <<"ProcessInputExp FileDim ConDim ParaDim--------------------Process input file from experiment\n"
    <<"*********************Loading calibration data****************\n"
    <<"CalConfigSim name ON/OFF----------------------------------Turn Sim-Calibration ON/OFF\n"
    <<"CalConfigExp name ON/OFF----------------------------------Turn Exp-Calibration ON/OFF\n"
    <<"SetCalIDSim name ID---------------------------------------Set Sim-Calibration ID\n"
    <<"SetCalIDExp name ID---------------------------------------Set Sim-Calibration ID\n";
}
