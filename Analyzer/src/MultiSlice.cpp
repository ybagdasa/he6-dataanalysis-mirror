// combined fit of two histogram with separate functions but a common parameter
// N.B. this macro must be compiled (with ACliC or in a C++ application) 

// Version 2: working with ROOT versions >= 5.26 


//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>

//ROOT includes
#include "Fit/Fitter.h"
#include "Fit/BinData.h"
#include "Fit/Chi2FCN.h"
#include "TList.h"
#include "Math/WrappedMultiTF1.h"
#include "HFitInterface.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TSpline.h"
#include "TH1F.h"

//Project includes
//#include "Analyzer.h"

using namespace std;

typedef double (*Spline_Function_Array) (double *x, double parms[]);

double spline_slice0_func(double *x, double parms[]);
double spline_slice1_func(double *x, double parms[]);
double spline_slice2_func(double *x, double parms[]);
double spline_slice3_func(double *x, double parms[]);
double spline_slice4_func(double *x, double parms[]);
double spline_slice5_func(double *x, double parms[]);
double spline_slice6_func(double *x, double parms[]);
double spline_slice7_func(double *x, double parms[]);
double spline_slice8_func(double *x, double parms[]);
double spline_slice9_func(double *x, double parms[]);


//double Std_First_N=0;  //Normalization of STD histograms
//double Std_Second_N=0;

double Std_First_N_slice[10]={0,0,0,0,0,0,0,0,0,0};  //Normalization of STD histograms
double Std_Second_N_slice[10]={0,0,0,0,0,0,0,0,0,0};

//TSpline5 *spline_plus=NULL;
//TSpline5 *spline_minus=NULL;

TSpline5 *spline_minus_slice[10]={NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};
TSpline5 *spline_plus_slice[10]={NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

struct GlobalChi2{
  GlobalChi2(  ROOT::Math::IMultiGenFunction * f1,  ROOT::Math::IMultiGenFunction * f2,  ROOT::Math::IMultiGenFunction * f3,  ROOT::Math::IMultiGenFunction * f4,  ROOT::Math::IMultiGenFunction * f5,   ROOT::Math::IMultiGenFunction * f6,   ROOT::Math::IMultiGenFunction * f7,   ROOT::Math::IMultiGenFunction * f8,   ROOT::Math::IMultiGenFunction * f9,   ROOT::Math::IMultiGenFunction * f10) :
    fChi2_1(f1), fChi2_2(f2), fChi2_3(f3), fChi2_4(f4), fChi2_5(f5), fChi2_6(f6), fChi2_7(f7), fChi2_8(f8), fChi2_9(f9), fChi2_10(f10) {}

  // parameter vector is first background (in common 1 and 2) and then is signal (only in 2)
  double operator() (const double *par) const {
    double p1[3],p2[3],p3[3],p4[3],p5[3],p6[3],p7[3],p8[3],p9[3],p10[3];
    p1[0] = par[0]; // amplitude of plots
    p2[0] = par[1]; // amplitude of plots
    p3[0] = par[2]; // amplitude of plots
    p4[0] = par[3]; // amplitude of plots
    p5[0] = par[4]; // amplitude of plots
    p6[0] = par[5]; // amplitude of plots
    p7[0] = par[6]; // amplitude of plots
    p8[0] = par[7]; // amplitude of plots
    p9[0] = par[8]; // amplitude of plots
    p10[0] = par[9]; // amplitude of plots

    p1[1] = par[10]; // beta-neutrino correlation 'a'
    p2[1] = par[10]; // beta-neutrino correlation 'a'
    p3[1] = par[10]; // beta-neutrino correlation 'a'
    p4[1] = par[10]; // beta-neutrino correlation 'a'
    p5[1] = par[10]; // beta-neutrino correlation 'a'
    p6[1] = par[10]; // beta-neutrino correlation 'a'
    p7[1] = par[10]; // beta-neutrino correlation 'a'
    p8[1] = par[10]; // beta-neutrino correlation 'a'
    p9[1] = par[10]; // beta-neutrino correlation 'a'
    p10[1] = par[10]; // beta-neutrino correlation 'a'

    p1[2] = par[11]; // TOFfset
    p2[2] = par[11]; // TOFfset
    p3[2] = par[11]; // TOFfset
    p4[2] = par[11]; // TOFfset
    p5[2] = par[11]; // TOFfset
    p6[2] = par[11]; // TOFfset
    p7[2] = par[11]; // TOFfset
    p8[2] = par[11]; // TOFfset
    p9[2] = par[11]; // TOFfset
    p10[2] = par[11]; // TOFfset

    double returnVal=0;
    if (fChi2_1!=NULL) returnVal+=(*fChi2_1)(p1);
    if (fChi2_2!=NULL) returnVal+=(*fChi2_2)(p2);
    if (fChi2_3!=NULL) returnVal+=(*fChi2_3)(p3);
    if (fChi2_4!=NULL) returnVal+=(*fChi2_4)(p4);
    if (fChi2_5!=NULL) returnVal+=(*fChi2_5)(p5);
    if (fChi2_6!=NULL) returnVal+=(*fChi2_6)(p6);
    if (fChi2_7!=NULL) returnVal+=(*fChi2_7)(p7);
    if (fChi2_8!=NULL) returnVal+=(*fChi2_8)(p8);
    if (fChi2_9!=NULL) returnVal+=(*fChi2_9)(p9);
    if (fChi2_10!=NULL) returnVal+=(*fChi2_10)(p10);
    return returnVal;
  }

  const  ROOT::Math::IMultiGenFunction * fChi2_1;
  const  ROOT::Math::IMultiGenFunction * fChi2_2;
  const  ROOT::Math::IMultiGenFunction * fChi2_3;
  const  ROOT::Math::IMultiGenFunction * fChi2_4;
  const  ROOT::Math::IMultiGenFunction * fChi2_5;
  const  ROOT::Math::IMultiGenFunction * fChi2_6;
  const  ROOT::Math::IMultiGenFunction * fChi2_7;
  const  ROOT::Math::IMultiGenFunction * fChi2_8;
  const  ROOT::Math::IMultiGenFunction * fChi2_9;
  const  ROOT::Math::IMultiGenFunction * fChi2_10;
};


struct FitResultLocal{
    double fit_val[10];
    double fit_error[10];
};


//FitResult MultiSlice();

double MultiSlice(int floatTime, int N_Slice,double FitRange_low,double FitRange_high){

  FitResultLocal Results;

  //    double FitRange_low = 150;
  //    double FitRange_high = 400;

  TFile *MultiSliceFile = new TFile("MultiSliceFile.root","READ");

  TH1 *Std_First_slice[10];
  TH1 *Std_Second_slice[10];
  TH1 *TOFHist_slice[10];

  for(int j=0;j < N_Slice; j++){
    Std_First_slice[j]=(TH1*)MultiSliceFile->Get(Form("Std_First_Slice%d",j));
    Std_Second_slice[j]=(TH1*)MultiSliceFile->Get(Form("Std_Second_Slice%d",j));
    TOFHist_slice[j]=(TH1*)MultiSliceFile->Get(Form("TOFHist_Slice%d",j));
  }

  TF1 *fit_func_slice[10];
  ROOT::Math::WrappedMultiTF1 *wrapped_func[10];

  Spline_Function_Array spline_func_slice[] =
  {
    spline_slice0_func,
    spline_slice1_func,
    spline_slice2_func,
    spline_slice3_func,
    spline_slice4_func,
    spline_slice5_func,
    spline_slice6_func,
    spline_slice7_func,
    spline_slice8_func,
    spline_slice9_func,
  };

  for(int j=0; j< N_Slice; j++){
    Std_First_N_slice[j] = Std_First_slice[j]->GetSumOfWeights();
    Std_Second_N_slice[j] = Std_Second_slice[j]->GetSumOfWeights();

    spline_plus_slice[j] = new TSpline5((TH1*)Std_First_slice[j],0,FitRange_low,FitRange_high);
    spline_minus_slice[j] = new TSpline5((TH1*)Std_Second_slice[j],0,FitRange_low,FitRange_high);


    fit_func_slice[j] = new TF1(Form("fit_func_slice%d",j),spline_func_slice[j],FitRange_low,FitRange_high,3);
    fit_func_slice[j]->SetParameter(0,TOFHist_slice[j]->GetSumOfWeights());
    fit_func_slice[j]->SetParameter(1,0);
    fit_func_slice[j]->SetParameter(2,0);

    wrapped_func[j] = new ROOT::Math::WrappedMultiTF1(*fit_func_slice[j],0);
  }
  //Set the rest of splines and histograms 0

  // *******************************************************************
  // *************** Set Fit Options & Set Data Range ******************
  // *******************************************************************

  ROOT::Fit::DataOptions opt;
  opt.fAsymErrors = 0;
  opt.fIntegral = 0;

  ROOT::Fit::DataRange range;
  range.SetRange(FitRange_low,FitRange_high);

  // *******************************************************************
  // *************** Set Up Binned Data & Define Chi2 ******************
  // *******************************************************************

  ROOT::Fit::BinData *bin_data[10];
  ROOT::Fit::Chi2Function *chi2_funcs[10];
  for(int j=0; j< 10; j++){
    bin_data[j] = NULL; 
    chi2_funcs[j] = NULL;
  }

  for(int j=0; j< N_Slice; j++){
    bin_data[j] = new ROOT::Fit::BinData(opt,range);
    ROOT::Fit::FillData(*bin_data[j],TOFHist_slice[j]);

    chi2_funcs[j] = new ROOT::Fit::Chi2Function(*bin_data[j],*wrapped_func[j]);
  }

  GlobalChi2 chi2_sum(chi2_funcs[0],chi2_funcs[1],chi2_funcs[2],chi2_funcs[3],chi2_funcs[4],chi2_funcs[5],chi2_funcs[6],chi2_funcs[7],chi2_funcs[8],chi2_funcs[9]);

  // *******************************************************************
  // ************* Load Fitter, Initialize & Setup Pars ****************
  // *******************************************************************

  ROOT::Fit::Fitter fitter;

  double pars[12];
  for(int j=0; j<10; j++){
    pars[j] = 0.0;
  }

  cout << N_Slice<<endl;
  for(int j=0; j<N_Slice; j++){
    pars[j] = TOFHist_slice[j]->GetSumOfWeights();
    cout << pars[j] << endl;
  }

  pars[10] = -0.25; //initialize 'a' = 0
  pars[11] = 0.0; //initialize TOFfset = 0

  fitter.Config().SetParamsSettings(12,pars);

  for(int j=0; j<10; j++){
    fitter.Config().ParSettings(j).Fix();
  }

  fitter.Config().ParSettings(10).SetLimits(-0.5,0.5);

  if(floatTime){
    cout << "Allowing fits to float.\n";
    fitter.Config().ParSettings(11).SetLimits(-50,50);
  } else{
    cout << "Fixing leading edge of fits.\n";
    fitter.Config().ParSettings(11).Fix();
  }

  unsigned int data_size=0;

  for(int j=0;j<N_Slice;j++){
    data_size += bin_data[j]->Size();
  }

  fitter.FitFCN(12,chi2_sum,pars,data_size,true);

  // *******************************************************************
  // *************************** Run Fitter ****************************
  // *******************************************************************

  ROOT::Fit::FitResult result = fitter.Result();
  result.Print(std::cout);

  cout << "Chi2 = "<< result.MinFcnValue() << " : NDF = " << result.Ndf() << endl;
  cout << "TimeShift = " << result.Value(11) << endl;


  Results.fit_val[0] = result.Value(10);
  Results.fit_error[0] = result.Error(10);
  Results.fit_val[1] = result.Value(11);
  Results.fit_error[1] = result.Error(11);

  cout << "Fitting complete!"<<endl;
  cout << "a = " <<Results.fit_val[0] << " +/- "<<Results.fit_error[0]<<endl;

  TH1* TOF_Fit_slice[10];
  TH1* TOF_Res_slice[10];

  //Construct histogram
  for(int j=0; j<N_Slice; j++){

    fit_func_slice[j]->SetParameters(result.Value(j),result.Value(10),result.Value(11));
    fit_func_slice[j]->SetParError(0,result.Error(j));
    fit_func_slice[j]->SetParError(1,result.Error(10));
    fit_func_slice[j]->SetParError(2,result.Error(11));

    TOF_Fit_slice[j] = (TH1*)(fit_func_slice[j]->GetHistogram());

    TOF_Res_slice[j] = new TH1D(Form("TOF_Res_Slice%d",j),"TOF Residual",500,0,500);

    int N_start = TOFHist_slice[j]->FindBin(FitRange_low);
    int N_stop  = TOFHist_slice[j]->FindBin(FitRange_high);

    double h_error;

    for (int k=N_start; k<=N_stop; k++){
      h_error = TOFHist_slice[j]->GetBinError(k);
      if (h_error!=0)
	TOF_Res_slice[j]->SetBinContent(k,(TOFHist_slice[j]->GetBinContent(k)-fit_func_slice[j]->Eval(TOFHist_slice[j]->GetBinCenter(k)) )/h_error);
      else
	TOF_Res_slice[j]->SetBinContent(k,(TOFHist_slice[j]->GetBinContent(k)-fit_func_slice[j]->Eval(TOFHist_slice[j]->GetBinCenter(k)) ));
    }
  }

  //Histogram to save Chi2 and dof
  TH1 * Chi2Hist = new TH1D("Chi2Hist","Chi2Hist",2,0,2);
  Chi2Hist->SetBinContent(1,result.MinFcnValue());
  Chi2Hist->SetBinContent(2,result.Ndf());
  TFile *MultiSliceFile_Results = new TFile("MultiSliceFile_Results.root","RECREATE");

  for(int j=0;j < N_Slice; j++){
    fit_func_slice[j]->Write(Form("MultiSlice_FitFunc%d",j));
    TOF_Fit_slice[j]->Write(Form("MultiSlice_TOFit%d",j));
    TOF_Res_slice[j]->Write(Form("MultiSlice_TOFRes%d",j));
  }

  Chi2Hist->Write();
  MultiSliceFile_Results->Write();
  MultiSliceFile_Results->Close();

  return 0;
}

/*int main() {
    MultiSlice();
}*/
double spline_slice0_func(double *x, double parms[]){
    
    if(Std_Second_N_slice[0] == 0 || Std_First_N_slice[0] == 0) return 0;
    
    return parms[0]*( (-1.0/3.0+parms[1])*(-3.0/2.0)*(1.0/Std_Second_N_slice[0])*spline_minus_slice[0]->Eval(x[0]+parms[2]) + (1.0/3.0+parms[1])*(3.0/2.0)*(1.0/Std_First_N_slice[0])*spline_plus_slice[0]->Eval(x[0]+parms[2]) );
}

double spline_slice1_func(double *x, double parms[]){
    
    if(Std_Second_N_slice[1] == 0 || Std_First_N_slice[1] == 0) return 0;

    return parms[0]*( (-1.0/3.0+parms[1])*(-3.0/2.0)*(1.0/Std_Second_N_slice[1])*spline_minus_slice[1]->Eval(x[0]+parms[2]) + (1.0/3.0+parms[1])*(3.0/2.0)*(1.0/Std_First_N_slice[1])*spline_plus_slice[1]->Eval(x[0]+parms[2]) );
}

double spline_slice2_func(double *x, double parms[]){
    
    if(Std_Second_N_slice[2] == 0 || Std_First_N_slice[2] == 0) return 0;

    
    return parms[0]*( (-1.0/3.0+parms[1])*(-3.0/2.0)*(1.0/Std_Second_N_slice[2])*spline_minus_slice[2]->Eval(x[0]+parms[2]) + (1.0/3.0+parms[1])*(3.0/2.0)*(1.0/Std_First_N_slice[2])*spline_plus_slice[2]->Eval(x[0]+parms[2]) );
}

double spline_slice3_func(double *x, double parms[]){
    
    if(Std_Second_N_slice[3] == 0 || Std_First_N_slice[3] == 0) return 0;

    
    return parms[0]*( (-1.0/3.0+parms[1])*(-3.0/2.0)*(1.0/Std_Second_N_slice[3])*spline_minus_slice[3]->Eval(x[0]+parms[2]) + (1.0/3.0+parms[1])*(3.0/2.0)*(1.0/Std_First_N_slice[3])*spline_plus_slice[3]->Eval(x[0]+parms[2]) );
}

double spline_slice4_func(double *x, double parms[]){
    
    if(Std_Second_N_slice[4] == 0 || Std_First_N_slice[4] == 0) return 0;

    return parms[0]*( (-1.0/3.0+parms[1])*(-3.0/2.0)*(1.0/Std_Second_N_slice[4])*spline_minus_slice[4]->Eval(x[0]+parms[2]) + (1.0/3.0+parms[1])*(3.0/2.0)*(1.0/Std_First_N_slice[4])*spline_plus_slice[4]->Eval(x[0]+parms[2]) );
}

double spline_slice5_func(double *x, double parms[]){
    
    if(Std_Second_N_slice[5] == 0 || Std_First_N_slice[5] == 0) return 0;

    
    return parms[0]*( (-1.0/3.0+parms[1])*(-3.0/2.0)*(1.0/Std_Second_N_slice[5])*spline_minus_slice[5]->Eval(x[0]+parms[2]) + (1.0/3.0+parms[1])*(3.0/2.0)*(1.0/Std_First_N_slice[5])*spline_plus_slice[5]->Eval(x[0]+parms[2]) );
}

double spline_slice6_func(double *x, double parms[]){
    
    if(Std_Second_N_slice[6] == 0 || Std_First_N_slice[6] == 0) return 0;

    
    return parms[0]*( (-1.0/3.0+parms[1])*(-3.0/2.0)*(1.0/Std_Second_N_slice[6])*spline_minus_slice[6]->Eval(x[0]+parms[2]) + (1.0/3.0+parms[1])*(3.0/2.0)*(1.0/Std_First_N_slice[6])*spline_plus_slice[6]->Eval(x[0]+parms[2]) );
}

double spline_slice7_func(double *x, double parms[]){
    
    if(Std_Second_N_slice[7] == 0 || Std_First_N_slice[7] == 0) return 0;

    
    return parms[0]*( (-1.0/3.0+parms[1])*(-3.0/2.0)*(1.0/Std_Second_N_slice[7])*spline_minus_slice[7]->Eval(x[0]+parms[2]) + (1.0/3.0+parms[1])*(3.0/2.0)*(1.0/Std_First_N_slice[7])*spline_plus_slice[7]->Eval(x[0]+parms[2]) );
}

double spline_slice8_func(double *x, double parms[]){
    
    if(Std_Second_N_slice[8] == 0 || Std_First_N_slice[8] == 0) return 0;

    
    return parms[0]*( (-1.0/3.0+parms[1])*(-3.0/2.0)*(1.0/Std_Second_N_slice[8])*spline_minus_slice[8]->Eval(x[0]+parms[2]) + (1.0/3.0+parms[1])*(3.0/2.0)*(1.0/Std_First_N_slice[8])*spline_plus_slice[8]->Eval(x[0]+parms[2]) );
}

double spline_slice9_func(double *x, double parms[]){
    
    if(Std_Second_N_slice[9] == 0 || Std_First_N_slice[9] == 0) return 0;

    
    return parms[0]*( (-1.0/3.0+parms[1])*(-3.0/2.0)*(1.0/Std_Second_N_slice[9])*spline_minus_slice[9]->Eval(x[0]+parms[2]) + (1.0/3.0+parms[1])*(3.0/2.0)*(1.0/Std_First_N_slice[9])*spline_plus_slice[9]->Eval(x[0]+parms[2]) );
}


