
#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string>
#include <sstream> 
#include <vector>
#include <stdint.h>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TGraphErrors.h"
//Analyzer includes
#include "Analyzer.h"
#include "Calibration.h"

using namespace std;

/***********************************************************************************/
Calibration::Calibration(Analyzer* AnaPtr)
{
  CurrentAnalyzer = AnaPtr;
  //MWPC
  CalibrationScheme = "Pol";
  KeepLessTrigger = false;
//  string CalibrationScheme = "Micro";
  MWPCAnodeThreshold = 0.0;
  MWPCCathodeThreshold = 0.0;

  //MCP Related
  PreShiftX=0.0;
  PreShiftY=0.0;
  PreScaleX=1.0;
  PreScaleY=1.0;
  PreCutR=38.5;
  GridMode="Global";
}
/***********************************************************************************/
Calibration::~Calibration()
{
  ;
}
/********************************************************************
int Calibration::SetupInputFile(int FileID, TFile* &tfilein, TTree* &InTree){
  ostringstream convert;
  convert << setfill('0') << setw(9)<<FileID;
  string fName = CurrentAnalyzer->SIM_DATA_DIRECTORY + string("/BetaOnly_EventFile") + convert.str();
  fName += ".root";
  
  tfilein = new TFile(fName.c_str(),"READ");
  if (tfilein->IsZombie()){
    cout << "No such file: "<<fName<<endl;
    cout << "Consider including the full path!\n";
	  return -1;
  }
  
  InTree = (TTree*) tfilein->Get("BetaTree");
  InTree->AddFriend("ProcessorTree");       
   
  return 0;
}
********************************************************************/
int Calibration::CalibrateBeta(string FilePrefix,int FileID,int CalID,int SimCalID,string Source,string GainMapSwitch)
{
  CurrentAnalyzer->Calibrating = true;
  string spectrumfile;

  double EnergyThreshold = 1000;
  double EnergyUpperBound = 4000;
  double MWPCEUpperBound = 15.0;
//  double CalibrateTo = 935.61;//keV

  ExpDataStruct ExpData;      //Data struct for exp data reading
  RecDataStruct RecData;      //Reconstructed data struct
  CurrentAnalyzer->ConstructExpParameterList();
  CurrentAnalyzer->CheckExpConditionSettings();

//  double ScintCalibration;
//  double ScintCalibrationCorrected;
  double LEDCalibration=1.0;
  double ScintLEDCalibration=1.0;

  // root histos
  TH1D *h_Scint_Alone  = new TH1D("h_Scint_Alone","Scintillator spectrum (Alone)",1024,0,6000);
  TH1D *h_Scint_Coin  = new TH1D("h_Scint_Coin","Scintillator spectrum (Coincidence)",1024,0,6000);
  TH1D *h_MWPC1  = new TH1D("h_MWPC1","MWPC Anode1 spectrum",1024,0,30);
  TH1D *h_MWPC2  = new TH1D("h_MWPC2","MWPC Anode2 spectrum",1024,0,30);
  TH2* h_A1A2 = new TH2D("h_A1A2","A1 A2 correlation",1024,0,30,1024,0,30);
  TH2* h_PosX_Charge = new TH2D("h_PosX_Charge","X-TotalCharge correlation",800,-25.03125,24.96875,1024,0,30);
  TH2* h_PosY_Charge = new TH2D("h_PosY_Charge","Y-TotalCharge correlation",800,-25.03125,24.96875,1024,0,30);
  TH1D *h_MWPC_tot  = new TH1D("h_MWPC_tot","MWPC Anode_tot spectrum",1024,0,30);
  TH1D *h_MWPC_Cond  = new TH1D("h_MWPC_Cond","MWPC Anode_tot spectrum",1024,0,30);
  TH1 *h_FiredWireX = new TH1D("h_FiredWireX","# of fired stripes X",10,0,10);
  TH1 *h_FiredWireY = new TH1D("h_FiredWireY","# of fired stripes Y",10,0,10);

  TH1D *h_Cathode_X = new TH1D("h_Cathode_X","MWPC CathodeX position spectrum",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y = new TH1D("h_Cathode_Y","MWPC CathodeY position spectrum",800,-25.03125,24.96875);
  TH1D *h_Anode_Pos = new TH1D("h_Anode_Pos","MWPC Anode position spectrum",800,-25.03125,24.96875);
  TH2D *h_Cathode_Image = new TH2D("h_Cathode_Image","2D image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Anode_Image = new TH2D("h_Anode_Image","2D Anode image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_MWPC_Corr  = new TH2D("h_MWPC1_2","MWPC Anode1-2 spectrum",1024,0,30,1024,0,30);
  TH2D *h_ACCorrelation = new TH2D("h_ACCorrelation","MWPC anode cathode x correlation",200,-25,25,200,-25,25);
  TH1D *h_Charge_X = new TH1D("h_Charge_X","Total Charge on MWPC X",1024,0,30);
  TH1D *h_Charge_Y = new TH1D("h_Charge_Y","Total Charge on MWPC Y",1024,0,30);

  //Histograms for calibration
  TH2D *h_EfficiencyMap = new TH2D("h_EfficiencyMap","2D Anode Efficiency of MWPC",25,-25.0,25.0,24,-24.0,24.0);
  TH1D *h_LED = new TH1D("h_LED","LED signal",2500,0,250000);
  TH1D *h_Scint_LED  = new TH1D("h_Scint","Scintillator spectrum",1500,0,15000);

  //Time spectrum
  TH1D *h_Scint_T = new TH1D("h_Scint_T","Scint. alone time spectrum",900,0,900);
  TH1D *h_Coin_T = new TH1D("h_Coin_T","Coincidence time spectrum",900,0,900);
  TH1D *h_Coin_Cond_T = new TH1D("h_Coin_cond_T","Coincidence time spectrum conditioned",900,0,900);
  TH1D *h_Eff_T = new TH1D("h_Eff_T","MWPC Efficiency time spectrum",900,0,900);
  TH1D *h_Eff_Cond_T = new TH1D("h_Eff_Cond_T","MWPC Conditioned Efficiency time spectrum",900,0,900);

  //Uniformity calibration
  TH1* CellSpec[15][16];
  TH1D *h_Scint_Cal  = new TH1D("h_Scint_Cal","Scintillator spectrum (uniformity calibrated)",1024,0,6000);
  TH1D *h_Scint_Cal_3rdpeak;

  double GainMap[15][16];
  TF1* CellFitFunc[15][16];
  TF1 * FitFunc2;
  TF1 * FitFunc3;
  string HistName;
  string HistTitle;
  for (int i=0;i<15;i++){
    for (int j=0;j<16;j++){
      ostringstream double2str;
      double2str << setfill('0') << setw(2) <<i<<"_"<< setw(2) <<j;
      HistName="Cell_" + double2str.str();
      HistTitle="Spectrum for cell " + double2str.str();
      CellSpec[i][j]=new TH1D(HistName.c_str(),HistTitle.c_str(),1024,0,6000);
      HistName="CellFitFunc"+double2str.str();
      if (Source.compare("Sr90")==0){
	CellFitFunc[i][j] = new TF1("CellFitFunc","-[0]*(x-[1])",1600.0,2200.0);
	CellFitFunc[i][j]->SetParameters(1.0,2300.0);
      }else if (Source.compare("Bi207")==0){
	CellFitFunc[i][j] = new TF1("CellFitFunc","gaus",0.0,2000.0);
      }else{
	cout << "Invalid source type."<<endl;
	return 1;
      }
      GainMap[i][j] = 0.0;
    }
  }
  vector<double> Gain;
  vector<double> GainX;
  vector<double> GainY;
  vector<double> GainChargeX;
  vector<double> GainChargeY;
  
  ostringstream convert;
  convert << setfill('0') << setw(4)<<FileID;
  TString fName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + FilePrefix + string("_") + convert.str();
  fName += ".root";
  TFile *f = new TFile (fName.Data(), "read");

  TTree *Tree_Scint = (TTree*)f->Get("Tree_Scint");
  TTree *Tree_Beta = (TTree*)f->Get("Tree_Beta"); 
  TTree *Tree_PMT_STB = (TTree*)f->Get("Tree_PMT_STB");

  Tree_Scint->SetBranchAddress("Event_No",&ExpData.Event_Num);
  Tree_Scint->SetBranchAddress("GroupTime",&ExpData.GroupTime);
  Tree_Scint->SetBranchAddress("QPMT_A",&ExpData.QScint_A);
  Tree_Scint->SetBranchAddress("QPMT_D",&ExpData.QScint_D);
  Tree_Scint->SetBranchAddress("TPMT_A_rel",&ExpData.TScint_A_rel);
  Tree_Scint->SetBranchAddress("TPMT_DA",&ExpData.TScint_DA);
  Tree_Scint->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
  int N_Scint = Tree_Scint->GetEntries();

  Tree_Beta->SetBranchAddress("Event_No",&ExpData.Event_Num);
  Tree_Beta->SetBranchAddress("GroupTime",&ExpData.GroupTime);
  Tree_Beta->SetBranchAddress("QPMT_A",&ExpData.QScint_A);
  Tree_Beta->SetBranchAddress("QPMT_D",&ExpData.QScint_D);
  Tree_Beta->SetBranchAddress("TPMT_A_rel",&ExpData.TScint_A_rel);
  Tree_Beta->SetBranchAddress("TPMT_DA",&ExpData.TScint_DA);
  Tree_Beta->SetBranchAddress("AMWPC_anode1",&ExpData.AMWPC_anode1);
  Tree_Beta->SetBranchAddress("AMWPC_anode2",&ExpData.AMWPC_anode2);
  Tree_Beta->SetBranchAddress("TBeta_diff",&ExpData.TBeta_diff);
  Tree_Beta->SetBranchAddress("MWPC_X",&ExpData.MWPC_X);
  Tree_Beta->SetBranchAddress("TMWPC_X",&ExpData.TMWPC_X);
  Tree_Beta->SetBranchAddress("MWPC_Y",&ExpData.MWPC_Y);
  Tree_Beta->SetBranchAddress("TMWPC_Y",&ExpData.TMWPC_Y);
  Tree_Beta->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
  int N_Beta = Tree_Beta->GetEntries();

//  Tree_PMT_STB->SetBranchAddress("Event_No",&ExpData.Event_Num);
//  Tree_PMT_STB->SetBranchAddress("GroupTime",&ExpData.GroupTime);
  Tree_PMT_STB->SetBranchAddress("Scint",&ExpData.QScint_A);
  Tree_PMT_STB->SetBranchAddress("LED",&ExpData.LED);
  Tree_PMT_STB->SetBranchAddress("LED_T",&ExpData.LED_T);
//  Tree_PMT_STB->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
  int N_LED = Tree_PMT_STB->GetEntries();

  double StartTime = 0;

  for(int i=0;i<N_Scint;i++){
    //Read Tree
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    Tree_Scint->GetEntry(i);
    if (i==0)StartTime = ExpData.GroupTime;
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);
    h_Scint_Alone->Fill(RecData.EScintA[0]);
    if(RecData.EScintA[0]>EnergyThreshold && RecData.EScintA[0]<EnergyUpperBound) h_Scint_T->Fill((ExpData.GroupTime-StartTime)/60.0);
    /*    int hour = int(GroupTime/3600);
	  int minute = int((GroupTime - hour*3600)/60);
	  int second = GroupTime - hour*3600 - 60*minute;
     */
  }//End file loop
  cout << N_Scint << " Scint events"<<endl;

  /*/
  for(int i=0;i<N_LED;i++){
    //Read Tree
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    Tree_PMT_STB->GetEntry(i);
  //  cout <<RecData.LED<<endl;
    if (i==0)StartTime = ExpData.GroupTime;
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);
    if (RecData.LED>100 && RecData.EScintA[0]>10){
      h_LED->Fill(RecData.LED);
      h_Scint_LED->Fill(RecData.EScintA[0]);
    }
  }//End file loop
  cout << N_LED << " LED events"<<endl;
  //LED Fit and calibration
  double LED_Peak = h_LED->GetBinCenter(h_LED->GetMaximumBin());
  h_LED->Fit("gaus","","",LED_Peak-1000,LED_Peak+1000);
  TF1* LEDFit = h_LED->GetFunction("gaus");
  LEDCalibration = LEDFit->GetParameter(1);
  double Chi2 = LEDFit->GetChisquare();
  double NDF = LEDFit->GetNDF();
  cout << "LED Fit Chi2= "<<Chi2/NDF<<endl;

  double Scint_LED_Peak = h_Scint_LED->GetBinCenter(h_Scint_LED->GetMaximumBin());
  h_Scint_LED->Fit("gaus","","",Scint_LED_Peak-500,Scint_LED_Peak+500);
  TF1* Scint_LEDFit = h_Scint_LED->GetFunction("gaus");
  ScintLEDCalibration = Scint_LEDFit->GetParameter(1);
  Chi2 = Scint_LEDFit->GetChisquare();
  NDF = Scint_LEDFit->GetNDF();
  cout << "Scint_LED Fit Chi2= "<<Chi2/NDF<<endl;
*/
  //March 2nd 2016, remove LED calibration for this

  for(int i=0;i<N_Beta;i++){
    //Read Tree
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    Tree_Beta->GetEntry(i);
    if (i==0)StartTime = ExpData.GroupTime;
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);
//    cout << RecData.MWPCPos_X<<endl;
    //Fill Histograms
    if(RecData.EScintA[0]>EnergyThreshold && RecData.EScintA[0]<EnergyUpperBound) h_Coin_T->Fill((ExpData.GroupTime-StartTime)/60.0);
    //Conditions
    bool cond1 = RecData.EMWPC_anode1>=CurrentAnalyzer->MWPCThreshold && RecData.EMWPC_anode2>=CurrentAnalyzer->MWPCThreshold && (RecData.EMWPC_anode1+RecData.EMWPC_anode2)<MWPCEUpperBound;
    double R = sqrt(pow(RecData.MWPCPos_X,2.0) + pow(RecData.MWPCPos_Y,2.0));
    bool cond2 = R<CurrentAnalyzer->MWPC_R;
    bool cond3 = RecData.EScintA[0]>EnergyThreshold && RecData.EScintA[0]<EnergyUpperBound;
    bool cond4 = RecData.MWPCPos_X>-15.0 && RecData.MWPCPos_X<15.0 && RecData.MWPCPos_Y>-16.0 && RecData.MWPCPos_Y<16.0;

    h_FiredWireX->Fill(RecData.FiredWireX);
    h_FiredWireY->Fill(RecData.FiredWireY);
    if (cond1 && cond2 && ((ExpData.TriggerMap&(1<<9))==0)){
      h_Scint_Coin->Fill(RecData.EScintA[0]);
      //Saved for Gain Uniformity correction
      Gain.push_back(RecData.EScintA[0]);
      GainX.push_back(RecData.MWPCPos_X);
      GainY.push_back(RecData.MWPCPos_Y);
    }

    if (cond1 && cond4 && ((ExpData.TriggerMap&(1<<9))==0)){
      //Gain Uniformity
      int ii = int((RecData.MWPCPos_X+15.0)/2.0);
      int jj = int((RecData.MWPCPos_Y+16.0)/2.0);
      if (ii>=0 && ii<15 && jj>=0 && jj<16){
	CellSpec[ii][jj]->Fill(RecData.EScintA[0]);
      }
    }

    if (cond1 && cond4 && cond3){
      h_EfficiencyMap->Fill(RecData.MWPCPos_X,RecData.MWPCPos_Y);
    }
    if (cond1 && cond2 && cond3){
      double TotCharge = RecData.EMWPC_anode1+RecData.EMWPC_anode2;
      h_MWPC1->Fill(RecData.EMWPC_anode1);
      h_MWPC2->Fill(RecData.EMWPC_anode2);
      h_A1A2->Fill(RecData.EMWPC_anode1,RecData.EMWPC_anode2);
      h_PosX_Charge->Fill(RecData.MWPCPos_X,TotCharge);
      h_PosY_Charge->Fill(RecData.MWPCPos_Y,TotCharge);
      h_MWPC_tot->Fill(TotCharge);
      h_MWPC_Cond->Fill(TotCharge);

      h_Cathode_X->Fill(RecData.MWPCPos_X);
      h_Cathode_Y->Fill(RecData.MWPCPos_Y_C);
      h_Anode_Pos->Fill(RecData.MWPCPos_Y);
      h_Cathode_Image->Fill(RecData.MWPCPos_X,RecData.MWPCPos_Y_C);
      h_Anode_Image->Fill(RecData.MWPCPos_X,RecData.MWPCPos_Y);
      //  h_MWPC_Corr->Fill(,);
      h_ACCorrelation->Fill(RecData.MWPCPos_Y_C,RecData.MWPCPos_Y);
      h_Charge_X->Fill(RecData.QMWPC_cathode_X);
      h_Charge_Y->Fill(RecData.QMWPC_cathode_Y);
      h_Coin_Cond_T->Fill((ExpData.GroupTime-StartTime)/60.0);
    }
  }
  for (int i=1;i<=900;i++){
    if (h_Scint_T->GetBinContent(i)!=0){
      h_Eff_T->Fill(i,h_Coin_T->GetBinContent(i)/h_Scint_T->GetBinContent(i)*100.0);
      h_Eff_Cond_T->Fill(i,h_Coin_Cond_T->GetBinContent(i)/h_Scint_T->GetBinContent(i)*100.0);
    }
  }

  double IntegralFiredX = h_FiredWireX->Integral();
  double IntegralFiredY = h_FiredWireY->Integral();
  h_FiredWireX->Scale(100.0/IntegralFiredX);
  h_FiredWireY->Scale(100.0/IntegralFiredY);
  //Uniformity calibration**************************************************************
  TGraph2DErrors *GainMapGraph = new TGraph2DErrors();
  TH1* GainStatistics = new TH1D("GainStatistics","GainStatistics",500,0.9,1.1);
  TH1* GainErrorStatistics = new TH1D("GainErrorStatistics","GainErrorStatistics",200,0.0,0.03);
  TGraph2D *GainMapChi2 = new TGraph2D();
  TCanvas* CellCanvas = new TCanvas("GainMap","GainMap",0,0,15000,16000);
  CellCanvas->Divide(15,16);

  TGraphErrors *CalGraph = new TGraphErrors();
  CalGraph->SetName("CalGraph");
  CalGraph->SetTitle("CalGraph");

  string SpecFile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Spectra/") + FilePrefix + string("_") + convert.str() + string("_PMTGainMap.pdf(");
  if (Source.compare("Sr90")==0){
    //Fit [7][7] as the reference point
    CellCanvas->cd(7*15+7+1);
    CellSpec[7][7]->Rebin(4);
    //  int MaxBin = CellSpec[7][7]->GetMaximumBin();
    //  double Peak = CellSpec[7][7]->GetBinCenter(MaxBin);
    CellSpec[7][7]->Fit(CellFitFunc[7][7],"","",1600.0,2200.0);
    double ReferenceGain = CellFitFunc[7][7]->GetParameter(1);
    double ReferenceError = CellFitFunc[7][7]->GetParError(1);
    GainMap[7][7] = 1.0; 
    CellSpec[7][7]->Draw();
    for (int i=0;i<15;i++){
      for (int j=0;j<16;j++){
	if (i==7 && j==7){
	  GainMapGraph->SetPoint(j*15+i,-14+i*2.0,-15+j*2.0,1);
	  GainMapGraph->SetPointError(j*15+i,0,0,ReferenceError/ReferenceGain);
	  GainMapChi2->SetPoint(j*15+i,-14+i*2.0,-15+j*2.0,1.0);
	  GainStatistics->Fill(1.0);
	  GainErrorStatistics->Fill(ReferenceError/ReferenceGain);
	  continue;
	}
	CellCanvas->cd(j*15+i+1);
	CellSpec[i][j]->Rebin(4);
	//      MaxBin = CellSpec[i][j]->GetMaximumBin();
	//      Peak = CellSpec[i][j]->GetBinCenter(MaxBin);
	double DataCount = CellSpec[i][j]->Integral(CellSpec[i][j]->FindBin(1600.0),CellSpec[i][j]->FindBin(2200.0));
	if (DataCount>150){
	  TFitResultPtr r =  CellSpec[i][j]->Fit(CellFitFunc[i][j],"","",1600.0,2200.0);
	  double Chi2 = CellFitFunc[i][j]->GetChisquare();
	  double NDF = CellFitFunc[i][j]->GetNDF();
	  if (NDF==0){
	    Chi2=1;
	  }else{
	    Chi2/=double(NDF);
	  }
	  cout <<i<<" "<<j<<" Chi2 = "<<Chi2 <<endl;
	  GainMap[i][j] = CellFitFunc[i][j]->GetParameter(1)/ReferenceGain;
	  GainMapGraph->SetPoint(j*15+i,-14+i*2.0,-15+j*2.0,GainMap[i][j]);
	  GainMapGraph->SetPointError(j*15+i,0.0,0.0,CellFitFunc[i][j]->GetParError(1)/ReferenceGain);
	  GainMapChi2->SetPoint(j*15+i,-14+i*2.0,-15+j*2.0,Chi2);
	  GainStatistics->Fill(GainMap[i][j]);
	  GainErrorStatistics->Fill(CellFitFunc[i][j]->GetParError(1)/ReferenceGain);
	  CellSpec[i][j]->Draw();
	}
	else{
	  GainMap[i][j] = 1.0;
	  GainMapGraph->SetPoint(j*15+i,-14+i*2.0,-15+j*2.0,GainMap[i][j]);
	  GainMapGraph->SetPointError(j*15+i,0.0,0.0,0.0);
	  GainMapChi2->SetPoint(j*15+i,-14+i*2.0,-15+j*2.0,0);
	  CellSpec[i][j]->Draw();
	}
      }
    }
    CellCanvas->SaveAs(SpecFile.c_str());
    delete CellCanvas;
    //Fit the total histogram
    FitFunc2 = new TF1("FitFunc2","-[0]*(x-[1])",1400.0,1900.0);
    FitFunc2->SetParameters(1.0,2300);
    h_Scint_Coin->Fit(FitFunc2,"","",1400.0,1900.0);
    double EndPoint = h_Scint_Coin->GetFunction("FitFunc2")->GetParameter(1);
    double EndPointError = h_Scint_Coin->GetFunction("FitFunc2")->GetParError(1);
/*    ScintCalibration = EndPoint/2284.0;
    cout << "Calibration parameter= "<<ScintCalibration<<endl;
    cout << "Calibration precision= "<<EndPointError/2284.0<<endl;
    */
  }
  else if(Source.compare("Bi207")==0){
    //Fit [7][7] as the reference point
    CellCanvas->cd(7*15+7+1);
    CellSpec[7][7]->Rebin(4);
    int MaxBin = CellSpec[7][7]->GetMaximumBin();
    double Peak = CellSpec[7][7]->GetBinCenter(MaxBin);
    CellFitFunc[7][7]->SetParameters(CellSpec[7][7]->GetBinContent(MaxBin),Peak,Peak*0.1);
    CellSpec[7][7]->Fit(CellFitFunc[7][7],"","",Peak*0.92,Peak*1.08);
    double ReferenceGain = CellFitFunc[7][7]->GetParameter(1);
    double ReferenceError = CellFitFunc[7][7]->GetParError(1);
    GainMap[7][7] = 1.0; 
    CellSpec[7][7]->Draw();
    for (int i=0;i<15;i++){
      for (int j=0;j<16;j++){
	if (i==7 && j==7){
	  GainMapGraph->SetPoint(j*15+i,-14+i*2.0,-15+j*2.0,1);
	  GainMapGraph->SetPointError(j*15+i,0,0,ReferenceError/ReferenceGain);
	  GainMapChi2->SetPoint(j*15+i,-14+i*2.0,-15+j*2.0,1.0);
	  GainStatistics->Fill(1.0);
	  GainErrorStatistics->Fill(ReferenceError/ReferenceGain);
	  continue;
	}
	CellCanvas->cd(j*15+i+1);
	CellSpec[i][j]->Rebin(4);
	MaxBin = CellSpec[i][j]->GetMaximumBin();
	Peak = CellSpec[i][j]->GetBinCenter(MaxBin);
	double DataCount = CellSpec[i][j]->Integral(CellSpec[i][j]->FindBin(Peak*0.92),CellSpec[i][j]->FindBin(Peak*1.08));
	if (DataCount>250){
	  CellFitFunc[i][j]->SetParameters(CellSpec[i][j]->GetBinContent(MaxBin),Peak,Peak*0.1);
	  TFitResultPtr r =  CellSpec[i][j]->Fit(CellFitFunc[i][j],"","",Peak*0.9,Peak*1.1);
	  double Chi2 = CellFitFunc[i][j]->GetChisquare();
	  double NDF = CellFitFunc[i][j]->GetNDF();
	  if (NDF==0){
	    Chi2=1;
	  }else{
	    Chi2/=double(NDF);
	  }
	  cout <<i<<" "<<j<<" Chi2 = "<<Chi2 <<endl;
	  GainMap[i][j] = CellFitFunc[i][j]->GetParameter(1)/ReferenceGain;
	  GainMapGraph->SetPoint(j*15+i,-14+i*2.0,-15+j*2.0,GainMap[i][j]);
	  GainMapGraph->SetPointError(j*15+i,0.0,0.0,CellFitFunc[i][j]->GetParError(1)/ReferenceGain);
	  GainMapChi2->SetPoint(j*15+i,-14+i*2.0,-15+j*2.0,Chi2);
	  GainStatistics->Fill(GainMap[i][j]);
	  GainErrorStatistics->Fill(CellFitFunc[i][j]->GetParError(1)/ReferenceGain);
	  CellSpec[i][j]->Draw();
	}
	else{
	  GainMap[i][j] = 1.0;
	  GainMapGraph->SetPoint(j*15+i,-14+i*2.0,-15+j*2.0,GainMap[i][j]);
	  GainMapGraph->SetPointError(j*15+i,0.0,0.0,0.0);
	  GainMapChi2->SetPoint(j*15+i,-14+i*2.0,-15+j*2.0,0);
	  CellSpec[i][j]->Draw();
	}
      }
    }
    CellCanvas->SaveAs(SpecFile.c_str());
    delete CellCanvas;
    //Fit the total histogram
    MaxBin = h_Scint_Coin->GetMaximumBin();
    Peak = h_Scint_Coin->GetBinCenter(MaxBin);
    double MaxCount = h_Scint_Coin->GetBinContent(MaxBin);
    FitFunc2 = new TF1("FitFunc2","gaus",0.0,2000.0);
    FitFunc2->SetParameters(h_Scint_Coin->GetBinContent(MaxBin),Peak,Peak*0.1);
    h_Scint_Coin->Fit(FitFunc2,"","",Peak*0.95,Peak*1.05);
/*    TF1* FitFunc2 = new TF1("FitFunc2","[0]*(exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+0.26*exp(-pow(x-[1]-72.14,2.0)/2.0/pow([2],2.0))+0.062*exp(-pow(x-[1]-84.15,2.0)/2.0/pow([2],2.0))+[3]*ROOT::Math::erfc((x-[1]+[4]*[2]*[2])/(sqrt(2)*[2]))*exp([4]/2.0*(2.0*x-2.0*[1]-[4]*[2]*[2])))+[5]*(exp(-pow(x-[6],2.0)/2.0/pow([7],2.0))+0.29*exp(-pow(x-[6]-72.14,2.0)/2.0/pow([7],2.0))+[8]*ROOT::Math::erfc((x-[6]+[9]*[7]*[7])/(sqrt(2)*[7]))*exp([9]/2.0*(2.0*x-2.0*[6]-[9]*[7]*[7])))",0,2000);
    FitFunc2->SetParameters(MaxCount,Peak,0.06*Peak,0.25,0.0013,0.21*MaxCount,0.49*Peak,0.086*Peak,0.9,0.0061);
    FitFunc2->SetNpx(500);
    h_Scint_Coin->Fit(FitFunc2,"","",150.0,2000.0);*/
/*
    double Peak_1MeV = h_Scint_Coin->GetFunction("FitFunc2")->GetParameter(1);
    double Peak_1MeVError = h_Scint_Coin->GetFunction("FitFunc2")->GetParError(1);
    ScintCalibration = CalibrateTo/Peak_1MeV;
    cout << "Calibration parameter= "<<ScintCalibration<<endl;
    cout << "Calibration precision= "<<CalibrateTo*Peak_1MeVError/pow(Peak_1MeV,2.0)<<endl;
    */
  }
  else{
    cout << "Invalid source type."<<endl;
    return 1;
  }
  TCanvas* CellCanvas1 = new TCanvas("GainMap1","GainMap1",0,0,15000,16000);
  GainMapGraph->SetName("PMTGainMap");
  GainMapGraph->SetTitle("PMTGainMap");
  GainMapGraph->GetXaxis()->SetTitle("X");
  GainMapGraph->GetYaxis()->SetTitle("Y");
  GainMapGraph->GetZaxis()->SetTitle("Relative Gain");
  GainMapGraph->Draw("surf1");
  SpecFile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Spectra/") + FilePrefix + string("_") + convert.str() + string("_PMTGainMap.pdf");
  CellCanvas1->SaveAs(SpecFile.c_str());
  GainMapChi2->SetName("Chi2");
  GainMapChi2->SetTitle("Chi2");
  GainMapChi2->Draw("surf1");
  SpecFile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Spectra/") + FilePrefix + string("_") + convert.str() + string("_PMTGainMap.pdf)");
  CellCanvas1->SaveAs(SpecFile.c_str());
  delete CellCanvas1;
  //Load Data again, correct the gain non-uniform
  int NCal = Gain.size();
  for (int i=0;i<NCal;i++){
    int ii = int((GainX[i]+15.0)/2.0);
    int jj = int((GainY[i]+16.0)/2.0);
    if (ii>=0 && ii<15 && jj>=0 && jj<16){
      double GainFactor = GainMap[ii][jj];
      if (GainFactor>0){
	//Apply the fiducial cut, 14 mm as standard
	if (pow(GainX[i],2.0)+pow(GainY[i],2.0)<pow(14,2.0)){
	  h_Scint_Cal->Fill(Gain[i]/GainFactor);
	}
      }
    }
  } 

  double Peak_1MeVCal;
  double Peak_1MeVErrorCal;
  double Peak_1MeVWidthCal;
  double Peak_1MeVWidthErrorCal;

  double Peak_500keVCal;
  double Peak_500keVErrorCal;
  double Peak_500keVWidthCal;
  double Peak_500keVWidthErrorCal;

  double Peak_1700keVCal;
  double Peak_1700keVErrorCal;
  double Peak_1700keVWidthCal;
  double Peak_1700keVWidthErrorCal;

  //Clone h_Scint_Cal
  h_Scint_Cal_3rdpeak = (TH1D*)(h_Scint_Cal->Clone());
  h_Scint_Cal_3rdpeak->SetName("h_Scint_Cal_3rdpeak");

  if (Source.compare("Sr90")==0){
    FitFunc3 = new TF1("FitFunc3","-[0]*(x-[1])",1600.0,2200.0);
    FitFunc3->SetParameters(1.0,2300);
    h_Scint_Cal->Fit(FitFunc3,"","",1400.0,1900.0);
    double EndPointCal = FitFunc3->GetParameter(1);
    double EndPointErrorCal = FitFunc3->GetParError(1);
/*    ScintCalibrationCorrected = 2284.0/EndPointCal;
    cout << "Calibration parameter= "<<ScintCalibrationCorrected<<endl;
    cout << "Calibration precision= "<<2284.0*EndPointErrorCal/pow(EndPointCal,2.0)<<endl;
    */
  }else if (Source.compare("Bi207")==0){ 
    int MaxBin = h_Scint_Cal->GetMaximumBin();
    int Peak = h_Scint_Cal->GetBinCenter(MaxBin);
    double MaxCount = h_Scint_Cal->GetBinContent(MaxBin);
   /* FitFunc3 = new TF1("FitFunc3","gaus",0.0,2000.0);
    FitFunc3->SetParameters(h_Scint_Cal->GetBinContent(MaxBin),Peak,Peak*0.1);
    h_Scint_Cal->Fit(FitFunc3,"","",Peak*0.95,Peak*1.05);*/
    TF1* FitFunc3 = new TF1("FitFunc3","[0]*(exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+0.26*exp(-pow(x-1.074*[1],2.0)/2.0/pow([2],2.0))+0.062*exp(-pow(x-1.086*[1],2.0)/2.0/pow([2],2.0))+[3]*ROOT::Math::erfc((x-[1]+[4]*[2]*[2])/(sqrt(2)*[2]))*exp([4]/2.0*(2.0*x-2.0*[1]+[4]*[2]*[2])))+[5]*(exp(-pow(x-[6],2.0)/2.0/pow([7],2.0))+0.29*exp(-pow(x-1.15*[6],2.0)/2.0/pow([7],2.0))+0.072*exp(-pow(x-1.175*[6],2.0)/2.0/pow([7],2.0))+[8]*ROOT::Math::erfc((x-[6]+[9]*[7]*[7])/(sqrt(2)*[7]))*exp([9]/2.0*(2.0*x-2.0*[6]+[9]*[7]*[7])))",0,2000);
    FitFunc3->SetParameters(MaxCount,Peak,0.06*Peak,0.25,0.0013,0.21*MaxCount,0.45*Peak,0.086*Peak,0.9,0.0061);
    FitFunc3->SetNpx(500);
    h_Scint_Cal->Fit(FitFunc3,"","",300.0,1400.0);
    Peak_500keVCal = FitFunc3->GetParameter(6);
    Peak_500keVErrorCal = FitFunc3->GetParError(6);
    Peak_500keVWidthCal = FitFunc3->GetParameter(7);
    Peak_500keVWidthErrorCal = FitFunc3->GetParError(7);
    Peak_1MeVCal = FitFunc3->GetParameter(1);
    Peak_1MeVErrorCal = FitFunc3->GetParError(1);
    Peak_1MeVWidthCal = FitFunc3->GetParameter(2);
    Peak_1MeVWidthErrorCal = FitFunc3->GetParError(2);
/*    ScintCalibrationCorrected = CalibrateTo/Peak_1MeVCal*ScintCalibration;
    cout << "Calibration parameter= "<<ScintCalibrationCorrected<<endl;
    cout << "Calibration precision= "<<CalibrateTo*Peak_1MeVErrorCal*ScintCalibration/pow(Peak_1MeVCal,2.0)<<endl;*/
    h_Scint_Cal->Fit("gaus","","",1.6*Peak,2.074*Peak);
//    h_Scint_Cal->Fit("gaus","","",1.68*Peak,2.15*Peak);
    Peak_1700keVCal = h_Scint_Cal->GetFunction("gaus")->GetParameter(1);
    Peak_1700keVErrorCal = h_Scint_Cal->GetFunction("gaus")->GetParError(1);
    Peak_1700keVWidthCal = h_Scint_Cal->GetFunction("gaus")->GetParameter(2);
    Peak_1700keVWidthErrorCal = h_Scint_Cal->GetFunction("gaus")->GetParError(2);
    cout << "Three Peaks from calibration:\n";
    cout << Peak_500keVCal<<" +/- "<<Peak_500keVErrorCal<<" : "<< Peak_500keVWidthCal<<" +/- "<<Peak_500keVWidthErrorCal<<endl;
    cout << Peak_1MeVCal<<" +/- "<<Peak_1MeVErrorCal<<" : " << Peak_1MeVWidthCal<<" +/- "<<Peak_1MeVWidthErrorCal<<endl;
    cout << Peak_1700keVCal<<" +/- "<<Peak_1700keVErrorCal<<" : "<< Peak_1700keVWidthCal<<" +/- "<<Peak_1700keVWidthErrorCal<<endl;

    //Good for visualization
    h_Scint_Cal->Fit(FitFunc3,"","",150.0,2000.0);
    h_Scint_Cal_3rdpeak->Fit("gaus","","",1.6*Peak,2.074*Peak);
    //Load From simulation file and find the peaks
    double p,pe,w,we;
    char filename[100];
    ifstream SimCalFileIn;
    sprintf(filename,"%s/SimScintCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),SimCalID);
    SimCalFileIn.open(filename,ios::in);
    SimCalFileIn>>p>>pe>>w>>we;
    CalGraph->SetPoint(0,Peak_500keVCal,p);
    CalGraph->SetPointError(0,Peak_500keVErrorCal,pe);
    SimCalFileIn>>p>>pe>>w>>we;
    CalGraph->SetPoint(1,Peak_1MeVCal,p);
    CalGraph->SetPointError(1,Peak_1MeVErrorCal,pe);
    SimCalFileIn>>p>>pe>>w>>we;
    CalGraph->SetPoint(2,Peak_1700keVCal,p);
    CalGraph->SetPointError(2,Peak_1700keVErrorCal,pe);
    SimCalFileIn.close();
    
    CalGraph->Fit("pol1");
    TF1* GraphFunction = CalGraph->GetFunction("pol1");
    double EScale = GraphFunction->GetParameter(1);
    double EShift = GraphFunction->GetParameter(0);
    ScintLEDCalibration = ScintLEDCalibration*EScale+EShift;

    //Write to calibration file
    cout <<"Writing to calibration files.\n";

    ofstream ECalFileOut;
    sprintf(filename,"%s/ScintCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
    ECalFileOut.open(filename,ios::out);
    /*  cout <<"ScintA[0]=ScintD Calibration factor (before correction) = "<<ScintCalibration<<endl;
	cout <<"Calibration factor after gain uniformity correction= "<<ScintCalibrationCorrected<<endl;
	cout <<"LED Calibration= "<<LEDCalibration<<endl;*/
    cout <<"Scint LED Calibration= "<<ScintLEDCalibration<<endl;
    cout <<"Linear fit to peaks: "<<EScale<<" "<<EShift<<endl;
    //  ECalFileOut<<ScintCalibration<<" "<<ScintCalibrationCorrected<<" "<<LEDCalibration<<" "<<ScintLEDCalibration<<endl;
    ECalFileOut <<LEDCalibration<<" "<<ScintLEDCalibration<<endl;
    ECalFileOut << Peak_500keVCal<<" "<<Peak_500keVErrorCal<<" "<< Peak_500keVWidthCal<<" "<<Peak_500keVWidthErrorCal<<endl;
    ECalFileOut << Peak_1MeVCal<<" "<<Peak_1MeVErrorCal<<" " << Peak_1MeVWidthCal<<" "<<Peak_1MeVWidthErrorCal<<endl;
    ECalFileOut << Peak_1700keVCal<<" "<<Peak_1700keVErrorCal<<" "<< Peak_1700keVWidthCal<<" "<<Peak_1700keVWidthErrorCal<<endl;
    ECalFileOut.close();

    if (GainMapSwitch.compare("ON")==0){
      ofstream GainCalFileOut;
      sprintf(filename,"%s/ScintGainMap%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
      GainCalFileOut.open(filename,ios::out);
      for (int i=0;i<15;i++){
	for (int j=0;j<16;j++){
	  GainCalFileOut<<-14.0+i*2.0<<" "<<-15.0+j*2.0<<" "<<GainMap[i][j]<<endl;
	}
	GainCalFileOut <<endl;
      }
      GainCalFileOut.close();
    }
  }else{
    cout << "Invalid source type."<<endl;
    return 1;
  }
  //Fit the MWPC Energy Peak
  h_MWPC_tot->Fit("landau","0","",0.5,30);
  TF1 *FitLandau = h_MWPC_tot->GetFunction("landau");
  cout <<"Landau fit of the MWPC spectrum: "<<FitLandau->GetParameter(1)<<endl;
  /************************************************************************************************/
  //Further Write to calibration file
  //Scale the Efficiency Map
  char filename[100];
  int CenterBin = h_EfficiencyMap->FindBin(0,0);
  double scale = h_EfficiencyMap->GetBinContent(CenterBin);
  h_EfficiencyMap->Scale(1.0/scale);
  ofstream EffCalFileOut;
  sprintf(filename,"%s/MWPCEffMap%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  EffCalFileOut.open(filename,ios::out);
  for (int i=0;i<15;i++){
    for (int j=0;j<16;j++){
      double x = -14+i*2.0;
      double y = -15+j*2.0;
      int binIndex = h_EfficiencyMap->FindBin(x,y);
      EffCalFileOut<<h_EfficiencyMap->GetBinContent(binIndex)<<" ";
    }
    EffCalFileOut <<endl;
  }
  EffCalFileOut.close();
 
  //Write to root file
  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_BetaCal.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");

  h_Scint_Alone->Write();
  h_Scint_Coin->Write();
  h_MWPC1->Write();
  h_MWPC2->Write();
  h_MWPC_tot->Write();
//  h_MWPC_Cond->Write();
  h_Anode_Pos->Write();
  h_Cathode_Image->Write();
  h_ACCorrelation->Write();
  h_Anode_Image->Write();
  h_FiredWireX->Write();
  h_FiredWireY->Write();

  h_Cathode_X->Write();
  h_Cathode_Y->Write();
  h_Charge_X->Write();
  h_Charge_Y->Write();
  h_A1A2->Write();
  h_PosX_Charge->Write();
  h_PosY_Charge->Write();
  h_EfficiencyMap->Write();
  h_Eff_T->Write();
  h_Eff_Cond_T->Write();
  h_LED->Write();
  h_Scint_LED->Write();
  h_Scint_Cal->Write();
  h_Scint_Cal_3rdpeak->Write();
  GainMapGraph->Write();
  GainStatistics->Write();
  GainErrorStatistics->Write();
  CalGraph->Write();
  //Pix spectra
  
  for (int i=0;i<15;i++){
    for (int j=0;j<16;j++){
      CellSpec[i][j]->Write();
    }
  }
  //////////////////////////////////
  histfile->Close();
  CurrentAnalyzer->Calibrating = false;
  return 0;
}

/********************************************************************/
int Calibration::CalibrateLED(string FilePrefix,int FileID,int CalID,string mode,double Period)
{
  CurrentAnalyzer->Calibrating = true;
  string spectrumfile;

  ExpDataStruct ExpData;      //Data struct for exp data reading
  RecDataStruct RecData;      //Reconstructed data struct
  CurrentAnalyzer->ConstructExpParameterList();
  CurrentAnalyzer->CheckExpConditionSettings();

  // root histos
  TH1D *h_Scint_tot  = new TH1D("h_Scint_tot","Scintillator spectrum (total)",1024,0,20000);
  TH1D *h_Scint  = new TH1D("h_Scint","Scintillator spectrum",1024,0,20000);
  //Histograms for calibration
  TH1D *h_LED_tot = new TH1D("h_LED_tot","LED signal total",1000,0,100000);
  TH1D *h_Scint_LED_tot  = new TH1D("h_Scint_LED_tot","Scintillator spectrum (LED)",1024,0,20000);
  TH1D *h_LED = new TH1D("h_LED","LED signal",1000,0,100000);
  TH1D *h_Scint_LED  = new TH1D("h_Scint_LED","Scintillator spectrum",1024,0,20000);
  TH2D *LED_Scint = new TH2D("LED_Scint","LED vs Scint",1024,0,20000,1400,0,140000);

  TH1D *LEDPeaks[17];
  TH1D *ScintPeaks[17];
  double PrimitiveLEDPeak[17] = {14000,19000,25000,30900,37000,43500,50200,56700,63500,70500,77500,84500,91200,98000,104600,111500,118000};
  string HistName;
  string HistTitle;
  for (int i=0;i<17;i++){
    ostringstream double2str;
    double2str << setfill('0') << setw(2) <<i<<"_"<< setw(2) <<i;
    HistName="LEDPeak_" + double2str.str();
    HistTitle="Spectrum for LED Peak " + double2str.str();
    LEDPeaks[i] = new TH1D(HistName.c_str(),HistTitle.c_str(),1400,0,140000);
    HistName="ScintPeak_" + double2str.str();
    HistTitle="Spectrum for Scint Peak " + double2str.str();
    ScintPeaks[i] = new TH1D(HistName.c_str(),HistTitle.c_str(),1000,0,10000);
  }

  //Graph
  TGraphErrors* Linearity = new TGraphErrors();
  Linearity->SetName("Linearity");
  Linearity->SetTitle("Linearity");
  TGraphErrors* LEDBetaCorr = new TGraphErrors;
  LEDBetaCorr->SetName("LEDBetaCorr");
  LEDBetaCorr->SetTitle("LED-Beta Correlation");
  //Time spectrum
  TGraphErrors* TBetaCalibration = new TGraphErrors;
  TBetaCalibration->SetName("TBetaCalibration");
  TBetaCalibration->SetTitle("Time spectrum Beta Calibration");
  TH1* BetaCalStat = new TH1D("BetaCalStat","BetaCalStat",50,0.98,1.02);

  TGraphErrors* TLEDScint = new TGraphErrors;
  TLEDScint->SetName("TLEDScint");
  TLEDScint->SetTitle("Time spectrum TLEDScint");

  TGraphErrors* TLED = new TGraphErrors;
  TLED->SetName("TLED");
  TLED->SetTitle("Time spectrum TLED");

  TGraphErrors* TLEDCalibration = new TGraphErrors;
  TLEDCalibration->SetName("TLEDCalibration");
  TLEDCalibration->SetTitle("Time spectrum TLEDCalibration");
  TH1* LEDCalStat = new TH1D("LEDCalStat","LEDCalStat",50,0.98,1.02);

  TGraphErrors* TBetaCorrected = new TGraphErrors;
  TBetaCorrected->SetName("TBetaCorrected");
  TBetaCorrected->SetTitle("Time spectrum Beta Calibration Corrected");

  ostringstream convert;
  convert << setfill('0') << setw(4)<<FileID;
  TString fName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + FilePrefix + string("_") + convert.str();
  fName += ".root";
  TFile *f = new TFile (fName.Data(), "read");

  TTree *Tree_Scint = (TTree*)f->Get("Tree_Scint");
  TTree *Tree_PMT_STB = (TTree*)f->Get("Tree_PMT_STB");
  TTree *Tree_Beta = (TTree*)f->Get("Tree_Beta");

  Tree_Scint->SetBranchAddress("Event_No",&ExpData.Event_Num);
  Tree_Scint->SetBranchAddress("GroupTime",&ExpData.GroupTime);
  Tree_Scint->SetBranchAddress("QPMT_A",&ExpData.QScint_A);
  Tree_Scint->SetBranchAddress("QPMT_D",&ExpData.QScint_D);
  Tree_Scint->SetBranchAddress("TPMT_A_rel",&ExpData.TScint_A_rel);
  Tree_Scint->SetBranchAddress("TPMT_DA",&ExpData.TScint_DA);
  Tree_Scint->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
  int N_Scint = Tree_Scint->GetEntries();


  Tree_PMT_STB->SetBranchAddress("Scint",&ExpData.QScint_A);
  Tree_PMT_STB->SetBranchAddress("LED",&ExpData.LED);
  Tree_PMT_STB->SetBranchAddress("LED_T",&ExpData.LED_T);
  int N_LED = Tree_PMT_STB->GetEntries();

  Tree_Beta->SetBranchAddress("Event_No",&ExpData.Event_Num);
  Tree_Beta->SetBranchAddress("GroupTime",&ExpData.GroupTime);
  Tree_Beta->SetBranchAddress("QPMT_A",&ExpData.QScint_A);
  Tree_Beta->SetBranchAddress("QPMT_D",&ExpData.QScint_D);
  Tree_Beta->SetBranchAddress("TPMT_A_rel",&ExpData.TScint_A_rel);
  Tree_Beta->SetBranchAddress("TPMT_DA",&ExpData.TScint_DA);
  Tree_Beta->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
  int N_Beta = Tree_Beta->GetEntries();

  double StartTime = 0;

  TF1* FitFunc;
  int jj=0;

  //This section is for Sr90 case, which is obsolete
  /*
  for(int i=0;i<N_Scint;i++){
    //Read Tree
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    Tree_Scint->GetEntry(i);
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);
    if ((RecData.TriggerMap&(1<<12))==0){
      h_Scint_tot->Fill(RecData.EScintA[0]);
      h_Scint->Fill(RecData.EScintA[0]);
      if (RecData.GroupTime-jj*Period>Period ){
	FitFunc = new TF1("FitFunc","-[0]*(x-[1])",1600.0,2200.0);
	FitFunc->SetParameters(1.0,2300);
	h_Scint->Fit(FitFunc,"","",1600.0,2200.0);
	double EndPoint = FitFunc->GetParameter(1);
	double FitError = FitFunc->GetParError(1);; 
	double ScintCalibration = EndPoint/2284.0;
	TBetaCalibration->SetPoint(jj,jj*Period/60.0,ScintCalibration);
	TBetaCalibration->SetPointError(jj,0,FitError/2284.0);
	delete h_Scint;
	h_Scint = new TH1D("h_Scint","Scintillator spectrum",1024,0,10000);;
	jj++;
      }
    }
  }//End file loop
  cout << N_Scint << " Scint events"<<endl;
*/

  //Calibration using Bi207
  double BetaCalNorm = 1;
  for(int i=0;i<N_Beta;i++){
    //Read Tree
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    Tree_Beta->GetEntry(i);
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);
    if ((RecData.TriggerMap&(1<<12))==0){
      h_Scint_tot->Fill(RecData.EScintA[0]);
      h_Scint->Fill(RecData.EScintA[0]);
      if (RecData.GroupTime-jj*Period>Period ){
	int MaxBin = h_Scint->GetMaximumBin();
	int Max = h_Scint->GetBinCenter(MaxBin);
//	FitFunc = new TF1("FitFunc","-[0]*(x-[1])",1600.0,2200.0);
//	FitFunc->SetParameters(1.0,2300);
	h_Scint->Fit("gaus","","",Max*0.9,Max*1.1);
	FitFunc = h_Scint->GetFunction("gaus");
	double Peak = FitFunc->GetParameter(1);
	double PeakError = FitFunc->GetParError(1);; 
	if (jj==0){
	  BetaCalNorm = Peak;
	}
//	double ScintCalibration = Peak/975.651;
	double ScintCalibration = Peak/BetaCalNorm;
	TBetaCalibration->SetPoint(jj,jj*Period/60.0,ScintCalibration);
	TBetaCalibration->SetPointError(jj,0,PeakError/BetaCalNorm);
	BetaCalStat->Fill(ScintCalibration);
	delete h_Scint;
	h_Scint = new TH1D("h_Scint","Scintillator spectrum",1024,0,20000);;
	jj++;
      }
    }
  }//End file loop
  cout << N_Scint << " Scint events"<<endl;

  double LEDVal;
  double ScintLEDVal;
  double LEDCalibration;

  jj=0;
  double LEDNorm = 1;
  double ScintLEDNorm = 1;
  for(int i=0;i<N_LED;i++){
    //Read Tree
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    Tree_PMT_STB->GetEntry(i);
    if (i==0)StartTime = ExpData.GroupTime;
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);
    if (RecData.LED>100 && RecData.EScintA[0]>10 && (RecData.TriggerMap&(1<<8))==0){
      h_LED_tot->Fill(RecData.LED);
      h_Scint_LED_tot->Fill(RecData.EScintA[0]);
      h_LED->Fill(RecData.LED);
      h_Scint_LED->Fill(RecData.EScintA[0]);
      LED_Scint->Fill(RecData.EScintA[0],RecData.LED);
      if (mode.compare("Stability")==0){
	if (RecData.LED_T-jj*Period>Period){
	  double LED_Peak = h_LED->GetBinCenter(h_LED->GetMaximumBin());
	  h_LED->Fit("gaus","","",LED_Peak-1000,LED_Peak+1000);
	  TF1* LEDFit = h_LED->GetFunction("gaus");
	  LEDVal = LEDFit->GetParameter(1);
	  if (jj==0)LEDNorm=LEDVal;
	  LEDVal = LEDVal/LEDNorm;
	  double LEDError = LEDFit->GetParError(1)/LEDNorm;
	  double Chi2 = LEDFit->GetChisquare();
	  double NDF = LEDFit->GetNDF();
	  cout << "LED Fit Chi2= "<<Chi2/NDF<<endl;
	  TLED->SetPoint(jj,jj*Period/60.0,LEDVal);
	  TLED->SetPointError(jj,0,LEDError);

	  double ScintLED_Peak = h_Scint_LED->GetBinCenter(h_Scint_LED->GetMaximumBin());
	  h_Scint_LED->Fit("gaus","","",ScintLED_Peak-500,ScintLED_Peak+500);
	  TF1* ScintLEDFit = h_Scint_LED->GetFunction("gaus");
	  ScintLEDVal = ScintLEDFit->GetParameter(1);
	  if (jj==0)ScintLEDNorm=ScintLEDVal;
	  ScintLEDVal = ScintLEDVal/ScintLEDNorm;
	  double ScintLEDError = ScintLEDFit->GetParError(1)/ScintLEDNorm;
	  double ScintChi2 = ScintLEDFit->GetChisquare();
	  double ScintNDF = ScintLEDFit->GetNDF();
	  cout << "ScintLED Fit Chi2= "<<ScintChi2/ScintNDF<<endl;
	  TLEDScint->SetPoint(jj,jj*Period/60.0,ScintLEDVal);
	  TLEDScint->SetPointError(jj,0,ScintLEDError);

	  TLEDCalibration->SetPoint(jj,jj*Period/60.0,ScintLEDVal/LEDVal);
	  TLEDCalibration->SetPointError(jj,0,ScintLEDVal/LEDVal*sqrt(pow(ScintLEDError/ScintLEDVal,2.0)+pow(LEDError/LEDVal,2.0)));
	  LEDCalStat->Fill(ScintLEDVal/LEDVal);

	  double error1 = sqrt(pow(ScintLEDError/ScintLEDVal,2.0)+pow(LEDError/LEDVal,2.0));
	  double auxX,auxY;
	  TBetaCalibration->GetPoint(jj,auxX,auxY);
	  double error2 = TBetaCalibration->GetErrorY(jj)/auxY;
	  TBetaCorrected->SetPoint(jj,jj*Period/60.0,auxY/(ScintLEDVal/LEDVal));
	  TBetaCorrected->SetPointError(jj,0,auxY/(ScintLEDVal/LEDVal)*sqrt(error1*error1+error2*error2));
	  jj++;

	  delete h_LED;
	  delete h_Scint_LED;
	  h_LED = new TH1D("h_LED","LED signal",1000,0,100000);
	  h_Scint_LED  = new TH1D("h_Scint_LED","Scintillator spectrum",1024,0,20000);
	}
      }
    }
    if (mode.compare("Linearity")==0){
      int j=0;
      for (j=0;j<17;j++){
	if ((RecData.LED>PrimitiveLEDPeak[j]-1000) && (RecData.LED<PrimitiveLEDPeak[j]+1000))break;
      }
      if (j<17){
	LEDPeaks[j]->Fill(RecData.LED);
	ScintPeaks[j]->Fill(RecData.EScintA[0]);
      }
    }
  }//End file loop
  cout << N_LED << " LED events"<<endl;
  if (mode.compare("Linearity")==0){
    for (int i=0;i<17;i++){
      LEDPeaks[i]->Fit("gaus");
      TF1* LEDFit = LEDPeaks[i]->GetFunction("gaus"); 
      double LEDPos = LEDFit->GetParameter(1);
      double LEDError = LEDFit->GetParameter(2);
      double CurrentPeak = ScintPeaks[i]->GetBinCenter(ScintPeaks[i]->GetMaximumBin());
      ScintPeaks[i]->Fit("gaus","","",CurrentPeak-200,CurrentPeak+200);
      TF1* ScintFit = ScintPeaks[i]->GetFunction("gaus"); 
      double ScintPos = ScintFit->GetParameter(1);
      double ScintError = ScintFit->GetParameter(2);
      Linearity->SetPoint(i,LEDPos,ScintPos);
      Linearity->SetPointError(i,LEDError,ScintError);
    }
    Linearity->Fit("pol1");
    TF1* LinearFit = Linearity->GetFunction("pol1");
    double Chi2 = LinearFit->GetChisquare();
    double NDF = LinearFit->GetNDF();
    cout << Chi2<<" "<<NDF<<endl;
  }

  //Fit to constant
  TBetaCalibration->Fit("pol0");
  TBetaCorrected->Fit("pol0");
  /************************************************************************************************/
  //Write to root file
  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_LEDCal.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");
  
  h_Scint_tot->Write();
  h_Scint->Write();
  h_LED_tot->Write();
  h_Scint_LED_tot->Write();
  h_LED->Write();
  h_Scint_LED->Write();
  BetaCalStat->Write();
  LEDCalStat->Write();
  LED_Scint->Write();
  if (mode.compare("Linearity")==0){
    for (int i=0;i<17;i++){
      LEDPeaks[i]->Write();
      ScintPeaks[i]->Write();
    }
    Linearity->Write();
  }

  TLED->Write();
  TLEDScint->Write();
  TLEDCalibration->Write();
  TBetaCalibration->Write();
  TBetaCorrected->Write();
  histfile->Close();
  CurrentAnalyzer->Calibrating = false;
  return 0;
}

/********************************************************************/
int Calibration::CalibrateBetaSim(int FileID,int CalID,string Source)
{
  CurrentAnalyzer->SetMode("Beta");
  CurrentAnalyzer->Calibrating = true;
//  CurrentAnalyzer->SetParameter("MWPCC_Threshold",0,0.0182);
  string spectrumfile;

  double EnergyThreshold = 1000;
  double EnergyUpperBound = 4000;
  double MWPCEUpperBound = 30.0;

  double Peak_1MeVCal;
  double Peak_1MeVErrorCal;
  double Peak_1MeVWidthCal;
  double Peak_1MeVWidthErrorCal;

  double Peak_500keVCal;
  double Peak_500keVErrorCal;
  double Peak_500keVWidthCal;
  double Peak_500keVWidthErrorCal;

  double Peak_1700keVCal;
  double Peak_1700keVErrorCal;
  double Peak_1700keVWidthCal;
  double Peak_1700keVWidthErrorCal;

  TF1 * FitFunc2;

  TFile* tfilein = NULL; //pointer to input root file
  TTree * InTree = NULL; //input tree pointer
  InDataStruct InData;  //input data unit
  InDataStructDouble InDataDouble;      //input data double format
  SimRecDataStruct SimRecData;  //Reconstructed data
  CurrentAnalyzer->ConstructParameterList();
  CurrentAnalyzer->CheckConditionSettings();

  // root histos
  TH1D *h_Scint_Coin  = new TH1D("h_Scint_Coin","Scintillator spectrum (Coincidence)",1024,0,6000);
  TH1D *h_MWPC1  = new TH1D("h_MWPC1","MWPC Anode1 spectrum",1024,0,20);
  TH1D *h_MWPC2  = new TH1D("h_MWPC2","MWPC Anode2 spectrum",1024,0,20);
  TH2* h_PosX_Charge = new TH2D("h_PosX_Charge","X-TotalCharge correlation",800,-25.03125,24.96875,1024,0,30);
  TH2* h_PosY_Charge = new TH2D("h_PosY_Charge","Y-TotalCharge correlation",800,-25.03125,24.96875,1024,0,30);
  TH2* h_A1A2 = new TH2D("h_A1A2","A1 A2 correlation",1024,0,20,1024,0,20);
  TH1D *h_MWPC_tot  = new TH1D("h_MWPC_tot","MWPC Anode_tot spectrum",1024,0,30);
  TH1 *h_FiredWireX = new TH1D("h_FiredWireX","# of fired stripes X",10,0,10);
  TH1 *h_FiredWireY = new TH1D("h_FiredWireY","# of fired stripes Y",10,0,10);

  TH2D *h_ACCorrelation = new TH2D("h_ACCorrelation","MWPC anode cathode x correlation",200,-25,25,200,-25,25);
  TH1D *h_Cathode_X = new TH1D("h_Cathode_X","MWPC CathodeX position spectrum",800,-25.03125,24.96875);
  TH1D *h_Cathode_Y = new TH1D("h_Cathode_Y","MWPC CathodeY position spectrum",800,-25.03125,24.96875);
  TH1D *h_Anode_Pos = new TH1D("h_Anode_Pos","MWPC Anode position spectrum",800,-25.03125,24.96875);
  TH2D *h_Cathode_Image = new TH2D("h_Cathode_Image","2D image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH2D *h_Anode_Image = new TH2D("h_Anode_Image","2D Anode image of MWPC",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH1D *h_Charge_X = new TH1D("h_Charge_X","Total Charge on MWPC X",1024,0,20);
  TH1D *h_Charge_Y = new TH1D("h_Charge_Y","Total Charge on MWPC Y",1024,0,20);
  TH2D *h_EfficiencyMap = new TH2D("h_EfficiencyMap","2D Anode Efficiency of MWPC",25,-25.0,25.0,24,-24.0,24.0);

  if(CurrentAnalyzer->SetupInputFile(FileID,tfilein,InTree)==-1) return -1;
  CurrentAnalyzer->SetInTreeAddress(InTree, InData);
  int returnVal = 0;  
  int iEvent = 0;

  while(1){
    returnVal = InTree->GetEntry(iEvent);
    if (returnVal==0) break; //eof
    else if(returnVal==-1) return -1;
    CurrentAnalyzer->ConvertDataToDouble(InData,InDataDouble); 
    //Reconstruct
    CurrentAnalyzer->ReconstructSim(0,0,SimRecData,InDataDouble,InData);
    //Fill Histograms
    //Conditions
    bool cond1 = SimRecData.MWPCEnergy>=CurrentAnalyzer->MWPCThreshold && SimRecData.MWPCEnergy<MWPCEUpperBound;
    double R = sqrt(pow(SimRecData.MWPCHitPos[0],2.0) + pow(SimRecData.MWPCHitPos[1],2.0));
    bool cond2 = R<CurrentAnalyzer->MWPC_R;
    bool cond3 = SimRecData.ScintEnergy>EnergyThreshold && SimRecData.ScintEnergy<EnergyUpperBound;
    bool cond4 = SimRecData.MWPCHitPos[0]>-15.0 && SimRecData.MWPCHitPos[0]<15.0 && SimRecData.MWPCHitPos[1]>-14.0 && SimRecData.MWPCHitPos[1]<14.0;

    if (cond1 && cond2){
      //Apply another very small threshold to avoid zero
      if (SimRecData.ScintEnergy>10)h_Scint_Coin->Fill(SimRecData.ScintEnergy);
      //h_Scint_Coin->Fill(SimRecData.ScintEnergy);
    }

    if (cond1 && cond4 && cond3){
      h_EfficiencyMap->Fill(SimRecData.MWPCHitPos[0],SimRecData.MWPCHitPos[1]);
    }
    if (cond1 && cond2 && cond3){
      double TotCharge = SimRecData.MWPCEnergy;
      h_MWPC1->Fill(InDataDouble.MWPCAnodeSignal1);
      h_MWPC2->Fill(InDataDouble.MWPCAnodeSignal2);
      h_A1A2->Fill(InDataDouble.MWPCAnodeSignal1,InDataDouble.MWPCAnodeSignal2);
      h_PosX_Charge->Fill(SimRecData.MWPCHitPos[0],TotCharge);
      h_PosY_Charge->Fill(SimRecData.MWPCHitPos[1],TotCharge);
      h_MWPC_tot->Fill(TotCharge);
      h_FiredWireX->Fill(SimRecData.FiredWireX);
      h_FiredWireY->Fill(SimRecData.FiredWireY);

      h_Cathode_X->Fill(SimRecData.MWPCHitPos[0]);
      h_Cathode_Y->Fill(SimRecData.MWPC_CY);
      h_Anode_Pos->Fill(SimRecData.MWPCHitPos[1]);
      h_Cathode_Image->Fill(SimRecData.MWPCHitPos[0],SimRecData.MWPC_CY);
      h_Anode_Image->Fill(SimRecData.MWPCHitPos[0],SimRecData.MWPCHitPos[1]);
      //  h_MWPC_Corr->Fill(,);
      h_ACCorrelation->Fill(SimRecData.MWPC_CY,SimRecData.MWPCHitPos[1]);
      h_Charge_X->Fill(SimRecData.MWPCCathodeSignalX);
      h_Charge_Y->Fill(SimRecData.MWPCCathodeSignalY);
    }
    iEvent++;
  }
  //close root input file
  if (tfilein!=NULL){
    //tfilein->Close(); done automatically upon deleting
    delete tfilein;
    tfilein =NULL;
  }
  double IntegralFiredX = h_FiredWireX->Integral();
  double IntegralFiredY = h_FiredWireY->Integral();
  h_FiredWireX->Scale(100.0/IntegralFiredX);
  h_FiredWireY->Scale(100.0/IntegralFiredY);

  TH1* histScintCopy = (TH1*)h_Scint_Coin->Clone();
  //Fit the total histogram
  if (Source.compare("Sr90")==0){
    FitFunc2 = new TF1("FitFunc2","-[0]*(x-[1])",1600.0,2200.0);
    FitFunc2->SetParameters(1.0,2300);
    h_Scint_Coin->Fit(FitFunc2,"","",1600.0,2200.0);
    double EndPoint = h_Scint_Coin->GetFunction("FitFunc2")->GetParameter(1);
    double EndPointError = h_Scint_Coin->GetFunction("FitFunc2")->GetParError(1);
    cout << "Sr90 EndPoint= "<<EndPoint<<endl;
    cout << "Sr90 EndPoint Error= "<<EndPointError<<endl;
  }else if (Source.compare("Bi207")==0){
    int MaxBin = h_Scint_Coin->GetMaximumBin();
    int Peak = h_Scint_Coin->GetBinCenter(MaxBin);
    double MaxCount = h_Scint_Coin->GetBinContent(MaxBin);
   /* FitFunc3 = new TF1("FitFunc3","gaus",0.0,2000.0);
    FitFunc3->SetParameters(h_Scint_Coin->GetBinContent(MaxBin),Peak,Peak*0.1);
    h_Scint_Coin->Fit(FitFunc3,"","",Peak*0.95,Peak*1.05);*/
    TF1* FitFunc3 = new TF1("FitFunc3","[0]*(exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+0.26*exp(-pow(x-1.074*[1],2.0)/2.0/pow([2],2.0))+0.062*exp(-pow(x-1.086*[1],2.0)/2.0/pow([2],2.0))+[3]*ROOT::Math::erfc((x-[1]+[4]*[2]*[2])/(sqrt(2)*[2]))*exp([4]/2.0*(2.0*x-2.0*[1]+[4]*[2]*[2])))+[5]*(exp(-pow(x-[6],2.0)/2.0/pow([7],2.0))+0.29*exp(-pow(x-1.15*[6],2.0)/2.0/pow([7],2.0))+0.072*exp(-pow(x-1.175*[6],2.0)/2.0/pow([7],2.0))+[8]*ROOT::Math::erfc((x-[6]+[9]*[7]*[7])/(sqrt(2)*[7]))*exp([9]/2.0*(2.0*x-2.0*[6]+[9]*[7]*[7])))",0,2000);
    FitFunc3->SetParameters(MaxCount,Peak,0.06*Peak,0.25,0.0013,0.21*MaxCount,0.45*Peak,0.086*Peak,0.9,0.0061);
    FitFunc3->SetNpx(500);
    h_Scint_Coin->Fit(FitFunc3,"","",300.0,1400.0);
    Peak_500keVCal = FitFunc3->GetParameter(6);
    Peak_500keVErrorCal = FitFunc3->GetParError(6);
    Peak_500keVWidthCal = FitFunc3->GetParameter(7);
    Peak_500keVWidthErrorCal = FitFunc3->GetParError(7);
    Peak_1MeVCal = FitFunc3->GetParameter(1);
    Peak_1MeVErrorCal = FitFunc3->GetParError(1);
    Peak_1MeVWidthCal = FitFunc3->GetParameter(2);
    Peak_1MeVWidthErrorCal = FitFunc3->GetParError(2);

    h_Scint_Coin->Fit("gaus","","",1.68*Peak,2.074*Peak);
    Peak_1700keVCal = h_Scint_Coin->GetFunction("gaus")->GetParameter(1);
    Peak_1700keVErrorCal = h_Scint_Coin->GetFunction("gaus")->GetParError(1);
    Peak_1700keVWidthCal = h_Scint_Coin->GetFunction("gaus")->GetParameter(2);
    Peak_1700keVWidthErrorCal = h_Scint_Coin->GetFunction("gaus")->GetParError(2);
    cout << "Three Peaks from calibration:\n";
    cout << Peak_500keVCal<<" +/- "<<Peak_500keVErrorCal<<" : "<< Peak_500keVWidthCal<<" +/- "<<Peak_500keVWidthErrorCal<<endl;
    cout << Peak_1MeVCal<<" +/- "<<Peak_1MeVErrorCal<<" : " << Peak_1MeVWidthCal<<" +/- "<<Peak_1MeVWidthErrorCal<<endl;
    cout << Peak_1700keVCal<<" +/- "<<Peak_1700keVErrorCal<<" : "<< Peak_1700keVWidthCal<<" +/- "<<Peak_1700keVWidthErrorCal<<endl;
    //Check the fit of first two peaks
    histScintCopy->Fit(FitFunc3,"","",300.0,1400.0);
    histScintCopy->SetName("h_Scint_Coin_Copy");
  }
  //Fit the MWPC Energy Peak
  h_MWPC_tot->Fit("landau","0","",0.5,30);
  TF1 *FitLandau = h_MWPC_tot->GetFunction("landau");
  cout <<"Landau fit of the MWPC spectrum: "<<FitLandau->GetParameter(1)<<endl;

  /************************************************************************************************/
  //Write to calibration file

  cout <<"Writing to calibration files.\n";

  char filename[100];
  ofstream ECalFileOut;
  sprintf(filename,"%s/SimScintCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  ECalFileOut.open(filename,ios::out);
  ECalFileOut<< Peak_500keVCal<<" "<<Peak_500keVErrorCal<<" "<< Peak_500keVWidthCal<<" "<<Peak_500keVWidthErrorCal<<endl;
  ECalFileOut<< Peak_1MeVCal<<" "<<Peak_1MeVErrorCal<<" " << Peak_1MeVWidthCal<<" "<<Peak_1MeVWidthErrorCal<<endl;
  ECalFileOut<< Peak_1700keVCal<<" "<<Peak_1700keVErrorCal<<" "<< Peak_1700keVWidthCal<<" "<<Peak_1700keVWidthErrorCal<<endl;
  ECalFileOut.close();

  //Scale the Efficiency Map
  int CenterBin = h_EfficiencyMap->FindBin(0,0);
  double scale = h_EfficiencyMap->GetBinContent(CenterBin);
  h_EfficiencyMap->Scale(1.0/scale);
  ofstream EffCalFileOut;
  sprintf(filename,"%s/SimMWPCEffMap%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  EffCalFileOut.open(filename,ios::out);
  for (int i=0;i<15;i++){
    for (int j=0;j<16;j++){
      double x = -14+i*2.0;
      double y = -15+j*2.0;
      int binIndex = h_EfficiencyMap->FindBin(x,y);
      EffCalFileOut<<h_EfficiencyMap->GetBinContent(binIndex)<<" ";
    }
    EffCalFileOut <<endl;
  }
  EffCalFileOut.close();
 
  //Write to root file
  ostringstream convert;
  convert << setfill('0') << setw(9)<<FileID;
  spectrumfile = CurrentAnalyzer->SIM_DATA_DIRECTORY + string("/../Histograms/") + "SimBetaCal" + string("_") + convert.str() + string(".root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");

  //////////////////////////////////
  h_Scint_Coin->Write();
  
  if (Source.compare("Bi207")==0){
    histScintCopy->Write();
  }
  h_MWPC1->Write();
  h_MWPC2->Write();
  h_MWPC_tot->Write();
//  h_MWPC_Cond->Write();
  h_Anode_Pos->Write();
  h_Cathode_Image->Write();
  h_ACCorrelation->Write();
  h_Anode_Image->Write();
  h_FiredWireX->Write();
  h_FiredWireY->Write();

  h_Cathode_X->Write();
  h_Cathode_Y->Write();
  h_Charge_X->Write();
  h_Charge_Y->Write();
  h_A1A2->Write();
  h_EfficiencyMap->Write();
  histfile->Close();
//  delete h_Scint_Coin;
//  delete h_MWPC1;
//  delete h_MWPC2;
//  delete h_PosX_Charge;
//  delete h_PosY_Charge;
//  delete h_A1A2;
//  delete h_MWPC_tot;
//  delete h_FiredWireX;
//  delete h_FiredWireY;

//  delete h_ACCorrelation;
//  delete h_Cathode_X;
//  delete h_Cathode_Y;
//  delete h_Anode_Pos;
//  TH2D *h_Cathode_Image;
//  TH2D *h_Anode_Image;
//  TH1D *h_Charge_X;
//  TH1D *h_Charge_Y;
//  TH2D *h_EfficiencyMap;
  delete histfile;
  CurrentAnalyzer->Calibrating = false;
  return 0;
}
/********************************************************************/
