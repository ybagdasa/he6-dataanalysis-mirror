/********************************************************************
  He6 experiment analyzer, for both simulation and experiemtal data
  Author:	Ran Hong
  Date:		Jul. 8th. 2013
  Version:	1.03

  UI command line
  Processing simulation event file and build histograms
  Read histograms from files
  Save std histograms for fit
  Fit TOF and Energy spectrum, get 'a' and 'b' (in Fit.cpp)

  v1.03: commandline UI is done, and batch mode is supported.
********************************************************************/

//C++ includes
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>
#include <unistd.h>
#include <fcntl.h>

//Analyzer includes
#include "Analyzer.h"
#include "Calibration.h"
#include "AnalyzerUI.h"

using namespace std;

int main(int argc, char **argv)
{
  string infilename;	//input file name
  string label;
  string filename;
  int fd;
  int returnVal;
  char *SimDataDirect;
  char *ExpDataDirect;
  char *AnalyzerDirect;
  char *CalibrationDirect;
  
  //Root verbosity level
	gErrorIgnoreLevel=kError; //output errors but not warnings or info
  
  //Get Data Directory
  SimDataDirect=getenv("HE6_SIMULATION_DATA_DIRECTORY");
  if (SimDataDirect==NULL){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
  
  ExpDataDirect=getenv("HE6_EXPERIMENT_DATA_DIRECTORY");
  if (ExpDataDirect==NULL){
    cout << "The environment variable HE6_EXPERIMENT_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
  
  CalibrationDirect=getenv("HE6_CALIBRATION_DATA_DIRECTORY");
  if (CalibrationDirect==NULL){
    cout << "The environment variable HE6_CALIBRATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
  
  AnalyzerDirect=getenv("HE6_ANALYZER_DIRECTORY");
  if (AnalyzerDirect==NULL){
    cout << "The environment variable HE6_ANALYZER_DIRECTORY is not defined! Set it up to the Analyzer directory first!\n";
    return -1;
  }

  //Construct Analyzer
  Analyzer *He6_Analyzer = new Analyzer(SimDataDirect,ExpDataDirect,AnalyzerDirect,CalibrationDirect);
  Calibration *He6_Calibration = new Calibration(He6_Analyzer);

  //Construct Analyzer User Interface
  AnalyzerUI *He6_Analyzer_UI = new AnalyzerUI(He6_Analyzer,He6_Calibration);

  //Initialization
  He6_Analyzer_UI->Init();

  //Mode selection
  if(argc==1){
    returnVal=He6_Analyzer_UI->Start(0);	//Commandline mode
  }else if (argc==2){
    filename = argv[1];
    fd = open(filename.c_str(),O_RDONLY);
    dup2(fd,0);			//Batch mode
    returnVal=He6_Analyzer_UI->Start(1);
  }else{
    cout<< "Usage: \n./Analyzer: for commandline mode\n./Analyzer ScriptfileName: for batch mode\n";
    return -1;
  }

  delete He6_Analyzer;
  delete He6_Analyzer_UI;
  delete He6_Calibration;

  if (returnVal==0){
    cout<<"Done!\n";
    return 0;
  }else{
    cout<<"Analyzing aborted!\n";
    return -1;
  }
}
