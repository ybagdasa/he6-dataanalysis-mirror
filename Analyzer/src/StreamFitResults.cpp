#include <iostream>
#include <sstream>
#include <iomanip>
#include "TF1.h"

using namespace std;

string StreamFitResults(TF1* fit, int precision);
string StreamFitResults(TF1* fit, int precision){
  
  ostringstream stm;
  char star[] = "*";
  int Ndim, Npar, Ndf;
  double Chi2, Prob;
  const double *Pars, *ParErrors;

  Ndim = fit->GetNdim();
  Npar = fit->GetNpar();
  Ndf = fit->GetNDF();
  Chi2 = fit->GetChisquare();
  Prob = fit->GetProb();
  Pars = fit->GetParameters();
  ParErrors = fit->GetParErrors();

  stm << "\nFit Results:\n"<<string(80,star[0])<<"\n";
  stm << "Ndim = "<<Ndim<<"\tNpar = "<<Npar<<"\nFunction: "<<fit->GetName()<<"\n";
  stm << fixed<<setprecision(precision);
  for (int i=0;i<Npar;i++){
    stm << fit->GetParName(i)<<": "<<Pars[i]<<"+/-"<<ParErrors[i]<<"\n";
  }
  stm << "Chi2 = "<< Chi2<<"\tNDF ="<<Ndf<<"\tProb = "<<Prob<<"\n";
  stm << string(80,star[0])<<"\n"; 
  return stm.str();
}
