#include <iostream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream> 
#include <vector>
#include <stdint.h>
#include <stdlib.h>


//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TGraphErrors.h"
#include "TVectorD.h"
#include "TMatrixD.h"
#include "TMatrixDEigen.h"
#include "TEllipse.h"
#include "TF1.h"
#include "TF2.h"
#include "TVectorD.h"
#include "TMatrixD.h"
#include "TMatrixDEigen.h"
#include "TEllipse.h"
//Analyzer includes
#include "Analyzer.h"
#include "Calibration.h"

#pragma pack(1)
typedef struct tagBITMAPFILEHEADER
{
  unsigned char bfType[2];
  unsigned int bfSize;
  unsigned short bfReserved1;
  unsigned short bfReserved2;
  unsigned int bfOffBits;
}fileHeader;
#pragma pack()

#pragma pack(1)
typedef struct tagBITMAPINFOHEADER
{
  unsigned int biSize;
  int biWidth;
  int biHeight;
  unsigned short biPlanes;
  unsigned short biBitCount;
  unsigned int biCompression;
  unsigned int biSizeImage;
  int biXPixPerMeter;
  int biYPixPerMeter;
  unsigned int biClrUsed;
  unsigned int biClrImporant;
}fileInfo;
#pragma pack()

#pragma pack(1)
typedef struct tagRGBQUAD
{
  unsigned char rgbBlue;
  unsigned char rgbGreen;
  unsigned char rgbRed;
  unsigned char rgbReserved;
}rgbq;
#pragma pack()

using namespace std;

//Function and struct declarations are moved to Calibration.h and Datastruct.h

/***********************************************************************************/
//Calibrate the center region and find the MOT parameters
int Calibration::CalibrateMOT(string FilePrefix,int FileID,int CalID)
{
  if(CurrentAnalyzer->ReadMode.compare("MCP")!=0){
      cout <<"Only MCP mode is allowed."<<endl;
    return -1;
  }
  string spectrumfile;

  ExpDataStruct ExpData;      //Data struct for exp data reading
  RecDataStruct RecData;      //Reconstructed data struct
  TFile* tfilein = NULL; //pointer to input root file
  TTree * Tree_MCP = NULL; //input tree pointer
  CurrentAnalyzer->ConstructExpParameterList();
  CurrentAnalyzer->CheckExpConditionSettings();

  double RotationAngle = -0.3927;	//22.5 deg

  //Histograms
  TH2* h_MCPImage_Cal = new TH2D("h_MCPImage_Cal","MCPImage Calibrated",1000,-50,50,1000,-50,50);
  TH2* h_MCPImage_Center = new TH2D("h_MCPImage_Center","MCPImage Calibrated Center",100,-5,5,100,-5,5);
  TH2* h_MCPImage_CenterRot = new TH2D("h_MCPImage_CenterRot","MCPImage Calibrated Center rotated",100,-5,5,100,-5,5);
  TH1* h_CenterX = new TH1D("h_CenterX","Center X Projection",100,-5.0,5.0);
  TH1* h_CenterY = new TH1D("h_CenterY","Center Y Projection",100,-5.0,5.0);

  //Read In Data
  ostringstream convert;
  convert << setfill('0') << setw(4)<<FileID;
  TString fName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + FilePrefix + string("_") + convert.str();
  fName += ".root";
  
  if(CurrentAnalyzer->OpenExpInputFile(fName.Data(),tfilein)==-1) return -1;
  CurrentAnalyzer->SetExpInTreeAddress(tfilein,Tree_MCP,ExpData);
  cout << "Reading file "<<fName<<endl;
  int N_MCP = Tree_MCP->GetEntries();
  for(int i=0;i<N_MCP;i++){
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    //Read Tree
    Tree_MCP->GetEntry(i);
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);

    //Pre-shift
//    RecData.MCPPos_X+=PreShiftX;
//    RecData.MCPPos_Y+=PreShiftY;

    //Fill Histograms
    bool cond;
    cond = RecData.MCP_TXSum>CurrentAnalyzer->TXSumLow && RecData.MCP_TXSum<CurrentAnalyzer->TXSumHigh && RecData.MCP_TYSum>CurrentAnalyzer->TYSumLow && RecData.MCP_TYSum<CurrentAnalyzer->TYSumHigh;
    if (cond)h_MCPImage_Cal->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
    double X = RecData.MCPPos_X;
    double Y = RecData.MCPPos_Y;
    double rotX = RecData.MCPPos_X*cos(RotationAngle)-RecData.MCPPos_Y*sin(RotationAngle);
    double rotY = RecData.MCPPos_Y*cos(RotationAngle)+RecData.MCPPos_X*sin(RotationAngle);
    bool cond2;
    cond2 = rotX>-5 && rotX<5 && rotY>-5 && rotY<5;
    if(cond && cond2){
      h_CenterX->Fill(rotX);
      h_CenterY->Fill(rotY);
      h_MCPImage_CenterRot->Fill(rotX,rotY);
    }
    h_MCPImage_Center->Fill(X,Y);
  }

  double CenterX = h_CenterX->GetBinCenter(h_CenterX->GetMaximumBin());
  double CenterY = h_CenterY->GetBinCenter(h_CenterY->GetMaximumBin());
//  cout <<CenterX<<" "<<CenterY<<endl;
  TF1* FitFunc = new TF1("FitFunc","gaus(0)+[3]",-2,2);
  FitFunc->SetParameters(h_CenterX->GetBinContent(CenterX)-h_CenterX->GetBinContent(CenterX-3),CenterX,1,h_CenterX->GetBinContent(CenterX-3));
  h_CenterX->Fit(FitFunc,"","",CenterX-1.5,CenterX+1.5);
  CenterX = FitFunc->GetParameter(1);
  double WidthX = FitFunc->GetParameter(2);

  FitFunc->SetParameters(h_CenterY->GetBinContent(CenterY)-h_CenterY->GetBinContent(CenterY-2),CenterY,1,h_CenterY->GetBinContent(CenterY-2));
  h_CenterY->Fit(FitFunc,"","",CenterY-1.0,CenterY+1.0);
  CenterY = FitFunc->GetParameter(1);
  double WidthY = FitFunc->GetParameter(2);
  double WidthZ = WidthX;	//Assuming the B field is symmetric

  //Fit to 2D gaussian
  TF2* FitFunc2D = new TF2("FitFunc2D",Gauss2DBkg,-2.0,2.0,-2.0,2.0,7);
  double Max2D = h_MCPImage_Center->GetMaximum();
  FitFunc2D->SetParameters(Max2D,CenterX,CenterY,WidthX,WidthY,0.0,0.0);
  
  h_MCPImage_Center->Fit(FitFunc2D);

  //Write to calibration file

  cout <<"Writing to calibration files.\n";

  char filename[100];

  ofstream MOTCalFileOut;
  sprintf(filename,"%s/MOTPosCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  MOTCalFileOut.open(filename,ios::out);
  MOTCalFileOut<<CenterX<<" "<<WidthX<<" "<<CenterY<<" "<<WidthY<<" "<<WidthZ;
  MOTCalFileOut.close();

  //f->Close();
  //Close input file
  if (tfilein!=NULL){
    tfilein->Close();
    delete tfilein;
    tfilein =NULL;
  }
  //Write to root file

  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_MOTCal.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");

  h_MCPImage_Cal->Write();
  h_CenterX->Write();
  h_CenterY->Write();
  h_MCPImage_Center->Write();
  h_MCPImage_CenterRot->Write();

  histfile->Close();
  return 0;
}

//Calibration based on loaded histograms
int Calibration::CalibrateMOTSimple(int CalID)
{
  string spectrumfile;

  double RotationAngle = -0.3927;	//22.5 deg

  //Histograms
  TH2* h_MCPImage_Center = (TH2*)(CurrentAnalyzer->CombExpHistBox["MCP_Image_Zoom"].Hist2DList[0][0]->Clone());
  TH1* h_CenterX = (TH1*)(CurrentAnalyzer->CombExpHistBox["MCP_Image_Zoom"].Hist2DList[0][0]->ProjectionX()->Clone());
  TH1* h_CenterY = (TH1*)(CurrentAnalyzer->CombExpHistBox["MCP_Image_Zoom"].Hist2DList[0][0]->ProjectionY()->Clone());;

  //Read In Data

  double CenterX = h_CenterX->GetBinCenter(h_CenterX->GetMaximumBin());
  double CenterY = h_CenterY->GetBinCenter(h_CenterY->GetMaximumBin());
//  cout <<CenterX<<" "<<CenterY<<endl;
  TF1* FitFunc = new TF1("FitFunc","gaus(0)+[3]",-2,2);
  FitFunc->SetParameters(h_CenterX->GetBinContent(CenterX),CenterX,1,0.0);
  h_CenterX->Fit(FitFunc,"","",CenterX-1.5,CenterX+1.5);
  CenterX = FitFunc->GetParameter(1);
  double WidthX = FitFunc->GetParameter(2);

  FitFunc->SetParameters(h_CenterY->GetBinContent(CenterY),CenterY,1,0.0);
  h_CenterY->Fit(FitFunc,"","",CenterY-1.5,CenterY+1.5);
  CenterY = FitFunc->GetParameter(1);
  double WidthY = FitFunc->GetParameter(2);
  double WidthZ = WidthX;	//Assuming the B field is symmetric

  //Fit to 2D gaussian
  TF2* FitFunc2D = new TF2("FitFunc2D",Gauss2DBkg,-2.0,2.0,-2.0,2.0,7);
  double Max2D = h_MCPImage_Center->GetMaximum();
  FitFunc2D->SetParameters(Max2D,CenterX,CenterY,WidthX,WidthY,0.0,0.0);
  
  h_MCPImage_Center->Fit(FitFunc2D);

  //Write to calibration file

  cout <<"Writing to calibration files.\n";

  char filename[100];

  ofstream MOTCalFileOut;
  sprintf(filename,"%s/MOTPosCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  MOTCalFileOut.open(filename,ios::out);
  MOTCalFileOut<<CenterX<<" "<<WidthX<<" "<<CenterY<<" "<<WidthY<<" "<<WidthZ;
  MOTCalFileOut.close();

  //Write to root file

  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + CurrentAnalyzer->ExpFilePrefix + string("_MOTCal.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");

  h_MCPImage_Center->Write();
  h_CenterX->Write();
  h_CenterY->Write();
  histfile->Close();
  return 0;
}
/***********************************************************************************/
int Calibration::CalibrateMOTPicture(string CalFile, string ImageFile, string BkgFile,int CalID)
{
  string CalFileName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + CalFile + string(".bmp");
  string ImageFileName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + ImageFile + string(".bmp");
  string BkgFileName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + BkgFile + string(".bmp");

  string CalCircleFileName = CurrentAnalyzer->CALIBRATION_DIRECTORY + string("/") + CalFile + string(".txt");
  string BkgCircleFileName = CurrentAnalyzer->CALIBRATION_DIRECTORY + string("/") + BkgFile + string(".txt");
  TH2* histCal = new TH2D("histCal","Calibration",1392,0,1392,1040,0,1040);
  TH2* histImage = new TH2D("histImage","Image",696,0,1392,520,0,1040);
  TH2* histBkg = new TH2D("histBkg","Background",696,0,1392,520,0,1040);
  TH2* histBkgSub;

  TGraph* CalCircle = new TGraph();;
  CalCircle->SetName("CalCircle");
  CalCircle->SetTitle("CalCircle");
  TGraph* BkgCircle = new TGraph();
  BkgCircle->SetName("BkgCircle");
  BkgCircle->SetTitle("BkgCircle");
  
  //Get Circle graph
  ifstream circlefile;
  circlefile.open(CalCircleFileName.c_str(),ios::in);
  int NPoints=0;
  circlefile>>NPoints;
  for (int i=0;i<NPoints;i++){
    double x,y;
    circlefile>>x>>y;
    CalCircle->SetPoint(i,x,y);
  }
  circlefile.close();
  circlefile.open(BkgCircleFileName.c_str(),ios::in);
  circlefile>>NPoints;
  for (int i=0;i<NPoints;i++){
    double x,y;
    circlefile>>x>>y;
    BkgCircle->SetPoint(i,x*2,y*2);
  }
  circlefile.close();
  //fit to ellipse
  TVectorD conicCal = fit_ellipse(CalCircle);
  TVectorD ellipseCal = ConicToParametric(conicCal);
  TVectorD conicBkg = fit_ellipse(BkgCircle);
  TVectorD ellipseBkg = ConicToParametric(conicBkg);

  cout << "Ruler:"<<endl;
  cout << "x0 = " << ellipseCal[0] << endl;
  cout << "y0 = " << ellipseCal[1] << endl;
  cout << "a = " << ellipseCal[2] << endl;
  cout << "b = " << ellipseCal[3] << endl;
  cout << "Bkg:"<<endl;
  cout << "x0 = " << ellipseBkg[0] << endl;
  cout << "y0 = " << ellipseBkg[1] << endl;
  cout << "a = " << ellipseBkg[2] << endl;
  cout << "b = " << ellipseBkg[3] << endl;

  TEllipse *EllipseFitCal;
  EllipseFitCal = new TEllipse(ellipseCal[0], ellipseCal[1], // "x0", "y0"
      ellipseCal[2], ellipseCal[3], // "a", "b"
      0, 360,
      ellipseCal[4]); // "theta" (in degrees)
  EllipseFitCal->SetFillStyle(0); // hollow
  TEllipse *EllipseFitBkg;
  EllipseFitBkg = new TEllipse(ellipseBkg[0], ellipseBkg[1], // "x0", "y0"
      ellipseBkg[2], ellipseBkg[3], // "a", "b"
      0, 360,
      ellipseBkg[4]); // "theta" (in degrees)
  EllipseFitBkg->SetFillStyle(0); // hollow

  //Calibration
  BMP2ROOT(histCal,CalFileName);
  //Image
  BMP2ROOT(histImage,ImageFileName);
  //Bkg
  BMP2ROOT(histBkg,BkgFileName);

  //Clean out the black bkg
  double BkgThreshold = 20.0;
  for (int i=1;i<=1392;i++){
    for (int j=1;j<=1040;j++){
      if (histCal->GetBinContent(i,j)<BkgThreshold || i<630 ||i>895 || j<280 || j> 575)histCal->SetBinContent(i,j,0);
    }
  }
  for (int i=1;i<=696;i++){
    for (int j=1;j<=520;j++){
      if (histImage->GetBinContent(i,j)<BkgThreshold || i<315 ||i>445 || j<150 || j> 290)histImage->SetBinContent(i,j,0);
      if (histBkg->GetBinContent(i,j)<BkgThreshold || i<315 ||i>445 || j<150 || j> 290)histBkg->SetBinContent(i,j,0);
    }
  }

  //Bkg subtraction
  histBkgSub = (TH2*)histImage->Clone();
  histBkgSub->SetName("histBkgSub");
  histBkgSub->SetTitle("Background Subtracted Image");
  histBkgSub->Add(histBkg,-1.0);

  TH1* projectX = (TH1*)histBkgSub->ProjectionX("ProjectionX",170,300)->Clone();
  TH1* projectY = (TH1*)histBkgSub->ProjectionY("ProjectionY",325,425)->Clone();

  projectX->Fit("gaus","","",650,850);
  projectY->Fit("gaus","","",350,480);

  //Output

  ostringstream convert;
  convert << setfill('0') << setw(2)<<CalID;
  string OutName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + string("MOTPictureCalibration") + string("_") + convert.str();
  OutName += ".root";
  TFile *f = new TFile (OutName.c_str(), "recreate");

  histCal->Write();
  histImage->Write();
  histBkg->Write();
  histBkgSub->Write();
  projectX->Write();
  projectY->Write();
  CalCircle->Write();
  BkgCircle->Write();
  EllipseFitCal->Write("EllipseCal");
  EllipseFitBkg->Write("EllipseBkg");

  f->Close();


  return 0;
}
/***********************************************************************************/
int BMP2ROOT(TH2* hist,string filename)
{
  unsigned char ImgData[3][3];
  int i,j,k;
  ifstream fpBMP;
  fileHeader * fh;
  fileInfo * fi;
  rgbq * fq;

  fpBMP.open(filename.c_str(),ios::binary);
  if (fpBMP.eof())
  {
    printf("Failed to load file.\n");
    exit(0);
  }

  fh = new fileHeader;
  fi = new fileInfo;

  fpBMP.read((char*)fh,sizeof(fileHeader));
  fpBMP.read((char*)fi,sizeof(fileInfo));

  cout <<"Reading BMP File "<< filename<<endl;
  cout << "Size: " << fi->biWidth<<" x "<<fi->biHeight<<endl;

  fq=(rgbq *)malloc(256*sizeof(rgbq));
  for(i=0;i<256;i++){
    fq[i].rgbBlue=fq[i].rgbGreen=fq[i].rgbRed=i;
  }

  short pix;
  char buffer[2000];
  fpBMP.read(buffer,fh->bfOffBits-14-40);
  for ( i=0;i<fi->biHeight;i++ ){
    for(j=0;j<(fi->biWidth+3)/4*4;j++){
      pix=0;//clear the non-used bits
      fpBMP.read((char*)(&pix),1);
      hist->SetBinContent(j+1,i+1,pix);
    }
  }

  fpBMP.close();

  return 0;
}

/***********************************************************************************/
