//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>
#include <stdint.h>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
#include "TRandom3.h"
//Analyzer includes
#include "Analyzer.h"
#include "GlobalConstants.h"

using namespace std;

/***********************************************************************************/
/******************Exp-Condition and file list Processing***************************/
/***********************************************************************************/
/*********Define allowed conditions**************/
void Analyzer::SetAllowedExpConditions(){
  //Define allowed ExpConditions and link the names to variables
  AllowedExpConditions["ScintThreshold"] = &ScintThreshold;
  AllowedExpConditions["ScintUpperbound"] = &ScintUpperbound;
  AllowedExpConditions["ScintMidpoint"] = &ScintMidpoint;
  AllowedExpConditions["MWPCThreshold"] = &MWPCThreshold;
  AllowedExpConditions["MWPC_R"] = &MWPC_R;
  AllowedExpConditions["MWPC_RIn"] = &MWPC_RIn;
  AllowedExpConditions["MWPC_X"] = &MWPC_X;
  AllowedExpConditions["MWPC_Y"] = &MWPC_Y;
  AllowedExpConditions["MCP_R"] = &MCP_R;
  AllowedExpConditions["MCP_RIn"] = &MCP_RIn;
  AllowedExpConditions["MCP_ThetaLow"] = &MCP_ThetaLow;
  AllowedExpConditions["MCP_ThetaHigh"] = &MCP_ThetaHigh;
  AllowedExpConditions["MCP_X"] = &MCP_X;
  AllowedExpConditions["MCP_Y"] = &MCP_Y;
  AllowedExpConditions["MCP_QLow"] = &MCP_QLow;
  AllowedExpConditions["MCP_QHigh"] = &MCP_QHigh;
  AllowedExpConditions["TXSumLow"] = &TXSumLow;
  AllowedExpConditions["TXSumHigh"] = &TXSumHigh;
  AllowedExpConditions["TYSumLow"] = &TYSumLow;
  AllowedExpConditions["TYSumHigh"] = &TYSumHigh;
  AllowedExpConditions["TMWPCLow"] = &TMWPCLow;
  AllowedExpConditions["TMWPCHigh"] = &TMWPCHigh;
  AllowedExpConditions["TOFLow"] = &TOFLow;
  AllowedExpConditions["TOFHigh"] = &TOFHigh;
  AllowedExpConditions["TOFMidpoint"] = &TOFMidpoint;
  AllowedExpConditions["QValueLow"] = &QValueLow;
  AllowedExpConditions["QValueHigh"] = &QValueHigh;
  AllowedExpConditions["TriggerBlock"] = &TriggerBlock;
  AllowedExpConditions["GroupTimeLow"] = &GroupTimeLow;
  AllowedExpConditions["GroupTimeHigh"] = &GroupTimeHigh;
  AllowedExpConditions["MCPTimeLow"] = &MCPTimeLow;
  AllowedExpConditions["MCPTimeHigh"] = &MCPTimeHigh;
  AllowedExpConditions["LaserSwitchStartTime"] = &SwitchStart;
}

/***********List allowed conditions**************/
int Analyzer::LsExpConditions()
{
	cout<<"\nList of Possible Exp Conditions:"<<endl;
  map <string,double * >::iterator it;
  for (it=AllowedExpConditions.begin();it!=AllowedExpConditions.end();it++){
    cout << it->first<<endl;
  }
  cout<<"\n";
  return 0;
}
/***********List enabled conditions**************/
int Analyzer::LsEnabledExpCondFunc()
{
  map <string,bool (Analyzer::*)(RecDataStruct& RecData)>::iterator it;
  cout<<"\nList of Enabled NonLoop Exp Conditions:"<<endl;
  for (it=ExpNonLoopFunctions.begin();it!=ExpNonLoopFunctions.end();it++){
    cout << it->first<<endl;
  }
  cout<<"\nList of Enabled Loop Exp Conditions:"<<endl;
  for (it=ExpLoopFunctions.begin();it!=ExpLoopFunctions.end();it++){
    cout << it->first<<endl;
  }
  cout<<"\n";
  return 0;
}
/*********Define allowed conditions**************/
int Analyzer::SetDefaultExpConditions(){
  InitExpCondition("ScintThreshold",1,10,0);
  InitExpCondition("ScintUpperbound",1,4000.0,0);
  InitExpCondition("ScintMidpoint",1,1300.0,0);
  InitExpCondition("MWPCThreshold",1,0.05,0);
  InitExpCondition("MWPC_R",1,14.0,0);
  InitExpCondition("MWPC_RIn",1,0.0,0);
  InitExpCondition("MWPC_X",1,0.0,0);
  InitExpCondition("MWPC_Y",1,0.0,0);
  InitExpCondition("MCP_R",1,35.0,0);
  InitExpCondition("MCP_RIn",1,0.0,0);
  InitExpCondition("MCP_ThetaLow",1,-PI,0);
  InitExpCondition("MCP_ThetaHigh",1,PI,0);
  InitExpCondition("MCP_X",1,0.0,0);
  InitExpCondition("MCP_Y",1,0.0,0);
  InitExpCondition("MCP_QLow",1,0.0,0);
  InitExpCondition("MCP_QHigh",1,300e3,0);
  InitExpCondition("TXSumLow",1,-1000.0,0);
  InitExpCondition("TXSumHigh",1,2000.0,0);
  InitExpCondition("TYSumLow",1,-1000.0,0);
  InitExpCondition("TYSumHigh",1,2000.0,0);
  InitExpCondition("TMWPCLow",1,-200,0);
  InitExpCondition("TMWPCHigh",1,300.0,0);
  InitExpCondition("TOFLow",1,75.0,0);
  InitExpCondition("TOFHigh",1,475.0,0);
  InitExpCondition("TOFMidpoint",1,200.0,0);
  InitExpCondition("QValueLow",1,2977.0,0);
  InitExpCondition("QValueHigh",1,4043.0,0);
  InitExpCondition("TriggerBlock",1,0,0);
  InitExpCondition("GroupTimeLow",1,0,0);
  InitExpCondition("GroupTimeHigh",1,20000,0);
  InitExpCondition("MCPTimeLow",1,0,0);
  InitExpCondition("MCPTimeHigh",1,2000000,0);
  InitExpCondition("LaserSwitchStartTime",1,2000000,0);
  UnSetExpCondLoopPtrs();
  return 0;
}

/*********Fill ExpForbiddenCondFunctions by default**************/
void Analyzer::InitDefaultExpForbiddenCondFunctions()
{
  ExpForbiddenCondFunctions.clear();
  ExpForbiddenCondFunctions.push_back("ExpCond_ScintE");
  ExpForbiddenCondFunctions.push_back("ExpCond_MWPCE");
  ExpForbiddenCondFunctions.push_back("ExpCond_MWPCT");
  ExpForbiddenCondFunctions.push_back("ExpCond_MWPCArea");
  ExpForbiddenCondFunctions.push_back("ExpCond_MCPArea");
  ExpForbiddenCondFunctions.push_back("ExpCond_MCPT1T2");
  ExpForbiddenCondFunctions.push_back("ExpCond_TOFRange");
  ExpForbiddenCondFunctions.push_back("ExpCond_TOFScintE2D");
  ExpForbiddenCondFunctions.push_back("ExpCond_QValue");
  ExpForbiddenCondFunctions.push_back("ExpCond_QValue_C");
  ExpForbiddenCondFunctions.push_back("ExpCond_TriggerBlock");
  ExpForbiddenCondFunctions.push_back("ExpCond_GroupTime");
  ExpForbiddenCondFunctions.push_back("ExpCond_MCPTime");
  ExpForbiddenCondFunctions.push_back("ExpCond_MCPCharge");
}

void Analyzer::FindAndRemoveVector(vector<string> & StrVec,string Name)
{
  vector<string>::iterator it;
  it = find(StrVec.begin(), StrVec.end(), Name);
  if(it!=StrVec.end()) {StrVec.erase(it); cout <<"Removing "<<Name<<" from string vector."<<endl;}
  else cout << "No entry " << Name << " found in string vector."<<endl;  
}
/********Loop Pointers for conditions**************/
int Analyzer::AddExpCondLoopPtr(int index, string Name)
{
  if (ExpConditionListBox.find(Name)==ExpConditionListBox.end()){
    cout << "ExpCondition " << Name << " is not setup yet!\n";
    return -1;
  }
  if (index==1){
    LinkedPtr newPtr;
    newPtr.array_ptr = &(ExpConditionListBox[Name]);
    newPtr.linked_ptr = AllowedExpConditions[Name];
    CondLoopPtrs1.push_back(newPtr);
  }else if (index==2){
    LinkedPtr newPtr;
    newPtr.array_ptr = &(ExpConditionListBox[Name]);
    newPtr.linked_ptr = AllowedExpConditions[Name];
    CondLoopPtrs2.push_back(newPtr);
  }else{
    cout << "Only 1 or 2!\n";
    return -1;
  }
  //Set Loop Functions
  if (Name.compare("ScintThreshold")==0)
    SetExpCondFuncMap("ExpCond_ScintE",true,true);
  else if (Name.compare("ScintUpperbound")==0)
    SetExpCondFuncMap("ExpCond_ScintE",true,true);
  else if (Name.compare("ScintMidpoint")==0)
    SetExpCondFuncMap("ExpCond_TOFScintE2D",true,true);
  else if (Name.compare("MWPCThreshold")==0)
    SetExpCondFuncMap("ExpCond_MWPCE",true,true);
  else if (Name.compare("MWPC_R")==0)
    SetExpCondFuncMap("ExpCond_MWPCArea",true,true);
  else if (Name.compare("MWPC_RIn")==0)
    SetExpCondFuncMap("ExpCond_MWPCArea",true,true);
  else if (Name.compare("MWPC_X")==0)
    SetExpCondFuncMap("ExpCond_MWPCArea",true,true);
  else if (Name.compare("MWPC_Y")==0)
    SetExpCondFuncMap("ExpCond_MWPCArea",true,true);
  else if (Name.compare("MCP_R")==0)
    SetExpCondFuncMap("ExpCond_MCPArea",true,true);
  else if (Name.compare("MCP_RIn")==0)
    SetExpCondFuncMap("ExpCond_MCPArea",true,true);
  else if (Name.compare("MCP_ThetaLow")==0)
    SetExpCondFuncMap("ExpCond_MCPArea",true,true);
  else if (Name.compare("MCP_ThetaHigh")==0)
    SetExpCondFuncMap("ExpCond_MCPArea",true,true);
  else if (Name.compare("MCP_X")==0)
    SetExpCondFuncMap("ExpCond_MCPArea",true,true);
  else if (Name.compare("MCP_Y")==0)
    SetExpCondFuncMap("ExpCond_MCPArea",true,true);
  else if (Name.compare("MCP_QLow")==0)
    SetExpCondFuncMap("ExpCond_MCPCharge",true,true);
  else if (Name.compare("MCP_QHigh")==0)
    SetExpCondFuncMap("ExpCond_MCPCharge",true,true);
  else if (Name.compare("TMWPCLow")==0)
    SetExpCondFuncMap("ExpCond_MWPCT",true,true);
  else if (Name.compare("TMWPCHigh")==0)
    SetExpCondFuncMap("ExpCond_MWPCT",true,true);
  else if (Name.compare("TXSumLow")==0)
    SetExpCondFuncMap("ExpCond_MCPT1T2",true,true);
  else if (Name.compare("TXSumHigh")==0)
    SetExpCondFuncMap("ExpCond_MCPT1T2",true,true);
  else if (Name.compare("TYSumLow")==0)
    SetExpCondFuncMap("ExpCond_MCPT1T2",true,true);
  else if (Name.compare("TYSumHigh")==0)
    SetExpCondFuncMap("ExpCond_MCPT1T2",true,true);
  else if (Name.compare("TOFLow")==0)
    SetExpCondFuncMap("ExpCond_TOFRange",true,true);
  else if (Name.compare("TOFHigh")==0)
    SetExpCondFuncMap("ExpCond_TOFRange",true,true);
  else if (Name.compare("TOFMidpoint")==0)
    SetExpCondFuncMap("ExpCond_TOFScintE2D",true,true);
  else if (Name.compare("QValueLow")==0)
    SetExpCondFuncMap("ExpCond_QValue",true,true);
  else if (Name.compare("QValueHigh")==0)
    SetExpCondFuncMap("ExpCond_QValue",true,true);
  else if (Name.compare("TriggerBlock")==0)
    SetExpCondFuncMap("ExpCond_TriggerBlock",true,true);
  else if (Name.compare("GroupTimeLow")==0)
    SetExpCondFuncMap("ExpCond_GroupTime",true,true);
  else if (Name.compare("GroupTimeHigh")==0)
    SetExpCondFuncMap("ExpCond_GroupTime",true,true);
  else if (Name.compare("MCPTimeLow")==0)
    SetExpCondFuncMap("ExpCond_MCPTime",true,true);
  else if (Name.compare("MCPTimeHigh")==0)
    SetExpCondFuncMap("ExpCond_MCPTime",true,true);
  else if (Name.compare("LaserSwitchStartTime")==0)
    SetExpCondFuncMap("ExpCond_LaserSwitch",true,true);
  return 0;
}

int Analyzer::UnSetExpCondLoopPtrs()
{
  CondLoopPtrs1.clear();
  CondLoopPtrs2.clear();
  nCondExp1 = 1;
  nCondExp2 = 1;

  InitExpCondFuncMap();
  return 0;
}

/*************Function list operations******************/
int Analyzer::SetExpCondFuncMap(string Name,bool On, bool Loop)
{
  bool (Analyzer::*ExpCondFunPtr)(RecDataStruct& RecData);

  if (Name.compare("ExpCond_ScintE")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_ScintE;
  }else if (Name.compare("ExpCond_MWPCE")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_MWPCE;
  }else if (Name.compare("ExpCond_MWPCT")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_MWPCT;
  }else if (Name.compare("ExpCond_MWPCArea")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_MWPCArea;
  }else if (Name.compare("ExpCond_MCPArea")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_MCPArea;
  }else if (Name.compare("ExpCond_MCPCharge")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_MCPCharge;
  }else if (Name.compare("ExpCond_MCPT1T2")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_MCPT1T2;
  }else if (Name.compare("ExpCond_TOFRange")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_TOFRange;
  }else if (Name.compare("ExpCond_TOFScintE2D")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_TOFScintE2D;
  }else if (Name.compare("ExpCond_QValue")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_QValue;
  }else if (Name.compare("ExpCond_QValue_C")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_QValue_C;
  }else if (Name.compare("ExpCond_TriggerBlock")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_TriggerBlock;
  }else if (Name.compare("ExpCond_GroupTime")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_GroupTime;
  }else if (Name.compare("ExpCond_MCPTime")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_MCPTime;
  }else if (Name.compare("ExpCond_LaserSwitch")==0){
    ExpCondFunPtr = &Analyzer::ExpCond_LaserSwitch;
  }else{
    cout << "The Function " << Name << " is not defined!\n";
    return -1;
  }
  if (!On){
    ExpNonLoopFunctions.erase(Name);
    ExpLoopFunctions.erase(Name);
    return 0;
  }
  if (Loop){
    ExpLoopFunctions[Name] = ExpCondFunPtr;
    ExpNonLoopFunctions.erase(Name);
  }else{
    ExpNonLoopFunctions[Name] = ExpCondFunPtr;
    ExpLoopFunctions.erase(Name);
  }
  return 0;
}

int Analyzer::InitExpCondFuncMap()
{	cout<<"Initializing ExpCond Function Map..."<<endl;
  ExpNonLoopFunctions.clear();
  ExpLoopFunctions.clear();
  SetExpCondFuncMap("ExpCond_ScintE",true);
  SetExpCondFuncMap("ExpCond_MWPCE",true);
  SetExpCondFuncMap("ExpCond_MWPCT",true);
  SetExpCondFuncMap("ExpCond_MWPCArea",true);
  SetExpCondFuncMap("ExpCond_MCPArea",true);
  SetExpCondFuncMap("ExpCond_MCPCharge",false);
  SetExpCondFuncMap("ExpCond_MCPT1T2",true);
  SetExpCondFuncMap("ExpCond_TOFRange",true);
  SetExpCondFuncMap("ExpCond_TOFScintE2D",false);
  SetExpCondFuncMap("ExpCond_QValue",true);
  SetExpCondFuncMap("ExpCond_QValue_C",false);
  SetExpCondFuncMap("ExpCond_TriggerBlock",true);
  SetExpCondFuncMap("ExpCond_GroupTime",false);
  SetExpCondFuncMap("ExpCond_MCPTime",false);
  SetExpCondFuncMap("ExpCond_LaserSwitch",false);
  int n = ExpForbiddenCondFunctions.size();
  cout<<"Disabling forbidden ExpCondition functions:"<<endl;
  for (int i=0;i<n;i++){
    SetExpCondFuncMap(ExpForbiddenCondFunctions[i],false);
    cout<<ExpForbiddenCondFunctions[i]<<endl;
  }
  return 0;
}
/***************Reset Exp Condition Maps******************************************/

int Analyzer::ResetExpConditions()
{
  if(!ExpConditionListBox.empty())ExpConditionListBox.clear();
  if(!SysWFiles.empty())SysWFiles.clear();
  SetDefaultExpConditions();
  //InitExpCondFuncMap(); //Controls looped and not-looped function calls -- called in SetDefaultExpConditions() via UnSetExpCondLoopPtrs();
  nCondExp1=nCondExp2=1;
  nFileExp1=nFileExp2=1;
  return 0;
}

/*************Initialize ExpCondition settings*************/
int Analyzer::InitExpCondition(string Name,int N, double low, double interval)
{
  //check name
  if (AllowedExpConditions.find(Name)==AllowedExpConditions.end()){
    cout << "Unidentified purpose "<<Name<<endl;
    return -1;
  }

  if (ExpConditionListBox.find(Name)!=ExpConditionListBox.end())ExpConditionListBox[Name].clear();
  for (int i=0;i<N;i++){
    ExpConditionListBox[Name].push_back(low+i*interval);
  }
  *(AllowedExpConditions[Name]) = ExpConditionListBox[Name][0];
  return 0;
}

/***********************Get ExpCondition********************/
vector<double> Analyzer::GetExpCondition(string Name)
{
  if (ExpConditionListBox.find(Name)==ExpConditionListBox.end()){
    cout << "ExpCondition " << Name << " is not initialized or allowed!\n";
    exit(1);
  }
  else{
    return ExpConditionListBox[Name];
  }
}
/***********************Set ExpCondition********************/
int Analyzer::SetExpCondition(string Name,int i,double val)
{
  if (ExpConditionListBox.find(Name)==ExpConditionListBox.end()){
    cout << "ExpCondition " << Name << " is not initialized or allowed!\n";
    return -1;
  }
  if (i>=ExpConditionListBox[Name].size()){
    cout << "Current ExpCondition " << Name << " does not have long enough list.\n";
    cout << "Please initialize this condition list with longer length and try again.\n";
    return -1;
  }
  ExpConditionListBox[Name][i] = val;
  return 0;
}

/*************Check ExpCondition settings*************/
int Analyzer::CheckExpConditionSettings()
{
  int n = ExpForbiddenCondFunctions.size();
  cout<<"Disabling forbidden ExpCondition functions:"<<endl;
  for (int i=0;i<n;i++){
    SetExpCondFuncMap(ExpForbiddenCondFunctions[i],false);
    cout<<ExpForbiddenCondFunctions[i]<<endl;
  }
  map <string,double *>::iterator it; 
  map <string,vector<double> >::iterator it2;
  for (it = AllowedExpConditions.begin();it != AllowedExpConditions.end();++it){
    it2=ExpConditionListBox.find(it->first);
    if(it2==ExpConditionListBox.end()){
      cout << "ExpCondition "<< it->first <<" is not initialized yet!\n";
      return 1;
    }
    if((it2->second).size()==0){
      cout << "ExpCondition "<< it->first <<" list is empty!\n";
	return 1;
    }
    //Update all current conditions
    *(it->second) = it2->second[0];
  }
  //check loop pointers
  if (CondLoopPtrs1.size()==0)
    nCondExp1 = 1;
  else
    nCondExp1 = (*CondLoopPtrs1[0].array_ptr).size();
  if (CondLoopPtrs2.size()==0)
    nCondExp2 = 1;
  else
    nCondExp2 = (*CondLoopPtrs2[0].array_ptr).size();

  vector <LinkedPtr>::iterator it3;
  for (it3 = CondLoopPtrs1.begin();it3 != CondLoopPtrs1.end();++it3){
    if ((*(it3->array_ptr)).size() != (CondLoopPtrs1[0].array_ptr)->size()){
      cout << "Linked ExpConditions don't have same dimenstions!\n";
      return 1;
    }
  }
  for (it3 = CondLoopPtrs2.begin();it3 != CondLoopPtrs2.end();++it3){
    if ((*(it3->array_ptr)).size() != (CondLoopPtrs2[0].array_ptr)->size()){
      cout << "Linked ExpConditions don't have same dimenstions!\n";
      return 1;
    }
  }
  if(FileCondLnkd){
    if (nFileExp1 != (CondLoopPtrs1[0].array_ptr)->size()){
      cout << "File dimensions don't equal Linked ExpConditions dimensions!\n";
      return 1;
    }
  }

  return 0;
}

/**********Process ExpConditions*************/
void Analyzer::ProcessExpConditions(RecDataStruct& RecData, int iFile)
{
  bool returnval1 = true;
  bool returnval2 = true;
  //Process laser and push beam vetos
  double Time = *RecData.Time_rel;
  bool vetoed = CheckPushBeamVeto(Time);
  //bool vetoed = CheckPushBeamVeto(Time) ||  CheckLaserSwitchVeto(Time); //Added to ExpConditions
  //Process non-looped condition functions
  map <string,bool (Analyzer::*)(RecDataStruct& RecData)>::iterator it;
  for (it = ExpNonLoopFunctions.begin();it != ExpNonLoopFunctions.end();++it){
    returnval1 = returnval1 && (this->*(it->second))(RecData);
  }
  if (!returnval1 || KillEvent||vetoed){
    for (int i=0;i<nCondExp1;i++){
      for (int j=0;j<nCondExp2;j++){
	Conditions[i][j] = false;
      }
    }
    return;
  }
  //Process looped condition functions
  if (ExpLoopFunctions.size()==0){//If there is not looped functions, return value from the non-looped functions (nCondExp1 and nCondExp2 should both equal 1 in this case)
    for (int i=0;i<nCondExp1;i++){
      for (int j=0;j<nCondExp2;j++){
	Conditions[i][j] = returnval1;
      }
    }
    return;
  }

  if(FileCondLnkd){
    int i = iFile;
    //Update current condition pointers
    vector <LinkedPtr>::iterator it2;
    for (it2 = CondLoopPtrs1.begin();it2 != CondLoopPtrs1.end();++it2){
      *(it2->linked_ptr) = (*(it2->array_ptr))[i];
    }
    //Invoke function
    for (it = ExpLoopFunctions.begin();it != ExpLoopFunctions.end();++it){
      returnval2 = returnval1 && (this->*(it->second))(RecData);
      if (!returnval2) break;
    }
    Conditions[0][0] = returnval2;

  }
  else{
    for (int i=0;i<nCondExp1;i++){
      for (int j=0;j<nCondExp2;j++){
	//Update current condition pointers
	vector <LinkedPtr>::iterator it2;
	for (it2 = CondLoopPtrs1.begin();it2 != CondLoopPtrs1.end();++it2){
	  *(it2->linked_ptr) = (*(it2->array_ptr))[i];
	}
	for (it2 = CondLoopPtrs2.begin();it2 != CondLoopPtrs2.end();++it2){
	  *(it2->linked_ptr) = (*(it2->array_ptr))[j];
	}
	//Invoke function
	for (it = ExpLoopFunctions.begin();it != ExpLoopFunctions.end();++it){
	  returnval2 = returnval1 && (this->*(it->second))(RecData);
	  if (!returnval2) break;
	}
	Conditions[i][j] = returnval2;
      }
    }
  }
  //Return all loop iterators to 0 position
  for (int k=0;k<CondLoopPtrs1.size();k++){
    *CondLoopPtrs1[k].linked_ptr = (*CondLoopPtrs1[k].array_ptr)[0];
  }
  for (int k=0;k<CondLoopPtrs2.size();k++){
    *CondLoopPtrs2[k].linked_ptr = (*CondLoopPtrs2[k].array_ptr)[0];
  }
}


/*********ExpCondition Test Functions*************/
bool Analyzer::ExpCond_ScintE(RecDataStruct& RecData)
{
  if (RecData.EScintA[0]>=ScintThreshold && RecData.EScintA[0]<=ScintUpperbound) return true;
  else return false;
}

bool Analyzer::ExpCond_MWPCE(RecDataStruct& RecData)
{
//  if (RecData.EMWPC_anode1>=MWPCThreshold && RecData.EMWPC_anode2>=MWPCThreshold) cout<<"MWPCE condition: success"<<endl;
//  else cout<<"MWPCE condition: fail"<<endl;
  if (RecData.EMWPC_anode1+RecData.EMWPC_anode2>=MWPCThreshold) return true;
//  if (RecData.EMWPC_anode1>=MWPCThreshold && RecData.EMWPC_anode2>=MWPCThreshold) return true;
  else return false;
}

bool Analyzer::ExpCond_MWPCArea(RecDataStruct& RecData)
{
  double R;
  double Theta;
  R = sqrt(pow(RecData.MWPCPos_X-MWPC_X,2.0) + pow(RecData.MWPCPos_Y-MWPC_Y,2.0));
  Theta = asin((RecData.MWPCPos_Y-MWPC_Y)/R);
  if (RecData.MWPCPos_X<0 && Theta>0) Theta = PI - Theta;
  if (RecData.MWPCPos_X<0 && Theta<0) Theta = -PI - Theta;
  if (RecData.MWPCPos_X<0 && Theta==0) Theta = -PI;

//  if (R <= MWPC_R && R >= MWPC_RIn) cout << "MWPCArea condition: success"<<endl;
//  else cout << "MWPCArea condition: fail"<<endl;
  if (R <= MWPC_R && R >= MWPC_RIn) return true;
  else return false;
}

bool Analyzer::ExpCond_MCPArea(RecDataStruct& RecData)
{
  double R;
  double Theta;
  R = sqrt(pow(RecData.MCPPos_X-MCP_X,2.0) + pow(RecData.MCPPos_Y-MCP_Y,2.0));
  Theta = asin((RecData.MCPPos_Y-MCP_Y)/R);
  if (RecData.MCPPos_X-MCP_X<0 && Theta>0) Theta = PI - Theta;
  if (RecData.MCPPos_X-MCP_X<0 && Theta<0) Theta = -PI - Theta;
  if (RecData.MCPPos_X-MCP_X<0 && Theta==0) Theta = -PI;
  if (R <= MCP_R && R >= MCP_RIn && Theta <=MCP_ThetaHigh && Theta >= MCP_ThetaLow) return true;
  else return false; 
}

bool Analyzer::ExpCond_MCPCharge(RecDataStruct& RecData)
{
  if(RecData.QMCP>MCP_QLow && RecData.QMCP<MCP_QHigh) return true;
  else return false;
}
bool Analyzer::ExpCond_MWPCT(RecDataStruct& RecData)
{
//  if (RecData.TBeta_diff[0]>TMWPCLow && RecData.TBeta_diff[0]<TMWPCHigh && RecData.TBeta_diff[1]>TMWPCLow && RecData.TBeta_diff[1]<TMWPCHigh) cout <<"MWPCT condition: success"<<endl;
//  else cout <<"MWPCT condition: fail"<<endl;
  if (RecData.TBeta_diff[0]>TMWPCLow && RecData.TBeta_diff[0]<TMWPCHigh && RecData.TBeta_diff[1]>TMWPCLow && RecData.TBeta_diff[1]<TMWPCHigh) return true;
  else return false;
}

bool Analyzer::ExpCond_MCPT1T2(RecDataStruct& RecData)
{
  if (RecData.MCP_TXSum>TXSumLow && RecData.MCP_TXSum<TXSumHigh && RecData.MCP_TYSum>TYSumLow && RecData.MCP_TYSum<TYSumHigh) return true;
  else return false;
}

bool Analyzer::ExpCond_TOFRange(RecDataStruct& RecData)
{
  if (RecData.TOF>TOFLow && RecData.TOF<TOFHigh) return true;
  else return false;
}

bool Analyzer::ExpCond_TOFScintE2D(RecDataStruct& RecData)
{
  //The TOF limits and ScintE limits are imposed by other functions
  //Only need to apply the linear cut
  if (RecData.TOF<TOFMidpoint && TOFMidpoint>TOFLow){
    if (RecData.EScintA[0]<=(ScintUpperbound+(ScintMidpoint-ScintUpperbound)/(TOFMidpoint-TOFLow)*(RecData.TOF-TOFLow)))return true;
    else return false;
  }else{
    if (RecData.EScintA[0]<=(ScintUpperbound+(ScintThreshold-ScintUpperbound)/(TOFHigh-TOFMidpoint)*(RecData.TOF-TOFMidpoint)))return true;
    else return false;
  }
}

bool Analyzer::ExpCond_QValue(RecDataStruct& RecData)
{
//  if ((RecData.Q_Value[0]>QValueLow && RecData.Q_Value[0]<QValueHigh) || (RecData.Q_Value[1]>QValueLow && RecData.Q_Value[1]<QValueHigh)) return true;
  if ((RecData.Q_Value[0]>QValueLow && RecData.Q_Value[0]<QValueHigh) || (RecData.Q_Value[1]>QValueLow && RecData.Q_Value[1]<QValueHigh) || (Charge3Switch&&((RecData.Q_Value[2]>QValueLow && RecData.Q_Value[2]<QValueHigh)))) return true;
  else return false;
}

bool Analyzer::ExpCond_QValue_C(RecDataStruct& RecData)
{
//  if ((RecData.Q_Value[0]>QValueLow && RecData.Q_Value[0]<QValueHigh) || (RecData.Q_Value[1]>QValueLow && RecData.Q_Value[1]<QValueHigh)) return false;
  if ((RecData.Q_Value[0]>QValueLow && RecData.Q_Value[0]<QValueHigh) || (RecData.Q_Value[1]>QValueLow && RecData.Q_Value[1]<QValueHigh) || (Charge3Switch&&((RecData.Q_Value[2]>QValueLow && RecData.Q_Value[2]<QValueHigh)))) return false;
  else return true;
}

bool Analyzer::ExpCond_TriggerBlock(RecDataStruct& RecData)
{
  if ((((unsigned short)TriggerBlock)&(RecData.TriggerMap))==0) return true;
  else return false;
}

bool Analyzer::ExpCond_GroupTime(RecDataStruct& RecData)
{
  if (RecData.GroupTime>GroupTimeLow && RecData.GroupTime<GroupTimeHigh) return true;
  else return false;
}

bool Analyzer::ExpCond_MCPTime(RecDataStruct& RecData)
{
  if (RecData.TMCP_rel>MCPTimeLow && RecData.TMCP_rel<MCPTimeHigh) return true;
  else return false;
}

bool Analyzer::ExpCond_LaserSwitch(RecDataStruct& RecData)
{
  double Time = *RecData.Time_rel;
  bool laser_ON = ((Time<SwitchStart) && (Time>(SwitchStart+N_cycle*SwitchPeriod-PushPeriod))) || (Time>SwitchStart && fmod((Time-SwitchStart),SwitchPeriod)>SwitchPeriod/2.) || (Time<(SwitchStart+N_cycle*SwitchPeriod-PushPeriod) && fmod((Time+PushPeriod-SwitchStart),SwitchPeriod)>SwitchPeriod/2.);
  
  if ((LaserStatus && !laser_ON) || (!LaserStatus && laser_ON)) return false; //Vetoed
  else if((Time>SwitchStart && (fmod((Time-SwitchStart),.5*SwitchPeriod)<(WaitTime)|| fmod((Time-SwitchStart),.5*SwitchPeriod)>(-WaitTime+.5*SwitchPeriod)) || (Time<(SwitchStart+N_cycle*SwitchPeriod-PushPeriod) && (fmod((Time+PushPeriod-SwitchStart),.5*SwitchPeriod)<(WaitTime)||fmod((Time+PushPeriod-SwitchStart),.5*SwitchPeriod)>(-WaitTime+.5*SwitchPeriod)))))
    return false; //Veto laser on/off/on transitions
  else return true;
}

/****END**ExpCondition Test Functions*************/

/*************************File List Operations**************************************/
/***********************************************************************************/
int Analyzer::SetFileExpDim(int dim1,int dim2)
{
  if (dim1>N_OF_HIST || dim2>N_OF_HIST){
    cout << "File dimensions cannot be larger than N_OF_HIST." << endl;
    return -1;
  } 
  nFileExp1 = dim1;
  nFileExp2 = dim2;
  return 0; 
}
/***********************************************************************************/
int Analyzer::SetFileIDEntryExp(int i,int j,int ID) 
{
  FileIDListExp[i][j] = ID;
  return 0;
}
/***********************************************************************************/
int Analyzer::SetFileIDListExp(int j,int startID,int Inc)
{
  for (int i=0;i<nFileExp1;i++){
    FileIDListExp[i][j] = startID+i*Inc;
  }
  return 0;
}
/***********************************************************************************/
int Analyzer::LogExpFileIDList()
{
  char buffer[100];
  AnalysisLog<<"Expulation FileID List:"<<endl;
  for (int j=0;j<nFileExp2;j++){
    for (int i=0;i<nFileExp1;i++){
      sprintf(buffer,"%09d",FileIDListExp[i][j]);
      AnalysisLog<<buffer<<" ";
    }
    AnalysisLog<<endl;
  }
  return 0;
}
/***********************************************************************************/
int Analyzer::LogExpConditionList()
{
  AnalysisLog<<"Experiment condition list:"<<endl;
  map <string,vector<double> >::iterator it;
  for (it=ExpConditionListBox.begin();it!=ExpConditionListBox.end();it++)
  {
    AnalysisLog<<it->first<<"; "<<(it->second).size()<<" conditions: ";
    for (int i=0;i<(it->second).size();i++){
      AnalysisLog<<it->second[i]<<" ";
    }
    AnalysisLog<<endl;
  }
  AnalysisLog<< CondLoopPtrs1.size()<<" ExpConditionPtr1 :";
  for (int i=0;i<CondLoopPtrs1.size();i++){
    map <string,double *>::iterator it2;
    for (it2=AllowedExpConditions.begin();it2!=AllowedExpConditions.end();it2++){
      if (it2->second == CondLoopPtrs1[i].linked_ptr) AnalysisLog<<it2->first<<" ";
    }
  }
  AnalysisLog<<endl;
  AnalysisLog<< CondLoopPtrs2.size()<<" ExpConditionPtr2 :";
  for (int i=0;i<CondLoopPtrs2.size();i++){
    map <string,double *>::iterator it2;
    for (it2=AllowedExpConditions.begin();it2!=AllowedExpConditions.end();it2++){
      if (it2->second == CondLoopPtrs2[i].linked_ptr) AnalysisLog<<it2->first<<" ";
    }
  }
  AnalysisLog<<endl;
  AnalysisLog<<"File related condition list:"<<endl;
  for (it=SysWFiles.begin();it!=SysWFiles.end();it++)
  {
    AnalysisLog<<it->first<<"; "<<(it->second).size()<<" conditions: ";
    for (int i=0;i<(it->second).size();i++){
      AnalysisLog<<it->second[i]<<" ";
    }
    AnalysisLog<<endl;
  }
  /******************Enabled Conditions***********************/
  map <string,bool (Analyzer::*)(RecDataStruct& RecData)>::iterator it2;
  AnalysisLog<<"\nList of Enabled NonLoop Exp Conditions:"<<endl;
  for (it2=ExpNonLoopFunctions.begin();it2!=ExpNonLoopFunctions.end();it2++){
    AnalysisLog << it2->first<<endl;
  }
  AnalysisLog<<"\nList of Enabled Loop Exp Conditions:"<<endl;
  for (it2=ExpLoopFunctions.begin();it2!=ExpLoopFunctions.end();it2++){
    AnalysisLog << it2->first<<endl;
  }
   AnalysisLog<<"\n\n";
  return 0;
}












