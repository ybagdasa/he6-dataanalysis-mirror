/*
 * =====================================================================================
 *
 *       Filename:  Background.cpp
 *
 *    Description:  Background construction, normalization and subtraction 
 *
 *        Version:  1.0
 *        Created:  06/11/2015 14:03:15
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Ran Hong
 *   Organization:  
 *
 * =====================================================================================
 */
#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <stdint.h>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TF1.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TGaxis.h"
#include "TFrame.h"
#include "TPaveText.h"
#include "TRandom3.h"
#include "TLegend.h"
#include "TGraphErrors.h"

//Analyzer includes
#include "Analyzer.h"

/***********************************************************************************/
//Function definations

using namespace std;

/***********************************************************************************/
//Background handling functions
/***********************************************************************************/

/***********************************************************************************/
int Analyzer::SubtractRandomCoincidence(string Name)
{
  if (HistDim1<=1){
    cout <<"Function SubtractRandomCoincidence needs at least two TOF spectrum with different parameter TOFShift."<<endl;
    cout <<"One for normal TOFShift, and the other (very large shift) for studying the random coincidences."<<endl;
    return -1;
  }

//for(int j=0;j<HistDim2;j++){

  TH1* hist_original = ExpHistBox["TOFHist"].Hist1DList[0][0];
  TH1* hist_RandBkg = ExpHistBox["TOFHist"].Hist1DList[1][0];
  TH1* hist_subtracted = (TH1*)hist_original->Clone();
  TH1* hist_suppression = (TH1*)hist_original->Clone();
//  TH1* hist_Scint_EA = (TH1*)ExpHistBox["Scint_EA"].Hist1DList[0][0]->Clone();
//  TH1* hist_CosThetaENu = (TH1*)ExpHistBox["CosThetaENu"].Hist1DList[0][0]->Clone();
//  TH1* hist_EIon1 = (TH1*)ExpHistBox["EIon1"].Hist1DList[0][0]->Clone();
//  TH1* hist_EIon2 = (TH1*)ExpHistBox["EIon2"].Hist1DList[0][0]->Clone();

  if (Name.compare("All")==0){
    hist_original->Fit("pol0","","",-900,-300);
    double bkg = hist_original->GetFunction("pol0")->GetParameter(0);
    hist_original->Fit("pol0","","",1200,1500);
    double bkg_late = hist_original->GetFunction("pol0")->GetParameter(0);
    cout <<"Random const Bkg component of the diffuse He6 background: "<< bkg<<endl;
    cout <<"Random late const Bkg component of the diffuse He6 background: "<< bkg_late<<endl;
    //Fine tune the background subtraction
    int LowBin = hist_original->FindBin(-5);
    int HighBin = hist_original->FindBin(700);
    double Total = hist_original->Integral(LowBin,HighBin)-(HighBin-LowBin+1)*bkg_late;
    int N = hist_original->GetNbinsX();
    hist_suppression->Scale(0);
    double accumulator = 0;
    for (int i=LowBin;i<=HighBin;i++){
      hist_suppression->SetBinContent(i,1.0-accumulator/Total*(1.0-bkg_late/bkg));
      accumulator+=(hist_original->GetBinContent(i)-bkg_late);
    }
    for (int i=1;i<LowBin;i++){
      hist_suppression->SetBinContent(i,1.0);
    }
    for (int i=HighBin+1;i<=N;i++){
      hist_suppression->SetBinContent(i,bkg_late/bkg);
    }
    //Rand bkg subtraction
    for (int i=1;i<=N;i++){
      hist_subtracted->SetBinContent(i,hist_original->GetBinContent(i)-bkg*hist_suppression->GetBinContent(i));
      hist_subtracted->SetBinError(i,hist_original->GetBinError(i));
    }
    ExpHistBox["TOFHist"].Hist1DList[0][0]=hist_subtracted;
    ExtBkgHistBox["SuppressionFactor"]=hist_suppression;
    ExtBkgHistBox["SuppressionFactor"]->SetName("Suppression Factor");
    ExtBkgHistBox["SuppressionFactor"]->SetTitle("Suppression Factor");
    ExtBkgStatus|=1;
  }else if (Name.compare("Q_Cut_ON")==0){
    int N = hist_original->GetNbinsX();
    for (int i=1;i<=N;i++){
      int ii = ExtBkgHistBox["SuppressionFactor"]->FindBin(hist_subtracted->GetBinCenter(i));
      hist_subtracted->SetBinContent(i,hist_original->GetBinContent(i)-hist_RandBkg->GetBinContent(i)*ExtBkgHistBox["SuppressionFactor"]->GetBinContent(ii));
      hist_subtracted->SetBinError(i,sqrt(pow(hist_original->GetBinError(i),2.0)+pow(hist_RandBkg->GetBinError(i),2.0)));
    }
    ExpHistBox["TOFHist"].Hist1DList[0][0]=hist_subtracted;
//    ExtBkgHistBox["Q_Cut_ON"]->SetName("BkgQOn");
//    ExtBkgHistBox["Q_Cut_ON"]->SetTitle("Background Q_Cut ON");
//    ExtBkgStatus|=(1<<1);
    //Other bkg hists
//    ExtBkgHistBox["Scint_EA"]=hist_Scint_EA;
//    ExtBkgHistBox["Scint_EA"]->SetName("Scint_EA");
//    ExtBkgHistBox["Scint_EA"]->SetTitle("Scint_EA");
//    ExtBkgHistBox["CosThetaENu"]=hist_CosThetaENu;
//    ExtBkgHistBox["CosThetaENu"]->SetName("CosThetaENu");
//    ExtBkgHistBox["CosThetaENu"]->SetTitle("CosThetaENu");
//    ExtBkgHistBox["EIon1"]=hist_EIon1;
//    ExtBkgHistBox["EIon1"]->SetName("EIon1");
//    ExtBkgHistBox["EIon1"]->SetTitle("EIon1");
//    ExtBkgHistBox["EIon2"]=hist_EIon2;
//    ExtBkgHistBox["EIon2"]->SetName("EIon2");
//    ExtBkgHistBox["EIon2"]->SetTitle("EIon2");
  }else if (Name.compare("QC_Cut_ON")==0){
    hist_original->Fit("pol0","","",1200,1500);
    double bkg = hist_original->GetFunction("pol0")->GetParameter(0);
    cout <<"Random Bkg component of the trapped He6: "<< bkg<<endl;
    int N = hist_original->GetNbinsX();
    for (int i=1;i<=N;i++){
      hist_subtracted->SetBinContent(i,hist_original->GetBinContent(i)-bkg);
      hist_subtracted->SetBinError(i,hist_original->GetBinError(i));
    }
    ExtBkgHistBox["QC_Cut_ON"]=hist_subtracted;
    ExtBkgHistBox["QC_Cut_ON"]->SetName("BkgQCOn");
    ExtBkgHistBox["QC_Cut_ON"]->SetTitle("Background QC_Cut ON");
    ExtBkgStatus|=(1<<2);
  }else{
    cout <<"Hist name "<<Name<<" is not identified."<<endl;
    return -1;
  }
//}
  return 0;
}

/**********************************************************************************************/
int Analyzer::ConstructBkgHistogram(string Name)
{
  if (HistDim1<=1){
    cout <<"Function ConstructBkgHistogram needs at least two TOF spectrum with different parameter TOFShift."<<endl;
    cout <<"One for normal TOFShift, and the other (very large shift) for studying the random coincidences."<<endl;
    return -1;
  }
  TH1* hist_original = ExpHistBox["TOFHist"].Hist1DList[0][0];
  TH1* hist_RandBkg = ExpHistBox["TOFHist"].Hist1DList[1][0];
  TH1* hist_subtracted = (TH1*)hist_original->Clone();
  TH1* hist_suppression = (TH1*)hist_original->Clone();
  TH1* hist_Scint_EA = (TH1*)ExpHistBox["Scint_EA"].Hist1DList[0][0]->Clone();
  TH1* hist_CosThetaENu = (TH1*)ExpHistBox["CosThetaENu"].Hist1DList[0][0]->Clone();
  TH1* hist_EIon1 = (TH1*)ExpHistBox["EIon1"].Hist1DList[0][0]->Clone();
  TH1* hist_EIon2 = (TH1*)ExpHistBox["EIon2"].Hist1DList[0][0]->Clone();

  if (Name.compare("All")==0){
    hist_original->Fit("pol0","","",-900,-300);
    double bkg = hist_original->GetFunction("pol0")->GetParameter(0);
    hist_original->Fit("pol0","","",1200,1500);
    double bkg_late = hist_original->GetFunction("pol0")->GetParameter(0);
    cout <<"Random const Bkg component of the diffuse He6 background: "<< bkg<<endl;
    cout <<"Random late const Bkg component of the diffuse He6 background: "<< bkg_late<<endl;
    //Fine tune the background subtraction
    int LowBin = hist_original->FindBin(-5);
    int HighBin = hist_original->FindBin(700);
    double Total = hist_original->Integral(LowBin,HighBin)-(HighBin-LowBin+1)*bkg_late;
    int N = hist_original->GetNbinsX();
    hist_suppression->Scale(0);
    double accumulator = 0;
    for (int i=LowBin;i<=HighBin;i++){
      hist_suppression->SetBinContent(i,1.0-accumulator/Total*(1.0-bkg_late/bkg));
      accumulator+=(hist_original->GetBinContent(i)-bkg_late);
    }
    for (int i=1;i<LowBin;i++){
      hist_suppression->SetBinContent(i,1.0);
    }
    for (int i=HighBin+1;i<=N;i++){
      hist_suppression->SetBinContent(i,bkg_late/bkg);
    }
    //Rand bkg subtraction
    for (int i=1;i<=N;i++){
      hist_subtracted->SetBinContent(i,hist_original->GetBinContent(i)-bkg*hist_suppression->GetBinContent(i));
      hist_subtracted->SetBinError(i,hist_original->GetBinError(i));
    }
    ExtBkgHistBox["All"]=hist_subtracted;
    ExtBkgHistBox["All"]->SetName("BkgAll");
    ExtBkgHistBox["All"]->SetTitle("Background All");
    ExtBkgHistBox["SuppressionFactor"]=hist_suppression;
    ExtBkgHistBox["SuppressionFactor"]->SetName("Suppression Factor");
    ExtBkgHistBox["SuppressionFactor"]->SetTitle("Suppression Factor");
    ExtBkgStatus|=1;
  }else if (Name.compare("Q_Cut_ON")==0){
    int N = hist_original->GetNbinsX();
    for (int i=1;i<=N;i++){
      int ii = ExtBkgHistBox["SuppressionFactor"]->FindBin(hist_subtracted->GetBinCenter(i));
      hist_subtracted->SetBinContent(i,hist_original->GetBinContent(i)-hist_RandBkg->GetBinContent(i)*ExtBkgHistBox["SuppressionFactor"]->GetBinContent(ii));
      hist_subtracted->SetBinError(i,sqrt(pow(hist_original->GetBinError(i),2.0)+pow(hist_RandBkg->GetBinError(i),2.0)));
    }
    ExtBkgHistBox["Q_Cut_ON"]=hist_subtracted;
    ExtBkgHistBox["Q_Cut_ON"]->SetName("BkgQOn");
    ExtBkgHistBox["Q_Cut_ON"]->SetTitle("Background Q_Cut ON");
    ExtBkgStatus|=(1<<1);
    //Other bkg hists
    ExtBkgHistBox["Scint_EA"]=hist_Scint_EA;
    ExtBkgHistBox["Scint_EA"]->SetName("Scint_EA");
    ExtBkgHistBox["Scint_EA"]->SetTitle("Scint_EA");
    ExtBkgHistBox["CosThetaENu"]=hist_CosThetaENu;
    ExtBkgHistBox["CosThetaENu"]->SetName("CosThetaENu");
    ExtBkgHistBox["CosThetaENu"]->SetTitle("CosThetaENu");
    ExtBkgHistBox["EIon1"]=hist_EIon1;
    ExtBkgHistBox["EIon1"]->SetName("EIon1");
    ExtBkgHistBox["EIon1"]->SetTitle("EIon1");
    ExtBkgHistBox["EIon2"]=hist_EIon2;
    ExtBkgHistBox["EIon2"]->SetName("EIon2");
    ExtBkgHistBox["EIon2"]->SetTitle("EIon2");
  }else if (Name.compare("QC_Cut_ON")==0){
    hist_original->Fit("pol0","","",1200,1500);
    double bkg = hist_original->GetFunction("pol0")->GetParameter(0);
    cout <<"Random Bkg component of the diffuse He6 background: "<< bkg<<endl;
    int N = hist_original->GetNbinsX();
    for (int i=1;i<=N;i++){
      hist_subtracted->SetBinContent(i,hist_original->GetBinContent(i)-bkg);
      hist_subtracted->SetBinError(i,hist_original->GetBinError(i));
    }
    ExtBkgHistBox["QC_Cut_ON"]=hist_subtracted;
    ExtBkgHistBox["QC_Cut_ON"]->SetName("BkgQCOn");
    ExtBkgHistBox["QC_Cut_ON"]->SetTitle("Background QC_Cut ON");
    ExtBkgStatus|=(1<<2);
  }else{
    cout <<"Bkg name "<<Name<<" is not identified."<<endl;
    return -1;
  }

  return 0;
}

/**********************************************************************************************/
int Analyzer::SaveBkgHistogram(int ID)
{
  ostringstream convert;
  convert << setfill('0') << setw(2)<<ID;

  string filename = HISTOGRAM_DIRECTORY + "/ExtBkgHistograms_";
  filename += convert.str();
  filename += ".root";

  TFile* OutFile = new TFile(filename.c_str(),"recreate");

  if ((ExtBkgStatus&(1))!=0){
    ExtBkgHistBox["All"]->Write();
    ExtBkgHistBox["SuppressionFactor"]->Write();
  }
  if ((ExtBkgStatus&(1<<1))!=0){
    ExtBkgHistBox["Q_Cut_ON"]->Write();
    ExtBkgHistBox["Scint_EA"]->Write();
    ExtBkgHistBox["CosThetaENu"]->Write();
    ExtBkgHistBox["EIon1"]->Write();
    ExtBkgHistBox["EIon2"]->Write();
  }
  if ((ExtBkgStatus&(1<<2))!=0){
    ExtBkgHistBox["QC_Cut_ON"]->Write();
  }

  OutFile->Close();
  return 0;
}

/**********************************************************************************************/
int Analyzer::LoadBkgHistogram(int ID)
{
  ostringstream convert;
  convert << setfill('0') << setw(2)<<ID;

  string  filename = HISTOGRAM_DIRECTORY + "/ExtBkgHistograms_";
  filename += convert.str();
  filename += ".root";

  TFile* InFile = new TFile(filename.c_str(),"read");
  if (InFile->IsZombie()){
    cout<<"Fail when opening file " << filename.c_str()<<"!\n";
    return -1;
  }
  TH1 * h = NULL;
  gROOT->cd();

  h = (TH1D*)InFile->Get("BkgAll");
  if (h!=NULL){
    ExtBkgHistBox["All"] = (TH1*)h->Clone();
    ExtBkgStatus|=1;
    cout  << "Found BkgAll."<<endl;
  }
  h = (TH1D*)InFile->Get("BkgQOn");
  if (h!=NULL){
    ExtBkgHistBox["Q_Cut_ON"] = (TH1*)h->Clone();
    ExtBkgStatus|=(1<<1);
    cout  << "Found BkgQOn."<<endl;
  }
  h = (TH1D*)InFile->Get("BkgQCOn");
  if (h!=NULL){
    ExtBkgHistBox["QC_Cut_ON"] = (TH1*)h->Clone();
    ExtBkgStatus|=(1<<2);
    cout  << "Found BkgQCOn."<<endl;
  }
  h = (TH1D*)InFile->Get("Scint_EA");
  if (h!=NULL){
    ExtBkgHistBox["Scint_EA"] = (TH1*)h->Clone();
//    ExtBkgStatus|=(1<<1);
    cout  << "Found Scint_EA."<<endl;
  }
  h = (TH1D*)InFile->Get("CosThetaENu");
  if (h!=NULL){
    ExtBkgHistBox["CosThetaENu"] = (TH1*)h->Clone();
//    ExtBkgStatus|=(1<<1);
    cout  << "Found CosThetaENu."<<endl;
  }
  h = (TH1D*)InFile->Get("EIon1");
  if (h!=NULL){
    ExtBkgHistBox["EIon1"] = (TH1*)h->Clone();
//    ExtBkgStatus|=(1<<1);
    cout  << "Found EIon1."<<endl;
  }
  h = (TH1D*)InFile->Get("EIon2");
  if (h!=NULL){
    ExtBkgHistBox["EIon2"] = (TH1*)h->Clone();
//    ExtBkgStatus|=(1<<1);
    cout  << "Found EIon2."<<endl;
  }

  InFile->Close();
  return 0;
}

/**********************************************************************************************/
int Analyzer::DelBkgHistogram()
{
  if (ExtBkgHistBox["All"]!=NULL)delete ExtBkgHistBox["All"];
  if (ExtBkgHistBox["Q_Cut_ON"]!=NULL)delete ExtBkgHistBox["Q_Cut_ON"];
  if (ExtBkgHistBox["QC_Cut_ON"]!=NULL)delete ExtBkgHistBox["QC_Cut_ON"];
  if (ExtBkgHistBox["Scint_EA"]!=NULL)delete ExtBkgHistBox["Scint_EA"];
  if (ExtBkgHistBox["CosThetaENu"]!=NULL)delete ExtBkgHistBox["CosThetaENu"];
  if (ExtBkgHistBox["EIon1"]!=NULL)delete ExtBkgHistBox["EIon1"];
  if (ExtBkgHistBox["EIon2"]!=NULL)delete ExtBkgHistBox["EIon2"];
  return 0;
}

/**********************************************************************************************/
int Analyzer::NormalizeBkg(string label)
{
  //Hist and BkgHist need to have the same bin settings
  TH1* hist = (TH1*)(ExpHistBox["TOFHist"].Hist1DList[0][0]->Clone());
  TH1* histbkg = (TH1*)(ExtBkgHistBox["All"]->Clone());
  if (ExtBkgHistBox["All"]==NULL){
    cout << "Background histogram \"ALL\" not loaded. "<<endl;
    return -1;
  }
  double RangeLow = 10.0;
  double RangeHigh = 110.0;
  int LowBin = hist->FindBin(RangeLow);
  int HighBin = hist->FindBin(RangeHigh);
  double Bin500 = hist->FindBin(500.0);
  double Bin140 = hist->FindBin(140.0);
  double Bin320 = hist->FindBin(320.0);

  double Norm = hist->Integral(LowBin,HighBin);
  double NormBkg = histbkg->Integral(LowBin,HighBin);

  histbkg->Scale(Norm/NormBkg);
  TCanvas* c1 = new TCanvas("c1","c1",0,0,800,600);
//  hist->Rebin(10);
//  histbkg->Rebin(10);
  hist->Draw();
  histbkg->SetLineColor(kRed);
  histbkg->Draw("same");

  string filename = HISTOGRAM_DIRECTORY + "/BkgComparison_" + label + ".root";
  TFile* outfile = new TFile(filename.c_str(),"recreate");
  c1->Write();
  outfile->Close();

  double Uncertainty = Norm/NormBkg*sqrt(1/Norm+1/NormBkg);
  cout << "Bkg Normalization factor: "<< Norm/NormBkg << endl;
  cout << "Uncertainty of the Bkg Normalization factor "<<Uncertainty<<endl;
  cout << "Signal to background: "<< hist->Integral(Bin140,Bin320)/histbkg->Integral(Bin140,Bin320)-1.0<<endl;

  //Update the analyzer parameter for bkg subtraction
  BkgNorm = Norm/NormBkg;

  return 0;
}

/**********************************************************************************************/
int Analyzer::SetBkgNorm(double Norm)
{
  BkgNorm = Norm;
  return 0;
}

/**********************************************************************************************/
int Analyzer::ExpBkgSubtraction(string label)
{
  if (!ExpHistBox["TOFHist"].Enabled){
    cout<<"Error: TOF Hist not enabled. Cannot perform background subtraction. ExpBkgSubtraction()."<<endl;
    return -1;
  
  }
  //Hist and BkgHist need to have the same bin settings
  TH1* hist = (TH1*)(ExpHistBox["TOFHist"].Hist1DList[0][0]->Clone());
  TH1* histbkg = (TH1*)(ExtBkgHistBox["Q_Cut_ON"]->Clone());
  if (ExtBkgHistBox["Q_Cut_ON"]==NULL){
    cout << "Background histogram \"Q_Cut_ON\" not loaded. "<<endl;
    return -1;
  }
  int N = histbkg->GetNbinsX();
  for (int i=1;i<=N;i++){
    histbkg->SetBinContent(i,histbkg->GetBinContent(i)*BkgNorm);
    histbkg->SetBinError(i,histbkg->GetBinError(i)*BkgNorm);
  }
  //Subtraction
  for (int i=1;i<=N;i++){
    ExpHistBox["TOFHist"].Hist1DList[0][0]->SetBinContent(i,ExpHistBox["TOFHist"].Hist1DList[0][0]->GetBinContent(i)-histbkg->GetBinContent(i));
    ExpHistBox["TOFHist"].Hist1DList[0][0]->SetBinError(i,sqrt(pow(ExpHistBox["TOFHist"].Hist1DList[0][0]->GetBinError(i),2.0)+pow(histbkg->GetBinError(i),2.0)));
  }

  //Signal to background calculation
  double Bin140 = histbkg->FindBin(140.0);
  double Bin320 = histbkg->FindBin(320.0);
  
  double NormBkg = histbkg->Integral(Bin140,Bin320);
   
  Bin140 = ExpHistBox["TOFHist"].Hist1DList[0][0]->FindBin(140.0);
  Bin320 = ExpHistBox["TOFHist"].Hist1DList[0][0]->FindBin(320.0); 

  double NormSig = ExpHistBox["TOFHist"].Hist1DList[0][0]->Integral(Bin140,Bin320);
  cout << "Signal to Background: "<< NormSig/NormBkg << endl;

  //Bkg Subtraction for other histograms

  TCanvas* c1 = new TCanvas("c1","c1",0,0,800,600);
//  hist->Rebin(10);
//  histbkg->Rebin(10);
  hist->SetLineColor(kBlack);
  hist->Draw();
  histbkg->SetLineColor(kRed);
  histbkg->Draw("same");
  ExpHistBox["TOFHist"].Hist1DList[0][0]->SetLineColor(kBlue);
  ExpHistBox["TOFHist"].Hist1DList[0][0]->Draw("same");

  string filename = HISTOGRAM_DIRECTORY + "/BkgSubtraction_" + label + ".root";
  TFile* outfile = new TFile(filename.c_str(),"recreate");
  c1->Write();
  outfile->Close();

  return 0;
}

/**********************************************************************************************/
int Analyzer::SimBkgMixing(string label)
{
  //Hist and BkgHist need to have the same bin settings
  TH1* hist = (TH1*)(SimHistBox["TOFHist"].Hist1DList[0][0]->Clone());
  TH1* histbkg = (TH1*)(ExtBkgHistBox["Q_Cut_ON"]->Clone());
  if (ExtBkgHistBox["Q_Cut_ON"]==NULL){
    cout << "Background histogram \"Q_Cut_ON\" not loaded. "<<endl;
    return -1;
  }
  int N = histbkg->GetNbinsX();
  for (int i=1;i<=N;i++){
    histbkg->SetBinContent(i,histbkg->GetBinContent(i)*BkgNorm);
    histbkg->SetBinError(i,histbkg->GetBinError(i)*BkgNorm);
  }
  //Mixing
  for (int i=1;i<=N;i++){
    SimHistBox["TOFHist"].Hist1DList[0][0]->SetBinContent(i,SimHistBox["TOFHist"].Hist1DList[0][0]->GetBinContent(i)+histbkg->GetBinContent(i));
    SimHistBox["TOFHist"].Hist1DList[0][0]->SetBinError(i,sqrt(pow(SimHistBox["TOFHist"].Hist1DList[0][0]->GetBinError(i),2.0)+pow(histbkg->GetBinError(i),2.0)));
  }

  //Signal to background calculation
  double NormSig = hist->Integral();
  double NormBkg = histbkg->Integral();

  cout << "Signal to Background: "<< NormSig/NormBkg << endl;

  TCanvas* c1 = new TCanvas("c1","c1",0,0,800,600);
//  hist->Rebin(10);
//  histbkg->Rebin(10);
  hist->SetLineColor(kBlack);
  hist->Draw();
  histbkg->SetLineColor(kRed);
  histbkg->Draw("same");
  SimHistBox["TOFHist"].Hist1DList[0][0]->SetLineColor(kBlue);
  SimHistBox["TOFHist"].Hist1DList[0][0]->Draw("same");

  string filename = HISTOGRAM_DIRECTORY + "/BkgMixing_" + label + ".root";
  TFile* outfile = new TFile(filename.c_str(),"recreate");
  c1->Write();
  outfile->Close();

  return 0;
}

/**********************************************************************************************/
