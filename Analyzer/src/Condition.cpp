//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>
#include <stdint.h>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
#include "TRandom3.h"
//Analyzer includes
#include "Analyzer.h"
#include "GlobalConstants.h"

using namespace std;

/***********************************************************************************/
/*********************Condition and file list Processing****************************/
/***********************************************************************************/
/*********Define allowed conditions**************/
void Analyzer::SetAllowedConditions(){
  //Define allowed Conditions and link the names to variables
  AllowedConditions["ScintThreshold"] = &ScintThreshold;
  AllowedConditions["ScintUpperbound"] = &ScintUpperbound;
  AllowedConditions["ScintMidpoint"] = &ScintMidpoint;
  AllowedConditions["MWPCThreshold"] = &MWPCThreshold;
  AllowedConditions["MWPC_R"] = &MWPC_R;
  AllowedConditions["MWPC_RIn"] = &MWPC_RIn;
  AllowedConditions["MWPC_X"] = &MWPC_X;
  AllowedConditions["MWPC_Y"] = &MWPC_Y;
  AllowedConditions["MCP_R"] = &MCP_R;
  AllowedConditions["MCP_RIn"] = &MCP_RIn;
  AllowedConditions["MCP_ThetaLow"] = &MCP_ThetaLow;
  AllowedConditions["MCP_ThetaHigh"] = &MCP_ThetaHigh;
  AllowedConditions["MCP_X"] = &MCP_X;
  AllowedConditions["MCP_Y"] = &MCP_Y;
  AllowedConditions["MCPEff_Scale"] = &MCPEff_Scale;
  AllowedConditions["ScattCut"] = &ScattCut;
  AllowedConditions["LeadingEdgeRange_Low"] = &LeadingEdgeRange_Low;
  AllowedConditions["LeadingEdgeRange_High"] = &LeadingEdgeRange_High;
  AllowedConditions["TOFLow"] = &TOFLow;
  AllowedConditions["TOFHigh"] = &TOFHigh;
  AllowedConditions["TOFMidpoint"] = &TOFMidpoint;
  AllowedConditions["QValueLow"] = &QValueLow;
  AllowedConditions["QValueHigh"] = &QValueHigh;
  AllowedConditions["ZDecLow"] = &ZDecLow;
  AllowedConditions["ZDecHigh"] = &ZDecHigh;
}

/***********List allowed conditions**************/
int Analyzer::LsConditions()
{
  if(Purpose==1){
    return LsExpConditions();
  }
  map <string,double * >::iterator it;
  for (it=AllowedConditions.begin();it!=AllowedConditions.end();it++){
    cout << it->first<<endl;
  }
  return 0;
}
/***********List enabled conditions**************/
int Analyzer::LsEnabledCondFunc()
{
  if(Purpose==1){
    return LsEnabledExpCondFunc();
  }
  map <string,bool (Analyzer::*)(SimRecDataStruct)>::iterator it;
  cout<<"\nList of Enabled NonLoop Conditions:"<<endl;
  for (it=NonLoopFunctions.begin();it!=NonLoopFunctions.end();it++){
    cout << it->first<<endl;
  }
  cout<<"\nList of Enabled Loop Conditions:"<<endl;
  for (it=LoopFunctions.begin();it!=LoopFunctions.end();it++){
    cout << it->first<<endl;
  }
  cout<<"\n";
  return 0;
}
/*********Define allowed conditions**************/
int Analyzer::SetDefaultConditions(){
/*  if(Purpose==1){
    return SetDefaultExpConditions();
  }*/
  InitCondition("ScintThreshold",1,500.0,0);
  InitCondition("ScintUpperbound",1,4000.0,0);
  InitCondition("ScintMidpoint",1,1500.0,0);
  InitCondition("MWPCThreshold",1,0.5,0);
  InitCondition("MWPC_R",1,14.0,0);
  InitCondition("MWPC_RIn",1,0.0,0);
  InitCondition("MWPC_X",1,0.0,0);
  InitCondition("MWPC_Y",1,0.0,0);
  InitCondition("MCP_R",1,35.0,0);
  InitCondition("MCP_RIn",1,0.0,0);
  InitCondition("MCP_ThetaLow",1,-PI,0);
  InitCondition("MCP_ThetaHigh",1,PI,0);
  InitCondition("MCP_X",1,0.0,0);
  InitCondition("MCP_Y",1,0.0,0);
  InitCondition("MCPEff_Scale",1,1.0,0);
  InitCondition("ScattCut",1,0,0);
  InitCondition("LeadingEdgeRange_Low",1,192,0);
  InitCondition("LeadingEdgeRange_High",1,196,0);
  InitCondition("TOFLow",1,75.0,0);
  InitCondition("TOFHigh",1,475.0,0);
  InitCondition("TOFMidpoint",1,100.0,0);
  InitCondition("QValueLow",1,2977.0,0);
  InitCondition("QValueHigh",1,4043.0,0);
  InitCondition("ZDecLow",1,-100,0);
  InitCondition("ZDecHigh",1,100.0,0);
  UnSetCondLoopPtrs();
  return 0;
}
/*********Fill ForbiddenCondFunctions by default**************/
void Analyzer::InitDefaultSimForbiddenCondFunctions()
{
  SimForbiddenCondFunctions.clear();
  SimForbiddenCondFunctions.push_back("Cond_ScintE");
  SimForbiddenCondFunctions.push_back("Cond_MWPCE");
  //SimForbiddenCondFunctions.push_back("Cond_MWPCT");
  SimForbiddenCondFunctions.push_back("Cond_MWPCArea");
  SimForbiddenCondFunctions.push_back("Cond_MCPArea");
  //SimForbiddenCondFunctions.push_back("Cond_MCPT1T2");
  SimForbiddenCondFunctions.push_back("Cond_TOFRange");
  SimForbiddenCondFunctions.push_back("Cond_TOFScintE2D");
  SimForbiddenCondFunctions.push_back("Cond_QValue");
  SimForbiddenCondFunctions.push_back("Cond_QValue_C");
  SimForbiddenCondFunctions.push_back("Cond_ZDec");
  
  SimForbiddenCondFunctions.push_back("Cond_Scatter");
  SimForbiddenCondFunctions.push_back("Cond_MCPEff");

}
/********Loop Pointers for conditions**************/
int Analyzer::AddCondLoopPtr(int index, string Name)
{
/*  if(Purpose==1){
    return AddExpCondLoopPtr(index,Name);
  }*/
  if (SysWConditions.find(Name)==SysWConditions.end()){
    cout << "Condition " << Name << " is not setup yet!\n";
    return -1;
  }
  if (index==1){
    LinkedPtr newPtr;
    newPtr.array_ptr = &(SysWConditions[Name]);
    newPtr.linked_ptr = AllowedConditions[Name];
    CondLoopPtrs1.push_back(newPtr);
  }else if (index==2){
    LinkedPtr newPtr;
    newPtr.array_ptr = &(SysWConditions[Name]);
    newPtr.linked_ptr = AllowedConditions[Name];
    CondLoopPtrs2.push_back(newPtr);
  }else{
    cout << "Only 1 or 2!\n";
    return -1;
  }
  //Set Loop Functions
  if (Name.compare("ScintThreshold")==0)
    SetCondFuncMap("Cond_ScintE",true,true);
  else if (Name.compare("ScintUpperbound")==0)
    SetCondFuncMap("Cond_ScintE",true,true);
  else if (Name.compare("MWPCThreshold")==0)
    SetCondFuncMap("Cond_MWPCE",true,true);
  else if (Name.compare("MWPC_R")==0)
    SetCondFuncMap("Cond_MWPCArea",true,true);
  else if (Name.compare("MWPC_RIn")==0)
    SetCondFuncMap("Cond_MWPCArea",true,true);
  else if (Name.compare("MWPC_X")==0)
    SetCondFuncMap("Cond_MWPCArea",true,true);
  else if (Name.compare("MWPC_Y")==0)
    SetCondFuncMap("Cond_MWPCArea",true,true);
  else if (Name.compare("MCP_R")==0)
    SetCondFuncMap("Cond_MCPArea",true,true);
  else if (Name.compare("MCP_RIn")==0)
    SetCondFuncMap("Cond_MCPArea",true,true);
  else if (Name.compare("MCP_ThetaLow")==0)
    SetCondFuncMap("Cond_MCPArea",true,true);
  else if (Name.compare("MCP_ThetaHigh")==0)
    SetCondFuncMap("Cond_MCPArea",true,true);
  else if (Name.compare("MCP_X")==0)
    SetCondFuncMap("Cond_MCPArea",true,true);
  else if (Name.compare("MCP_Y")==0)
    SetCondFuncMap("Cond_MCPArea",true,true);
  else if (Name.compare("MCPEff_Scale")==0)
    SetCondFuncMap("Cond_MCPEff",true,true);
  else if (Name.compare("ScattCut")==0)
    SetCondFuncMap("Cond_Scatter",true,true);
  else if (Name.compare("TOFLow")==0)
    SetCondFuncMap("Cond_TOFRange",true,true);
  else if (Name.compare("TOFHigh")==0)
    SetCondFuncMap("Cond_TOFRange",true,true);
  else if (Name.compare("QValueLow")==0)
    SetCondFuncMap("Cond_QValue",true,true);
  else if (Name.compare("QValueHigh")==0)
    SetCondFuncMap("Cond_QValue",true,true);
  else if (Name.compare("ZDecLow")==0)
    SetCondFuncMap("Cond_ZDec",true,true);
  else if (Name.compare("ZDecHigh")==0)
    SetCondFuncMap("Cond_ZDec",true,true);


  return 0;
}

int Analyzer::UnSetCondLoopPtrs()
{
/*  if(Purpose==1){
    return UnSetExpCondLoopPtrs(); 
  }*/
  CondLoopPtrs1.clear();
  CondLoopPtrs2.clear();
  nCond1 = 1;
  nCond2 = 1;

  InitCondFuncMap();
  
  //Turn unnecessary conditions off
/*  if(Purpose2 ==1){
    SetCondFuncMap("Cond_ScintE",false);
    SetCondFuncMap("Cond_MWPCE",false);
    SetCondFuncMap("Cond_MWPCArea",false);
    SetCondFuncMap("Cond_Scatter",false);
  }*/
  return 0;
}

/*************Function list operations******************/
int Analyzer::SetCondFuncMap(string Name,bool On, bool Loop)
{
//  //Check if it is forbidden
//  if (On){
//    int n = SimForbiddenCondFunctions.size();
//    for (int i=0;i<n;i++){
//      if (SimForbiddenCondFunctions[i].compare(Name)==0){
//				NonLoopFunctions.erase(Name);
//				LoopFunctions.erase(Name);
//			//	cout << "Function "<<Name<<" is forbidden. It cannot be turned on."<<endl;
//				return 1;
//      }
//    }
//  }
  bool (Analyzer::*CondFunPtr)(SimRecDataStruct);

  if (Name.compare("Cond_ScintE")==0){
    CondFunPtr = &Analyzer::Cond_ScintE;
  }else if (Name.compare("Cond_MWPCE")==0){
    CondFunPtr = &Analyzer::Cond_MWPCE;
  }else if (Name.compare("Cond_MWPCArea")==0){
    CondFunPtr = &Analyzer::Cond_MWPCArea;
  }else if (Name.compare("Cond_MCPArea")==0){
    CondFunPtr = &Analyzer::Cond_MCPArea;
  }else if (Name.compare("Cond_Scatter")==0){
    CondFunPtr = &Analyzer::Cond_Scatter;
  }else if (Name.compare("Cond_MCPEff")==0){
    CondFunPtr = &Analyzer::Cond_MCPEff;
  }else if (Name.compare("Cond_TOFRange")==0){
    CondFunPtr = &Analyzer::Cond_TOFRange;
  }else if (Name.compare("Cond_TOFScintE2D")==0){
    CondFunPtr = &Analyzer::Cond_TOFScintE2D;
  }else if (Name.compare("Cond_QValue")==0){
    CondFunPtr = &Analyzer::Cond_QValue;
  }else if (Name.compare("Cond_QValue_C")==0){
    CondFunPtr = &Analyzer::Cond_QValue_C;
  }else if (Name.compare("Cond_ZDec")==0){
    CondFunPtr = &Analyzer::Cond_ZDec;
  }else{
    cout << "The Function " << Name << " is not defined!\n";
    return -1;
  }
  if (!On){
    NonLoopFunctions.erase(Name);
    LoopFunctions.erase(Name);
    return 0;
  }
  if (Loop){
    LoopFunctions[Name] = CondFunPtr;
    NonLoopFunctions.erase(Name);
  }else{
    NonLoopFunctions[Name] = CondFunPtr;
    LoopFunctions.erase(Name);
  }
  return 0;
}

int Analyzer::InitCondFuncMap()
{
  NonLoopFunctions.clear();
  LoopFunctions.clear();
  SetCondFuncMap("Cond_ScintE",true);
  SetCondFuncMap("Cond_MWPCE",true);
  SetCondFuncMap("Cond_MWPCArea",true);
  SetCondFuncMap("Cond_MCPArea",true);
  SetCondFuncMap("Cond_Scatter",true);
  SetCondFuncMap("Cond_MCPEff",true);
  SetCondFuncMap("Cond_TOFRange",true);
  SetCondFuncMap("Cond_TOFScintE2D",false); //disable triangle cut
  SetCondFuncMap("Cond_QValue",true);
  SetCondFuncMap("Cond_QValue_C",false);
  SetCondFuncMap("Cond_ZDec",true);
  int n = SimForbiddenCondFunctions.size();
  for (int i=0;i<n;i++){
    SetCondFuncMap(SimForbiddenCondFunctions[i],false);
  }
  return 0;
}
/***************Reset Systematic Maps******************************************/

int Analyzer::ResetSystematics()
{
  if(!SysWConditions.empty())SysWConditions.clear();
  if(!SysWFiles.empty())SysWFiles.clear();
  SetDefaultConditions();
  InitCondFuncMap();
  nCond1=nCond2=1;
  nFileSim1=nFileSim2=1;
  return 0;
}

/*************Initialize Condition settings*************/
int Analyzer::InitCondition(string Name,int N, double low, double interval)
{
/*  if(Purpose==1){
    return InitExpCondition(Name,N,low,interval); 
  }*/
  //check name
  if (AllowedConditions.find(Name)==AllowedConditions.end()){
    cout << "Unidentified purpose "<<Name<<endl;
    return -1;
  }

  if (SysWConditions.find(Name)!=SysWConditions.end())SysWConditions[Name].clear();
  for (int i=0;i<N;i++){
    SysWConditions[Name].push_back(low+i*interval);
  }
  *(AllowedConditions[Name]) = SysWConditions[Name][0];
  return 0;
}

/***********************Set Condition********************/
int Analyzer::SetCondition(string Name,int i,double val)
{
  if(Purpose==1){
    return SetExpCondition(Name,i,val); 
  }
  if (SysWConditions.find(Name)==SysWConditions.end()){
    cout << "Condition " << Name << " is not initialized or allowed!\n";
    return -1;
  }
  if (i>=SysWConditions[Name].size()){
    cout << "Current Condition " << Name << " does not have long enough list.\n";
    cout << "Please initialize this condition list with longer length and try again.\n";
    return -1;
  }
  SysWConditions[Name][i] = val;
  return 0;
}

/*************Check Condition settings*************/
int Analyzer::CheckConditionSettings()
{
/*  if(Purpose==1){
    return CheckExpConditionSettings(); 
  }*/
  map <string,double *>::iterator it; 
  map <string,vector<double> >::iterator it2;
  for (it = AllowedConditions.begin();it != AllowedConditions.end();++it){
    it2=SysWConditions.find(it->first);
    if(it2==SysWConditions.end()){
      cout << "Condition "<< it->first <<" is not initialized yet!\n";
      return 1;
    }
    if((it2->second).size()==0){
      cout << "Condition "<< it->first <<" list is empty!\n";
	return 1;
    }
    //Update all current conditions
    *(it->second) = it2->second[0];
  }
  //check loop pointers
  if (CondLoopPtrs1.size()==0)
    nCond1 = 1;
  else
    nCond1 = (*CondLoopPtrs1[0].array_ptr).size();
  if (CondLoopPtrs2.size()==0)
    nCond2 = 1;
  else
    nCond2 = (*CondLoopPtrs2[0].array_ptr).size();

  vector <LinkedPtr>::iterator it3;
  for (it3 = CondLoopPtrs1.begin();it3 != CondLoopPtrs1.end();++it3){
      if ((*(it3->array_ptr)).size() != (CondLoopPtrs1[0].array_ptr)->size()){
	cout << "Linked Conditions don't have same dimenstions!\n";
	return 1;
      }
  }
  for (it3 = CondLoopPtrs2.begin();it3 != CondLoopPtrs2.end();++it3){
      if ((*(it3->array_ptr)).size() != (CondLoopPtrs2[0].array_ptr)->size()){
	cout << "Linked Conditions don't have same dimenstions!\n";
	return 1;
      }
  }
   if(FileCondLnkd){
  	if (nFileSim1 != (CondLoopPtrs1[0].array_ptr)->size()){
			cout << "File dimensions don't equal Linked Conditions dimensions!\n";
			return 1;
  	}
  }
  return 0;
}

/**********Process Conditions*************/
void Analyzer::ProcessConditions(SimRecDataStruct SimRecData, int iFile)
{
  bool returnval1 = true;
  bool returnval2 = true;
  //Process non-looped condition functions
  map <string,bool (Analyzer::*)(SimRecDataStruct)>::iterator it;
  for (it = NonLoopFunctions.begin();it != NonLoopFunctions.end();++it){
    returnval1 = returnval1 && (this->*(it->second))(SimRecData);
  }
  if (!returnval1 || KillEvent){
    for (int i=0;i<nCond1;i++){
      for (int j=0;j<nCond2;j++){
	Conditions[i][j] = false;
      }
    }
    return;
  }
  //Process looped condition functions
  if (LoopFunctions.size()==0){	//If there is not looped functions, return value from the non-looped functions
    for (int i=0;i<nCond1;i++){
      for (int j=0;j<nCond2;j++){
	Conditions[i][j] = returnval1;
      }
    }
    return;
  }
  if(FileCondLnkd){
  	int i = iFile;
  	//Update current condition pointers
    vector <LinkedPtr>::iterator it2;
    for (it2 = CondLoopPtrs1.begin();it2 != CondLoopPtrs1.end();++it2){
			*(it2->linked_ptr) = (*(it2->array_ptr))[i];
    }
		//Invoke function
		for (it = LoopFunctions.begin();it != LoopFunctions.end();++it){
				returnval2 = returnval1 && (this->*(it->second))(SimRecData);
				if (!returnval2) break;
		}
		Conditions[0][0] = returnval2;
  
  }
  else{
		for (int i=0;i<nCond1;i++){
		  for (int j=0;j<nCond2;j++){
		    //Update current condition pointers
		    vector <LinkedPtr>::iterator it2;
		    for (it2 = CondLoopPtrs1.begin();it2 != CondLoopPtrs1.end();++it2){
		*(it2->linked_ptr) = (*(it2->array_ptr))[i];
		    }
		    for (it2 = CondLoopPtrs2.begin();it2 != CondLoopPtrs2.end();++it2){
		*(it2->linked_ptr) = (*(it2->array_ptr))[j];
		    }
		    //Invoke function
		    for (it = LoopFunctions.begin();it != LoopFunctions.end();++it){
		returnval2 = returnval1 && (this->*(it->second))(SimRecData);
		if (!returnval2)
			break;
		    }
		    Conditions[i][j] = returnval2;
		  }
		}
	}
  //Return all loop iterators to 0 position
  for (int k=0;k<CondLoopPtrs1.size();k++){
    *CondLoopPtrs1[k].linked_ptr = (*CondLoopPtrs1[k].array_ptr)[0];
  }
  for (int k=0;k<CondLoopPtrs2.size();k++){
    *CondLoopPtrs2[k].linked_ptr = (*CondLoopPtrs2[k].array_ptr)[0];
  }
}

int Analyzer::SetFileCondLnk(bool status){
	FileCondLnkd = status;
	return 0;
}

int Analyzer::SetCharge3Switch(bool status){
	Charge3Switch = status;
	return 0;
}
/******Condition Test Functions*************/
bool Analyzer::Cond_ScintE(SimRecDataStruct SimRecData)
{
  if (SimRecData.ScintEnergy>=ScintThreshold && SimRecData.ScintEnergy<=ScintUpperbound) return true;
  else return false;
}

bool Analyzer::Cond_MWPCE(SimRecDataStruct SimRecData)
{
  if ((SimRecData.MWPCEnergy)>=MWPCThreshold) return true;
  else return false;
}

bool Analyzer::Cond_MWPCArea(SimRecDataStruct SimRecData)
{
  double R;
  double Theta;
  R = sqrt(pow(SimRecData.MWPCHitPos[0]-MWPC_X,2.0) + pow(SimRecData.MWPCHitPos[1]-MWPC_Y,2.0));
  Theta = asin((SimRecData.MWPCHitPos[1]-MWPC_Y)/R);
  if (SimRecData.MWPCHitPos[0]<0 && Theta>0) Theta = PI - Theta;
  if (SimRecData.MWPCHitPos[0]<0 && Theta<0) Theta = -PI - Theta;
  if (SimRecData.MWPCHitPos[0]<0 && Theta==0) Theta = -PI;
  if (R <= MWPC_R && R >= MWPC_RIn) return true;
  else return false;
}

bool Analyzer::Cond_MCPArea(SimRecDataStruct SimRecData)
{
  //Check IonStatus First
  if (SimRecData.IonStatus!=0)return false;
  double R;
  double Theta;
  R = sqrt(pow(SimRecData.HitPos[0]-MCP_X,2.0) + pow(SimRecData.HitPos[1]-MCP_Y,2.0));
  Theta = asin((SimRecData.HitPos[1]-MCP_Y)/R);
  if (SimRecData.HitPos[0]-MCP_X<0 && Theta>0) Theta = PI - Theta;
  if (SimRecData.HitPos[0]-MCP_X<0 && Theta<0) Theta = -PI - Theta;
  if (SimRecData.HitPos[0]-MCP_X<0 && Theta==0) Theta = -PI;
  if (R <= MCP_R && R >= MCP_RIn && Theta <=MCP_ThetaHigh && Theta >= MCP_ThetaLow) return true;
  else return false;
}

bool Analyzer::Cond_Scatter(SimRecDataStruct SimRecData)
{
  uint8_t Opt = uint8_t(ScattCut);
  if (ScattMode.compare("Reject")==0){
    if ((Opt&SimRecData.Hits)==0)return true;
    else return false;
  }else if (ScattMode.compare("Select")==0){
    if ((Opt&SimRecData.Hits)==0)return false;
    else return true;
  }
  cout << "Unkown ScattMode "<<ScattMode<<endl;
  return false;
  /*
  if (int(ScattCut)==0 || (int(ScattCut)==1 && (InData.Hits>>3)==0) || (int(ScattCut)==2 && (InData.Hits>>3)!=0) || (int(ScattCut)==3 && (InData.Hits>>7)==0) || (int(ScattCut)==4 && (InData.Hits>>7)!=0) ){
    return true;
  }else{
    return  false;
  }*/
}

bool Analyzer::Cond_MCPEff(SimRecDataStruct SimRecData)
{
  //Static Random generator
/*  static TRandom3 Rand_MCP_Eff(0);
  //MCP MAP cell position
  int x_cell, y_cell;
  x_cell = (int)floor((SimRecData.HitPos[0]/10.0)+4.0);
  y_cell = (int)floor((SimRecData.HitPos[1]/10.0)+4.0);

  if(x_cell<0 || x_cell >7 || y_cell<0 || y_cell>7){
    x_cell=y_cell=4;
  }

  double Weight = 1.0-MCPefficiencyMap[x_cell][y_cell]*(double)MCPefficiencyMapSwitch*MCPEff_Scale;
  double aux = Rand_MCP_Eff.Rndm();
  if (aux > Weight)
    return false;
*/  return true;
}

bool Analyzer::Cond_TOFRange(SimRecDataStruct SimRecData)
{  
  if (SimRecData.TOF>TOFLow && SimRecData.TOF<TOFHigh) return true;
  else return false;
}

bool Analyzer::Cond_TOFScintE2D(SimRecDataStruct SimRecData)
{  
  //The TOF limits and ScintE limits are imposed by other functions
  //Only need to apply the linear cut
  if (SimRecData.TOF<TOFMidpoint){
    if (SimRecData.ScintEnergy<=(ScintUpperbound+(ScintMidpoint-ScintUpperbound)/(TOFMidpoint-TOFLow)*(SimRecData.TOF-TOFLow)))return true;
    else return false;
  }else{
    if (SimRecData.ScintEnergy<=(ScintUpperbound+(ScintThreshold-ScintUpperbound)/(TOFHigh-TOFMidpoint)*(SimRecData.TOF-TOFMidpoint)))return true;
    else return false;
  }
}

bool Analyzer::Cond_QValue(SimRecDataStruct SimRecData)
{
  if ((SimRecData.Q_Value[0]>QValueLow && SimRecData.Q_Value[0]<QValueHigh) || (SimRecData.Q_Value[1]>QValueLow && SimRecData.Q_Value[1]<QValueHigh) || (Charge3Switch&&((SimRecData.Q_Value[2]>QValueLow && SimRecData.Q_Value[2]<QValueHigh)))) return true;
//  if ((SimRecData.Q_Value[0]>QValueLow && SimRecData.Q_Value[0]<QValueHigh) || (SimRecData.Q_Value[1]>QValueLow && SimRecData.Q_Value[1]<QValueHigh)) return true;
  else return false;
}

bool Analyzer::Cond_QValue_C(SimRecDataStruct SimRecData)
{
    if ((SimRecData.Q_Value[0]>QValueLow && SimRecData.Q_Value[0]<QValueHigh) || (SimRecData.Q_Value[1]>QValueLow && SimRecData.Q_Value[1]<QValueHigh) || (Charge3Switch&&((SimRecData.Q_Value[2]>QValueLow && SimRecData.Q_Value[2]<QValueHigh)))) return true;
  //if ((SimRecData.Q_Value[0]>QValueLow && SimRecData.Q_Value[0]<QValueHigh) || (SimRecData.Q_Value[1]>QValueLow && SimRecData.Q_Value[1]<QValueHigh)) return false;
  else return true;
}


bool Analyzer::Cond_ZDec(SimRecDataStruct SimRecData)
{
  if (SimRecData.DECPos[2]>ZDecLow && SimRecData.DECPos[2]<ZDecHigh) return true;
  else return false;
}
/****END**Condition Test Functions*************/

/*************************File List Operations**************************************/
/***********************************************************************************/
int Analyzer::SetFileSimDim(int dim1,int dim2)
{
  if (dim1>N_OF_HIST || dim2>N_OF_HIST){
    cout << "File dimensions cannot be larger than N_OF_HIST." << endl;
    return -1;
  } 
  nFileSim1 = dim1;
  nFileSim2 = dim2;
  return 0; 
}
/***********************************************************************************/
int Analyzer::SetFileIDEntrySim(int i,int j,int ID1, int ID2, int ID3, int ID4)
{
  FileIDListSim[i][j] = ID1*1000000 + ID2*10000 + ID3*100 + ID4;
  return 0;
}
/***********************************************************************************/
int Analyzer::SetFileIDListSim(int j,int ID1, int ID2, int ID3, int ID4, string Inc)
{
  for (int i=0;i<nFileSim1;i++){
    FileIDListSim[i][j] = ID1*1000000 + ID2*10000 + ID3*100 + ID4;
    if (Inc.compare("Beta")==0) ID2++;
    else if(Inc.compare("Ion")==0) ID3++;
    else if(Inc.compare("Post")==0) ID4++;
    else if(Inc.compare("Generator")==0) ID1++;
    else{
      cout << "Unkown ID name "<< Inc <<endl;
      return -1;
    }
  }
  return 0;
}
/***********************************************************************************/
int Analyzer::SetFileIDListSim2D(string Inc)
{
  int IDInc = 1;
  if (Inc.compare("Beta")==0) IDInc=10000;
  else if(Inc.compare("Ion")==0) IDInc=100;
  else if(Inc.compare("Post")==0) IDInc=1;
  else if(Inc.compare("Generator")==0) IDInc=1000000;
  else{
    cout << "Unkown ID name "<< Inc <<endl;
    return -1;
  }
  for (int i=0;i<nFileSim1;i++){
    for (int j=1;j<nFileSim2;j++){
      FileIDListSim[i][j] = FileIDListSim[i][0] + IDInc*j;
    }
  }
  return 0;
}
/***********************************************************************************/
int Analyzer::InitSysWFile(string Name,int N, double low, double interval)
{
  if (SysWFiles.find(Name)!=SysWFiles.end())SysWFiles[Name].clear();
  for (int i=0;i<N;i++){
    SysWFiles[Name].push_back(low+i*interval);
  }
  return 0;
}
/***********************************************************************************/
int Analyzer::SetSysWFile(string Name,int i,double val)
{
  if (SysWFiles.find(Name)==SysWFiles.end()){
    cout << "Condition " << Name << "is not initialized or allowed!\n";
    return -1;
  }
  if (i>=SysWFiles[Name].size()){
    cout << "Current Condition " << Name << " does not have long enough list.\n";
    cout << "Please initialize this condition list with longer length and try again.\n";
    return -1;
  }
  SysWFiles[Name][i] = val;
  return 0;
}

/***********************************************************************************/
int Analyzer::InitSliceBoundary(int N,double begin,double interval)
{
  if (N!=nSlice && (Std_First_Slice.size()!=0 || Std_Second_Slice.size()!=0)){
    cout<<"Warning: Input number of slices conflicts with the current one and the Std histograms are loaded.\n";
    cout<<"Using the old value instead of the input one.\n";
    N = nSlice;
  }
  for (int i=0;i<=N;i++){
    SliceBoundary.push_back(begin+i*interval);
  }
  nSlice = N;
  return 0;
}

/***********************************************************************************/
int Analyzer::LogSimFileIDList()
{
  char buffer[100];
  AnalysisLog<<"Simulation FileID List:"<<endl;
  for (int j=0;j<nFileSim2;j++){
    for (int i=0;i<nFileSim1;i++){
      sprintf(buffer,"%09d",FileIDListSim[i][j]);
      AnalysisLog<<buffer<<" ";
    }
    AnalysisLog<<endl;
  }
  return 0;
}
/***********************************************************************************/
int Analyzer::LogConditionList()
{
  AnalysisLog<<"Internal condition list:"<<endl;
  map <string,vector<double> >::iterator it;
  for (it=SysWConditions.begin();it!=SysWConditions.end();it++)
  {
    AnalysisLog<<it->first<<"; "<<(it->second).size()<<" conditions: ";
    for (int i=0;i<(it->second).size();i++){
      AnalysisLog<<it->second[i]<<" ";
    }
    AnalysisLog<<endl;
  }
  AnalysisLog<< CondLoopPtrs1.size()<<" ConditionPtr1 :";
  for (int i=0;i<CondLoopPtrs1.size();i++){
    map <string,double *>::iterator it2;
    for (it2=AllowedConditions.begin();it2!=AllowedConditions.end();it2++){
      if (it2->second == CondLoopPtrs1[i].linked_ptr) AnalysisLog<<it2->first<<" ";
    }
  }
  AnalysisLog<<endl;
  AnalysisLog<< CondLoopPtrs2.size()<<" ConditionPtr2 :";
  for (int i=0;i<CondLoopPtrs2.size();i++){
    map <string,double *>::iterator it2;
    for (it2=AllowedConditions.begin();it2!=AllowedConditions.end();it2++){
      if (it2->second == CondLoopPtrs2[i].linked_ptr) AnalysisLog<<it2->first<<" ";
    }
  }
  AnalysisLog<<endl;
  AnalysisLog<<"File related condition list:"<<endl;
  for (it=SysWFiles.begin();it!=SysWFiles.end();it++)
  {
    AnalysisLog<<it->first<<"; "<<(it->second).size()<<" conditions: ";
    for (int i=0;i<(it->second).size();i++){
      AnalysisLog<<it->second[i]<<" ";
    }
    AnalysisLog<<endl;
  }
  /******************Enabled Conditions***********************/  
  map <string,bool (Analyzer::*)(SimRecDataStruct)>::iterator it2;
  cout<<"\nList of Enabled NonLoop Conditions:"<<endl;
  for (it2=NonLoopFunctions.begin();it2!=NonLoopFunctions.end();it2++){
    AnalysisLog << it2->first<<endl;
  }
  cout<<"\nList of Enabled Loop Conditions:"<<endl;
  for (it2=LoopFunctions.begin();it2!=LoopFunctions.end();it2++){
    AnalysisLog << it2->first<<endl;
  }
  AnalysisLog<<"\n\n";
  return 0;
}












