#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>

//Analyzer includes
#include "T0Correction.h"
#include "Cell.h"
//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TGraphErrors.h"

using namespace std;

T0Correction::T0Correction(const char* filename, int i, int j){
  char* Direct;
  Direct=getenv("HE6_EXPERIMENT_DATA_DIRECTORY");
  if (Direct==NULL){
    cout << "The environment variable HE6_EXPERIMENT_DATA_DIRECTORY is not defined! Set it up first!\n";
    exit(1);
  }
  OutTreesDirect = string(Direct) + "/../OutputTrees/Exp/";
  CalDirect = string(Direct) + "/../CalData/";
  //string fname = DIRECT + "/MinExampleData/OutTrees_He6_Diffuse_06172016_IonTOFCut_Triple.root";
  
  string fname = OutTreesDirect + string(filename) + ".root"; 
  fin = new TFile(fname.c_str(),"update");
  if(fin->IsZombie()){
    cout<< "Error opening file "<< fname<<"."<<endl;
    exit(1);
  } 
  string tname = "EventTree_Cond" + to_string(i) + "," + to_string(j);
  OutTrees = NULL;
  OutTrees = (TTree*) fin->Get(tname.c_str());
  if(OutTrees==NULL){
    cout<< "Could not find tree "<<tname<<" in file "<<fname<<"!"<<endl;
    exit(1);
  }
}
T0Correction::~T0Correction(){
  delete fin;
}
int T0Correction::GenerateQMap(double delX, double delY, double MCP_R, int QCalID){
  /*if (!CurrentAnalyzer->OutTreesInitialized || CurrentAnalyzer->ExpStatus!=2){
    cout<<"ERROR: Output trees are not initialized AND/OR loaded. Check that ProcessInputExp() has run successfully and that macro option SaveRootTrees is set to true."<<endl;
    return -1;
  }else if(CurrentAnalyzer->ReadMode.compare("Triple")!=0 && CurrentAnalyzer->ReadMode.compare("Double")!=0 && CurrentAnalyzer->ReadMode.compare("MCP")!=0){
    cout<<"ERROR: GenerateQMap can only be run in Triple, Double, or MCP modes."<<endl;
    return -1;  
  }
  else{*/
    cout<<"Generating Q Map with "<<delX<< " by "<<delY<<" mm cells bounded by MCP radius of "<<MCP_R<<" mm..."<<endl;
 // }
  
  Q_Cell qcell(delX, delY, MCP_R); //Initialize Q cell instance
  
  int n = qcell.GetNCells();
  double Qmean,Qerr;
  TCut xyCellCut;
  TH1D* h_qcell=NULL;

  //Find Qmean of each cell
  for(int i=0;i<n;i++){
    xyCellCut = qcell.GetXYCut(i,"MCPHitPosAA.X", "MCPHitPosAA.Y");
    OutTrees->Draw("QMCP>>h_qcell_internal(100,0,220e3)", xyCellCut); //h_qcell_internal belongs to OutTrees[0][0] and will be destroyed upon closing
    h_qcell = (TH1D*) gDirectory->Get("h_qcell_internal");//brings into memory, still belongs to OutTrees directory
    Qmean = h_qcell->GetMean();
    Qerr = h_qcell->GetMeanError();
    qcell.SetPoint(i,Qmean,Qerr);
    delete h_qcell;
  }
 // Save QMap to CalData
  ostringstream convert;
  convert << setfill('0') << setw(4)<<QCalID;
  string fName = CalDirect + "MCPQMap" + convert.str() + ".root";
  string fNameTxt = CalDirect + "MCPQMap" + convert.str() + ".txt";

  qcell.SaveToFile(fName.c_str());
  qcell.OutputToTextFile(fNameTxt.c_str());

}
int T0Correction::GenerateT0CorrectionMap(int QCalID, int T0CalID, double t_lo, double t_hi){
  ostringstream convert;
  convert << setfill('0') << setw(4)<<QCalID;
  string fName = CalDirect + "MCPQMap" + convert.str() + ".root";
  
  ostringstream convert2;
  convert2 << setfill('0') << setw(4)<<T0CalID;
  string fName2 = CalDirect + "T0CorrMap" + convert2.str() + ".root"; 
  string fName2Txt = CalDirect + "T0CorrMap" + convert2.str() + ".txt"; 
  

  cout<<"Generating T0Correction Map "<<fName2<<" from QMap "<<fName<<endl;

  T0_Cell tcell(fName.c_str());//Initialize T0 cell instance from QMap file
  int ind = tcell.GetIndex(0,0);//Get index of center cell
  tcell.SetRelT0Index(ind); //Set everything relative to center cell
  Q_Cell qcell(fName.c_str());//Initialize Q cell instance from file (load MCP QMap) 

  int n = qcell.GetNCells();
  int nq = Qlo.size();
  double Qmean,T0mean,T0err,T0std;
  TCut xyCellCut,QCellCut,QCut;
  TH1D* h_tcell = NULL;
  TH1D* h = NULL;
  TF1* t0Fit = NULL;
  TGraphErrors* g_tcell = NULL;
 
// double Qfact = .5;
  double Qfact = 0;//no position dependent cut on MCP Charge

  TFile* f2 = new TFile(fName2.c_str(),"recreate");
  f2->mkdir("T0Cell_HistFits/");
  f2->mkdir("T0Cell_QCuts/");
  f2->mkdir("QCutT0HistFits/");
  t0Fit = new TF1("t0fit","gaus",-5,5);
  for(int i=0;i<n;i++){
    f2->cd("T0Cell_HistFits/"); 
    
    Qmean=T0mean=T0err=0;
    ostringstream stm,hstm;
    string dumstr;
    
    xyCellCut = qcell.GetXYCut(i,"MCPHitPosAA.X", "MCPHitPosAA.Y");
    Qmean = qcell.GetQMean(i);
    stm <<"QMCP>"<<Qfact*Qmean;
    dumstr = stm.str();
    QCellCut=dumstr.c_str();
    //cout<<QCellCut<<endl;
    //cout<<xyCellCut<<endl;
    //Create histogram with cuts and change ownership to file 
    OutTrees->Draw("TOFAA>>h_tcell_internal(100,-10,10)", xyCellCut+QCellCut); //h_tcell_internal belongs to OutTrees[0][0] and will be destroyed upon closing
    h_tcell = (TH1D*) gDirectory->Get("h_tcell_internal");//brings into memory, still belongs to OutTrees directory
    h_tcell -> SetDirectory(f2);
    
    //Fit T0

    //Check for empty cells
    if(h_tcell->GetEntries()==0){
     // cout<<"Histogram EMPTY in fit range. ABORTING FIT!"<<endl;
     // cout << "       Setting fit parameters to 0 for cell index "<<i<<endl;
    }      
    else{
      //Set initial parameters
       // t0Fit->SetParameter(0,h_tcell->GetMaximum());
       // t0Fit->SetParameter(1,h_tcell->GetMean());
       // t0Fit->SetParameter(2,h_tcell->GetStdDev());
      TFitResultPtr r = h_tcell->Fit(t0Fit,"Quiet","",t_lo,t_hi);
      Int_t fitStatus = r;
      if (r<0) {
          cout << "ERROR in fit. Check statistics in fit range. ABORTING FIT!"<<endl;
          return -1;
      }
      else if (t0Fit->GetParameter(1)>t_hi || t0Fit->GetParameter(1)<t_lo){
        cout<< "Error in fit: Check STATISTICS in fit range. ABORTING FIT!"<<endl;
        cout<<  "       Setting fit parameters to 0 for cell index "<<i<<endl;
      }
      else{
        T0mean = t0Fit->GetParameter(1);
        T0std = t0Fit->GetParameter(2);
        T0err = t0Fit->GetParError(1);
      }
    }
    tcell.SetPoint(i,T0mean,T0err);    
    //Write to file
    hstm<<"h_tcell_"<<i;
    dumstr = hstm.str();
    h_tcell->Write(dumstr.c_str(),TObject::kOverwrite);
    delete h_tcell;

    //for each cell find T0 vs Q
    f2->cd("T0Cell_QCuts/");
    g_tcell = new TGraphErrors(nq);
    for(int j=0;j<nq;j++){
      stm.str("");
      stm.clear();
      stm << "QMCP>"<<Qlo[j]<<"&QMCP<"<<Qhi[j];
      dumstr = stm.str();
      QCut=dumstr.c_str();
      //cout<<QCut<<endl;

      OutTrees->Draw("TOFAA>>h_tcell_internal(100,-10,10)", xyCellCut+QCut); //h_tcell_internal belongs to OutTrees[0][0] and will be destroyed upon closing
      h_tcell = (TH1D*) gDirectory->Get("h_tcell_internal");//brings into memory, still belongs to OutTrees directory
      h_tcell -> SetDirectory(f2);

      //Check for empty cells
      if(h_tcell->GetEntries()==0){
       // cout<<"Histogram EMPTY in fit range. ABORTING FIT!"<<endl;
       // cout << "       Setting fit parameters to 0 for cell index "<<i<<endl;
      }      
      else{
        //Set initial parameters
         // t0Fit->SetParameter(0,h_tcell->GetMaximum());
         // t0Fit->SetParameter(1,h_tcell->GetMean());
         // t0Fit->SetParameter(2,h_tcell->GetStdDev());
        TFitResultPtr r = h_tcell->Fit(t0Fit,"Quiet","",t_lo,t_hi);
        Int_t fitStatus = r;
        if (r<0) {
            cout << "ERROR in fit. Check statistics in fit range. ABORTING FIT!"<<endl;
            return -1;
        }
        else if (t0Fit->GetParameter(1)>t_hi || t0Fit->GetParameter(1)<t_lo){
          cout<< "Error in fit: Check STATISTICS in fit range. ABORTING FIT!"<<endl;
          cout<<  "       Setting fit parameters to 0 for cell index "<<i<<endl;
        }
        else{
          T0mean = t0Fit->GetParameter(1);
          T0std = t0Fit->GetParameter(2);
          T0err = t0Fit->GetParError(1);
        }
      }
      g_tcell->SetPoint(j,Qlo.at(j),T0mean);
      g_tcell->SetPointError(j,0,T0err);
      delete h_tcell;
    }
    //Write T0 v QMCP graph to file
    ostringstream gstm;
    gstm<<"gQ_tcell_"<<i;
    dumstr = gstm.str();
    g_tcell->Write(dumstr.c_str(),TObject::kOverwrite);
    delete g_tcell;         
  }//end of cell loop

 //for all cells find T0 vs Q
  f2->cd("QCutT0HistFits/");
  g_tcell = new TGraphErrors(nq);
  Qmean=T0mean=T0err=0;
  ostringstream stm,hstm;
  string dumstr;
  for(int j=0;j<nq;j++){
    stm.str("");
    stm.clear();
    stm << "QMCP>"<<Qlo[j]<<"&QMCP<"<<Qhi[j];
    dumstr = stm.str();
    QCut=dumstr.c_str();
    //cout<<QCut<<endl;

    OutTrees->Draw("TOFAA>>h_tcell_internal(100,-10,10)", QCut); //h_tcell_internal belongs to OutTrees[0][0] and will be destroyed upon closing
    h_tcell = (TH1D*) gDirectory->Get("h_tcell_internal");//brings into memory, still belongs to OutTrees directory
    h_tcell -> SetDirectory(f2);

    //Check for empty cells
    if(h_tcell->GetEntries()==0){
     // cout<<"Histogram EMPTY in fit range. ABORTING FIT!"<<endl;
     // cout << "       Setting fit parameters to 0 for cell index "<<i<<endl;
    }      
    else{
      //Set initial parameters
       // t0Fit->SetParameter(0,h_tcell->GetMaximum());
       // t0Fit->SetParameter(1,h_tcell->GetMean());
       // t0Fit->SetParameter(2,h_tcell->GetStdDev());
      TFitResultPtr r = h_tcell->Fit(t0Fit,"Quiet","",t_lo,t_hi);
      Int_t fitStatus = r;
      if (r<0) {
          cout << "ERROR in fit. Check statistics in fit range. ABORTING FIT!"<<endl;
          return -1;
      }
      else if (t0Fit->GetParameter(1)>t_hi || t0Fit->GetParameter(1)<t_lo){
        cout<< "Error in fit: Check STATISTICS in fit range. ABORTING FIT!"<<endl;
        cout<<  "       Setting fit parameters to 0 for index "<<j<<endl;
      }
      else{
        T0mean = t0Fit->GetParameter(1);
        T0std = t0Fit->GetParameter(2);
        T0err = t0Fit->GetParError(1);
      }
    }
    g_tcell->SetPoint(j,Qlo.at(j),T0mean);
    g_tcell->SetPointError(j,0,T0err);
    //Write to file
    hstm.str("");
    hstm.clear();
    hstm<<"h_tQcut_"<<j;
    dumstr = hstm.str();
    h_tcell->Write(dumstr.c_str(),TObject::kOverwrite);
    delete h_tcell;
  }
  //Write T0 v QMCP graph to file
  ostringstream gstm;
  gstm<<"gT0_v_QMCP";
  dumstr = gstm.str();
  g_tcell->Write(dumstr.c_str(),TObject::kOverwrite);
  delete g_tcell;         

  //qcell.PrintClass();
  //tcell.PrintClass(); 
  delete t0Fit;
  delete f2;

  tcell.SaveToFile(fName2.c_str());
  tcell.OutputToTextFile(fName2Txt.c_str());
  return 0;
}

int T0Correction::InitQSlices(int nq,double qlo, double qhi, double inv){
  Qlo.resize(nq);
  Qhi.resize(nq);
  for(int i=0;i<nq;i++){
    Qlo[i] = qlo + i*inv;
    Qhi[i] = qhi +i*inv;
  }
}
int T0Correction::InitQSlices(std::vector<double> qlo, std::vector<double> qhi){
  Qlo=qlo;
  Qhi=qhi;
}
