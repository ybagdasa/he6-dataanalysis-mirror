//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>
#include <stdint.h>
#include <stdexcept>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
//Analyzer includes
#include "Analyzer.h"

using namespace std;

//Globals
extern int TOF_High;
extern int TOF_Low;
extern int TOF_N;

/***********************************************************************************/
int Analyzer::InitSimHistBox()
{
  SimHistBox.clear();
  //RawScint
  SimHistBox["RawScint"].Title = "RawScint";
  SimHistBox["RawScint"].Dim = 1;
  SimHistBox["RawScint"].MultiSlice = false;
  SimHistBox["RawScint"].XTitle = "E_Scint [keV]";
  SimHistBox["RawScint"].YTitle = "Counts";
  SimHistBox["RawScint"].ZTitle = "";
  SimHistBox["RawScint"].XBinNum = 4100;
  SimHistBox["RawScint"].XRange.low = 0.0;
  SimHistBox["RawScint"].XRange.high = 4100.0;
  SimHistBox["RawScint"].YBinNum = 0;
  SimHistBox["RawScint"].YRange.low = 0.0;
  SimHistBox["RawScint"].YRange.high = 0.0;
  SimHistBox["RawScint"].Hist1DList = NULL;
  SimHistBox["RawScint"].Hist2DList = NULL;
  SimHistBox["RawScint"].Hist1DListSliced = NULL;
  SimHistBox["RawScint"].Enabled = false;
  //FillFunctions["RawScint"] = &Analyzer::FillRawScint;
  
  //Raw MWPC
  SimHistBox["RawMWPC"].Title = "RawMWPC";
  SimHistBox["RawMWPC"].Dim = 1;
  SimHistBox["RawMWPC"].MultiSlice = false;
  SimHistBox["RawMWPC"].XTitle = "E_MWPC [keV]";
  SimHistBox["RawMWPC"].YTitle = "Counts";
  SimHistBox["RawMWPC"].ZTitle = "";
  SimHistBox["RawMWPC"].XBinNum = 500;
  SimHistBox["RawMWPC"].XRange.low = 0.0;
  SimHistBox["RawMWPC"].XRange.high = 40.0;
  SimHistBox["RawMWPC"].YBinNum = 0;
  SimHistBox["RawMWPC"].YRange.low = 0.0;
  SimHistBox["RawMWPC"].YRange.high = 0.0;
  SimHistBox["RawMWPC"].Hist1DList = NULL;
  SimHistBox["RawMWPC"].Hist2DList = NULL;
  SimHistBox["RawMWPC"].Hist1DListSliced = NULL;
  SimHistBox["RawMWPC"].Enabled = false;
  //FillFunctions["RawMWPC"] = &Analyzer::FillRawMWPC;
  //CoinScint
  SimHistBox["CoinScint"].Title = "CoinScint";
  SimHistBox["CoinScint"].Dim = 1;
  SimHistBox["CoinScint"].MultiSlice = false;
  SimHistBox["CoinScint"].XTitle = "E_Scint [keV]";
  SimHistBox["CoinScint"].YTitle = "Counts";
  SimHistBox["CoinScint"].ZTitle = "";
  SimHistBox["CoinScint"].XBinNum = 400;
  SimHistBox["CoinScint"].XRange.low = 0.0;
  SimHistBox["CoinScint"].XRange.high = 8000.0;
  SimHistBox["CoinScint"].YBinNum = 0;
  SimHistBox["CoinScint"].YRange.low = 0.0;
  SimHistBox["CoinScint"].YRange.high = 0.0;
  SimHistBox["CoinScint"].Hist1DList = NULL;
  SimHistBox["CoinScint"].Hist2DList = NULL;
  SimHistBox["CoinScint"].Hist1DListSliced = NULL;
  SimHistBox["CoinScint"].Enabled = false;
  //FillFunctions["CoinScint"] = &Analyzer::FillCoinScint;
  //CoinMWPC
  SimHistBox["CoinMWPC"].Title = "CoinMWPC";
  SimHistBox["CoinMWPC"].Dim = 1;
  SimHistBox["CoinMWPC"].MultiSlice = false;
  SimHistBox["CoinMWPC"].XTitle = "E_MWPC [keV]";
  SimHistBox["CoinMWPC"].YTitle = "Counts";
  SimHistBox["CoinMWPC"].ZTitle = "";
  SimHistBox["CoinMWPC"].XBinNum = 600;
  SimHistBox["CoinMWPC"].XRange.low = 0.0;
  SimHistBox["CoinMWPC"].XRange.high = 30.0;
  SimHistBox["CoinMWPC"].YBinNum = 0;
  SimHistBox["CoinMWPC"].YRange.low = 0.0;
  SimHistBox["CoinMWPC"].YRange.high = 0.0;
  SimHistBox["CoinMWPC"].Hist1DList = NULL;
  SimHistBox["CoinMWPC"].Hist2DList = NULL;
  SimHistBox["CoinMWPC"].Hist1DListSliced = NULL;
  SimHistBox["CoinMWPC"].Enabled = false;
  //MWPCPosX
  SimHistBox["MWPCPosX"].Title = "MWPCPosX";
  SimHistBox["MWPCPosX"].Dim = 1;
  SimHistBox["MWPCPosX"].MultiSlice = false;
  SimHistBox["MWPCPosX"].XTitle = "MWPC_X [mm]";
  SimHistBox["MWPCPosX"].YTitle = "Counts";
  SimHistBox["MWPCPosX"].ZTitle = "";
  SimHistBox["MWPCPosX"].XBinNum = 200;
  SimHistBox["MWPCPosX"].XRange.low = -25.0;
  SimHistBox["MWPCPosX"].XRange.high = 25.0;
  SimHistBox["MWPCPosX"].YBinNum = 0;
  SimHistBox["MWPCPosX"].YRange.low = 0.0;
  SimHistBox["MWPCPosX"].YRange.high = 0.0;
  SimHistBox["MWPCPosX"].Hist1DList = NULL;
  SimHistBox["MWPCPosX"].Hist2DList = NULL;
  SimHistBox["MWPCPosX"].Hist1DListSliced = NULL;
  SimHistBox["MWPCPosX"].Enabled = false;
  //MWPCPosY
  SimHistBox["MWPCPosY"].Title = "MWPCPosY";
  SimHistBox["MWPCPosY"].Dim = 1;
  SimHistBox["MWPCPosY"].MultiSlice = false;
  SimHistBox["MWPCPosY"].XTitle = "MWPC_Y [mm]";
  SimHistBox["MWPCPosY"].YTitle = "Counts";
  SimHistBox["MWPCPosY"].ZTitle = "";
  SimHistBox["MWPCPosY"].XBinNum = 200;
  SimHistBox["MWPCPosY"].XRange.low = -25.0;
  SimHistBox["MWPCPosY"].XRange.high = 25.0;
  SimHistBox["MWPCPosY"].YBinNum = 0;
  SimHistBox["MWPCPosY"].YRange.low = 0.0;
  SimHistBox["MWPCPosY"].YRange.high = 0.0;
  SimHistBox["MWPCPosY"].Hist1DList = NULL;
  SimHistBox["MWPCPosY"].Hist2DList = NULL;
  SimHistBox["MWPCPosY"].Hist1DListSliced = NULL;
  SimHistBox["MWPCPosY"].Enabled = false;
  //MWPC_Vs_Scint
  SimHistBox["MWPC_vs_Scint"].Title = "MWPC_vs_Scint";
  SimHistBox["MWPC_vs_Scint"].Dim = 2;
  SimHistBox["MWPC_vs_Scint"].MultiSlice = false;
  SimHistBox["MWPC_vs_Scint"].XTitle = "E [keV]";
  SimHistBox["MWPC_vs_Scint"].YTitle = "E [keV]";
  SimHistBox["MWPC_vs_Scint"].ZTitle = "Counts";
  SimHistBox["MWPC_vs_Scint"].XBinNum = 500;
  SimHistBox["MWPC_vs_Scint"].XRange.low = 0.0;
  SimHistBox["MWPC_vs_Scint"].XRange.high = 18.0;
  SimHistBox["MWPC_vs_Scint"].YBinNum = 4100;
  SimHistBox["MWPC_vs_Scint"].YRange.low = 0.0;
  SimHistBox["MWPC_vs_Scint"].YRange.high = 4100.0;
  SimHistBox["MWPC_vs_Scint"].Hist1DList = NULL;
  SimHistBox["MWPC_vs_Scint"].Hist2DList = NULL;
  SimHistBox["MWPC_vs_Scint"].Hist1DListSliced = NULL;
  SimHistBox["MWPC_vs_Scint"].Enabled = false;
  //FillFunctions["MWPC_vs_Scint"] = &Analyzer::FillMWPC_vs_Scint;
  //MWPC_Image
  SimHistBox["MWPC_Image"].Title = "MWPC_Image";
  SimHistBox["MWPC_Image"].Dim = 2;
  SimHistBox["MWPC_Image"].MultiSlice = false;
  SimHistBox["MWPC_Image"].XTitle = "X [mm]";
  SimHistBox["MWPC_Image"].YTitle = "Y [mm]";
  SimHistBox["MWPC_Image"].ZTitle = "Counts";
  SimHistBox["MWPC_Image"].XBinNum = 200;
  SimHistBox["MWPC_Image"].XRange.low = -25.0;
  SimHistBox["MWPC_Image"].XRange.high = 25.0;
  SimHistBox["MWPC_Image"].YBinNum = 200;
  SimHistBox["MWPC_Image"].YRange.low = -25.0;
  SimHistBox["MWPC_Image"].YRange.high = 25.0;
  SimHistBox["MWPC_Image"].Hist1DList = NULL;
  SimHistBox["MWPC_Image"].Hist2DList = NULL;
  SimHistBox["MWPC_Image"].Hist1DListSliced = NULL;
  SimHistBox["MWPC_Image"].Enabled = false;
  //FillFunctions["MWPC_Image"] = &Analyzer::FillMWPC_Image;
  //MWPC_Cathode-Anode-YCorrelation
  SimHistBox["MWPC_CAY"].Title = "MWPC_CAY";
  SimHistBox["MWPC_CAY"].Dim = 2;
  SimHistBox["MWPC_CAY"].MultiSlice = false;
  SimHistBox["MWPC_CAY"].XTitle = "CY [mm]";
  SimHistBox["MWPC_CAY"].YTitle = "AY [mm]";
  SimHistBox["MWPC_CAY"].ZTitle = "Counts";
  SimHistBox["MWPC_CAY"].XBinNum = 200;
  SimHistBox["MWPC_CAY"].XRange.low = -25.0;
  SimHistBox["MWPC_CAY"].XRange.high = 25.0;
  SimHistBox["MWPC_CAY"].YBinNum = 200;
  SimHistBox["MWPC_CAY"].YRange.low = -25.0;
  SimHistBox["MWPC_CAY"].YRange.high = 25.0;
  SimHistBox["MWPC_CAY"].Hist1DList = NULL;
  SimHistBox["MWPC_CAY"].Hist2DList = NULL;
  SimHistBox["MWPC_CAY"].Hist1DListSliced = NULL;
  SimHistBox["MWPC_CAY"].Enabled = false;
  //MCP_Image
  SimHistBox["MCP_Image"].Title = "MCP_Image";
  SimHistBox["MCP_Image"].Dim = 2;
  SimHistBox["MCP_Image"].MultiSlice = false;
  SimHistBox["MCP_Image"].XTitle = "X [mm]";
  SimHistBox["MCP_Image"].YTitle = "Y [mm]";
  SimHistBox["MCP_Image"].ZTitle = "Counts";
  SimHistBox["MCP_Image"].XBinNum = 400;
  SimHistBox["MCP_Image"].XRange.low = -40.0;
  SimHistBox["MCP_Image"].XRange.high = 40.0;
  SimHistBox["MCP_Image"].YBinNum = 400;
  SimHistBox["MCP_Image"].YRange.low = -40.0;
  SimHistBox["MCP_Image"].YRange.high = 40.0;
  SimHistBox["MCP_Image"].Hist1DList = NULL;
  SimHistBox["MCP_Image"].Hist2DList = NULL;
  SimHistBox["MCP_Image"].Hist1DListSliced = NULL;
  SimHistBox["MCP_Image"].Enabled = false;
  //FillFunctions["MCP_Image"] = &Analyzer::FillMCP_Image;
  //MCP_R
  SimHistBox["MCP_R"].Title = "MCP_R";
  SimHistBox["MCP_R"].Dim = 1;
  SimHistBox["MCP_R"].MultiSlice = false;
  SimHistBox["MCP_R"].XTitle = "R [mm]";
  SimHistBox["MCP_R"].YTitle = "Counts";
  SimHistBox["MCP_R"].ZTitle = "";
  SimHistBox["MCP_R"].XBinNum = 400;
  SimHistBox["MCP_R"].XRange.low = 0.0;
  SimHistBox["MCP_R"].XRange.high = 40.0;
  SimHistBox["MCP_R"].YBinNum = 0;
  SimHistBox["MCP_R"].YRange.low = 0.0;
  SimHistBox["MCP_R"].YRange.high = 0.0;
  SimHistBox["MCP_R"].Hist1DList = NULL;
  SimHistBox["MCP_R"].Hist2DList = NULL;
  SimHistBox["MCP_R"].Hist1DListSliced = NULL;
  SimHistBox["MCP_R"].Enabled = false;
  //FillFunctions["MCP_R"] = &Analyzer::FillMCP_R;
  //TOFHist
  SimHistBox["TOFHist"].Title = "TOFHist";
  SimHistBox["TOFHist"].Dim = 1;
  SimHistBox["TOFHist"].MultiSlice = false;
  SimHistBox["TOFHist"].XTitle = "TOF [ns]";
  SimHistBox["TOFHist"].YTitle = "Counts";
  SimHistBox["TOFHist"].ZTitle = "";
  SimHistBox["TOFHist"].XBinNum = TOF_N;
  SimHistBox["TOFHist"].XRange.low = TOF_Low;
  SimHistBox["TOFHist"].XRange.high = TOF_High;
  SimHistBox["TOFHist"].YBinNum = 0;
  SimHistBox["TOFHist"].YRange.low = 0.0;
  SimHistBox["TOFHist"].YRange.high = 0.0;
  SimHistBox["TOFHist"].Hist1DList = NULL;
  SimHistBox["TOFHist"].Hist2DList = NULL;
  SimHistBox["TOFHist"].Hist1DListSliced = NULL;
  SimHistBox["TOFHist"].Enabled = false;
  //FillFunctions["TOFHist"] = &Analyzer::FillTOFHist;
  //TOFvsScint
  SimHistBox["TOF_vs_Scint"].Title = "TOF_vs_Scint";
  SimHistBox["TOF_vs_Scint"].Dim = 2;
  SimHistBox["TOF_vs_Scint"].MultiSlice = false;
  SimHistBox["TOF_vs_Scint"].XTitle = "TOF [ns]";
  SimHistBox["TOF_vs_Scint"].YTitle = "E_Scint [keV]";
  SimHistBox["TOF_vs_Scint"].ZTitle = "Counts";
  SimHistBox["TOF_vs_Scint"].XBinNum = TOF_N;
  SimHistBox["TOF_vs_Scint"].XRange.low = TOF_Low;
  SimHistBox["TOF_vs_Scint"].XRange.high = TOF_High;
  SimHistBox["TOF_vs_Scint"].YBinNum = 200;
  SimHistBox["TOF_vs_Scint"].YRange.low = 0.0;
  SimHistBox["TOF_vs_Scint"].YRange.high = 4000.0;
  SimHistBox["TOF_vs_Scint"].Hist1DList = NULL;
  SimHistBox["TOF_vs_Scint"].Hist2DList = NULL;
  SimHistBox["TOF_vs_Scint"].Hist1DListSliced = NULL;
  SimHistBox["TOF_vs_Scint"].Enabled = false;
  //FillFunctions["TOF_vs_Scint"] = &Analyzer::FillTOF_vs_Scint;
  //ExitAngleHist
  SimHistBox["ExitAngleHist"].Title = "ExitAngleHist";
  SimHistBox["ExitAngleHist"].Dim = 1;
  SimHistBox["ExitAngleHist"].MultiSlice = false;
  SimHistBox["ExitAngleHist"].XTitle = "Angle";
  SimHistBox["ExitAngleHist"].YTitle = "Counts";
  SimHistBox["ExitAngleHist"].ZTitle = "";
  SimHistBox["ExitAngleHist"].XBinNum = 180;
  SimHistBox["ExitAngleHist"].XRange.low = 0.0;
  SimHistBox["ExitAngleHist"].XRange.high = 180.0;
  SimHistBox["ExitAngleHist"].YBinNum = 0;
  SimHistBox["ExitAngleHist"].YRange.low = 0.0;
  SimHistBox["ExitAngleHist"].YRange.high = 0.0;
  SimHistBox["ExitAngleHist"].Hist1DList = NULL;
  SimHistBox["ExitAngleHist"].Hist2DList = NULL;
  SimHistBox["ExitAngleHist"].Hist1DListSliced = NULL;
  SimHistBox["ExitAngleHist"].Enabled = false;
  //FillFunctions["ExitAngleHist"] = &Analyzer::FillExitAngleHist;
  //Q value for charge 1 and 2
  SimHistBox["Q_Values"].Title = "Q_Values";
  SimHistBox["Q_Values"].Dim = 2;
  SimHistBox["Q_Values"].MultiSlice = false;
  SimHistBox["Q_Values"].XTitle = "Q1 [keV]";
  SimHistBox["Q_Values"].YTitle = "Q2 [keV]";
  SimHistBox["Q_Values"].ZTitle = "Counts";
  SimHistBox["Q_Values"].XBinNum = 500;
  SimHistBox["Q_Values"].XRange.low = 0.0;
  SimHistBox["Q_Values"].XRange.high = 12000;
  SimHistBox["Q_Values"].YBinNum = 500;
  SimHistBox["Q_Values"].YRange.low = 0.0;
  SimHistBox["Q_Values"].YRange.high = 12000;
  SimHistBox["Q_Values"].Hist1DList = NULL;
  SimHistBox["Q_Values"].Hist2DList = NULL;
  SimHistBox["Q_Values"].Hist1DListSliced = NULL;
  SimHistBox["Q_Values"].Enabled = false;

  //E-Nu Correlation
  SimHistBox["CosThetaENu"].Title = "E-Nu Correlation";
  SimHistBox["CosThetaENu"].Dim = 1;
  SimHistBox["CosThetaENu"].MultiSlice = false;
  SimHistBox["CosThetaENu"].XTitle = "CosThetaENu";
  SimHistBox["CosThetaENu"].YTitle = "Counts";
  SimHistBox["CosThetaENu"].ZTitle = "";
  SimHistBox["CosThetaENu"].XBinNum = 400;
  SimHistBox["CosThetaENu"].XRange.low = -1.0;
  SimHistBox["CosThetaENu"].XRange.high = 1.0;
  SimHistBox["CosThetaENu"].YBinNum = 0;
  SimHistBox["CosThetaENu"].YRange.low = 0.0;
  SimHistBox["CosThetaENu"].YRange.high = 0.0;
  SimHistBox["CosThetaENu"].Hist1DList = NULL;
  SimHistBox["CosThetaENu"].Hist2DList = NULL;
  SimHistBox["CosThetaENu"].Hist1DListSliced = NULL;
  SimHistBox["CosThetaENu"].Enabled = false;

  //Decay Positions
  SimHistBox["DECPos"].Title = "DECPos";
  SimHistBox["DECPos"].Dim = 2;
  SimHistBox["DECPos"].MultiSlice = false;
  SimHistBox["DECPos"].XTitle = "X [mm]";
  SimHistBox["DECPos"].YTitle = "Z [mm]";
  SimHistBox["DECPos"].ZTitle = "Counts";
  SimHistBox["DECPos"].XBinNum = 500;
  SimHistBox["DECPos"].XRange.low = -100.0;
  SimHistBox["DECPos"].XRange.high = 100.0;
  SimHistBox["DECPos"].YBinNum = 500;
  SimHistBox["DECPos"].YRange.low = -100.0;
  SimHistBox["DECPos"].YRange.high = 100.0;
  SimHistBox["DECPos"].Hist1DList = NULL;
  SimHistBox["DECPos"].Hist2DList = NULL;
  SimHistBox["DECPos"].Hist1DListSliced = NULL;
  SimHistBox["DECPos"].Enabled = false;

  //Ion Final Energy
  SimHistBox["EkIon"].Title = "Ion energy";
  SimHistBox["EkIon"].Dim = 1;
  SimHistBox["EkIon"].MultiSlice = false;
  SimHistBox["EkIon"].XTitle = "E_Ion [keV]";
  SimHistBox["EkIon"].YTitle = "Counts";
  SimHistBox["EkIon"].ZTitle = "";
  SimHistBox["EkIon"].XBinNum = 400;
  SimHistBox["EkIon"].XRange.low = 0;
  SimHistBox["EkIon"].XRange.high = 30;
  SimHistBox["EkIon"].YBinNum = 0;
  SimHistBox["EkIon"].YRange.low = 0.0;
  SimHistBox["EkIon"].YRange.high = 0.0;
  SimHistBox["EkIon"].Hist1DList = NULL;
  SimHistBox["EkIon"].Hist2DList = NULL;
  SimHistBox["EkIon"].Hist1DListSliced = NULL;
  SimHistBox["EkIon"].Enabled = false;

  //Ion Initial Energy
  SimHistBox["EIon0"].Title = "Ion initial energy";
  SimHistBox["EIon0"].Dim = 1;
  SimHistBox["EIon0"].MultiSlice = false;
  SimHistBox["EIon0"].XTitle = "E_Ion [keV]";
  SimHistBox["EIon0"].YTitle = "Counts";
  SimHistBox["EIon0"].ZTitle = "";
  SimHistBox["EIon0"].XBinNum = 400;
  SimHistBox["EIon0"].XRange.low = 0;
  SimHistBox["EIon0"].XRange.high = 1.6; 
  SimHistBox["EIon0"].YBinNum = 0;
  SimHistBox["EIon0"].YRange.low = 0.0;
  SimHistBox["EIon0"].YRange.high = 0.0;
  SimHistBox["EIon0"].Hist1DList = NULL;
  SimHistBox["EIon0"].Hist2DList = NULL;
  SimHistBox["EIon0"].Hist1DListSliced = NULL;
  SimHistBox["EIon0"].Enabled = false;

  SimHistBox["EIon1"].Title = "Ion initial energy";
  SimHistBox["EIon1"].Dim = 1;
  SimHistBox["EIon1"].MultiSlice = false;
  SimHistBox["EIon1"].XTitle = "E_Ion [keV]";
  SimHistBox["EIon1"].YTitle = "Counts";
  SimHistBox["EIon1"].ZTitle = "";
  SimHistBox["EIon1"].XBinNum = 400;
  SimHistBox["EIon1"].XRange.low = 0;
  SimHistBox["EIon1"].XRange.high = 1.6;
  SimHistBox["EIon1"].YBinNum = 0;
  SimHistBox["EIon1"].YRange.low = 0.0;
  SimHistBox["EIon1"].YRange.high = 0.0;
  SimHistBox["EIon1"].Hist1DList = NULL;
  SimHistBox["EIon1"].Hist2DList = NULL;
  SimHistBox["EIon1"].Hist1DListSliced = NULL;
  SimHistBox["EIon1"].Enabled = false;

  SimHistBox["EIon2"].Title = "Ion initial energy";
  SimHistBox["EIon2"].Dim = 1;
  SimHistBox["EIon2"].MultiSlice = false;
  SimHistBox["EIon2"].XTitle = "E_Ion [keV]";
  SimHistBox["EIon2"].YTitle = "Counts";
  SimHistBox["EIon2"].ZTitle = "";
  SimHistBox["EIon2"].XBinNum = 400;
  SimHistBox["EIon2"].XRange.low = 0;
  SimHistBox["EIon2"].XRange.high = 1.6;
  SimHistBox["EIon2"].YBinNum = 0;
  SimHistBox["EIon2"].YRange.low = 0.0;
  SimHistBox["EIon2"].YRange.high = 0.0;
  SimHistBox["EIon2"].Hist1DList = NULL;
  SimHistBox["EIon2"].Hist2DList = NULL;
  SimHistBox["EIon2"].Hist1DListSliced = NULL;
  SimHistBox["EIon2"].Enabled = false;

  //Ion Hit Angle
  SimHistBox["HitAngle"].Title = "Ion initial energy";
  SimHistBox["HitAngle"].Dim = 1;
  SimHistBox["HitAngle"].MultiSlice = false;
  SimHistBox["HitAngle"].XTitle = "HitAngle [deg]";
  SimHistBox["HitAngle"].YTitle = "Counts";
  SimHistBox["HitAngle"].ZTitle = "";
  SimHistBox["HitAngle"].XBinNum = 400;
  SimHistBox["HitAngle"].XRange.low = 0;
  SimHistBox["HitAngle"].XRange.high = 90.0;
  SimHistBox["HitAngle"].YBinNum = 0;
  SimHistBox["HitAngle"].YRange.low = 0.0;
  SimHistBox["HitAngle"].YRange.high = 0.0;
  SimHistBox["HitAngle"].Hist1DList = NULL;
  SimHistBox["HitAngle"].Hist2DList = NULL;
  SimHistBox["HitAngle"].Hist1DListSliced = NULL;
  SimHistBox["HitAngle"].Enabled = false;

  //MWPC auxilliary
  //Position Shifts
  SimHistBox["MWPCHitShift"].Title = "MWPCHitShift";
  SimHistBox["MWPCHitShift"].Dim = 2;
  SimHistBox["MWPCHitShift"].MultiSlice = false;
  SimHistBox["MWPCHitShift"].XTitle = "X_Shift [mm]";
  SimHistBox["MWPCHitShift"].YTitle = "Y_Shift [mm]";
  SimHistBox["MWPCHitShift"].ZTitle = "Counts";
  SimHistBox["MWPCHitShift"].XBinNum = 500;
  SimHistBox["MWPCHitShift"].XRange.low = -5.0;
  SimHistBox["MWPCHitShift"].XRange.high = 5.0;
  SimHistBox["MWPCHitShift"].YBinNum = 500;
  SimHistBox["MWPCHitShift"].YRange.low = -5.0;
  SimHistBox["MWPCHitShift"].YRange.high = 5.0;
  SimHistBox["MWPCHitShift"].Hist1DList = NULL;
  SimHistBox["MWPCHitShift"].Hist2DList = NULL;
  SimHistBox["MWPCHitShift"].Hist1DListSliced = NULL;
  SimHistBox["MWPCHitShift"].Enabled = false;

  //Angle Shifts
  SimHistBox["COSBetaShiftAngle"].Title = "COSBetaShiftAngle";
  SimHistBox["COSBetaShiftAngle"].Dim = 1;
  SimHistBox["COSBetaShiftAngle"].MultiSlice = false;
  SimHistBox["COSBetaShiftAngle"].XTitle = "COSBetaShiftAngle";
  SimHistBox["COSBetaShiftAngle"].YTitle = "Counts";
  SimHistBox["COSBetaShiftAngle"].ZTitle = "";
  SimHistBox["COSBetaShiftAngle"].XBinNum = 100;
  SimHistBox["COSBetaShiftAngle"].XRange.low = -1.0;
  SimHistBox["COSBetaShiftAngle"].XRange.high = 1.0;
  SimHistBox["COSBetaShiftAngle"].YBinNum = 0;
  SimHistBox["COSBetaShiftAngle"].YRange.low = 0.0;
  SimHistBox["COSBetaShiftAngle"].YRange.high = 0.0;
  SimHistBox["COSBetaShiftAngle"].Hist1DList = NULL;
  SimHistBox["COSBetaShiftAngle"].Hist2DList = NULL;
  SimHistBox["COSBetaShiftAngle"].Hist1DListSliced = NULL;
  SimHistBox["COSBetaShiftAngle"].Enabled = false;

  return 0;
}
/***********Enable/Disable HistUnits**************/
int Analyzer::EnableHist(string Name){
  if (Name.compare("All")==0) EnableDisableAll(true); //set Enabled to true
  else{
    try {
      if (Purpose==0)(SimHistBox.at(Name)).Enabled = true;
      if (Purpose==1){
				(ExpHistBox.at(Name)).Enabled = true;
				(BkgHistBox.at(Name)).Enabled = true;
				(CombExpHistBox.at(Name)).Enabled = true;
				(CombBkgHistBox.at(Name)).Enabled = true;
      }
    }
    catch (const out_of_range& oor){
      if (Purpose==0)cout<<"ERROR: SimHistBox["<<Name<<"] does not exist!"<<endl;
      if (Purpose==1)cout<<"ERROR: ExpHistBox["<<Name<<"] does not exist!"<<endl;
      cerr<<"Out of Range error: " << oor.what() << '\n';
      return -1;
    }
  }
  return 0;
}

int Analyzer::EnableBkgHist(string Name){
  if (Name.compare("All")==0) EnableDisableAllBkg(true); //set Enabled to true
  else{
    try {
      if (Purpose==0)cout<<"WARNING: Simulation does not contain Background Histograms."<<endl;
      if (Purpose==1){
				(BkgHistBox.at(Name)).Enabled = true;
				(CombBkgHistBox.at(Name)).Enabled = true;
      }
    }
    catch (const out_of_range& oor){
      if (Purpose==0)cout<<"ERROR: SimHistBox["<<Name<<"] does not exist!"<<endl;
      if (Purpose==1)cout<<"ERROR: ExpHistBox["<<Name<<"] does not exist!"<<endl;
      cerr<<"Out of Range error: " << oor.what() << '\n';
      return -1;
    }
  }
  return 0;
}

int Analyzer::DisableHist(string Name){
  if (Name.compare("All")==0) EnableDisableAll(false); //set Enabled to false
  else{
    try {
      if (Purpose==0)(SimHistBox.at(Name)).Enabled = false;
      if (Purpose==1){
	(ExpHistBox.at(Name)).Enabled = false;
	(BkgHistBox.at(Name)).Enabled = false;
	(CombExpHistBox.at(Name)).Enabled = false;
	(CombBkgHistBox.at(Name)).Enabled = false;
      }
    }
    catch (const out_of_range& oor){
      if (Purpose==0)cout<<"ERROR: SimHistBox["<<Name<<"] does not exist!"<<endl;
      if (Purpose==1)cout<<"ERROR: ExpHistBox["<<Name<<"] does not exist!"<<endl;
      cerr<<"Out of Range error: " << oor.what() << '\n';
      return -1;
    }
  }
  return 0;
}

int Analyzer::DisableBkgHist(string Name){
  if (Name.compare("All")==0) EnableDisableAllBkg(false); //set Enabled to false
  else{
    try {
      if (Purpose==0)cout<<"WARNING: Simulation does not contain Background Histograms."<<endl;
      if (Purpose==1){
				(BkgHistBox.at(Name)).Enabled = false;
				(CombBkgHistBox.at(Name)).Enabled = false;
      }
    }
    catch (const out_of_range& oor){
      if (Purpose==0)cout<<"ERROR: SimHistBox["<<Name<<"] does not exist!"<<endl;
      if (Purpose==1)cout<<"ERROR: ExpHistBox["<<Name<<"] does not exist!"<<endl;
      cerr<<"Out of Range error: " << oor.what() << '\n';
      return -1;
    }
  }
  return 0;
}

int Analyzer::EnableDisableAllBkg(bool status){
  map <string,HistUnit>::iterator it;
  if (Purpose==0){
		cout<<"WARNING: Simulation does not contain Background Histograms."<<endl;
  }
  if (Purpose==1){
    for (it=BkgHistBox.begin();it!=BkgHistBox.end();it++){
      it->second.Enabled = status;
    }
    for (it=CombBkgHistBox.begin();it!=CombBkgHistBox.end();it++){
      it->second.Enabled = status;
    }
  }
  return 0;
}	

int Analyzer::EnableDisableAll(bool status){
  map <string,HistUnit>::iterator it;
  if (Purpose==0){
    for (it=SimHistBox.begin();it!=SimHistBox.end();it++){
      it->second.Enabled = status;
    }
  }
  if (Purpose==1){
    for (it=ExpHistBox.begin();it!=ExpHistBox.end();it++){
      it->second.Enabled = status;
    }
    for (it=BkgHistBox.begin();it!=BkgHistBox.end();it++){
      it->second.Enabled = status;
    }
    for (it=CombExpHistBox.begin();it!=CombExpHistBox.end();it++){
      it->second.Enabled = status;
    }
    for (it=CombBkgHistBox.begin();it!=CombBkgHistBox.end();it++){
      it->second.Enabled = status;
    }
  }
  return 0;
}	
/***********List allowed Histograms**************/
int Analyzer::LsHist()
{ 
  if (Purpose==1){
    return LsExpHist();
  }
  map <string,HistUnit>::iterator it;
  for (it=SimHistBox.begin();it!=SimHistBox.end();it++){
    cout << it->first<< " Dimension: " << it->second.Dim<<endl;
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::InitFillFunctions(){
  if(SimHistBox["RawScint"].Enabled) FillFunctions["RawScint"] = &Analyzer::FillRawScint;
  if(SimHistBox["RawMWPC"].Enabled) FillFunctions["RawMWPC"] = &Analyzer::FillRawMWPC;
  if(SimHistBox["CoinScint"].Enabled) FillFunctions["CoinScint"] = &Analyzer::FillCoinScint;
  if(SimHistBox["CoinMWPC"].Enabled) FillFunctions["CoinMWPC"] = &Analyzer::FillCoinMWPC;
  if(SimHistBox["MWPC_Image"].Enabled) FillFunctions["MWPC_Image"] = &Analyzer::FillMWPC_Image;
  if(SimHistBox["MWPC_CAY"].Enabled) FillFunctions["MWPC_CAY"] = &Analyzer::FillMWPC_CAY;
  if(SimHistBox["MWPCPosX"].Enabled) FillFunctions["MWPCPosX"] = &Analyzer::FillMWPCPosX;
  if(SimHistBox["MWPCPosY"].Enabled) FillFunctions["MWPCPosY"] = &Analyzer::FillMWPCPosY;
  if(SimHistBox["MWPC_vs_Scint"].Enabled) FillFunctions["MWPC_vs_Scint"] = &Analyzer::FillMWPC_vs_Scint;
  if(SimHistBox["MCP_Image"].Enabled) FillFunctions["MCP_Image"] = &Analyzer::FillMCP_Image;
  if(SimHistBox["MCP_R"].Enabled) FillFunctions["MCP_R"] = &Analyzer::FillMCP_R;
  if(SimHistBox["TOFHist"].Enabled)	FillFunctions["TOFHist"] = &Analyzer::FillTOFHist;
  if(SimHistBox["TOF_vs_Scint"].Enabled) FillFunctions["TOF_vs_Scint"] = &Analyzer::FillTOF_vs_Scint;
  if(SimHistBox["ExitAngleHist"].Enabled) FillFunctions["ExitAngleHist"] = &Analyzer::FillExitAngleHist;
  if(SimHistBox["Q_Values"].Enabled) FillFunctions["Q_Values"] = &Analyzer::FillQ_Values;
  if(SimHistBox["CosThetaENu"].Enabled) FillFunctions["CosThetaENu"] = &Analyzer::FillCosThetaENu;
  if(SimHistBox["DECPos"].Enabled) FillFunctions["DECPos"] = &Analyzer::FillDECPos;
  if(SimHistBox["EkIon"].Enabled) FillFunctions["EkIon"] = &Analyzer::FillEkIon;
  if(SimHistBox["EIon0"].Enabled) FillFunctions["EIon0"] = &Analyzer::FillEIon0;
  if(SimHistBox["EIon1"].Enabled) FillFunctions["EIon1"] = &Analyzer::FillEIon1;
  if(SimHistBox["EIon2"].Enabled) FillFunctions["EIon2"] = &Analyzer::FillEIon2;
  if(SimHistBox["HitAngle"].Enabled) FillFunctions["HitAngle"] = &Analyzer::FillHitAngle;
  if(SimHistBox["MWPCHitShift"].Enabled) FillFunctions["MWPCHitShift"] = &Analyzer::FillMWPCHitShift;
  if(SimHistBox["COSBetaShiftAngle"].Enabled) FillFunctions["COSBetaShiftAngle"] = &Analyzer::FillCOSBetaShiftAngle;
  return 0;
} 
/***********************************************************************************/
void Analyzer::InitToNull(){
  //Initialize histograms
  map <string,HistUnit>::iterator it;
  for (it = SimHistBox.begin();it != SimHistBox.end();++it){
    if((it->second).Dim==1){
      (it->second).Hist1DList = new TH1**[N_OF_HIST];
      for(int i=0;i<N_OF_HIST;i++){
	(it->second).Hist1DList[i] = new TH1*[N_OF_HIST];
	for(int j=0;j<N_OF_HIST;j++){
	  (it->second).Hist1DList[i][j] = NULL;
	}
      }
      if((it->second).MultiSlice){
	(it->second).Hist1DListSliced = new TH1***[N_OF_HIST];
	for(int i=0;i<N_OF_HIST;i++){
	  (it->second).Hist1DListSliced[i] = new TH1**[N_OF_HIST];
	  for(int j=0;j<N_OF_HIST;j++){
	    (it->second).Hist1DListSliced[i][j] = new TH1*[N_OF_SLICE];
	    for(int k=0;k<N_OF_SLICE;k++){
	      (it->second).Hist1DListSliced[i][j][k] = NULL;
	    }
	  }
	}
      }
    }else if((it->second).Dim==2){
      (it->second).Hist2DList = new TH2**[N_OF_HIST];
      for(int i=0;i<N_OF_HIST;i++){
	(it->second).Hist2DList[i] = new TH2*[N_OF_HIST];
	for(int j=0;j<N_OF_HIST;j++){
	  (it->second).Hist2DList[i][j] = NULL;
	}
      }
    }
  }	

}

/***********************************************************************************/
int Analyzer::InitHistograms()
{
  if (Status!=0){
    cout << "Histograms already initialized. Delete histograms before reinitializing.\n";
    return 1;
  }
  InitToNull();
  string HistName;
  string HistTitle;
  //Initialize histograms
  map <string,HistUnit>::iterator it;
  for (it = SimHistBox.begin();it != SimHistBox.end();++it){
    if((it->second).Enabled){
      if((it->second).Dim==1){
	for(int i=0;i<HistDim1;i++){
	  for(int j=0;j<HistDim2;j++){
	    HistName = it->first;
	    HistName+=Form("_Cond%d,%d",i,j);
	    HistTitle = (it->second).Title;
	    HistTitle+=Form(": Condition%d,%d",i,j);
//	    cout <<HistName<<endl;
	    (it->second).Hist1DList[i][j] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
	    (it->second).Hist1DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	    (it->second).Hist1DList[i][j]->GetXaxis()->SetTitleFont(60);
	    (it->second).Hist1DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	    (it->second).Hist1DList[i][j]->GetYaxis()->SetTitleFont(60);
	  }
	}
	if((it->second).MultiSlice){
	  for(int i=0;i<HistDim1;i++){
	    for(int j=0;j<HistDim2;j++){
	      for(int k=0;k<N_OF_SLICE;k++){
		HistName = it->first;
		HistName+=Form("_Cond%d,%d;Slice%d",i,j,k);
		HistTitle = (it->second).Title;
		HistTitle+=Form(": Condition%d,%d,Slice%d",i,j,k);
		(it->second).Hist1DListSliced[i][j][k] = new TH1D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high);
		(it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
		(it->second).Hist1DListSliced[i][j][k]->GetXaxis()->SetTitleFont(60);
		(it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
		(it->second).Hist1DListSliced[i][j][k]->GetYaxis()->SetTitleFont(60);
	      }
	    }
	  }
	}
      }else if((it->second).Dim==2){
	for(int i=0;i<HistDim1;i++){
	  for(int j=0;j<HistDim2;j++){
	    HistName = it->first;
	    HistName+=Form("_Cond%d,%d",i,j);
	    HistTitle = (it->second).Title;
	    HistTitle+=Form(": Condition%d,%d",i,j);
	    (it->second).Hist2DList[i][j] = new TH2D(HistName.c_str(),HistTitle.c_str(),(it->second).XBinNum,(it->second).XRange.low,(it->second).XRange.high,(it->second).YBinNum,(it->second).YRange.low,(it->second).YRange.high);
	    (it->second).Hist2DList[i][j]->GetXaxis()->SetTitle((it->second).XTitle.c_str()); 
	    (it->second).Hist2DList[i][j]->GetXaxis()->SetTitleFont(60);
	    (it->second).Hist2DList[i][j]->GetYaxis()->SetTitle((it->second).YTitle.c_str()); 
	    (it->second).Hist2DList[i][j]->GetYaxis()->SetTitleFont(60);
	    (it->second).Hist2DList[i][j]->GetZaxis()->SetTitle((it->second).ZTitle.c_str()); 
	    (it->second).Hist2DList[i][j]->GetZaxis()->SetTitleFont(60);
	  }
	}
      }
      else cout<<"Error in InitHistograms()"<<endl;
    } 
  }

  Status=1;
  InitFillFunctions();
  return 0;
}

/***********************************************************************************/
int Analyzer::DelHistograms()
{
  if (Status ==0 ){
    cout <<"Already deleted!\n";
    return 0;
  }
  //delete histograms
  map <string,HistUnit>::iterator it;
  for (it = SimHistBox.begin();it != SimHistBox.end();++it){
    if((it->second).Dim==1){
      for(int i=0;i<N_OF_HIST;i++){
	      for(int j=0;j<N_OF_HIST;j++){
	        if ((it->second).Hist1DList[i][j]==NULL){break;}		  
	        else {delete (it->second).Hist1DList[i][j];} //deallocate memory for histogram 
	      }
	      delete[] (it->second).Hist1DList[i]; //deallocate memory for every element (pointer Hist1DList[i][j]) in array Hist1DList[i]
      }
      delete[] (it->second).Hist1DList; //deallocate memory for every element (pointer Hist1DList[i]) in array Hist1DList
      (it->second).Hist1DList = NULL;
      if((it->second).MultiSlice){
	      for(int i=0;i<N_OF_HIST;i++){
	        for(int j=0;j<N_OF_HIST;j++){
	          for(int k=0;k<N_OF_SLICE;k++){
	            if ((it->second).Hist1DListSliced[i][j][k]==NULL){break;}
	            else {delete (it->second).Hist1DListSliced[i][j][k];} 
	          }			    
	          delete[] (it->second).Hist1DListSliced[i][j];
	        }
	        delete[] (it->second).Hist1DListSliced[i];
	      }
	      delete[] (it->second).Hist1DListSliced;
	      (it->second).Hist1DListSliced = NULL;
      }
    }else if((it->second).Dim==2){
      for(int i=0;i<N_OF_HIST;i++){
	      for(int j=0;j<N_OF_HIST;j++){
	        if ((it->second).Hist2DList[i][j]==NULL){break;}
	        else {delete (it->second).Hist2DList[i][j];} 
	      }
	      delete[] (it->second).Hist2DList[i];
      }
      delete[] (it->second).Hist2DList;
      (it->second).Hist2DList = NULL;
    }
  }

  Status = 0;
  return 0;
}

/***********************************************************************************/
int Analyzer::DelStdHistograms()
{
  if (Std_First!=0){ 
    delete Std_First;
    Std_First=NULL;
  }
  if (Std_Second!=0){
    delete Std_Second;
    Std_Second=NULL;
  }
  for (int i=0;i<Std_First_Slice.size();i++){
    delete Std_First_Slice[i];
  }
  for (int i=0;i<Std_Second_Slice.size();i++){
    delete Std_Second_Slice[i];
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::RenewHistograms()
{
  //Delete histograms
  DelHistograms();
  //Redefine
  return InitHistograms();
}

/***********************************************************************************/
int Analyzer::SaveHistograms(string label)
{
	if(Purpose==1) return SaveExpHistograms(label, "Array"); 
  if(Status!=2){
    cout<<"Histograms are not yet loaded!\n";
    return -1;
  }
  string filename;
  filename = HISTOGRAM_DIRECTORY + "/SimHistograms_";
  filename += label;
  filename += ".root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");

  map <string,HistUnit>::iterator it;
  for (it = SimHistBox.begin();it != SimHistBox.end();++it){//for each histogram type in the box
    if((it->second).Enabled){
      if((it->second).Dim==1){ //if it's a 1D hist type
	for(int i=0;i<HistDim1;i++){//for each histogram in that array
	  for(int j=0;j<HistDim2;j++){
	    (it->second).Hist1DList[i][j]->Write(); //write the histogram from the 1D list
	  }
	}
	if ((it->second).MultiSlice){
	  for(int i=0;i<HistDim1;i++){
	    for(int j=0;j<HistDim2;j++){
	      for(int k=0;k<nSlice;k++){
		(it->second).Hist1DListSliced[i][j][k]->Write(); 
	      }
	    }
	  }
	}
      }else if((it->second).Dim==2){ //otherwise, if it is a 2D hist type
	for(int i=0;i<HistDim1;i++){
	  for(int j=0;j<HistDim2;j++){
	    (it->second).Hist2DList[i][j]->Write(); //write the histogram from the 2D list
	  }
	}
      }
    }
  }

  //Log
  AnalysisLog << "Histograms are saved to file: "<<filename<<endl;
  OutFile->Close();
  delete OutFile;
  return 0;
}

/***********************************************************************************/
int Analyzer::SaveGraphs(string label)
{
  string filename;
  filename = HISTOGRAM_DIRECTORY + "/SimHistograms_";
  filename += label;
  filename += ".root";
  TFile *OutFile = new TFile(filename.c_str(),"UPDATE");
  map <string,TGraphErrors *>::iterator ig;
  for (ig = SysGraphBox1D.begin();ig != SysGraphBox1D.end();++ig){
    (ig->second)->Write();
  }
  map <string,TGraph2DErrors *>::iterator igg;
  for (igg = SysGraphBox2D.begin();igg != SysGraphBox2D.end();++igg){
    (igg->second)->Write();
  }	
  cout << "Histograms are saved to file: "<<filename<<endl;
  //this is where we would save the graph if its status is defined
  OutFile->Close();
  delete OutFile;
  return 0;
}

/***********************************************************************************/
int Analyzer::LoadHistograms(string label)
{
  string filename;
  filename = HISTOGRAM_DIRECTORY + "/SimHistograms_";
  filename += label;
  filename += ".root";
  //TCanvas *mydummycanvas=new TCanvas();

  TFile *InFile = new TFile(filename.c_str(),"READ");
  if (InFile->IsZombie()){
    cout<<"Fail when opening file " << filename.c_str()<<"!\n";
    return -1;
  }
  //Read Histograms
  if (Status!=0)DelHistograms(); //if initialized, delete hists first.
  int i=0;
  int j=0;
  int k=0;
  TH1 * h = NULL;
  TH2 * hh = NULL;
  string HistName;

  gROOT->cd();

  map <string,HistUnit>::iterator it;
  
  InitToNull();	//Initialize array pointers for each hist unit to NULL.
  //Get the name of the histogram from the box and try to get the corresponding address of each array object in the input file. If successful, clone the histogram object from the file into the newly created one in the box. Otherwise, skip over to the next valid entry. Do this for all pointers in the histogram array, for each hist unit.
  HistDim1 = N_OF_HIST;
  HistDim2 = N_OF_HIST;
  for (it = SimHistBox.begin();it != SimHistBox.end();++it){
    if (!(it->second).Enabled)continue;
    if((it->second).Dim==1){
      for(j=0;j<N_OF_HIST;j++){
	for(i=0;i<N_OF_HIST;i++){
	  HistName = it->first;
	  HistName+=Form("_Cond%d,%d",i,j);
	  h = (TH1D*)InFile->Get(HistName.c_str());
	  if (h==NULL){
	    if(i==0 && j!=0) HistDim2=j; //set the histogram dimension of the column to the number of the last successful load of the column
	    break;
	  }
	  else{
	    (it->second).Hist1DList[i][j] = (TH1*)h->Clone();
	    //delete h; //delete object pointed to from memory
	  }
	}
	if (j==0){//the first histogram of the new column is not loaded
	  if(i!=0) HistDim1=i;// if at least one histogram has been loaded, set the histogram dimension of the row to the number of the last successful load of the row					
	  continue;
	}
	else {break;}
      }
      //for multi slice
      i=j=k=0;
      if((it->second).MultiSlice){
	for(i=0;i<HistDim1;i++){
	  for(j=0;j<HistDim2;j++){
	    for(k=0;k<N_OF_SLICE;k++){
	      HistName = it->first;
	      HistName+=Form("_Cond%d,%d;Slice%d",i,j,k);
	      h = (TH1D*)InFile->Get(HistName.c_str());
	      if (h==NULL){
		nSlice=k;
		break;
	      }
	      else{
		(it->second).Hist1DListSliced[i][j][k] = (TH1*)h->Clone();
		//delete h;
	      }
	    }
	    if (k==0){break;}
	  }
	  if (j==0){break;}
	}
      }
    }else if((it->second).Dim==2){
      for(j=0;j<N_OF_HIST;j++){
	for(i=0;i<N_OF_HIST;i++){
	  HistName = it->first;
	  HistName+=Form("_Cond%d,%d",i,j);
	  hh = (TH2D*)InFile->Get(HistName.c_str());
	  if (hh==NULL){
	    if(i==0 && j!=0){HistDim2=j;} //set the histogram dimension of the column to the number of the last successful load of the column
	    break;
	  }
	  else{
	    (it->second).Hist2DList[i][j] = (TH2*)hh->Clone();
	    //delete hh;
	  }
	}
	if (j==0){ //if the first histogram of the new column is not loaded
	  if (i!=0) HistDim1=i; //set the histogram dimension of the row to the number of the last successful load of the row
	  continue;
	}
	else{break;}
      }
    }
  }
  //Log
  AnalysisLog << "Histograms are loaded from file: "<<filename<<endl;


  Status=2;

  InFile->Close();
  delete InFile;

  return 0;
}

/***********************************************************************************/
int Analyzer::SetStdHist(string HistName, string LabelName)
{
  if(Status!=2){
    cout<<"Histograms are not yet loaded!\n";
    return -1;
  }
  if (SimHistBox.find(HistName)==SimHistBox.end()){
    cout << "Histogram "<<HistName<<" is not found.\n";
    return -1;
  }
  if (!SimHistBox[HistName].Enabled){
  	cout << "Histogram "<<HistName<<" was not enabled! \n";
  }
  if (SimHistBox[HistName].Hist1DList[0][0] == NULL){
   	cout << "Histogram " <<HistName<<" was not initialized! \n";
   	return -1;
  }	
  if(LabelName.compare("aPlus")==0){
    if (Std_First!=0) delete Std_First;
    Std_First = (TH1*)(SimHistBox[HistName].Hist1DList)[0][0]->Clone("Std_First");
  }
  else if(LabelName.compare("aMinus")==0){
    if (Std_Second!=0) delete Std_Second;
    Std_Second = (TH1*)(SimHistBox[HistName].Hist1DList)[0][0]->Clone("Std_Second");
  }
  else{
    cout<<"Wrong label name!\n";
    return -1;
  }
  //Log
  AnalysisLog << HistName << "[0][0]"<<" is set to Std-Histogram: "<<LabelName<<endl;
  //For Sliced Fit
  if (nSlice!=0){
    if (!SimHistBox[HistName].MultiSlice){
      cout << "MultiSlice is not allowed for "<<HistName<<endl;
      return -1;
    }
    if(LabelName.compare("aPlus")==0){
      for (int i=0;i<Std_First_Slice.size();i++){
	delete Std_First_Slice[i];
      }
      Std_First_Slice.clear();
      for (int i=0;i<nSlice;i++){
	Std_First_Slice.push_back((TH1*)(SimHistBox[HistName].Hist1DListSliced)[0][0][i]->Clone(Form("Std_First_Slice%d",i)));
      }
    }else if(LabelName.compare("aMinus")==0){
      for (int i=0;i<Std_Second_Slice.size();i++){
	delete Std_Second_Slice[i];
      }
      Std_Second_Slice.clear();
      for (int i=0;i<nSlice;i++){
	Std_Second_Slice.push_back((TH1*)(SimHistBox[HistName].Hist1DListSliced)[0][0][i]->Clone(Form("Std_Second_Slice%d",i)));
      }
    }else{
      cout<<"Wrong label name!\n";
      return -1;
    }
    AnalysisLog << HistName << "[0][0]"<<" is set to sliced Std-Histogram: "<<LabelName<<endl;
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::SaveStdHist(string HistName,string label)
{
  if (Std_First==NULL || Std_Second==NULL){
    cout<<"One of the standard histograms is not set yet!\n";
    return -1;
  }
  if (SimHistBox.find(HistName)==SimHistBox.end()){
    cout <<"Invalid histogram name "<<HistName<<endl;
    return -1;
  }
  string filename;
  filename = HISTOGRAM_DIRECTORY + "/StdHistograms_";
  filename += HistName;
  filename += "_";
  filename += label;
  filename += ".root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");

  Std_First->Write();
  Std_Second->Write();

  //for sliced fit
  if (Std_First_Slice.size()!=0){
    if (Std_First_Slice.size()!=Std_Second_Slice.size()){
      cout  << "Slice number for aPlus and aMinus don't match.\n";
      return -1;
    }
    for (int i=0;i<Std_First_Slice.size();i++){
      Std_First_Slice[i]->Write();
    }
    for (int i=0;i<Std_Second_Slice.size();i++){
      Std_Second_Slice[i]->Write();
    }
  }
  //Log
  AnalysisLog << "Std-Histograms are saved to file: "<<filename<<endl;
  OutFile->Close();
  delete OutFile;
  return 0;
}

/***********************************************************************************/
int Analyzer::LoadStdHist(string HistName,string label)
{
  DelStdHistograms();
  string filename;
  filename = HISTOGRAM_DIRECTORY + "/StdHistograms_";
  filename += HistName;
  filename += "_";
  filename += label;
  filename += ".root";
  if (SimHistBox.find(HistName)==SimHistBox.end()){
    cout <<"Invalid histogram name "<<HistName<<endl;
    return -1;
  }
  TFile *InFile = new TFile(filename.c_str(),"READ");
  if (InFile->IsZombie()){
    cout<<"Fail when opening file " << filename.c_str()<<"!\n";
    return -1;
  }
  //Read Histograms
  TH1* h1;
  TH1* h2;
  gROOT->cd();

  h1 = (TH1D*)InFile->Get("Std_First");
  h2 = (TH1D*)InFile->Get("Std_Second");

  Std_First  = (TH1*)h1->Clone();
  Std_Second = (TH1*)h2->Clone();
  //delete h1;
  //delete h2;

  //Try to read sliced histograms
  int i=0;
  for(i=0;i<N_OF_SLICE;i++){
    h1 = (TH1D*)InFile->Get(Form("Std_First_Slice%d",i));
    h2 = (TH1D*)InFile->Get(Form("Std_Second_Slice%d",i));
    if (h1==NULL || h2==NULL){
      break;
    }
    Std_First_Slice.push_back((TH1*)h1->Clone());
    Std_Second_Slice.push_back((TH1*)h2->Clone());
    //delete h1;
    //delete h2;
  }
  nSlice = i;
  //Log
  AnalysisLog << "Std-Histograms are loaded from file: "<<filename<<endl;
  InFile->Close();
  delete InFile;
  return 0;
}

/***********************************************************************************/
int Analyzer::FillHistograms(int i,int j,bool cond,SimRecDataStruct SimRecData){
  int returnval = 0;
  map <string,int (Analyzer::*)(int,int,bool,SimRecDataStruct)>::iterator it;
  for (it = FillFunctions.begin();it != FillFunctions.end();++it){
    //cout<<i<<" "<< it->first<<endl;
    returnval += (this->*(it->second))(i,j,cond,SimRecData);
  }
  //(this->*(FillFunctions["TOFHist"]))(i,j,cond,SimRecData);
  if (SlicedHistList.size()!=0) FillSliceHistograms(i,j,SimRecData);
  return returnval;
}
//int Analyzer::FillHistograms(int i,int j,bool cond,SimRecDataStruct SimRecData)
//{
//  if(SimHistBox["RawScint"].Enabled) ((SimHistBox["RawScint"].Hist1DList)[i][j])->Fill(SimRecData.ScintEnergy);
//  if(SimHistBox["RawMWPC"].Enabled) ((SimHistBox["RawMWPC"].Hist1DList)[i][j])->Fill(SimRecData.MWPCEnergy);
//  Conditioned
//  if (cond){
//    if(SimHistBox["CoinScint"].Enabled) ((SimHistBox["CoinScint"].Hist1DList)[i][j])->Fill(SimRecData.ScintEnergy);
//    if(SimHistBox["CoinMWPC"].Enabled) ((SimHistBox["CoinMWPC"].Hist1DList)[i][j])->Fill(SimRecData.MWPCEnergy);
//    if(SimHistBox["MWPC_Image"].Enabled) ((SimHistBox["MWPC_Image"].Hist2DList)[i][j])->Fill(SimRecData.MWPCHitPos[0],SimRecData.MWPCHitPos[1]);
//    if(SimHistBox["MWPC_vs_Scint"].Enabled) ((SimHistBox["MWPC_vs_Scint"].Hist2DList)[i][j])->Fill(SimRecData.MWPCEnergy,SimRecData.ScintEnergy);
//    if(SimHistBox["MCP_Image"].Enabled) ((SimHistBox["MCP_Image"].Hist2DList)[i][j])->Fill(SimRecData.HitPos[0],SimRecData.HitPos[1]);
//    if(SimHistBox["TOFHist"].Enabled) ((SimHistBox["TOFHist"].Hist1DList)[i][j])->Fill(SimRecData.TOF);
//    if(SimHistBox["TOF_vs_Scint"].Enabled) ((SimHistBox["TOF_vs_Scint"].Hist2DList)[i][j])->Fill(SimRecData.TOF,SimRecData.ScintEnergy);
//    if(SimHistBox["ExitAngleHist"].Enabled) ((SimHistBox["ExitAngleHist"].Hist1DList)[i][j])->Fill(SimRecData.ExitAngle_Be);
//    if (SlicedHistList.size()!=0) FillSliceHistograms(i,j,SimRecData);
//  }
//  return 0;
//}
int Analyzer::FillRawScint(int i,int j,bool cond,SimRecDataStruct SimRecData){
  ((SimHistBox["RawScint"].Hist1DList)[i][j])->Fill(SimRecData.ScintEnergy);
  return 0;
}
int Analyzer::FillRawMWPC(int i,int j,bool cond,SimRecDataStruct SimRecData){
  ((SimHistBox["RawMWPC"].Hist1DList)[i][j])->Fill(SimRecData.MWPCEnergy);
  return 0;
}
int Analyzer::FillCoinScint(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond)	((SimHistBox["CoinScint"].Hist1DList)[i][j])->Fill(SimRecData.ScintEnergy);
  return 0;
}
int Analyzer::FillCoinMWPC(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond)	((SimHistBox["CoinMWPC"].Hist1DList)[i][j])->Fill(SimRecData.MWPCEnergy);
  return 0;
}
int Analyzer::FillMWPC_Image(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond)	((SimHistBox["MWPC_Image"].Hist2DList)[i][j])->Fill(SimRecData.MWPCHitPos[0],SimRecData.MWPCHitPos[1]);
  return 0;
}
int Analyzer::FillMWPC_CAY(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond)	((SimHistBox["MWPC_CAY"].Hist2DList)[i][j])->Fill(SimRecData.MWPC_CY,SimRecData.MWPCHitPos[1]);
  return 0;
}
int Analyzer::FillMWPCPosX(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond)	((SimHistBox["MWPCPosX"].Hist1DList)[i][j])->Fill(SimRecData.MWPCHitPos[0]);
  return 0;
}
int Analyzer::FillMWPCPosY(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond)	((SimHistBox["MWPCPosY"].Hist1DList)[i][j])->Fill(SimRecData.MWPCHitPos[1]);
  return 0;
}
int Analyzer::FillMWPC_vs_Scint(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond)	((SimHistBox["MWPC_vs_Scint"].Hist2DList)[i][j])->Fill(SimRecData.MWPCEnergy,SimRecData.ScintEnergy);
  return 0;
}
int Analyzer::FillMCP_Image(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond)	((SimHistBox["MCP_Image"].Hist2DList)[i][j])->Fill(SimRecData.HitPos[0],SimRecData.HitPos[1]);
  return 0;
}
int Analyzer::FillMCP_R(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond)	((SimHistBox["MCP_R"].Hist1DList)[i][j])->Fill(sqrt(pow(SimRecData.HitPos[0],2.0)+pow(SimRecData.HitPos[1],2.0)));
  return 0;
}
int Analyzer::FillTOFHist(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond)	((SimHistBox["TOFHist"].Hist1DList)[i][j])->Fill(SimRecData.TOF);
  return 0;
}
int Analyzer::FillTOF_vs_Scint(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond)	((SimHistBox["TOF_vs_Scint"].Hist2DList)[i][j])->Fill(SimRecData.TOF,SimRecData.ScintEnergy);
  return 0;
}
int Analyzer::FillExitAngleHist(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond)	((SimHistBox["ExitAngleHist"].Hist1DList)[i][j])->Fill(SimRecData.ExitAngle_Be);
  return 0;
}
int Analyzer::FillQ_Values(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond) ((SimHistBox["Q_Values"].Hist2DList)[i][j])->Fill(SimRecData.Q_Value[0],SimRecData.Q_Value[1]);
  return 0;
}
int Analyzer::FillCosThetaENu(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond) ((SimHistBox["CosThetaENu"].Hist1DList)[i][j])->Fill(SimRecData.CosThetaENu);
  return 0;
}
int Analyzer::FillDECPos(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond){
//    if (SimRecData.DECPos[2]>65) cout <<"TOF= "<<SimRecData.TOF<<endl;
    if (SimRecData.DECPos[1]>-5 && SimRecData.DECPos[1]<5){
      ((SimHistBox["DECPos"].Hist2DList)[i][j])->Fill(SimRecData.DECPos[0],SimRecData.DECPos[2]);
    }
  }
  return 0;
}
int Analyzer::FillEkIon(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond) ((SimHistBox["EkIon"].Hist1DList)[i][j])->Fill(SimRecData.IonEnergy);
  return 0;
}
int Analyzer::FillEIon0(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond) ((SimHistBox["EIon0"].Hist1DList)[i][j])->Fill(SimRecData.EIonInit);
  return 0;
}
int Analyzer::FillEIon1(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond) ((SimHistBox["EIon1"].Hist1DList)[i][j])->Fill(SimRecData.EIon[0]);
  return 0;
}
int Analyzer::FillEIon2(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond) ((SimHistBox["EIon2"].Hist1DList)[i][j])->Fill(SimRecData.EIon[1]);
  return 0;
}
int Analyzer::FillHitAngle(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond) ((SimHistBox["HitAngle"].Hist1DList)[i][j])->Fill(SimRecData.HitAngle);
  return 0;
}
int Analyzer::FillMWPCHitShift(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond) ((SimHistBox["MWPCHitShift"].Hist2DList)[i][j])->Fill(SimRecData.MWPCHitShift[0],SimRecData.MWPCHitShift[1]);
  return 0;
}
int Analyzer::FillCOSBetaShiftAngle(int i,int j,bool cond,SimRecDataStruct SimRecData){
  if (cond) ((SimHistBox["COSBetaShiftAngle"].Hist1DList)[i][j])->Fill(SimRecData.COSBetaShiftAngle);
  return 0;
}
/***********************************************************************************/
int Analyzer::AddSlicedHist(string HistName,string ConditionName)
{
  if (Purpose==1){
    return AddSlicedExpHist(HistName,ConditionName);
  }
  if (SlicedHistList.find(HistName)!=SlicedHistList.end()){
    cout << HistName << " is already in the sliced histogram list.\n" <<endl;
    return 1;
  }
  if (SimHistBox.find(HistName)==SimHistBox.end()){
    cout << HistName << " is not an allowed histogram name. Check allowed histogram names first." <<endl;
    return -1;
  }
  SlicedHistList[HistName] = ConditionName;
  SimHistBox[HistName].MultiSlice = true;
  if (Status!=0){
    cout << "Histograms are already initialized before this change. Please reinitialize them!\n";
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::ClearSlicedHistList()
{
  if (Purpose==1){
    return ClearSlicedExpHistList();
  }
  if (Status!=0){
    cout << "Histograms are not deleted. Please delete them first!\n";
    return -1;
  }
  map <string, string>::iterator it;
  for (it=SlicedHistList.begin();it!=SlicedHistList.end();++it){
    SimHistBox[it->first].MultiSlice = false;
  }
  SlicedHistList.clear();
  return 0;
}

/***********************************************************************************/
int Analyzer::FillSliceHistograms(int i,int j,SimRecDataStruct SimRecData)
{
  string HistName;
  string ConditionName;
  map<string,string>::iterator it;
  int k=0;
  for (it=SlicedHistList.begin();it!=SlicedHistList.end();++it){
    HistName = it->first;
    ConditionName = it->second;
    if (ConditionName.compare("MCP_Rings")==0){
      double R = sqrt(pow(SimRecData.HitPos[0]-MCP_X,2.0) + pow(SimRecData.HitPos[1]-MCP_Y,2.0));
      for (k=0;k<nSlice;k++){
	if (R<SliceBoundary[k+1])
	  break;
      }
      //Out of range
      if (k==nSlice) return 0;
    }else{
      cout << "Unidentified slice condition name "<<ConditionName<<endl;
      return -1;
    }
    if (HistName.compare("TOFHist")==0){
      SimHistBox[HistName].Hist1DListSliced[i][j][k]->Fill(SimRecData.TOF);
    }else{
      cout <<"Histogram "<<HistName<<" is not available for multi-slice fit yet."<<endl;
      return 0;
    }
  }
  return 0;
}

/***********************************************************************************/
int Analyzer::ConfigHistogram(string Name,double xlow,double xhigh,int xNBin,double ylow,double yhigh,int yNBin)
{
  if (SimHistBox.find(Name)==SimHistBox.end()){
    cout << Name <<" is not found in histogram list!"<<endl;
    return -1;
  }
  SimHistBox[Name].XBinNum = xNBin;
  SimHistBox[Name].XRange.low = xlow;
  SimHistBox[Name].XRange.high = xhigh;
  if (SimHistBox[Name].Dim==2){
    SimHistBox[Name].YBinNum = yNBin;
    SimHistBox[Name].YRange.low = ylow;
    SimHistBox[Name].YRange.high = yhigh;
  }
  return 0;
}


/************************************************************************************/
int Analyzer::OutputHistogramToTxt(string Name, string fname){
  map <string,HistUnit> *HistBox;
  map<string,HistUnit>::iterator it;
  string filename, rootname, label;
  ofstream fout;
  int i,j,ix,iy,Nx,Ny,ibin;
  double x,y,z;
  TH1 * h = NULL;
  TH2 * hh = NULL;
  rootname = HISTOGRAM_DIRECTORY + "/TextOutput/" + fname +"_";

  
  if (Purpose==0) HistBox = &SimHistBox;
  else if (Purpose==1) HistBox = &ExpHistBox;
  
  it = HistBox->find(Name);
  if (it==HistBox->end()){
    cout <<"Error: "<< Name <<" is not found in histogram list!"<<endl;
    return -1;
  } 
  if (!(it->second).Enabled){
    cout <<"Error: "<< Name <<" is not Enabled!"<<endl;
    return -1; 
  }
  if((it->second).Dim==1){
     if((it->second).Hist1DList == NULL){
      cout <<"Error: "<< Name <<" is not initialized!"<<endl;
     }  
     for(j=0;j<HistDim2;j++){
	    for(i=0;i<HistDim1;i++){
	      if((it->second).Hist1DList[i][j] == NULL) continue;
	      h = (it->second).Hist1DList[i][j];
	      Nx = h->GetNbinsX();
	      label = it->first;
	      label+=Form("_Cond%d,%d",i,j);
	      label+=".txt";
	      filename = rootname + label;
	      fout.open(filename.c_str());
	      if (!fout.is_open()){
	        cout<< "Error: Failed to open file " << filename <<"for writing!"<<endl;
	      }
	      
	      for (ix=0;ix<Nx;ix++){             
	        x = h->GetBinCenter(ix);
          y = h->GetBinContent(ix);
	        fout<<x<<"\t"<<y<<endl;
        }
        fout.close();
      }
	  }
	}
  if((it->second).Dim==2){
    if((it->second).Hist2DList == NULL){
      cout <<"Error: "<< Name <<" is not initialized!"<<endl;
    }
    for(j=0;j<HistDim2;j++){
	    for(i=0;i<HistDim1;i++){
	      if((it->second).Hist2DList[i][j] == NULL) continue;
	      hh = (it->second).Hist2DList[i][j];
	      Nx = hh->GetNbinsX();
	      Ny = hh->GetNbinsY();
	      label = it->first;
	      label+=Form("_Cond%d,%d",i,j);
	      label+=".txt";
	      filename = rootname + label;
	      fout.open(filename.c_str());
	      if (!fout.is_open()){
	        cout<< "Error: Failed to open file " << filename <<"for writing!"<<endl;
	      }
	      for (ix=0;ix<Nx;ix++){
	        for (iy=0;iy<Ny;iy++){             
	          x = hh->GetXaxis()->GetBinCenter(ix);
            y = hh->GetYaxis()->GetBinCenter(iy);
            ibin = hh->GetBin(ix,iy);
            z = hh->GetBinContent(ibin);
	          fout<<x<<"\t"<<y<<"\t"<<z<<endl;
          }
        }
        fout.close();
      }
	  }
	}
  
  
  

}
/************************************************************************************/
int Analyzer::MakeMovie(string Name, string fname, int histdim, int index, string draw_option){
  map <string,HistUnit> *HistBox;
  map<string,HistUnit>::iterator it;
  string mname,msave,label;
  int i, N;
  int *I,*J;
  int zero = 0; 
  TH1 * h = NULL;
  TH2 * hh = NULL;
  mname = HISTOGRAM_DIRECTORY +"/" + fname + ".gif";//+50";
  msave = HISTOGRAM_DIRECTORY +"/" + fname + ".gif+50";

  double valmax= -1e-6;
  double valmax_temp;
  
  if (Purpose==0) HistBox = &SimHistBox;
  else if (Purpose==1) HistBox = &ExpHistBox;
  
  it = HistBox->find(Name);
  if (it==HistBox->end()){
    cout <<"Error: "<< Name <<" is not found in histogram list!"<<endl;
    return -1;
  } 
  if (!(it->second).Enabled){
    cout <<"Error: "<< Name <<" is not Enabled!"<<endl;
    return -1; 
  }

  if (histdim == 1){ 
    J = &index; 
    I = &i; 
    N = HistDim1;
  } else if(histdim == 2){
    I = &index;
    J = &i; 
    N = HistDim2;
  }else{
    cout<<"Error: Histunit dimenstion can only take value 1 or 2!"<<endl; 
    return -1;
  }
  ifstream ifile(mname.c_str());
  if(ifile){
    ifile.close();
    remove(mname.c_str());
  }//remove existing file

  TCanvas *can1 = new TCanvas("can1","",2*850,2*500);
  can1->cd();

  if((it->second).Dim==1){
     if((it->second).Hist1DList == NULL){
      cout <<"Error: "<< Name <<" is not initialized!"<<endl;
     }  
    for(i=0;i<N;i++){
      if((it->second).Hist1DList[*I][*J] == NULL) continue;
      h = (it->second).Hist1DList[*I][*J];
      valmax_temp = h->GetMaximum();
      if(valmax_temp > valmax) valmax = valmax_temp;
    }

    for(i=0;i<N;i++){
      if((it->second).Hist1DList[*I][*J] == NULL) continue;
      h = (it->second).Hist1DList[*I][*J];
      h->GetYaxis()->SetRangeUser(0,valmax);
      h->Draw(draw_option.c_str());
      can1->SaveAs(msave.c_str());
     
    }
	  
  }
  if((it->second).Dim==2){
    if((it->second).Hist2DList == NULL){
      cout <<"Error: "<< Name <<" is not initialized!"<<endl;
    }
    for(i=0;i<N;i++){
      if((it->second).Hist2DList[*I][*J] == NULL) continue;
      hh = (it->second).Hist2DList[*I][*J];
      valmax_temp = hh->GetMaximum();
      if(valmax_temp > valmax) valmax = valmax_temp;
    }
    for(i=0;i<N;i++){
      if((it->second).Hist2DList[*I][*J] == NULL) continue;
      hh = (it->second).Hist2DList[*I][*J];
      hh->GetZaxis()->SetRangeUser(0,valmax);
      hh->Draw(draw_option.c_str());
      //can1->SetTheta(0);
      //can1->SetPhi(155);
      gStyle->SetOptStat(kFALSE);      
      can1->SetLogz();
      can1->Update();
      can1->SaveAs(msave.c_str());
    }
  }

  delete can1;
  return 0;
}








