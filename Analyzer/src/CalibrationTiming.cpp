#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string>
#include <sstream> 
#include <vector>
#include <stdint.h>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TGraphErrors.h"
//Analyzer includes
#include "Analyzer.h"
#include "Calibration.h"

using namespace std;

/***********************************************************************************/
double Calibration::CalibrateTiming(){
  if (CurrentAnalyzer->ExpStatus!=2){
    cout << "Histograms are not yet loaded."<<endl;
    return 0.0;
  }

  TGraphErrors* Timing1 = new TGraphErrors();
  TGraphErrors* Timing2 = new TGraphErrors();
  TGraphErrors* TimingRes1 = new TGraphErrors();
  TGraphErrors* TimingRes2 = new TGraphErrors();
  TGraphErrors* TimeZero = new TGraphErrors();

  //Write to root file
  ostringstream convert1;
  convert1 << setfill('0') << setw(4)<<CurrentAnalyzer->FileIDListExp[0][0];
  ostringstream convert2;
  convert2 << setfill('0') << setw(4)<<CurrentAnalyzer->FileIDListExp[CurrentAnalyzer->HistDim1-1][0];
  string spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + CurrentAnalyzer->ExpFilePrefix + string("_") + convert1.str() + string("to") + convert2.str() + string("_TimingCal.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");
  for (int i=0;i<CurrentAnalyzer->HistDim1;i++){
    TF1* fitfunc = new TF1("DoubleGaus","gaus(0)+gaus(3)",-3.0,3.0);
    double Bin0 = CurrentAnalyzer->ExpHistBox["TOFHist"].Hist1DList[i][0]->FindBin(-0.8);
    double p0 = CurrentAnalyzer->ExpHistBox["TOFHist"].Hist1DList[i][0]->GetBinContent(Bin0);
    double Bin3 = CurrentAnalyzer->ExpHistBox["TOFHist"].Hist1DList[i][0]->FindBin(0.8);
    double p3 = CurrentAnalyzer->ExpHistBox["TOFHist"].Hist1DList[i][0]->GetBinContent(Bin3);
    fitfunc->SetParameters(p0,-0.8,0.2,p3,0.8,0.3);
    CurrentAnalyzer->ExpHistBox["TOFHist"].Hist1DList[i][0]->Fit(fitfunc,"","",-3.0,3.0);
    CurrentAnalyzer->ExpHistBox["TOFHist"].Hist1DList[i][0]->Write();
    Timing1->SetPoint(i,CurrentAnalyzer->FileIDListExp[i][0],fitfunc->GetParameter(1));
    Timing1->SetPointError(i,0,fitfunc->GetParError(1));
    TimingRes1->SetPoint(i,CurrentAnalyzer->FileIDListExp[i][0],fitfunc->GetParameter(2));
    TimingRes1->SetPointError(i,0,fitfunc->GetParError(2));
    Timing2->SetPoint(i,CurrentAnalyzer->FileIDListExp[i][0],fitfunc->GetParameter(4));
    Timing2->SetPointError(i,0,fitfunc->GetParError(4));
    TimingRes2->SetPoint(i,CurrentAnalyzer->FileIDListExp[i][0],fitfunc->GetParameter(5));
    TimingRes2->SetPointError(i,0,fitfunc->GetParError(5));
    TimeZero->SetPoint(i,CurrentAnalyzer->FileIDListExp[i][0],(fitfunc->GetParameter(4)+fitfunc->GetParameter(1))/2.0);
    TimeZero->SetPointError(i,0,sqrt(pow(fitfunc->GetParError(1),2.0)+pow(fitfunc->GetParError(4),2.0))/2.0);
    delete fitfunc;
  }
  Timing1->SetMarkerStyle(21);
  Timing1->SetName("Timing1");
  Timing1->SetTitle("Timing1");
  Timing1->Write();
  TimingRes1->SetMarkerStyle(21);
  TimingRes1->SetName("TimingRes1");
  TimingRes1->SetTitle("TimingRes1");
  TimingRes1->Write();
  Timing2->SetMarkerStyle(21);
  Timing2->SetName("Timing2");
  Timing2->SetTitle("Timing2");
  Timing2->Write();
  TimingRes2->SetMarkerStyle(21);
  TimingRes2->SetName("TimingRes2");
  TimingRes2->SetTitle("TimingRes2");
  TimingRes2->Write();
  TimeZero->SetMarkerStyle(21);
  TimeZero->SetName("Time Zero");
  TimeZero->SetTitle("Time Zero");
  TimeZero->Write();
  histfile->Close();
  delete histfile;
  return 0.0;
}

/***********************************************************************************/
int Calibration::CalibrateTimingRes(){
  if (CurrentAnalyzer->CombStatus!=2){
    cout << "Histograms are not yet loaded."<<endl;
    return 0;
  }


  double CalInterval = 200.0;
  int CalNum = 20;
  double MCPCalInterval = 12800.0;
  int MCPCalNum = 13;
  double TOFShift = -83.6;

  int CalID = 0;
  TGraphErrors* Timing1 = new TGraphErrors();
  TGraphErrors* Timing2 = new TGraphErrors();
  TGraphErrors* TimingRes1 = new TGraphErrors();
  TGraphErrors* TimingRes2 = new TGraphErrors();

  TGraphErrors* MCPTiming1 = new TGraphErrors();
  TGraphErrors* MCPTiming2 = new TGraphErrors();
  TGraphErrors* MCPTimingRes1 = new TGraphErrors();
  TGraphErrors* MCPTimingRes2 = new TGraphErrors();

  //Write to root file
  ostringstream convert;
  convert << setfill('0') << setw(4)<<CalID;
  string spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + CurrentAnalyzer->ExpFilePrefix + string("_TimingResCal_") + convert.str() + string(".root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");

  //Escint bin configurations
  double EStart = CurrentAnalyzer->CombExpHistBox["TOF_vs_Scint"].YRange.low;
  double Eend = CurrentAnalyzer->CombExpHistBox["TOF_vs_Scint"].YRange.high;
  int ENum = CurrentAnalyzer->CombExpHistBox["TOF_vs_Scint"].YBinNum;
  double EBinWidth = (Eend-EStart)/double(ENum);

  //QMCP bin configurations
  double QStart = CurrentAnalyzer->CombExpHistBox["TOF_vs_QMCP"].YRange.low;
  double Qend = CurrentAnalyzer->CombExpHistBox["TOF_vs_QMCP"].YRange.high;
  int QNum = CurrentAnalyzer->CombExpHistBox["TOF_vs_QMCP"].YBinNum;
  double QBinWidth = (Qend-QStart)/double(QNum);

  CurrentAnalyzer->CombExpHistBox["TOF_vs_Scint"].Hist2DList[0][0]->Write();
  CurrentAnalyzer->CombExpHistBox["TOF_vs_QMCP"].Hist2DList[0][0]->Write();

  for (int i=0;i<CalNum;i++){
    double Start = i*CalInterval;
    double End = (i+1)*CalInterval;
    int StartBin = int((Start - EStart)/EBinWidth);
    int EndBin = int((End-EStart)/EBinWidth);
    TH1* TOFProjection = CurrentAnalyzer->CombExpHistBox["TOF_vs_Scint"].Hist2DList[0][0]->ProjectionX("TOF",StartBin,EndBin);
    //Fitting
    if (i<=CalNum){
      TF1* fitfunc = new TF1("DoubleGaus","gaus(0)+gaus(3)",-3.0,3.0);
      double Bin0 = TOFProjection->FindBin(TOFShift-0.8);
      double p0 = TOFProjection->GetBinContent(Bin0);
      double Bin3 = TOFProjection->FindBin(TOFShift+0.8);
      double p3 = TOFProjection->GetBinContent(Bin3);
      int MaxBin = TOFProjection->GetMaximumBin();
      double Max = TOFProjection->GetBinCenter(MaxBin);
//      fitfunc->SetParameters(p0,TOFShift-0.8,0.2,p3,TOFShift+0.8,0.3);
      fitfunc->SetParameters(p0,Max,0.2,p3,Max+1.7,0.3);
      TOFProjection->Fit(fitfunc,"","",TOFShift-3.0,TOFShift+3.0);
      TOFProjection->Write();
      Timing1->SetPoint(i,(i+0.5)*CalInterval,fitfunc->GetParameter(1));
      Timing1->SetPointError(i,0,fitfunc->GetParError(1));
      TimingRes1->SetPoint(i,(i+0.5)*CalInterval,fitfunc->GetParameter(2));
      TimingRes1->SetPointError(i,0,fitfunc->GetParError(2));
      Timing2->SetPoint(i,(i+0.5)*CalInterval,fitfunc->GetParameter(4));
      Timing2->SetPointError(i,0,fitfunc->GetParError(4));
      TimingRes2->SetPoint(i,(i+0.5)*CalInterval,fitfunc->GetParameter(5));
      TimingRes2->SetPointError(i,0,fitfunc->GetParError(5));
      delete fitfunc;
    }else{
      TOFProjection->Write();
    }
  }

  for (int i=0;i<MCPCalNum;i++){
    double Start = i*MCPCalInterval;
    double End = (i+1)*MCPCalInterval;
    int StartBin = int((Start - QStart)/QBinWidth);
    int EndBin = int((End-QStart)/QBinWidth);
    TH1* TOFProjection2 = CurrentAnalyzer->CombExpHistBox["TOF_vs_QMCP"].Hist2DList[0][0]->ProjectionX("TOF2",StartBin,EndBin);
    //Fitting
    if (i<=MCPCalNum){
      TF1* fitfunc = new TF1("DoubleGaus","gaus(0)+gaus(3)",-3.0,3.0);
      double Bin0 = TOFProjection2->FindBin(TOFShift-0.8);
      double p0 = TOFProjection2->GetBinContent(Bin0);
      double Bin3 = TOFProjection2->FindBin(TOFShift+0.8);
      double p3 = TOFProjection2->GetBinContent(Bin3);
      fitfunc->SetParameters(p0,TOFShift-0.8,0.2,p3,TOFShift+0.8,0.3);
      TOFProjection2->Fit(fitfunc,"","",TOFShift-3.0,TOFShift+3.0);
      TOFProjection2->Write();
      MCPTiming1->SetPoint(i,i*MCPCalInterval,fitfunc->GetParameter(1));
      MCPTiming1->SetPointError(i,0,fitfunc->GetParError(1));
      MCPTimingRes1->SetPoint(i,i*MCPCalInterval,fitfunc->GetParameter(2));
      MCPTimingRes1->SetPointError(i,0,fitfunc->GetParError(2));
      MCPTiming2->SetPoint(i,i*MCPCalInterval,fitfunc->GetParameter(4));
      MCPTiming2->SetPointError(i,0,fitfunc->GetParError(4));
      MCPTimingRes2->SetPoint(i,i*MCPCalInterval,fitfunc->GetParameter(5));
      MCPTimingRes2->SetPointError(i,0,fitfunc->GetParError(5));
      delete fitfunc;
    }else{
      TOFProjection2->Write();
    }
  }
  Timing1->SetMarkerStyle(21);
  Timing1->SetName("Timing1");
  Timing1->SetTitle("Timing1");
  Timing1->Write();
  TimingRes1->SetMarkerStyle(21);
  TimingRes1->SetName("TimingRes1");
  TimingRes1->SetTitle("TimingRes1");
  TimingRes1->Write();
  Timing2->SetMarkerStyle(21);
  Timing2->SetName("Timing2");
  Timing2->SetTitle("Timing2");
  Timing2->Write();
  TimingRes2->SetMarkerStyle(21);
  TimingRes2->SetName("TimingRes2");
  TimingRes2->SetTitle("TimingRes2");
  TimingRes2->Write();

  MCPTiming1->SetMarkerStyle(21);
  MCPTiming1->SetName("MCPTiming1");
  MCPTiming1->SetTitle("MCPTiming1");
  MCPTiming1->Write();
  MCPTimingRes1->SetMarkerStyle(21);
  MCPTimingRes1->SetName("MCPTimingRes1");
  MCPTimingRes1->SetTitle("MCPTimingRes1");
  MCPTimingRes1->Write();
  MCPTiming2->SetMarkerStyle(21);
  MCPTiming2->SetName("MCPTiming2");
  MCPTiming2->SetTitle("MCPTiming2");
  MCPTiming2->Write();
  MCPTimingRes2->SetMarkerStyle(21);
  MCPTimingRes2->SetName("MCPTimingRes2");
  MCPTimingRes2->SetTitle("MCPTimingRes2");
  MCPTimingRes2->Write();
  histfile->Close();
  delete histfile;
  return 0;
}

/***********************************************************************************/
int Calibration::CalibrateTimingZero(string FilePrefix,int FileID,int CalID){
  string spectrumfile;
  ExpDataStruct ExpData;      //Data struct for exp data reading
  RecDataStruct RecData;      //Reconstructed data struct
  TFile* tfilein = NULL; //pointer to input root file
  TTree * The_Tree = NULL; //input tree pointer
  CurrentAnalyzer->ConstructExpParameterList();
  CurrentAnalyzer->CheckExpConditionSettings();

  //Fit function select
//  string FitFuncSelect = "Gaus";
  string FitFuncSelect = "EMG";
  double FitRange = 2.0;

  //Escint bin configurations
  double EStart = 0.0;
  double EEnd = 10000.0;
  int ENum = 1000;
  double EBinWidth = (EEnd-EStart)/double(ENum);

  //QMCP bin configurations
  double QStart = 0.0;
  double QEnd = 640000.0;
  int QNum = 1000;
  double QBinWidth = (QEnd-QStart)/double(QNum);

  //Histograms
  TH2* TOF_vs_Scint = new TH2D("TOF_vs_Scint","TOF_vs_Scint",2000,-100,0,ENum,EStart,EEnd);
  TH2* TOF_vs_QMCP = new TH2D("TOF_vs_QMCP","TOF_vs_QMCP",2000,-100,0,QNum,QStart,QEnd);
  TH2* TOF_vs_Scint_Cal = new TH2D("TOF_vs_Scint_Cal","TOF_vs_Scint_Cal",2000,-50,50,ENum,EStart,EEnd);
  TH2* TOF_vs_QMCP_Cal = new TH2D("TOF_vs_QMCP_Cal","TOF_vs_QMCP_Cal",2000,-50,50,QNum,QStart,QEnd);
  TH1* h_TOF = new TH1D("h_TOF","TOF histogram",2000,-100,0);
  TH1* h_TOF_cond = new TH1D("h_TOF_cond","TOF histogram conditioned",2000,-100,0);
  TH1* h_TOF_cond2 = new TH1D("h_TOF_cond2","TOF histogram conditioned 2",2000,-100,0);
  TH1* h_TOF_Cal = new TH1D("h_TOF_Cal","TOF histogram calibrated",2000,-50,50);

  TH1* h_ProjectBelow;
  TH1* h_ProjectAbove;
  TF1* fitfuncBelow = new TF1(Form("fitfuncBelow"),"[0]*ROOT::Math::erfc((-x+[1]+[3]*[2]*[2])/(sqrt(2)*[2]))*exp([3]/2.0*(-2.0*x+2.0*[1]+[3]*[2]*[2]))",-100,-70);
  TF1* fitfuncAbove = new TF1(Form("fitfuncAbove"),"[0]*ROOT::Math::erfc((-x+[1]+[3]*[2]*[2])/(sqrt(2)*[2]))*exp([3]/2.0*(-2.0*x+2.0*[1]+[3]*[2]*[2]))",-100,-70);

  //T1T2 and MCP Image
  TH1* h_T1T2X = new TH1D("h_T1T2X","TX1+TX2",500,0,200);
  TH1* h_T1T2Y = new TH1D("h_T1T2Y","TY1+TY2",500,0,200);
  TH2* h_MCPImage = new TH2D("h_MCPImage","MCPImage",450,-45,45,450,-45,45);

  int CalNum = 16;
  double CalInterval = 50.0;
//  int CalNum = 20;
//  double CalInterval = 100.0;
  int MCPCalNum = 9;
  double MCPCalInterval = 15000.0;
  //Graphs
  TGraphErrors* ScintTiming = new TGraphErrors();
  ScintTiming->SetName("ScintTiming");
  ScintTiming->SetTitle("ScintTiming");
  TGraphErrors* ScintTimingRes = new TGraphErrors();
  ScintTimingRes->SetName("ScintTimingRes");
  ScintTimingRes->SetTitle("ScintTimingRes");

  TGraphErrors* MCPTiming = new TGraphErrors();
  MCPTiming->SetName("MCPTiming");
  MCPTiming->SetTitle("MCPTiming");
  TGraphErrors* MCPTimingRes = new TGraphErrors();
  MCPTimingRes->SetName("MCPTimingRes");
  MCPTimingRes->SetTitle("MCPTimingRes");

  TGraphErrors* ScintTiming_Cal = new TGraphErrors();
  ScintTiming_Cal->SetName("ScintTiming_Cal");
  ScintTiming_Cal->SetTitle("ScintTiming_Cal");
  TGraphErrors* ScintTimingRes_Cal = new TGraphErrors();
  ScintTimingRes_Cal->SetName("ScintTimingRes_Cal");
  ScintTimingRes_Cal->SetTitle("ScintTimingRes_Cal");

  TGraphErrors* MCPTiming_Cal = new TGraphErrors();
  MCPTiming_Cal->SetName("MCPTiming_Cal");
  MCPTiming_Cal->SetTitle("MCPTiming_Cal");
  TGraphErrors* MCPTimingRes_Cal = new TGraphErrors();
  MCPTimingRes_Cal->SetName("MCPTimingRes_Cal");
  MCPTimingRes_Cal->SetTitle("MCPTimingRes_Cal");

  //Read In Data
  if(CurrentAnalyzer->ReadMode.compare("MCP")!=0 && CurrentAnalyzer->ReadMode.compare("Double")!=0 && CurrentAnalyzer->ReadMode.compare("Triple")!=0 && CurrentAnalyzer->ReadMode.compare("He4")!=0){
      cout <<"Only MCP Triple Double He4 modes are allowed."<<endl;
    return -1;
  }
  
  ostringstream convert;
  convert << setfill('0') << setw(4)<<FileID;
  TString fName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + FilePrefix + string("_") + convert.str();
  fName += ".root";
  if(CurrentAnalyzer->OpenExpInputFile(fName.Data(),tfilein)==-1) return -1;
  CurrentAnalyzer->SetExpInTreeAddress(tfilein,The_Tree,ExpData);
  cout << "Reading file "<<fName<<endl;   

  int N_Event = The_Tree->GetEntries();
  double StartTime = 0;

  for(int i=0;i<N_Event;i++){
    //Read Tree
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    The_Tree->GetEntry(i);
    if (i==0)StartTime = ExpData.GroupTime;
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);
    //Apply T1T2 Cut
    h_T1T2X->Fill(RecData.MCP_TXSum);
    h_T1T2Y->Fill(RecData.MCP_TYSum);
    bool cond;
    cond = RecData.MCP_TXSum>CurrentAnalyzer->TXSumLow && RecData.MCP_TXSum<CurrentAnalyzer->TXSumHigh && RecData.MCP_TYSum>CurrentAnalyzer->TYSumLow && RecData.MCP_TYSum<CurrentAnalyzer->TYSumHigh;
    if (!cond)continue;
    h_TOF->Fill(RecData.TOF);
    TOF_vs_Scint->Fill(RecData.TOF,RecData.EScintA[0]);
    if (RecData.EScintA[0]<CurrentAnalyzer->ScintThreshold || RecData.EScintA[0]>CurrentAnalyzer->ScintUpperbound)continue;
    TOF_vs_QMCP->Fill(RecData.TOF,RecData.QMCP);
    h_TOF_cond->Fill(RecData.TOF);
    if(RecData.EScintA[0]>600 && RecData.EScintA[0]<1000)h_TOF_cond2->Fill(RecData.TOF);
    h_MCPImage->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
  }
  cout << N_Event << " events"<<endl;


  //Get the Timing peak
  TF1* FitEMG = new TF1("EMG","[0]*ROOT::Math::erfc((-x+[1]+[3]*[2]*[2])/(sqrt(2)*[2]))*exp([3]/2.0*(-2.0*x+2.0*[1]+[3]*[2]*[2]))",-100,-70);
  TF1* FitGaus = new TF1("Gaus","[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]",-100,-70);
  double Max = h_TOF_cond2->GetMaximum();
//  double Mean = h_TOF_cond->GetMean();
//  double RMS = h_TOF_cond->GetRMS();
  int MaxBin = h_TOF_cond2->GetMaximumBin();
  int Peak = h_TOF_cond2->GetBinCenter(MaxBin);
  FitEMG->SetParameters(Max,Peak,0.7,2.0);
  FitGaus->SetParameters(Max,Peak,0.7,0.0);
  FitGaus->FixParameter(3,0.0);
  h_TOF_cond2->Fit(FitFuncSelect.c_str(),"","",Peak-FitRange,Peak+FitRange);
  cout << "**********TOF Fit "<<h_TOF_cond2->GetFunction(FitFuncSelect.c_str())->GetParameter(1)<<endl;
  Max = h_TOF_cond->GetMaximum();
  MaxBin = h_TOF_cond->GetMaximumBin();
  Peak = h_TOF_cond->GetBinCenter(MaxBin);
  FitEMG->SetParameters(Max,Peak,0.7,2.0);
  FitGaus->SetParameters(Max,Peak,0.7,0.0);
  FitGaus->FixParameter(3,0.0);
  h_TOF_cond->Fit(FitFuncSelect.c_str(),"","",Peak-FitRange,Peak+FitRange);
  cout << "**********TOF whole range Fit "<<h_TOF_cond->GetFunction(FitFuncSelect.c_str())->GetParameter(1)<<endl;
  double TOFPeak = h_TOF_cond->GetFunction(FitFuncSelect.c_str())->GetParameter(1);
//  h_TOF_cond->Fit("EMG","","",Peak-FitRange,Peak+FitRange);
//  double TOFPeak = h_TOF_cond->GetFunction("EMG")->GetParameter(1);

  for (int i=0;i<CalNum;i++){
    double Start = i*CalInterval+CurrentAnalyzer->ScintThreshold;
    double End = (i+1)*CalInterval+CurrentAnalyzer->ScintThreshold;
    int StartBin = int((Start - EStart)/EBinWidth);
    int EndBin = int((End-EStart)/EBinWidth);
    TH1* TOFProjection = TOF_vs_Scint->ProjectionX(Form("TOF%d",i),StartBin,EndBin);
    //Fitting
    if (i<=CalNum){
      TF1* fitEMG = new TF1("EMGLocal","[0]*ROOT::Math::erfc((-x+[1]+[3]*[2]*[2])/(sqrt(2)*[2]))*exp([3]/2.0*(-2.0*x+2.0*[1]+[3]*[2]*[2]))",-100,-70);
      TF1* fitGaus = new TF1("GausLocal","[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]",-100,-70);
      int MaxBin = TOFProjection->GetMaximumBin();
      int Peak = TOFProjection->GetBinCenter(MaxBin);
      double MaxVal = TOFProjection->GetMaximum();
      fitEMG->SetParameters(MaxVal,Peak,0.7,2.0);
      fitEMG->SetParLimits(2,0.0,2.0);
      fitGaus->SetParameters(MaxVal,Peak,0.7,0.0);
      fitGaus->SetParLimits(0,0.8*MaxVal,1.2*MaxVal);
      fitGaus->SetParLimits(1,Peak-FitRange,Peak+FitRange);
      fitGaus->SetParLimits(2,0.0,2.0);
      fitGaus->SetParLimits(3,0.0,MaxVal*0.2);
      fitGaus->FixParameter(3,0.0);
      TOFProjection->Fit((FitFuncSelect+"Local").c_str(),"","",Peak-FitRange,Peak+FitRange);
//      TOFProjection->Write();
      ScintTiming->SetPoint(i,(i+0.5)*CalInterval+CurrentAnalyzer->ScintThreshold,TOFProjection->GetFunction((FitFuncSelect+"Local").c_str())->GetParameter(1));
      ScintTiming->SetPointError(i,0,TOFProjection->GetFunction((FitFuncSelect+"Local").c_str())->GetParError(1));
      ScintTimingRes->SetPoint(i,(i+0.5)*CalInterval+CurrentAnalyzer->ScintThreshold,TOFProjection->GetFunction((FitFuncSelect+"Local").c_str())->GetParameter(2));
      ScintTimingRes->SetPointError(i,0,TOFProjection->GetFunction((FitFuncSelect+"Local").c_str())->GetParError(2));
      delete fitEMG;
      delete fitGaus;
    }  
  }

  for (int i=0;i<MCPCalNum;i++){
    double Start = i*MCPCalInterval;
    double End = (i+1)*MCPCalInterval;
    int StartBin = int((Start - QStart)/QBinWidth);
    int EndBin = int((End-QStart)/QBinWidth);
    TH1* TOFProjection2 = TOF_vs_QMCP->ProjectionX(Form("MCPTOF%d",i),StartBin,EndBin);
    //Fitting
    if (i<=MCPCalNum){
      TF1* fitEMG = new TF1("EMGLocal","[0]*ROOT::Math::erfc((-x+[1]+[3]*[2]*[2])/(sqrt(2)*[2]))*exp([3]/2.0*(-2.0*x+2.0*[1]+[3]*[2]*[2]))",-100,-70);
      TF1* fitGaus = new TF1("GausLocal","[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]",-100,-70);
      int MaxBin = TOFProjection2->GetMaximumBin();
      int Peak = TOFProjection2->GetBinCenter(MaxBin);
      double MaxVal = TOFProjection2->GetMaximum();
      fitEMG->SetParameters(MaxVal,Peak,0.7,2.0);
      fitEMG->SetParLimits(2,0.0,4.0);
      fitGaus->SetParameters(MaxVal,Peak,0.7,0.0);
      fitGaus->SetParameters(MaxVal,Peak,0.7,0.0);
      fitGaus->SetParLimits(0,0.8*MaxVal,1.2*MaxVal);
      fitGaus->SetParLimits(1,Peak-FitRange,Peak+FitRange);
      fitGaus->SetParLimits(2,0.0,2.0);
      fitGaus->SetParLimits(3,0.0,MaxVal*0.2);
      fitGaus->FixParameter(3,0.0);
      TOFProjection2->Fit((FitFuncSelect+"Local").c_str(),"","",Peak-FitRange,Peak+FitRange);
//      TOFProjection2->Write();
      MCPTiming->SetPoint(i,(i+0.5)*MCPCalInterval,TOFProjection2->GetFunction((FitFuncSelect+"Local").c_str())->GetParameter(1));
      MCPTiming->SetPointError(i,0,TOFProjection2->GetFunction((FitFuncSelect+"Local").c_str())->GetParError(1));
      MCPTimingRes->SetPoint(i,(i+0.5)*MCPCalInterval,TOFProjection2->GetFunction((FitFuncSelect+"Local").c_str())->GetParameter(2));
      MCPTimingRes->SetPointError(i,0,TOFProjection2->GetFunction((FitFuncSelect+"Local").c_str())->GetParError(2));
      delete fitEMG;
      delete fitGaus;
    }  
  }

  //Reprocess
  for(int i=0;i<N_Event;i++){
    //Read Tree
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    The_Tree->GetEntry(i);
    if (i==0)StartTime = ExpData.GroupTime;
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);
    //Apply T1T2 Cut
    bool cond;
    cond = RecData.MCP_TXSum>CurrentAnalyzer->TXSumLow && RecData.MCP_TXSum<CurrentAnalyzer->TXSumHigh && RecData.MCP_TYSum>CurrentAnalyzer->TYSumLow && RecData.MCP_TYSum<CurrentAnalyzer->TYSumHigh;
    if (!cond)continue;
    if (RecData.EScintA[0]<CurrentAnalyzer->ScintThreshold || RecData.EScintA[0]>CurrentAnalyzer->ScintUpperbound)continue;
    //Correct MCP timing
    int Index  = RecData.QMCP/MCPCalInterval;
    if (Index>=MCPCalNum)continue;
    double TOFShift_MCP;
    double Q_read;
    MCPTiming->GetPoint(Index,Q_read,TOFShift_MCP);
//    TOFShift_MCP-=TOFPeak;
    //Correct Scint timing
    Index  = (RecData.EScintA[0]-CurrentAnalyzer->ScintThreshold)/CalInterval;
    if (Index>=CalNum)continue;
    double TOFShift_Scint;
    double E_read;
    ScintTiming->GetPoint(Index,E_read,TOFShift_Scint);
//    TOFShift_Scint-=TOFPeak;

    TOF_vs_Scint_Cal->Fill(RecData.TOF-TOFShift_MCP,RecData.EScintA[0]);
    TOF_vs_QMCP_Cal->Fill(RecData.TOF-TOFShift_MCP,RecData.QMCP);
    h_TOF_Cal->Fill(RecData.TOF-TOFShift_MCP-TOFShift_Scint);
  }
  cout << N_Event << " events"<<endl;
  //f->Close();
    //Close input file
  if (tfilein!=NULL){
    tfilein->Close();
    delete tfilein;
    tfilein =NULL;
  }
  //Calculate TOF difference for E_scint below and above 500keV
  h_ProjectBelow = (TH1*)TOF_vs_Scint->ProjectionX(Form("MCPTOFBelow"),int(250/EBinWidth),int(450/EBinWidth))->Clone();
  h_ProjectAbove = (TH1*)TOF_vs_Scint->ProjectionX(Form("MCPTOFAbove"),int(550/EBinWidth),int(750/EBinWidth))->Clone();

  int MaxBinBelow = h_ProjectBelow->GetMaximumBin();
  int PeakBelow = h_ProjectBelow->GetBinCenter(MaxBinBelow);
  int MaxValBelow = h_ProjectBelow->GetMaximum();
  fitfuncBelow->SetParLimits(2,0.0,2.0);
  fitfuncBelow->SetParameters(MaxValBelow,PeakBelow,0.7,2.0);
  h_ProjectBelow->Fit(fitfuncBelow,"","",PeakBelow-4.0,PeakBelow+4.0);

  int MaxBinAbove = h_ProjectAbove->GetMaximumBin();
  int PeakAbove = h_ProjectAbove->GetBinCenter(MaxBinAbove);
  int MaxValAbove = h_ProjectAbove->GetMaximum();
  fitfuncAbove->SetParLimits(2,0.0,2.0);
  fitfuncAbove->SetParameters(MaxValAbove,PeakAbove,0.7,2.0);
  h_ProjectAbove->Fit(fitfuncAbove,"","",PeakAbove-4.0,PeakAbove+4.0);

  cout << "TOF Below = " << fitfuncBelow->GetParameter(1)<< " +/- "<<fitfuncBelow->GetParError(1)<<endl;
  cout << "TOF Above = " << fitfuncAbove->GetParameter(1)<< " +/- "<<fitfuncAbove->GetParError(1)<<endl;

  //Write to root file
  //Write sliced TOF histograms first
  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_TimingZeroCal.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");

  for (int i=0;i<CalNum;i++){
    double Start = i*CalInterval+CurrentAnalyzer->ScintThreshold;
    double End = (i+1)*CalInterval+CurrentAnalyzer->ScintThreshold;
    int StartBin = int((Start - EStart)/EBinWidth);
    int EndBin = int((End-EStart)/EBinWidth);
    TH1* TOFProjection = TOF_vs_Scint_Cal->ProjectionX(Form("TOF%d",i),StartBin,EndBin);
    //Fitting
    if (i<=CalNum){
      TF1* fitEMG = new TF1("EMGLocal","[0]*ROOT::Math::erfc((-x+[1]+[3]*[2]*[2])/(sqrt(2)*[2]))*exp([3]/2.0*(-2.0*x+2.0*[1]+[3]*[2]*[2]))",-100,-70);
      TF1* fitGaus = new TF1("GausLocal","[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]",-100,-70);
      int MaxBin = TOFProjection->GetMaximumBin();
      int Peak = TOFProjection->GetBinCenter(MaxBin);
      double MaxVal = TOFProjection->GetMaximum();
      fitEMG->SetParameters(MaxVal,Peak,0.7,2.0);
      fitEMG->SetParLimits(2,0.0,2.0);
      fitGaus->SetParameters(MaxVal,Peak,0.7,0.0);
      fitGaus->SetParameters(MaxVal,Peak,0.7,0.0);
      fitGaus->SetParLimits(0,0.8*MaxVal,1.2*MaxVal);
      fitGaus->SetParLimits(1,Peak-FitRange,Peak+FitRange);
      fitGaus->SetParLimits(2,0.0,2.0);
      fitGaus->SetParLimits(3,0.0,MaxVal*0.2);
      fitGaus->FixParameter(3,0.0);
      TOFProjection->Fit((FitFuncSelect+"Local").c_str(),"","",Peak-FitRange,Peak+FitRange);
      TOFProjection->Write();
      ScintTiming_Cal->SetPoint(i,(i+0.5)*CalInterval+CurrentAnalyzer->ScintThreshold,TOFProjection->GetFunction((FitFuncSelect+"Local").c_str())->GetParameter(1));
      ScintTiming_Cal->SetPointError(i,0,TOFProjection->GetFunction((FitFuncSelect+"Local").c_str())->GetParError(1));
      ScintTimingRes_Cal->SetPoint(i,(i+0.5)*CalInterval+CurrentAnalyzer->ScintThreshold,TOFProjection->GetFunction((FitFuncSelect+"Local").c_str())->GetParameter(2));
      ScintTimingRes_Cal->SetPointError(i,0,TOFProjection->GetFunction((FitFuncSelect+"Local").c_str())->GetParError(2));
      delete fitEMG;
      delete fitGaus;
    }  
  }

  for (int i=0;i<MCPCalNum;i++){
    double Start = i*MCPCalInterval;
    double End = (i+1)*MCPCalInterval;
    int StartBin = int((Start - QStart)/QBinWidth);
    int EndBin = int((End-QStart)/QBinWidth);
    TH1* TOFProjection2 = TOF_vs_QMCP_Cal->ProjectionX(Form("MCPTOF%d",i),StartBin,EndBin);
    //Fitting
    if (i<=MCPCalNum){
      TF1* fitEMG = new TF1("EMGLocal","[0]*ROOT::Math::erfc((-x+[1]+[3]*[2]*[2])/(sqrt(2)*[2]))*exp([3]/2.0*(-2.0*x+2.0*[1]+[3]*[2]*[2]))",-100,-70);
      TF1* fitGaus = new TF1("GausLocal","[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]",-100,-70);
      int MaxBin = TOFProjection2->GetMaximumBin();
      int Peak = TOFProjection2->GetBinCenter(MaxBin);
      double MaxVal = TOFProjection2->GetMaximum();
      fitEMG->SetParameters(MaxVal,Peak,0.7,2.0);
      fitEMG->SetParLimits(2,0.0,4.0);
      fitGaus->SetParameters(MaxVal,Peak,0.7,0.0);
      fitGaus->SetParameters(MaxVal,Peak,0.7,0.0);
      fitGaus->SetParLimits(0,0.8*MaxVal,1.2*MaxVal);
      fitGaus->SetParLimits(1,Peak-FitRange,Peak+FitRange);
      fitGaus->SetParLimits(2,0.0,2.0);
      fitGaus->SetParLimits(3,0.0,MaxVal*0.2);
      fitGaus->FixParameter(3,0.0);
      TOFProjection2->Fit((FitFuncSelect+"Local").c_str(),"","",Peak-FitRange,Peak+FitRange);
      TOFProjection2->Write();
      MCPTiming_Cal->SetPoint(i,(i+0.5)*MCPCalInterval,TOFProjection2->GetFunction((FitFuncSelect+"Local").c_str())->GetParameter(1));
      MCPTiming_Cal->SetPointError(i,0,TOFProjection2->GetFunction((FitFuncSelect+"Local").c_str())->GetParError(1));
      MCPTimingRes_Cal->SetPoint(i,(i+0.5)*MCPCalInterval,TOFProjection2->GetFunction((FitFuncSelect+"Local").c_str())->GetParameter(2));
      MCPTimingRes_Cal->SetPointError(i,0,TOFProjection2->GetFunction((FitFuncSelect+"Local").c_str())->GetParError(2));
      delete fitEMG;
      delete fitGaus;
    }  
  }

  /************************************************************************************************/
  //Further Write to calibration file
  char filename[100];
  ofstream CalFileOut;
  sprintf(filename,"%s/TimingZeroCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  CalFileOut.open(filename,ios::out);
  ///
  CalFileOut.close();

  //Write the remaining histograms and graphs
//  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_TimingZeroCal.root");
//  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");
  h_T1T2X->Write();
  h_T1T2Y->Write();
  h_MCPImage->Write();
  h_TOF->Write();
  h_TOF_cond->Write();
  h_TOF_cond2->Write();
  h_TOF_Cal->Write();
  TOF_vs_Scint->Write(); 
  TOF_vs_QMCP->Write(); 
  TOF_vs_Scint_Cal->Write();
  TOF_vs_QMCP_Cal->Write();
  ScintTiming->Write(); 
  ScintTimingRes->Write();
  MCPTiming->Write();
  MCPTimingRes->Write();
  ScintTiming_Cal->Write(); 
  ScintTimingRes_Cal->Write();
  MCPTiming_Cal->Write();
  MCPTimingRes_Cal->Write();
  h_ProjectBelow->Write();
  h_ProjectAbove->Write();
  histfile->Close();
  return 0;
}

/***********************************************************************************/
