/*
 * =====================================================================================
 *
 *       Filename:  UserFunctions.cpp
 *
 *    Description:  functions developed by user for specific purposes
 *
 *        Version:  1.0
 *        Created:  06/11/2015 14:03:15
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Ran Hong
 *   Organization:  
 *
 * =====================================================================================
 */
#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <stdint.h>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TF1.h"
#include "TH1.h"
#include "TFile.h"
#include "TTree.h"
#include "TCutG.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TGaxis.h"
#include "TFrame.h"
#include "TPaveText.h"
#include "TRandom3.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TFitResult.h"
#include "TString.h"
#include "GlobalConstants.h"

//Analyzer includes
#include "Analyzer.h"

/***********************************************************************************/
//Function definations

using namespace std;

double TriangleWave(double *x,double *par);
string StreamFitResults(TF1* fit, int precision);

//External Parameter List
/***********************************************************************************/
int Analyzer::InitExtParList(string Name, int N, double low, double interval){
  nExtPar = N;
  ExtParName = Name;
  for (int i=0;i<N;i++) ExtParList[i] = low + i*interval;
  return 0;
}
int Analyzer::SetExtParList(string Name, int N, double val[]){
  nExtPar = N;
  ExtParName = Name;
  for (int i=0;i<N;i++) ExtParList[i] = val[i];
  return 0;
}
/***********************************************************************************/
//User-defined functions
/***********************************************************************************/
int Analyzer::User_MatchBetaSpectra(int Dim,string Purpose1,double low1, double interval1,string XUnit,string Purpose2,double low2, double interval2, string YUnit)
{
  //Initialization
  string GraphName;
  string XTitle,YTitle,ZTitle;
  if (Status == 2){
    cout<<"Histograms loaded."<<endl;
    SetFileSimDim(HistDim1,HistDim2);
  }
  else {
    cout << "Load histograms first!";
  }

  if (HistDim2==1 && Dim!=1){
    cout << "Warning: there is actually only 1 histogram dimension. Set Dim to 1."<<endl;
    Dim=1;
  }

//  double XVal[7] = {0.0450*935,0.0453*935,0.0456*935,0.0460*935,0.0463*935,0.0466*935,0.0470*935};
  double XVal[7] = {-0.06,-0.03,0.0,0.03,0.06,0.09,0.12};

  //Rebin Histograms
  int Rebin = 1;
  ExpHistBox["Scint_EA"].Hist1DList[0][0]->Rebin(Rebin);
  for (int i=0;i<HistDim1;i++){
    SimHistBox["CoinScint"].Hist1DList[i][0]->Rebin(Rebin);
  }
  //Construct chi2
  double NormExp = ExpHistBox["Scint_EA"].Hist1DList[0][0]->Integral();
  double NormSim = 1;
  TH1** ResHist;
  ResHist = new TH1*[HistDim1];

  TGraph* Chi2Graph = new TGraph();
  Chi2Graph->SetName("Chi2");

  TCanvas* c1 = new TCanvas("canvas","canvas",0,0,1400,400);
  c1->Divide(7,2);
  for (int i=0;i<HistDim1;i++){
    NormSim = SimHistBox["CoinScint"].Hist1DList[i][0]->Integral();
    double scale = NormExp/NormSim;
    SimHistBox["CoinScint"].Hist1DList[i][0]->Scale(scale);
    SimHistBox["CoinScint"].Hist1DList[i][0]->SetLineColor(kRed);
    c1->cd(i+1);
    ExpHistBox["Scint_EA"].Hist1DList[0][0]->Draw();
    SimHistBox["CoinScint"].Hist1DList[i][0]->Draw("same");

    ResHist[i] = new TH1D(Form("Residual%d",i),Form("Residual%d",i),2000/Rebin,0,2000);
    double Chi2 = 0;
    double Ndf = 0;
    for (int j=0;j<2000/Rebin;j++){
      double y_exp = ExpHistBox["Scint_EA"].Hist1DList[0][0]->GetBinContent(j);
      double e_exp = ExpHistBox["Scint_EA"].Hist1DList[0][0]->GetBinError(j);
      double y_sim = SimHistBox["CoinScint"].Hist1DList[i][0]->GetBinContent(j);
      double e_sim = SimHistBox["CoinScint"].Hist1DList[i][0]->GetBinError(j);
      double tot_error = sqrt(pow(e_exp,2.0)+pow(e_sim,2.0));
      double y_diff = y_exp-y_sim;
      if (tot_error!=0.0){
	y_diff/=tot_error;
	Chi2+=pow(y_diff,2.0);
	Ndf+=1.0;
      }
      else y_diff=0;
      ResHist[i]->SetBinContent(j,y_diff);
      ResHist[i]->SetBinError(j,1.0);
    }

    Chi2Graph->SetPoint(i,XVal[i],(Chi2)/(Ndf-1.0));
    c1->cd(i+8);
    ResHist[i]->Draw();
  }
  
  //Save
  string filename;
  filename = HISTOGRAM_DIRECTORY + "/UserHist_MatchBetaSpectrum.root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");
  ExpHistBox["Scint_EA"].Hist1DList[0][0]->Write();
  for (int i=0;i<HistDim1;i++){
    SimHistBox["CoinScint"].Hist1DList[i][0]->Write();
    ResHist[i]->Write();
  }
  Chi2Graph->Write();
  OutFile->Close();
  delete OutFile;
  //Plot
  string SpectrumFileName = SPECTRA_DIRECTORY + "/Spectra_MatchBetaSpectra.pdf(";
  c1->SaveAs(SpectrumFileName.c_str());
  delete c1;
  TCanvas* c2 = new TCanvas("canvas2","canvas2",0,0,800,600);
  Chi2Graph->Draw("APL");
  Chi2Graph->SetMarkerStyle(21);
  Chi2Graph->Fit("pol2");
  SpectrumFileName = SPECTRA_DIRECTORY + "/Spectra_MatchBetaSpectra.pdf)";
  c2->SaveAs(SpectrumFileName.c_str());
  delete c2;

  for (int i=0;i<HistDim1;i++){
    delete ResHist[i];
  }
  delete[] ResHist;

  return 0;
}

/**********************************************************************************************/

int Analyzer::User_MCPGainStudy(string ParName,double Xlow, double XInc)
{
  TF1** FitFuncArray = new TF1*[HistDim1];
  TGraphErrors* PeakPos = new TGraphErrors();
  TGraphErrors* PeakWidth = new TGraphErrors();
  TGraphErrors* PeakTail = new TGraphErrors();
  string GraphName = string("PeakPos_vs_")+ParName;
  PeakPos->SetName(GraphName.c_str());
  GraphName = string("PeakWidth_vs_")+ParName;
  PeakWidth->SetName(GraphName.c_str());
  GraphName = string("PeakTail_vs_")+ParName;
  PeakTail->SetName(GraphName.c_str());

  for (int i=0;i<HistDim1;i++){
    FitFuncArray[i] = new TF1(Form("FitFunc%d",i),"[0]*ROOT::Math::erfc((x-[1]+[3]*[2]*[2])/(sqrt(2)*[2]))*exp([3]/2.0*(2.0*x-2.0*[1]+[3]*[2]*[2]))",0,150000);
    TH1* hist = ExpHistBox["MCP_Charge"].Hist1DList[i][0];
    double Max = hist->GetMaximum();
    int MaxBin = hist->GetMaximumBin();
    double Peak = hist->GetBinCenter(MaxBin);
//    double Mean = hist->GetMean();
    hist->Fit("gaus","","",Peak*0.8,Peak*1.2);
//    double RMS = hist->GetRMS();
    double Width = hist->GetFunction("gaus")->GetParameter(2);
//    FitFuncArray[i]->SetParameters(Max,Mean,RMS,0.0001);
    FitFuncArray[i]->SetParameters(Max,Peak,Width,0.0001);
    hist->Fit(FitFuncArray[i],"","",0,Peak*1.3);
    PeakPos->SetPoint(i,Xlow+i*XInc,FitFuncArray[i]->GetParameter(1));
    PeakPos->SetPointError(i,0,FitFuncArray[i]->GetParError(1));
    PeakWidth->SetPoint(i,Xlow+i*XInc,FitFuncArray[i]->GetParameter(2));
    PeakWidth->SetPointError(i,0,FitFuncArray[i]->GetParError(2));
    PeakTail->SetPoint(i,Xlow+i*XInc,1.0/FitFuncArray[i]->GetParameter(3)/FitFuncArray[i]->GetParameter(1));
    PeakTail->SetPointError(i,0,FitFuncArray[i]->GetParError(3)/pow(FitFuncArray[i]->GetParameter(3),2.0)/FitFuncArray[i]->GetParameter(1));
  }

  //Output
  string filename;
  filename = HISTOGRAM_DIRECTORY + "/UserHist_MCPGainStudy.root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");
  for (int i=0;i<HistDim1;i++){
    ExpHistBox["MCP_Charge"].Hist1DList[i][0]->Write();
  }
  PeakPos->Write();
  PeakWidth->Write();
  PeakTail->Write();
  OutFile->Close();
  return 0;
}

/**********************************************************************************************/

int Analyzer::User_CompareMCPMap(string label,int CalID)
{
  //Initialization
  string GraphName;
  string XTitle,YTitle,ZTitle;
  if (Status == 2){
    cout<<"Histograms loaded."<<endl;
    SetFileSimDim(HistDim1,HistDim2);
  }
  else {
    cout << "Load histograms first!";
  }

  //Re-assign Histogram pointers
  TH2* ExpMCPHist = ExpHistBox["MCP_Image"].Hist2DList[0][0];
  TH2* SimMCPHist = SimHistBox["MCP_Image"].Hist2DList[0][0];

  //Efficiency Map histograms
  TH2* ExpMapBase = new TH2D("ExpMapBase","ExpMapBase",17,-34.0,34.0,17,-34.0,34.0);
  TH2* SimMapBase = new TH2D("SimMapBase","SimMapBase",17,-34.0,34.0,17,-34.0,34.0);
  TH2* MCPEfficiency = new TH2D("MCPEfficiency","MCP Efficiency",17,-34.0,34.0,17,-34.0,34.0);
  TH1* EffStat = new TH1D("EffStat","EffStat",240,50,110);
  
  //Load data into maps
  int ExpNx = ExpMCPHist->GetNbinsX();
  int ExpNy = ExpMCPHist->GetNbinsY();
  int SimNx = SimMCPHist->GetNbinsX();
  int SimNy = SimMCPHist->GetNbinsY();
  for (int i=1;i<=ExpNx;i++){
    for (int j=1;j<=ExpNy;j++){
      double x = ExpMCPHist->GetXaxis()->GetBinCenter(i);
      double y = ExpMCPHist->GetYaxis()->GetBinCenter(j);
      double z = ExpMCPHist->GetBinContent(i,j);
      ExpMapBase->Fill(x,y,z);
    }
  }
  for (int i=1;i<=SimNx;i++){
    for (int j=1;j<=SimNy;j++){
      double x = SimMCPHist->GetXaxis()->GetBinCenter(i);
      double y = SimMCPHist->GetYaxis()->GetBinCenter(j);
      double z = SimMCPHist->GetBinContent(i,j);
      SimMapBase->Fill(x,y,z);
    }
  }
  for (int i=1;i<=17;i++){
    for (int j=1;j<=17;j++){
      double VExp = ExpMapBase->GetBinContent(i,j);
      double VSim = SimMapBase->GetBinContent(i,j);
      double Val = 0.0;
      if (VSim>0.0)Val = VExp/VSim;
      MCPEfficiency->SetBinContent(i,j,Val);
    }
  }
  double Max = MCPEfficiency->GetMaximum();
  MCPEfficiency->Scale(1.0/Max);
  for (int i=1;i<=17;i++){
    for (int j=1;j<=17;j++){
      double Val = MCPEfficiency->GetBinContent(i,j);
      EffStat->Fill(Val*100);
    }
  }

  //Save
  string filename;
  filename = HISTOGRAM_DIRECTORY + "/UserHist_CompareMCPMap_" + label + ".root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");
  ExpMCPHist->Write();
  SimMCPHist->Write();
  ExpMapBase->Write();
  SimMapBase->Write();
  MCPEfficiency->Write();
  EffStat->Write();
  OutFile->Close();
  delete OutFile;
  //Plot
  /*
  string SpectrumFileName = SPECTRA_DIRECTORY + "/Spectra_MatchBetaSpectra.pdf(";
  c1->SaveAs(SpectrumFileName.c_str());
  delete c1;
  TCanvas* c2 = new TCanvas("canvas2","canvas2",0,0,800,600);
  Chi2Graph->Draw("APL");
  Chi2Graph->SetMarkerStyle(21);
  Chi2Graph->Fit("pol2");
  SpectrumFileName = SPECTRA_DIRECTORY + "/Spectra_MatchBetaSpectra.pdf)";
  c2->SaveAs(SpectrumFileName.c_str());
  delete c2;
*/
  //Output to MCP Efficiency map
  ostringstream convert;
  convert << setfill('0') << setw(2)<<CalID;

  filename = CALIBRATION_DIRECTORY + "/MCPEfficiencyMap";      
  filename += convert.str();
  filename += ".txt";

  ofstream outfile;
  outfile.open(filename.c_str());
  for (int i=1;i<=17;i++){
    for (int j=1;j<=17;j++){
      outfile<<MCPEfficiency->GetBinContent(i,j)<<" ";
    }
    outfile<<endl;
  }
  outfile.close();
  return 0;
}

/**********************************************************************************************/
int Analyzer::User_MWPCScatStudy(double Interval,string OutLabel)
{
  SetMode("Beta");
  int FileID = FileIDListSim[0][0];
  //Static Random generator
  static TRandom3 Rand_UserMWPCScat(0);
  
  TFile* tfilein = NULL; //pointer to input root file
  TTree * InTree = NULL; //input tree pointer
  InDataStruct InData;  //input data unit
  InDataStructDouble InDataDouble;      //input data double format
  SimRecDataStruct SimRecData;  //Reconstructed data
  ExpDataStruct ExpData;      //Data struct for exp data reading
  RecDataStruct RecData;      //Reconstructed data struct

  //Histograms
  TH2 *h_MWPCImageSim = new TH2D("h_MWPCImageSim","Image of MWPC Simulation",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH1 *h_ScintESim = new TH1D("h_ScintESim","Scint Energy Simulation",400,0,4000);
  TH2 *h_MWPCImageSimIn = new TH2D("h_MWPCImageSimIn","Image of MWPC Simulation Inside",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH1 *h_ScintESimIn = new TH1D("h_ScintESimIn","Scint Energy Simulation Inside",400,0,4000);
  TH2 *h_MWPCImageSimOut = new TH2D("h_MWPCImageSimOut","Image of MWPC Simulation Outside",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH1 *h_ScintESimOut = new TH1D("h_ScintESimOut","Scint Energy Simulation Outside",400,0,4000);

  TH2 *h_MWPCImageExp = new TH2D("h_MWPCImageExp","Image of MWPC Expriment",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH1 *h_ScintEExp = new TH1D("h_ScintEExp","Scint Energy Experiment",400,0,4000);
  TH2 *h_MWPCImageExpIn = new TH2D("h_MWPCImageExpIn","Image of MWPC Expriment Inside",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH1 *h_ScintEExpIn = new TH1D("h_ScintEExpIn","Scint Energy Experiment Inside",400,0,4000);
  TH2 *h_MWPCImageExpOut = new TH2D("h_MWPCImageExpOut","Image of MWPC Expriment Outside",200,-25.03125,24.96875,200,-25.03125,24.96875);
  TH1 *h_ScintEExpOut = new TH1D("h_ScintEExpOut","Scint Energy Experiment Outside",400,0,4000);

  //Geometry
  double D_MWPC = 84.65;//mm
  double D_Col = 65.0;//mm
  double R_Col = 13.0;//mm
  double R_Projection = R_Col*D_MWPC/D_Col;

  double X_Source = 40.30;//In MWPC coordinate system
  double Y_Source = -10.80;//In MWPC coordinate system
  double X_Projection = -X_Source*(D_MWPC-D_Col)/D_Col;
  double Y_Projection = -Y_Source*(D_MWPC-D_Col)/D_Col;

  //Density calculation angle range
  
  double theta = 30.0;
  cout  << "Projection information -- Center: "<<X_Projection<<" "<<Y_Projection << " ; R: "<<R_Projection<<endl;

  //Construct Offcenter cut array
  double OffsetDist = sqrt(pow(X_Projection,2.0)+pow(Y_Projection,2.0));
  double R_Min = 5.0;
  double R_Max = OffsetDist+MWPC_R;
  int NInterval = int((R_Max-R_Min)/Interval);
  double* RadiusArray = new double[NInterval+1];
  for (int i=0;i<=NInterval;i++){
    RadiusArray[i]=R_Min+i*Interval;
  }
  TH1** h_ScintEArraySim = new TH1*[NInterval];
  TH1** h_ScintEArrayExp = new TH1*[NInterval];
  for (int i=0;i<NInterval;i++){
    h_ScintEArraySim[i] = new TH1D(Form("h_ScintEArraySim_%d",i),Form("h_ScintEArraySim_%d",i),400,0,4000);
    h_ScintEArrayExp[i] = new TH1D(Form("h_ScintEArrayExp_%d",i),Form("h_ScintEArrayExp_%d",i),400,0,4000);
  }
  TH1* h_RadiusSim = new TH1D("h_RadiusSim","h_RadiusSim",NInterval,R_Min,R_Max);
  TH1* h_RadiusExp = new TH1D("h_RadiusExp","h_RadiusExp",NInterval,R_Min,R_Max);
  TH1* h_DensitySim = new TH1D("h_DensitySim","h_DensitySim",NInterval,R_Min,R_Max);
  TH1* h_DensityExp = new TH1D("h_DensityExp","h_DensityExp",NInterval,R_Min,R_Max);
    
  //Check Condition lists and parameter list
  int returnval = 0;
  returnval = CheckConditionSettings();
  if (returnval!=0)
  {
    cout << "Condition setting problem!\n";
    return -1;
  }
  ConstructParameterList();

  //Process Simulation Input
  if(SetupInputFile(FileID,tfilein,InTree)==-1) return -1;
  SetInTreeAddress(InTree, InData);
  int returnVal = 0; 
  int N_Event=0;
 
  //Read in

  while(1){
    returnVal = InTree->GetEntry(N_Event);
    if (returnVal==0) break; //eof
    else if(returnVal==-1) return -1;
    ConvertDataToDouble(InData,InDataDouble); 
    N_Event++;
//    InData.ScintEnergy = 0;
//    filein.read((char *)&(InData.ScintEnergy),sizeof(int));
//    if(filein.eof())break;
//    N_Event++;
//    filein.read((char *)&(InData.MWPCEnergy),sizeof(unsigned short));
//    filein.read((char *)(InData.MWPCHitPos),2*sizeof(short));
//    filein.read((char *)&(InData.MWPCAnodeSignal1),sizeof(unsigned short));
//    filein.read((char *)&(InData.MWPCAnodeSignal2),sizeof(unsigned short));
//    filein.read((char *)(InData.MWPCCathodeSignal),12*sizeof(unsigned short));
//    //        filein.read((char *)(InData.MWPCCathodeSignal),12*sizeof(unsigned int));
//    filein.read((char *)&(InData.ExitAngle_Be),sizeof(unsigned short));
//    filein.read((char *)(InData.InitP),3*sizeof(int));
//    filein.read((char *)(InData.DECPos),3*sizeof(short));
//    filein.read((char *)&(InData.Hits),sizeof(uint16_t));
//    filein.read((char *)&(InData.ParticleID),sizeof(uint8_t));
//    //Convert to double
//    InDataDouble.ScintEnergy = double(InData.ScintEnergy)/1000.0;
//    InDataDouble.MWPCEnergy = double(InData.MWPCEnergy)/1000.0;
//    InDataDouble.MWPCAnodeSignal1 = double(InData.MWPCAnodeSignal1)/2000.0;
//    InDataDouble.MWPCAnodeSignal2 = double(InData.MWPCAnodeSignal2)/2000.0;
///*    for (int i=0;i<12;i++){
//      InDataDouble.MWPCCathodeSignal[i] = double(InData.MWPCCathodeSignal[i])/8000.0;
//    }*/
//    InDataDouble.MWPC_X.AX1 = double(InData.MWPCCathodeSignal[0])/8000.0;
//    InDataDouble.MWPC_X.AX2 = double(InData.MWPCCathodeSignal[1])/8000.0;
//    InDataDouble.MWPC_X.AX3 = double(InData.MWPCCathodeSignal[2])/8000.0;
//    InDataDouble.MWPC_X.AX4 = double(InData.MWPCCathodeSignal[3])/8000.0;
//    InDataDouble.MWPC_X.AX5 = double(InData.MWPCCathodeSignal[4])/8000.0;
//    InDataDouble.MWPC_X.AX6 = double(InData.MWPCCathodeSignal[5])/8000.0;
//    InDataDouble.MWPC_Y.AY1 = double(InData.MWPCCathodeSignal[6])/8000.0;
//    InDataDouble.MWPC_Y.AY2 = double(InData.MWPCCathodeSignal[7])/8000.0;
//    InDataDouble.MWPC_Y.AY3 = double(InData.MWPCCathodeSignal[8])/8000.0;
//    InDataDouble.MWPC_Y.AY4 = double(InData.MWPCCathodeSignal[9])/8000.0;
//    InDataDouble.MWPC_Y.AY5 = double(InData.MWPCCathodeSignal[10])/8000.0;
//    InDataDouble.MWPC_Y.AY6 = double(InData.MWPCCathodeSignal[11])/8000.0;

//    for(int i=0;i<2;i++){
//      InDataDouble.MWPCHitPos[i] = double(InData.MWPCHitPos[i])/1000.0;
//    }
//    InDataDouble.ExitAngle_Be = double(InData.ExitAngle_Be)/100.0;
//    for(int i=0;i<3;i++){
//      InDataDouble.InitP[i] = double(InData.InitP[i])/1000.0;
//    }
//    for(int i=0;i<3;i++){
//      InDataDouble.DECPos[i] = double(InData.DECPos[i])/200.0;
//    }

    //Reconstruct
    ReconstructSim(0,0,SimRecData,InDataDouble,InData);

    //Adding smearing
    SimRecData.MWPCHitPos[0] += Rand_UserMWPCScat.Gaus(0.0,1.0);
    SimRecData.MWPCHitPos[1] += Rand_UserMWPCScat.Gaus(0.0,1.0);

    double R = sqrt(pow(SimRecData.MWPCHitPos[0],2.0)+pow(SimRecData.MWPCHitPos[1],2.0));
    double R_OffCenter = sqrt(pow(SimRecData.MWPCHitPos[0]-X_Projection,2.0)+pow(SimRecData.MWPCHitPos[1]-Y_Projection,2.0));
    //Conditions
    bool cond1 = SimRecData.ScintEnergy>ScintThreshold;
    bool cond2 = SimRecData.MWPCEnergy>MWPCThreshold;
    bool cond3 = R<MWPC_R;
    bool cond = cond1&&cond2&&cond3;
    //Fill Histograms
    if (cond){
      h_MWPCImageSim->Fill(SimRecData.MWPCHitPos[0],SimRecData.MWPCHitPos[1]);
      h_ScintESim->Fill(SimRecData.ScintEnergy);
      h_RadiusSim->Fill(R_OffCenter);
      //Determine the angle rel to the shadow center
      double D_x = SimRecData.MWPCHitPos[0] - X_Projection;
      double D_y = SimRecData.MWPCHitPos[1] - Y_Projection;
      if (D_x>0.0 && (D_y < R_OffCenter*sin((theta-15.0)/180.0*PI) || D_y > R_OffCenter*sin((-theta-15.0)/180.0*PI))){
	h_DensitySim->Fill(R_OffCenter);
      }
      //Off-center cuts
      if (R_OffCenter<R_Projection){
	h_MWPCImageSimIn->Fill(SimRecData.MWPCHitPos[0],SimRecData.MWPCHitPos[1]);
	h_ScintESimIn->Fill(SimRecData.ScintEnergy);
      }else{
	h_MWPCImageSimOut->Fill(SimRecData.MWPCHitPos[0],SimRecData.MWPCHitPos[1]);
	h_ScintESimOut->Fill(SimRecData.ScintEnergy);
      }
      int n = int((R_OffCenter-R_Min)/Interval);
      if (n>=0 && n<NInterval){
	h_ScintEArraySim[n]->Fill(SimRecData.ScintEnergy);
      }
    }
  }

  //close root input file
  if (tfilein!=NULL){
    //tfilein->Close(); done automatically upon deleting
    delete tfilein;
    tfilein =NULL;
  }
  
  //Process Experiment Input
  //Check Condition lists and parameter list
  returnval = CheckExpConditionSettings();
  if (returnval!=0)
  {
    cout << "Exp-Condition setting problem!\n";
    return -1;
  }
  ConstructExpParameterList();
  //Read in
  ostringstream convertExp;
  convertExp << setfill('0') << setw(4)<<FileIDListExp[0][0];
  TString fNameExp = EXP_DATA_DIRECTORY + string("/") + ExpFilePrefix.c_str() + string("_") + convertExp.str();
  fNameExp += ".root";
  TFile *f = new TFile (fNameExp.Data(), "read");

  TTree *Tree_Beta = (TTree*)f->Get("Tree_Beta");

  Tree_Beta->SetBranchAddress("Event_No",&ExpData.Event_Num);
  Tree_Beta->SetBranchAddress("GroupTime",&ExpData.GroupTime);
  Tree_Beta->SetBranchAddress("QPMT_A",&ExpData.QScint_A);
  Tree_Beta->SetBranchAddress("QPMT_D",&ExpData.QScint_D);
  Tree_Beta->SetBranchAddress("TPMT_A_rel",&ExpData.TScint_A_rel);
  Tree_Beta->SetBranchAddress("TPMT_DA",&ExpData.TScint_DA);
  Tree_Beta->SetBranchAddress("AMWPC_anode1",&ExpData.AMWPC_anode1);
  Tree_Beta->SetBranchAddress("AMWPC_anode2",&ExpData.AMWPC_anode2);
  Tree_Beta->SetBranchAddress("TBeta_diff",&ExpData.TBeta_diff);
  Tree_Beta->SetBranchAddress("MWPC_X",&ExpData.MWPC_X);
  Tree_Beta->SetBranchAddress("TMWPC_X",&ExpData.TMWPC_X);
  Tree_Beta->SetBranchAddress("MWPC_Y",&ExpData.MWPC_Y);
  Tree_Beta->SetBranchAddress("TMWPC_Y",&ExpData.TMWPC_Y);
  Tree_Beta->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
  int N_Beta = Tree_Beta->GetEntries();

  for(int i=0;i<N_Beta;i++){
    //Read Tree
    ClearTempData(ExpData,RecData);
    Tree_Beta->GetEntry(i);
    Reconstruct(0,0,RecData,ExpData);
    //Adding smearing
    RecData.MWPCPos_X += Rand_UserMWPCScat.Gaus(0.0,1.0);
    RecData.MWPCPos_Y += Rand_UserMWPCScat.Gaus(0.0,1.0);

    double R = sqrt(pow(RecData.MWPCPos_X,2.0)+pow(RecData.MWPCPos_Y,2.0));
    double R_OffCenter = sqrt(pow(RecData.MWPCPos_X-X_Projection,2.0)+pow(RecData.MWPCPos_Y-Y_Projection,2.0));
    //Conditions
    bool cond1 = RecData.EScintA[0]>ScintThreshold;
    bool cond2 = RecData.EMWPC_anode1+RecData.EMWPC_anode2>MWPCThreshold;
    bool cond3 = R<MWPC_R;
//    cout << sqrt(pow(RecData.MWPCPos_X,2.0)+pow(RecData.MWPCPos_Y,2.0))<<" "<<MWPC_R<<endl;
    bool cond = cond1 && cond2 && cond3;
    //Fill Histograms
    if (cond){
      h_MWPCImageExp->Fill(RecData.MWPCPos_X,RecData.MWPCPos_Y);
      h_ScintEExp->Fill(RecData.EScintA[0]);
      h_RadiusExp->Fill(R_OffCenter);
      //Determine the angle rel to the shadow center
      double D_x = RecData.MWPCPos_X - X_Projection;
      double D_y = RecData.MWPCPos_Y - Y_Projection;
      if (D_y < D_x*tan((theta-15.0)/180.0*PI) || D_y > D_x*tan((-theta-15.0)/180.0*PI)){
	h_DensityExp->Fill(R_OffCenter);
      }
      if (R_OffCenter<R_Projection){
	h_MWPCImageExpIn->Fill(RecData.MWPCPos_X,RecData.MWPCPos_Y);
	h_ScintEExpIn->Fill(RecData.EScintA[0]);
      }else{
	h_MWPCImageExpOut->Fill(RecData.MWPCPos_X,RecData.MWPCPos_Y);
	h_ScintEExpOut->Fill(RecData.EScintA[0]);
      }
      int n = int((R_OffCenter-R_Min)/Interval);
      if (n>=0 && n<NInterval){
	h_ScintEArrayExp[n]->Fill(RecData.EScintA[0]);
      }
    }
  }

  //Generating the circular edge
  TGraph* Circle = new TGraph();
  Circle->SetName("Circle");
  Circle->SetTitle("Circle");
  int NPoints = 100;
  int np = 0;
  for (int i=0;i<NPoints;i++){
    double Theta = double(i)/double(NPoints)*2.0*PI;
    double XCirc = X_Projection+R_Projection*cos(Theta);
    double YCirc = Y_Projection+R_Projection*sin(Theta);
    double DistToZero = sqrt(pow(XCirc,2.0)+pow(YCirc,2.0));
    if (DistToZero<MWPC_R){
      Circle->SetPoint(np,XCirc,YCirc);
      np++;
    }
  }

  //Calculate the density
  
  for (int i=1;i<=NInterval;i++){
    double Ri = R_Min + (i-1)*Interval;
    double Ro = R_Min + i*Interval;
    double Area = PI*(Ro*Ro - Ri*Ri)*theta/360; 
    h_DensitySim->SetBinContent(i,h_DensitySim->GetBinContent(i)/Area);
    h_DensityExp->SetBinContent(i,h_DensityExp->GetBinContent(i)/Area);
  }
  h_DensityExp->Scale(h_DensitySim->Integral()/h_DensityExp->Integral());

  //Some important information
  double InOutRatioSim = h_ScintESimIn->Integral()/h_ScintEExpOut->Integral();
  double InOutRatioExp = h_ScintEExpIn->Integral()/h_ScintEExpOut->Integral();
  cout <<"In/Out ratio Sim "<<InOutRatioSim<<" ; Exp "<<InOutRatioExp<<endl;

  //Write to root file
  string spectrumfile = SIM_DATA_DIRECTORY + string("/../Histograms/") + "UserHist_MWPCScatStudy" + string("_") + OutLabel + string(".root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");
  h_MWPCImageSim->Write();
  h_ScintESim->Write();
  h_MWPCImageSimIn->Write();
  h_ScintESimIn->Write();
  h_MWPCImageSimOut->Write();
  h_ScintESimOut->Write();

  h_MWPCImageExp->Write();
  h_ScintEExp->Write();
  h_MWPCImageExpIn->Write();
  h_ScintEExpIn->Write();
  h_MWPCImageExpOut->Write();
  h_ScintEExpOut->Write();

  h_RadiusSim->Write();
  h_RadiusExp->Write();
  h_DensitySim->Write();
  h_DensityExp->Write();

  for (int i=0;i<NInterval;i++){
    h_ScintEArraySim[i]->Write();
    h_ScintEArrayExp[i]->Write();
  }

  Circle->Write();

  histfile->Close();
  return 0;
}

/**********************************************************************************************/
int Analyzer::User_CompareSpectra(string ExpHistName, int NExp,string SimHistName, int NSim, bool Normalize, int Style, string label)
{
	cout<<"Running User_CaompareSpectra..."<<endl;
  int ExpDim = 1;
  int SimDim = 1;
  TH1*** Hist1DListExp = NULL;
  TH1*** Hist1DListSim = NULL;
  TH2*** Hist2DListExp = NULL;
  TH2*** Hist2DListSim = NULL;
  if (NExp>HistDim1 && NSim>HistDim1){
    cout << "Incorrect number of histograms:\t HistDim1 = "<<HistDim1<<"\n";
    return -1;
  }
  //Set Exp Histograms
  if (ExpHistName.compare("None")==0){
    NExp=0;
  }else{
    if (ExpHistBox.find(ExpHistName)==ExpHistBox.end()){
      cout << "Exp-Histogram "<<ExpHistName<<" is not found.\n";
      return -1;
    }
    ExpDim = ExpHistBox[ExpHistName].Dim; 
    if (ExpDim==1 && ExpHistBox[ExpHistName].Hist1DList==NULL){
      cout << "Exp-Histogram "<<ExpHistName<<" is not initialized.\n";
      return -1;
    }else{
      Hist1DListExp = ExpHistBox[ExpHistName].Hist1DList;
    }
    if (ExpDim==2 && ExpHistBox[ExpHistName].Hist2DList==NULL){
      cout << "Exp-Histogram "<<ExpHistName<<" is not initialized.\n";
      return -1;
    }else{
      Hist2DListExp = ExpHistBox[ExpHistName].Hist2DList;
    }
  }
  //Set Sim Histograms
  if (SimHistName.compare("None")==0){
    NSim=0;
  }else{
    if (SimHistBox.find(SimHistName)==SimHistBox.end()){
      cout << "Sim-Histogram "<<SimHistName<<" is not found.\n";
      return -1;
    }
    SimDim = SimHistBox[SimHistName].Dim; 
    if (SimDim==1 && SimHistBox[SimHistName].Hist1DList==NULL){
      cout << "Sim-Histogram "<<SimHistName<<" is not initialized.\n";
      return -1;
    }else{
      Hist1DListSim = SimHistBox[SimHistName].Hist1DList;
    }
    if (SimDim==2 && SimHistBox[SimHistName].Hist2DList==NULL){
      cout << "Sim-Histogram "<<SimHistName<<" is not initialized.\n";
      return -1;
    }else{
      Hist2DListSim = SimHistBox[SimHistName].Hist2DList;
    }
  }
/*
  if (SimDim != ExpDim){
    cout << "Dimension mismatch between Exp and Sim."<<endl;
    return -1;
  }
*/
  //Start plotting
  gStyle->SetOptStat("");
  if (Style==0){
    if (ExpDim==1){
      TCanvas* c1 = new TCanvas("c1","c1",0,0,1024,768);
      TLegend* Leg = new TLegend(0.65,0.78,0.865,0.89);
      int HistCounts = 0;
      double Norm;
      if (ExpHistName.compare("None")!=0){
      Norm = Hist1DListExp[0][0]->Integral("width");
      }else Norm = Hist1DListSim[0][0]->Integral("width");
      if (ExpHistName.compare("None")!=0){
	for (int i=0;i<NExp;i++){
	  if (HistCounts <9) Hist1DListExp[i][0]->SetLineColor(HistCounts+1);
	  else Hist1DListExp[i][0]->SetLineColor(HistCounts+2);
	  if (Normalize)Hist1DListExp[i][0]->Scale(Norm/Hist1DListExp[i][0]->Integral("width"));
	  if (HistCounts==0)Hist1DListExp[i][0]->Draw();
	  else Hist1DListExp[i][0]->Draw("same");
	  Leg->AddEntry(Hist1DListExp[i][0],Form("Exp.%d",i),"l");
	  HistCounts++;
	}
      }
      if (SimHistName.compare("None")!=0){
	for (int i=0;i<NSim;i++){
	  if (HistCounts <9) Hist1DListSim[i][0]->SetLineColor(HistCounts+1);
	  else Hist1DListSim[i][0]->SetLineColor(HistCounts+2);
	  if (Normalize)Hist1DListSim[i][0]->Scale(Norm/Hist1DListSim[i][0]->Integral("width"));
	  if (HistCounts==0)Hist1DListSim[i][0]->Draw();
	  else Hist1DListSim[i][0]->Draw("same");
	  Leg->AddEntry(Hist1DListSim[i][0],Form("Sim.%d",i),"l");
	  HistCounts++;
	}
      }
      Leg->Draw();
      string OutputFileName = SPECTRA_DIRECTORY + "/UserCompareSpectra_"+ label + ".pdf";
      c1->SaveAs(OutputFileName.c_str());
      //Save canvas to root
      OutputFileName = HISTOGRAM_DIRECTORY + "/UserCompareSpectra_"+ label + ".root";
      TFile *canvasfile = new TFile(OutputFileName.c_str(),"recreate");
      c1->Write();
      canvasfile->Close();
      delete canvasfile;
    }else if (ExpDim==2){
      TCanvas* c1 = new TCanvas("c1","c1",0,0,2048,768);
      c1->Divide(2);
      c1->cd(1);
      //X Projection
      TLegend* LegX = new TLegend(0.65,0.78,0.865,0.89);
      int HistCounts = 0;
      double Norm;
      if (ExpHistName.compare("None")!=0) Norm = Hist2DListExp[0][0]->Integral("width");
      else Norm = Hist2DListSim[0][0]->Integral("width");
      if (ExpHistName.compare("None")!=0){
	for (int i=0;i<NExp;i++){
	  TH1* histtemp = (TH1*)(Hist2DListExp[i][0]->ProjectionX()->Clone());
	  histtemp->SetLineColor(HistCounts+1);
	  if (Normalize)histtemp->Scale(Norm/histtemp->Integral("width"));
	  if (HistCounts==0)histtemp->Draw();
	  else histtemp->Draw("same");

	  LegX->AddEntry(histtemp,Form("Exp.%d",i),"l");
	  HistCounts++;
	}
      }
      if (SimHistName.compare("None")!=0){
	for (int i=0;i<NSim;i++){
	  TH1* histtemp = (TH1*)(Hist2DListSim[i][0]->ProjectionX()->Clone());
	  histtemp->SetLineColor(HistCounts+1);
	  if (Normalize)histtemp->Scale(Norm/histtemp->Integral("width"));
	  if (HistCounts==0)histtemp->Draw();
	  else histtemp->Draw("same");
	  LegX->AddEntry(histtemp,Form("Sim.%d",i),"l");
	  HistCounts++;
	}
      }
      LegX->Draw();
      //Y Projection
      c1->cd(2);
      TLegend* LegY = new TLegend(0.65,0.78,0.865,0.89);
      HistCounts = 0;
      Norm = Hist2DListExp[0][0]->Integral("width");
      if (ExpHistName.compare("None")!=0){
	for (int i=0;i<NExp;i++){
	  TH1* histtemp = (TH1*)(Hist2DListExp[i][0]->ProjectionY()->Clone());
	  histtemp->SetLineColor(HistCounts+1);
	  if (Normalize)histtemp->Scale(Norm/histtemp->Integral("width"));
	  if (HistCounts==0)histtemp->Draw();
	  else histtemp->Draw("same");
	  LegY->AddEntry(histtemp,Form("Exp.%d",i),"l");
	  HistCounts++;
	}
      }
      if (SimHistName.compare("None")!=0){
	for (int i=0;i<NSim;i++){
	  TH1* histtemp = (TH1*)(Hist2DListSim[i][0]->ProjectionY()->Clone());
	  histtemp->SetLineColor(HistCounts+1);
	  if (Normalize)histtemp->Scale(Norm/histtemp->Integral("width"));
	  if (HistCounts==0)histtemp->Draw();
	  else histtemp->Draw("same");
	  LegY->AddEntry(histtemp,Form("Sim.%d",i),"l");
	  HistCounts++;
	}
      }
      LegY->Draw();
      string OutputFileName = SPECTRA_DIRECTORY + "/UserCompareSpectra_"+ label + ".pdf";
      c1->SaveAs(OutputFileName.c_str());
      //Save canvas to root
      OutputFileName = HISTOGRAM_DIRECTORY + "/UserCompareSpectra_"+ label + ".root";
      TFile *canvasfile = new TFile(OutputFileName.c_str(),"recreate");
      c1->Write();
      canvasfile->Close();
      delete canvasfile;
    }
  }else if(Style==1){
    if (ExpDim==1){
      //Exp
      TCanvas* c1 = new TCanvas("c1","c1",0,0,1024,768);
      TLegend* Leg1 = new TLegend(0.65,0.78,0.865,0.89);
      int HistCounts = 0;
      double Norm;
      if (ExpHistName.compare("None")!=0) Norm = Hist1DListExp[0][0]->Integral("width");
      else Norm=0;
      if (ExpHistName.compare("None")!=0){
	for (int i=0;i<NExp;i++){
	  Hist1DListExp[i][0]->SetLineColor(HistCounts+1);
	  if (Normalize)Hist1DListExp[i][0]->Scale(Norm/Hist1DListExp[i][0]->Integral("width"));
	  if (HistCounts==0)Hist1DListExp[i][0]->Draw();
	  else Hist1DListExp[i][0]->Draw("same");
	  Leg1->AddEntry(Hist1DListExp[i][0],Form("Exp.%d",i),"l");
	  HistCounts++;
	}
      }
      Leg1->Draw();
      string OutputFileName = SPECTRA_DIRECTORY + "/UserCompareSpectra_"+ label + ".pdf(";
      c1->SaveAs(OutputFileName.c_str());
      //Sim
      TCanvas* c2 = new TCanvas("c2","c2",0,0,1024,768);
      TLegend* Leg2 = new TLegend(0.65,0.78,0.865,0.89);
      HistCounts = 0;
      if (SimHistName.compare("None")!=0) Norm = Hist1DListSim[0][0]->Integral("width");
      else Norm=0;
      if (SimHistName.compare("None")!=0){
	for (int i=0;i<NSim;i++){
	  Hist1DListSim[i][0]->SetLineColor(HistCounts+1);
	  if (Normalize)Hist1DListSim[i][0]->Scale(Norm/Hist1DListSim[i][0]->Integral("width"));
	  if (HistCounts==0)Hist1DListSim[i][0]->Draw();
	  else Hist1DListSim[i][0]->Draw("same");
	  Leg2->AddEntry(Hist1DListSim[i][0],Form("Sim.%d",i),"l");
	  HistCounts++;
	}
      }
      Leg2->Draw();
      OutputFileName = SPECTRA_DIRECTORY + "/UserCompareSpectra_"+ label + ".pdf)";
      c2->SaveAs(OutputFileName.c_str());
      //Save canvas to root
      OutputFileName = HISTOGRAM_DIRECTORY + "/UserCompareSpectra_"+ label + ".root";
      TFile *canvasfile = new TFile(OutputFileName.c_str(),"recreate");
      c1->Write();
      c2->Write();
      canvasfile->Close();
      delete canvasfile;
    }else if (ExpDim==2){
      //Exp
      TCanvas* c1 = new TCanvas("c1","c1",0,0,2048,768);
      c1->Divide(2);
      c1->cd(1);
      //X Projection
      TLegend* Leg1X = new TLegend(0.65,0.78,0.865,0.89);
      int HistCounts = 0;
      double Norm;
      if (ExpHistName.compare("None")!=0) Norm = Hist2DListExp[0][0]->Integral("width");
      else Norm = 0;
      if (ExpHistName.compare("None")!=0){
	for (int i=0;i<NExp;i++){
	  TH1* histtemp = (TH1*)(Hist2DListExp[i][0]->ProjectionX()->Clone());
	  histtemp->SetLineColor(HistCounts+1);
	  if (Normalize)histtemp->Scale(Norm/histtemp->Integral("width"));
	  if (HistCounts==0)histtemp->Draw();
	  else histtemp->Draw("same");
	  Leg1X->AddEntry(histtemp,Form("Exp.%d",i),"l");
	  HistCounts++;
	}
      }
      Leg1X->Draw();
      //Y Projection
      c1->cd(2);
      TLegend* Leg1Y = new TLegend(0.65,0.78,0.865,0.89);
      HistCounts = 0;
      if (ExpHistName.compare("None")!=0){
	for (int i=0;i<NExp;i++){
	  TH1* histtemp = (TH1*)(Hist2DListExp[i][0]->ProjectionY()->Clone());
	  histtemp->SetLineColor(HistCounts+1);
	  if (Normalize)histtemp->Scale(Norm/histtemp->Integral("width"));
	  if (HistCounts==0)histtemp->Draw();
	  else histtemp->Draw("same");
	  Leg1Y->AddEntry(histtemp,Form("Exp.%d",i),"l");
	  HistCounts++;
	}
      }
      Leg1Y->Draw();
      string OutputFileName = SPECTRA_DIRECTORY + "/UserCompareSpectra_"+ label + ".pdf(";
      c1->SaveAs(OutputFileName.c_str());
      //Sim
      TCanvas* c2 = new TCanvas("c2","c2",0,0,2048,768);
      c2->Divide(2);
      c2->cd(1);
      //X Projection
      TLegend* Leg2X = new TLegend(0.65,0.78,0.865,0.89);
      HistCounts = 0;
      if (SimHistName.compare("None")!=0) Norm = Hist2DListSim[0][0]->Integral("width");
      else Norm = 0;
      if (SimHistName.compare("None")!=0){
	for (int i=0;i<NSim;i++){
	  TH1* histtemp = (TH1*)(Hist2DListSim[i][0]->ProjectionX()->Clone());
	  histtemp->SetLineColor(HistCounts+1);
	  if (Normalize)histtemp->Scale(Norm/histtemp->Integral("width"));
	  if (HistCounts==0)histtemp->Draw();
	  else histtemp->Draw("same");
	  Leg2X->AddEntry(histtemp,Form("Sim.%d",i),"l");
	  HistCounts++;
	}
      }
      Leg2X->Draw();
      //Y Projection
      c2->cd(2);
      TLegend* Leg2Y = new TLegend(0.65,0.78,0.865,0.89);
      HistCounts = 0;
      if (SimHistName.compare("None")!=0){
	for (int i=0;i<NSim;i++){
	  TH1* histtemp = (TH1*)(Hist2DListSim[i][0]->ProjectionY()->Clone());
	  histtemp->SetLineColor(HistCounts+1);
	  if (Normalize)histtemp->Scale(Norm/histtemp->Integral("width"));
	  if (HistCounts==0)histtemp->Draw();
	  else histtemp->Draw("same");
	  Leg2Y->AddEntry(histtemp,Form("Sim.%d",i),"l");
	  HistCounts++;
	}
      }
      Leg2Y->Draw();
      OutputFileName = SPECTRA_DIRECTORY + "/UserCompareSpectra_"+ label + ".pdf)";
      c2->SaveAs(OutputFileName.c_str());
      //Save canvas to root
      OutputFileName = HISTOGRAM_DIRECTORY + "/UserCompareSpectra_"+ label + ".root";
      TFile *canvasfile = new TFile(OutputFileName.c_str(),"recreate");
      c1->Write();
      c2->Write();
      canvasfile->Close();
      delete canvasfile;
    }
  }else if(Style==2){
    if (ExpHistName.compare("None")==0 || SimHistName.compare("None")==0){
      cout <<"In Style 2, both Exp and Sim are needed."<<endl;
      return -1;
    }
    if (NExp!=NSim){
      cout <<"Histogram numbers of Exp and Sim need to match in Style 2!"<<endl;
      return -1;
    }
    if (ExpDim==1){
      //Save canvas to root
      string canvasFileName = HISTOGRAM_DIRECTORY + "/UserCompareSpectra_"+ label + ".root";
      TFile *canvasfile = new TFile(canvasFileName.c_str(),"recreate");
      for (int i=0;i<NExp;i++){
	TCanvas* c1 = new TCanvas(Form("c%d",i),Form("c%d",i),0,0,1024,768);
	TLegend* Leg = new TLegend(0.65,0.78,0.865,0.89);
	double Norm = Hist1DListExp[i][0]->Integral("width");
	Hist1DListExp[i][0]->SetLineColor(kBlue);
	Hist1DListExp[i][0]->Draw();
	Hist1DListSim[i][0]->SetLineColor(kRed);
	if (Normalize)Hist1DListSim[i][0]->Scale(Norm/Hist1DListSim[i][0]->Integral("width"));
	Hist1DListSim[i][0]->Draw("same");

	Leg->AddEntry(Hist1DListExp[i][0],Form("Exp.%d",i),"l");
	Leg->AddEntry(Hist1DListSim[i][0],Form("Sim.%d",i),"l");
	Leg->Draw();
	string OutputFileName;
	if (i==0)OutputFileName = SPECTRA_DIRECTORY + "/UserCompareSpectra_"+ label + ".pdf(";
	else if (i==NExp-1)OutputFileName = SPECTRA_DIRECTORY + "/UserCompareSpectra_"+ label + ".pdf)";
	else OutputFileName = SPECTRA_DIRECTORY + "/UserCompareSpectra_"+ label + ".pdf";
	c1->SaveAs(OutputFileName.c_str());
	c1->Write();
	delete c1;
      }
      canvasfile->Close();
      delete canvasfile;
    }else if (ExpDim==2){
      //Save canvas to root
      string canvasFileName = HISTOGRAM_DIRECTORY + "/UserCompareSpectra_"+ label + ".root";
      TFile *canvasfile = new TFile(canvasFileName.c_str(),"recreate");
      for (int i=0;i<NExp;i++){
	TH1* histtemp1;
	TH1* histtemp2;
	TCanvas* c1 = new TCanvas(Form("c%d",i),Form("c%d",i),0,0,2048,768);
	c1->Divide(2);
	c1->cd(1);
	TLegend* LegX = new TLegend(0.65,0.78,0.865,0.89);
	histtemp1 =  (TH1*)(Hist2DListExp[i][0]->ProjectionX()->Clone());
	histtemp2 =  (TH1*)(Hist2DListSim[i][0]->ProjectionX()->Clone());
	double Norm = histtemp1->Integral("width");
	histtemp1->SetLineColor(kBlue);
	histtemp1->Draw();
	histtemp2->SetLineColor(kRed);
	if (Normalize)histtemp2->Scale(Norm/histtemp2->Integral("width"));
	histtemp2->Draw("same");

	LegX->AddEntry(histtemp1,Form("Exp.%d",i),"l");
	LegX->AddEntry(histtemp2,Form("Sim.%d",i),"l");
	LegX->Draw();

	c1->cd(2);
	TLegend* LegY = new TLegend(0.65,0.78,0.865,0.89);
	histtemp1 =  (TH1*)(Hist2DListExp[i][0]->ProjectionY()->Clone());
	histtemp2 =  (TH1*)(Hist2DListSim[i][0]->ProjectionY()->Clone());
	Norm = histtemp1->Integral("width");
	histtemp1->SetLineColor(kBlue);
	histtemp1->Draw();
	histtemp2->SetLineColor(kRed);
	if (Normalize)histtemp2->Scale(Norm/histtemp2->Integral("width"));
	histtemp2->Draw("same");

	LegY->AddEntry(histtemp1,Form("Exp.%d",i),"l");
	LegY->AddEntry(histtemp2,Form("Sim.%d",i),"l");
	LegY->Draw();

	string OutputFileName;
	if (i==0 && i!=(NExp-1))OutputFileName = SPECTRA_DIRECTORY + "/UserCompareSpectra_"+ label + ".pdf(";
	else if (i==NExp-1 && i!=0)OutputFileName = SPECTRA_DIRECTORY + "/UserCompareSpectra_"+ label + ".pdf)";
	else OutputFileName = SPECTRA_DIRECTORY + "/UserCompareSpectra_"+ label + ".pdf";
	c1->SaveAs(OutputFileName.c_str());
	c1->Write();
	delete c1;
      }
      canvasfile->Close();
      delete canvasfile;
    }
  }else{
    cout << "Non-supported style."<<endl;
    return -1;
  }
  return 0;
}
/**********************************************************************************************/
int Analyzer::User_PhotoIonTOFStudy(double Alpha_low, double Alpha_interval, double E0, double E0err,string label)
{
  if (!ExpHistBox["TOFHist"].Enabled){
    cout<< "Error: TOFHist not enabled! Check ReadMode!"<<endl;
    return -1;
  }
  //Initialization
  string GraphName;
  string XTitle,YTitle,ZTitle;
  int nFile1,nFile2;
  double z_fit;
  double mass = MASS_HE4;
  int charge_state = 1;
  double Q = (1e-6)*charge_state*CSPEED*CSPEED/mass;                    //q/m [mm/ns]^2/V
  E0 = E0*100; //convert to V/mm
  char IDstr[100];
  int returnVal;

  //Init Graph
  TGraphErrors* Graph = new TGraphErrors();
  Graph->SetName("EFieldScaling");
  Graph->SetTitle("TOF vs EFieldScaling");
  int NGraph=0;
  AnalysisLog << "\n\nPhotoIonTOFStudy:\n***************************************\n";
  //Fit and get peaks
  for (int i=0;i<nFileExp1;i++){
    TH1* hist = ExpHistBox["TOFHist"].Hist1DList[i][0];
    int MaxBin = hist->GetMaximumBin();
    double Max = hist->GetBinCenter(MaxBin);
    hist->Fit("gaus","","",Max-5.0,Max+5.0);
    double Peak = hist->GetFunction("gaus")->GetParameter(1);
    double PeakError = hist->GetFunction("gaus")->GetParError(1);
    Graph->SetPoint(NGraph,Alpha_low+i*Alpha_interval,Peak);
    Graph->SetPointError(NGraph,0.00,PeakError);
    NGraph++;
    
    AnalysisLog <<"**********Fit Result:**********\n";
    AnalysisLog << "k = " <<Alpha_low+i*Alpha_interval<<endl;
    AnalysisLog << "TOF = " <<Peak << " +/- "<<PeakError<<endl;
    AnalysisLog << "Chi2 = " << hist->GetFunction("gaus")->GetChisquare()<<" : Dof = "<<hist->GetFunction("gaus")->GetNDF() <<endl;
  }

  //Fit Graph
  TF1* FitFunc = new TF1("FitFunc","[0]+[1]/sqrt(x)",0,10);
  Graph->Fit(FitFunc,"","",0,7);
  Graph->SetMarkerStyle(21);
  cout << Graph->GetFunction("FitFunc")->GetChisquare()/double(nFileExp1-2)<<endl;
  double t0 = Graph->GetFunction("FitFunc")->GetParameter(0);
  double d_t0 = Graph->GetFunction("FitFunc")->GetParError(0);
  double k = Graph->GetFunction("FitFunc")->GetParameter(1);
  double d_k = Graph->GetFunction("FitFunc")->GetParError(1);
  double z0_fit = .5*Q*E0*k*k;
  double z0_err = sqrt(pow(d_k*k*E0*Q,2) + pow(z0_fit*E0err,2));
  cout<<"Determined MCP-MOT distance = "<< z0_fit<<"+/-"<< z0_err<<" [mm]"<<endl;
  cout<< "E0 = " << E0<<"+/-"<< E0err*E0<<" [V/mm]"<<endl;
  cout << "T0 = " << t0<<"+/-"<< d_t0<<" [ns]"<<endl;

  AnalysisLog << "\nSystematic study inverse square root fit:\n*************************************** "<<endl;
  AnalysisLog << "alpha = " <<k <<"+/-"<<d_k  <<endl;
  AnalysisLog << "t0 = " << t0<<"+/-"<<d_t0 <<" [ns]"<<endl;
  AnalysisLog << "Chi2 = " << FitFunc->GetChisquare()/FitFunc->GetNDF() <<endl;
// AnalysisLog << "z0_Fit = " << z0_fit<<"+/-"<< z0_err<<" [mm]"<<endl;
//  AnalysisLog << "E0 = " << E0<<"+/-"<< E0err*E0<<" [V/mm]"<<endl;
//  AnalysisLog << "T0 = " << t0<<"+/-"<< d_t0<<" [ns]"<<endl;

  //Output
  string filename;
  filename = HISTOGRAM_DIRECTORY + "/UserHist_PhotoIonTOF" + label + ".root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");
  for (int i=0;i<nFileExp1;i++){
    ExpHistBox["TOFHist"].Hist1DList[i][0]->Write();
  }
  Graph->Write();
  OutFile->Close();
  delete OutFile;

  return 0;
}
/**********************************************************************************************/
int Analyzer::User_LocalRateTOFStudy(string PhotoIonInput, string PenningIonInput,string title)
{
  int N, i, k;
  int *I,*J;
  int zero = 0;
  double *PenningIonRate;
  double *PenningIonRateError;
  double *RunTime;
  //MCP Image hist bin parameters
  double xlo = -5;//mm
  double ylo = -5;//mm
  double xhi = 5;//mm
  double yhi = 5;//mm
  int nxbin = 1000;
  int nybin = 1000;
  double dx = (xhi-xlo)/nxbin;
  double dy = (yhi-ylo)/nybin;
  double dxdy = dx*dy; 
 
  //Load PenningIonFile
  SetMode("MCP");
  LoadOutTrees(PenningIonInput.c_str());

  if (!OutTreesInitialized){
    cout<<"Error: Output trees are not initialized! Load before running LocalRateTOFStudy function."<<endl;
    return -1;
  }

  i = 0;
  if (HistDim1>1 && HistDim2>1){
    cout<<"Warning: Both hist dimensions >1. Choosing HistDim1 by default."<<endl;
    N = HistDim1;
    I = &i;
    J = &zero;
  }else if(HistDim1>HistDim2){
    N = HistDim1;
    I = &i;
    J = &zero;
  }else{
    N = HistDim2;
    J = &i;
    I = &zero;
  }


  PenningIonRate = new double[N];//compute
  PenningIonRateError = new double[N];//compute
  RunTime = new double[N];
  
  //Output
  string filename,fitResults;
  filename = HISTOGRAM_DIRECTORY + "/UserHist_LocalRateTOFStudy" + title + ".root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");
  //MCP image hist list 
  TH2D** hist2Dlist = new TH2D*[N];
  //TH2D** hres2Dlist = new TH2D*[N];
  TH2D* myhtemp = new TH2D("myhtemp","MCP Image",nxbin,xlo,xhi,nybin,ylo,yhi);
  myhtemp->GetXaxis()->SetTitle("X [mm]");
  myhtemp->GetYaxis()->SetTitle("Y [mm]");
  //For each run, draw local rate hists
  for (i=0;i<N;i++){
    //draw contents into histogram (Y:X)
    OutTrees[*I][*J]->Draw("MCPHitPosAA.Y:MCPHitPosAA.X>>myhtemp");
    RunTime[i] = OutTrees[*I][*J]->GetMaximum("GroupTime") - OutTrees[*I][*J]->GetMinimum("GroupTime");
    PenningIonRate[i] = myhtemp->GetEntries()/RunTime[i];
    PenningIonRateError[i] = sqrt(PenningIonRate[i])/RunTime[i];
    myhtemp->Scale(1/dxdy/RunTime[i]);
    stringstream ss;
    ss<<i;
    string HistName = "MCPRate_Cond_"+ss.str(); 
    hist2Dlist[i] =(TH2D*) myhtemp->Clone(HistName.c_str());   
    hist2Dlist[i]->Write("",TObject::kOverwrite);
  }

  //Load PhotoIonFile
  SetMode("Double");
  LoadOutTrees(PhotoIonInput.c_str());
  RecDataStruct RecData;      //Reconstructed data struct
//  SetOutTreesAddress(RecData);//doesn't work for loading in trees again

  ostringstream stm1,stm2;
  stm1 <<"Index:\tTotal Double Events:\tTotal Runtime: [s] \tAverage MCP Total Rate:\tAverage MCP Flux for TOF:\tMCP Flux RMS:\n";
  //stm2 <<"Index:\tTotal Events:\tTotal Runtime: [s] \tAverage Rate:\tParam:\tTOF Fit:\tTOF Fit Err:\n";
  

  OutFile->cd();
  //Create Graph
  TGraph* Graph = new TGraph();
  Graph->SetName("LocalMCPRate_vs_TOF");
  Graph->SetTitle("LocalMCPRate_vs_TOF");
  Graph->SetMarkerStyle(20);
  Graph->SetMarkerSize(1);

  double LocalRate,bin,binx,biny;
  int kprev = 0;
  int Nevents= 0;

//TOF and flux binning
  double ntbin = 50;
  double nfbin = 50;
  double tlo = 135;
  double thi = 141;
  double flo = 0;
  double fhi = 4000;
  
  //Flux vs TOF hist list 
  TH2D** rate2Dlist = new TH2D*[N];
  //TH2D* rate2Dtemp = new TH2D("rate2Dtemp","MCP Flux vs TOF",ntbin,tlo,thi,nfbin,flo,fhi);
  //rate2Dtemp->GetXaxis()->SetTitle("TOF [ns]");
  //rate2Dtemp->GetYaxis()->SetTitle("MCP Flux [Hz*m^-2]");
  
  //Flux histogram  
  TH1D** rate1Dlist = new TH1D*[N];
  TH1D* rate1Dtemp = new TH1D("rate1Dtemp","MCP Flux",nfbin,flo,fhi);
  rate1Dtemp->GetXaxis()->SetTitle("MCP Flux [Hz*mm^-2]");

 

  
  //For each run, draw local rate hists
  for (i=0;i<N;i++){
     Nevents = OutTrees[*I][*J]->GetEntries();
     OutTrees[*I][*J]->SetBranchAddress("MCPHitPosAA",&RecData.MCPPos_X);//in mm
     //OutTrees[*I][*J]->SetBranchAddress("MCPHitPosAA.Y",&RecData.MCPPos_Y);//in mm
     OutTrees[*I][*J]->SetBranchAddress("TOFAA",&RecData.TOF);//in ns //MCP time relative to Scint

    //Make new tree
    stringstream ss;
    ss<<i;
    string tree_name = "LocalRate_Cond_" + ss.str(); 
    TTree rateTree(tree_name.c_str(),tree_name.c_str());
    rateTree.Branch("TOFAA",&RecData.TOF,"TOFAA/D");//in ns //MCP time relative to Scint
    rateTree.Branch("MCPHitPosAA",&RecData.MCPPos_X,"X/D:Y/D");//in mm
    rateTree.Branch("MCPFlux",&LocalRate,"MCPFlux/D"); //Hz/mm^2

     for(k=0;k<Nevents;k++){
        //Caluculate MCP flux for each event
        OutTrees[*I][*J]->GetEntry(k);
        binx =  hist2Dlist[i]->GetXaxis()->FindBin(RecData.MCPPos_X);
        biny =  hist2Dlist[i]->GetYaxis()->FindBin(RecData.MCPPos_Y);
        bin = hist2Dlist[i]->GetBin(binx,biny);
        LocalRate = hist2Dlist[i]->GetBinContent(bin);
        //cout<<LocalRate<<"\t"<<bin<<endl;
        Graph->SetPoint(kprev + k,RecData.TOF,LocalRate);
        
        //fill new tree
        rateTree.Fill();
        
    }    
    rateTree.Draw("MCPFlux>>rate1Dtemp");
    stringstream ss2;
    ss2<<i;
    string rhist_name = "MCPFlux_Cond_" + ss2.str();
    rate1Dlist[i] =(TH1D*) rate1Dtemp->Clone(rhist_name.c_str());   
    rate1Dlist[i]->Write("",TObject::kOverwrite);


    //rateTree.Draw("TOFAA>>toftemp");
    //TH1D *toftemp = gDirectory->Get("toftemp");
    //double tofmean = toftemp->GetMean();
    rateTree.Draw("MCPFlux:TOFAA>>rate2Dtemp");
    string rhist_name2 = "MCPFlux_vs_TOF_Cond_" + ss2.str();
    TH2D *rate2Dtemp =(TH2D*)  gDirectory->Get("rate2Dtemp");
    rate2Dlist[i] =(TH2D*) rate2Dtemp->Clone(rhist_name2.c_str());   
    rate2Dlist[i]->Write("",TObject::kOverwrite);
    gDirectory->Delete("rate2Dtemp");
  
    stm1 <<i<<"\t"<<Nevents<<"\t"<<RunTime[i] <<"\t"<<PenningIonRate[i] <<"\t"<<rate1Dlist[i]->GetMean()<<"\t"<<rate1Dlist[i]->GetRMS()<<"\n";
    //stm2 <<i<<"\t"<<PhotoIonRate[i] <<"\t"<<RunTime <<"\t"<<OriginalPhotoIonRate[i]<<"\t"<<ParArray[i]<<"\t"<<TOFArray[i]<<"\t"<<TOFErrorArray[i]<<"\n";
    
    kprev +=Nevents;
    rateTree.Write();
  }
  cout<< stm1.str() <<endl;
  //cout<< stm2.str() <<endl;
  OutFile->cd();
  Graph->Write(); 

  int colors[14] = {1, 632, 416, 600, 400, 616, 432, 800, 820, 840, 860, 880, 900,920};//basic colorwheel colors

  TCanvas c1("Combined","c1",0,0,800,600);
  TLegend legend(.6,.7,.9,.9);
  for (i=0;i<N;i++){ 
    rate1Dlist[i]->SetLineColor(colors[i]+2);//darker
    rate1Dlist[i]->SetLineWidth(2);
//    rate1Dlist[i]->SetBit(TH1::kCanRebin);
    rate1Dlist[i]->Draw("same");
    legend.AddEntry(rate1Dlist[i],rate1Dlist[i]->GetName(),"l");
  }
  legend.Draw();
  c1.Write();
  //Create Graph

//  TF1* fit1 = (TF1*) Graph->FindObject("pol1");
  //fitResults = StreamFitResults(fit1,3);
  //cout<<fitResults<<endl;
  

  OutFile->Close();
  delete OutFile;

  return 0;
}
/**********************************************************************************************/
int Analyzer::User_QMCPStudy(string PhotoIonInput, string PenningIonInput, string title,string label, double Alpha_low, double Alpha_interval)
{
  
  LoadExpHistograms(PhotoIonInput,"Array");
  double*  QMCP1Array = new double[HistDim1];
	double*  QMCP1ErrorArray = new double[HistDim1];
	double*  QMCP2Array = new double[HistDim1];
	double*  QMCP2ErrorArray = new double[HistDim1];
	double*  ParArray = new double[HistDim1];
  double*  QRatioArray = new double[HistDim1]; 
  double*  QRatioErrorArray = new double[HistDim1];
  double*  QDiffArray = new double[HistDim1]; 
  double*  QDiffErrorArray = new double[HistDim1];

  ostringstream stm1,stm2;
  stm1 <<"Index:\tParam:\tQMPC1 Fit:\tQMCP1 Fit Err:\tQMPC2 Fit:\tQMCP2 Fit Err:\n";
  //stm2 <<"Index:\tTotal Events:\tTotal Runtime: [s] \tAverage Rate:\tParam:\tTOF Fit:\tTOF Fit Err:\n";
  
  //Output
  string filename,fitResults;
  filename = HISTOGRAM_DIRECTORY + "/UserHist_QMCPStudy_" + title + ".root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");
  //Fit and get peaks
  for (int i=0;i<HistDim1;i++){
    TH1* hist = ExpHistBox["MCP_Charge"].Hist1DList[i][0];
    if (hist==NULL){
	  cout << "ERROR: MCP_Charge["<<i<<"][0] was never initialized. Make sure histogram is enabled. ABORTING FIT!"<<endl;
	  if(!ExpHistBox["MCP_Charge"].Enabled) cout<< "Histgoram not enabled."<<endl;
	  return -1;
    }
    TF1* g1 = new TF1("m1","gaus",0,25000);
    TF1* g2 = new TF1("m2","gaus",25000,60000);
    //g3 = new TF1("m3","gaus",110,121);
    // The total is the sum of the three, each has 3 parameters
    TF1* total = new TF1("mstotal","gaus(0)+gaus(3)",0,60000);
    // Define the parameter array for the total function
    Double_t par[6];   // Fit each function and add it to the list of functions
    hist->Fit(g1,"SQR");
    hist->Fit(g2,"SQR");
    //h->Fit(g3,"R+");
    // Get the parameters from the fit
    g1->GetParameters(&par[0]);
    g2->GetParameters(&par[3]);
    //g3->GetParameters(&par[6]);

    // Use the parameters on the sum
    total->SetParameters(par);
    //h->Fit(total,"R+");

//    int MaxBin = hist->GetMaximumBin();
//    double Max = hist->GetBinCenter(MaxBin);
    TFitResultPtr r = hist->Fit(total,"SQR");
    Int_t fitStatus = r;
    if (r<0) {
	  cout << "Error in fit. Histogram maybe empty in fit range. Check timeshift fit range. ABORTING FIT!"<<endl;
	  return -1;
    }
    hist->Write("",TObject::kOverwrite);
    cout<<"Prob = "<<r->Prob()<<"\n";
    r->Print();
    double cov = r->CovMatrix(1,4);
    
    cout<< "covariance: " << r->CovMatrix(1,1)<<"\t"<<r->CovMatrix(4,4)<<"\t"<<r->CovMatrix(1,4)<<endl;
    QMCP1Array[i] = total->GetParameter(1);
    QMCP1ErrorArray[i]= total->GetParError(1);
    QMCP2Array[i]  = total->GetParameter(4);
    QMCP2ErrorArray[i] = total->GetParError(4);
    double amp1 = total->GetParameter(0);
    double width1 = total->GetParameter(2);
    double amp2 = total->GetParameter(3);
    double width2 = total->GetParameter(5);
    QRatioArray[i] = amp2*width2/amp1/width1;
    QRatioErrorArray[i]=TMath::Sqrt(r->CovMatrix(0,0)/total->GetParameter(0)/total->GetParameter(0) +  r->CovMatrix(3,3)/ total->GetParameter(3)/total->GetParameter(3)+ r->CovMatrix(2,2)/ total->GetParameter(2)/total->GetParameter(2)+r->CovMatrix(5,5)/ total->GetParameter(5)/total->GetParameter(5));
    QDiffArray[i] = QMCP1Array[i] - QMCP2Array[i];  
    QDiffErrorArray[i] = TMath::Sqrt(QMCP2ErrorArray[i]*QMCP2ErrorArray[i] + QMCP1ErrorArray[i]*QMCP1ErrorArray[i] - 2*cov);
    ParArray[i]= Alpha_low + i*Alpha_interval;
    stm1 <<i<<"\t"<<ParArray[i]<<"\t"<<QMCP1Array[i]<<"\t"<<QMCP1ErrorArray[i]<<"\t"<<QMCP2Array[i]<<"\t"<<QMCP2ErrorArray[i]<<"\n";
    
  }
  cout<<stm1.str();
  OutFile->cd();
  //Create Graph

  //Set Graph Properties and Write
  TCanvas c1("QMCP1QMCP2","c1",0,0,800,600);
  string GraphName;

  TMultiGraph *mg = new TMultiGraph();
  GraphName = "QMCP Mean_vs_" + label;
  mg->SetName(GraphName.c_str());
  mg->SetTitle(GraphName.c_str());
  
  TGraphErrors* Graph = new TGraphErrors(HistDim1,ParArray,QMCP1Array,NULL,QMCP1ErrorArray);
  GraphName = "QMCP1Mean_vs_" + label;
  Graph->SetName(GraphName.c_str());
  Graph->SetTitle(GraphName.c_str());
  Graph->SetMarkerColor(kRed);
  Graph->GetXaxis()->SetTitle(label.c_str());
  Graph->GetYaxis()->SetTitle("QMCP1 Mean [au]");
  Graph->GetYaxis()->SetTitleOffset(1.5);
  Graph->SetMarkerStyle(21);
  Graph->Fit("pol1");
  Graph->Write();

  TF1* fit1 = (TF1*) Graph->FindObject("pol1");
  fitResults = StreamFitResults(fit1,3);
  cout<<fitResults<<endl;
  
  TGraphErrors* Graph2 = new TGraphErrors(HistDim1,ParArray,QMCP2Array,NULL,QMCP2ErrorArray);
  GraphName = "QMCP2Mean_vs_" + label;
  Graph2->SetName(GraphName.c_str());
  Graph2->SetTitle(GraphName.c_str());
  Graph2->GetXaxis()->SetTitle(label.c_str());
  Graph2->GetYaxis()->SetTitle("QMCP2 Mean [au]");
  Graph2->GetYaxis()->SetTitleOffset(1.5);
  Graph2->SetMarkerStyle(21);
  Graph2->SetMarkerColor(kBlue);
  Graph2->Fit("pol1");
  Graph2->Write();

  fit1 = (TF1*) Graph2->FindObject("pol1");
  fitResults = StreamFitResults(fit1,3);
  cout<<fitResults<<endl;
  
  TGraphErrors* Graph3 = new TGraphErrors(HistDim1,ParArray,QRatioArray,NULL,QRatioErrorArray);
  GraphName = "MCPChargeRatio_vs_" + label;
  Graph3->SetName(GraphName.c_str());
  Graph3->SetTitle(GraphName.c_str());
  Graph3->GetXaxis()->SetTitle(label.c_str());
  Graph3->GetYaxis()->SetTitle("Q2:Q1 Integral [au]");
  Graph3->GetYaxis()->SetTitleOffset(1.5);
  Graph3->SetMarkerStyle(21);
  Graph3->SetMarkerColor(kBlue);
  Graph3->Fit("pol1");
  Graph3->Write();

  fit1 = (TF1*) Graph3->FindObject("pol1");
  fitResults = StreamFitResults(fit1,3);
  cout<<fitResults<<endl;
  
  TGraphErrors* Graph4 = new TGraphErrors(HistDim1,ParArray,QDiffArray,NULL,QDiffErrorArray);
  GraphName = "Q1-Q2_vs_" + label;
  Graph4->SetName(GraphName.c_str());
  Graph4->SetTitle(GraphName.c_str());
  Graph4->GetXaxis()->SetTitle(label.c_str());
  Graph4->GetYaxis()->SetTitle("Q1-Q2 Integral [au]");
  Graph4->GetYaxis()->SetTitleOffset(1.5);
  Graph4->SetMarkerStyle(21);
  Graph4->SetMarkerColor(kBlue);
  Graph4->Fit("pol1");
  Graph4->Write();

  fit1 = (TF1*) Graph4->FindObject("pol1");
  fitResults = StreamFitResults(fit1,3);
  cout<<fitResults<<endl;
  //gPad->SetLeftMargin(.13);
  mg->Add(Graph,"PL");
  mg->Add(Graph2,"PL");
  mg->Draw("A");
  
  auto legend = new TLegend(0.7,0.85,0.9,.95);
  //legend->SetHeader("The Legend Title","C"); // option "C" allows to center the header
  legend->AddEntry(Graph,"QMPC1","lep");
  legend->AddEntry(Graph2,"QMCP2","lep");
  //legend->AddEntry("gr","Graph with error bars","lep");
  legend->Draw();
  
  c1.Write();
  OutFile->Close();
  delete OutFile;

  return 0;
}
/**********************************************************************************************/
int Analyzer::User_PhotoIonRateStudy(string PhotoIonInput, string PenningIonInput,string title)
{
  double *PhotoIonRate;
  double *PhotoIonRateError;
  double *PenningIonRate;
  double *PenningIonRateError;
  double *OriginalPhotoIonRate;
  double *OriginalPhotoIonRateError;
  double *OriginalPenningIonRate;
  double *OriginalPenningIonRateError;
  double *TOFArray;
  double *TOFErrorArray;
  double *ParArray;

  //Load PhotoIonFile
  LoadExpHistograms(PhotoIonInput,"Array");
  int N = HistDim1;
  
  if (nExtPar == 0){
    cout<<"Error: External parameter list not initialized!" <<endl;
    return -1;
  }
  if (nExtPar != N){
    cout<<"Error: External parameter list "<<ExtParName<<" and histogram dimensions do not match!\n nExtPar = "<<nExtPar<<"\n nHist = "<<N<<endl;
    return -1;
  }

  string label = ExtParName;
  
  PhotoIonRate = new double[HistDim1];
  PhotoIonRateError = new double[HistDim1];
  PenningIonRate = new double[HistDim1];
  PenningIonRateError = new double[HistDim1];
  OriginalPhotoIonRate = new double[HistDim1];
  OriginalPhotoIonRateError = new double[HistDim1];
  OriginalPenningIonRate = new double[HistDim1];
  OriginalPenningIonRateError = new double[HistDim1];
  TOFArray = new double[HistDim1];
  TOFErrorArray = new double[HistDim1];
  ParArray = new double[HistDim1];
  //Output
  string filename,fitResults;
  filename = HISTOGRAM_DIRECTORY + "/UserHist_PhotoIonRate" + title + ".root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");
  //Txt Fit output file
  ofstream fout;
  string foutname = HISTOGRAM_DIRECTORY + "/PhotoIonRateStudy" + title + ".txt";
  fout.open(foutname.c_str(),ios::out);

  //Fit and get peaks
  for (int i=0;i<HistDim1;i++){
    TH1* hist = ExpHistBox["TOFHist"].Hist1DList[i][0];
    if (hist==NULL){
	  cout << "ERROR: TOFHist["<<i<<"][0] was never initialized. Make sure histogram is enabled. ABORTING FIT!"<<endl;
	  if(!ExpHistBox["TOFHist"].Enabled) cout<< "Histgoram not enabled."<<endl;
	  return -1;
    }
    int MaxBin = hist->GetMaximumBin();
    double Max = hist->GetBinCenter(MaxBin);
    TFitResultPtr r = hist->Fit("gaus","SQ","",Max-4.0,Max+4.0);
    Int_t fitStatus = r;
    if (r<0) {
	  cout << "Error in fit. Histogram maybe empty in fit range. Check timeshift fit range. ABORTING FIT!"<<endl;
	  return -1;
    }
    hist->Write("",TObject::kOverwrite);
    cout<<"Prob = "<<r->Prob()<<"\n";
    r->Print();
    double Peak = hist->GetFunction("gaus")->GetParameter(1);
    double PeakError = hist->GetFunction("gaus")->GetParError(1);
    TOFArray[i] = Peak;
    TOFErrorArray[i] = PeakError;
    PhotoIonRate[i] = hist->Integral();
    PhotoIonRateError[i] = sqrt(hist->Integral());
    ParArray[i]= ExtParList[i];
  }

  //Load PenningIonFile
  LoadExpHistograms(PenningIonInput,"Array");

  ostringstream stm1;//,stm2;
  stm1 <<"Index:\tTotal MCP Events:\tTotal Double Events:\tTotal Runtime: [s] \tAverage MCP Rate:\tAverage Double Rate:\tParam:\tTOF Fit:\tTOF Fit Err:\n";
  //stm1 <<"Index:\tTotal Events:\tTotal Runtime: [s] \tAverage Rate:\tParam:\tTOF Fit:\tTOF Fit Err:\n";
  //stm2 <<"Index:\tTotal Events:\tTotal Runtime: [s] \tAverage Rate:\tParam:\tTOF Fit:\tTOF Fit Err:\n";
  double LastBin, FirstBin, RunTime;
  for (int i=0;i<HistDim1;i++){
    TH2* hist = ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0];
    TH1* hist_gt = ExpHistBox["GroupTime"].Hist1DList[i][0];
    FirstBin = hist_gt->FindFirstBinAbove();
    LastBin = hist_gt->FindLastBinAbove();
    RunTime = hist_gt->GetBinCenter(LastBin) - hist_gt->GetBinCenter(FirstBin);
    
    PenningIonRate[i] = hist->Integral();
    PenningIonRateError[i] = sqrt(hist->Integral());
 
    OriginalPhotoIonRate[i]=PhotoIonRate[i]/RunTime;
    OriginalPhotoIonRateError[i]=PhotoIonRateError[i]/RunTime;
 
    OriginalPenningIonRateError[i]=PenningIonRateError[i]/RunTime;
    OriginalPenningIonRate[i]=PenningIonRate[i]/RunTime;
    
    //stm1.width(15); would need to call this every time
    stm1 <<i<<"\t"<<PenningIonRate[i] <<"\t"<<PhotoIonRate[i] <<"\t"<<RunTime <<"\t"<<OriginalPenningIonRate[i]<<"\t"<<OriginalPhotoIonRate[i]<<"\t"<<ParArray[i]<<"\t"<<TOFArray[i]<<"\t"<<TOFErrorArray[i]<<"\n";
    //stm1 <<i<<"\t"<<PenningIonRate[i] <<"\t"<<RunTime <<"\t"<<OriginalPenningIonRate[i]<<"\t"<<ParArray[i]<<"\n";
   // stm2 <<i<<"\t"<<PhotoIonRate[i] <<"\t"<<RunTime <<"\t"<<OriginalPhotoIonRate[i]<<"\t"<<ParArray[i]<<"\t"<<TOFArray[i]<<"\t"<<TOFErrorArray[i]<<"\n";
    
    if(PenningIonRate[i]>0 && PhotoIonRate[i]>0){
      double ratio = PhotoIonRateError[i]/PhotoIonRate[i];
      double ratio2 = PenningIonRateError[i]/PenningIonRate[i];
      PhotoIonRate[i] /= PenningIonRate[i];
      PhotoIonRateError[i] = PhotoIonRate[i]*sqrt(ratio*ratio+ratio2*ratio2);
    }
  }
  cout<< stm1.str() <<endl;
  //cout<< stm2.str() <<endl;
  fout<<stm1.str() <<endl;
  fout.close();
  OutFile->cd();
  //Create Graph
  /*TGraphErrors* Graph = new TGraphErrors(HistDim1,TOFArray,PhotoIonRate,TOFErrorArray,PhotoIonRateError);
  Graph->SetName("PhotoIonRate_vs_TOF");
  Graph->SetTitle("PhotoIonRate_vs_TOF");
  Graph->SetMarkerStyle(21);
  Graph->Fit("gaus");
  Graph->Write();

  TF1* fit1 = (TF1*) Graph->FindObject("gaus");
  */

  string GraphName;

  TGraphErrors* Graph = new TGraphErrors(HistDim1,ParArray,TOFArray,NULL,TOFErrorArray);
  GraphName = "TOF_vs_" + label;
  Graph->SetName(GraphName.c_str());
  Graph->SetTitle(GraphName.c_str());
  Graph->GetXaxis()->SetTitle(label.c_str());
  Graph->GetYaxis()->SetTitle("TOF [ns]");
  Graph->GetYaxis()->SetTitleOffset(1.5);
  Graph->SetMarkerStyle(21);
  Graph->Fit("pol1");
  Graph->Write();

  TF1* fit1 = (TF1*) Graph->FindObject("pol1");
  fitResults = StreamFitResults(fit1,3);
  cout<<fitResults<<endl;
  
  TGraphErrors* Graph2 = new TGraphErrors(HistDim1,ParArray,PhotoIonRate,NULL,PhotoIonRateError);
  GraphName = "NormalizedPhotoIonRate_vs_" + label;
  Graph2->SetName(GraphName.c_str());
  Graph2->SetTitle(GraphName.c_str());
  Graph2->GetXaxis()->SetTitle(label.c_str());
  Graph2->GetYaxis()->SetTitle("Photoion Rate Ratio");
  Graph2->GetYaxis()->SetTitleOffset(1.5);
  Graph2->SetMarkerStyle(21);
  Graph2->Fit("gaus");
  Graph2->Write();
  
  TGraphErrors* Graph3 = new TGraphErrors(HistDim1,ParArray,OriginalPhotoIonRate,NULL,OriginalPhotoIonRateError);
  GraphName = "PhotoIonRate_vs_" + label;
  Graph3->SetName(GraphName.c_str());
  Graph3->SetTitle(GraphName.c_str());
  Graph3->GetXaxis()->SetTitle(label.c_str());
  Graph3->GetYaxis()->SetTitle("Photoion Rate");
  Graph3->GetYaxis()->SetTitleOffset(1.5);
  Graph3->SetMarkerStyle(21);
  Graph3->Write();
  
  TGraphErrors* Graph4 = new TGraphErrors(HistDim1,ParArray,OriginalPenningIonRate,NULL,OriginalPenningIonRateError);
  GraphName = "PenningIonRate_vs_" + label;
  Graph4->SetName(GraphName.c_str());
  Graph4->SetTitle(GraphName.c_str());
  Graph4->GetXaxis()->SetTitle(label.c_str());
  Graph4->GetYaxis()->SetTitle("Penningion Rate");
  Graph4->GetYaxis()->SetTitleOffset(1.5);
  Graph4->SetMarkerStyle(21);
  Graph4->Write();

  TF1* fit2 = (TF1*) Graph2->FindObject("gaus");
  fitResults = StreamFitResults(fit2,3);
  cout<<fitResults<<endl;
  OutFile->Close();
  delete OutFile;

  return 0;
}
/**********************************************************************************************/
int Analyzer::User_LaserSwitchStartStudy(string Histname, string title, string label, double Alpha_low, double Alpha_interval)
{
  double *PhotoIonRate;
  double *PhotoIonRateError;
  double *TOFArray;
  double *TOFErrorArray;
  double *ParArray;

  //Load PhotoIonFile
  LoadExpHistograms(Histname,"Array");

  PhotoIonRate = new double[HistDim1];
  PhotoIonRateError = new double[HistDim1];
  TOFArray = new double[HistDim1];
  TOFErrorArray = new double[HistDim1];
  ParArray = new double[HistDim1];
  //Fit and get peaks
  for (int i=0;i<HistDim1;i++){
    TH1* hist = ExpHistBox["TOFHist"].Hist1DList[i][0];
    if (hist==NULL){
	  cout << "ERROR: TOFHist["<<i<<"][0] was never initialized. Make sure histogram is enabled. ABORTING FIT!"<<endl;
	  if(!ExpHistBox["TOFHist"].Enabled) cout<< "Histgoram not enabled."<<endl;
	  return -1;
    }
    int MaxBin = hist->GetMaximumBin();
    double Max = hist->GetBinCenter(MaxBin);
    TFitResultPtr r = hist->Fit("gaus","","",Max-2.0,Max+2.0);
    Int_t fitStatus = r;
    if (r<0) {
	  cout << "Error in fit. Histogram maybe empty in fit range. Check timeshift fit range. ABORTING FIT!"<<endl;
	  return -1;
    }
    double Peak = hist->GetFunction("gaus")->GetParameter(1);
    double PeakError = hist->GetFunction("gaus")->GetParError(1);
    TOFArray[i] = Peak;
    TOFErrorArray[i] = PeakError;
    PhotoIonRate[i] = hist->Integral();
    PhotoIonRateError[i] = sqrt(hist->Integral());
    ParArray[i]= Alpha_low + i*Alpha_interval;
  }


  //Output
  string filename,GraphName;
  filename = HISTOGRAM_DIRECTORY + "/UserHist_LaserSwitchStartStudy" + title + ".root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");

  //Create Graph
  TGraphErrors* Graph = new TGraphErrors(HistDim1,ParArray,TOFArray, NULL,TOFErrorArray);
  GraphName = "TOF_vs_" + label;
  Graph->SetName(GraphName.c_str());
  Graph->SetTitle(GraphName.c_str());
  Graph->SetMarkerStyle(21);
  Graph->Fit("gaus");
  Graph->Write();

  TGraphErrors* Graph2 = new TGraphErrors(HistDim1,ParArray,PhotoIonRate,NULL,PhotoIonRateError);
  GraphName = "IonCounts_vs_" + label;
  Graph2->SetName(GraphName.c_str());
  Graph2->SetTitle(GraphName.c_str());
  Graph2->SetMarkerStyle(21);
  //Graph2->Fit("gaus");
 // TF1* sfit = new TF1("sfit", "[0]*cos([1]*x+[2])+[3]",ParArray[0],ParArray[HistDim1-1]);
  TF1* sfit = new TF1("sfit", TriangleWave,ParArray[0],ParArray[HistDim1-1],4);
  //sfit->SetParameters(.5*(Graph2->GetMaximum()),2*PI/SwitchPeriod,0,.5*(Graph2->GetMaximum()));
  //sfit->FixParameter(1,2*PI/SwitchPeriod);
  sfit->SetParameters(.5*(Graph2->GetMaximum()),SwitchPeriod,0,.5*(Graph2->GetMaximum()));
 
  sfit->FixParameter(1,SwitchPeriod);
  // sfit->SetParLimits(2,-PI,PI);
 // sfit->SetParLimits(0,0,2*(Graph2->GetMaximum()));
  Graph2->Fit("sfit");
  if (sfit->GetParameter(0)<0) {
    sfit->FixParameter(0,-sfit->GetParameter(0));
    Graph2->Fit("sfit");
  }
  gStyle->SetOptFit(1111);
  cout<<"Period = "<<sfit->GetParameter(1)<<endl;
  cout<<"Phase = "<<fmod(sfit->GetParameter(2),sfit->GetParameter(1))<<endl;
  cout<<"Amplitude = "<<sfit->GetParameter(0)/2.0<<endl;
  cout<<"Constant = "<<sfit->GetParameter(3)<<endl;
  cout<<"tmax = "<<-fmod(sfit->GetParameter(2),sfit->GetParameter(1))<<endl;
  cout<<"SwitchStart = n*Period + tmax"<<endl;
  Graph2->Write();

  OutFile->Close();
  delete OutFile;

  return 0;
}
/**********************************************************************************************/
//Double_t Analyzer::User_TriangleWave(Double_t *x,  Double_t *par)
double TriangleWave(double *x,double *p)
{
  double xx = x[0];
  //double return_val = par[0]*(par[1]*xx+par[2]);
  double return_val = 2*p[0]/p[1]*(fabs(fmod(xx+p[2],p[1])-.5*p[1])-.25*p[1])+p[3];
  return return_val;
}
/**********************************************************************************************/
int Analyzer::User_TOFFit(string title, string label, double Alpha_low, double Alpha_interval)
{
  int N, i;
  int *I,*J;
  int zero = 0;
  double par;
  if (ExpStatus!=2){
    cout<<"Error: Experiment histograms not loaded! Load before running UserTOFFit function."<<endl;
    return -1;
  }
  i = 0;
  if (HistDim1>1 && HistDim2>1){
    cout<<"Warning: Both hist dimensions >1. Choosing HistDim1 by default."<<endl;
    N = HistDim1;
    I = &i;
    J = &zero;
  }else if(HistDim1>HistDim2){
    N = HistDim1;
    I = &i;
    J = &zero;
  }else{
    N = HistDim2;
    J = &i;
    I = &zero;
  }
  
  //Output
  string filename,fitResults;
  filename = HISTOGRAM_DIRECTORY + "/UserHist_TOFFit_" + title + ".root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");

  TH1D** hist1Dlist = new TH1D*[N];
  TF1* gaus1D = new TF1("TOFfit","gaus");

  string GraphName;
  TGraphErrors* gTOF = new TGraphErrors(N);
  GraphName = "TOF_vs_" + label;
  gTOF->SetName(GraphName.c_str());
  gTOF->SetTitle(GraphName.c_str());

  TGraphErrors* gWidthTOF = new TGraphErrors(N);
  GraphName = "TOFWidth_vs_" + label;
  gWidthTOF->SetName(GraphName.c_str());
  gWidthTOF->SetTitle(GraphName.c_str());

  //Fit and get peaks
  for (i=0;i<N;i++){
    hist1Dlist[i] = (TH1D*)(ExpHistBox["TOFHist"].Hist1DList[*I][*J]->Clone());
    if (hist1Dlist[i]==NULL){
	  cout << "ERROR: TOFHist["<<*I<<"]["<<*J<<"] was never initialized. Make sure histogram is enabled. ABORTING FIT!"<<endl;
	  if(!ExpHistBox["TOFHist"].Enabled) cout<< "Histogram not enabled."<<endl;
	  return -1;
    }

    int MaxBin = hist1Dlist[i]->GetMaximumBin();
    double Max = hist1Dlist[i]->GetBinCenter(MaxBin);
    TFitResultPtr r = hist1Dlist[i]->Fit("TOFfit","SQ","",Max-4.0,Max+4.0);
    Int_t fitStatus = r;
    if (r<0) {
	  cout << "Error in fit. Histogram maybe empty in fit range. ABORTING FIT!"<<endl;
	  return -1;
    }
   // gStyle->SetOptStat(1111111);
    gStyle->SetOptFit(1);
    hist1Dlist[i]->Write("",TObject::kOverwrite);
    cout<<"\n\nProb = "<<r->Prob()<<"\n";
    r->Print();
    //Fill Graphs
    par = Alpha_low+i*Alpha_interval;
    gTOF->SetPoint(i,par,r->Parameter(1));
    gTOF->SetPointError(i,0,r->ParError(1));
    gWidthTOF->SetPoint(i,par,r->Parameter(2));
    gWidthTOF->SetPointError(i,0,r->ParError(2));
  }
//  //Compute Allan Deviate
//  double sig_AD2, sig_AD; //allan deviate
//  double sum = 0;
//  double M = N; //number of points
//  double y1,y0,x1,x0;
//  
//  for (i=0;i<N-1;i++){
//		gTOF->GetPoint(i+1,x1,y1);
//		gTOF->GetPoint(i,x0,y0);
//  	sum = sum + (y1-y0)*(y1-y0);
//  }
//  sig_AD2 = sum/2/(M-1);
//  sig_AD = sqrt(sig_AD2);
//  cout << "\n"<<sig_AD<<endl;
  
  //Set Graph Properties and Write
  TCanvas c1("Combined","c1",0,0,2*800,600);
  c1.Divide(2,1);
  
  c1.cd(1);
  gTOF->SetMarkerStyle(21);
  gTOF->SetMarkerColor(kBlue);
  gTOF->GetXaxis()->SetTitle(label.c_str());
  gTOF->GetYaxis()->SetTitle("TOF [ns]");
  gTOF->Draw();
  gTOF->GetYaxis()->SetTitleOffset(1.7);
  gPad->SetLeftMargin(.13);
  gTOF->Write();

  c1.cd(2);
  gWidthTOF->SetMarkerStyle(21);
  gWidthTOF->SetMarkerColor(kBlue);
  gWidthTOF->GetXaxis()->SetTitle(label.c_str());
  gWidthTOF->GetYaxis()->SetTitle("TOF Width [ns]");
  gWidthTOF->Draw();
  gWidthTOF->Write();
  
  c1.Write();
  OutFile->Close();
  delete OutFile;
  delete [] hist1Dlist;
}  
/**********************************************************************************************/
int Analyzer::User_TOFFit_2DCond(string title, string cond1, string cond2)
{
  int i, j, k;
  double par1,par2;
  if (ExpStatus!=2){
    cout<<"Error: Experiment histograms not loaded! Load before running UserTOFFit function."<<endl;
    return -1;
  }
  i = j = k =0;
  
  if (ExpConditionListBox.find(cond1)==ExpConditionListBox.end()){
    cout << "ExpCondition " << cond1 << " is not setup yet!\n";
    return -1;
  }
  if (ExpConditionListBox.find(cond2)==ExpConditionListBox.end()){
    cout << "ExpCondition " << cond2 << " is not setup yet!\n";
    return -1;
  }

  int N = HistDim1*HistDim2;
  
  //Output
  string filename,fitResults;
  filename = HISTOGRAM_DIRECTORY + "/UserHist_TOFFit_" + title + ".root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");
  OutFile->mkdir("TOFHistFits/");

  TH1* hist1D = NULL;
  TF1* gaus1D = new TF1("TOFfit","gaus");

  TGraphErrors** gTOFlist = new TGraphErrors* [HistDim1];
  TMultiGraph *mg = new TMultiGraph("mg","mg");

  string GraphName;
  TGraph2DErrors* gTOF = new TGraph2DErrors(N);
  GraphName = "TOF_vs_" + cond1 + "_vs_" + cond2;
  gTOF->SetName(GraphName.c_str());
  gTOF->SetTitle(GraphName.c_str());

  TGraph2DErrors* gWidthTOF = new TGraph2DErrors(N);
  GraphName = "TOFWidth_vs_" + cond1 + "_vs_" + cond2;
  gWidthTOF->SetName(GraphName.c_str());
  gWidthTOF->SetTitle(GraphName.c_str());

  //Fit and get peaks
  OutFile->cd("TOFHistFits/");
  for (i=0;i<HistDim1;i++){
    gTOFlist[i] = new TGraphErrors(HistDim2);
    gTOFlist[i]->SetMarkerStyle(22);
    gTOFlist[i]->SetLineWidth(2);
    gTOFlist[i]->SetLineColor(i+1);
    par1 = ExpConditionListBox[cond1][i];
    string gtitle = cond1 + "=" + to_string(par1);
    gTOFlist[i]->SetTitle(gtitle.c_str());
    for(j=0;j<HistDim2;j++){
      hist1D = (TH1*)(ExpHistBox["TOFHist"].Hist1DList[i][j]->Clone());
      if (hist1D==NULL){
            cout << "ERROR: TOFHist["<<i<<"]["<<j<<"] was never initialized. Make sure histogram is enabled. ABORTING FIT!"<<endl;
            if(!ExpHistBox["TOFHist"].Enabled) cout<< "Histogram not enabled."<<endl;
            return -1;
      }

      int MaxBin = hist1D->GetMaximumBin();
      double Max = hist1D->GetBinCenter(MaxBin);
      TFitResultPtr r = hist1D->Fit("TOFfit","SQ","",Max-4.0,Max+4.0);
      Int_t fitStatus = r;
      if (r<0) {
            cout << "Error in fit. Histogram maybe empty in fit range. ABORTING FIT!"<<endl;
            return -1;
      }
     // gStyle->SetOptStat(1111111);
      gStyle->SetOptFit(1);
      hist1D->SetDirectory(OutFile);
      hist1D->Write("",TObject::kOverwrite);
      cout<<"\n\nProb = "<<r->Prob()<<"\n";
      r->Print();
      //Fill Graphs
      par2 = ExpConditionListBox[cond2][j];
      gTOF->SetPoint(k,par1,par2,r->Parameter(1));
      gTOF->SetPointError(k,0,0,r->ParError(1));
      gWidthTOF->SetPoint(k,par1,par2,r->Parameter(2));
      gWidthTOF->SetPointError(k,0,0,r->ParError(2));
      k++;
      gTOFlist[i]->SetPoint(j,par2,r->Parameter(1));
      gTOFlist[i]->SetPointError(j,0,r->ParError(1));
      delete hist1D;
    }
    mg->Add(gTOFlist[i]);
  }
  OutFile->cd(); 
  //Set Graph Properties and Write
  TCanvas c1("Combined","c1",0,0,2*800,600);
  c1.Divide(2,1);
  
  c1.cd(1);
  gTOF->SetMarkerStyle(21);
  gTOF->SetMarkerColor(kBlue);
  gTOF->Draw("perr");
  gPad->Update();
  gTOF->GetHistogram()->GetXaxis()->SetTitle(cond1.c_str());
  gTOF->GetHistogram()->GetYaxis()->SetTitle(cond2.c_str());
  gTOF->GetHistogram()->GetZaxis()->SetTitle("TOF [ns]");
  gTOF->GetHistogram()->GetYaxis()->SetTitleOffset(1.7);
  gPad->SetLeftMargin(.13);
  gTOF->Write();
  gPad->Modified();
  gPad->Update();

  c1.cd(2);
  gWidthTOF->SetMarkerStyle(21);
  gWidthTOF->SetMarkerColor(kBlue);
  gWidthTOF->Draw("perr");
  gPad->Update();
  gWidthTOF->GetXaxis()->SetTitle(cond1.c_str());
  gWidthTOF->GetYaxis()->SetTitle(cond2.c_str());
  gWidthTOF->GetZaxis()->SetTitle("TOF Width [ns]");
  gWidthTOF->Write();
  gPad->Modified();
  gPad->Update();
  
  TCanvas c2("MultiGraph","c2",0,0,800,600);
  c2.cd();
  string mgraphtitle = "TOF vs " + cond2;
  mg->SetTitle(mgraphtitle.c_str());
  mg->Draw("apl");
  mg->GetXaxis()->SetTitle(cond2.c_str());
  mg->GetYaxis()->SetTitle("TOF [ns]");
  mg->Write();
  gPad->Modified();
  gPad->Update();
  c2.BuildLegend();
    

  c1.Write();
  c2.Write();
  OutFile->Close();
  delete OutFile;
}  
/**********************************************************************************************/
int Analyzer::User_Gauss1DFit(string HistName,string title)
{
  int N, i;
  int *I,*J;
  int zero = 0;
  double par;
  if (ExpStatus!=2){
    cout<<"Error: Experiment histograms not loaded! Load before running UserTOFFit function."<<endl;
    return -1;
  }
  i = 0;
  if (HistDim1>1 && HistDim2>1){
    cout<<"Warning: Both hist dimensions >1. Choosing HistDim1 by default."<<endl;
    N = HistDim1;
    I = &i;
    J = &zero;
  }else if(HistDim1>HistDim2){
    N = HistDim1;
    I = &i;
    J = &zero;
  }else{
    N = HistDim2;
    J = &i;
    I = &zero;
  }
  
  if (nExtPar == 0){
    cout<<"Error: External parameter list not initialized!" <<endl;
    return -1;
  }
  if (nExtPar != N){
    cout<<"Error: External parameter list "<<ExtParName<<" and histogram dimensions do not match!\n nExtPar = "<<nExtPar<<"\n nHist = "<<N<<endl;
    return -1;
  }

  string label = ExtParName;

  //Output
  string filename,fitResults;
  filename = HISTOGRAM_DIRECTORY + "/UserHist1DGaussFit_" + HistName + "_" + title + ".root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");

  TH1D** hist1Dlist = new TH1D*[N];
  TF1* gaus1D = new TF1("histfit","gaus");

  string GraphName;
  TGraphErrors* gTOF = new TGraphErrors(N);
  GraphName = HistName + "_Mu_vs_" + label;
  gTOF->SetName(GraphName.c_str());
  gTOF->SetTitle(GraphName.c_str());

  TGraphErrors* gWidthTOF = new TGraphErrors(N);
  GraphName = HistName + "_Sig_vs_" + label;
  gWidthTOF->SetName(GraphName.c_str());
  gWidthTOF->SetTitle(GraphName.c_str());

  //Fit and get peaks
  for (i=0;i<N;i++){
    hist1Dlist[i] = (TH1D*)(ExpHistBox[HistName].Hist1DList[*I][*J]->Clone());
    if (hist1Dlist[i]==NULL){
	  cout << "ERROR:"<<HistName<<"["<<*I<<"]["<<*J<<"] was never initialized. Make sure histogram is enabled. ABORTING FIT!"<<endl;
	  if(!ExpHistBox[HistName].Enabled) cout<< "Histogram not enabled."<<endl;
	  return -1;
    }

    int MaxBin = hist1Dlist[i]->GetMaximumBin();
    double Max = hist1Dlist[i]->GetBinCenter(MaxBin);
    TFitResultPtr r = hist1Dlist[i]->Fit("histfit","SQ","",Max-4.0,Max+4.0);
    Int_t fitStatus = r;
    if (r<0) {
	  cout << "Error in fit. Histogram maybe empty in fit range. ABORTING FIT!"<<endl;
	  return -1;
    }
   // gStyle->SetOptStat(1111111);
    gStyle->SetOptFit(1);
    hist1Dlist[i]->Write("",TObject::kOverwrite);
    cout<<"\n\nProb = "<<r->Prob()<<"\n";
    r->Print();
    //Fill Graphs
    par = ExtParList[i];
    gTOF->SetPoint(i,par,r->Parameter(1));
    gTOF->SetPointError(i,0,r->ParError(1));
    gWidthTOF->SetPoint(i,par,r->Parameter(2));
    gWidthTOF->SetPointError(i,0,r->ParError(2));
  }
  
  //Set Graph Properties and Write
  TCanvas c1("Combined","c1",0,0,2*800,600);
  c1.Divide(2,1);
  
  string ylabel_mu = "Mu_"+ExpHistBox[HistName].XTitle;
  string ylabel_sig = "Sig_"+ExpHistBox[HistName].XTitle;


  c1.cd(1);
  gTOF->SetMarkerStyle(21);
  gTOF->SetMarkerColor(kBlue);
  gTOF->GetXaxis()->SetTitle(label.c_str());
  gTOF->GetYaxis()->SetTitle(ylabel_mu.c_str());
  gTOF->Draw();
  gTOF->GetYaxis()->SetTitleOffset(1.7);
  gPad->SetLeftMargin(.13);
  gTOF->Write();

  c1.cd(2);
  gWidthTOF->SetMarkerStyle(21);
  gWidthTOF->SetMarkerColor(kBlue);
  gWidthTOF->GetXaxis()->SetTitle(label.c_str());
  gWidthTOF->GetYaxis()->SetTitle(ylabel_sig.c_str());
  gWidthTOF->Draw();
  gWidthTOF->GetYaxis()->SetTitleOffset(1.2);
  gWidthTOF->Write();
  
  c1.Write();
  OutFile->Write();
  OutFile->Close();
  delete OutFile;
  delete [] hist1Dlist;
}  
/**********************************************************************************************/

int Analyzer::User_MOT2DFit(string title,int n_pk)
{
  int N, i;
  int *I,*J;
  int zero = 0;
  double par;
  ofstream fout;
  
  if (ExpStatus!=2){
    cout<<"Error: Experiment histograms not loaded! Load before running MOT2DFit function."<<endl;
    return -1;
  }
  if (!OutTreesInitialized){
    cout<<"Error: Output trees are not initialized! Load before running MOT2DFit function."<<endl;
    return -1;
  }
  
  i = 0;
  if (HistDim1>1 && HistDim2>1){
    cout<<"Warning: Both hist dimensions >1. Choosing HistDim1 by default."<<endl;
    N = HistDim1;
    I = &i;
    J = &zero;
  }else if(HistDim1>HistDim2){
    N = HistDim1;
    I = &i;
    J = &zero;
  }else{
    N = HistDim2;
    J = &i;
    I = &zero;
  }
 
  if (nExtPar == 0){
    cout<<"Error: External parameter list not initialized!" <<endl;
    return -1;
  }
  if (nExtPar != N){
    cout<<"Error: External parameter list "<<ExtParName<<" and histogram dimensions do not match!\n nExtPar = "<<nExtPar<<"\n nHist = "<<N<<endl;
    return -1;
  }

  string label = ExtParName;

  //Get hist binning information to copy
  /*int nxbin = ExpHistBox["MCP_Image_Zoom"].XBinNum;
  double xlo = ExpHistBox["MCP_Image_Zoom"].XRange.low;
  double xhi = ExpHistBox["MCP_Image_Zoom"].XRange.high;
  int nybin = ExpHistBox["MCP_Image_Zoom"].YBinNum;
  double ylo = ExpHistBox["MCP_Image_Zoom"].YRange.low;
  double yhi = ExpHistBox["MCP_Image_Zoom"].YRange.high;
  */
  double xlo = -5;//mm
  double ylo = -5;//mm
  double xhi = 5;//mm
  double yhi = 5;//mm
  int nxbin = 100;
  int nybin = 100; 
  
  //Output
  string filename,fitResults,foutname;
  foutname = HISTOGRAM_DIRECTORY + "/MOT2DFit_" + title + ".txt";
  
  filename = HISTOGRAM_DIRECTORY + "/UserHist_MOT2DFit_" + title + ".root";
  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");
 
  TH2D** hist2Dlist = new TH2D*[N];
  TH2D** hres2Dlist = new TH2D*[N];
  //TH2D* myhtemp = (TH2D*)(ExpHistBox["MCP_Image_Zoom"].Hist2DList[0][0]->Clone("myhtemp"));
  TH2D* myhtemp = new TH2D("myhtemp","MCP Image",nxbin,xlo,xhi,nybin,ylo,yhi);
  myhtemp->GetXaxis()->SetTitle("X [mm]");
  myhtemp->GetYaxis()->SetTitle("Y [mm]");
  //TF2* gaus2D = new TF2("XYgaus","xygaus",xlo,xhi,ylo,yhi);
  TF2* gaus2D = new TF2("XYgaus","[0]*exp(-([1]*(x-[4])*(x-[4]) -2*[2]*(x-[4])*(y-[5]) +[3]*(y-[5])*(y-[5])))",xlo,xhi,ylo,yhi);
  TF2* bkg = new TF2("bkg","[0]+0*x*y",xlo,xhi,ylo,yhi);
  TF2* twopeak_gaus2D = new TF2("TwoXYgaus","XYgaus + XYgaus + bkg",xlo,xhi,ylo,yhi);
  TF2* fit_func; //fit function
  if(n_pk == 2) fit_func = twopeak_gaus2D;
  else fit_func = gaus2D;
 
  string GraphName;
  TGraphErrors* gPosX = new TGraphErrors(N);
  GraphName = "MOTPosX1_vs_" + label;
  gPosX->SetName(GraphName.c_str());
  gPosX->SetTitle(GraphName.c_str());
  TGraphErrors* gPosY = new TGraphErrors(N);
  GraphName = "MOTPosY1_vs_" + label;
  gPosY->SetName(GraphName.c_str());
  gPosY->SetTitle(GraphName.c_str());
  TGraphErrors* gWidthX = new TGraphErrors(N);
  GraphName = "MOTWidthX1_vs_" + label;
  gWidthX->SetName(GraphName.c_str());
  gWidthX->SetTitle(GraphName.c_str());
  TGraphErrors* gWidthY = new TGraphErrors(N);
  GraphName = "MOTWidthY1_vs_" + label;
  gWidthY->SetName(GraphName.c_str());
  gWidthY->SetTitle(GraphName.c_str());
  
  TGraphErrors* gPosX2 = new TGraphErrors(N);
  GraphName = "MOTPosX2_vs_" + label;
  gPosX2->SetName(GraphName.c_str());
  gPosX2->SetTitle(GraphName.c_str());
  TGraphErrors* gPosY2 = new TGraphErrors(N);
  GraphName = "MOTPosY2_vs_" + label;
  gPosY2->SetName(GraphName.c_str());
  gPosY2->SetTitle(GraphName.c_str());
  TGraphErrors* gWidthX2 = new TGraphErrors(N);
  GraphName = "MOTWidthX2_vs_" + label;
  gWidthX2->SetName(GraphName.c_str());
  gWidthX2->SetTitle(GraphName.c_str());
  TGraphErrors* gWidthY2 = new TGraphErrors(N);
  GraphName = "MOTWidthY2_vs_" + label;
  gWidthY2->SetName(GraphName.c_str());
  gWidthY2->SetTitle(GraphName.c_str());

//Define multigraphs
  TMultiGraph *mgX = new TMultiGraph();
  GraphName = "MOTPosX_vs_" + label;
  mgX->SetName(GraphName.c_str());
  mgX->SetTitle(GraphName.c_str());

  TMultiGraph *mgY = new TMultiGraph();
  GraphName = "MOTPosY_vs_" + label;
  mgY->SetName(GraphName.c_str());
  mgY->SetTitle(GraphName.c_str());
  
  TMultiGraph *mgsigX = new TMultiGraph();
  GraphName = "MOTSigX_vs_" + label;
  mgsigX->SetName(GraphName.c_str());
  mgsigX->SetTitle(GraphName.c_str());
  
  TMultiGraph *mgsigY = new TMultiGraph();
  GraphName = "MOTSigY_vs_" + label;
  mgsigY->SetName(GraphName.c_str());
  mgsigY->SetTitle(GraphName.c_str());
  
//double theta = -0.3926991; //angle of rotation in radians
   
  //make cut for tree
  double MaskPitch, GridTh;
  MaskPitch = 4;//mm
  GridTh = .4;//mm to fall on bins
  double xm = (MaskPitch-GridTh)/2;
  double xp = (MaskPitch+GridTh)/2;
  double ym = xm;
  double yp = xp;

  double xarray[5] = {xm,xp,xp,xm,xm};
  double yarray[5] = {yhi,yhi,ylo,ylo,yhi};
  TCutG cutg1("cutg1",5,xarray,yarray);
  cutg1.SetVarX("MCPHitPosAA.X");//required
  cutg1.SetVarY("MCPHitPosAA.Y");
  TCutG cutg2("cutg2",5,yarray,xarray);
  cutg2.SetVarX("MCPHitPosAA.X");
  cutg2.SetVarY("MCPHitPosAA.Y");
  for(i=0;i<5;i++) xarray[i]-=MaskPitch;
  TCutG cutg3("cutg3",5,xarray,yarray);
  cutg3.SetVarX("MCPHitPosAA.X");
  cutg3.SetVarY("MCPHitPosAA.Y");
  TCutG cutg4("cutg4",5,yarray,xarray);
  cutg4.SetVarX("MCPHitPosAA.X");
  cutg4.SetVarY("MCPHitPosAA.Y");
 

    ostringstream stm1;
    stm1 <<"X1\tXerr1\tY1\tYerr1\tXwidth1\tXwidtherr1\tYwidth1\tYwidtherr1\ttheta1\t";
    stm1 <<"X2\tXerr2\tY2\tYerr2\tXwidth2\tXwidtherr2\tYwidth2\tYwidtherr2\ttheta2\t"<<ExtParName<<"\n";
   // gStyle->SetOptFit(1);
   //Fit and get peaks
  for (i=0;i<N;i++){
    //draw contents into histogram (Y:X)
    //OutTrees[*I][*J]->Draw("MCPHitPosAA.X*sin(-.392)+MCPHitPosAA.Y*cos(-.392):MCPHitPosAA.X*cos(-.392)-MCPHitPosAA.Y*sin(-.392)>>myhtemp","!cutg1&&!cutg2&&!cutg3&&!cutg4");
    OutTrees[*I][*J]->Draw("MCPHitPosAA.Y:MCPHitPosAA.X>>myhtemp","!cutg1&&!cutg2&&!cutg3&&!cutg4");
    string HistName = ExpHistBox["MCP_Image_Zoom"].Hist2DList[*I][*J]->GetName();
    hist2Dlist[i] =(TH2D*) myhtemp->Clone(HistName.c_str());   
    string ResHistName = "Res_" + HistName;
    hres2Dlist[i] =(TH2D*) myhtemp->Clone(ResHistName.c_str());   
    hres2Dlist[i]->Reset();
    hres2Dlist[i]->SetTitle("Residuals");
    
    //Fit
    TFitResultPtr r;
    
    r = hist2Dlist[i]->Fit(fit_func,"SQ");
    fit_func->SetParameter(4,hist2Dlist[i]->GetMean(1));
    fit_func->SetParameter(5,hist2Dlist[i]->GetMean(2));
    //fit_func->SetParameter(10,hist2Dlist[i]->GetMean(1));
    //fit_func->SetParameter(11,hist2Dlist[i]->GetMean(2));
    //twopeak_gaus2D->SetParameter(4,hist2Dlist[i]->GetMean(1));
    //twopeak_gaus2D->SetParameter(5,hist2Dlist[i]->GetMean(2));
    r = hist2Dlist[i]->Fit(fit_func,"SQ");
    Int_t fitStatus = r;
    if (r<0) {
	  cout << "Error in fit. Histogram maybe empty in fit range. ABORTING FIT!"<<endl;
	  return -1;
    }
    hist2Dlist[i]->Write("",TObject::kOverwrite);
    r->Print();
    cout<<"Prob = "<<r->Prob()<<"\n";
    
    //Compute residuals
    double binx,biny,xpos,ypos,res;
    for(int j=1;j<=nxbin;j++){//avoid underflow bin 0
      xpos = hist2Dlist[i]->GetXaxis()->GetBinCenter(j);
      for(int k=1;k<=nybin;k++){
        ypos = hist2Dlist[i]->GetYaxis()->GetBinCenter(k);
        if(cutg1.IsInside(xpos,ypos)||cutg2.IsInside(xpos,ypos)||cutg3.IsInside(xpos,ypos)||cutg4.IsInside(xpos,ypos)) res = 0;
        else res = hist2Dlist[i]->GetBinContent(j,k) - fit_func->Eval(xpos,ypos);
        hres2Dlist[i]->SetBinContent(j,k,res);
      }
    }

    hres2Dlist[i]->Write("",TObject::kOverwrite);
    //Fill Graphs
    //par = Alpha_low+i*Alpha_interval;
    par = ExtParList[i];

    //Calculate widths Peak1
    double a,b,c,theta,sigX,sigXerr,sigY,sigYerr;
    a = r->Parameter(1);
    b = r->Parameter(2);
    c = r->Parameter(3);
    
    theta = .5*atan2(2*b,a-c)-PI/2;
    sigX = sqrt(1/(a+c +2*b/(sin(2*theta))));
    sigY = sqrt(1/(a+c -2*b/(sin(2*theta))));
    sigXerr = 0;
    sigYerr = 0;
 //   ostringstream stm;
 //   stm << string("Theta = ")<<theta<<"\n";
    cout<<"Theta = "<<180/PI*theta<<endl;

    gPosX->SetPoint(i,par,r->Parameter(4));
    gPosX->SetPointError(i,0,r->ParError(4));
    gWidthX->SetPoint(i,par,sigX);
    gWidthX->SetPointError(i,0,sigXerr);
    gPosY->SetPoint(i,par,r->Parameter(5));
    gPosY->SetPointError(i,0,r->ParError(5));
    gWidthY->SetPoint(i,par,sigY);
    gWidthY->SetPointError(i,0,sigYerr);
    
   stm1 <<r->Parameter(4)<<"\t"<<r->ParError(4)<<"\t"<<r->Parameter(5)<<"\t"<<r->ParError(5)<<"\t"<<sigX<<"\t"<<sigXerr<<"\t"<<sigY<<"\t"<<sigYerr<<"\t"<<theta<<"\t";
    
    if(n_pk==2){
      //Calculate widths Peak2
      a = r->Parameter(7);
      b = r->Parameter(8);
      c = r->Parameter(9);
      
      theta = .5*atan2(2*b,a-c)-PI/2;
      sigX = sqrt(1/(a+c +2*b/(sin(2*theta))));
      sigY = sqrt(1/(a+c -2*b/(sin(2*theta))));
      sigXerr = 0;
      sigYerr = 0;
   //   ostringstream stm;
   //   stm << string("Theta = ")<<theta<<"\n";
      cout<<"Theta = "<<180/PI*theta<<endl;

      gPosX2->SetPoint(i,par,r->Parameter(10));
      gPosX2->SetPointError(i,0,r->ParError(10));
      gWidthX2->SetPoint(i,par,sigX);
      gWidthX2->SetPointError(i,0,sigXerr);
      gPosY2->SetPoint(i,par,r->Parameter(11));
      gPosY2->SetPointError(i,0,r->ParError(11));
      gWidthY2->SetPoint(i,par,sigY);
      gWidthY2->SetPointError(i,0,sigYerr);


      stm1 <<r->Parameter(10)<<"\t"<<r->ParError(10)<<"\t"<<r->Parameter(11)<<"\t"<<r->ParError(11)<<"\t"<<sigX<<"\t"<<sigXerr<<"\t"<<sigY<<"\t"<<sigYerr<<"\t"<<theta<<"\t"<<par<<"\n";
    }
    else{
      stm1 <<0<<"\t"<<0<<"\t"<<0<<"\t"<<0<<"\t"<<0<<"\t"<<0<<"\t"<<0<<"\t"<<0<<"\t"<<0<<"\t"<<par<<"\n";
    }
    //stm1 <<sigX<<"\t"<<sigXerr<<"\t"<<sigY<<"\t"<<sigYerr<<"\t"<<theta<<"\n";
    //stm1 <<"testing"<<"\n";
    /*gPosX->SetPoint(i,par,r->Parameter(1));
    gPosX->SetPointError(i,0,r->ParError(1));
    gWidthX->SetPoint(i,par,r->Parameter(2));
    gWidthX->SetPointError(i,0,r->ParError(2));
    gPosY->SetPoint(i,par,r->Parameter(3));
    gPosY->SetPointError(i,0,r->ParError(3));
    gWidthY->SetPoint(i,par,r->Parameter(4));
    gWidthY->SetPointError(i,0,r->ParError(4));
    */
  }
  
  
  cout<< stm1.str();
  
  gPosX->SetMarkerStyle(21);
  gPosX->SetMarkerColor(kBlue);
  gPosX->Write();
  
  gPosY->SetMarkerStyle(21);
  gPosY->SetMarkerColor(kRed);
  gPosY->Write();
  
  gWidthX->SetMarkerStyle(21);
  gWidthX->SetMarkerColor(kBlue);
  gWidthX->Write();
  
  gWidthY->SetMarkerStyle(21);
  gWidthY->SetMarkerColor(kRed);
  gWidthY->Write();
  
  if(n_pk==2){
    gPosX2->SetMarkerStyle(22);
    gPosX2->SetMarkerColor(kBlue);
    gPosX2->Write();
    
    gPosY2->SetMarkerStyle(22);
    gPosY2->SetMarkerColor(kRed);
    gPosY2->Write();
    
    gWidthX2->SetMarkerStyle(22);
    gWidthX2->SetMarkerColor(kBlue);
    gWidthX2->Write();
    
    gWidthY2->SetMarkerStyle(22);
    gWidthY2->SetMarkerColor(kRed);
    gWidthY2->Write();
  }
  auto legend = new TLegend(0.7,0.85,0.9,.95);
  //legend->SetHeader("The Legend Title","C"); // option "C" allows to center the header
  legend->AddEntry(gPosX,"Peak 1","lep");
  if(n_pk==2) legend->AddEntry(gPosX2,"Peak 2","lep");
  //legend->AddEntry("gr","Graph with error bars","lep");
  //legend->Draw();
  if(n_pk==2){
    mgX->Add(gPosX,"PL");
    mgX->Add(gPosX2,"PL");
    legend->Write();
    mgX->Write();
    
    mgY->Add(gPosY,"PL");
    mgY->Add(gPosY2,"PL");
    mgY->Write();
    
    mgsigX->Add(gWidthX,"PL");
    mgsigX->Add(gWidthX2,"PL");
    mgsigX->Write();

    mgsigY->Add(gWidthY,"PL");
    mgsigY->Add(gWidthY2,"PL");
    mgsigY->Write();
  }
  


  OutFile->Close();
  delete OutFile;
  delete [] hist2Dlist;

  fout.open(foutname.c_str(),ios::out);
  fout<< stm1.str();
  fout.close();
 
  return 0; 
}
/**********************************************************************************************/
int Analyzer::User_MOTEvolution(double TStart,double TStop, int NInterval)
{
  if (TStop>250)TStop=250;
  double Interval = (TStop-TStart)/double(NInterval);
  string spectrumfile;

  ExpDataStruct ExpData;      //Data struct for exp data reading
  RecDataStruct RecData;      //Reconstructed data struct
  ConstructExpParameterList();
  CheckExpConditionSettings();

  double RotationAngle = -0.3927;       //22.5 deg

  //Histograms
  TH2** h_MCPImage = new TH2*[NInterval];
  TH2** h_MCPImage_Center = new TH2*[NInterval];
  for (int i=0;i<NInterval;i++){
    h_MCPImage[i] = new TH2D(Form("h_MCPImage%d",i),Form("MCP Image T = %f ms",TStart+i*Interval),400,-40,40,400,-40,40);
    h_MCPImage_Center[i] = new TH2D(Form("h_MCPImage_Center%d",i),Form("MCP Image Center T = %f ms",TStart+i*Interval),100,-5,5,100,-5,5);
  }

  //Read In Data
  ostringstream convert;
  convert << setfill('0') << setw(4)<<FileIDListExp[0][0];
  TString fName = EXP_DATA_DIRECTORY + string("/") + ExpFilePrefix + string("_") + convert.str();
  fName += ".root";
  TFile *f = new TFile (fName.Data(), "read");

  TTree *Tree_MCP = (TTree*)f->Get("Tree_MCP");

  Tree_MCP->SetBranchAddress("Event_No",&ExpData.Event_Num);
  Tree_MCP->SetBranchAddress("GroupTime",&ExpData.GroupTime);
  Tree_MCP->SetBranchAddress("TMCP_rel",&ExpData.TMCP_rel);
  Tree_MCP->SetBranchAddress("QMCP",&ExpData.QMCP);
  Tree_MCP->SetBranchAddress("QMCP_anodes",&ExpData.QMCP_anodes);
  Tree_MCP->SetBranchAddress("TMCP_anodes",&ExpData.TMCP_anodes);
  Tree_MCP->SetBranchAddress("TriggerMap",&ExpData.TriggerMap);
  int N_MCP = Tree_MCP->GetEntries();
  for(int i=0;i<N_MCP;i++){
    ClearTempData(ExpData,RecData);
    //Read Tree
    Tree_MCP->GetEntry(i);
    Reconstruct(0,0,RecData,ExpData);
    ProcessExpConditions(RecData,0);

    //Fill Histograms
    if (Conditions[0][0]){
      if (RecData.TMCP_rel>TStop || RecData.TMCP_rel<TStart)continue;
      int Index = int((RecData.TMCP_rel-TStart)/Interval);
      if (Index<0 || Index>=NInterval)continue;//Safety check
      h_MCPImage[Index]->Fill( RecData.MCPPos_X, RecData.MCPPos_Y);
      h_MCPImage_Center[Index]->Fill( RecData.MCPPos_X, RecData.MCPPos_Y);
    }
  }

  //Output
  spectrumfile = EXP_DATA_DIRECTORY + string("/../Histograms/UserHist_") + ExpFilePrefix + string("_") + convert.str() + string("_MOTEvolution.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");

  for(int i=0;i<NInterval;i++){
    h_MCPImage[i]->Write();
    h_MCPImage_Center[i]->Write();
  }

  histfile->Close();


  return 0;
}
/**********************************************************************************************/

int Analyzer::User_Monitor(string Object,string label)
{
  int nx,ny;
  int N = HistDim1; 
//int N = HistDim1>HistDim2 ? HistDim1 : HistDim2; 
  if (Object.compare("MOT")==0){
    TGraphErrors* gPosX = new TGraphErrors();
    gPosX->SetName("MOT_Pos_X");
    gPosX->SetTitle("MOT_Pos_X");
    TGraphErrors* gPosY = new TGraphErrors();
    gPosY->SetName("MOT_Pos_Y");
    gPosY->SetTitle("MOT_Pos_Y");
    TGraphErrors* gWidthX = new TGraphErrors();
    gWidthX->SetName("MOT_Width_X");
    gWidthX->SetTitle("MOT_Width_X");
    TGraphErrors* gWidthY = new TGraphErrors();
    gWidthY->SetName("MOT_Width_Y");
    gWidthY->SetTitle("MOT_Width_Y");
    TH1** histXlist = new TH1*[N];
    TH1** histYlist = new TH1*[N];
    TF1** FitFuncXlist = new TF1*[N];
    TF1** FitFuncYlist = new TF1*[N];
    for (int i=0;i<N;i++){
/*      //Bkg subtraction
      double average = ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->Integral(53,76,76,98)/(552.0);
      TH2* h_MOTImageBkgSub = new TH2D(Form("BkgSub%d",i),"BkgSub",100,-5,5,100,-5,5);
      for (int ii=1;ii<=100;ii++){
	for (int jj=1;jj<=100;jj++){
	  if ((ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->GetBinContent(ii,jj)-average)>0)
	    h_MOTImageBkgSub->SetBinContent(ii,jj,ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->GetBinContent(ii,jj)-average*1.2);
	  else
	    h_MOTImageBkgSub->SetBinContent(ii,jj,0);
	}
      }
*/    nx = ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->GetXaxis()->GetNbins();
      histXlist[i] = (TH1*)(ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->ProjectionX("_px",1,nx)->Clone());
      gStyle->SetOptStat(1111111);
      //      histXlist[i] = (TH1*)(h_MOTImageBkgSub->ProjectionX()->Clone());
      double BkgX;
      if (histXlist[i]->Integral(histXlist[i]->FindBin(-5),histXlist[i]->FindBin(-3))>0){
	histXlist[i]->Fit("pol0","WW","",-5,-3);
	BkgX = histXlist[i]->GetFunction("pol0")->GetParameter(0);
      }else{
	BkgX=0;
      }
      int MaxBin = histXlist[i]->GetMaximumBin();
      double Max = histXlist[i]->GetBinCenter(MaxBin);
      double Peak = histXlist[i]->GetMaximum();
      FitFuncXlist[i] = new TF1(Form("FitFuncX_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]",Max-3,Max+3);
      FitFuncXlist[i]->SetParameters(Peak,Max,0.2,BkgX);
      FitFuncXlist[i]->FixParameter(3,BkgX);
//      FitFuncXlist[i] = new TF1(Form("FitFuncX_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))",Max-3,Max+3);
//      FitFuncXlist[i]->SetParameters(Peak,Max,0.6);
      FitFuncXlist[i]->SetParLimits(2,0.03,2.0);
      histXlist[i]->Fit(FitFuncXlist[i],"","",Max-3,Max+3);
//      histXlist[i]->Fit("gaus","","",Max-1.5,Max+1.5);

      gPosX->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(1));
      gPosX->SetPointError(i,0,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(1));
      gWidthX->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(2));
      gWidthX->SetPointError(i,0,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(2));

      ny = ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->GetYaxis()->GetNbins();
      histYlist[i] = (TH1*)(ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->ProjectionY("_py",1,ny)->Clone());
      gStyle->SetOptStat(1111111);
      //      histYlist[i] = (TH1*)(h_MOTImageBkgSub->ProjectionY()->Clone());
      double BkgY;
      if (histYlist[i]->Integral(histYlist[i]->FindBin(-5),histYlist[i]->FindBin(-3))>0){
	histYlist[i]->Fit("pol0","WW","",-5,-3);
	BkgY = histYlist[i]->GetFunction("pol0")->GetParameter(0);
      }else{
	BkgY=0;
      }
      MaxBin = histYlist[i]->GetMaximumBin();
      Max = histYlist[i]->GetBinCenter(MaxBin);
      Peak = histYlist[i]->GetMaximum();
      FitFuncYlist[i] = new TF1(Form("FitFuncY_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]",Max-3,Max+3);
      FitFuncYlist[i]->SetParameters(Peak,Max,0.2,BkgY);
//      FitFuncYlist[i]->SetParameters(Peak,Max,0.6,BkgY);
      FitFuncYlist[i]->FixParameter(3,BkgY);
//      FitFuncYlist[i] = new TF1(Form("FitFuncY_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))",Max-3,Max+3);
//      FitFuncYlist[i]->SetParameters(Peak,Max,0.6);
      FitFuncYlist[i]->SetParLimits(2,0.03,2.0);
      histYlist[i]->Fit(FitFuncYlist[i],"","",Max-3,Max+3);
//      histYlist[i]->Fit("gaus","","",Max-1.5,Max+1.5);

      gPosY->SetPoint(i,i+1,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(1));
      gPosY->SetPointError(i,0,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(1));
      gWidthY->SetPoint(i,i+1,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(2));
      gWidthY->SetPointError(i,0,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(2));
    }

    //Draw
    TCanvas* c1 = new TCanvas("c1","c1",0,0,800,600);
    gPosX->SetMarkerStyle(21);
    gPosX->SetMarkerColor(kBlue);
    gPosX->Draw("APL");
    TCanvas* c2 = new TCanvas("c2","c2",0,0,800,600);
    gPosY->SetMarkerStyle(21);
    gPosY->SetMarkerColor(kRed);
    gPosY->Draw("APL");
    TCanvas* c3 = new TCanvas("c3","c3",0,0,800,600);
    gWidthX->SetMarkerStyle(21);
    gWidthX->SetMarkerColor(kBlue);
    gWidthX->Draw("APL");
    TCanvas* c4 = new TCanvas("c4","c4",0,0,800,600);
    gWidthY->SetMarkerStyle(21);
    gWidthY->SetMarkerColor(kRed);
    gWidthY->Draw("APL");

    //Fit
    gPosX->Fit("pol0");
    gPosY->Fit("pol0");
    gWidthX->Fit("pol0");
    gWidthY->Fit("pol0");

    string OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf(";
    c1->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf";
    c2->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf";
    c3->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf)";
    c4->SaveAs(OutputFileName.c_str());

    OutputFileName = HISTOGRAM_DIRECTORY +  "/UserMonitorHist_" + Object + "_" + label + ".root";
    TFile *OutFile = new TFile(OutputFileName.c_str(),"RECREATE");
    for (int i=0;i<N;i++){
      histXlist[i]->Write();
      histYlist[i]->Write();
    }
    gPosX->Write();
    gPosY->Write();
    gWidthX->Write();
    gWidthY->Write();
    c1->Write();
    c2->Write();
    c3->Write();
    c4->Write();
    OutFile->Close();
    delete OutFile;
  }else if (Object.compare("PhotoIonZ")==0){
    if (ReadMode.compare("He4")!=0){
      cout <<"It is only possible to monitor the PhotoIon TOF in He4 Mode."<<endl;
      //return -1;
    }
    TGraphErrors* gPosZ = new TGraphErrors();
    gPosZ->SetName("MOT_Pos_Z");
    gPosZ->SetTitle("MOT_Pos Z");
    TGraphErrors* gTOF = new TGraphErrors();
    gTOF->SetName("PhotoIon_TOF");
    gTOF->SetTitle("PhotoIon TOF");
    TGraphErrors* gWidthZ = new TGraphErrors();
    gWidthZ->SetName("MOT_Width Z");
    gWidthZ->SetTitle("MOT_Width Z");
    TGraphErrors* gWidthTOF = new TGraphErrors();
    gWidthTOF->SetName("PhotoIon_TOFWidth");
    gWidthTOF->SetTitle("PhotoIon_TOFWidth");
    TH1** histTOFlist = new TH1*[N];
    for (int i=0;i<N;i++){
      histTOFlist[i] = (TH1*)(ExpHistBox["TOFHist"].Hist1DList[i][0]->Clone());
      int MaxBin = histTOFlist[i]->GetMaximumBin();
      double Max = histTOFlist[i]->GetBinCenter(MaxBin);
      histTOFlist[i]->Fit("gaus","","",Max-3.0,Max+3.0);

      gTOF->SetPoint(i,i+1,histTOFlist[i]->GetFunction("gaus")->GetParameter(1));
      gTOF->SetPointError(i,0,histTOFlist[i]->GetFunction("gaus")->GetParError(1));
      gWidthTOF->SetPoint(i,i+1,histTOFlist[i]->GetFunction("gaus")->GetParameter(2));
      gWidthTOF->SetPointError(i,0,histTOFlist[i]->GetFunction("gaus")->GetParError(2));
    }
    //Draw
    TCanvas* c1 = new TCanvas("c1","c1",0,0,800,600);
    gTOF->SetMarkerStyle(21);
    gTOF->SetMarkerColor(kBlue);
    gTOF->Draw("APL");
    TCanvas* c3 = new TCanvas("c3","c3",0,0,800,600);
    gWidthTOF->SetMarkerStyle(21);
    gWidthTOF->SetMarkerColor(kBlue);
    gWidthTOF->Draw("APL");
/*    TCanvas* c2 = new TCanvas("c2","c2",0,0,800,600);
    gPosZ->SetMarkerStyle(21);
    gPosZ->SetMarkerColor(kRed);
    gPosZ->Draw("APL");
    
    TCanvas* c4 = new TCanvas("c4","c4",0,0,800,600);
    gWidthZ->SetMarkerStyle(21);
    gWidthZ->SetMarkerColor(kRed);
    gWidthZ->Draw("APL");
    */

    //Fit
    gTOF->Fit("pol0");
    gWidthTOF->Fit("pol0");
//    gPosZ->Fit("pol0");
//    gWidthZ->Fit("pol0");

    string OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf(";
    c1->SaveAs(OutputFileName.c_str());
//    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf";
//    c2->SaveAs(OutputFileName.c_str());
//    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf";
//    c3->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf)";
    c3->SaveAs(OutputFileName.c_str());

    OutputFileName = HISTOGRAM_DIRECTORY +  "/UserMonitorHist_" + Object + "_" + label + ".root";
    TFile *OutFile = new TFile(OutputFileName.c_str(),"RECREATE");
    for (int i=0;i<N;i++){
      histTOFlist[i]->Write();
    }
    gTOF->Write();
    gWidthTOF->Write();
//    gPosZ->Write();
//    gWidthZ->Write();
    c1->Write();
//    c2->Write();
//    c4->Write();
    c3->Write();
    OutFile->Close();
    delete OutFile;
  }else if (Object.compare("TimingZero")==0){
    if (ReadMode.compare("Triple")!=0 && ReadMode.compare("Double")!=0){
      cout <<"It is only possible to monitor the Timing Zero in Triple or Double Mode."<<endl;
      return -1;
    }
    TGraphErrors* LPeak = new TGraphErrors();
    LPeak->SetName("LPeak");
    LPeak->SetTitle("Left Peak");
    TGraphErrors* LWidth = new TGraphErrors();
    LWidth->SetName("LWidth");
    LWidth->SetTitle("Left Width");
    TGraphErrors* RPeak = new TGraphErrors();
    RPeak->SetName("RPeak");
    RPeak->SetTitle("Right Peak");
    TGraphErrors* RWidth = new TGraphErrors();
    RWidth->SetName("RWidth");
    RWidth->SetTitle("Right Width");
    TH1** histTOFlist = new TH1*[N];
    for (int i=0;i<N;i++){
      histTOFlist[i] = (TH1*)(ExpHistBox["TOFHist"].Hist1DList[i][0]->Clone());
      int MaxBin = histTOFlist[i]->GetMaximumBin();
      double Max = histTOFlist[i]->GetBinCenter(MaxBin);
      double SemiMax = Max+1.7;
      int SemiMaxBin = histTOFlist[i]->FindBin(SemiMax);

      TF1* fitfunc = new TF1("DoubleGaus","gaus(0)+gaus(3)",-3.0,3.0);
      double p0 = histTOFlist[i]->GetBinContent(MaxBin);
      double p3 = histTOFlist[i]->GetBinContent(SemiMaxBin);
      fitfunc->SetParameters(p0,Max,0.3,p3,Max+1.7,0.3);
      fitfunc->SetParLimits(2,0.2,1.0);
      fitfunc->SetParLimits(5,0.2,1.0);

      histTOFlist[i]->Fit(fitfunc,"","",Max-3.0,Max+4.7);

      LPeak->SetPoint(i,i+1,fitfunc->GetParameter(1));
      LPeak->SetPointError(i,0,fitfunc->GetParError(1));
      LWidth->SetPoint(i,i+1,fitfunc->GetParameter(2));
      LWidth->SetPointError(i,0,fitfunc->GetParError(2));
      RPeak->SetPoint(i,i+1,fitfunc->GetParameter(4));
      RWidth->SetPointError(i,0,fitfunc->GetParError(5));
      RWidth->SetPoint(i,i+1,fitfunc->GetParameter(5));
      RPeak->SetPointError(i,0,fitfunc->GetParError(4));
    }
    //Draw
    TCanvas* c1 = new TCanvas("c1","c1",0,0,800,600);
    LPeak->SetMarkerStyle(21);
    LPeak->SetMarkerColor(kBlue);
    LPeak->Draw("APL");
    RPeak->SetMarkerStyle(21);
    RPeak->SetMarkerColor(kRed);
    RPeak->Draw("same");
    TCanvas* c2 = new TCanvas("c2","c2",0,0,800,600);
    LWidth->SetMarkerStyle(21);
    LWidth->SetMarkerColor(kBlue);
    LWidth->Draw("APL");
    RWidth->SetMarkerStyle(21);
    RWidth->SetMarkerColor(kRed);
    RWidth->Draw("same");

    //Fit
    LPeak->Fit("pol0");
    RPeak->Fit("pol0");
    LWidth->Fit("pol0");
    RWidth->Fit("pol0");

    string OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf(";
    c1->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf)";
    c2->SaveAs(OutputFileName.c_str());

    OutputFileName = HISTOGRAM_DIRECTORY +  "/UserMonitorHist_" + Object + "_" + label + ".root";
    TFile *OutFile = new TFile(OutputFileName.c_str(),"RECREATE");
    for (int i=0;i<N;i++){
      histTOFlist[i]->Write();
    }
    LPeak->Write();
    RPeak->Write();
    LWidth->Write();
    RWidth->Write();
    c1->Write();
    c2->Write();
    OutFile->Close();
    delete OutFile;
  }else if (Object.compare("Bi207Cal")==0){
    if (ReadMode.compare("Beta")!=0){
      cout <<"It is only possible to monitor the Bi207 calibration in Beta Mode."<<endl;
      return -1;
    }
    TGraphErrors* Peak = new TGraphErrors();
    Peak->SetName("Peak");
    Peak->SetTitle("Peak");
    TGraphErrors* Width = new TGraphErrors();
    Width->SetName("Width");
    Width->SetTitle("Width");
    TH1** histEBetalist = new TH1*[N];
    for (int i=0;i<N;i++){
      histEBetalist[i] = (TH1*)(ExpHistBox["Scint_EA"].Hist1DList[i][0]->Clone());
      int MaxBin = histEBetalist[i]->GetMaximumBin();
      double Max = histEBetalist[i]->GetBinCenter(MaxBin);
      double MaxCount = histEBetalist[i]->GetBinContent(MaxBin);

      TF1* fitfunc = new TF1("fitfunc","[0]*(exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+0.26*exp(-pow(x-1.074*[1],2.0)/2.0/pow([2],2.0))+0.062*exp(-pow(x-1.086*[1],2.0)/2.0/pow([2],2.0))+[3]*ROOT::Math::erfc((x-[1]+[4]*[2]*[2])/(sqrt(2)*[2]))*exp([4]/2.0*(2.0*x-2.0*[1]+[4]*[2]*[2])))+[5]*(exp(-pow(x-[6],2.0)/2.0/pow([7],2.0))+0.29*exp(-pow(x-1.15*[6],2.0)/2.0/pow([7],2.0))+0.072*exp(-pow(x-1.175*[6],2.0)/2.0/pow([7],2.0))+[8]*ROOT::Math::erfc((x-[6]+[9]*[7]*[7])/(sqrt(2)*[7]))*exp([9]/2.0*(2.0*x-2.0*[6]+[9]*[7]*[7])))",0,2000);
      fitfunc->SetParameters(MaxCount,Max,0.06*Max,0.25,0.0013,0.21*MaxCount,0.45*Max,0.086*Max,0.9,0.0061);
      fitfunc->SetNpx(500);

      histEBetalist[i]->Fit(fitfunc,"","",300,1400);

      Peak->SetPoint(i,i+1,fitfunc->GetParameter(1));
      Peak->SetPointError(i,0,fitfunc->GetParError(1));
      Width->SetPoint(i,i+1,fitfunc->GetParameter(2));
      Width->SetPointError(i,0,fitfunc->GetParError(2));
    }
    //Draw
    TCanvas* c1 = new TCanvas("c1","c1",0,0,800,600);
    Peak->SetMarkerStyle(21);
    Peak->SetMarkerColor(kBlue);
    Peak->Draw("APL");
    TCanvas* c2 = new TCanvas("c2","c2",0,0,800,600);
    Width->SetMarkerStyle(21);
    Width->SetMarkerColor(kBlue);
    Width->Draw("APL");

    //Fit
    Peak->Fit("pol0");
    Width->Fit("pol0");

    string OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf(";
    c1->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf)";
    c2->SaveAs(OutputFileName.c_str());

    OutputFileName = HISTOGRAM_DIRECTORY +  "/UserMonitorHist_" + Object + "_" + label + ".root";
    TFile *OutFile = new TFile(OutputFileName.c_str(),"RECREATE");
    for (int i=0;i<N;i++){
      histEBetalist[i]->Write();
    }
    Peak->Write();
    Width->Write();
    c1->Write();
    c2->Write();
    OutFile->Close();
    delete OutFile;
  }else if (Object.compare("EMWPC")==0){
    TGraphErrors* Peak = new TGraphErrors();
    Peak->SetName("Peak");
    Peak->SetTitle("Peak");
    TGraphErrors* Width = new TGraphErrors();
    Width->SetName("Width");
    Width->SetTitle("Width");
    TH1** histEMWPClist = new TH1*[N];
    for (int i=0;i<N;i++){
      histEMWPClist[i] = (TH1*)(ExpHistBox["MWPC_Anode"].Hist1DList[i][0]->Clone());
      int MaxBin = histEMWPClist[i]->GetMaximumBin();
      double Max = histEMWPClist[i]->GetBinCenter(MaxBin);
      double MaxCount = histEMWPClist[i]->GetBinContent(MaxBin);

      histEMWPClist[i]->Fit("landau","","",2.0,6.5);
      double peakpos = histEMWPClist[i]->GetFunction("landau")->GetParameter(1);
      double sigma = histEMWPClist[i]->GetFunction("landau")->GetParameter(2);
      histEMWPClist[i]->Fit("landau","","",peakpos-1.5*sigma,peakpos+5.0*sigma);
      TF1* fitfunc = histEMWPClist[i]->GetFunction("landau");

      Peak->SetPoint(i,i+1,fitfunc->GetParameter(1));
      Peak->SetPointError(i,0,fitfunc->GetParError(1));
      Width->SetPoint(i,i+1,fitfunc->GetParameter(2));
      Width->SetPointError(i,0,fitfunc->GetParError(2));
    }
    //Draw
    TCanvas* c1 = new TCanvas("c1","c1",0,0,800,600);
    Peak->SetMarkerStyle(21);
    Peak->SetMarkerColor(kBlue);
    Peak->Draw("APL");
    TCanvas* c2 = new TCanvas("c2","c2",0,0,800,600);
    Width->SetMarkerStyle(21);
    Width->SetMarkerColor(kBlue);
    Width->Draw("APL");

    //Fit
    Peak->Fit("pol0");
    Width->Fit("pol0");

    string OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf(";
    c1->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitor_" + Object + "_" + label + ".pdf)";
    c2->SaveAs(OutputFileName.c_str());

    OutputFileName = HISTOGRAM_DIRECTORY +  "/UserMonitorHist_" + Object + "_" + label + ".root";
    TFile *OutFile = new TFile(OutputFileName.c_str(),"RECREATE");
    for (int i=0;i<N;i++){
      histEMWPClist[i]->Write();
    }
    Peak->Write();
    Width->Write();
    c1->Write();
    c2->Write();
    OutFile->Close();
    delete OutFile;
  }
  return 0;
}
/**********************************************************************************************/

int Analyzer::User_Monitor_Penning(string label,double PhotoionWidthX,double PhotoionWidthY)
{
  int nx,ny;
  int N = HistDim1; 
//int N = HistDim1>HistDim2 ? HistDim1 : HistDim2; 

    TGraphErrors* gPosX = new TGraphErrors();
    gPosX->SetName("MOT_Pos_X");
    gPosX->SetTitle("MOT_Pos_X");
    TGraphErrors* gPosY = new TGraphErrors();
    gPosY->SetName("MOT_Pos_Y");
    gPosY->SetTitle("MOT_Pos_Y");
    TGraphErrors* gWidthX = new TGraphErrors();
    gWidthX->SetName("MOT_Width_X");
    gWidthX->SetTitle("MOT_Width_X");
    TGraphErrors* gWidthY = new TGraphErrors();
    gWidthY->SetName("MOT_Width_Y");
    gWidthY->SetTitle("MOT_Width_Y");

    TGraphErrors* gPosXN = new TGraphErrors();
    gPosXN->SetName("MOT_Pos_X_Narrow");
    gPosXN->SetTitle("MOT_Pos_X_Narrow");
    TGraphErrors* gPosYN = new TGraphErrors();
    gPosYN->SetName("MOT_Pos_Y_Narrow");
    gPosYN->SetTitle("MOT_Pos_Y_Narrow");
    TGraphErrors* gWidthXN = new TGraphErrors();
    gWidthXN->SetName("MOT_Width_X_Narrow");
    gWidthXN->SetTitle("MOT_Width_X_Narrow");
    TGraphErrors* gWidthYN = new TGraphErrors();
    gWidthYN->SetName("MOT_Width_Y_Narrow");
    gWidthYN->SetTitle("MOT_Width_Y_Narrow");

    TGraphErrors* gRateX = new TGraphErrors();
    gRateX->SetName("Rate_Photoion_vs_NarrowPeak_from_px");
    gRateX->SetTitle("Rate_Photoion_vs_NarrowPeak_from_px");
    TGraphErrors* gRateY = new TGraphErrors();
    gRateX->SetName("Rate_Photoion_vs_NarrowPeak_from_py");
    gRateX->SetTitle("Rate_Photoion_vs_NarrowPeak_from_py");
    TGraphErrors* gRateRatio = new TGraphErrors();
    gRateRatio->SetName("Rate_PenningIon/Photoion_form_px");
    gRateRatio->SetTitle("Rate_PenningIon/Photoion_from_px");


    TH1** histXlist = new TH1*[N];
    TH1** histYlist = new TH1*[N];
    TF1** FitFuncXlist = new TF1*[N];
    TF1** FitFuncYlist = new TF1*[N];
    if (PhotoionWidthX>0&&PhotoionWidthY>0){
    for (int i=0;i<N;i++){

      nx = ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->GetXaxis()->GetNbins();
      histXlist[i] = (TH1*)(ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->ProjectionX("_px",1,nx)->Clone());
      gStyle->SetOptStat(1111111);
      //      histXlist[i] = (TH1*)(h_MOTImageBkgSub->ProjectionX()->Clone());
      double BkgX;
      if (histXlist[i]->Integral(histXlist[i]->FindBin(-5),histXlist[i]->FindBin(-3))>0){
	histXlist[i]->Fit("pol0","WW","",-5,-3);
	BkgX = histXlist[i]->GetFunction("pol0")->GetParameter(0);
      }else{
	BkgX=0;
      }
      int MaxBin = histXlist[i]->GetMaximumBin();
      double Max = histXlist[i]->GetBinCenter(MaxBin);
      double Peak = histXlist[i]->GetMaximum();
      FitFuncXlist[i] = new TF1(Form("FitFuncX_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]*exp(-pow(x-[4],2.0)/2.0/pow([5],2.0))+[6]",Max-3,Max+3);
      FitFuncXlist[i]->SetParameters(Peak,Max,0.4,Peak,Max,PhotoionWidthX,BkgX);
//      FitFuncXlist[i]->FixParameter(6,BkgX);
      FitFuncXlist[i]->FixParameter(5,PhotoionWidthX);
//      FitFuncXlist[i] = new TF1(Form("FitFuncX_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))",Max-3,Max+3);
//      FitFuncXlist[i]->SetParameters(Peak,Max,0.6);
      FitFuncXlist[i]->SetParLimits(2,0,2.0);
      histXlist[i]->Fit(FitFuncXlist[i],"","",Max-3,Max+3);
//      histXlist[i]->Fit("gaus","","",Max-1.5,Max+1.5);

      gPosX->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(1));
      gPosX->SetPointError(i,0,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(1));
      gWidthX->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(2));
      gWidthX->SetPointError(i,0,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(2));

      ny = ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->GetYaxis()->GetNbins();
      histYlist[i] = (TH1*)(ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->ProjectionY("_py",1,ny)->Clone());
      gStyle->SetOptStat(1111111);
      //      histYlist[i] = (TH1*)(h_MOTImageBkgSub->ProjectionY()->Clone());
      double BkgY;
      if (histYlist[i]->Integral(histYlist[i]->FindBin(-5),histYlist[i]->FindBin(-3))>0){
	histYlist[i]->Fit("pol0","WW","",-5,-3);
	BkgY = histYlist[i]->GetFunction("pol0")->GetParameter(0);
      }else{
	BkgY=0;
      }
      MaxBin = histYlist[i]->GetMaximumBin();
      Max = histYlist[i]->GetBinCenter(MaxBin);
      Peak = histYlist[i]->GetMaximum();
      FitFuncYlist[i] = new TF1(Form("FitFuncY_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]*exp(-pow(x-[4],2.0)/2.0/pow([5],2.0))+[6]",Max-3,Max+3);
      FitFuncYlist[i]->SetParameters(Peak,Max,0.4,Peak,Max,PhotoionWidthY,BkgY);
//      FitFuncYlist[i]->FixParameter(6,BkgY);
      FitFuncYlist[i]->FixParameter(5,PhotoionWidthY);
//      FitFuncYlist[i] = new TF1(Form("FitFuncY_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))",Max-3,Max+3);
//      FitFuncYlist[i]->SetParameters(Peak,Max,0.6);
      FitFuncYlist[i]->SetParLimits(2,0,2.0);
      histYlist[i]->Fit(FitFuncYlist[i],"","",Max-3,Max+3);
//      histYlist[i]->Fit("gaus","","",Max-1.5,Max+1.5);

      gPosY->SetPoint(i,i+1,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(1));
      gPosY->SetPointError(i,0,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(1));
      gWidthY->SetPoint(i,i+1,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(2));
      gWidthY->SetPointError(i,0,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(2));
    }

    //Draw
    TCanvas* c1 = new TCanvas("c1","c1",0,0,800,600);
    gPosX->SetMarkerStyle(21);
    gPosX->SetMarkerColor(kBlue);
    gPosX->Draw("APL");
    TCanvas* c2 = new TCanvas("c2","c2",0,0,800,600);
    gPosY->SetMarkerStyle(21);
    gPosY->SetMarkerColor(kRed);
    gPosY->Draw("APL");
    TCanvas* c3 = new TCanvas("c3","c3",0,0,800,600);
    gWidthX->SetMarkerStyle(21);
    gWidthX->SetMarkerColor(kBlue);
    gWidthX->Draw("APL");
    TCanvas* c4 = new TCanvas("c4","c4",0,0,800,600);
    gWidthY->SetMarkerStyle(21);
    gWidthY->SetMarkerColor(kRed);
    gWidthY->Draw("APL");

    //Fit
    gPosX->Fit("pol0");
    gPosY->Fit("pol0");
    gWidthX->Fit("pol0");
    gWidthY->Fit("pol0");

    string OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf(";
    c1->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c2->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c3->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf)";
    c4->SaveAs(OutputFileName.c_str());

    OutputFileName = HISTOGRAM_DIRECTORY +  "/UserMonitorPenningHist_" + label + ".root";
    TFile *OutFile = new TFile(OutputFileName.c_str(),"RECREATE");
    for (int i=0;i<N;i++){
      histXlist[i]->Write();
      histYlist[i]->Write();
    }
    gPosX->Write();
    gPosY->Write();
    gWidthX->Write();
    gWidthY->Write();
    c1->Write();
    c2->Write();
    c3->Write();
    c4->Write();
    OutFile->Close();
    delete OutFile;
    
    }else if(PhotoionWidthX*PhotoionWidthY>100){//If either one of PhotoionWidthX and PhotoionWidthY is zero or minus, both of them are not fixed

      double runtime[N];
      double photoXrate[N];
      double photoYrate[N];

      ifstream intime("RunTime.txt");
      ifstream inphoto("PhotoionRate.txt");

      for (int i=0;i<N;i++){
intime>>runtime[i];
inphoto>>photoXrate[i];
inphoto>>photoYrate[i];

      nx = ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->GetXaxis()->GetNbins();
      histXlist[i] = (TH1*)(ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->ProjectionX("_px",1,nx)->Clone());
      gStyle->SetOptStat(1111111);
      //      histXlist[i] = (TH1*)(h_MOTImageBkgSub->ProjectionX()->Clone());
      ny = ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->GetYaxis()->GetNbins();
      histYlist[i] = (TH1*)(ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->ProjectionY("_py",1,ny)->Clone());
      gStyle->SetOptStat(1111111);
      //      histYlist[i] = (TH1*)(h_MOTImageBkgSub->ProjectionY()->Clone());

      double BkgX;
      if (histXlist[i]->Integral(histXlist[i]->FindBin(-5),histXlist[i]->FindBin(-3))>0){
	histXlist[i]->Fit("pol0","WW","",-5,-3);
	BkgX = histXlist[i]->GetFunction("pol0")->GetParameter(0);
      }else{
	BkgX=0;
      }
      int MaxBin = histXlist[i]->GetMaximumBin();
      double Max = histXlist[i]->GetBinCenter(MaxBin);
      double Peak = histXlist[i]->GetMaximum();
      int MaxBinOther = histYlist[i]->GetMaximumBin();
      histXlist[i] = (TH1*)(ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->ProjectionX("_px",histYlist[i]->FindBin(histYlist[i]->GetBinCenter(MaxBinOther)-0.5),histYlist[i]->FindBin(histYlist[i]->GetBinCenter(MaxBinOther)+0.5))->Clone());
      FitFuncXlist[i] = new TF1(Form("FitFuncX_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]*exp(-pow(x-[4],2.0)/2.0/pow([5],2.0))+[6]",Max-3,Max+3);
      FitFuncXlist[i]->SetParameters(Peak,Max,0.6,Peak,Max,0.10,BkgX);
//      FitFuncXlist[i]->FixParameter(6,BkgX);
//      FitFuncXlist[i] = new TF1(Form("FitFuncX_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))",Max-3,Max+3);
//      FitFuncXlist[i]->SetParameters(Peak,Max,0.6);
      FitFuncXlist[i]->SetParLimits(2,0,2.0);
      FitFuncXlist[i]->SetParLimits(5,0.03,0.3);
      histXlist[i]->Fit(FitFuncXlist[i],"","",Max-3,Max+3);
//      histXlist[i]->Fit("gaus","","",Max-1.5,Max+1.5);

      gPosX->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(1));
      gPosX->SetPointError(i,0,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(1));
      gWidthX->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(2));
      gWidthX->SetPointError(i,0,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(2));
      gPosXN->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(4));
      gPosXN->SetPointError(i,0,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(4));
      gWidthXN->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(5));
      gWidthXN->SetPointError(i,0,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(5));

      gRateX->SetPoint(i,photoXrate[i],histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(3)*histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(5)/runtime[i]);
      gRateX->SetPointError(i,0,(histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(3)*histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(5)+histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(5)*histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(3))/runtime[i]);
      gRateRatio->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(0)*histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(2)/runtime[i]/photoXrate[i]);

      double BkgY;
      if (histYlist[i]->Integral(histYlist[i]->FindBin(-5),histYlist[i]->FindBin(-3))>0){
	histYlist[i]->Fit("pol0","WW","",-5,-3);
	BkgY = histYlist[i]->GetFunction("pol0")->GetParameter(0);
      }else{
	BkgY=0;
      }
      MaxBin = histYlist[i]->GetMaximumBin();
      Max = histYlist[i]->GetBinCenter(MaxBin);
      Peak = histYlist[i]->GetMaximum();
      MaxBinOther = histXlist[i]->GetMaximumBin();
      histYlist[i] = (TH1*)(ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->ProjectionY("_py",histXlist[i]->FindBin(histXlist[i]->GetBinCenter(MaxBinOther)-0.5),histXlist[i]->FindBin(histXlist[i]->GetBinCenter(MaxBinOther)+0.5))->Clone());
      FitFuncYlist[i] = new TF1(Form("FitFuncY_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]*exp(-pow(x-[4],2.0)/2.0/pow([5],2.0))+[6]",Max-3,Max+3);
      FitFuncYlist[i]->SetParameters(Peak,Max,0.6,Peak,Max,0.10,BkgY);
//      FitFuncYlist[i]->FixParameter(6,BkgY);
//      FitFuncYlist[i] = new TF1(Form("FitFuncY_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))",Max-3,Max+3);
//      FitFuncYlist[i]->SetParameters(Peak,Max,0.6);
      FitFuncYlist[i]->SetParLimits(2,0,2.0);
      FitFuncYlist[i]->SetParLimits(5,0.03,0.3);
      histYlist[i]->Fit(FitFuncYlist[i],"","",Max-3,Max+3);
//      histYlist[i]->Fit("gaus","","",Max-1.5,Max+1.5);

      gPosY->SetPoint(i,i+1,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(1));
      gPosY->SetPointError(i,0,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(1));
      gWidthY->SetPoint(i,i+1,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(2));
      gWidthY->SetPointError(i,0,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(2));
      gPosYN->SetPoint(i,i+1,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(4));
      gPosYN->SetPointError(i,0,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(4));
      gWidthYN->SetPoint(i,i+1,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(5));
      gWidthYN->SetPointError(i,0,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(5));
      gRateY->SetPoint(i,photoYrate[i],histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(3)*histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(5)/runtime[i]);
      
      gRateY->SetPointError(i,0,(histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(3)*histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(5)+histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(5)*histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(3))/runtime[i]);

    }    

    //Draw
    TCanvas* c1 = new TCanvas("c1","c1",0,0,800,600);
    gPosX->SetMarkerStyle(21);
    gPosX->SetMarkerColor(kBlue);
    gPosX->Draw("APL");
    TCanvas* c2 = new TCanvas("c2","c2",0,0,800,600);
    gPosY->SetMarkerStyle(21);
    gPosY->SetMarkerColor(kRed);
    gPosY->Draw("APL");
    TCanvas* c3 = new TCanvas("c3","c3",0,0,800,600);
    gWidthX->SetMarkerStyle(21);
    gWidthX->SetMarkerColor(kBlue);
    gWidthX->Draw("APL");
    TCanvas* c4 = new TCanvas("c4","c4",0,0,800,600);
    gWidthY->SetMarkerStyle(21);
    gWidthY->SetMarkerColor(kRed);
    gWidthY->Draw("APL");

    TCanvas* c5 = new TCanvas("c5","c5",0,0,800,600);
    gPosXN->SetMarkerStyle(21);
    gPosXN->SetMarkerColor(kBlue);
    gPosXN->Draw("APL");
    TCanvas* c6 = new TCanvas("c6","c6",0,0,800,600);
    gPosYN->SetMarkerStyle(21);
    gPosYN->SetMarkerColor(kRed);
    gPosYN->Draw("APL");
    TCanvas* c7 = new TCanvas("c7","c7",0,0,800,600);
    gWidthXN->SetMarkerStyle(21);
    gWidthXN->SetMarkerColor(kBlue);
    gWidthXN->Draw("APL");
    TCanvas* c8 = new TCanvas("c8","c8",0,0,800,600);
    gWidthYN->SetMarkerStyle(21);
    gWidthYN->SetMarkerColor(kRed);
    gWidthYN->Draw("APL");

    TCanvas* c9 = new TCanvas("c9","c9",0,0,800,600);
    gRateX->SetMarkerStyle(21);
    gRateX->SetMarkerColor(kBlue);
    gRateX->Draw("AP");

    TCanvas* c10 = new TCanvas("c10","c10",0,0,800,600);
    gRateY->SetMarkerStyle(21);
    gRateY->SetMarkerColor(kRed);
    gRateY->Draw("AP");

    TCanvas* c11 = new TCanvas("c11","c11",0,0,800,600);
    gRateRatio->SetMarkerStyle(21);
    gRateRatio->SetMarkerColor(kBlue);
    gRateRatio->Draw("APL");

    //Fit
    gPosX->Fit("pol0");
    gPosY->Fit("pol0");
    gWidthX->Fit("pol0");
    gWidthY->Fit("pol0");
    gPosXN->Fit("pol0");
    gPosYN->Fit("pol0");
    gWidthXN->Fit("pol0");
    gWidthYN->Fit("pol0");

    TF1* f1=new TF1("f1","[0]*x^2");
    gRateX->Fit("f1");
    gRateY->Fit("f1");
    gRateRatio->Fit("pol1");

    string OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf(";
    c1->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c2->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c3->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c4->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c5->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c6->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c7->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c8->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c9->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c10->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf)";
    c11->SaveAs(OutputFileName.c_str());

    OutputFileName = HISTOGRAM_DIRECTORY +  "/UserMonitorPenningHist_" + label + ".root";
    TFile *OutFile = new TFile(OutputFileName.c_str(),"RECREATE");
    for (int i=0;i<N;i++){
      histXlist[i]->Write();
      histYlist[i]->Write();
    }
    gPosX->Write();
    gPosY->Write();
    gWidthX->Write();
    gWidthY->Write();
    gPosXN->Write();
    gPosYN->Write();
    gWidthXN->Write();
    gWidthYN->Write();
    gRateX->Write();
    gRateY->Write();
    gRateRatio->Write();
    c1->Write();
    c2->Write();
    c3->Write();
    c4->Write();
    c5->Write();
    c6->Write();
    c7->Write();
    c8->Write();
    c9->Write();
    c10->Write();
    c11->Write();
    OutFile->Close();
    delete OutFile;
/*    ofstream outfile("He6He6_Normalization.txt",ios::app);
    for(int i=0;i<N;i++){
        outfile<<histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(0)<<" "<<histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(2)<<" "<<histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(3)<<" "<<histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(5)<<" "<<histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(0)<<" "<<histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(2)<<" "<<histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(3)<<" "<<histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(5)<<endl;
    }
    outfile.close();*/
    }else{
      for (int i=0;i<N;i++){
      nx = ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->GetXaxis()->GetNbins();
      histXlist[i] = (TH1*)(ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->ProjectionX("_px",1,nx)->Clone());
      gStyle->SetOptStat(1111111);
      //      histXlist[i] = (TH1*)(h_MOTImageBkgSub->ProjectionX()->Clone());
      double BkgX;
      if (histXlist[i]->Integral(histXlist[i]->FindBin(-5),histXlist[i]->FindBin(-3))>0){
	histXlist[i]->Fit("pol0","WW","",-5,-3);
	BkgX = histXlist[i]->GetFunction("pol0")->GetParameter(0);
      }else{
	BkgX=0;
      }
      int MaxBin = histXlist[i]->GetMaximumBin();
      double Max = histXlist[i]->GetBinCenter(MaxBin);
      double Peak = histXlist[i]->GetMaximum();
      FitFuncXlist[i] = new TF1(Form("FitFuncX_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]*exp(-pow(x-[4],2.0)/2.0/pow([5],2.0))+[6]",Max-3,Max+3);
      FitFuncXlist[i]->SetParameters(Peak,Max,0.6,Peak,Max,0.10,BkgX);
//      FitFuncXlist[i]->FixParameter(6,BkgX);
//      FitFuncXlist[i] = new TF1(Form("FitFuncX_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))",Max-3,Max+3);
//      FitFuncXlist[i]->SetParameters(Peak,Max,0.6);
      FitFuncXlist[i]->SetParLimits(2,0,2.0);
      FitFuncXlist[i]->SetParLimits(5,0.03,0.3);
      histXlist[i]->Fit(FitFuncXlist[i],"","",Max-3,Max+3);
//      histXlist[i]->Fit("gaus","","",Max-1.5,Max+1.5);

      gPosX->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(1));
      gPosX->SetPointError(i,0,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(1));
      gWidthX->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(2));
      gWidthX->SetPointError(i,0,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(2));
      gPosXN->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(4));
      gPosXN->SetPointError(i,0,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(4));
      gWidthXN->SetPoint(i,i+1,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParameter(5));
      gWidthXN->SetPointError(i,0,histXlist[i]->GetFunction(Form("FitFuncX_%d",i))->GetParError(5));

      ny = ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->GetYaxis()->GetNbins();
      histYlist[i] = (TH1*)(ExpHistBox["MCP_Image_Zoom"].Hist2DList[i][0]->ProjectionY("_py",1,ny)->Clone());
      gStyle->SetOptStat(1111111);
      //      histYlist[i] = (TH1*)(h_MOTImageBkgSub->ProjectionY()->Clone());
      double BkgY;
      if (histYlist[i]->Integral(histYlist[i]->FindBin(-5),histYlist[i]->FindBin(-3))>0){
	histYlist[i]->Fit("pol0","WW","",-5,-3);
	BkgY = histYlist[i]->GetFunction("pol0")->GetParameter(0);
      }else{
	BkgY=0;
      }
      MaxBin = histYlist[i]->GetMaximumBin();
      Max = histYlist[i]->GetBinCenter(MaxBin);
      Peak = histYlist[i]->GetMaximum();
      FitFuncYlist[i] = new TF1(Form("FitFuncY_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))+[3]*exp(-pow(x-[4],2.0)/2.0/pow([5],2.0))+[6]",Max-3,Max+3);
      FitFuncYlist[i]->SetParameters(Peak,Max,0.6,Peak,Max,0.10,BkgY);
//      FitFuncYlist[i]->FixParameter(6,BkgY);
//      FitFuncYlist[i] = new TF1(Form("FitFuncY_%d",i),"[0]*exp(-pow(x-[1],2.0)/2.0/pow([2],2.0))",Max-3,Max+3);
//      FitFuncYlist[i]->SetParameters(Peak,Max,0.6);
      FitFuncYlist[i]->SetParLimits(2,0,2.0);
      FitFuncYlist[i]->SetParLimits(5,0.03,0.3);
      histYlist[i]->Fit(FitFuncYlist[i],"","",Max-3,Max+3);
//      histYlist[i]->Fit("gaus","","",Max-1.5,Max+1.5);

      gPosY->SetPoint(i,i+1,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(1));
      gPosY->SetPointError(i,0,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(1));
      gWidthY->SetPoint(i,i+1,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(2));
      gWidthY->SetPointError(i,0,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(2));
      gPosYN->SetPoint(i,i+1,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(4));
      gPosYN->SetPointError(i,0,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(4));
      gWidthYN->SetPoint(i,i+1,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParameter(5));
      gWidthYN->SetPointError(i,0,histYlist[i]->GetFunction(Form("FitFuncY_%d",i))->GetParError(5));

    }    

    //Draw
    TCanvas* c1 = new TCanvas("c1","c1",0,0,800,600);
    gPosX->SetMarkerStyle(21);
    gPosX->SetMarkerColor(kBlue);
    gPosX->Draw("APL");
    TCanvas* c2 = new TCanvas("c2","c2",0,0,800,600);
    gPosY->SetMarkerStyle(21);
    gPosY->SetMarkerColor(kRed);
    gPosY->Draw("APL");
    TCanvas* c3 = new TCanvas("c3","c3",0,0,800,600);
    gWidthX->SetMarkerStyle(21);
    gWidthX->SetMarkerColor(kBlue);
    gWidthX->Draw("APL");
    TCanvas* c4 = new TCanvas("c4","c4",0,0,800,600);
    gWidthY->SetMarkerStyle(21);
    gWidthY->SetMarkerColor(kRed);
    gWidthY->Draw("APL");

    TCanvas* c5 = new TCanvas("c5","c5",0,0,800,600);
    gPosXN->SetMarkerStyle(21);
    gPosXN->SetMarkerColor(kBlue);
    gPosXN->Draw("APL");
    TCanvas* c6 = new TCanvas("c6","c6",0,0,800,600);
    gPosYN->SetMarkerStyle(21);
    gPosYN->SetMarkerColor(kRed);
    gPosYN->Draw("APL");
    TCanvas* c7 = new TCanvas("c7","c7",0,0,800,600);
    gWidthXN->SetMarkerStyle(21);
    gWidthXN->SetMarkerColor(kBlue);
    gWidthXN->Draw("APL");
    TCanvas* c8 = new TCanvas("c8","c8",0,0,800,600);
    gWidthYN->SetMarkerStyle(21);
    gWidthYN->SetMarkerColor(kRed);
    gWidthYN->Draw("APL");

    //Fit
    gPosX->Fit("pol0");
    gPosY->Fit("pol0");
    gWidthX->Fit("pol0");
    gWidthY->Fit("pol0");
    gPosXN->Fit("pol0");
    gPosYN->Fit("pol0");
    gWidthXN->Fit("pol0");
    gWidthYN->Fit("pol0");

    string OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf(";
    c1->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c2->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c3->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c4->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c5->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c6->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf";
    c7->SaveAs(OutputFileName.c_str());
    OutputFileName = SPECTRA_DIRECTORY + "/UserMonitorPenning_" + label + ".pdf)";
    c8->SaveAs(OutputFileName.c_str());

    OutputFileName = HISTOGRAM_DIRECTORY +  "/UserMonitorPenningHist_" + label + ".root";
    TFile *OutFile = new TFile(OutputFileName.c_str(),"RECREATE");
    for (int i=0;i<N;i++){
      histXlist[i]->Write();
      histYlist[i]->Write();
    }
    gPosX->Write();
    gPosY->Write();
    gWidthX->Write();
    gWidthY->Write();
    gPosXN->Write();
    gPosYN->Write();
    gWidthXN->Write();
    gWidthYN->Write();
    c1->Write();
    c2->Write();
    c3->Write();
    c4->Write();
    c5->Write();
    c6->Write();
    c7->Write();
    c8->Write();
    OutFile->Close();
    delete OutFile;
    }

  return 0;
}
/**********************************************************************************************/
//Charge state 3 analysis
int Analyzer::User_ChargeState3Ana(double KeptRatio, string label, string opt)
{
  //Check Bkg status
  if (((ExtBkgStatus&(1))==0) || ((ExtBkgStatus&(1<<1))==0)){
    cout << "Bkg histograms are not properly loaded."<<endl;
    return -1;
  }
  TH1* histTOF = (TH1*)(ExpHistBox["TOFHist"].Hist1DList[0][0]->Clone());;
  TH1* histBkg;
  if (opt.compare("QCutOFF")){
    histBkg = (TH1*)(ExtBkgHistBox["Q_Cut_ON"]->Clone());
  }else{
    histBkg = (TH1*)(ExtBkgHistBox["All"]->Clone());
  }
  double TOFBound23 = ExpParameters["TOFBound23"][0];

  double SignalCount = histTOF->Integral(histTOF->FindBin(110.0),histTOF->FindBin(TOFBound23));
  double BkgCount = histBkg->Integral(histBkg->FindBin(110.0),histBkg->FindBin(TOFBound23));
  double SignalCountAll = histTOF->Integral(histTOF->FindBin(110.0),histTOF->FindBin(350.0));
  double BkgCountAll = histBkg->Integral(histBkg->FindBin(110.0),histBkg->FindBin(350.0));

  double SignalCountBkgSub = SignalCount - BkgNorm*BkgCount;
  double SignalUncertainty = sqrt(SignalCount + BkgNorm*BkgNorm*BkgCount);
  double SignalCountAllBkgSub = SignalCountAll - BkgNorm*BkgCountAll;
  double SignalUncertaintyAll = sqrt(SignalCountAll + BkgNorm*BkgNorm*BkgCountAll);

  double P3 = (SignalCountBkgSub/KeptRatio)/(SignalCountAllBkgSub);
  double P3_Error = abs(P3)*sqrt(pow((SignalUncertainty/SignalCountBkgSub),2.0)+pow(SignalUncertaintyAll/SignalCountAllBkgSub,2.0))/KeptRatio; 

  cout << "Charge-state 3 probability = "<<P3 <<" +/- " << P3_Error<<endl;

  //Scale the bkg histogram before output
  histBkg->Scale(BkgNorm);

  //Output
  string OutputFileName = SPECTRA_DIRECTORY + "/UserChargeState3Ana_" + label + ".pdf";
  //c2->SaveAs(OutputFileName.c_str());

  OutputFileName = HISTOGRAM_DIRECTORY +  "/UserChargeState3Ana_" + label + ".root";
  TFile *OutFile = new TFile(OutputFileName.c_str(),"RECREATE");
  histTOF->Write();
  histBkg->Write();
  OutFile->Close();
  delete OutFile;
  return 0;
}
/**********************************************************************************************/
//Charge state 1 and 2 analysis
int Analyzer::User_ChargeState12Ana(string label)
{
  TH1* histEIon1;
  TH1* histEIon1Bkg;
  TH1* histEIon2;
  TH1* histEIon2Bkg;
  TH1* histRatio; 
  TH1* histRes;
  double N1;
  double N2;
  double B1;
  double B2;
  double ratio;
  double error1;
  double error2;
  double errortot;
  double error;
  if (Purpose==0){
    histEIon1 = (TH1*)(SimHistBox["EIon1"].Hist1DList[0][0]->Clone());
    histEIon2 = (TH1*)(SimHistBox["EIon2"].Hist1DList[0][0]->Clone());
    histEIon1->Rebin(5);
    histEIon2->Rebin(5);
    N1 = histEIon1->Integral();
    N2 = histEIon2->Integral();
    ratio = (histEIon2->Integral())/(histEIon2->Integral()+histEIon1->Integral());
    error1 = sqrt(N1);
    error2 = sqrt(N2);
    errortot = sqrt(N1+N2+B1*BkgNorm+B2*BkgNorm);
    error = ratio*sqrt(pow(error2/N2,2.0)+pow(errortot/(N1+N2),2.0));
  }else if (Purpose==1){
    //Check Bkg status
    if (((ExtBkgStatus&(1))==0) || ((ExtBkgStatus&(1<<1))==0)){
      cout << "Bkg histograms are not properly loaded."<<endl;
      return -1;
    }
    histEIon1 = (TH1*)(ExpHistBox["EIon1"].Hist1DList[0][0]->Clone());
    histEIon1Bkg = (TH1*)(ExtBkgHistBox["EIon1"]->Clone());
    histEIon2 = (TH1*)(ExpHistBox["EIon2"].Hist1DList[0][0]->Clone());
    histEIon2Bkg = (TH1*)(ExtBkgHistBox["EIon2"]->Clone());

    histEIon1Bkg->Scale(BkgNorm);
    histEIon2Bkg->Scale(BkgNorm);

    histEIon1->Rebin(5);
    histEIon1Bkg->Rebin(5);
    histEIon2->Rebin(5);
    histEIon2Bkg->Rebin(5);

    N1 = histEIon1->Integral();
    N2 = histEIon2->Integral();
    B1 = histEIon1Bkg->Integral();
    B2 = histEIon2Bkg->Integral();
    ratio = (histEIon2->Integral()-histEIon2Bkg->Integral())/(histEIon2->Integral()-histEIon2Bkg->Integral()+histEIon1->Integral()-histEIon1Bkg->Integral());
    error1 = sqrt(N1+B1*BkgNorm);
    error2 = sqrt(N2+B2*BkgNorm);
    errortot = sqrt(N1+N2+B1*BkgNorm+B2*BkgNorm);
    error = ratio*sqrt(pow(error2/(N2-B2),2.0)+pow(errortot/(N1-B1+N2-B2),2.0));

    histEIon1->Add(histEIon1Bkg,-1.0);
    histEIon2->Add(histEIon2Bkg,-1.0);
  }

  int N=histEIon1->GetNbinsX();
  histRatio = new TH1D("histratio","hist ratio",N,0,1.6);
  histRes = new TH1D("histRes","hist Res",N,0,1.6);

  for (int i=1;i<=N;i++)
  {
    if (histEIon1->GetBinContent(i)!=0 && histEIon2->GetBinContent(i)!=0){
      histRatio->SetBinContent(i,histEIon2->GetBinContent(i)/histEIon1->GetBinContent(i));
      //    cout <<histEIon2->GetBinContent(i)/histEIon1->GetBinContent(i)<<endl;
      //      histRatio->SetBinError(i,histEIon2->GetBinError(i)/histEIon1->GetBinContent(i));
      double error1 = histEIon1->GetBinError(i)/histEIon1->GetBinContent(i);
      double error2 = histEIon2->GetBinError(i)/histEIon2->GetBinContent(i);

      histRatio->SetBinError(i,histEIon2->GetBinContent(i)/histEIon1->GetBinContent(i)*sqrt(error1*error1+error2*error2));
    }
  }

  TF1* fitfunc = new TF1("fitfunc","([0]+[1]*x)/(1.0-[0]-[1]*x)",0,1.4);
  fitfunc->SetParameters(0.5,0.2);
  histRatio->Fit(fitfunc,"","",0.1,1.1);
  cout <<"Chi2 = "<< fitfunc->GetChisquare()<<endl;
  cout <<"NDF = "<< fitfunc->GetNDF()<<endl;
  for (int i=1;i<=N;i++)
  {
    if (histRatio->GetBinError(i)>0){
      histRes->SetBinContent(i,(histRatio->GetBinContent(i)-fitfunc->Eval(histRatio->GetBinCenter(i)))/histRatio->GetBinError(i));
    }
  }

  cout <<"Charge 2 total : "<< histEIon2->Integral()<<endl;
  cout <<"Charge 1 total : "<< histEIon1->Integral()<<endl;
  cout <<"Charge 2 portion: "<<  histEIon2->Integral()/( histEIon2->Integral()+ histEIon1->Integral())<<endl;
  cout <<"Charge 2 portion uncertainty: "<< error<<endl;


  //Output
  string OutputFileName = SPECTRA_DIRECTORY + "/UserChargeState12Ana_" + label + ".pdf";
  //Canvas

  OutputFileName = HISTOGRAM_DIRECTORY +  "/UserChargeState12Ana_" + label + ".root";
  TFile *OutFile = new TFile(OutputFileName.c_str(),"RECREATE");
  histEIon1->Write();
  histEIon2->Write();
  if (Purpose==1){
    histEIon1Bkg->Write();
    histEIon2Bkg->Write();
  }
  histRatio->Write();
  histRes->Write();
  OutFile->Close();
  delete OutFile;
  return 0;
}
/**********************************************************************************************/
int Analyzer::User_RawDataStats(){
   TFile* tfilein = NULL; //pointer to input root file
  TTree * InTreeTriple = NULL; //input tree pointer
  TTree * InTreeDouble = NULL; //input tree pointer
  TTree * InTreeMCP = NULL; //input tree pointer
  int N_MCP = 0;
  int N_Double=0;
  int N_Triple=0;
  double Runtime=0;
  int iFile=0;
  int jFile=0;
  char filename[500];
  ostringstream stm1;

  stm1 <<"Filename"<<"\t"<<"Runtime"<<"\t"<<"N_Triple"<<"\t"<<"N_Double"<<"\t"<<"N_MCP"<<"\t"<<"TripleRate"<<"\t"<<"DoubleRate"<<"\t"<<"MCPRate";
  cout<<stm1.str()<<endl;

  for (iFile=0;iFile<nFileExp1;iFile++){  //File loop i starts
    sprintf(filename,"%s/%s_%04d.root",EXP_DATA_DIRECTORY.c_str(),ExpFilePrefix.c_str(),FileIDListExp[iFile][jFile]);
    if(OpenExpInputFile(filename,tfilein)==-1) return -1;

    InTreeTriple = (TTree*)tfilein->Get("Tree_Triple");
    N_Triple = InTreeTriple->GetEntries();
    InTreeDouble = (TTree*)tfilein->Get("Tree_Double");
    N_Double = InTreeDouble->GetEntries();
    InTreeMCP = (TTree*)tfilein->Get("Tree_MCP");
    N_MCP = InTreeMCP->GetEntries();

    InTreeMCP->SetBranchAddress("GroupTime",&Runtime);
    InTreeMCP->GetEntry(N_MCP-1);

    sprintf(filename,"%s_%04d.root",ExpFilePrefix.c_str(),FileIDListExp[iFile][jFile]);
    ostringstream stm;
    stm <<filename<<"\t"<<Runtime<<"\t"<<N_Triple<<"\t"<<N_Double<<"\t"<<N_MCP<<"\t"<<N_Triple/Runtime<<"\t"<<N_Double/Runtime<<"\t"<<N_MCP/Runtime;
    cout<< stm.str() <<endl;
    if (tfilein!=NULL){
      tfilein->Close();
      delete tfilein;
      tfilein =NULL;
    }

  }
  return 0;
}




