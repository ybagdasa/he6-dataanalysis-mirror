#include <iostream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream> 
#include <vector>
#include <stdint.h>


//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TSpline.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TGraphErrors.h"
#include "TVectorD.h"
#include "TMatrixD.h"
#include "TMatrixDEigen.h"
#include "TEllipse.h"
#include "TF1.h"
#include "TF2.h"
//Analyzer includes
#include "Analyzer.h"
#include "Calibration.h"
#include "GlobalConstants.h"

using namespace std;

//Function and struct declarations are moved to Calibration.h and Datastruct.h

/***********************************************************************************/
int Calibration::ConfigCalMCP(string Mode,double ShiftX,double ShiftY,double ScaleX, double ScaleY, double CutR){
  PreShiftX=ShiftX;
  PreShiftY=ShiftY;
  PreScaleX=ScaleX;
  PreScaleY=ScaleY;
  PreCutR=CutR;
  GridMode=Mode;
  if (Mode.compare("Global")!=0 && Mode.compare("Local")!=0){
    cout << "Only Global and Local are allowed mode."<<endl;
    GridMode="Global";
  }
  return 0;
}

/***********************************************************************************/
int Calibration::CalibratePreviewMCP(string FilePrefix,int FileID)
{
  CurrentAnalyzer->Calibrating = true;
  string spectrumfile;

  ExpDataStruct ExpData;      //Data struct for exp data reading
  RecDataStruct RecData;      //Reconstructed data struct
  TFile* tfilein = NULL; //pointer to input root file
  TTree * The_Tree = NULL; //input tree pointer
  CurrentAnalyzer->ConstructExpParameterList();
  CurrentAnalyzer->CheckExpConditionSettings();

  //Histograms
  TH1* h_QMCP = new TH1D("h_QMCP","QMCP",500,0,250000);
  TH1* h_QX1 = new TH1D("h_QX1","QX1",500,0,150000);
  TH1* h_QX2 = new TH1D("h_QX2","QX2",500,0,150000);
  TH1* h_QY1 = new TH1D("h_QY1","QY1",500,0,150000);
  TH1* h_QY2 = new TH1D("h_QY2","QY2",500,0,150000);

  TH2* h_QX1QX2 = new TH2D("h_QX1QX2","h_QX1QX2",500,0,150000,500,0,150000);
  TH2* h_QY1QY2 = new TH2D("h_QY1QY2","h_QY1QY2",500,0,150000,500,0,150000);
  TH2* h_QXQY = new TH2D("h_QXQY","h_QXQY",500,0,300000,500,0,300000);
  TH2* h_QXYQMCP = new TH2D("h_QXYQMCP","h_QXYQMCP",500,0,300000,500,0,250000);

  TH2* h_MCPImage = new TH2D("h_MCPImage","MCPImage",450,-45,45,450,-45,45);
  TH2* h_MCPImage_Cond = new TH2D("h_MCPImage_Cond","MCPImage Conditioned",450,-45,45,450,-45,45);
  TH2* h_MCPImage_CondQ = new TH2D("h_MCPImage_CondQ","MCPImage Q-Conditioned",450,-45,45,450,-45,45);
  TH2* h_MCPBrightness = new TH2D("h_MCPBrightness","MCPBrightness",450,-45,45,450,-45,45);
  TH1* h_T1T2X = new TH1D("h_T1T2X","TX1+TX2",500,0,200);
  TH1* h_T1T2Y = new TH1D("h_T1T2Y","TY1+TY2",500,0,200);
  TH2* h_TXSum_vs_X = new TH2D("TXSum_vs_X","TXSum_vs_X",500,0,200,450,-45,45);
  TH2* h_TYSum_vs_Y = new TH2D("TYSum_vs_Y","TYSum_vs_Y",500,0,200,450,-45,45);

  TH1* h_TOF = new TH1D("h_TOF","TOF",8000,-4000,4000);
  TH1* h_TimeBetween = new TH1D("h_TimeBetween","h_TimeBetween",1000,0,100);
  
  //Read In Data
  if(CurrentAnalyzer->ReadMode.compare("MCP")!=0 && CurrentAnalyzer->ReadMode.compare("Double")!=0 && CurrentAnalyzer->ReadMode.compare("Triple")!=0){
      cout <<"Only MCP Triple Double modes are allowed."<<endl;
    return -1;
  }
  
  ostringstream convert;
  convert << setfill('0') << setw(4)<<FileID;
  string fName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + FilePrefix + string("_") + convert.str();
  fName += ".root";
  if(CurrentAnalyzer->OpenExpInputFile(fName.c_str(),tfilein)==-1) return -1;
  CurrentAnalyzer->SetExpInTreeAddress(tfilein,The_Tree,ExpData);
  cout << "Reading file "<<fName<<endl;
  int N_MCP = The_Tree->GetEntries();
  int N_Keep = 0;
  int N_Pass = 0;
  double QMCPThreshold = 20000;

  double LastT=0;
  double TimeBetween=0;
  for(int i=0;i<N_MCP;i++){
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    //Read Tree
    The_Tree->GetEntry(i);
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);
    
    TimeBetween = (ExpData.GroupTime-LastT)*1000000;
//    cout << (ExpData.GroupTime-LastT)*1000000<<endl;;
    h_TimeBetween->Fill(TimeBetween);
    LastT = ExpData.GroupTime;

    h_TOF->Fill(RecData.TOF);
    if (((ExpData.TriggerMap&(1<<3))==0)||((ExpData.TriggerMap&(1<<4))==0))continue;
    N_Pass++;
    bool TOFcond;
    TOFcond = RecData.TOF>CurrentAnalyzer->TOFLow && RecData.TOF<CurrentAnalyzer->TOFHigh;
    if (CurrentAnalyzer->ReadMode.compare("Triple")==0 || CurrentAnalyzer->ReadMode.compare("Double")==0){
      if (!TOFcond)continue;
    }
    //Fill Histograms
    h_T1T2X->Fill(RecData.MCP_TXSum);
    h_T1T2Y->Fill(RecData.MCP_TYSum);
    h_TXSum_vs_X->Fill(RecData.MCP_TXSum,RecData.MCPPos_X); 
    h_TYSum_vs_Y->Fill(RecData.MCP_TYSum,RecData.MCPPos_Y); 
    h_MCPImage->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);


    bool cond;
    cond = RecData.MCP_TXSum>CurrentAnalyzer->TXSumLow && RecData.MCP_TXSum<CurrentAnalyzer->TXSumHigh && RecData.MCP_TYSum>CurrentAnalyzer->TYSumLow && RecData.MCP_TYSum<CurrentAnalyzer->TYSumHigh;
    bool condT;
    condT = TimeBetween>20;
    bool condQ;
    condQ = ExpData.QMCP>QMCPThreshold;

    if(cond){
      h_MCPImage_Cond->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
      h_MCPBrightness->Fill(RecData.MCPPos_X,RecData.MCPPos_Y,ExpData.QMCP);
      if(condQ)h_MCPImage_CondQ->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
      h_QMCP->Fill(ExpData.QMCP);
      h_QX1->Fill(ExpData.QMCP_anodes.QX1);
      h_QX2->Fill(ExpData.QMCP_anodes.QX2);
      h_QY1->Fill(ExpData.QMCP_anodes.QY1);
      h_QY2->Fill(ExpData.QMCP_anodes.QY2);
      h_QX1QX2->Fill(ExpData.QMCP_anodes.QX1,ExpData.QMCP_anodes.QX2); 
      h_QY1QY2->Fill(ExpData.QMCP_anodes.QY1,ExpData.QMCP_anodes.QY2);
      h_QXQY->Fill(ExpData.QMCP_anodes.QX1+ExpData.QMCP_anodes.QX2,ExpData.QMCP_anodes.QY1+ExpData.QMCP_anodes.QY2);
      h_QXYQMCP->Fill(ExpData.QMCP_anodes.QX1+ExpData.QMCP_anodes.QX2+ExpData.QMCP_anodes.QY1+ExpData.QMCP_anodes.QY2,ExpData.QMCP);
      N_Keep++;
    }
  }
  //Write to root file
  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_MCPPreview.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");

  h_TOF->Write();
  h_TimeBetween->Write();

  h_QMCP->Write();
  h_QX1->Write();
  h_QX2->Write();
  h_QY1->Write();
  h_QY2->Write();
  h_QX1QX2->Write(); 
  h_QY1QY2->Write();
  h_QXQY->Write();
  h_QXYQMCP->Write();

  h_MCPImage->Write();
  h_MCPImage_Cond->Write();
  h_MCPImage_CondQ->Write();
  h_MCPBrightness->Write();
  h_T1T2X->Write();
  h_T1T2Y->Write();
  h_TXSum_vs_X->Write();
  h_TYSum_vs_Y->Write();

  histfile->Close();
  delete h_QMCP;
  delete h_QX1;
  delete h_QX2;
  delete h_QY1;
  delete h_QY2;
  delete h_QX1QX2;
  delete h_QY1QY2;
  delete h_QXQY;
  delete h_QXYQMCP;

  delete h_MCPImage;
  delete h_MCPImage_Cond;
  delete h_T1T2X;
  delete h_T1T2Y;
  delete h_TXSum_vs_X;
  delete h_TYSum_vs_Y;
  delete histfile;

  cout << "Kept ratio (T1T2): "<<double(N_Keep)/double(N_MCP)<<endl;
  cout << "Pass ratio (All anode triggers): "<<double(N_Pass)/double(N_MCP)<<endl;
  //Close input file
  if (tfilein!=NULL){
    tfilein->Close();
    delete tfilein;
    tfilein =NULL;
  }
  return 0;
}

/***********************************************************************************/
int Calibration::CalibrateMCP(string FilePrefix,int FileID,int CalID,string Reference)
{
  if(CurrentAnalyzer->ReadMode.compare("MCP")!=0){
      cout <<"Only MCP mode is allowed."<<endl;
    return -1;
  }
  
  CurrentAnalyzer->Calibrating = true;
  string spectrumfile;

  ExpDataStruct ExpData;      //Data struct for exp data reading
  RecDataStruct RecData;      //Reconstructed data struct
  TFile* tfilein = NULL; //pointer to input root file
  TTree * Tree_MCP = NULL; //input tree pointer
  CurrentAnalyzer->ConstructExpParameterList();
  CurrentAnalyzer->CheckExpConditionSettings();
  CurrentAnalyzer->LogExpConditionList();

  double ShiftX;		//shift to match the outer circle
  double ShiftY;
  double ScaleX;		//Scale from ellipse to circle
  double ScaleY;
  double MOTX;			//Find the new MOT position
  double MOTY;
  double CountThreshold = 500;
  double DelayLineWidth = 75.0;
  double RotationAngle = 0.3927;	//22.5 deg
  double MaskRadius = 37.485;
  int NPointEdge = 5;
  
  //Histograms
  TH2* h_MCPImage = new TH2D("h_MCPImage","MCPImage",2000,-50,50,2000,-50,50);
  TH1* h_MCPImageProjectX;
  TH1* h_MCPImageProjectY;
  TH1* h_T1T2X = new TH1D("h_T1T2X","TX1+TX2",1000,-200,200);
  TH1* h_T1T2Y = new TH1D("h_T1T2Y","TY1+TY2",1000,-200,200);
  
  TH2* h_MCPImage_PreCal = new TH2D("h_MCPImage_PreCal","MCPImage PreCalibrated",2000,-50,50,2000,-50,50);
  TH2* h_MCPImage_2ndCal = new TH2D("h_MCPImage_2ndCal","MCPImage Further Calibrated",10000,-50,50,10000,-50,50);
  
  TH2* h_MCPImage_CalLowRes = new TH2D("h_MCPImage_CalLowRes","MCPImage Further Calibrated Low Res",500,-50,50,500,-50,50);
  TH2* h_MCPImage_CalZoom = new TH2D("h_MCPImage_CalZoom","MCPImage Further Calibrated Zoom",400,-8,8,400,-8,8);
  TH2* h_MCPImage_CalZoom2 = new TH2D("h_MCPImage_CalZoom2","MCPImage Further Calibrated Zoom2",400,-38,-22,400,8,24);
  TH2* h_MCPImage_Cal = new TH2D("h_MCPImage_Cal","MCPImage Calibrated",1000,-50,50,1000,-50,50);
  TH2* h_MCPImage_Center = new TH2D("h_MCPImage_Center","MCPImage Calibrated Center",100,-5,5,100,-5,5);
  TH1* h_CenterX = new TH1D("h_CenterX","Center X Projection",100,-5.0,5.0);
  TH1* h_CenterY = new TH1D("h_CenterY","Center Y Projection",100,-5.0,5.0);

  //For circle calibration
  TH1** h_RimSearch = new TH1*[20];
  for (int i=0;i<20;i++){
    h_RimSearch[i] = new TH1D(Form("Projection%dDeg",i),Form("Projection%dDeg",i),2000,-50,50);
  }

  //Graph
  TGraph* Rim = new TGraph();
  Rim->SetName("Rim");
  Rim->SetTitle("Rim");

  TGraph* Grid = new TGraph();
  Grid->SetName("Grid");
  Grid->SetTitle("Grid");

  TGraph* OriginalGrid = new TGraph();
  OriginalGrid->SetName("OriginalGrid");
  OriginalGrid->SetTitle("OriginalGrid");

  TGraph* TrueGrid = new TGraph();
  TrueGrid->SetName("TrueGrid");
  TrueGrid->SetTitle("TrueGrid");
  
  TGraph* GridEdgeH = new TGraph();
  GridEdgeH->SetName("GridEdgeH");
  GridEdgeH->SetTitle("GridEdgeH");

  TGraph* GridEdgeV = new TGraph();
  GridEdgeV->SetName("GridEdgeV");
  GridEdgeV->SetTitle("GridEdgeV");

  TGraph* TrueEdgeH = new TGraph();
  TrueEdgeH->SetName("TrueEdgeH");
  TrueEdgeH->SetTitle("TrueEdgeH");

  TGraph* TrueEdgeV = new TGraph();
  TrueEdgeV->SetName("TrueEdgeV");
  TrueEdgeV->SetTitle("TrueEdgeV");

  TGraph* CorrectedGrid1 = new TGraph();
  CorrectedGrid1->SetName("CorrectedGrid1");
  CorrectedGrid1->SetTitle("CorrectedGrid1");

  TGraph* FailedPlot = new TGraph();
  FailedPlot->SetName("FailedPlot");
  FailedPlot->SetTitle("FailedPlot");

  TGraph* GRadius = new TGraph();
  GRadius->SetName("Radius");
  GRadius->SetTitle("Radius");

  TGraph* GDeviation = new TGraph();
  GDeviation->SetName("Deviation");
  GDeviation->SetTitle("Deviation");
  TH1* hDeviation = new TH1D("hDeviation","Deviation",120,0,1200);
  TH1* hDeviationX = new TH1D("hDeviationX","DeviationX",200,-100,100);
  TH1* hDeviationY = new TH1D("hDeviationY","DeviationY",200,-100,100);

  TGraph* GSquareEven = new TGraph();
  GSquareEven->SetName("GSquareEven");
  GSquareEven->SetTitle("GSquareEven");

  TGraph* GSquareOdd = new TGraph();
  GSquareOdd->SetName("GSquareOdd");
  GSquareOdd->SetTitle("GSquareOdd");

  TGraph* GOneSquare = new TGraph();
  GOneSquare->SetName("GOneSquare");
  GOneSquare->SetTitle("GOneSquare");

  TGraph2D* XGraph2D = new TGraph2D();
  XGraph2D->SetName("XGraph2D");
  XGraph2D->SetTitle("XGraph2D");

  TGraph2D* YGraph2D = new TGraph2D();
  YGraph2D->SetName("YGraph2D");
  YGraph2D->SetTitle("YGraph2D");

  int sizeX,sizeY;
  double startX,startY;

  TEllipse *EllipseFit;

  GridPoint** GridArray;
  GridEdge** HEdges;
  GridEdge** VEdges;
  GridSquare** SquareArray;
  //Fit Parameters
  double XFitPar[6];
  double YFitPar[6];
//  double XFitPar[10];
//  double YFitPar[10];
  //Read In Data
  ostringstream convert;
  convert << setfill('0') << setw(4)<<FileID;
  string fName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + FilePrefix + string("_") + convert.str();
  fName += ".root";
  
  if(CurrentAnalyzer->OpenExpInputFile(fName.c_str(),tfilein)==-1) return -1;
  CurrentAnalyzer->SetExpInTreeAddress(tfilein,Tree_MCP,ExpData);
  cout << "Reading file "<<fName<<endl;

  int N_MCP = Tree_MCP->GetEntries();
  for(int i=0;i<N_MCP;i++){
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    //Read Tree
    Tree_MCP->GetEntry(i);
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);

    //Pre-shift
//    RecData.MCPPos_X+=PreShiftX;
//    RecData.MCPPos_Y+=PreShiftY;

    //Fill Histograms
    bool cond;
    cond = RecData.MCP_TXSum>CurrentAnalyzer->TXSumLow && RecData.MCP_TXSum<CurrentAnalyzer->TXSumHigh && RecData.MCP_TYSum>CurrentAnalyzer->TYSumLow && RecData.MCP_TYSum<CurrentAnalyzer->TYSumHigh;
    if(cond)h_MCPImage->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
    h_T1T2X->Fill(RecData.MCP_TXSum);
    h_T1T2Y->Fill(RecData.MCP_TYSum);
  }

  //Use edge of the delaylines as reference
  if (Reference.compare("Edge")==0){
    h_MCPImageProjectX = h_MCPImage->ProjectionX();
    h_MCPImageProjectY = h_MCPImage->ProjectionY();
    double LeftEdge=-50.0;
    double BottomEdge=-50.0;
    double RightEdge=50.0;
    double TopEdge=50.0;
    for (int i=1;i<=2000;i++){
      LeftEdge = h_MCPImageProjectX->GetBinCenter(i);
      if (h_MCPImageProjectX->GetBinContent(i)>CountThreshold)break;
    }
    for (int i=1;i<=2000;i++){
      BottomEdge = h_MCPImageProjectY->GetBinCenter(i);
      if (h_MCPImageProjectY->GetBinContent(i)>CountThreshold)break;
    }
    for (int i=2000;i>=1;i--){
      RightEdge = h_MCPImageProjectX->GetBinCenter(i);
      if (h_MCPImageProjectX->GetBinContent(i)>CountThreshold)break;
    }
    for (int i=2000;i>=1;i--){
      TopEdge = h_MCPImageProjectY->GetBinCenter(i);
      if (h_MCPImageProjectY->GetBinContent(i)>CountThreshold)break;
    }
//    cout << LeftEdge<<" "<<RightEdge<<" "<<BottomEdge<<" "<<TopEdge<<endl;
    ShiftX = PreShiftX - (LeftEdge+RightEdge)/2.0;
    ShiftY = PreShiftY - (BottomEdge+TopEdge)/2.0;
    ScaleX = DelayLineWidth/(RightEdge-LeftEdge);
    ScaleY = DelayLineWidth/(TopEdge-BottomEdge);
    //Update Calibration Parameters
    CurrentAnalyzer->ExpParameterList[0][0].MCPXShift=0.0;
    CurrentAnalyzer->ExpParameterList[0][0].MCPYShift=0.0;
    CurrentAnalyzer->MCPShiftX = ShiftX;
    CurrentAnalyzer->MCPShiftY = ShiftY;
    CurrentAnalyzer->MCPScaleX = ScaleX;
    CurrentAnalyzer->MCPScaleY = ScaleY;
  }else if (Reference.compare("Circle")==0){
    for(int i=0;i<N_MCP;i++){
      CurrentAnalyzer->ClearTempData(ExpData,RecData);
      //Read Tree
      Tree_MCP->GetEntry(i);
      CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);

      //Fill Histograms
      bool cond;
      cond = RecData.MCP_TXSum>CurrentAnalyzer->TXSumLow && RecData.MCP_TXSum<CurrentAnalyzer->TXSumHigh && RecData.MCP_TYSum>CurrentAnalyzer->TYSumLow && RecData.MCP_TYSum<CurrentAnalyzer->TYSumHigh;

      if(cond){
	for (int j=0;j<10;j++){
	  double Angle = (-45.0 + j*9.0)/180.0*PI;
	  if (abs(RecData.MCPPos_X*sin(Angle)+RecData.MCPPos_Y*cos(Angle))<1.0){
	    h_RimSearch[j]->Fill(RecData.MCPPos_X*cos(Angle)-RecData.MCPPos_Y*sin(Angle));
	  }
	}
	for (int j=10;j<20;j++){
	  double Angle = (-45.0 + (j-10+1)*9.0)/180.0*PI;
	  if (abs(RecData.MCPPos_X*cos(Angle)-RecData.MCPPos_Y*sin(Angle))<1.0){
	    h_RimSearch[j]->Fill(RecData.MCPPos_X*sin(Angle)+RecData.MCPPos_Y*cos(Angle));
	  }
	}
      }
    }
    FindRim(Rim,h_RimSearch);
    TVectorD conic = fit_ellipse(Rim);
    TVectorD ellipse = ConicToParametric(conic);

    std::cout << std::endl;
    std::cout << "x0 = " << ellipse[0] << std::endl;
    std::cout << "y0 = " << ellipse[1] << std::endl;
    std::cout << "a = " << ellipse[2] << std::endl;
    std::cout << "b = " << ellipse[3] << std::endl;
    std::cout << "theta = " << ellipse[4] << std::endl;
    std::cout << std::endl;
    EllipseFit = new TEllipse(ellipse[0], ellipse[1], // "x0", "y0"
	ellipse[2], ellipse[3], // "a", "b"
	0, 360,
	ellipse[4]); // "theta" (in degrees)
    EllipseFit->SetFillStyle(0); // hollow
    // void EllipseTGraphDLSF(TGraph *g);
    ShiftX = PreShiftX - ellipse[0];
    ShiftY = PreShiftY - ellipse[1];
    double theta = ellipse[4];
    if (theta>-5 && theta<5){
      ScaleX = MaskRadius/ellipse[2];
      ScaleY = MaskRadius/ellipse[3];
    }else if (theta>85 && theta<95){
      ScaleX = MaskRadius/ellipse[3];
      ScaleY = MaskRadius/ellipse[2];
    }else{
      cout << "WARNING: Fit not quite right."<<endl;
    }
    //Update Calibration Parameters
    CurrentAnalyzer->ExpParameterList[0][0].MCPXShift=0.0;
    CurrentAnalyzer->ExpParameterList[0][0].MCPYShift=0.0;
    CurrentAnalyzer->MCPShiftX = ShiftX;
    CurrentAnalyzer->MCPShiftY = ShiftY;
    CurrentAnalyzer->MCPScaleX = ScaleX;
    CurrentAnalyzer->MCPScaleY = ScaleY;
  }else if (Reference.compare("Grid")==0){
    //Set the pre-calibration parameters
    CurrentAnalyzer->MCPShiftX = PreShiftX;
    CurrentAnalyzer->MCPShiftY = PreShiftY;
    CurrentAnalyzer->MCPScaleX = PreScaleX;
    CurrentAnalyzer->MCPScaleY = PreScaleY;
    //Do the circle calibrate first
/*    for(int i=0;i<N_MCP;i++){
      CurrentAnalyzer->ClearTempData(ExpData,RecData);
      //Read Tree
      Tree_MCP->GetEntry(i);
      CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);

      //Fill Histograms
      bool cond;
      cond = RecData.MCP_TXSum>CurrentAnalyzer->TXSumLow && RecData.MCP_TXSum<CurrentAnalyzer->TXSumHigh && RecData.MCP_TYSum>CurrentAnalyzer->TYSumLow && RecData.MCP_TYSum<CurrentAnalyzer->TYSumHigh && pow(RecData.MCPPos_X,2.0)+pow(RecData.MCPPos_Y,2.0)<pow(PreCutR,2.0);

      if(cond){
	for (int j=0;j<10;j++){
	  double Angle = (-45.0 + j*9.0)/180.0*PI;
	  if (abs(RecData.MCPPos_X*sin(Angle)+RecData.MCPPos_Y*cos(Angle))<1.0){
	    h_RimSearch[j]->Fill(RecData.MCPPos_X*cos(Angle)-RecData.MCPPos_Y*sin(Angle));
	  }
	}
	for (int j=10;j<20;j++){
	  double Angle = (-45.0 + (j-10+1)*9.0)/180.0*PI;
	  if (abs(RecData.MCPPos_X*cos(Angle)-RecData.MCPPos_Y*sin(Angle))<1.0){
	    h_RimSearch[j]->Fill(RecData.MCPPos_X*sin(Angle)+RecData.MCPPos_Y*cos(Angle));
	  }
	}
      }
    }
    //Find the Rim First and scale
    FindRim(Rim,h_RimSearch);
    TVectorD conic = fit_ellipse(Rim);
    TVectorD ellipse = ConicToParametric(conic);

    std::cout << std::endl;
    std::cout << "x0 = " << ellipse[0] << std::endl;
    std::cout << "y0 = " << ellipse[1] << std::endl;
    std::cout << "a = " << ellipse[2] << std::endl;
    std::cout << "b = " << ellipse[3] << std::endl;
    std::cout << "theta = " << ellipse[4] << std::endl;
    std::cout << std::endl;
    EllipseFit = new TEllipse(ellipse[0], ellipse[1], // "x0", "y0"
	ellipse[2], ellipse[3], // "a", "b"
	0, 360,
	ellipse[4]); // "theta" (in degrees)
    EllipseFit->SetFillStyle(0); // hollow
    // void EllipseTGraphDLSF(TGraph *g);
    ShiftX = PreShiftX - ellipse[0];
    ShiftY = PreShiftY - ellipse[1];
    double theta = ellipse[4];
    if (theta>-5 && theta<5){
      ScaleX = MaskRadius/ellipse[2];
      ScaleY = MaskRadius/ellipse[3];
    }else if (theta>85 && theta<95){
      ScaleX = MaskRadius/ellipse[3];
      ScaleY = MaskRadius/ellipse[2];
    }else{
      cout << "WARNING: Fit not quite right."<<endl;
    }
    */
    //Constructed the circle-calibrated Image
/*    CurrentAnalyzer->MCPShiftX = ShiftX;
    CurrentAnalyzer->MCPShiftY = ShiftY;
    CurrentAnalyzer->MCPScaleX = ScaleX;
    CurrentAnalyzer->MCPScaleY = ScaleY;
    */
    for(int i=0;i<N_MCP;i++){
      CurrentAnalyzer->ClearTempData(ExpData,RecData);
      //Read Tree
      Tree_MCP->GetEntry(i);
      CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);
      //Fill Histograms
      bool cond;
      cond = RecData.MCP_TXSum>CurrentAnalyzer->TXSumLow && RecData.MCP_TXSum<CurrentAnalyzer->TXSumHigh && RecData.MCP_TYSum>CurrentAnalyzer->TYSumLow && RecData.MCP_TYSum<CurrentAnalyzer->TYSumHigh && pow(RecData.MCPPos_X,2.0)+pow(RecData.MCPPos_Y,2.0)<pow(PreCutR,2.0);
      if (cond)h_MCPImage_PreCal->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
    }
    /***************************************************************/
    //Initialize Grid
    vector <double> XPoints;
    vector <double> YPoints;
    InitGrid(XPoints,YPoints,h_MCPImage_PreCal);
    sizeX = XPoints.size();
    sizeY = YPoints.size();
    startX = -2.0*sizeX+2.0;
    startY = -2.0*sizeY+2.0;
    //Terminate if the size of the grid is not right
/*    if (sizeX!=18 || sizeY!=18){
      cout <<"Grid initialization failed. Check the code and setting!"<<endl;
      cout <<"size X : Y  "<<sizeX<<" "<<sizeY<<endl;
      return -1;
    }*/
    //Create the GridArray
    GridArray = new GridPoint*[sizeX];
    for (int i=0;i<sizeX;i++){
      GridArray[i] = new GridPoint[sizeY];
    }
    for (int i=0;i<sizeX;i++){
      for (int j=0;j<sizeY;j++){
	GridArray[i][j].x = XPoints[i];
	GridArray[i][j].y = YPoints[j];
	if ((pow(GridArray[i][j].x,2.0)+pow(GridArray[i][j].y,2.0))<pow(MaskRadius+1.0,2.0)){
	  GridArray[i][j].status=1;
	}else{
	  GridArray[i][j].status=0;
	}
      }
    }      
    //Fine tune the Grid
    FindGrid(GridArray,sizeX,sizeY,h_MCPImage_PreCal,1.2);
    AdjustGrid(GridArray,sizeX,sizeY);
    FindGrid(GridArray,sizeX,sizeY,h_MCPImage_PreCal,0.4);
    AdjustFailed(GridArray,sizeX,sizeY,0.1);
    FindGrid(GridArray,sizeX,sizeY,h_MCPImage_PreCal,0.8,true);
/*    AdjustGrid(GridArray,sizeX,sizeY);
    FindGrid(GridArray,sizeX,sizeY,h_MCPImage_PreCal,0.4);
    AdjustGrid(GridArray,sizeX,sizeY);
    FindGrid(GridArray,sizeX,sizeY,h_MCPImage_PreCal,0.4);
  */  
/*    AdjustFailed(GridArray,sizeX,sizeY,0.1);
    FindGrid(GridArray,sizeX,sizeY,h_MCPImage_PreCal,0.4,true);
*/    
    //Create failed point graph
    int NFailed = 0;
    for (int i=0;i<sizeX;i++){
      for (int j=0;j<sizeY;j++){
	if (GridArray[i][j].status==2){
	  FailedPlot->SetPoint(NFailed,GridArray[i][j].x,GridArray[i][j].y);
	  NFailed++;
	}
      }
    }      
    //Fill the original grid before moving around
    int NGrid = 0;
    for (int i=0;i<sizeX;i++){
      for (int j=0;j<sizeY;j++){
	if (GridArray[i][j].status==1){
	  OriginalGrid->SetPoint(NGrid,GridArray[i][j].x,GridArray[i][j].y);
	  NGrid++;
	}
      }
    }      
    //Correct the over all shift
    //Center the middle box
    int MidBinX = sizeX/2-1;
    int MidBinY = sizeY/2-1;
    double MidBoxCenterX = (GridArray[MidBinX][MidBinY].x+GridArray[MidBinX][MidBinY+1].x+GridArray[MidBinX+1][MidBinY].x+GridArray[MidBinX+1][MidBinY+1].x)/4.0;
    double MidBoxCenterY = (GridArray[MidBinX][MidBinY].y+GridArray[MidBinX][MidBinY+1].y+GridArray[MidBinX+1][MidBinY].y+GridArray[MidBinX+1][MidBinY+1].y)/4.0;
    for (int i=0;i<sizeX;i++){
      for (int j=0;j<sizeY;j++){
	if (GridArray[i][j].status==1){
	  GridArray[i][j].x-=MidBoxCenterX;
	  GridArray[i][j].y-=MidBoxCenterY;
	}
      }
    }
    //Update Calibration Parameters
    //Be careful, in the reconstruction algorithm, shifting comes earlier than scaling
    CurrentAnalyzer->MCPShiftX-=MidBoxCenterX/(CurrentAnalyzer->MCPScaleX);
    CurrentAnalyzer->MCPShiftY-=MidBoxCenterY/(CurrentAnalyzer->MCPScaleY);
    //update temp variables
    ShiftX = CurrentAnalyzer->MCPShiftX;
    ShiftY = CurrentAnalyzer->MCPShiftY;
    ScaleX = CurrentAnalyzer->MCPScaleX;
    ScaleY = CurrentAnalyzer->MCPScaleY;

    //Load again, finer bin width for finding grid lines and resolution
    for(int i=0;i<N_MCP;i++){
      CurrentAnalyzer->ClearTempData(ExpData,RecData);
      //Read Tree
      Tree_MCP->GetEntry(i);
      CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);
      //Fill Histograms
      bool cond;
      cond = RecData.MCP_TXSum>CurrentAnalyzer->TXSumLow && RecData.MCP_TXSum<CurrentAnalyzer->TXSumHigh && RecData.MCP_TYSum>CurrentAnalyzer->TYSumLow && RecData.MCP_TYSum<CurrentAnalyzer->TYSumHigh;
      bool cond2 = pow(RecData.MCPPos_X,2.0)+pow(RecData.MCPPos_Y,2.0)<pow(PreCutR,2.0);
      if (cond){
	h_MCPImage_2ndCal->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
	if (cond2) h_MCPImage_CalLowRes->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
	h_MCPImage_CalZoom->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
	h_MCPImage_CalZoom2->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
      }
    }

    //Further Fine tune the grid first
    FineTuneGrid(GridArray,sizeX,sizeY,h_MCPImage_2ndCal,0.8);
    NGrid = 0;
    for (int i=0;i<sizeX;i++){
      for (int j=0;j<sizeY;j++){
	if (GridArray[i][j].status==1){
	  Grid->SetPoint(NGrid,GridArray[i][j].x,GridArray[i][j].y);
	  TrueGrid->SetPoint(NGrid,startX+4.0*i,startY+4.0*j);
	  XGraph2D->SetPoint(NGrid,GridArray[i][j].x,GridArray[i][j].y,startX+4.0*i);
	  YGraph2D->SetPoint(NGrid,GridArray[i][j].x,GridArray[i][j].y,startY+4.0*j);
	  //Deviations
	  double X = GridArray[i][j].x;
	  double Y = GridArray[i][j].y;
	  double XTrue = startX+4.0*i;
	  double YTrue = startY+4.0*j;
	  GDeviation->SetPoint(NGrid,sqrt(X*X+Y*Y),sqrt(pow(X-XTrue,2.0)+pow(Y-YTrue,2.0)));
	  hDeviation->Fill(sqrt(pow(X-XTrue,2.0)+pow(Y-YTrue,2.0))*1000);
	  NGrid++;
	}
      }
    }      

    //Construct Grid Edges
    HEdges = new GridEdge*[sizeY];
    for (int i=0;i<sizeY;i++){
      HEdges[i] = new GridEdge[sizeX-1];
    }
    VEdges = new GridEdge*[sizeX];
    for (int i=0;i<sizeX;i++){
      VEdges[i] = new GridEdge[sizeY-1];
    }

    FindEdges(HEdges,VEdges,GridArray,sizeX,sizeY,h_MCPImage_2ndCal,NPointEdge);
    NGrid = 0;
    for (int i=0;i<sizeY;i++){
      for (int j=0;j<sizeX-1;j++){
	if (HEdges[i][j].status!=1)continue;
	for (int Index=1;Index<=NPointEdge-2;Index++){
	  GridEdgeH->SetPoint(NGrid,HEdges[i][j].PointList[Index].x,HEdges[i][j].PointList[Index].y);
	  TrueEdgeH->SetPoint(NGrid,startX+4.0*j+1.0*Index,startY+4.0*i);
	  //Deviations
	  double X = HEdges[i][j].PointList[Index].x; 
	  double Y = HEdges[i][j].PointList[Index].y;
	  double XTrue = startX+4.0*j+1.0*Index;
	  double YTrue = startY+4.0*i;
	  hDeviation->Fill(sqrt(pow(X-XTrue,2.0)+pow(Y-YTrue,2.0))*1000);
	  NGrid++;
	}
      }
    }
    NGrid = 0;
    for (int i=0;i<sizeX;i++){
      for (int j=0;j<sizeY-1;j++){
	if (VEdges[i][j].status!=1)continue;
	for (int Index=1;Index<=NPointEdge-2;Index++){
	  GridEdgeV->SetPoint(NGrid,VEdges[i][j].PointList[Index].x,VEdges[i][j].PointList[Index].y);
	  TrueEdgeV->SetPoint(NGrid,startX+4.0*i,startY+4.0*j+1.0*Index);
	  //Deviations
	  double X = VEdges[i][j].PointList[Index].x; 
	  double Y = VEdges[i][j].PointList[Index].y;
	  double XTrue = startX+4.0*i;
	  double YTrue = startY+4.0*j+1.0*Index;
	  hDeviation->Fill(sqrt(pow(X-XTrue,2.0)+pow(Y-YTrue,2.0))*1000);
	  NGrid++;
	}
      }
    }
    //Construct squares
    SquareArray = new GridSquare*[sizeX-1];
    for (int i=0;i<sizeX-1;i++){
      SquareArray[i] = new GridSquare[sizeY-1];
    }
    for (int i=0;i<sizeX-1;i++){
      for (int j=0;j<sizeY-1;j++){
	if ((GridArray[i][j].status!=1) || (GridArray[i+1][j].status!=1) || (GridArray[i][j+1].status!=1 || (GridArray[i+1][j+1].status!=1))){
	  SquareArray[i][j].status=0;
	  continue;
	}
	SquareArray[i][j].status=1;
	//Set Corner values
	int n=0;
	for (int ii=0;ii<2;ii++){
	  for (int jj=0;jj<2;jj++){
	    SquareArray[i][j].Corners[n].x = GridArray[i+ii][j+jj].x;
	    SquareArray[i][j].Corners[n].y = GridArray[i+ii][j+jj].y;
	    SquareArray[i][j].TrueCorners[n].x = startX+4.0*(i+ii);
	    SquareArray[i][j].TrueCorners[n].y = startY+4.0*(j+jj);
	    n++;
	  }
	}
	//Set Edge Values
	//Corners are included in the edges,but may not be polted into the graph later
	for (n=0;n<=NPointEdge-1;n++){
	  SquareArray[i][j].Edges[0].PointList[n].x = HEdges[j][i].PointList[n].x; 
	  SquareArray[i][j].Edges[0].PointList[n].y = HEdges[j][i].PointList[n].y; 
	  SquareArray[i][j].Edges[1].PointList[n].x = HEdges[j+1][i].PointList[n].x; 
	  SquareArray[i][j].Edges[1].PointList[n].y = HEdges[j+1][i].PointList[n].y; 
	  SquareArray[i][j].Edges[2].PointList[n].x = VEdges[i][j].PointList[n].x; 
	  SquareArray[i][j].Edges[2].PointList[n].y = VEdges[i][j].PointList[n].y; 
	  SquareArray[i][j].Edges[3].PointList[n].x = VEdges[i+1][j].PointList[n].x; 
	  SquareArray[i][j].Edges[3].PointList[n].y = VEdges[i+1][j].PointList[n].y; 
	  //True edges
	  SquareArray[i][j].TrueEdges[0].PointList[n].x = startX+4.0*i+1.0*n;
	  SquareArray[i][j].TrueEdges[0].PointList[n].y = startY+4.0*j;
	  SquareArray[i][j].TrueEdges[1].PointList[n].x = startX+4.0*i+1.0*n;
	  SquareArray[i][j].TrueEdges[1].PointList[n].y = startY+4.0*(j+1);
	  SquareArray[i][j].TrueEdges[2].PointList[n].x = startX+4.0*i;
	  SquareArray[i][j].TrueEdges[2].PointList[n].y = startY+4.0*j+1.0*n;
	  SquareArray[i][j].TrueEdges[3].PointList[n].x = startX+4.0*(i+1);
	  SquareArray[i][j].TrueEdges[3].PointList[n].y = startY+4.0*j+1.0*n;
	}
      }
    }
    //Determin Square Limits, for square finding
    for (int i=0;i<sizeX-1;i++){
      for (int j=0;j<sizeY-1;j++){
	if (SquareArray[i][j].status!=1)continue;
	SquareArray[i][j].X_in_left = SquareArray[i][j].Edges[2].PointList[0].x;
	SquareArray[i][j].X_in_right = SquareArray[i][j].Edges[3].PointList[0].x;
	SquareArray[i][j].Y_in_bottom = SquareArray[i][j].Edges[0].PointList[0].y;
	SquareArray[i][j].Y_in_top = SquareArray[i][j].Edges[1].PointList[0].y;
	SquareArray[i][j].X_out_left = SquareArray[i][j].Edges[2].PointList[0].x;
	SquareArray[i][j].X_out_right = SquareArray[i][j].Edges[3].PointList[0].x;
	SquareArray[i][j].Y_out_bottom = SquareArray[i][j].Edges[0].PointList[0].y;
	SquareArray[i][j].Y_out_top = SquareArray[i][j].Edges[1].PointList[0].y;
	for (int n=1;n<=NPointEdge-1;n++){
	  if (SquareArray[i][j].Edges[2].PointList[n].x > SquareArray[i][j].X_in_left)SquareArray[i][j].X_in_left = SquareArray[i][j].Edges[2].PointList[n].x;
	  if (SquareArray[i][j].Edges[3].PointList[n].x < SquareArray[i][j].X_in_right)SquareArray[i][j].X_in_right = SquareArray[i][j].Edges[3].PointList[n].x;
	  if (SquareArray[i][j].Edges[0].PointList[n].y > SquareArray[i][j].Y_in_bottom)SquareArray[i][j].Y_in_bottom = SquareArray[i][j].Edges[0].PointList[n].y;
	  if (SquareArray[i][j].Edges[1].PointList[n].y < SquareArray[i][j].Y_in_top)SquareArray[i][j].Y_in_top = SquareArray[i][j].Edges[1].PointList[n].y;

	  if (SquareArray[i][j].Edges[2].PointList[n].x < SquareArray[i][j].X_out_left)SquareArray[i][j].X_out_left = SquareArray[i][j].Edges[2].PointList[n].x;
	  if (SquareArray[i][j].Edges[3].PointList[n].x > SquareArray[i][j].X_out_right)SquareArray[i][j].X_out_right = SquareArray[i][j].Edges[3].PointList[n].x;
	  if (SquareArray[i][j].Edges[0].PointList[n].y < SquareArray[i][j].Y_out_bottom)SquareArray[i][j].Y_out_bottom = SquareArray[i][j].Edges[0].PointList[n].y;
	  if (SquareArray[i][j].Edges[1].PointList[n].y > SquareArray[i][j].Y_out_top)SquareArray[i][j].Y_out_top = SquareArray[i][j].Edges[1].PointList[n].y;
	}
      }
    }

    //Local Fit
    if (GridMode.compare("Local")==0){
      //Calibrate square by square
      for (int i=0;i<sizeX-1;i++){
	for (int j=0;j<sizeY-1;j++){
	  if (SquareArray[i][j].status!=1)continue;

	  TGraph2D* XGraph2DLocal = new TGraph2D();
	  XGraph2DLocal->SetName("XGraph2DLocal");
	  XGraph2DLocal->SetTitle("XGraph2DLocal");

	  TGraph2D* YGraph2DLocal = new TGraph2D();
	  YGraph2DLocal->SetName("YGraph2DLocal");
	  YGraph2DLocal->SetTitle("YGraph2DLocal");

	  int NCal=0;
	  double XCenter = startX+4.0*i+2.0;
	  double YCenter = startY+4.0*j+2.0;

	  //Load data in graphs
	  for (int n=0;n<4;n++){
	    XGraph2DLocal->SetPoint(NCal,SquareArray[i][j].Corners[n].x-XCenter,SquareArray[i][j].Corners[n].y-YCenter,SquareArray[i][j].TrueCorners[n].x-XCenter);
	    YGraph2DLocal->SetPoint(NCal,SquareArray[i][j].Corners[n].x-XCenter,SquareArray[i][j].Corners[n].y-YCenter,SquareArray[i][j].TrueCorners[n].y-YCenter);
	    NCal++;
	  }
	  for (int m=0;m<4;m++){
	    for (int n=1;n<=NPointEdge-2;n++){
	      XGraph2DLocal->SetPoint(NCal,SquareArray[i][j].Edges[m].PointList[n].x-XCenter,SquareArray[i][j].Edges[m].PointList[n].y-YCenter,SquareArray[i][j].TrueEdges[m].PointList[n].x-XCenter);
	      YGraph2DLocal->SetPoint(NCal,SquareArray[i][j].Edges[m].PointList[n].x-XCenter,SquareArray[i][j].Edges[m].PointList[n].y-YCenter,SquareArray[i][j].TrueEdges[m].PointList[n].y-YCenter);
	      NCal++;
	    }
	  }
	  
	  //Fit the Graphs
	  //Define 2D quadratic functions
	  TF2* XFitFunc = new TF2("XFitFunc","[0]*x*x+[1]*x*y+[2]*y*y+[3]*x+[4]*y+[5]",-5,5,-5,5);
	  XFitFunc->SetParameters(0,0,0,1,0,0);
	  TF2* YFitFunc = new TF2("YFitFunc","[0]*x*x+[1]*x*y+[2]*y*y+[3]*x+[4]*y+[5]",-5,5,-5,5);
	  YFitFunc->SetParameters(0,0,0,0,1,0);

	  XGraph2DLocal->Fit(XFitFunc);
	  YGraph2DLocal->Fit(YFitFunc);

	  for (int ii=0;ii<6;ii++){
	    SquareArray[i][j].XFitPar[ii] = XFitFunc->GetParameter(ii);
	    SquareArray[i][j].YFitPar[ii] = YFitFunc->GetParameter(ii);
	  }

	  delete XGraph2DLocal;
	  delete YGraph2DLocal;
	  delete XFitFunc;
	  delete YFitFunc;
	}
      }

      //Construct the corrected positions and Construct square graphs
      int NEven=0;
      int NOdd=0;
      for (int i=0;i<sizeX-1;i++){
	for (int j=0;j<sizeY-1;j++){
	  if (SquareArray[i][j].status!=1)continue;
	  double XCenter = startX+4.0*i+2.0;
	  double YCenter = startY+4.0*j+2.0;
	  for (int ii=0;ii<6;ii++){
	    XFitPar[ii]=SquareArray[i][j].XFitPar[ii];
	    YFitPar[ii]=SquareArray[i][j].YFitPar[ii];
	  }
	  //Set Corner values
	  for (int n=0;n<4;n++){
	    double X = SquareArray[i][j].Corners[n].x-XCenter;
	    double Y = SquareArray[i][j].Corners[n].y-YCenter;
	    double XCorrected = XFitPar[0]*X*X+XFitPar[1]*X*Y+XFitPar[2]*Y*Y+XFitPar[3]*X+XFitPar[4]*Y+XFitPar[5];
	    double YCorrected = YFitPar[0]*X*X+YFitPar[1]*X*Y+YFitPar[2]*Y*Y+YFitPar[3]*X+YFitPar[4]*Y+YFitPar[5];
	    SquareArray[i][j].CorrectedCorners[n].x = XCorrected+XCenter;
	    SquareArray[i][j].CorrectedCorners[n].y = YCorrected+YCenter;
	    if ((i+j)%2==0){
	      GSquareEven->SetPoint(NEven,SquareArray[i][j].CorrectedCorners[n].x,SquareArray[i][j].CorrectedCorners[n].y);
	      NEven++;
	    }else{
	      GSquareOdd->SetPoint(NOdd,SquareArray[i][j].CorrectedCorners[n].x,SquareArray[i][j].CorrectedCorners[n].y);
	      NOdd++;
	    }
	  }
	  //Set Edge Values
	  for (int m=0;m<4;m++){
	    for (int n=1;n<=NPointEdge-2;n++){
	      double X = SquareArray[i][j].Edges[m].PointList[n].x-XCenter;
	      double Y = SquareArray[i][j].Edges[m].PointList[n].y-YCenter;
	      double XCorrected = XFitPar[0]*X*X+XFitPar[1]*X*Y+XFitPar[2]*Y*Y+XFitPar[3]*X+XFitPar[4]*Y+XFitPar[5];
	      double YCorrected = YFitPar[0]*X*X+YFitPar[1]*X*Y+YFitPar[2]*Y*Y+YFitPar[3]*X+YFitPar[4]*Y+YFitPar[5];
	      SquareArray[i][j].CorrectedEdges[m].PointList[n].x = XCorrected+XCenter;
	      SquareArray[i][j].CorrectedEdges[m].PointList[n].y = YCorrected+YCenter;
	      if ((i+j)%2==0){
		GSquareEven->SetPoint(NEven,SquareArray[i][j].CorrectedEdges[m].PointList[n].x,SquareArray[i][j].CorrectedEdges[m].PointList[n].y);
		//Calcluate deviations of the corrected calibration points
		//Only for even squares, no double counting
		if(n==0 || n==1){
		  hDeviationX->Fill(XCorrected+2.0);
		}else if(n==2 || n==3){
		  hDeviationX->Fill(XCorrected-2.0);
		}
		if(n==0 || n==2){
		  hDeviationY->Fill(YCorrected+2.0);
		}else if(n==1 || n==3){
		  hDeviationY->Fill(YCorrected-2.0);
		}
		NEven++;
	      }else{
		GSquareOdd->SetPoint(NOdd,SquareArray[i][j].CorrectedEdges[m].PointList[n].x,SquareArray[i][j].CorrectedEdges[m].PointList[n].y);
		NOdd++;
	      }
	    }
	  }
	}
      }
      //graph for one square
      GOneSquare->SetPoint(0,SquareArray[8][9].Corners[0].x,SquareArray[8][9].Corners[0].y);
      GOneSquare->SetPoint(1,SquareArray[8][9].Edges[0].PointList[1].x,SquareArray[8][9].Edges[0].PointList[1].y);
      GOneSquare->SetPoint(2,SquareArray[8][9].Edges[0].PointList[2].x,SquareArray[8][9].Edges[0].PointList[2].y);
      GOneSquare->SetPoint(3,SquareArray[8][9].Edges[0].PointList[3].x,SquareArray[8][9].Edges[0].PointList[3].y);
      GOneSquare->SetPoint(4,SquareArray[8][9].Corners[2].x,SquareArray[8][9].Corners[2].y);
      GOneSquare->SetPoint(5,SquareArray[8][9].Edges[3].PointList[1].x,SquareArray[8][9].Edges[3].PointList[1].y);
      GOneSquare->SetPoint(6,SquareArray[8][9].Edges[3].PointList[2].x,SquareArray[8][9].Edges[3].PointList[2].y);
      GOneSquare->SetPoint(7,SquareArray[8][9].Edges[3].PointList[3].x,SquareArray[8][9].Edges[3].PointList[3].y);
      GOneSquare->SetPoint(8,SquareArray[8][9].Corners[3].x,SquareArray[8][9].Corners[3].y);
      GOneSquare->SetPoint(9,SquareArray[8][9].Edges[1].PointList[1].x,SquareArray[8][9].Edges[1].PointList[1].y);
      GOneSquare->SetPoint(10,SquareArray[8][9].Edges[1].PointList[2].x,SquareArray[8][9].Edges[1].PointList[2].y);
      GOneSquare->SetPoint(11,SquareArray[8][9].Edges[1].PointList[3].x,SquareArray[8][9].Edges[1].PointList[3].y);
      GOneSquare->SetPoint(12,SquareArray[8][9].Corners[1].x,SquareArray[8][9].Corners[1].y);
      GOneSquare->SetPoint(13,SquareArray[8][9].Edges[2].PointList[1].x,SquareArray[8][9].Edges[2].PointList[1].y);
      GOneSquare->SetPoint(14,SquareArray[8][9].Edges[2].PointList[2].x,SquareArray[8][9].Edges[2].PointList[2].y);
      GOneSquare->SetPoint(15,SquareArray[8][9].Edges[2].PointList[3].x,SquareArray[8][9].Edges[2].PointList[3].y);
      GOneSquare->SetPoint(16,SquareArray[8][9].Corners[0].x,SquareArray[8][9].Corners[0].y);
      //Write to calibration file

      cout <<"Writing to calibration files.\n";

      char filename[100];
      ofstream MCPCalFileOut;
      sprintf(filename,"%s/MCPPosLocalCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
      MCPCalFileOut.open(filename,ios::out);
      MCPCalFileOut<<ShiftX<<" "<<ShiftY<<" "<<ScaleX<<" "<<ScaleY<<endl;
      MCPCalFileOut<<sizeX-1<<" "<<sizeY-1<<" "<<NPointEdge<<endl;
      for (int i=0;i<sizeX-1;i++){
	for (int j=0;j<sizeY-1;j++){
	  if (SquareArray[i][j].status!=1){
	    MCPCalFileOut<<i<<" "<<j<<" "<<0<<endl;
	    continue;
	  }else{
	    MCPCalFileOut<<i<<" "<<j<<" "<<1<<endl;
	    for (int n=0;n<4;n++){
	      MCPCalFileOut<<SquareArray[i][j].Corners[n].x<<" "<<SquareArray[i][j].Corners[n].y<<" ";
	    }
	    MCPCalFileOut<<endl;
	    for (int m=0;m<4;m++){
	      for (int n=0;n<=NPointEdge-1;n++){
		MCPCalFileOut<<SquareArray[i][j].Edges[m].PointList[n].x<<" "<<SquareArray[i][j].Edges[m].PointList[n].y<<" ";
	      }
	      MCPCalFileOut<<endl;
	    }
	    for (int n=0;n<6;n++){
	      MCPCalFileOut<<SquareArray[i][j].XFitPar[n]<<" "<<SquareArray[i][j].YFitPar[n]<<endl;
	    }
	    
	    MCPCalFileOut << SquareArray[i][j].X_in_left << endl;
	    MCPCalFileOut << SquareArray[i][j].X_in_right << endl;
	    MCPCalFileOut << SquareArray[i][j].Y_in_bottom << endl;
	    MCPCalFileOut << SquareArray[i][j].Y_in_top << endl;
	    MCPCalFileOut << SquareArray[i][j].X_out_left << endl;
	    MCPCalFileOut << SquareArray[i][j].X_out_right << endl;
	    MCPCalFileOut << SquareArray[i][j].Y_out_bottom << endl;
	    MCPCalFileOut << SquareArray[i][j].Y_out_top << endl;
	  }
	}
      }      
      MCPCalFileOut.close();
    }
    //Global Fit
    if (GridMode.compare("Global")==0){
      int NRadius = 0;
      for (int i=0;i<sizeX;i++){
	for (int j=0;j<sizeY;j++){
	  if (GridArray[i][j].status==1){
	    double RMeasure = sqrt(pow(GridArray[i][j].x,2.0)+pow(GridArray[i][j].y,2.0));
	    double RReal = sqrt(pow(startX+4.0*i,2.0)+pow(startY+4.0*j,2.0));
	    GRadius->SetPoint(NRadius,RMeasure,RReal);
	    NRadius++;
	  }
	}
      }
      //Define 2D quadratic functions
      TF2* XFitFunc = new TF2("XFitFunc","[0]*x*x+[1]*x*y+[2]*y*y+[3]*x+[4]*y+[5]",-40,40,-40,40);
      XFitFunc->SetParameters(0,0,0,1,0,0);
      TF2* YFitFunc = new TF2("YFitFunc","[0]*x*x+[1]*x*y+[2]*y*y+[3]*x+[4]*y+[5]",-40,40,-40,40);
      YFitFunc->SetParameters(0,0,0,0,1,0);

      //Third order polynomial
      /*    TF2* XFitFunc3 = new TF2("XFitFunc","[0]*x*x*x+[1]*x*x*y+[2]*x*y*y+[3]*y*y*y+[4]*x*x+[5]*x*y+[6]*y*y+[7]*x+[8]*y+[9]",-40,40,-40,40);
	    XFitFunc3->SetParameters(0,0,0,0,0,0,0,1,0,0);
	    TF2* YFitFunc3 = new TF2("YFitFunc","[0]*x*x*x+[1]*x*x*y+[2]*x*y*y+[3]*y*y*y+[4]*x*x+[5]*x*y+[6]*y*y+[7]*x+[8]*y+[9]",-40,40,-40,40);
	    YFitFunc3->SetParameters(0,0,0,0,0,0,0,0,1,0);
       */
      XGraph2D->Fit(XFitFunc);
      YGraph2D->Fit(YFitFunc);
      //    XGraph2D->Fit(XFitFunc3);
      //    YGraph2D->Fit(YFitFunc3);

      for (int i=0;i<6;i++){
	XFitPar[i] = XFitFunc->GetParameter(i);
	YFitPar[i] = YFitFunc->GetParameter(i);
      }
      /*    for (int i=0;i<10;i++){
	    XFitPar[i] = XFitFunc3->GetParameter(i);
	    YFitPar[i] = YFitFunc3->GetParameter(i);
	    }*/

      //Construct corrected Grid
      NGrid = 0;
      for (int i=0;i<sizeX;i++){
	for (int j=0;j<sizeY;j++){
	  if (GridArray[i][j].status==1){
	    double X = GridArray[i][j].x;
	    double Y = GridArray[i][j].y;
	    double XCorrected = XFitPar[0]*X*X+XFitPar[1]*X*Y+XFitPar[2]*Y*Y+XFitPar[3]*X+XFitPar[4]*Y+XFitPar[5];
	    double YCorrected = YFitPar[0]*X*X+YFitPar[1]*X*Y+YFitPar[2]*Y*Y+YFitPar[3]*X+YFitPar[4]*Y+YFitPar[5];
	    //	  double XCorrected = XFitPar[0]*X*X*X+XFitPar[1]*X*X*Y+XFitPar[2]*X*Y*Y+XFitPar[3]*Y*Y*Y+XFitPar[4]*X*X+XFitPar[5]*X*Y+XFitPar[6]*Y*Y+XFitPar[7]*X+XFitPar[8]*Y+XFitPar[9];
	    //	  double YCorrected = YFitPar[0]*X*X*X+YFitPar[1]*X*X*Y+YFitPar[2]*X*Y*Y+YFitPar[3]*Y*Y*Y+YFitPar[4]*X*X+YFitPar[5]*X*Y+YFitPar[6]*Y*Y+YFitPar[7]*X+YFitPar[8]*Y+YFitPar[9];
	    double XTrue = startX+4.0*i;
	    double YTrue = startY+4.0*j;
	    CorrectedGrid1->SetPoint(NGrid,XCorrected,YCorrected);
	    GDeviation->SetPoint(NGrid,sqrt(X*X+Y*Y),sqrt(pow(XCorrected-XTrue,2.0)+pow(YCorrected-YTrue,2.0)));
//	    hDeviation->Fill(sqrt(pow(XCorrected-XTrue,2.0)+pow(YCorrected-YTrue,2.0))*1000);
	    //GRadius->SetPoint(NGrid,XTrue,XCorrected-XTrue);
	    NGrid++;
	  }
	}
      }      
      //Write to calibration file

      cout <<"Writing to calibration files.\n";

      char filename[100];
      ofstream MCPCalFileOut;
      sprintf(filename,"%s/MCPPosGlobalCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
      MCPCalFileOut.open(filename,ios::out);
      MCPCalFileOut<<ShiftX<<" "<<ShiftY<<" "<<ScaleX<<" "<<ScaleY<<endl;
      for (int i=0;i<sizeX;i++){
	for (int j=0;j<sizeY;j++){
	  if (GridArray[i][j].status==1){
	    double X = GridArray[i][j].x;
	    double Y = GridArray[i][j].y;
	    MCPCalFileOut<<i<<" "<<j<<" "<<1<<" "<<X<<" "<<Y<<endl;
	  }else{
	    MCPCalFileOut<<i<<" "<<j<<" "<<0<<" "<<-100<<" "<<-100<<endl;
	  }
	}
      }      
      MCPCalFileOut.close();
    }
  }else{
    cout << "Unkown reference "<<Reference<<"!"<<endl;
    return -1;
  }

  //Constructed the calibrated Image
/*  CurrentAnalyzer->ExpParameterList[0][0].MCPXShift=0.0;
  CurrentAnalyzer->ExpParameterList[0][0].MCPYShift=0.0;
  CurrentAnalyzer->MCPShiftX = ShiftX;
  CurrentAnalyzer->MCPShiftY = ShiftY;
  CurrentAnalyzer->MCPScaleX = ScaleX;
  CurrentAnalyzer->MCPScaleY = ScaleY;*/
  for(int i=0;i<N_MCP;i++){
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    //Read Tree
    Tree_MCP->GetEntry(i);
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);

    //Fill Histograms
    bool cond;
    cond = RecData.MCP_TXSum>CurrentAnalyzer->TXSumLow && RecData.MCP_TXSum<CurrentAnalyzer->TXSumHigh && RecData.MCP_TYSum>CurrentAnalyzer->TYSumLow && RecData.MCP_TYSum<CurrentAnalyzer->TYSumHigh;
    if (Reference.compare("Grid")==0){
      if (GridMode.compare("Local")==0){
	double X = RecData.MCPPos_X;
	double Y = RecData.MCPPos_Y;
	double XCorrected;
	double YCorrected;
	bool cond2 = false;
	int returnval=0;
	int IndexX=0;
	int IndexY=0;
	returnval = FindSquare(SquareArray,sizeX-1,sizeY-1,X,Y,IndexX,IndexY,NPointEdge);
	if (returnval==1){
	  for (int ii=0;ii<6;ii++){
	    XFitPar[ii]=SquareArray[IndexX][IndexY].XFitPar[ii];
	    YFitPar[ii]=SquareArray[IndexX][IndexY].YFitPar[ii];
	  }
	  X -= (startX+4.0*IndexX+2.0);
	  Y -= (startY+4.0*IndexY+2.0);
	  XCorrected = XFitPar[0]*X*X+XFitPar[1]*X*Y+XFitPar[2]*Y*Y+XFitPar[3]*X+XFitPar[4]*Y+XFitPar[5] + startX+4.0*IndexX+2.0;
	  YCorrected = YFitPar[0]*X*X+YFitPar[1]*X*Y+YFitPar[2]*Y*Y+YFitPar[3]*X+YFitPar[4]*Y+YFitPar[5] + startY+4.0*IndexY+2.0;
	  cond2=true;
	}else{
	  cond2=false;
	}
	if (cond && cond2)h_MCPImage_Cal->Fill(XCorrected,YCorrected);
	
      }else if (GridMode.compare("Global")==0){
	double X = RecData.MCPPos_X;
	double Y = RecData.MCPPos_Y;
	double XCorrected = XFitPar[0]*X*X+XFitPar[1]*X*Y+XFitPar[2]*Y*Y+XFitPar[3]*X+XFitPar[4]*Y+XFitPar[5];
	double YCorrected = YFitPar[0]*X*X+YFitPar[1]*X*Y+YFitPar[2]*Y*Y+YFitPar[3]*X+YFitPar[4]*Y+YFitPar[5];
	if (cond)h_MCPImage_Cal->Fill(XCorrected,YCorrected);
	double rotX = XCorrected*cos(RotationAngle)-YCorrected*sin(RotationAngle);
	double rotY = YCorrected*cos(RotationAngle)+XCorrected*sin(RotationAngle);
	bool cond2;
	cond2 = rotX>-5 && rotX<5 && rotY>-5 && rotY<5;
	if(cond && cond2){
	  h_CenterX->Fill(rotX);
	  h_CenterY->Fill(rotY);
	  h_MCPImage_Center->Fill(rotX,rotY);
	}
      }
    }else{
      if (cond)h_MCPImage_Cal->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
      double rotX = RecData.MCPPos_X*cos(RotationAngle)-RecData.MCPPos_Y*sin(RotationAngle);
      double rotY = RecData.MCPPos_Y*cos(RotationAngle)+RecData.MCPPos_X*sin(RotationAngle);
      bool cond2;
      cond2 = rotX>-5 && rotX<5 && rotY>-5 && rotY<5;
      if(cond && cond2){
	h_CenterX->Fill(rotX);
	h_CenterY->Fill(rotY);
	h_MCPImage_Center->Fill(rotX,rotY);
      }
    }
  }
  /*
  double CenterX = h_CenterX->GetBinCenter(h_CenterX->GetMaximumBin());
  double CenterY = h_CenterY->GetBinCenter(h_CenterY->GetMaximumBin());
//  cout <<CenterX<<" "<<CenterY<<endl;
  TF1* FitFunc = new TF1("FitFunc","gaus(0)+[3]",-2,2);
  FitFunc->SetParameters(h_CenterX->GetBinContent(CenterX)-h_CenterX->GetBinContent(CenterX-3),CenterX,1,h_CenterX->GetBinContent(CenterX-3));
  h_CenterX->Fit(FitFunc,"","",CenterX-3.0,CenterX+3.0);
  CenterX = FitFunc->GetParameter(1);
  double WidthX = FitFunc->GetParameter(2);
  //Function and struct declarations are moved to Calibration.h and Datastruct.h


  FitFunc->SetParameters(h_CenterY->GetBinContent(CenterY)-h_CenterY->GetBinContent(CenterY-2),CenterY,1,h_CenterY->GetBinContent(CenterY-2));
  h_CenterY->Fit(FitFunc,"","",CenterY-3.0,CenterY+3.0);
  CenterY = FitFunc->GetParameter(1);
  double WidthY = FitFunc->GetParameter(2);
  double WidthZ = WidthX;	//Assuming the B field is symmetric
*/
/*
  ofstream MOTCalFileOut;
  sprintf(filename,"%s/MOTPosCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  MOTCalFileOut.open(filename,ios::out);
  MOTCalFileOut<<CenterX<<" "<<WidthX<<" "<<CenterY<<" "<<WidthY<<" "<<WidthZ;
  MOTCalFileOut.close();
*/
  
    //Close input file
  if (tfilein!=NULL){
    tfilein->Close();
    delete tfilein;
    tfilein =NULL;
  }
  //f->Close();
  //Write to root file
  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_MCPCal.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");

  h_MCPImage->Write();
  h_MCPImage_PreCal->Write();
  h_MCPImage_2ndCal->Write();
  h_MCPImage_Cal->Write();

//  h_MCPImageProjectX->Write();
//  h_MCPImageProjectY->Write();
  h_T1T2X->Write();
  h_T1T2Y->Write();
//  h_CenterX->Write();
//  h_CenterY->Write();
//  h_MCPImage_Center->Write();
  OriginalGrid->Write();
  FailedPlot->Write();
  Grid->Write();
  TrueGrid->Write();
  GridEdgeH->Write();
  GridEdgeV->Write();
  TrueEdgeH->Write();
  TrueEdgeV->Write();

  if (GridMode.compare("Global")==0){
    XGraph2D->Write();
    YGraph2D->Write();
    CorrectedGrid1->Write();
    GRadius->Write();
  }

  if (GridMode.compare("Local")==0){
    GSquareEven->Write();
    GSquareOdd->Write();
  }
  GDeviation->Write();
  hDeviation->Write();
  hDeviationX->Write();
  hDeviationY->Write();

  if (Reference.compare("Circle")==0){
    Rim->Write();
    EllipseFit->Write("EllipseFit");
    for (int i=0;i<20;i++){
      h_RimSearch[i]->Write(); 
    }
  }
  histfile->Close();
  delete histfile;

  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_MCPCalAddition.root");
  histfile = new TFile(spectrumfile.c_str(),"recreate");
  h_MCPImage_CalLowRes->Write();
  h_MCPImage_CalZoom->Write();
  h_MCPImage_CalZoom2->Write();
  Grid->Write();
  TrueGrid->Write();
  GridEdgeH->Write();
  GridEdgeV->Write();
  TrueEdgeH->Write();
  TrueEdgeV->Write();
  GOneSquare->Write();
  hDeviation->Write();
  delete histfile;
  CurrentAnalyzer->Calibrating = false;
  delete h_MCPImage;
  delete h_T1T2X;
  delete h_T1T2Y;
  return 0;
}

/***********************************************************************************/
int Calibration::CalibrateMCPRes(string FilePrefix,int FileID,int CalID)
{
  CurrentAnalyzer->Calibrating = true;
  string spectrumfile;

  ExpDataStruct ExpData;      //Data struct for exp data reading
  RecDataStruct RecData;      //Reconstructed data struct
  TFile* tfilein = NULL; //pointer to input root file
  TTree * The_Tree = NULL; //input tree pointer
  CurrentAnalyzer->ConstructExpParameterList();
  CurrentAnalyzer->CheckExpConditionSettings();

  //Histograms
  TH2* h_MCPImage_2ndCal = new TH2D("h_MCPImage_2ndCal","MCPImage Further Calibrated",10000,-50,50,10000,-50,50);
  TH2* h_MCPImage_Cal = new TH2D("h_MCPImage_Cal","MCPImage Calibrated",2000,-50,50,2000,-50,50);
  TH2* h_MCPImage_CalLowRes = new TH2D("h_MCPImage_CalLowRes","MCPImage Calibrated",400,-40,40,400,-40,40);
  TH2* h_MCPImage_CalZoom = new TH2D("h_MCPImage_CalZoom","MCPImage Further Calibrated Zoom",400,-8,8,400,-8,8);
  TH1* hDeviation = new TH1D("hDeviation","Deviation",100,0,200);
  TH1* hDeviationHR = new TH1D("hDeviationHR","DeviationHR",100,0,200);
  TH1* hDeviationX = new TH1D("hDeviationX","DeviationX",200,-100,100);
  TH1* hDeviationY = new TH1D("hDeviationY","DeviationY",200,-100,100);
  TH1** LocalProjects1;
  TH1** LocalProjects2;
  TH1* OneLocalProject;
  TH1* hLeftRes = new TH1D("hLeftRes","Left Resolution",100,0,200);
  TH1* hRightRes = new TH1D("hRightRes","Right Resolution",100,0,200);
  TH1* hVLineWidth = new TH1D("hVLineWidth","Vertical Line Width",100,0,500);
  TH1* hBottomRes = new TH1D("hBottomRes","Bottom Resolution",100,0,200);
  TH1* hTopRes = new TH1D("hTopRes","Top Resolution",100,0,200);
  TH1* hHLineWidth = new TH1D("hHLineWidth","Horizontal Line Width",100,0,500);
  TH1*** h_XT1T2;
  TH1*** h_YT1T2;
  TH1* h_TOF = new TH1D("h_TOF","TOF",3000,-500,1000);

  //Graphs
  TGraph* Grid = new TGraph();
  Grid->SetName("Grid");
  Grid->SetTitle("Grid");

  TGraph* GridHR = new TGraph();
  GridHR->SetName("GridHR");
  GridHR->SetTitle("GridHR");

  TGraph* GridEdgeH = new TGraph();
  GridEdgeH->SetName("GridEdgeH");
  GridEdgeH->SetTitle("GridEdgeH");

  TGraph* GridEdgeV = new TGraph();
  GridEdgeV->SetName("GridEdgeV");
  GridEdgeV->SetTitle("GridEdgeV");

  TGraph2D* GLeftRes = new TGraph2D();
  GLeftRes->SetName("GLeftRes");
  GLeftRes->SetTitle("GLeftRes");

  TGraph2D* GRightRes = new TGraph2D();
  GRightRes->SetName("GRightRes");
  GRightRes->SetTitle("GrightRes");

  TGraph2D* GVLineWidth = new TGraph2D();
  GVLineWidth->SetName("GVLineWidth");
  GVLineWidth->SetTitle("GVLineWidth");

  TGraph2D* GBottomRes = new TGraph2D();
  GBottomRes->SetName("GBottomRes");
  GBottomRes->SetTitle("GBottomRes");

  TGraph2D* GTopRes = new TGraph2D();
  GTopRes->SetName("GTopRes");
  GTopRes->SetTitle("GrightRes");

  TGraph2D* GHLineWidth = new TGraph2D();
  GHLineWidth->SetName("GHLineWidth");
  GHLineWidth->SetTitle("GHLineWidth");

  TGraph2D* GXT1T2 = new TGraph2D();
  GXT1T2->SetName("GXT1T2");
  GXT1T2->SetTitle("GXT1T2");

  TGraph2D* GYT1T2 = new TGraph2D();
  GYT1T2->SetName("GYT1T2");
  GYT1T2->SetTitle("GYT1T2");

  TGraph2D* GXT1T2Res = new TGraph2D();
  GXT1T2Res->SetName("GXT1T2Res");
  GXT1T2Res->SetTitle("GXT1T2Res");

  TGraph2D* GYT1T2Res = new TGraph2D();
  GYT1T2Res->SetName("GYT1T2Res");
  GYT1T2Res->SetTitle("GYT1T2Res");

  GridPoint** GridArray;
  GridEdge** HEdges;
  GridEdge** VEdges;
  GridSquare** SquareArray;
  //Fit Parameters
  double XFitPar[6];
  double YFitPar[6];
  int sizeX=0;
  int sizeY=0;
  double startX;
  double startY;
  int NPointEdge=0;
  double ShiftX;	
  double ShiftY;
  double ScaleX;	
  double ScaleY;

  //Load Calibrations
  char filename[100];
  ifstream MCPCalFileIn;
  sprintf(filename,"%s/MCPPosLocalCalibration%02d.txt",CurrentAnalyzer->CALIBRATION_DIRECTORY.c_str(),CalID);
  MCPCalFileIn.open(filename,ios::in);
  MCPCalFileIn>>ShiftX>>ShiftY>>ScaleX>>ScaleY;
  MCPCalFileIn>>sizeX>>sizeY>>NPointEdge;
  startX = -2.0*(sizeX+1)+2.0;
  startY = -2.0*(sizeY+1)+2.0;
  SquareArray = new GridSquare*[sizeX];
  for (int i=0;i<sizeX;i++){
    SquareArray[i] = new GridSquare[sizeY];
  }
  int auxint1;
  int auxint2;
  int status;
  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      MCPCalFileIn>>auxint1>>auxint2>>status;
      if (status!=1){
	SquareArray[i][j].status=0;
	continue;
      }else{
	SquareArray[i][j].status=1;
	for (int n=0;n<4;n++){
	  MCPCalFileIn>>SquareArray[i][j].Corners[n].x>>SquareArray[i][j].Corners[n].y;
	}
	for (int m=0;m<4;m++){
	  for (int n=0;n<=NPointEdge-1;n++){
	    MCPCalFileIn>>SquareArray[i][j].Edges[m].PointList[n].x>>SquareArray[i][j].Edges[m].PointList[n].y;
	  }
	}
	for (int n=0;n<6;n++){
	  MCPCalFileIn>>SquareArray[i][j].XFitPar[n]>>SquareArray[i][j].YFitPar[n];
	}

	MCPCalFileIn >> SquareArray[i][j].X_in_left;
	MCPCalFileIn >> SquareArray[i][j].X_in_right;
	MCPCalFileIn >> SquareArray[i][j].Y_in_bottom;
	MCPCalFileIn >> SquareArray[i][j].Y_in_top;
	MCPCalFileIn >> SquareArray[i][j].X_out_left;
	MCPCalFileIn >> SquareArray[i][j].X_out_right;
	MCPCalFileIn >> SquareArray[i][j].Y_out_bottom;
	MCPCalFileIn >> SquareArray[i][j].Y_out_top;
      }
    }
  }      
  MCPCalFileIn.close();
  CurrentAnalyzer->MCPShiftX = ShiftX;
  CurrentAnalyzer->MCPShiftY = ShiftY;
  CurrentAnalyzer->MCPScaleX = ScaleX;
  CurrentAnalyzer->MCPScaleY = ScaleY;
  cout <<"Calibration data loaded."<<endl;

  LocalProjects1 = new TH1*[sizeX*sizeY*4];
  LocalProjects2 = new TH1*[sizeX*sizeY*4];

  for (int i=0;i<sizeX*sizeY*4;i++){
    LocalProjects1[i] = new TH1D(Form("LocalProjectH%d",i),Form("LocalProjectH%d",i),160,-0.8,0.8);
    LocalProjects2[i] = new TH1D(Form("LocalProjectV%d",i),Form("LocalProjectV%d",i),160,-0.8,0.8);
  }
  
  h_XT1T2 = new TH1**[sizeX];
  h_YT1T2 = new TH1**[sizeX];
  for(int i=0;i<sizeX;i++){
    h_XT1T2[i] = new TH1*[sizeY];
    h_YT1T2[i] = new TH1*[sizeY];
    for (int j=0;j<sizeY;j++){
      h_XT1T2[i][j]=new TH1D(Form("XT1+T2_%d_%d",i,j),Form("XT1+T2_%d_%d",i,j),500,50,100);
      h_YT1T2[i][j]=new TH1D(Form("YT1+T2_%d_%d",i,j),Form("YT1+T2_%d_%d",i,j),500,50,100);
    }
  }
  

  //Read In Data
  if(CurrentAnalyzer->ReadMode.compare("MCP")!=0 && CurrentAnalyzer->ReadMode.compare("Double")!=0 && CurrentAnalyzer->ReadMode.compare("Triple")!=0){
      cout <<"Only MCP Triple Double modes are allowed."<<endl;
    return -1;
  }
  ostringstream convert;
  convert << setfill('0') << setw(4)<<FileID;
  string fName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + FilePrefix + string("_") + convert.str();
  fName += ".root";
  if(CurrentAnalyzer->OpenExpInputFile(fName.c_str(),tfilein)==-1) return -1;
  CurrentAnalyzer->SetExpInTreeAddress(tfilein,The_Tree,ExpData);
  cout << "Reading file "<<fName<<endl;
  int N_MCP = The_Tree->GetEntries();

  for(int i=0;i<N_MCP;i++){
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    //Read Tree
    The_Tree->GetEntry(i);
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);

    bool TOFcond;
    TOFcond = RecData.TOF>CurrentAnalyzer->TOFLow && RecData.TOF<CurrentAnalyzer->TOFHigh;
    if (CurrentAnalyzer->ReadMode.compare("Triple")==0 || CurrentAnalyzer->ReadMode.compare("Double")==0){
      if (!TOFcond)continue;
    }
    h_TOF->Fill(RecData.TOF);
    //Fill Histograms
    bool cond;
    cond = RecData.MCP_TXSum>CurrentAnalyzer->TXSumLow && RecData.MCP_TXSum<CurrentAnalyzer->TXSumHigh && RecData.MCP_TYSum>CurrentAnalyzer->TYSumLow && RecData.MCP_TYSum<CurrentAnalyzer->TYSumHigh;

    double X = RecData.MCPPos_X;
    double Y = RecData.MCPPos_Y;
    double XCorrected;
    double YCorrected;
    bool cond2 = false;
    int returnval=0;
    int IndexX=0;
    int IndexY=0;
    returnval = FindSquare(SquareArray,sizeX,sizeY,X,Y,IndexX,IndexY,NPointEdge);
    if (returnval==1){
      h_XT1T2[IndexX][IndexY]->Fill(RecData.MCP_TXSum);
      h_YT1T2[IndexX][IndexY]->Fill(RecData.MCP_TYSum);
      for (int ii=0;ii<6;ii++){
	XFitPar[ii]=SquareArray[IndexX][IndexY].XFitPar[ii];
	YFitPar[ii]=SquareArray[IndexX][IndexY].YFitPar[ii];
      }
      X -= (startX+4.0*IndexX+2.0);
      Y -= (startY+4.0*IndexY+2.0);
      XCorrected = XFitPar[0]*X*X+XFitPar[1]*X*Y+XFitPar[2]*Y*Y+XFitPar[3]*X+XFitPar[4]*Y+XFitPar[5] + startX +4.0*IndexX+2.0;
      YCorrected = YFitPar[0]*X*X+YFitPar[1]*X*Y+YFitPar[2]*Y*Y+YFitPar[3]*X+YFitPar[4]*Y+YFitPar[5] + startY +4.0*IndexY+2.0;
      cond2=true;
    }else{
      cond2=false;
    }
    if (cond && cond2){
      h_MCPImage_Cal->Fill(XCorrected,YCorrected);
      h_MCPImage_CalLowRes->Fill(XCorrected,YCorrected);
      h_MCPImage_CalZoom->Fill(XCorrected,YCorrected);
      h_MCPImage_2ndCal->Fill(XCorrected,YCorrected);
    }
  }

  //Initialize Grid for resolution measurement
  GridArray = new GridPoint*[sizeX+1];
  for (int i=0;i<sizeX+1;i++){
    GridArray[i] = new GridPoint[sizeY+1];
  }
  for (int i=0;i<sizeX+1;i++){
    for (int j=0;j<sizeY+1;j++){
      GridArray[i][j].x = startX+4.0*i;
      GridArray[i][j].y = startY+4.0*j;
      GridArray[i][j].status=0;
    }
  }      
  //Enable grid points for inner squares only
  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      if (SquareArray[i][j].status==1){
	if (i==0 || (i==sizeX-1) || j==0 || (j==sizeY-1)){
	  continue;
	}else{
	  if (SquareArray[i-1][j].status==1 && SquareArray[i+1][j].status==1 && SquareArray[i][j-1].status==1 && SquareArray[i][j+1].status==1 ){
	    GridArray[i][j].status=1;
	    GridArray[i+1][j].status=1;
	    GridArray[i][j+1].status=1;
	    GridArray[i+1][j+1].status=1;
	  }
	}
      }
    }
  }      
  FineTuneGrid(GridArray,sizeX+1,sizeY+1,h_MCPImage_2ndCal,0.8);
  int NGrid = 0;
  for (int i=0;i<sizeX+1;i++){
    for (int j=0;j<sizeY+1;j++){
      if (GridArray[i][j].status==1){
	double XTrue = startX+4.0*i;
	double YTrue = startY+4.0*j;
	hDeviation->Fill(1000*sqrt(pow((GridArray[i][j].x-XTrue),2.0)+pow((GridArray[i][j].y-YTrue),2.0)));
	hDeviationX->Fill(1000*(GridArray[i][j].x-XTrue));
	hDeviationY->Fill(1000*(GridArray[i][j].y-YTrue));
	Grid->SetPoint(NGrid,GridArray[i][j].x,GridArray[i][j].y);
	NGrid++;
      }
    }
  }      
  //Construct Grid Edges
  HEdges = new GridEdge*[sizeY+1];
  for (int i=0;i<sizeY+1;i++){
    HEdges[i] = new GridEdge[sizeX];
  }
  VEdges = new GridEdge*[sizeX+1];
  for (int i=0;i<sizeX+1;i++){
    VEdges[i] = new GridEdge[sizeY];
  }

  FindEdges(HEdges,VEdges,GridArray,sizeX+1,sizeY+1,h_MCPImage_2ndCal,NPointEdge);
  NGrid = 0;
  for (int i=0;i<sizeY+1;i++){
    for (int j=0;j<sizeX;j++){
      if (HEdges[i][j].status!=1)continue;
      for (int Index=1;Index<=NPointEdge-2;Index++){
	double XTrue = startX+4.0*j+Index*(4.0/double(NPointEdge-1));
	double YTrue = startY+4.0*i;
	hDeviation->Fill(1000*sqrt(pow((HEdges[i][j].PointList[Index].x-XTrue),2.0)+pow((HEdges[i][j].PointList[Index].y-YTrue),2.0)));
	hDeviationX->Fill(1000*(HEdges[i][j].PointList[Index].x-XTrue));
	hDeviationY->Fill(1000*(HEdges[i][j].PointList[Index].y-YTrue));
	GridEdgeH->SetPoint(NGrid,HEdges[i][j].PointList[Index].x,HEdges[i][j].PointList[Index].y);
	NGrid++;
      }
    }
  }
  NGrid = 0;
  for (int i=0;i<sizeX+1;i++){
    for (int j=0;j<sizeY;j++){
      if (VEdges[i][j].status!=1)continue;
      for (int Index=1;Index<=NPointEdge-2;Index++){
	double XTrue = startX+4.0*i;
	double YTrue = startY+4.0*j+Index*(4.0/double(NPointEdge-1));
	hDeviation->Fill(1000*sqrt(pow((VEdges[i][j].PointList[Index].x-XTrue),2.0)+pow((VEdges[i][j].PointList[Index].y-YTrue),2.0)));
	hDeviationX->Fill(1000*(VEdges[i][j].PointList[Index].x-XTrue));
	hDeviationY->Fill(1000*(VEdges[i][j].PointList[Index].y-YTrue));
	GridEdgeV->SetPoint(NGrid,VEdges[i][j].PointList[Index].x,VEdges[i][j].PointList[Index].y);
	NGrid++;
      }
    }
  }

  //Determine resolutions
  double LeftRes;
  double RightRes;
  double LineWidth;

  NGrid=0;
  for (int i=0;i<sizeY+1;i++){
    for (int j=0;j<sizeX;j++){
      if (HEdges[i][j].status!=1)continue;
      double GridX = HEdges[i][j].PointList[2].x;
      double GridY = HEdges[i][j].PointList[2].y;
      LocalFit(h_MCPImage_2ndCal,LocalProjects1[NGrid],GridX,GridY,"H",LeftRes,RightRes,LineWidth);
      LocalProjects1[NGrid]->SetName(Form("LocalProjectsH%d",NGrid));
      hBottomRes->Fill(1000*LeftRes);
      hTopRes->Fill(1000*RightRes);
      hHLineWidth->Fill(1000*LineWidth);
      GBottomRes->SetPoint(NGrid,GridX,GridY,LeftRes);
      GTopRes->SetPoint(NGrid,GridX,GridY,RightRes);
      GHLineWidth->SetPoint(NGrid,GridX,GridY,LineWidth);
      cout <<"Fit ID "<<NGrid<<endl;
      NGrid++;
    }
  }
  NGrid=0;
  for (int i=0;i<sizeX+1;i++){
    for (int j=0;j<sizeY;j++){
      if (VEdges[i][j].status!=1)continue;
      double GridX = VEdges[i][j].PointList[2].x;
      double GridY = VEdges[i][j].PointList[2].y;
      LocalFit(h_MCPImage_2ndCal,LocalProjects2[NGrid],GridX,GridY,"V",LeftRes,RightRes,LineWidth);
      LocalProjects2[NGrid]->SetName(Form("LocalProjectsV%d",NGrid));
      hLeftRes->Fill(1000*LeftRes);
      hRightRes->Fill(1000*RightRes);
      hVLineWidth->Fill(1000*LineWidth);
      GLeftRes->SetPoint(NGrid,GridX,GridY,LeftRes);
      GRightRes->SetPoint(NGrid,GridX,GridY,RightRes);
      GVLineWidth->SetPoint(NGrid,GridX,GridY,LineWidth);
      cout <<"Fit ID "<<NGrid<<endl;
      //Take one local project
      if (i==8 && j==9)OneLocalProject = LocalProjects2[NGrid];
      NGrid++;
    }
  }

  //T1T2 Graph
  int NT1T2=0;
  for(int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      if (SquareArray[i][j].status==1){
	double GridX = startX+4.0*i+2.0;
	double GridY = startY+4.0*j+2.0;
	TF1* FitFunc1 = new TF1("FitFunc1","gaus",0,200);
	TF1* FitFunc2 = new TF1("FitFunc2","gaus",0,200);
	int maxbin1 = h_XT1T2[i][j]->GetMaximumBin();
	int maxbin2 = h_YT1T2[i][j]->GetMaximumBin();
	double maxVal1 = h_XT1T2[i][j]->GetBinCenter(maxbin1);
	double maxVal2 = h_YT1T2[i][j]->GetBinCenter(maxbin2);
	FitFunc1->SetParameters(h_XT1T2[i][j]->GetMaximum(),maxVal1,0.1);
	FitFunc2->SetParameters(h_YT1T2[i][j]->GetMaximum(),maxVal2,0.1);
	h_XT1T2[i][j]->Fit(FitFunc1,"","",maxVal1-5,maxVal1+5);
	h_YT1T2[i][j]->Fit(FitFunc2,"","",maxVal2-5,maxVal2+5);
	GXT1T2->SetPoint(NT1T2,GridX,GridY,h_XT1T2[i][j]->GetFunction("FitFunc1")->GetParameter(1));
	GYT1T2->SetPoint(NT1T2,GridX,GridY,h_YT1T2[i][j]->GetFunction("FitFunc2")->GetParameter(1));
	GXT1T2Res->SetPoint(NT1T2,GridX,GridY,h_XT1T2[i][j]->GetFunction("FitFunc1")->GetParameter(2));
	GYT1T2Res->SetPoint(NT1T2,GridX,GridY,h_YT1T2[i][j]->GetFunction("FitFunc2")->GetParameter(2));
	NT1T2++;
      }
    }
  }
  
  //Construct a higher resolution grid graph, specially for triple coincidence data
  FineTuneGrid(GridArray,sizeX+1,sizeY+1,h_MCPImage_2ndCal,2.0);
  NGrid = 0;
  for (int i=0;i<sizeX+1;i++){
    for (int j=0;j<sizeY+1;j++){
      if (GridArray[i][j].status==1){
	double XTrue = startX+4.0*i;
	double YTrue = startY+4.0*j;
	hDeviationHR->Fill(1000*sqrt(pow((GridArray[i][j].x-XTrue),2.0)+pow((GridArray[i][j].y-YTrue),2.0)));
	GridHR->SetPoint(NGrid,GridArray[i][j].x,GridArray[i][j].y);
	NGrid++;
      }
    }
  }      


  //Write to root file
  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_MCPResCal.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");

  h_TOF->Write();
  h_MCPImage_Cal->Write();
  h_MCPImage_CalLowRes->Write();
  h_MCPImage_CalZoom->Write();
  Grid->Write();
  GridEdgeH->Write();
  GridEdgeV->Write();
  hDeviation->Write();
  hDeviationX->Write();
  hDeviationY->Write();
  GridHR->Write();
  hDeviationHR->Write();
  hLeftRes->Write();
  hRightRes->Write();
  hVLineWidth->Write();
  hBottomRes->Write();
  hTopRes->Write();
  hHLineWidth->Write();

  GLeftRes->Write();
  GRightRes->Write();
  GVLineWidth->Write();
  GBottomRes->Write();
  GTopRes->Write();
  GHLineWidth->Write();
  GXT1T2->Write();
  GYT1T2->Write();
  GXT1T2Res->Write();
  GYT1T2Res->Write();

  //Output T1+T2
  for(int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      if (SquareArray[i][j].status==1){
	h_XT1T2[i][j]->Write();
	h_YT1T2[i][j]->Write();
      }
    }
  }

  for (int i=0;i<NGrid;i++){
    LocalProjects1[i]->Write();
    LocalProjects2[i]->Write();
  }
  histfile->Close();

  //Additional file
  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_MCPResCalAddition.root");
  histfile = new TFile(spectrumfile.c_str(),"recreate");
  h_MCPImage_CalLowRes->Write();
  h_MCPImage_CalZoom->Write();
  OneLocalProject->Write();
  Grid->Write();
  GridEdgeH->Write();
  GridEdgeV->Write();
  hDeviation->Write();
  hDeviationX->Write();
  hDeviationY->Write();
  hLeftRes->Write();
  hRightRes->Write();
  hVLineWidth->Write();
  hBottomRes->Write();
  hTopRes->Write();
  hHLineWidth->Write();
  histfile->Close();
  CurrentAnalyzer->Calibrating = false;
  
  //Close input file
  if (tfilein!=NULL){
    tfilein->Close();
    delete tfilein;
    tfilein =NULL;
  }
  return 0;
}

/***********************************************************************************/
//Find the rim of the circle
void FindRim(TGraph* Rim,TH1** RimSearch)
{
  TH1* h_MCPImageProjectX;
  TH1* h_MCPImageProjectY;
  double CountThreshold=10;
  int GraphIndex = Rim->GetN();
  for (int j=0;j<10;j++){
    double LeftEdge=-50.0;
    double RightEdge=50.0;
    RimSearch[j]->Smooth(4);
    for (int i=1;i<=2000;i++){
      LeftEdge = RimSearch[j]->GetBinCenter(i);
      if (RimSearch[j]->GetBinContent(i)>CountThreshold)break;
    }
    for (int i=2000;i>=1;i--){
      RightEdge = RimSearch[j]->GetBinCenter(i);
      if (RimSearch[j]->GetBinContent(i)>CountThreshold)break;
    }

    //Set points for the graph
    double Angle = (-45.0 + j*9.0)/180.0*PI;
    Rim->SetPoint(GraphIndex,LeftEdge*cos(Angle),LeftEdge*sin(Angle));
    Rim->SetPoint(GraphIndex+1,RightEdge*cos(Angle),RightEdge*sin(Angle));
    GraphIndex+=2;
  }
  for (int j=10;j<20;j++){
    double LeftEdge=-50.0;
    double RightEdge=50.0;
    RimSearch[j]->Smooth(4);
    for (int i=1;i<=2000;i++){
      LeftEdge = RimSearch[j]->GetBinCenter(i);
      if (RimSearch[j]->GetBinContent(i)>CountThreshold)break;
    }
    for (int i=2000;i>=1;i--){
      RightEdge = RimSearch[j]->GetBinCenter(i);
      if (RimSearch[j]->GetBinContent(i)>CountThreshold)break;
    }

    //Set points for the graph
    double Angle = (-45.0 + (j-10+1)*9.0)/180.0*PI;
    Rim->SetPoint(GraphIndex,LeftEdge*sin(Angle),LeftEdge*cos(Angle));
    Rim->SetPoint(GraphIndex+1,RightEdge*sin(Angle),RightEdge*cos(Angle));
    GraphIndex+=2;
  }
}
//Find the grid in the circle
void InitGrid(vector<double>& XPoints,vector<double>& YPoints,TH2* GridSearch)
{
  TH1* h_MCPImageProjectX;
  TH1* h_MCPImageProjectY;
  double CountThreshold=10;
  double a_discriminationX1=5;
  double a_discriminationX2=5;
  double a_discriminationX3=5;
  double a_discriminationY1=4;
  double Xlow = -5;
  double Xhigh = 3;
  double Ylow = -5;
  double Yhigh =3;
  int XlowBin = (Xlow+50)*20;
  int XhighBin = (Xhigh+50)*20;
  int YlowBin = (Ylow+50)*20;
  int YhighBin = (Yhigh+50)*20;
  //Project the center out
  h_MCPImageProjectX = GridSearch->ProjectionX("ProjectionX",YlowBin,YhighBin);
  h_MCPImageProjectY = GridSearch->ProjectionY("ProjectionY",XlowBin,XhighBin);
  h_MCPImageProjectX->Smooth(15);
  h_MCPImageProjectY->Smooth(15);
  //Find local minima
  TGraph* minimaX = new TGraph();
  minimaX->SetName("minimaX");
  TGraph* minimaY = new TGraph();
  minimaY->SetName("minimaY");
  int NX=0;
  TFile* testfile = new TFile("test.root","recreate");
  for (int i=2;i<1999;i++){
    double current=h_MCPImageProjectX->GetBinContent(i);
    double last=h_MCPImageProjectX->GetBinContent(i-1);
    double next=h_MCPImageProjectX->GetBinContent(i+1);
    double currentPos = h_MCPImageProjectX->GetBinCenter(i);
    if(current<last && current<next){
      if (currentPos<20){
	h_MCPImageProjectX->Fit("pol2","","",currentPos-0.3,currentPos+0.3); 
	double a = h_MCPImageProjectX->GetFunction("pol2")->GetParameter(2);
	double b = h_MCPImageProjectX->GetFunction("pol2")->GetParameter(1);
	double c = h_MCPImageProjectX->GetFunction("pol2")->GetParameter(0);
	int centerbin = h_MCPImageProjectX->FindBin(-b/(2*a));
	double norm = h_MCPImageProjectX->GetBinContent(centerbin+10);
//	cout <<a/norm<<endl;
	if (a/norm>a_discriminationX1 && abs(-b/(2*a)-currentPos)<0.08){
//	  minimaX->SetPoint(NX,currentPos,current);
	  minimaX->SetPoint(NX,-b/(2*a),current);
	  NX++;
	}
      }else if(currentPos>20 && currentPos<30){
	h_MCPImageProjectX->Fit("pol2","","",currentPos-0.3,currentPos+0.3); 
	double a = h_MCPImageProjectX->GetFunction("pol2")->GetParameter(2);
	double b = h_MCPImageProjectX->GetFunction("pol2")->GetParameter(1);
	double c = h_MCPImageProjectX->GetFunction("pol2")->GetParameter(0);
	int centerbin = h_MCPImageProjectX->FindBin(-b/(2*a));
	double norm = h_MCPImageProjectX->GetBinContent(centerbin+10);
//	cout <<a/norm<<endl;
	if (a/norm>a_discriminationX2 && abs(-b/(2*a)-currentPos)<0.08){
//	  minimaX->SetPoint(NX,currentPos,current);
	  minimaX->SetPoint(NX,-b/(2*a),current);
	  NX++;
	}
      }else if(currentPos>30){
	h_MCPImageProjectX->Fit("pol2","","",currentPos-0.3,currentPos+0.3); 
	double a = h_MCPImageProjectX->GetFunction("pol2")->GetParameter(2);
	double b = h_MCPImageProjectX->GetFunction("pol2")->GetParameter(1);
	double c = h_MCPImageProjectX->GetFunction("pol2")->GetParameter(0);
	int centerbin = h_MCPImageProjectX->FindBin(-b/(2*a));
	double norm = h_MCPImageProjectX->GetBinContent(centerbin+10);
//	cout <<a/norm<<endl;
	if (a/norm>a_discriminationX3 && abs(-b/(2*a)-currentPos)<0.1){
//	  minimaX->SetPoint(NX,currentPos,current);
	  minimaX->SetPoint(NX,-b/(2*a),current);
	  NX++;
	}
      }
    }
  }
  int NY=0;
  for (int i=2;i<1999;i++){
    double current=h_MCPImageProjectY->GetBinContent(i);
    double last=h_MCPImageProjectY->GetBinContent(i-1);
    double next=h_MCPImageProjectY->GetBinContent(i+1);
    double currentPos = h_MCPImageProjectY->GetBinCenter(i);
    if(current<last && current<next){
      if (currentPos<40){
	h_MCPImageProjectY->Fit("pol2","","",currentPos-0.3,currentPos+0.3); 
	double a = h_MCPImageProjectY->GetFunction("pol2")->GetParameter(2);
	double b = h_MCPImageProjectY->GetFunction("pol2")->GetParameter(1);
	double c = h_MCPImageProjectY->GetFunction("pol2")->GetParameter(0);
	int centerbin = h_MCPImageProjectY->FindBin(-b/(2*a));
	double norm = h_MCPImageProjectY->GetBinContent(centerbin+10);
	if (a/norm>a_discriminationY1 && abs(-b/(2*a)-currentPos)<0.08){
//	  minimaY->SetPoint(NY,currentPos,current);
	  minimaY->SetPoint(NY,-b/(2*a),current);
	  NY++;
	}
      }    
    }
  }
  //Create the grid
  for (int i=0;i<NX;i++){
    double X;
    double XCount;
    minimaX->GetPoint(i,X,XCount);
    XPoints.push_back(X);
  }
  for (int j=0;j<NY;j++){
    double Y;
    double YCount;
    minimaY->GetPoint(j,Y,YCount);
    YPoints.push_back(Y);
  }

  h_MCPImageProjectX->Write();
  h_MCPImageProjectY->Write();
  minimaX->Write();
  minimaY->Write();
  testfile->Close();
  //END
}

void AdjustGrid(GridPoint **GridArray,int sizeX,int sizeY){
  for (int i=0;i<sizeX/2;i++){
    if (i!=0){
      if (GridArray[i-1][0].status==1)break;
    }
    for (int j=0;j<sizeY;j++){
      if (GridArray[i][j].status==1){
//	cout <<i<<" "<<j<<" "<< GridArray[i][j].x<<" " << GridArray[i][j].y <<endl;
	if (j!=0)GridArray[i][j-1].x=GridArray[i][j].x;
	if (i!=0)GridArray[i-1][j].y=GridArray[i][j].y;
	break;
      }
    }
    for (int j=sizeY-1;j>=0;j--){
      if (GridArray[i][j].status==1){
	if (j!=(sizeY-1))GridArray[i][j+1].x=GridArray[i][j].x;
	if (i!=0)GridArray[i-1][j].y=GridArray[i][j].y;
	break;
      }
    }
  }
  for (int i=sizeX-1;i>sizeX/2;i--){
    if (i!=(sizeX-1)){
      if (GridArray[i+1][0].status==1)break;
    }
    for (int j=0;j<sizeY;j++){
      if (GridArray[i][j].status==1){
	if (j!=0)GridArray[i][j-1].x=GridArray[i][j].x;
	if (i!=(sizeX-1))GridArray[i+1][j].y=GridArray[i][j].y;
	break;
      }
    }
    for (int j=sizeY-1;j>=0;j--){
      if (GridArray[i][j].status==1){
	if (j!=(sizeY-1))GridArray[i][j+1].x=GridArray[i][j].x;
	if (i!=(sizeX-1))GridArray[i+1][j].y=GridArray[i][j].y;
	break;
      }
    }
  }
}

void AdjustFailed(GridPoint** GridArray,int sizeX,int sizeY,double shift)
{
  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      if (GridArray[i][j].status==2){
	if (GridArray[i][j].x<0)GridArray[i][j].x+=shift;
	if (GridArray[i][j].x>=0)GridArray[i][j].x-=shift;
	if (GridArray[i][j].y<0)GridArray[i][j].y+=shift;
	if (GridArray[i][j].y>=0)GridArray[i][j].y-=shift;
      }
    }
  }
}

void FindGrid(GridPoint **GridArray,int sizeX,int sizeY,TH2* GridSearch,double BinRange,bool FailedOnly)
{
  //Fine tune the grid
  int BinN = int(BinRange*2*20);
  TH2* LocalImage = new TH2D("LocalImage","LocalImage",BinN,-BinRange,BinRange,BinN,-BinRange,BinRange);
  TH1* LocalProjectX;
  TH1* LocalProjectY;

  //Get the average count per local Image
  double TotCount = GridSearch->Integral();
  double CountThreshold = TotCount/(PI*37.5*37.5)*BinRange*BinRange*4*0.1;
  TFile* testfile = new TFile("testGrid.root","recreate");
  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      if (GridArray[i][j].status==0)continue;//jump around the far away points
      if (FailedOnly && GridArray[i][j].status!=2)continue;
      double GridX = GridArray[i][j].x;
      double GridY = GridArray[i][j].y;
      for (int ii=1;ii<=BinN;ii++){
	for (int jj=1;jj<=BinN;jj++){
	  if (((GridX-BinRange+50.0)*20.0+ii)<=0 || ((GridY-BinRange+50.0)*20.0+jj)<=0)continue;
	  if (((GridX-BinRange+50.0)*20.0+ii)>2000 || ((GridY-BinRange+50.0)*20.0+jj)>2000)continue;
	  double BinVal = GridSearch->GetBinContent((GridX-BinRange+50.0)*20.0+ii,(GridY-BinRange+50.0)*20.0+jj);
	  LocalImage->SetBinContent(ii,jj,BinVal);
	}
      }

      LocalImage->SetName(Form("LocalImage%d,%d",i,j));
      //Fit the curve bottom
      //Find minimum
      LocalProjectX = LocalImage->ProjectionX(Form("ProjectXPoint%d,%d",i,j));
      LocalProjectX->Smooth(5);
      double XMinBin = LocalProjectX->GetMinimumBin();
      double XMin = LocalProjectX->GetBinCenter(XMinBin);
      LocalProjectY = LocalImage->ProjectionY(Form("ProjectYPoint%d,%d",i,j));
      LocalProjectY->Smooth(5);
      double YMinBin = LocalProjectY->GetMinimumBin();
      double YMin = LocalProjectY->GetBinCenter(YMinBin);
      double NewXMin = XMin;
      double NewYMin = YMin;
      bool XSuccess=false;
      bool YSuccess=false;

      //Test if there are enough events int this region
      if (LocalImage->Integral()>CountThreshold){
	if (XMin>-0.75*BinRange && XMin<0.75*BinRange){
	  LocalProjectX->Fit("pol2","","",XMin-0.2,XMin+0.2);
	  double a = LocalProjectX->GetFunction("pol2")->GetParameter(2);
	  double b = LocalProjectX->GetFunction("pol2")->GetParameter(1);
	  NewXMin = -b/(2.0*a);
	  XSuccess=true;
	}
	if (YMin>-0.75*BinRange && YMin<0.75*BinRange){
	  LocalProjectY->Fit("pol2","","",YMin-0.2,YMin+0.2);
	  double a = LocalProjectY->GetFunction("pol2")->GetParameter(2);
	  double b = LocalProjectY->GetFunction("pol2")->GetParameter(1);
	  NewYMin = -b/(2.0*a);
	  YSuccess=true;
	}

	if (FailedOnly){
	  if (!XSuccess){
	    double XMaxVal = LocalProjectX->GetMaximum();
	    if (XMin<-0.75*BinRange){
	      for (int ix=1;ix<BinN/2;ix++){
		LocalProjectX->SetBinContent(ix,XMaxVal);
		XMinBin = LocalProjectX->GetMinimumBin();
		XMin = LocalProjectX->GetBinCenter(XMinBin);
		double XMinVal = LocalProjectX->GetMinimum();
		if (XMinBin>ix+1 && XMin<BinN-1 && XMinVal>2){
		  LocalProjectX->Fit("pol2","","",XMin-0.1,XMin+0.1);
		  double a = LocalProjectX->GetFunction("pol2")->GetParameter(2);
		  double b = LocalProjectX->GetFunction("pol2")->GetParameter(1);
		  NewXMin = -b/(2.0*a);
		  XSuccess=true;
		  break;
		}
	      }
	    }
	    if (XMin>0.75*BinRange){
	      for (int ix=BinN;ix>BinN/2;ix--){
		LocalProjectX->SetBinContent(ix,XMaxVal);
		XMinBin = LocalProjectX->GetMinimumBin();
		XMin = LocalProjectX->GetBinCenter(XMinBin);
		double XMinVal = LocalProjectX->GetMinimum();
		if (XMinBin>1 && XMinBin<ix-1 && XMinVal>2){
		  LocalProjectX->Fit("pol2","","",XMin-0.1,XMin+0.1);
		  double a = LocalProjectX->GetFunction("pol2")->GetParameter(2);
		  double b = LocalProjectX->GetFunction("pol2")->GetParameter(1);
		  NewXMin = -b/(2.0*a);
		  XSuccess=true;
		  break;
		}
	      }
	    }
	  }
	  if (!YSuccess){
	    double YMaxVal = LocalProjectY->GetMaximum();
	    if (YMin<-0.75*BinRange){
	      for (int iy=1;iy<BinN/2;iy++){
		LocalProjectY->SetBinContent(iy,YMaxVal);
		YMinBin = LocalProjectY->GetMinimumBin();
		YMin = LocalProjectY->GetBinCenter(YMinBin);
		double YMinVal = LocalProjectY->GetMinimum();
		if (YMinBin>iy+1 && YMinBin<BinN-1 && YMinVal>2){
		  LocalProjectY->Fit("pol2","","",YMin-0.1,YMin+0.1);
		  double a = LocalProjectY->GetFunction("pol2")->GetParameter(2);
		  double b = LocalProjectY->GetFunction("pol2")->GetParameter(1);
		  NewYMin = -b/(2.0*a);
		  YSuccess=true;
		  break;
		}
	      }
	    }
	    if (YMin>0.75*BinRange){
	      for (int iy=BinN;iy>BinN/2;iy--){
		LocalProjectY->SetBinContent(iy,YMaxVal);
		YMinBin = LocalProjectY->GetMinimumBin();
		YMin = LocalProjectY->GetBinCenter(YMinBin);
		double YMinVal = LocalProjectY->GetMinimum();
		if (YMinBin>1 && YMinBin<iy-1 && YMinVal>2){
		  LocalProjectY->Fit("pol2","","",YMin-0.1,YMin+0.1);
		  double a = LocalProjectY->GetFunction("pol2")->GetParameter(2);
		  double b = LocalProjectY->GetFunction("pol2")->GetParameter(1);
		  NewYMin = -b/(2.0*a);
		  YSuccess=true;
		  break;
		}
	      }
	    }
	    //	  LocalImage->Write();
	    //	  LocalProjectX->Write();
	    //	  LocalProjectY->Write();
	  }
	  /*
	  //	GridY+=NewYMin;
	  double Xincrement = 0.0;
	  //	if (GridY>0)GridY-=BinRange;
	  //	if (GridY<0)GridY+=BinRange;
	  if (XMin<-0.9*BinRange)Xincrement = 0.2*BinRange;
	  if (XMin>0.9*BinRange)Xincrement = -0.2*BinRange;

	  for (int ix=1;ix<=5;ix++){
	  GridX+=Xincrement;
	  for (int ii=1;ii<=BinN;ii++){
	  for (int jj=1;jj<=BinN;jj++){
	  if (((GridX-BinRange+50.0)*20.0+ii)<=0 || ((GridY-BinRange+50.0)*20.0+jj)<=0)continue;
	  if (((GridX-BinRange+50.0)*20.0+ii)>2000 || ((GridY-BinRange+50.0)*20.0+jj)>2000)continue;
	  double BinVal = GridSearch->GetBinContent((GridX-BinRange+50.0)*20.0+ii,(GridY-BinRange+50.0)*20.0+jj);
	  LocalImage->SetBinContent(ii,jj,BinVal);
	  }
	  }

	  LocalProjectX = LocalImage->ProjectionX(Form("ProjectXPoint%d,%d",i,j));
	  LocalProjectX->Smooth(5);
	  XMinBin = LocalProjectX->GetMinimumBin();
	  XMin = LocalProjectX->GetBinCenter(XMinBin);
	  if (XMin>-0.9*BinRange && XMin<0.9*BinRange){
	  LocalProjectX->Fit("pol2","","",XMin-0.2,XMin+0.2);
	  double a = LocalProjectX->GetFunction("pol2")->GetParameter(2);
	  double b = LocalProjectX->GetFunction("pol2")->GetParameter(1);
	  NewXMin = -b/(2.0*a)+ix*Xincrement;
	  XSuccess=true;
	  break;
	  }
	  }
	   */
	}
      }
      if (!XSuccess || !YSuccess){
	GridArray[i][j].status=2;
/*	LocalImage->Write();
	LocalProjectX->Write();
	LocalProjectY85>Write();*/
	continue; 
      }

      //Update the grid position
      GridArray[i][j].status=1;
      GridArray[i][j].x = GridX+NewXMin;
      GridArray[i][j].y = GridY+NewYMin;
          LocalImage->Write();
          LocalProjectX->Write();
          LocalProjectY->Write();

    }
  }
  testfile->Close();
}
/***********************************************************************************/
void FineTuneGrid(GridPoint **GridArray,int sizeX,int sizeY,TH2* GridSearch,double BinRange)
{
  //Fine tune the grid
  double BinWidth = 0.01;
  int BinN = int(BinRange*2/BinWidth);
  TH2* LocalImage = new TH2D("LocalImage","LocalImage",BinN,-BinRange,BinRange,BinN,-BinRange,BinRange);
  TH1* LocalProjectXBottom;
  TH1* LocalProjectXUp;
  TH1* LocalProjectYLeft;
  TH1* LocalProjectYRight;

  //Get the average count per local Image
  double TotCount = GridSearch->Integral();
  double CountThreshold = TotCount/(PI*37.5*37.5)*BinRange*BinRange*4*0.1;
  TFile* testfile = new TFile("testGridFineTune.root","recreate");
  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      if (GridArray[i][j].status!=1)continue;
      double GridX = GridArray[i][j].x;
      double GridY = GridArray[i][j].y;
      for (int ii=1;ii<=BinN;ii++){
	for (int jj=1;jj<=BinN;jj++){
	  int IndexX = ((GridX-BinRange+50.0)*100.0+ii);
	  int IndexY = ((GridY-BinRange+50.0)*100.0+jj);
	  if (IndexX<=0 || IndexY<=0)continue;
	  if (IndexX>10000 || IndexY>10000)continue;
	  double BinVal = GridSearch->GetBinContent(IndexX,IndexY);
	  LocalImage->SetBinContent(ii,jj,BinVal);
	}
      }

      LocalImage->SetName(Form("LocalImage%d,%d",i,j));

      //Find the true center of the cross point
      double X_Correction=0.0;
      double Y_Correction=0.0;
      
      //Parameters
      double EdgeLimHigh = 0.25;
      double EdgeLimLow = 0.05;
      double HistNormLim = 0.4;
      double FitRange = 0.6;
      double NormVeto = 0.2;

      if ((j==0 || (j!=0 && GridArray[i][j-1].status!=1)) && (i==0 || (i!=0 && GridArray[i-1][j].status!=1))){
	//Bottom-left corner
	double HistNorm1=1;
	double HistNorm2=1;
	LocalProjectXUp = LocalImage->ProjectionX(Form("ProjectXUpPoint%d,%d",i,j),BinN/2,BinN);
	LocalProjectYRight = LocalImage->ProjectionY(Form("ProjectRightYPoint%d,%d",i,j),BinN/2,BinN);

	//Fit to 2 Erro-functions plus a background
	TF1* FitFuncUp = new TF1("FitFuncUp","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	HistNorm1=LocalProjectXUp->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	HistNorm2=LocalProjectXUp->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	FitFuncUp->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	FitFuncUp->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	FitFuncUp->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	FitFuncUp->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	FitFuncUp->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	LocalProjectXUp->Fit(FitFuncUp,"","",-FitRange,FitRange);

	TF1* FitFuncRight = new TF1("FitFuncRight","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	HistNorm1=LocalProjectYRight->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	HistNorm2=LocalProjectYRight->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	FitFuncRight->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	FitFuncRight->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	FitFuncRight->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	FitFuncRight->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	FitFuncRight->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	LocalProjectYRight->Fit(FitFuncRight,"","",-FitRange,FitRange);

	//Generate corrections
	X_Correction = (FitFuncUp->GetParameter(1)+FitFuncUp->GetParameter(4))/2.0;
	Y_Correction = (FitFuncRight->GetParameter(1)+FitFuncRight->GetParameter(4))/2.0;

	LocalImage->Write();
	LocalProjectXUp->Write();
	LocalProjectYRight->Write();
      }
      else if ((j==0 || (j!=0 && GridArray[i][j-1].status!=1)) && (i==(sizeX-1) || (i!=(sizeX-1) && GridArray[i+1][j].status!=1))){
	//Bottom-right corner
	double HistNorm1=1;
	double HistNorm2=1;
	LocalProjectXUp = LocalImage->ProjectionX(Form("ProjectXUpPoint%d,%d",i,j),BinN/2,BinN);
	LocalProjectYLeft = LocalImage->ProjectionY(Form("ProjectYLeftPoint%d,%d",i,j),0,BinN/2);

	//Fit to 2 Erro-functions plus a background
	TF1* FitFuncUp = new TF1("FitFuncUp","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	HistNorm1=LocalProjectXUp->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	HistNorm2=LocalProjectXUp->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	FitFuncUp->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	FitFuncUp->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	FitFuncUp->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	FitFuncUp->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	FitFuncUp->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	LocalProjectXUp->Fit(FitFuncUp,"","",-FitRange,FitRange);

	TF1* FitFuncLeft = new TF1("FitFuncLeft","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	HistNorm1=LocalProjectYLeft->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	HistNorm2=LocalProjectYLeft->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	FitFuncLeft->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	FitFuncLeft->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	FitFuncLeft->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	FitFuncLeft->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	FitFuncLeft->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	LocalProjectYLeft->Fit(FitFuncLeft,"","",-FitRange,FitRange);

	//Generate corrections
	X_Correction = (FitFuncUp->GetParameter(1)+FitFuncUp->GetParameter(4))/2.0;
	Y_Correction = (FitFuncLeft->GetParameter(1)+FitFuncLeft->GetParameter(4))/2.0;

	LocalImage->Write();
	LocalProjectXUp->Write();
	LocalProjectYLeft->Write();
      }
      else if ((j==(sizeY-1) || (j!=(sizeY-1) && GridArray[i][j+1].status!=1)) && (i==0 || (i!=0 && GridArray[i-1][j].status!=1))){
	//Up-left corner
	double HistNorm1=1;
	double HistNorm2=1;
	LocalProjectXBottom = LocalImage->ProjectionX(Form("ProjectXBottomPoint%d,%d",i,j),0,BinN/2);
	LocalProjectYRight = LocalImage->ProjectionY(Form("ProjectRightYPoint%d,%d",i,j),BinN/2,BinN);

	//Fit to 2 Erro-functions plus a background
	TF1* FitFuncBottom = new TF1("FitFuncBottom","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	HistNorm1=LocalProjectXBottom->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	HistNorm2=LocalProjectXBottom->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	FitFuncBottom->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	FitFuncBottom->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	FitFuncBottom->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	FitFuncBottom->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	FitFuncBottom->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	LocalProjectXBottom->Fit(FitFuncBottom,"","",-FitRange,FitRange);

	TF1* FitFuncRight = new TF1("FitFuncRight","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	HistNorm1=LocalProjectYRight->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	HistNorm2=LocalProjectYRight->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	FitFuncRight->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	FitFuncRight->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	FitFuncRight->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	FitFuncRight->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	FitFuncRight->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	LocalProjectYRight->Fit(FitFuncRight,"","",-FitRange,FitRange);

	//Generate corrections
	X_Correction = (FitFuncBottom->GetParameter(1)+FitFuncBottom->GetParameter(4))/2.0;
	Y_Correction = (FitFuncRight->GetParameter(1)+FitFuncRight->GetParameter(4))/2.0;

	LocalImage->Write();
	LocalProjectXBottom->Write();
	LocalProjectYRight->Write();
      }
      else if ((j==(sizeY-1) || (j!=(sizeY-1) && GridArray[i][j+1].status!=1)) && (i==(sizeX-1) || (i!=(sizeX-1) && GridArray[i+1][j].status!=1))){
	//Up-right corner
	double HistNorm1=1;
	double HistNorm2=1;
	LocalProjectXBottom = LocalImage->ProjectionX(Form("ProjectXBottomPoint%d,%d",i,j),0,BinN/2);
	LocalProjectYLeft = LocalImage->ProjectionY(Form("ProjectYLeftPoint%d,%d",i,j),0,BinN/2);

	//Fit to 2 Erro-functions plus a background
	TF1* FitFuncBottom = new TF1("FitFuncBottom","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	HistNorm1=LocalProjectXBottom->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	HistNorm2=LocalProjectXBottom->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	FitFuncBottom->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	FitFuncBottom->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	FitFuncBottom->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	FitFuncBottom->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	FitFuncBottom->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	LocalProjectXBottom->Fit(FitFuncBottom,"","",-FitRange,FitRange);

	TF1* FitFuncLeft = new TF1("FitFuncLeft","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	HistNorm1=LocalProjectYLeft->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	HistNorm2=LocalProjectYLeft->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	FitFuncLeft->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	FitFuncLeft->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	FitFuncLeft->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	FitFuncLeft->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	FitFuncLeft->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	LocalProjectYLeft->Fit(FitFuncLeft,"","",-FitRange,FitRange);

	//Generate corrections
	X_Correction = (FitFuncBottom->GetParameter(1)+FitFuncBottom->GetParameter(4))/2.0;
	Y_Correction = (FitFuncLeft->GetParameter(1)+FitFuncLeft->GetParameter(4))/2.0;

	LocalImage->Write();
	LocalProjectXBottom->Write();
	LocalProjectYLeft->Write();
      }
      else{
	//Elsewhere
	double HistNorm1=1;
	double HistNorm2=1;
	LocalProjectXBottom = LocalImage->ProjectionX(Form("ProjectXBottomPoint%d,%d",i,j),0,BinN/2);
	LocalProjectXUp = LocalImage->ProjectionX(Form("ProjectXUpPoint%d,%d",i,j),BinN/2,BinN);
	LocalProjectYLeft = LocalImage->ProjectionY(Form("ProjectYLeftPoint%d,%d",i,j),0,BinN/2);
	LocalProjectYRight = LocalImage->ProjectionY(Form("ProjectRightYPoint%d,%d",i,j),BinN/2,BinN);

	//Fit to 2 Erro-functions plus a background
	TF1* FitFuncBottom = new TF1("FitFuncBottom","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	HistNorm1=LocalProjectXBottom->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	HistNorm2=LocalProjectXBottom->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	FitFuncBottom->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	FitFuncBottom->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	FitFuncBottom->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	FitFuncBottom->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	FitFuncBottom->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	LocalProjectXBottom->Fit(FitFuncBottom,"","",-FitRange,FitRange);

	TF1* FitFuncUp = new TF1("FitFuncUp","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	HistNorm1=LocalProjectXUp->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	HistNorm2=LocalProjectXUp->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	FitFuncUp->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	FitFuncUp->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	FitFuncUp->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	FitFuncUp->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	FitFuncUp->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	LocalProjectXUp->Fit(FitFuncUp,"","",-FitRange,FitRange);

	TF1* FitFuncLeft = new TF1("FitFuncLeft","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	HistNorm1=LocalProjectYLeft->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	HistNorm2=LocalProjectYLeft->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	FitFuncLeft->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	FitFuncLeft->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	FitFuncLeft->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	FitFuncLeft->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	FitFuncLeft->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	LocalProjectYLeft->Fit(FitFuncLeft,"","",-FitRange,FitRange);

	TF1* FitFuncRight = new TF1("FitFuncRight","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	HistNorm1=LocalProjectYRight->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	HistNorm2=LocalProjectYRight->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	FitFuncRight->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	FitFuncRight->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	FitFuncRight->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	FitFuncRight->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	FitFuncRight->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	LocalProjectYRight->Fit(FitFuncRight,"","",-FitRange,FitRange);

	//Generate corrections
	X_Correction = (FitFuncBottom->GetParameter(1)+FitFuncUp->GetParameter(1)+FitFuncBottom->GetParameter(4)+FitFuncUp->GetParameter(4))/4.0;
	Y_Correction = (FitFuncLeft->GetParameter(1)+FitFuncRight->GetParameter(1)+FitFuncLeft->GetParameter(4)+FitFuncRight->GetParameter(4))/4.0;

	LocalImage->Write();
	LocalProjectXBottom->Write();
	LocalProjectXUp->Write();
	LocalProjectYLeft->Write();
	LocalProjectYRight->Write();
      }

      //Update the grid position
      GridArray[i][j].x = GridX+X_Correction;
      GridArray[i][j].y = GridY+Y_Correction;
    }
  }
  testfile->Close();
}
/***********************************************************************************/
void FindEdges(GridEdge** HEdges,GridEdge** VEdges,GridPoint** GridArray, int sizeX, int sizeY, TH2* GridSearch,int NPoint)
{
  if (NPoint>=10){
    cout << "Too many points per edge!"<<endl;
    return;
  }
  //Fine tune the grid
  double PointSeparation = 1.0;// Need to set dynamically , not just 4.0/double(NPoint-1);
  double BinRange = 4.0/double(NPoint-1)/2.0;
  double BinWidth = 0.01;
  int BinN = int(BinRange*2/BinWidth);
  //Parameters
  double EdgeLimHigh = 0.25;
  double EdgeLimLow = 0.05;
  double HistNormLim = 0.4;
  double FitRange = 0.6;
  double NormVeto = 0.2;
  int NRep = 2;

  TH2* LocalImage = new TH2D("LocalImage","LocalImage",BinN,-BinRange,BinRange,BinN,-BinRange,BinRange);
  TH1* LocalProject;

  TFile* testfile = new TFile("testGridFindEdge.root","recreate");
  //Horizontal
  for (int i=0;i<sizeY;i++){
    for (int j=0;j<sizeX-1;j++){
      if (GridArray[j][i].status!=1 || GridArray[j+1][i].status!=1){
	HEdges[i][j].status=0;
	continue;
      }
      //
      HEdges[i][j].status=1;
      HEdges[i][j].PointList[0].x=GridArray[j][i].x;
      HEdges[i][j].PointList[0].y=GridArray[j][i].y;
      HEdges[i][j].PointList[NPoint-1].x=GridArray[j+1][i].x;
      HEdges[i][j].PointList[NPoint-1].y=GridArray[j+1][i].y;
      PointSeparation=(GridArray[j+1][i].x-GridArray[j][i].x)/double(NPoint-1);
      for (int Index=1;Index<=NPoint-2;Index++){
	double GridX;
	double GridY;
	for (int r=0;r<NRep;r++){
	  if (r==0)GridY = GridArray[j][i].y;
	  else GridY = HEdges[i][j].PointList[Index].y;
	  GridX = GridArray[j][i].x+PointSeparation*Index;
	  for (int ii=1;ii<=BinN;ii++){
	    for (int jj=1;jj<=BinN;jj++){
	      int IndexX = ((GridX-BinRange+50.0)*100.0+ii);
	      int IndexY = ((GridY-BinRange+50.0)*100.0+jj);
	      if (IndexX<=0 || IndexY<=0)continue;
	      if (IndexX>10000 || IndexY>10000)continue;
	      double BinVal = GridSearch->GetBinContent(IndexX,IndexY);
	      LocalImage->SetBinContent(ii,jj,BinVal);
	    }
	  }
	  LocalImage->SetName(Form("HLocalImage%d,%d,%d",j,i,Index));
	  double HistNorm1=1;
	  double HistNorm2=1;
	  LocalProject = LocalImage->ProjectionY(Form("HProjectPoint%d,%d,%d",j,i,Index));

	  //Fit to 2 Erro-functions plus a background
	  TF1* FitFunc = new TF1("FitFunc","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	  HistNorm1=LocalProject->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	  HistNorm2=LocalProject->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	  FitFunc->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	  FitFunc->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	  FitFunc->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	  FitFunc->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	  FitFunc->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	  LocalProject->Fit(FitFunc,"","",-FitRange,FitRange);
	  HEdges[i][j].PointList[Index].x=GridX;
	  HEdges[i][j].PointList[Index].y=GridY+(FitFunc->GetParameter(1)+FitFunc->GetParameter(4))/2.0;
	}

	//
	cout << "Horizontal "<<i<<" "<<j<<" "<<Index<<endl;
	LocalImage->Write();
	LocalProject->Write();
      }
    }
  }      

  //Vertical
  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY-1;j++){
      if (GridArray[i][j].status!=1 || GridArray[i][j+1].status!=1){
	VEdges[i][j].status=0;
	continue;
      }
      //
      VEdges[i][j].status=1;
      VEdges[i][j].PointList[0].x=GridArray[i][j].x;
      VEdges[i][j].PointList[0].y=GridArray[i][j].y;
      VEdges[i][j].PointList[NPoint-1].x=GridArray[i][j+1].x;
      VEdges[i][j].PointList[NPoint-1].y=GridArray[i][j+1].y;
      PointSeparation=(GridArray[i][j+1].y-GridArray[i][j].y)/double(NPoint-1);
      for (int Index=1;Index<=NPoint-2;Index++){
	for (int r=0;r<NRep;r++){
	  double GridX;
	  double GridY;
	  if (r==0)GridX = GridArray[i][j].x;
	  else GridX = VEdges[i][j].PointList[Index].x;
	  GridY = GridArray[i][j].y+PointSeparation*Index;
	  for (int ii=1;ii<=BinN;ii++){
	    for (int jj=1;jj<=BinN;jj++){
	      int IndexX = ((GridX-BinRange+50.0)*100.0+ii);
	      int IndexY = ((GridY-BinRange+50.0)*100.0+jj);
	      if (IndexX<=0 || IndexY<=0)continue;
	      if (IndexX>10000 || IndexY>10000)continue;
	      double BinVal = GridSearch->GetBinContent(IndexX,IndexY);
	      LocalImage->SetBinContent(ii,jj,BinVal);
	    }
	  }
	  LocalImage->SetName(Form("VLocalImage%d,%d,%d",i,j,Index));
	  double HistNorm1=1;
	  double HistNorm2=1;
	  LocalProject = LocalImage->ProjectionX(Form("VProjectPoint%d,%d,%d",i,j,Index));

	  //Fit to 2 Erro-functions plus a background
	  TF1* FitFunc = new TF1("FitFunc","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
	  HistNorm1=LocalProject->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
	  HistNorm2=LocalProject->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
	  FitFunc->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
	  FitFunc->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
	  FitFunc->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
	  FitFunc->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
	  FitFunc->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
	  LocalProject->Fit(FitFunc,"","",-FitRange,FitRange);
	  VEdges[i][j].PointList[Index].x=GridX+(FitFunc->GetParameter(1)+FitFunc->GetParameter(4))/2.0;
	  VEdges[i][j].PointList[Index].y=GridY;
	}

	//
	cout << "Vertical "<<i<<" "<<j<<" "<<Index<<endl;
	LocalImage->Write();
	LocalProject->Write();
      }
    }
  }      
  cout <<"Edge-finding complete!"<<endl;
  testfile->Close();
}

/***********************************************************************************/
void LocalFit(TH2* GridSearch,TH1* LocalProject,double GridX,double GridY,string Direction,double& LeftRes,double& RightRes,double& Width)
{
  //Fine tune the grid
  double BinRange = 0.8;
  double BinWidth = 0.01;
  int BinN = int(BinRange*2/BinWidth);
  //Parameters
  double EdgeLimHigh = 0.25;
  double EdgeLimLow = 0.05;
  double HistNormLim = 0.4;
  double FitRange = 0.6;
  double NormVeto = 0.2;
  double BkgLim = 0.5;
  double WidthLim = 0.1;

  TH2* LocalImage = new TH2D("LocalImage","LocalImage",BinN,-BinRange,BinRange,BinN,-BinRange,BinRange);
  TH1* Project;

  for (int ii=1;ii<=BinN;ii++){
    for (int jj=1;jj<=BinN;jj++){
      int IndexX = ((GridX-BinRange+50.0)*100.0+ii);
      int IndexY = ((GridY-BinRange+50.0)*100.0+jj);
      if (IndexX<=0 || IndexY<=0)continue;
      if (IndexX>10000 || IndexY>10000)continue;
      double BinVal = GridSearch->GetBinContent(IndexX,IndexY);
      LocalImage->SetBinContent(ii,jj,BinVal);
    }
  }
  double HistNorm1=1;
  double HistNorm2=1;
  if (Direction.compare("H")==0){
    Project = LocalImage->ProjectionY(Form("ProjectionY"));
  }else if (Direction.compare("V")==0){
    Project = LocalImage->ProjectionX(Form("ProjectionX"));
  }
    //Fit to 2 Erro-functions plus a background
  TF1* FitFunc = new TF1("FitFunc","[0]*ROOT::Math::erfc((x-[1])/(sqrt(2)*[2]))+[3]*(ROOT::Math::erf((x-[4])/(sqrt(2)*[5]))+1.0)+[6]",-BinRange,BinRange);
  HistNorm1=Project->Integral(1,int(BinN/2*(1-NormVeto)))/(BinN/2*(1-NormVeto));
  HistNorm2=Project->Integral(int(BinN/2*(1+NormVeto)),BinN)/(BinN/2*(1-NormVeto));
  FitFunc->SetParameters(HistNorm1/2,-0.125,0.1,HistNorm2/2,0.125,0.1,0.0);
  FitFunc->SetParLimits(1,-EdgeLimHigh,-EdgeLimLow);
  FitFunc->SetParLimits(4,EdgeLimLow,EdgeLimHigh);
  FitFunc->SetParLimits(0,(1-HistNormLim)*HistNorm1/2,(1+HistNormLim)*HistNorm1/2);
  FitFunc->SetParLimits(3,(1-HistNormLim)*HistNorm2/2,(1+HistNormLim)*HistNorm2/2);
  FitFunc->SetParLimits(6,0.0,(HistNorm1+HistNorm2)/2.0*BkgLim);
  FitFunc->SetParLimits(2,0.0,WidthLim);
  FitFunc->SetParLimits(5,0.0,WidthLim);
  for (int i=0;i<BinN;i++){
    LocalProject->Fill(Project->GetBinCenter(i),Project->GetBinContent(i));
  }
  LocalProject->Fit(FitFunc,"","",-FitRange,FitRange);

  LeftRes = FitFunc->GetParameter(2);
  RightRes = FitFunc->GetParameter(5);
  Width = FitFunc->GetParameter(4)-FitFunc->GetParameter(1);
  delete LocalImage;
}

/***********************************************************************************/
int FindSquare(GridSquare** SquareArray,int sizeX,int sizeY,double Xpos,double Ypos,int& IndexX, int& IndexY, int Npoints)
{
  //sizeX and sizeY are sizes of the square array
  double startX=-2.0*(sizeX+1)+2.0;
  double startY=-2.0*(sizeY+1)+2.0;
  int i;
  int j;
  i = int((Xpos-startX)/4.0);
  j = int((Ypos-startY)/4.0);
  if (Xpos<startX)i=-1;
  if (Ypos<startY)j=-1;
  int result = 0;

  if (i<0 || j<0 || i>=sizeX || j>=sizeY){
    result = 0;
  }else{
    if (SquareArray[i][j].status!=1){
      result = 0;
    }else{
      if (TestInSquare(SquareArray,i,j,Xpos,Ypos,startX,startY,Npoints)){
	IndexX=i;
	IndexY=j;
	result = 1;
      }else{
	result = 0;
      }
    }
  }
  if (result==1){
    return 1;
  }else{
    //Loop around neighbors
    int CorrectAnswerCount = 0;
    for (int ii=-1;ii<=1;ii++){
      for (int jj=-1;jj<=1;jj++){
	if (ii==0 && jj==0)continue;//skip the original point
	int I = i+ii;
	int J = j+jj;
	if (I<0 || J<0 || I>=sizeX || J>=sizeY){
	  continue;
	}else{
	  if (SquareArray[I][J].status!=1){
	    continue;
	  }else{
	    if (TestInSquare(SquareArray,I,J,Xpos,Ypos,startX,startY,Npoints)){
	      IndexX=I;
	      IndexY=J;
	      result = 1;
	      CorrectAnswerCount++;
	      //cout << ii<<" "<<jj<<" "<<CorrectAnswerCount<<endl;
	    }else{
	      continue;
	    }
	  }
	}
      }
    }
    if (CorrectAnswerCount>1){
      cout << "Error: one point in "<<CorrectAnswerCount<<" squares"<<endl;
      result=0;
    }
    if (result==1){
      return 1;
    }
  }
  return 0;
}

bool TestInSquare(GridSquare** SquareArray,int i,int j,double Xpos, double Ypos,double startX,double startY,int Npoints)
{
  bool within_left = Xpos>SquareArray[i][j].X_in_left;
  bool within_right = Xpos<SquareArray[i][j].X_in_right; 
  bool within_bottom = Ypos>SquareArray[i][j].Y_in_bottom;
  bool within_top = Ypos<SquareArray[i][j].Y_in_top;
  bool beyond_left = Xpos<SquareArray[i][j].X_out_left;
  bool beyond_right = Xpos>SquareArray[i][j].X_out_right; 
  bool beyond_bottom = Ypos<SquareArray[i][j].Y_out_bottom;
  bool beyond_top = Ypos>SquareArray[i][j].Y_out_top;
  if (within_left && within_right && within_bottom && within_top){
    //If the position is well within the boundary, return true
    return true;
  }else if (beyond_left || beyond_right || beyond_bottom || beyond_top){
    return false;
  }else{
    double XCenter = startX+4.0*i+2.0;
    double YCenter = startY+4.0*j+2.0;
    //Position of the two test points
    double X1,X2,Y1,Y2;
    //One violation
    if (!within_left && within_right && within_bottom && within_top){
      double X0 = SquareArray[i][j].Corners[0].x-XCenter;
      double Y0 = SquareArray[i][j].Corners[0].y-YCenter;
      double XP = Xpos-XCenter;
      double YP = Ypos-YCenter;
      double CosTheta0 = X0*XP+Y0*YP;

      int n=1;
      for (n=1;n<=Npoints-2;n++){
	double X = SquareArray[i][j].Edges[2].PointList[n].x-XCenter;
	double Y = SquareArray[i][j].Edges[2].PointList[n].y-YCenter;
	double CosTheta = X0*X+Y0*Y;
	if (CosTheta<CosTheta0)break;
      }
      if (n==1){
	X1 = SquareArray[i][j].Corners[0].x;
	Y1 = SquareArray[i][j].Corners[0].y;
	X2 = SquareArray[i][j].Edges[2].PointList[n].x;
	Y2 = SquareArray[i][j].Edges[2].PointList[n].y;
      }else if(n==Npoints-1){
	X1 = SquareArray[i][j].Edges[2].PointList[n-1].x;
	Y1 = SquareArray[i][j].Edges[2].PointList[n-1].y;
	X2 = SquareArray[i][j].Corners[1].x;
	Y2 = SquareArray[i][j].Corners[1].y;
      }else{
	X1 = SquareArray[i][j].Edges[2].PointList[n-1].x;
	Y1 = SquareArray[i][j].Edges[2].PointList[n-1].y;
	X2 = SquareArray[i][j].Edges[2].PointList[n].x;
	Y2 = SquareArray[i][j].Edges[2].PointList[n].y;
      }
      return TestSameSide(Xpos,Ypos,XCenter,YCenter,X1,Y1,X2,Y2);
    }
    if (within_left && !within_right && within_bottom && within_top){
      double X0 = SquareArray[i][j].Corners[2].x-XCenter;
      double Y0 = SquareArray[i][j].Corners[2].y-YCenter;
      double XP = Xpos-XCenter;
      double YP = Ypos-YCenter;
      double CosTheta0 = X0*XP+Y0*YP;

      int n=1;
      for (n=1;n<=Npoints-2;n++){
	double X = SquareArray[i][j].Edges[3].PointList[n].x-XCenter;
	double Y = SquareArray[i][j].Edges[3].PointList[n].y-YCenter;
	double CosTheta = X0*X+Y0*Y;
	if (CosTheta<CosTheta0)break;
      }
      if (n==1){
	X1 = SquareArray[i][j].Corners[2].x;
	Y1 = SquareArray[i][j].Corners[2].y;
	X2 = SquareArray[i][j].Edges[3].PointList[n].x;
	Y2 = SquareArray[i][j].Edges[3].PointList[n].y;
      }else if(n==Npoints-1){
	X1 = SquareArray[i][j].Edges[3].PointList[n-1].x;
	Y1 = SquareArray[i][j].Edges[3].PointList[n-1].y;
	X2 = SquareArray[i][j].Corners[3].x;
	Y2 = SquareArray[i][j].Corners[3].y;
      }else{
	X1 = SquareArray[i][j].Edges[3].PointList[n-1].x;
	Y1 = SquareArray[i][j].Edges[3].PointList[n-1].y;
	X2 = SquareArray[i][j].Edges[3].PointList[n].x;
	Y2 = SquareArray[i][j].Edges[3].PointList[n].y;
      }
      return TestSameSide(Xpos,Ypos,XCenter,YCenter,X1,Y1,X2,Y2);
    }
    if (within_left && within_right && !within_bottom && within_top){
      double X0 = SquareArray[i][j].Corners[0].x-XCenter;
      double Y0 = SquareArray[i][j].Corners[0].y-YCenter;
      double XP = Xpos-XCenter;
      double YP = Ypos-YCenter;
      double CosTheta0 = X0*XP+Y0*YP;

      int n=1;
      for (n=1;n<=Npoints-2;n++){
	double X = SquareArray[i][j].Edges[0].PointList[n].x-XCenter;
	double Y = SquareArray[i][j].Edges[0].PointList[n].y-YCenter;
	double CosTheta = X0*X+Y0*Y;
	if (CosTheta<CosTheta0)break;
      }
      if (n==1){
	X1 = SquareArray[i][j].Corners[0].x;
	Y1 = SquareArray[i][j].Corners[0].y;
	X2 = SquareArray[i][j].Edges[0].PointList[n].x;
	Y2 = SquareArray[i][j].Edges[0].PointList[n].y;
      }else if(n==Npoints-1){
	X1 = SquareArray[i][j].Edges[0].PointList[n-1].x;
	Y1 = SquareArray[i][j].Edges[0].PointList[n-1].y;
	X2 = SquareArray[i][j].Corners[2].x;
	Y2 = SquareArray[i][j].Corners[2].y;
      }else{
	X1 = SquareArray[i][j].Edges[0].PointList[n-1].x;
	Y1 = SquareArray[i][j].Edges[0].PointList[n-1].y;
	X2 = SquareArray[i][j].Edges[0].PointList[n].x;
	Y2 = SquareArray[i][j].Edges[0].PointList[n].y;
      }
      return TestSameSide(Xpos,Ypos,XCenter,YCenter,X1,Y1,X2,Y2);
    }
    if (within_left && within_right && within_bottom && !within_top){
      double X0 = SquareArray[i][j].Corners[1].x-XCenter;
      double Y0 = SquareArray[i][j].Corners[1].y-YCenter;
      double XP = Xpos-XCenter;
      double YP = Ypos-YCenter;
      double CosTheta0 = X0*XP+Y0*YP;

      int n=1;
      for (n=1;n<=Npoints-2;n++){
	double X = SquareArray[i][j].Edges[1].PointList[n].x-XCenter;
	double Y = SquareArray[i][j].Edges[1].PointList[n].y-YCenter;
	double CosTheta = X0*X+Y0*Y;
	if (CosTheta<CosTheta0)break;
      }
      if (n==1){
	X1 = SquareArray[i][j].Corners[1].x;
	Y1 = SquareArray[i][j].Corners[1].y;
	X2 = SquareArray[i][j].Edges[1].PointList[n].x;
	Y2 = SquareArray[i][j].Edges[1].PointList[n].y;
      }else if(n==Npoints-1){
	X1 = SquareArray[i][j].Edges[1].PointList[n-1].x;
	Y1 = SquareArray[i][j].Edges[1].PointList[n-1].y;
	X2 = SquareArray[i][j].Corners[3].x;
	Y2 = SquareArray[i][j].Corners[3].y;
      }else{
	X1 = SquareArray[i][j].Edges[1].PointList[n-1].x;
	Y1 = SquareArray[i][j].Edges[1].PointList[n-1].y;
	X2 = SquareArray[i][j].Edges[1].PointList[n].x;
	Y2 = SquareArray[i][j].Edges[1].PointList[n].y;
      }
      return TestSameSide(Xpos,Ypos,XCenter,YCenter,X1,Y1,X2,Y2);
    }
    //Two violations
    if (!within_left && within_right && !within_bottom && within_top){
      X1 = SquareArray[i][j].Corners[0].x;
      Y1 = SquareArray[i][j].Corners[0].y;
      X2 = SquareArray[i][j].Edges[2].PointList[1].x;
      Y2 = SquareArray[i][j].Edges[2].PointList[1].y;
      bool test1 = TestSameSide(Xpos,Ypos,XCenter,YCenter,X1,Y1,X2,Y2);
      X2 = SquareArray[i][j].Edges[0].PointList[1].x;
      Y2 = SquareArray[i][j].Edges[0].PointList[1].y;
      bool test2 = TestSameSide(Xpos,Ypos,XCenter,YCenter,X1,Y1,X2,Y2);
      return (test1 && test2);
    }
    if (!within_left && within_right && within_bottom && !within_top){
      X1 = SquareArray[i][j].Corners[1].x;
      Y1 = SquareArray[i][j].Corners[1].y;
      X2 = SquareArray[i][j].Edges[2].PointList[Npoints-2].x;
      Y2 = SquareArray[i][j].Edges[2].PointList[Npoints-2].y;
      bool test1 = TestSameSide(Xpos,Ypos,XCenter,YCenter,X1,Y1,X2,Y2);
      X2 = SquareArray[i][j].Edges[1].PointList[1].x;
      Y2 = SquareArray[i][j].Edges[1].PointList[1].y;
      bool test2 = TestSameSide(Xpos,Ypos,XCenter,YCenter,X1,Y1,X2,Y2);
      return (test1 && test2);
    }
    if (within_left && !within_right && !within_bottom && within_top){
      X1 = SquareArray[i][j].Corners[2].x;
      Y1 = SquareArray[i][j].Corners[2].y;
      X2 = SquareArray[i][j].Edges[3].PointList[1].x;
      Y2 = SquareArray[i][j].Edges[3].PointList[1].y;
      bool test1 = TestSameSide(Xpos,Ypos,XCenter,YCenter,X1,Y1,X2,Y2);
      X2 = SquareArray[i][j].Edges[0].PointList[Npoints-2].x;
      Y2 = SquareArray[i][j].Edges[0].PointList[Npoints-2].y;
      bool test2 = TestSameSide(Xpos,Ypos,XCenter,YCenter,X1,Y1,X2,Y2);
      return (test1 && test2);
    }
    if (within_left && !within_right && within_bottom && !within_top){
      X1 = SquareArray[i][j].Corners[3].x;
      Y1 = SquareArray[i][j].Corners[3].y;
      X2 = SquareArray[i][j].Edges[3].PointList[Npoints-2].x;
      Y2 = SquareArray[i][j].Edges[3].PointList[Npoints-2].y;
      bool test1 = TestSameSide(Xpos,Ypos,XCenter,YCenter,X1,Y1,X2,Y2);
      X2 = SquareArray[i][j].Edges[1].PointList[Npoints-2].x;
      Y2 = SquareArray[i][j].Edges[1].PointList[Npoints-2].y;
      bool test2 = TestSameSide(Xpos,Ypos,XCenter,YCenter,X1,Y1,X2,Y2);
      return (test1 && test2);
    }
  }
  cout <<"Cannot find the right situation of this position.Check the algorithm!"<<endl;
  return false;
}

bool TestSameSide(double Xpos,double Ypos,double XCenter,double YCenter,double X1,double Y1,double X2,double Y2){
  double FC = (YCenter-Y1)*(X2-X1)-(Y2-Y1)*(XCenter-X1);
  double FP = (Ypos-Y1)*(X2-X1)-(Y2-Y1)*(Xpos-X1);
  if (FC*FP>0)return true;
  return false;
}


/***********************************************************************************/
//Fit to ellipse
TVectorD fit_ellipse(TGraph *g)
{
  TVectorD ellipse;

  if (!g) return ellipse; // just a precaution
  if (g->GetN() < 6) return ellipse; // just a precaution

  Int_t i;
  Double_t tmp;

  Int_t N = g->GetN();
  Double_t xmin, xmax, ymin, ymax, X0, Y0;
  g->ComputeRange(xmin, ymin, xmax, ymax);
  X0 = (xmax + xmin) / 2.0;
  Y0 = (ymax + ymin) / 2.0;

  TMatrixD D1(N, 3); // quadratic part of the design matrix
  TMatrixD D2(N, 3); // linear part of the design matrix

  for (i = 0; i < N; i++) {
    Double_t x = (g->GetX())[i] - X0;
    Double_t y = (g->GetY())[i] - Y0;
    D1[i][0] = x * x;
    D1[i][1] = x * y;
    D1[i][2] = y * y;
    D2[i][0] = x;
    D2[i][1] = y;
    D2[i][2] = 1.0;
  }

  // quadratic part of the scatter matrix
  TMatrixD S1(TMatrixD::kAtA, D1);
  // combined part of the scatter matrix
  TMatrixD S2(D1, TMatrixD::kTransposeMult, D2);
  // linear part of the scatter matrix
  TMatrixD S3(TMatrixD::kAtA, D2);
  S3.Invert(&tmp); S3 *= -1.0;
  if (tmp == 0.0) {
    std::cout << "fit_ellipse : linear part of the scatter matrix is singular!" << std::endl;
    return ellipse;
  }
  // for getting a2 from a1
  TMatrixD T(S3, TMatrixD::kMultTranspose, S2);
  // reduced scatter matrix
  TMatrixD M(S2, TMatrixD::kMult, T); M += S1;
  // premultiply by inv(C1)
  for (i = 0; i < 3; i++) {
    tmp = M[0][i] / 2.0;
    M[0][i] = M[2][i] / 2.0;
    M[2][i] = tmp;
    M[1][i] *= -1.0;
  }
  // solve eigensystem
  TMatrixDEigen eig(M); // note: eigenvectors are not normalized
  const TMatrixD &evec = eig.GetEigenVectors();
  // const TVectorD &eval = eig.GetEigenValuesRe();
  if ((eig.GetEigenValuesIm()).Norm2Sqr() != 0.0) {
    std::cout << "fit_ellipse : eigenvalues have nonzero imaginary parts!" << std::endl;
    return ellipse;
  }
  // evaluate a’Ca (in order to find the eigenvector for min. pos. eigenvalue)
  for (i = 0; i < 3; i++) {
    tmp = 4.0 * evec[0][i] * evec[2][i] - evec[1][i] * evec[1][i];
    if (tmp > 0.0) break;
  }
  if (i > 2) {
    std::cout << "fit_ellipse : no min. pos. eigenvalue found!" << std::endl;
    // i = 2;
    return ellipse;
  }
  // eigenvector for min. pos. eigenvalue
  TVectorD a1(TMatrixDColumn_const(evec, i));
  tmp = a1.Norm2Sqr();
  if (tmp > 0.0) {
    a1 *= 1.0 / std::sqrt(tmp); // normalize this eigenvector
  } else {
    std::cout << "fit_ellipse : eigenvector for min. pos. eigenvalue is NULL!" << std::endl;
    return ellipse;
  }
  TVectorD a2(T * a1);

  // ellipse coefficients
  ellipse.ResizeTo(8);
  ellipse[0] = X0; // "X0"
  ellipse[1] = Y0; // "Y0"
  ellipse[2] = a1[0]; // "A"
  ellipse[3] = a1[1]; // "B"
  ellipse[4] = a1[2]; // "C"
  ellipse[5] = a2[0]; // "D"
  ellipse[6] = a2[1]; // "E"
  ellipse[7] = a2[2]; // "F"

  return ellipse;
}

TVectorD ConicToParametric(const TVectorD &conic)
{
  TVectorD ellipse;

  if (conic.GetNrows() != 8) {
    std::cout << "ConicToParametric : improper input vector length!" << std::endl;
    return ellipse;
  }

  Double_t a, b, theta;
  Double_t x0 = conic[0]; // = X0
  Double_t y0 = conic[1]; // = Y0

  // http://mathworld.wolfram.com/Ellipse.html
  Double_t A = conic[2];
  Double_t B = conic[3] / 2.0;
  Double_t C = conic[4];
  Double_t D = conic[5] / 2.0;
  Double_t F = conic[6] / 2.0;
  Double_t G = conic[7];

  Double_t J = B * B - A * C;
  Double_t Delta = A * F * F + C * D * D + J * G - 2.0 * B * D * F;
  Double_t I = - (A + C);

  // http://mathworld.wolfram.com/QuadraticCurve.html
  if (!( (Delta != 0.0) && (J < 0.0) && (I != 0.0) && (Delta / I < 0.0) )) {
    std::cout << "ConicToParametric : ellipse (real) specific constraints not met!" << std::endl;
    return ellipse;
  }

  x0 += (C * D - B * F) / J;
  y0 += (A * F - B * D) / J;

  Double_t tmp = std::sqrt((A - C) * (A - C) + 4.0 * B * B);
  a = std::sqrt(2.0 * Delta / J / (I + tmp));
  b = std::sqrt(2.0 * Delta / J / (I - tmp));

  theta = 0.0;
  if (B != 0.0) {
    tmp = (A - C) / 2.0 / B;
    theta = -45.0 * (std::atan(tmp) / TMath::PiOver2());
    if (tmp < 0.0) { theta -= 45.0; } else { theta += 45.0; }
    if (A > C) theta += 90.0;
  } else if (A > C) theta = 90.0;

  // try to keep "a" > "b"
  if (a < b) { tmp = a; a = b; b = tmp; theta -= 90.0; }
  // try to keep "theta" = -45 ... 135 degrees
  if (theta < -45.0) theta += 180.0;
  if (theta > 135.0) theta -= 180.0;

  // ellipse coefficients
  ellipse.ResizeTo(5);
  ellipse[0] = x0; // ellipse's "x" center
  ellipse[1] = y0; // ellipse's "y" center
  ellipse[2] = a; // ellipse's "semimajor" axis along "x"
  ellipse[3] = b; // ellipse's "semiminor" axis along "y"
  ellipse[4] = theta; // ellipse's axes rotation angle (in degrees)

  return ellipse;
}

/***********************************************************************************/
//Efficiency related functions
int Calibration::CalibrateMCPEff_Simple(string ExpFileName, string SimFileName, int CalID){
  TH2* ExpMCP_Image;
  TH2* SimMCP_Image;
  TH2* h_MCP_Eff_Map = new TH2D("MCP_Eff","MCP Efficiency Map",50,-50,50,50,-50,50);
  TH2* hh;
  double ExpCounts[50][50];
  double SimCounts[50][50];
  double MCPEffMap[50][50];
  string filename;
  for (int i=0;i<50;i++){
    for (int j=0;j<50;j++){
      ExpCounts[i][j]=0;
      SimCounts[i][j]=0;
      MCPEffMap[i][j]=0;
    }
  }

  //Reading Exp MCP Image
  filename = CurrentAnalyzer->HISTOGRAM_DIRECTORY + "/CombHistograms_";	//Only read combined histograms
  filename += ExpFileName;
  filename += ".root";
  TCanvas *mydummycanvas=new TCanvas();

  TFile *InFile1 = new TFile(filename.c_str(),"READ");
  if (InFile1->IsZombie()){
    cout<<"Fail when opening file " << filename.c_str()<<"!\n";
    return -1;
  }
  gROOT->cd();
  hh = (TH2*)InFile1->Get("CombExpMCP_Image_Cond0");
  ExpMCP_Image = (TH2*)hh->Clone();
  InFile1->Close();

  //Reading Sim MCP Image
  filename = CurrentAnalyzer->HISTOGRAM_DIRECTORY + "/SimHistograms_";	//Only read combined histograms
  filename += SimFileName;
  filename += ".root";
  TCanvas *mydummycanvas2=new TCanvas();

  TFile *InFile2 = new TFile(filename.c_str(),"READ");
  if (InFile2->IsZombie()){
    cout<<"Fail when opening file " << filename.c_str()<<"!\n";
    return -1;
  }
  gROOT->cd();
  hh = (TH2*)InFile2->Get("MCP_Image_Cond0,0");
  SimMCP_Image = (TH2*)hh->Clone();
  InFile2->Close();

  //Processing
  //Load histograms to array
  int ExpNX = ExpMCP_Image->GetNbinsX();
  int ExpNY = ExpMCP_Image->GetNbinsY();
  for (int i=1;i<=ExpNX;i++){
    for (int j=1;j<=ExpNY;j++){
      double Val = ExpMCP_Image->GetBinContent(i,j);
      int XIndex = int((ExpMCP_Image->GetXaxis()->GetBinCenter(i)+50.0)/2.0);
      int YIndex = int((ExpMCP_Image->GetYaxis()->GetBinCenter(j)+50.0)/2.0);
      if (XIndex>=0 && XIndex<50 && YIndex>=0 && YIndex<50){
	ExpCounts[XIndex][YIndex]+=Val;
      }
    }
  }

  int SimNX = SimMCP_Image->GetNbinsX();
  int SimNY = SimMCP_Image->GetNbinsY();
  for (int i=1;i<=SimNX;i++){
    for (int j=1;j<=SimNY;j++){
      double Val = SimMCP_Image->GetBinContent(i,j);
      int XIndex = int((SimMCP_Image->GetXaxis()->GetBinCenter(i)+50)/2.0);
      int YIndex = int((SimMCP_Image->GetYaxis()->GetBinCenter(j)+50)/2.0);
      if (XIndex>=0 && XIndex<50 && YIndex>=0 && YIndex<50){
	SimCounts[XIndex][YIndex]+=Val;
      }
    }
  }

  double max  = 0.0;
  for (int i=0;i<50;i++){
    for (int j=0;j<50;j++){
      if (SimCounts[i][j]!=0){
	MCPEffMap[i][j] = ExpCounts[i][j]/SimCounts[i][j];
      }else{
	MCPEffMap[i][j] = 0;
      }
      double x = -50.0+i*2.0;
      double y = -50.0+j*2.0;
      if (MCPEffMap[i][j]>max && (x*x+y*y<36*36))max=MCPEffMap[i][j]; //Prevent anomalies near the edge
    }
  }

  //Normalize
  for (int i=0;i<50;i++){
    for (int j=0;j<50;j++){
      MCPEffMap[i][j]/=max;
      if (MCPEffMap[i][j]>1)MCPEffMap[i][j]=1;  //Prevent anomalies near the edge
      double x = -50.0+i*2.0;
      double y = -50.0+j*2.0;
      h_MCP_Eff_Map->Fill(x,y,MCPEffMap[i][j]);
    }
  }

  //Output
  ostringstream convert;
  convert << setfill('0') << setw(2)<<CalID;

  filename = CurrentAnalyzer->HISTOGRAM_DIRECTORY + "/CalHistograms_MCPEfficiency";	
  filename += convert.str();
  filename += ".root";

  TFile *OutFile = new TFile(filename.c_str(),"RECREATE");
  h_MCP_Eff_Map->Write();
  ExpMCP_Image->Write();
  SimMCP_Image->Write();
  OutFile->Close();

  filename = CurrentAnalyzer->CALIBRATION_DIRECTORY + "/MCPEfficiencyMap";
  filename += convert.str();
  filename += ".txt";
  
  ofstream outfile;
  outfile.open(filename.c_str());
  for (int i=0;i<50;i++){
    for (int j=0;j<50;j++){
      outfile<<MCPEffMap[i][j]<<" ";
    }
    outfile<<endl;
  }
  outfile.close();

  return 0;
}

int Calibration::CalibrateMCPEff(string FilePrefix,int FileID,int CalID,string SimFileName){
  //CurrentAnalyzer->Calibrating = true;
  string spectrumfile;

  ExpDataStruct ExpData;      //Data struct for exp data reading
  RecDataStruct RecData;      //Reconstructed data struct
  TFile* tfilein = NULL; //pointer to input root file
  TTree * The_Tree = NULL; //input tree pointer
  CurrentAnalyzer->ConstructExpParameterList();
  CurrentAnalyzer->CheckExpConditionSettings();

  double SquareEdge = 1.92;
  double SquareEdgeIn = 1.875;

  if ((CurrentAnalyzer->MCPPosCalMode).compare("Local")!=0){
    cout << "Need Local calibration mode."<<endl;
    return -1;
  }

  int sizeX = CurrentAnalyzer->MCPsizeX;
  int sizeY = CurrentAnalyzer->MCPsizeY;
  double startX = -2.0*(sizeX+1)+2.0;
  double startY = -2.0*(sizeY+1)+2.0;

  //MCPThreshold
  double MCPThreshold = 3000.0;
  //double CellCounts[19][19];//Counts in each cell
  TH1*** CellHist;//MCP Charge spectrum of each cell
  TH1*** CellHistBkg;//MCP Charge spectrum of each cell,background
  TH1*** CellHistBkgSub;//MCP Charge spectrum of each cell,background subtracted
  TF1*** CellFitFunc;
  CellHist = new TH1**[sizeX];
  CellHistBkg = new TH1**[sizeX];
  CellHistBkgSub = new TH1**[sizeX];
  CellFitFunc = new TF1**[sizeX];
  TH2* RegionCount = new TH2D("RegionCount","RegionCount",17,-34.0,34.0,17,-34.0,34.0);
  TH2* RegionCountBkg = new TH2D("RegionCountBkg","RegionCountBkg",17,-34.0,34.0,17,-34.0,34.0);
  TH2* RegionCountBkgUniform = new TH2D("RegionCountBkgUniform","RegionCountBkgUniform",17,-34.0,34.0,17,-34.0,34.0);
  TH2* RegionCountBkgSub = new TH2D("RegionCountBkgSub","RegionCountBkgSub",17,-34.0,34.0,17,-34.0,34.0);
  TH2* hImage = new TH2D("hImage","MCPImage",1600,-40.0,40.0,1600,-40.0,40.0);
  TH2* hImageShadow = new TH2D("hImageShadow","MCPImageShadow",1600,-40.0,40.0,1600,-40.0,40.0);
  TH2* hImageLowRes = new TH2D("hImageLowRes","MCPImageLowRes",400,-40.0,40.0,400,-40.0,40.0);
  TH2* hImageLowRes2 = new TH2D("hImageLowRes2","MCPImageLowRes2",800,-40.0,40.0,800,-40.0,40.0);
  TH2* h_MCPBrightness = new TH2D("h_MCPBrightness","MCPBrightness",400,-40,40,400,-40,40);

  TH2* hImageIdeal = new TH2D("ImageIdeal","ImageIdeal",17,-34.0,34.0,17,-34.0,34.0);

  TH2* SimMCPImage;
  TH2* SimMapBase = new TH2D("SimMapBase","SimMapBase",17,-34.0,34.0,17,-34.0,34.0);
  TH2* MCPEfficiency = new TH2D("MCPEfficiency","MCP Efficiency",17,-34.0,34.0,17,-34.0,34.0);
  TH2* MCPEfficiencyOriginal = new TH2D("MCPEfficiencyOriginal","MCP Efficiency",17,-34.0,34.0,17,-34.0,34.0);
  TH1* EffStat = new TH1D("EffStat","EffStat",240,50,110);
  TH1* EffStatOriginal = new TH1D("EffStatOriginal","EffStat",240,50,110);

  for (int i=0;i<sizeX;i++){
    CellHist[i] = new TH1*[sizeY];
    CellHistBkg[i] = new TH1*[sizeY];
    CellHistBkgSub[i] = new TH1*[sizeY];
    CellFitFunc[i] = new TF1*[sizeY];
    for (int j=0;j<sizeY;j++){
  //    CellCounts[i][j]=0;
      CellHist[i][j] = new TH1D(Form("CellHist%d,%d",i,j),Form("QMCP of Cell %d,%d",i,j),500,0,300000);
      CellHistBkg[i][j] = new TH1D(Form("CellHistBkg%d,%d",i,j),Form("QMCP of Cell %d,%d",i,j),500,0,300000);
      CellHistBkgSub[i][j] = new TH1D(Form("CellHistBkgSub%d,%d",i,j),Form("QMCP of Cell %d,%d",i,j),500,0,300000);
      CellFitFunc[i][j] = new TF1("FitFunc","[0]*ROOT::Math::erfc((x-[1]+[3]*[2]*[2])/(sqrt(2)*[2]))*exp([3]/2.0*(2.0*x-2.0*[1]+[3]*[2]*[2]))",0,120000);
      CellFitFunc[i][j]->SetParameters(1.0,1.0,0.1,0.0001);
//      CellFitFunc[i][j] = new TF1("FitFunc","[0]*ROOT::Math::erfc((x-[1]+[3]*[2]*[2])/(sqrt(2)*[2]))*exp([3]/2.0*(2.0*x-2.0*[1]+[3]*[2]*[2]))+[4]*exp(-x/[5])",0,120000);
//      CellFitFunc[i][j]->SetParameters(1.0,1.0,0.1,0.0001,1.0,100000);
    }
  }
  TH1* h_TOF = new TH1D("h_TOF","TOF",3000,-500,1000);

  TGraph2D* GMean = new TGraph2D();
  GMean->SetName("GMean");
  GMean->SetTitle("GMean");

  TGraph2D* GRms = new TGraph2D();
  GRms->SetName("GRms");
  GRms->SetTitle("GRms");

  TGraph2D* GPeak = new TGraph2D();
  GPeak->SetName("GPeak");
  GPeak->SetTitle("GPeak");

  TGraph2D* GWidth = new TGraph2D();
  GWidth->SetName("GWidth");
  GWidth->SetTitle("GWidth");

  TGraph2D* GEff1 = new TGraph2D();
  GEff1->SetName("GEff1");
  GEff1->SetTitle("GEff1");

  TGraph2D* GEff2 = new TGraph2D();
  GEff2->SetName("GEff2");
  GEff2->SetTitle("GEff2");

  TGraph* DeadPoints = new TGraph();
  DeadPoints->SetName("DeadPoints");
  DeadPoints->SetTitle("DeadPoints");
  int NDeadPoint = 0;
  double DeadPointDiscrimination=0.2;

  TGraph* DeadPointsClustered = new TGraph();
  DeadPointsClustered->SetName("DeadPointsClustered");
  DeadPointsClustered->SetTitle("DeadPointsClustered");

  //Load Simulation MCP Map first
  TH2 * h2_temp;
  TString SimName = CurrentAnalyzer->HISTOGRAM_DIRECTORY + string("/SimHistograms_") + SimFileName +".root";
  TFile* SimFile = new TFile(SimName.Data(),"read");
  gROOT->cd();
  h2_temp = (TH2*)SimFile->Get("MCP_Image_Cond0,0");
  SimMCPImage = (TH2*)h2_temp->Clone();
  SimFile->Close();

  int SimNx = SimMCPImage->GetNbinsX();
  int SimNy = SimMCPImage->GetNbinsY();
  for (int i=1;i<=SimNx;i++){
    for (int j=1;j<=SimNy;j++){
      double x = SimMCPImage->GetXaxis()->GetBinCenter(i);
      double y = SimMCPImage->GetYaxis()->GetBinCenter(j);
      double z = SimMCPImage->GetBinContent(i,j);
      SimMapBase->Fill(x,y,z);
    }
  }
  //Load Exp data
  //Read In Data
  if(CurrentAnalyzer->ReadMode.compare("MCP")!=0 && CurrentAnalyzer->ReadMode.compare("Double")!=0 && CurrentAnalyzer->ReadMode.compare("Triple")!=0 && CurrentAnalyzer->ReadMode.compare("He4")!=0){
      cout <<"Only MCP Triple Double He4 modes are allowed."<<endl;
    return -1;
  }
  
  ostringstream convert;
  convert << setfill('0') << setw(4)<<FileID;
  string fName = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/") + FilePrefix + string("_") + convert.str();
  fName += ".root";
  if(CurrentAnalyzer->OpenExpInputFile(fName.c_str(),tfilein)==-1) return -1;
  CurrentAnalyzer->SetExpInTreeAddress(tfilein,The_Tree,ExpData);
  cout << "Reading file "<<fName<<endl;
  int N_MCP = The_Tree->GetEntries();
  
  int XIndex,YIndex;
  for(int i=0;i<N_MCP;i++){
    CurrentAnalyzer->ClearTempData(ExpData,RecData);
    //Read Tree
    The_Tree->GetEntry(i);
    CurrentAnalyzer->Reconstruct(0,0,RecData,ExpData);
    //apply T1+T2 conditions
    bool cond;
    cond = RecData.MCP_TXSum>CurrentAnalyzer->TXSumLow && RecData.MCP_TXSum<CurrentAnalyzer->TXSumHigh && RecData.MCP_TYSum>CurrentAnalyzer->TYSumLow && RecData.MCP_TYSum<CurrentAnalyzer->TYSumHigh;
    if (!cond)continue;
    bool TOFcond;
    TOFcond = RecData.TOF>CurrentAnalyzer->TOFLow && RecData.TOF<CurrentAnalyzer->TOFHigh;
    if (CurrentAnalyzer->ReadMode.compare("Triple")==0 || CurrentAnalyzer->ReadMode.compare("Double")==0 || CurrentAnalyzer->ReadMode.compare("He4")==0){
      if (!TOFcond)continue;
    }
    h_TOF->Fill(RecData.TOF);
//    if (CurrentAnalyzer->KillEvent)continue;

    XIndex = int (RecData.MCPPos_X-startX)/4.0;
    YIndex = int (RecData.MCPPos_Y-startY)/4.0;
    
    if (XIndex<0 || XIndex>=sizeX || YIndex<0 || YIndex>=sizeY)continue;
    if (CurrentAnalyzer->MCPSquareArray[XIndex][YIndex].status!=1)continue;
    //CellCounts[XIndex][YIndex]+=1.0;
    CellHist[XIndex][YIndex]->Fill(RecData.QMCP);
    //CellHist[XIndex][YIndex]->Fill(ExpData.QMCP_anodes.QX1+ExpData.QMCP_anodes.QX2);
    RegionCount->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
    hImage->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
    hImageLowRes->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
    hImageLowRes2->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
    h_MCPBrightness->Fill(RecData.MCPPos_X,RecData.MCPPos_Y,ExpData.QMCP);
    double X_Center = startX+XIndex*4.0+2.0;
    double Y_Center = startY+YIndex*4.0+2.0;
    if (RecData.MCPPos_X-X_Center<-SquareEdge || RecData.MCPPos_X-X_Center>SquareEdge || RecData.MCPPos_Y-Y_Center<-SquareEdge || RecData.MCPPos_Y-Y_Center>SquareEdge){
      hImageShadow->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
      RegionCountBkg->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
      CellHistBkg[XIndex][YIndex]->Fill(RecData.QMCP);
    }
    if (RecData.MCPPos_X-X_Center>-SquareEdgeIn && RecData.MCPPos_X-X_Center<SquareEdgeIn && RecData.MCPPos_Y-Y_Center>-SquareEdgeIn && RecData.MCPPos_Y-Y_Center<SquareEdgeIn){
      RegionCountBkgSub->Fill(RecData.MCPPos_X,RecData.MCPPos_Y);
      CellHistBkgSub[XIndex][YIndex]->Fill(RecData.QMCP);
    }
  }
  //f->Close();
  //Close input file
  if (tfilein!=NULL){
    tfilein->Close();
    delete tfilein;
    tfilein =NULL;
  }

  //Find Deadpoints
  int NOpen=0;
  int NCover=0;
  int AverageOpen=0;
  int AverageCover=0;
  double ImageBinInterval = 80.0/800.0;
  for (int i=1;i<=800;i++){
    for (int j=1;j<=800;j++){
      double Imagex=-40.0-ImageBinInterval/2.0+i*ImageBinInterval;
      double Imagey=-40.0-ImageBinInterval/2.0+j*ImageBinInterval;
      int IndexX = int((Imagex+34.0)/4.0);
      int IndexY = int((Imagey+34.0)/4.0);
      if (IndexX<0 || IndexX>=sizeX || IndexY<0 || IndexY>=sizeY)continue;
      if (CurrentAnalyzer->MCPSquareArray[IndexX][IndexY].status!=1)continue;
      double X_Center = startX+IndexX*4.0+2.0;
      double Y_Center = startY+IndexY*4.0+2.0;
      if (Imagex-X_Center<-1.875 || Imagex-X_Center>1.875 || Imagey-Y_Center<-1.875 || Imagey-Y_Center>1.875){
	NCover++;
	AverageCover+=hImageLowRes2->GetBinContent(i,j);
      }else{
	NOpen++;
	AverageOpen+=hImageLowRes2->GetBinContent(i,j);
      }
    }
  }
  AverageCover/=double(NCover);
  AverageOpen/=double(NOpen);

  cout<<"Average count per bin in open area "<<AverageOpen<<endl;
  cout<<"Average count per bin in covered area "<<AverageCover<<endl;

  for (int i=1;i<=800;i++){
    for (int j=1;j<=800;j++){
      double Imagex=-40.0-ImageBinInterval/2.0+i*ImageBinInterval;
      double Imagey=-40.0-ImageBinInterval/2.0+j*ImageBinInterval;
      int IndexX = int((Imagex+34.0)/4.0);
      int IndexY = int((Imagey+34.0)/4.0);
      if (IndexX<0 || IndexX>=sizeX || IndexY<0 || IndexY>=sizeY)continue;
      if (CurrentAnalyzer->MCPSquareArray[IndexX][IndexY].status!=1)continue;
      double X_Center = startX+IndexX*4.0+2.0;
      double Y_Center = startY+IndexY*4.0+2.0;
      if (Imagex-X_Center>-1.875 && Imagex-X_Center<1.875 && Imagey-Y_Center>-1.875 && Imagey-Y_Center<1.875){
	if (hImageLowRes2->GetBinContent(i,j)<AverageCover+DeadPointDiscrimination*(AverageOpen-AverageCover)){
	  DeadPoints->SetPoint(NDeadPoint,Imagex,Imagey);
	  NDeadPoint++;
	}
      }
    }
  }
  //Clusterize the dead points
  double RDead = 0.5;
  double* SortArray = new double[NDeadPoint];
  int NCluster = 0;
  bool Found=false;
  for (int i=0;i<NDeadPoint;i++){
    SortArray[i] = 0.0;
  }
  for (int i=0;i<NDeadPoint;i++){
    double X,Y;
    DeadPoints->GetPoint(i,X,Y);
    Found=false;
    for (int j=0;j<NCluster;j++){
      double X0,Y0;
      DeadPointsClustered->GetPoint(j,X0,Y0);
      if (pow(X-X0,2.0)+pow(Y-Y0,2.0)<RDead*RDead){
	double XNew = (X0*SortArray[j]+X)/(SortArray[j]+1.0);
	double YNew = (Y0*SortArray[j]+Y)/(SortArray[j]+1.0);
	SortArray[j]+=1.0;
	Found=true;
      }
    }
    if (Found)continue;
    DeadPointsClustered->SetPoint(NCluster,X,Y);
    SortArray[NCluster]+=1.0;
    NCluster++;
  }
  cout <<"Found dead points: "<<NCluster<<endl;

  //Solid angle histogram
  TF2* SolidAngleFunc = new TF2("FitFunc","[0]*(atan((x-[1]+1.875)*(y-[2]+1.875)/([3]*sqrt(pow(x-[1]+1.875,2.0)+pow(y-[2]+1.875,2.0)+pow([3],2.0))))-atan((x-[1]+1.875)*(y-[2]-1.875)/([3]*sqrt(pow(x-[1]+1.875,2.0)+pow(y-[2]-1.875,2.0)+pow([3],2.0))))-atan((x-[1]-1.875)*(y-[2]+1.875)/([3]*sqrt(pow(x-[1]-1.875,2.0)+pow(y-[2]+1.875,2.0)+pow([3],2.0))))+atan((x-[1]-1.875)*(y-[2]-1.875)/([3]*sqrt(pow(x-[1]-1.875,2.0)+pow(y-[2]-1.875,2.0)+pow([3],2.0)))))",-40,40,-40,40);
  SolidAngleFunc->SetParameters(100,0.0,0.0,92.0);
  int NActiveSquare = 0;
  for (int i=0;i<17;i++){
    for (int j=0;j<17;j++){
      if (CurrentAnalyzer->MCPSquareArray[i][j].status!=1)continue;
      NActiveSquare++;
      double X_Center = startX+i*4.0+2.0;
      double Y_Center = startY+j*4.0+2.0;
      hImageIdeal->Fill(X_Center,Y_Center,SolidAngleFunc->Eval(X_Center,Y_Center));
    }
  }

  //Construct mean and rms graphs and Bkg subtracted histograms
  //Fit cell hists one by one
  double BkgIntegral = RegionCountBkg->Integral();
  double BkgAverage = BkgIntegral/double(NActiveSquare);
  for (int i=0;i<17;i++){
    for (int j=0;j<17;j++){
      if (CurrentAnalyzer->MCPSquareArray[i][j].status!=1)continue;
      double X_Center = startX+i*4.0+2.0;
      double Y_Center = startY+j*4.0+2.0;
      RegionCountBkgUniform->Fill(X_Center,Y_Center,BkgAverage);
    }
  }
  double BkgRatio = pow(SquareEdgeIn*2.0,2.0)/(pow(4.0,2.0)-pow(SquareEdge*2.0,2.0));
//  RegionCountBkgSub->Add(RegionCountBkg,-BkgRatio);
  RegionCountBkgSub->Add(RegionCountBkgUniform,-BkgRatio*0.7);
  int NGraph=0;
  double MaxPeak=0;
  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      if (CurrentAnalyzer->MCPSquareArray[i][j].status!=1)continue;
      //Bkg subtraction
      CellHistBkgSub[i][j]->Add(CellHistBkg[i][j],-BkgRatio);
      //Fit to function
/*      double mean = CellHist[i][j]->GetMean();
      double rms = CellHist[i][j]->GetRMS();
      double max = CellHist[i][j]->GetMaximum();
      CellFitFunc[i][j]->SetParameters(max,mean,rms,0.0001,max*0.1,100000);
      CellFitFunc[i][j]->FixParameter(4,max*0.1);
      CellHist[i][j]->Fit(CellFitFunc[i][j],"","",0,mean+3.0*rms);
      double peak = CellFitFunc[i][j]->GetParameter(1);
      double width = CellFitFunc[i][j]->GetParameter(2);
      */
      
      double mean = CellHist[i][j]->GetMean();
      double rms = CellHist[i][j]->GetRMS();
      double max = CellHist[i][j]->GetMaximum();
      CellFitFunc[i][j]->SetParameters(max,mean,rms,0.0001);
      CellHist[i][j]->Fit(CellFitFunc[i][j],"","",0,mean+3.0*rms);
      double peak = CellFitFunc[i][j]->GetParameter(1);
      double width = CellFitFunc[i][j]->GetParameter(2);
      double AboveThreshRatio =CellFitFunc[i][j]->Integral(MCPThreshold,120000.0)/CellFitFunc[i][j]->Integral(0.0,120000.0);
      if (peak>MaxPeak)MaxPeak=peak;
      
      //mean and rms
      GMean->SetPoint(NGraph,startX+4.0*i+2.0,startY+4.0*j+2.0,mean);
      GRms->SetPoint(NGraph,startX+4.0*i+2.0,startY+4.0*j+2.0,rms/mean); //rms/mean
      GPeak->SetPoint(NGraph,startX+4.0*i+2.0,startY+4.0*j+2.0,peak);
      GWidth->SetPoint(NGraph,startX+4.0*i+2.0,startY+4.0*j+2.0,width); 
      GEff1->SetPoint(NGraph,startX+4.0*i+2.0,startY+4.0*j+2.0,AboveThreshRatio);
      NGraph++;
    }
  }
  //Find efficiency in the second way
  NGraph=0;
  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      if (CurrentAnalyzer->MCPSquareArray[i][j].status!=1)continue;
      double GainRatio = CellFitFunc[i][j]->GetParameter(1)/MaxPeak;
      int ThreshBin = CellHist[i][j]->FindBin(MCPThreshold);
      double NearThreshCount = CellHist[i][j]->Integral(ThreshBin,3*ThreshBin);
      double ThresholdLoss = NearThreshCount/(2*MCPThreshold)*(1-GainRatio);
      double Eff2 = (CellHist[i][j]->Integral()-ThresholdLoss)/CellHist[i][j]->Integral();
      GEff2->SetPoint(NGraph,startX+4.0*i+2.0,startY+4.0*j+2.0,Eff2);
      NGraph++;
    }
  }

  //Compare to simulation
  for (int i=1;i<=17;i++){
    for (int j=1;j<=17;j++){
      double VExp = RegionCount->GetBinContent(i,j);
      double VSim = SimMapBase->GetBinContent(i,j);
      double Val = 0.0;
      if (VSim>0.0)Val = VExp/VSim;
      MCPEfficiencyOriginal->SetBinContent(i,j,Val);
//      EffStatOriginal->Fill(Val*100);
    }
  }
  double MaxOriginal = MCPEfficiencyOriginal->GetMaximum();
  MCPEfficiencyOriginal->Scale(1.0/MaxOriginal);
  for (int i=1;i<=17;i++){
    for (int j=1;j<=17;j++){
      double Val = MCPEfficiencyOriginal->GetBinContent(i,j);
      EffStatOriginal->Fill(Val*100);
    }
  }
  for (int i=1;i<=17;i++){
    for (int j=1;j<=17;j++){
      double VExp = RegionCountBkgSub->GetBinContent(i,j);
      double VSim = SimMapBase->GetBinContent(i,j);
      double Val = 0.0;
      if (VSim>0.0)Val = VExp/VSim;
      MCPEfficiency->SetBinContent(i,j,Val);
//      EffStat->Fill(Val*100);
    }
  }
  double Max = MCPEfficiency->GetMaximum();
  MCPEfficiency->Scale(1.0/Max);
  for (int i=1;i<=17;i++){
    for (int j=1;j<=17;j++){
      double Val = MCPEfficiency->GetBinContent(i,j);
      EffStat->Fill(Val*100);
    }
  }

  //Write to calibration file
  ostringstream Calconvert;
  Calconvert << setfill('0') << setw(2)<<CalID;

  string calfilename = CurrentAnalyzer->CALIBRATION_DIRECTORY + "/MCPEfficiencyMap";
  calfilename += Calconvert.str();
  calfilename += ".txt";

  ofstream outfile;
  outfile.open(calfilename.c_str());
  for (int i=1;i<=17;i++){
    for (int j=1;j<=17;j++){
      outfile<<MCPEfficiency->GetBinContent(i,j)<<" ";
    }
    outfile<<endl;
  }
  outfile.close();
  cout <<"Calibration data is written to file "<< calfilename<<endl;

  //Write to spect file
  TCanvas* CellCanvas = new TCanvas("EffMap","EffMap",0,0,19000,19000);
  CellCanvas->Divide(sizeX,sizeY);
  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      CellCanvas->cd((sizeY-1-j)*sizeX+i+1);
      CellHist[i][j]->Draw();
    }
  }
  string SpecFile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Spectra/") + FilePrefix + string("_") + convert.str() + string("_MCPEffMap.pdf(");
  CellCanvas->SaveAs(SpecFile.c_str());

  TCanvas* CellBkgSubCanvas = new TCanvas("EffMapBkgSub","EffMap BkgSub",0,0,19000,19000);
  CellBkgSubCanvas->Divide(sizeX,sizeY);
  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      CellBkgSubCanvas->cd((sizeY-1-j)*sizeX+i+1);
      CellHistBkgSub[i][j]->Draw();
    }
  }
  SpecFile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Spectra/") + FilePrefix + string("_") + convert.str() + string("_MCPEffMap.pdf)");
  CellBkgSubCanvas->SaveAs(SpecFile.c_str());

  //Write to root file
  spectrumfile = CurrentAnalyzer->EXP_DATA_DIRECTORY + string("/../Histograms/") + FilePrefix + string("_") + convert.str() + string("_MCPEffCal.root");
  TFile * histfile = new TFile(spectrumfile.c_str(),"recreate");
  h_TOF->Write();
  RegionCount->Write();
  RegionCountBkg->Write();
  RegionCountBkgSub->Write();
  hImage->Write();
  hImageLowRes->Write();
  hImageLowRes2->Write();
  hImageShadow->Write();
  GMean->Write();
  GRms->Write();
  GPeak->Write();
  GWidth->Write();
  GEff1->Write();
  GEff2->Write();
  SimMCPImage->Write();
  SimMapBase->Write();
  MCPEfficiency->Write();
  MCPEfficiencyOriginal->Write();
  EffStat->Write();
  EffStatOriginal->Write();
  DeadPoints->Write();
  DeadPointsClustered->Write();
  hImageIdeal->Write();
  h_MCPBrightness->Write();

  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      CellHist[i][j]->Write();
    }
  }
  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      CellHistBkg[i][j]->Write();
    }
  }
  for (int i=0;i<sizeX;i++){
    for (int j=0;j<sizeY;j++){
      CellHistBkgSub[i][j]->Write();
    }
  }
  histfile->Close();
  //Close input file
  if (tfilein!=NULL){
    tfilein->Close();
    delete tfilein;
    tfilein =NULL;
  }
//  CurrentAnalyzer->Calibrating = false;
  return 0;
}

double Gauss2DBkg(double *x,double* par){
  double X=x[0]-par[1];
  double Y=x[1]-par[2];
  return par[0]*exp(-(X*X*(pow(cos(par[5])/par[3],2.0)/2.0+pow(sin(par[5])/par[4],2.0)/2.0)+Y*Y*(pow(sin(par[5])/par[3],2.0)/2.0+pow(cos(par[5])/par[4],2.0)/2.0)+X*Y*cos(par[5])*sin(par[5])*(1.0/pow(par[4],2.0)-1.0/pow(par[3],2.0))))+par[6];
}

