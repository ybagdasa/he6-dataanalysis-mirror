{
  TFile *InFilePlus = new TFile("HistogramsPlus.root","READ");
  TH1 *TOFPlus= (TH1D*)InFilePlus->Get("TOFHist");

  TFile *InFileMinus = new TFile("HistogramsMinus.root","READ");
  TH1 *TOFMinus= (TH1D*)InFileMinus->Get("TOFHist");


  TCanvas *canv = new TCanvas("canv","TOF",0,0,800,600);
  TOFPlus->SetLineColor(kRed);
  TOFPlus->Draw();
  TOFMinus->Draw("same");

  canv->SaveAs("TOFCompare.pdf");

}
