#ifndef DATASTRUCT_H
#define DATASTRUCT_H

#include "TF1.h"
#include "TF2.h"
 
using namespace std;

typedef struct DataStruct_MWPC_X{
  double AX1;
  double AX2;
  double AX3;
  double AX4;
  double AX5;
  double AX6;
}DataStruct_MWPC_X;

typedef struct DataStruct_MWPC_Y{
  double AY1;
  double AY2;
  double AY3;
  double AY4;
  double AY5;
  double AY6;
}DataStruct_MWPC_Y;

typedef struct DataStruct_TMWPC_X{
  double TX1;
  double TX2;
  double TX3;
  double TX4;
  double TX5;
  double TX6;
}DataStruct_TMWPC_X;

typedef struct DataStruct_TMWPC_Y{
  double TY1;
  double TY2;
  double TY3;
  double TY4;
  double TY5;
  double TY6;
}DataStruct_TMWPC_Y;

typedef struct DataStruct_QMCP_anodes{
  double QX1;
  double QX2;
  double QY1;
  double QY2;
}DataStruct_QMCP_anodes;

typedef struct DataStruct_TMCP_anodes{
  double TX1;
  double TX2;
  double TY1;
  double TY2;
}DataStruct_TMCP_anodes;

typedef struct InDataStruct{
  unsigned int ScintEnergy;     //from PostProcessor//in eV, only 3 bytes will be written
  unsigned short  MWPCEnergy;   //from PostProcessor//in eV
  unsigned int ScintEnergyBB;     //from BetaTracker//in eV, only 3 bytes will be written
  unsigned short  MWPCEnergyBB;   //from BetaTracker//in eV
  short MWPCHitPos[2];          //in um
  unsigned short MWPCAnodeSignal1;  //any unit
  unsigned short MWPCAnodeSignal2;  //any unit
  unsigned short MWPCCathodeSignal[12]; //any unit
//  unsigned int MWPCCathodeSignal[12]; //any unit
  short PosMWPCEntrance[3];	//in um,z component 10um
  short PosMWPCExit[3];		//in um,z component 10um
  unsigned short ExitAngle_Be;           //1/100 deg
  int InitP[3]; //Initial momentum
  short DECPos[3];	//Initial decay position  
  int V_Ion[3];			//in [10^-5 cm/us]
  uint16_t Hits;                //Hits bitmap: 0 MCP Wall Ele Col Win MWPC Sci
  uint8_t ParticleID;   //0:electron 1:photon
  unsigned int TOF;             //in ps, only 3 bytes will be written
  int HitPos[2];
  unsigned int TOFII;   //from IonTracker          //in ps, only 3 bytes will be written
  int HitPosII[2]; //from IonTracker
  unsigned short HitAngle;
  unsigned short IonEnergy; //in eV
  unsigned short EIonInit; //non-rel ion intitial kinetic energy [keV]
  uint8_t IonStatus;
  uint8_t ChargeState;

  
}InDataStruct;

typedef struct InDataStructDouble{
  double ScintEnergy;     //from PostProcessor in keV
  double MWPCEnergy;   //from PostProcessor in keV
  double ScintEnergyBB;   //from BetaTracker in keV
  double MWPCEnergyBB;   //from BetaTracker keV
  double MWPCHitPos[2];          //in mm
  double MWPCAnodeSignal1;  //any unit
  double MWPCAnodeSignal2;  //any unit
 // double MWPCCathodeSignal[12]; //any unit
  double PosMWPCEntrance[3];	//in mm
  double PosMWPCExit[3];		//in mm
  DataStruct_MWPC_X MWPC_X;	//MWPC cathode X ADCs
  DataStruct_MWPC_Y MWPC_Y;	//MWPC cathode Y ADCs
  double ExitAngle_Be;           // deg
  double InitP[3]; //Initial momentum
  double DECPos[3];	//Initial decay position
  double V_Ion[3]; //mm/ns
  double TOF;             //in ns
  double HitPos[2];//in mm
  double TOFII; //from IonTracker in ns
  double HitPosII[2];//from IonTracker in mm
  double HitAngle;
  double IonEnergy; //in keV
  double EIonInit; //non-rel ion intitial kinetic energy [keV]
}InDataStructDouble;

typedef struct SimRecDataStruct{
  double ScintEnergy;     //in keV
  double MWPCEnergy;   //in keV
  double MWPCCathodeSignalX;  //any unit
  double MWPCCathodeSignalY;  //any unit
  int FiredWireX;
  int FiredWireY;
  double MWPCHitPos[2];          //in mm
  double MWPC_CY;
  double MWPCHitShift[2];	//Shifts from the reconstructed position to real position
  double COSBetaShiftAngle;	//angle between initial momentum and reconstructed one
  double ExitAngle_Be;           // deg
  double InitP[3]; //Initial momentum
  double DECPos[3];	//Decay positions
  double TOF;             //in ns
  double HitPos[2];//in mm
  double HitAngle;
  double IonEnergy; //ion energy at MCP [keV]
  double EIonInit; //non-rel ion intitial kinetic energy [keV]
  double EIon[3]; //reconstructed non-rel initial kinetic energy of ion [keV] 
  uint16_t Hits;                //Hits bitmap: 0 MCP Wall Ele Col Win MWPC Sci
  uint8_t ParticleID;   //0:electron 1:photon
  uint8_t IonStatus;
  double Q_Value[3];
  double CosThetaENu;
}SimRecDataStruct;

typedef struct HitCount{
  int Scint_Hit;
  int MWPC_Hit;
  int Window_Hit;
  int Collimator_Hit;
  int Electrode_Hit;
  int Wall_Hit;
  int MCP_Hit;
  int BackScattering;
}HitCount;

typedef struct TriggerCount{
  int Scint_Trigger;
  int MWPC_A_Trigger;
  int MWPC_CX_Trigger;
  int MWPC_CY_Trigger;
  int MCP_Trigger;
  int MCP_A1_Trigger;
}TriggerCount;

typedef struct range{
  double low;
  double high;
}range;

typedef struct area{
  double r1;
  double r2;
  double theta1;
  double theta2;
  double shiftx;
  double shifty;
}area;

typedef struct FitResult{
  bool MultiSlice;
  double fit_val[10];
  double fit_error[10];
  double Chi2;
  double Chi2Slice[N_OF_SLICE];
  TF1* FitFunc;
  TF2* Chi2Func;
}FitResult;

typedef struct LinkedPtr{
  vector<double> * array_ptr;
  double * linked_ptr;
}LinkedPtr;

typedef struct HistUnit{
  string Title;
  int Dim;      //TH1 or TH2
  bool MultiSlice;      //Wheter multislice is allowed
  string XTitle;
  string YTitle;
  string ZTitle;
  range XRange;
  range YRange;
  int XBinNum;
  int YBinNum;
  TH1*** Hist1DList;
  TH2*** Hist2DList;
  TH1**** Hist1DListSliced;
  bool Enabled;
  //  int dim1;
  //  int dim2;
}HistUnit;

//Exp Data

typedef struct ExpDataStruct{
  int Event_Num;		//Event number
  double GroupTime;		//Group time
  double QScint_A[2];		//Scintillator QDC anode
  double QScint_D[2];		//Scintillator QDC dynode
  double TScint_A_rel;		//Scintillator time relative to Tref
  double TScint_DA;		//Scintillator anode dynode time difference
  double AMWPC_anode1;		//MWPC anode1 ADC
  double AMWPC_anode2;		//MWPC anode2 ADC
  double TMWPC_rel[2];		//MWPC time relative to Tref
  double TBeta_diff[2];		//MWPC time relative to Scint
  DataStruct_MWPC_X MWPC_X;	//MWPC cathode X ADCs
  DataStruct_TMWPC_X TMWPC_X;	//MWPC cathode X TDCs
  DataStruct_MWPC_Y MWPC_Y;	//MWPC cathode Y ADCs
  DataStruct_TMWPC_Y TMWPC_Y;	//MWPC cathode Y TDCs
  double QMCP;			//MCP QDC
  DataStruct_QMCP_anodes QMCP_anodes;	//MWPC anode QDCs
  DataStruct_TMCP_anodes TMCP_anodes;	//MWPC anode TDCs
  double TMCP_rel;		//MCP time relative to Tref
  double TOF;			//MCP time relative to Scint
  unsigned short TriggerMap;	//Trigger map
  double LED;
  double LED_T;			//LED time
  //He4Calibration Variables
  double QN2Laser;	//N2 Laser pulse charge on photodiode for event
  double TN2Laser;	//Timing of N2 Laser pulse on photodiode for event
}ExpDataStruct;  

typedef struct RecDataStruct{
  //copied
  int Event_Num;		//Event number
  double GroupTime;		//Group time
  double* Time_rel; //Chosen relative time to Tref for given mode for push beam and laser veto
  double TScint_A_rel;		//Scintillator time relative to Tref
  double TScint_DA;		//Scintillator anode dynode time difference
  double TMWPC_rel[2];		//MWPC time relative to Tref
  double TBeta_diff[2];		//MWPC time relative to Scint
  double LED_T;			//LED time  
  double QN2Laser;	//N2 Laser pulse charge on photodiode for event
  double TN2Laser;	//Timing of N2 Laser pulse on photodiode for event
  //For future use
//  DataStruct_TMWPC_X TMWPC_X;	//MWPC cathode X TDCs
//  DataStruct_TMWPC_Y TMWPC_Y;	//MWPC cathode Y TDCs
//  DataStruct_QMCP_anodes QMCP_anodes;	//MWPC anode QDCs
  DataStruct_QMCP_anodes QMCP_anodes;	//MCP anode QDCs
  DataStruct_TMCP_anodes TMCP_anodes;	//MCP anode TDCs
  double TMCP_rel;		//MCP time relative to Tref
  unsigned short TriggerMap;	//Trigger map
  double QMCP;			//MCP QDC
  double TOF;			//MCP time relative to Scint
  double LED;
  //Calculated
  double EScintA[2];		//Scintillator QDC anode
  double EScintD[2];		//Scintillator QDC dynode
  double EMWPC_anode1;		//MWPC anode1 ADC
  double EMWPC_anode2;		//MWPC anode2 ADC
  double EMWPC_anode;
  double QMWPC_cathode_X;
  double QMWPC_cathode_Y;
  double MWPCPos_X;
  double MWPCPos_Y;
  double MWPCPos_Y_C;	//Cathode reconstruction of Y
  double FiredWireX;
  double FiredWireY;
  double MCP_TXSum;             //TX1 + TX2
  double MCP_TYSum;             //TY1 + TY2
  double MCPPos_X;
  double MCPPos_Y;
  double Q_Value[3];
  double EIon[3];
  double CosThetaENu;
}RecDataStruct;

typedef struct SimParameterStruct{
  double MWPCA_Scale;
  double MWPCC_Scale;
  double MWPCX_Scale;
  double MWPCY_Scale;
  double MWPC_Res;
  double MWPCC_Threshold;
  double MCPEffMap_Scale;
  double E_Field;
  double Z_Pos;
  double TOFShift;
  double TOFBound12;
  double TOFBound23;
}SimParameterStruct;

typedef struct ExpParameterStruct{
  double ScintA_Scale;
  double MWPCA_Scale;
  double MWPCC_Scale;
  double MWPCX_Scale;
  double MWPCY_Scale;
  double MWPCC_Threshold;
  double TOFShift;
  double MCPXShift;
  double MCPYShift;
  double E_Field;
  double Z_Pos;
  double TOFBound12;
  double TOFBound23;
}ExpParameterStruct;

//Structs for MCP calibration
typedef struct GridPoint{
  double x;
  double y;
  int status;
} GridPoint;

typedef struct GridEdge{
  GridPoint PointList[10];      //Maximum 10 points per edge
  int status;
} GridEdge;

typedef struct GridSquare{
  GridPoint Corners[4];
  GridEdge Edges[4];
  GridPoint TrueCorners[4];
  GridEdge TrueEdges[4];
  GridPoint CorrectedCorners[4];
  GridEdge CorrectedEdges[4];
  //Fit Parameters
  double XFitPar[6];
  double YFitPar[6];
  int status;
  //Parameters for Square finding
  double X_in_left;
  double X_in_right;
  double Y_in_bottom;
  double Y_in_top;
  double X_out_left;
  double X_out_right;
  double Y_out_bottom;
  double Y_out_top;
} GridSquare;

typedef struct MWPCCalParStruct{
  double FitParametersA[4];
  double FitParametersX0[4];
  double FitParametersX1[3][4];
  double FitParametersX2[4][4];
} MWPCCalParStruct;

#endif



