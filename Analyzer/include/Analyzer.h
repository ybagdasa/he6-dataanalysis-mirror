#ifndef ANALYZER_H
#define ANALYZER_H

#include <string>
#include <stdint.h>
#include <fstream>
#include <vector>
#include <map>
#include <ctime>
#include <time.h>

#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TF2.h"

#include "TGraphErrors.h"
#include "TGraph2DErrors.h"
#include "TSpline.h"
#include "TTree.h"
#include "Cell.h"

//Define macros


#define N_OF_HIST 32 //Max Number of histograms in each dimension
#define N_OF_SLICE 1 //Max Number of slices, for sliced fit
//#define N_OF_COND 99
//#define N_OF_FILE_SIM 99

#include "DataStruct.h"
using namespace std;


class Analyzer{
private:
  /*********************Run Time Information*****************************/
  time_t beginTime, endTime, midTime, iTime0, iTime1;
  double timeSec,timeMin;

  /*********************Basic Information*****************************/
  int Purpose;		//Purpose 0: simulation ; 1: experiment
  int Purpose2;		//Purpose 0: decay ; 			1: calibration
  string ReadMode;	//Reading mode: Triple, Double, Beta, Scint, MWPC, MCP
  			//ReadMode is applied to both Exp and Sim
  map <string,string> Configurations;
  //Data directory
  string SIM_DATA_DIRECTORY;
  string HISTOGRAM_DIRECTORY;
  string SPECTRA_DIRECTORY;
  string OUTPUT_TREE_DIRECTORY;
  string EXP_DATA_DIRECTORY;
  string ANALYZER_DIRECTORY;
  string CALIBRATION_DIRECTORY;

  /*************************Systematics**********************/
  //External Parameter array for user function studies
  string ExtParName;
  int nExtPar;
  double ExtParList[N_OF_HIST];
  //Systematic Parameter/Condition maps
  map <string,vector<double> > SysWConditions;
  map <string,vector<double> > SysWFiles;
  map <string,double *> AllowedConditions;
  
  //Reconstructed data array
  //Parameter related systematics
  map <string,vector<double> > SysWParameters;//Parameters, different from conditions
  SimParameterStruct SimParameterList[N_OF_HIST][N_OF_HIST];
  map <string,int> ParameterLoopIndex;
  int nParameter1;
  int nParameter2;
  
  //Loop function pointer maps
  map <string,bool (Analyzer::*)(SimRecDataStruct)>NonLoopFunctions;
  map <string,bool (Analyzer::*)(SimRecDataStruct)>LoopFunctions;
  //Condition Loop pointers
  vector <LinkedPtr> CondLoopPtrs1;	//Used in looping conditions
  vector <LinkedPtr> CondLoopPtrs2;
  vector <string> SimForbiddenCondFunctions;
  //No. of Conditions
  int nCond1;
  int nCond2;
  //Condition Array
  bool Conditions[N_OF_HIST][N_OF_HIST];
  //Current Conditions
  double ScintThreshold;	//Scintillator threshold in keV
  double ScintUpperbound;	//Scintillator upper bound in keV
  double ScintMidpoint;		//Scintillator midpoint for 2D cut, in keV
  double MWPCThreshold;	//MWPC threshold in keV
  double MWPC_R;              //MWPC radius, in mm
  double MWPC_RIn;		//inner radius of MWPC
  double MWPC_X;              //MWPC x, in mm
  double MWPC_Y;              //MWPC y, in mm
  double MCP_R;	        //MCP radius, in mm
  double MCP_RIn;	//MCP inner radius
  double MCP_ThetaLow;	//MCP theta low
  double MCP_ThetaHigh;	//MCP theta high
  double MCP_X;	        //MCP x, in mm
  double MCP_Y;	        //MCP y, in mm
  double MCPEff_Scale;	//Scale of MCP Efficiency
  double ScattCut;		//Scattering decisions: 0: keep all, 1: No scattering, 2: Only Scattering 3:No BackScattering 4:Only BackScattering
  double TOFLow;	//TOF lower boundary
  double TOFHigh;	//TOF higher boundary
  double TOFMidpoint;	//TOF midpoint, for 2D cut
  double QValueLow;
  double QValueHigh;
  double ZDecLow;	//Decay Z-position low
  double ZDecHigh;	//Decay Z-position hight
  string  ScattMode;	//Rejct mode ;  Select mode
  double TriggerBlock;	//TriggerBlock
  double LeadingEdgeRange_Low; //Range for leading edge fit - Low, in ns
  double LeadingEdgeRange_High; //Range for leading edge fit - High, in ns
  //Charge state 3 switch
  bool Charge3Switch;
  
  //Exp file-condition correlation flag
  bool FileCondLnkd;
  //Exp T1T2 cut flag
  bool AutoT1T2;

  //TimeShift
  double TimeShift;

  //MCP Efficiency MAP
  int MCPefficiencyMapSwitch; //MCP Efficiency MAP switch: 0: turn off, 1: turn on
  double MCPefficiencyMap[8][8];

  //File Related conditions
  //No. of files
  int nFileSim1;
  int nFileSim2;
  //Simulation File ID list
  int FileIDListSim[N_OF_HIST][N_OF_HIST];

  //Simulation Calibrations
  map <string,bool> SimCalSwitches;	//simulation calibration switches
  map <string,int> SimCalIDs;	//simulation calibration IDs
  //Scint Calibration Parameters
  double SimCalPeaks[6];	//3 sets of value;error
  bool SimScintCalLoaded;
  //MWPC Calibration
  double SimMWPCAnode_Cal;	//scaling
  double SimMWPCCathode_Cal;	//scaling
//  double SimMWPCPosCalPar[12];
  double SimMWPCGainMap[17][18];	//MWPC Gain Map
  double SimMWPCEffMap[15][16];		//MWPC Efficiency Map
  double SimMWPCXCalPar[32];
  double SimMWPCYCalPar[4];
  double SimMWPCACalPar[4];
  //Some Hard coded calibration parameters
  double MWPCRawCalA;
  double MWPCRawCalC;
  double ScintRawCal;


  //Integrated systematic studies
//  string SystematicPurpose;//none :Scint_Threshold :MWPCE_Threshold :MWPCR_Cut :MCPR_Cut :MOT_size: MOTCenterX
  vector<double> * SysDataPtr1;
  vector<double> * SysDataPtr2;
  //For Sliced fit
  vector<double> SliceBoundary;
  int nSlice;

  /*************************Histograms and Fits**********************/
  //Status
  int Status;		//0: not initialized;1: initialized but not loaded; 2: loaded
  int CombStatus;	//0: not initialized;1: initialized
  // Histogram array dimensions
  int HistDim1;
  int HistDim2;
  int CombHistDim;
  // Histogram containers for simulations
  map <string,HistUnit> SimHistBox;
  map <string,HistUnit> FitHistBox;
  //For sliced fit
  map <string,string> SlicedHistList;	//HistName -> SliceConditionName
  //Define Simulation Histograms
  //Make arrays of them for different conditions or files
/*  TH1 *RawScint[N_OF_HIST][N_OF_HIST];  //Raw Scintillator Energy Histogram
  TH1 *RawMWPC[N_OF_HIST][N_OF_HIST];   //Raw MWPC Energy Histogram
  TH1 *CoinScint[N_OF_HIST][N_OF_HIST]; //Scintillator Energy Histogram, coincidence trigger
  TH1 *CoinMWPC[N_OF_HIST][N_OF_HIST];  //MWPC Energy Histogram, coincidence trigger
  TH2 *MWPC_vs_Scint[N_OF_HIST][N_OF_HIST];	//2D correlation plot of MWPC and Scint
  TH2 *MWPCPos[N_OF_HIST][N_OF_HIST];   //MWPC Position Histogram
  TH2 *MCP_Image[N_OF_HIST][N_OF_HIST];	//Image on MCP
  TH1 *TOFHist[N_OF_HIST][N_OF_HIST];   //Time of Flight Histogram
  TH2 *TOF_vs_Scint[N_OF_HIST][N_OF_HIST];    //2D correlation between TOF and Scint Energy
  TH1 *ExitAngleHist[N_OF_HIST][N_OF_HIST]; //Exit angle from Be window histogram
*/
  //Define StdHistograms
  TH1 *Std_First;	//Standard histogram for fitting,first,e.g. a=+1/3
  TH1 *Std_Second;	//Standard histogram for fitting,second,e.g. a=-1/3
  vector<TH1*> Std_First_Slice;	
  vector<TH1*> Std_Second_Slice;

  //Define fitted histograms
//  TH1 *TOF_Fit[N_OF_HIST][N_OF_HIST];  //Fitted histogram of TOF spectrum
//  TH1 *TOF_Res[N_OF_HIST][N_OF_HIST];	 //Residual histogram of TOF spectrum

  //Fitter Type
  string FitType;       
  int floatTime; //1 allows fits to float, 0 does not
  //Fit results
  map <string,FitResult**> FitResBox;
  /******************Output Trees*************************************/
  string OutTreesFilenameSim;
  string OutTreesFilenameExp;
  TFile* OutTreesFile;
  TTree*** OutTrees;  
  bool OutTreesInitialized;
  /******************Graphs and Output Spectra************************/
  map <string , TGraphErrors *> SysGraphBox1D;
  map <string , TGraph2DErrors *> SysGraphBox2D;
  string SpectrumFileName;    //Spectrum file name
  int SpectrumStatus;  //0:not opened 1:opened
  //Drawing Options
  bool GridOn;
  bool TitleOn;
  bool AxisTitleOn;
  string Option2DHist;
  string Option2DGraph;
  double alpha, alphaErr, t0, t0Err; //calibration fit parameters

  /*****************************Logging*******************************/
  int Event_Count[N_OF_HIST][N_OF_HIST];        //Count the good events
  int BkgEvent_Count[N_OF_HIST][N_OF_HIST];        //Count the background events
  TriggerCount Trigger_Count[N_OF_HIST][N_OF_HIST];//Count the triggers
  HitCount Hit_Count[N_OF_HIST][N_OF_HIST];
  ofstream AnalysisLog;	//Log file
 
  /*****************************Experiment****************************/
//  map <string, bool> ModeSwitches;	//On/Off status of each histogram in each mode
  bool BkgSeparation;	//Turn on/off the background separation
  double BkgRatio;	//data/bkg ratio
  double BkgTime;	//After this time, consider data as background data
  double TotalTime;	//Total time for a Foreground/Background cycle
  double BkgNorm;
  //Laser Push beam timing
  bool PushBeamVeto;	//Veto push beam	
  double PushPeriod;	//Push beam period, in ms
  double PushLength;	//Push beam on time length, in ms
  //MOT2 trapping laser status
  bool LaserSwitch;	//MOT2 laser is switching
  bool LaserStatus;
  double SwitchPeriod;	//Period of the switching [ms]
  double SwitchStart;	//Beginning of the switching [ms]
  int N_cycle;		//Number of switch cycles
  double WaitTime;	//Time to wait after each switch 
  
//  ExpDataStruct ExpData;	//Data struct for exp data reading
//  RecDataStruct RecData;	//Reconstructed data struct
  string ExpFilePrefix;	//Prefix for the Exp data file
  //No. of files
  int nFileExp1;
  int nFileExp2;
  //Experiment File ID list
  int FileIDListExp[N_OF_HIST][N_OF_HIST];

  //Systematic Parameter maps
  map <string,vector<double> > ExpParameters;//Parameters, different from conditions
  ExpParameterStruct ExpParameterList[N_OF_HIST][N_OF_HIST];
  map <string,int> ExpParameterLoopIndex;
  int nParameterExp1;
  int nParameterExp2;

  //Exp Conditions
  map <string,vector<double> > ExpConditionListBox;
  map <string,double *> AllowedExpConditions;
  //Loop function pointer maps
  map <string,bool (Analyzer::*)(RecDataStruct& RecData)> ExpNonLoopFunctions;
  map <string,bool (Analyzer::*)(RecDataStruct& RecData)> ExpLoopFunctions;
  vector <string> ExpForbiddenCondFunctions;
  map <string,int (Analyzer::*)(int,int,bool,SimRecDataStruct)> FillFunctions;
  //No. of Conditions
  int nCondExp1;
  int nCondExp2;
  //Exp specific conditions
  double TXSumLow;
  double TXSumHigh;
  double TYSumLow;
  double TYSumHigh;
  double TMWPCLow;
  double TMWPCHigh;
  double GroupTimeLow;
  double GroupTimeHigh;
  double MCPTimeLow;
  double MCPTimeHigh;
  double MCP_QLow;
  double MCP_QHigh;

  //Experiment Histograms
  int ExpStatus;
  map <string,HistUnit> ExpHistBox; //Hist Containers
  map <string,HistUnit> BkgHistBox;
  map <string,HistUnit> CombExpHistBox; //Combined Hist Containers
  map <string,HistUnit> CombBkgHistBox;
  map <string,string> SlicedExpHistList;	//HistName -> SliceConditionName
  map <string,int (Analyzer::*)(int,int,bool,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData)> ExpFillFunctions;
/*  TH1 *Exp_TOFHist[N_OF_HIST];   //Time of Flight Histogram
  TH2 *Exp_TOF_vs_Scint[N_OF_HIST];    //2D correlation between TOF and Scint Energy
  TH1 *Exp_TOF_Fit[N_OF_HIST];  //Fitted histogram of TOF spectrum
  TH1 *Exp_TOF_Res[N_OF_HIST];	 //Residual histogram of TOF spectrum
  */
  //For external bkg subtraction
  map <string,TH1*> ExtBkgHistBox;
//  map <string,TH1*> ExtBkgHistBox2;
  uint8_t ExtBkgStatus;

  //Calibration
  map <string,bool> ExpCalSwitches;	//experiment calibration switches
  map <string,int> ExpCalIDs;	//experiment calibration IDs
  //Scint Calibration Parameters
  double ExpCalPeaks[6];	//3 sets of value;error
  //Linear correction y=ax+b
  double ScintCalScale;	
  double ScintCalShift;
  double ScintGainMap[15][16];	//Scint Gain Map
  //LED gain correction
  double LED_Cal;	//LED calibration
  double LEDScint_Cal;	//LED_Scint Calibration
  bool LEDStablizer;	//LED gain correction switch
  double LEDPeriod;	//LED calibration period, in s
  vector<double> LEDGainCorrection;
  int LEDGainCorrectionSize;
  //MWPC
  string MWPCPosCalMode;
  bool KeepLessTrigger;
  double MWPCAnode_Cal;	//ADC to keV
  double MWPCCathode_Cal;	//ADC to keV
  double MWPCEScale;	//new way to calibrate the MWPC
  double MWPCEShift;
  double MWPCGainMap[17][18];	//MWPC Gain Map
  double MWPCEffMap[15][16];		//MWPC Efficiency Map
  double AmpCalX[6];
  double AmpCalY[6];
  double AmpCalA[2];
/*  double MWPCXCalPar[32];
  double MWPCYCalPar[4];
  double MWPCACalPar[4];
  */
  MWPCCalParStruct MWPCCalPar;
  double MWPCExactPos0[9];
  double MWPCExactPos1[15];
  double MWPCExactPos2[20];
  double MWPCExactPosA[18];
  double MWPCPeaksX0[9];
  double MWPCPeaksX1[15];
  double MWPCPeaksX2[20];
  double MWPCPeaksA[18];

  double MCPEffMap[50][50];
  string MCPPosCalMode;
  double MCPShiftX;
  double MCPShiftY;
  double MCPScaleX;
  double MCPScaleY;
  double XFitPar[6];
  double YFitPar[6];
  GridSquare** MCPSquareArray;
  int MCPNPointEdge;
  int MCPsizeX;
  int MCPsizeY;
  friend class Calibration;

  T0_Cell *t0cell;
  string T0CorrMode;
  TF1* t0vsQMCPpoly1; //correction poly based on QMCP
  //Flags
  bool Calibrating;
  bool KillEvent;
  bool RootInputSim;
  bool RootOutputSim;
  bool RootOutputExp;
  
public:
  /****************Constructor sand destructors**********************/
  Analyzer(char *SimDataDirect,char *ExpDataDirect,char *AnalyzerDirect,char *CalibrationDirect);
  ~Analyzer();
  
  int SetScattMode(string mode);
  /*************************Histograms*******************************/
  int LsHist();		//List all allowed histogram names
  int InitSimHistBox();	//Initialize histogram containers
  int EnableHist(string Name); //Enable hist unit
  int EnableBkgHist(string Name); //Enable backgrond hist unit
  int EnableDisableAll(bool status);
  int EnableDisableAllBkg(bool status);
  int DisableHist(string Name); //Disable hist unit
  int DisableBkgHist(string Name); //Disable background hist unit
  int InitFillFunctions(); //Fill FillFunctions container with function pointers corresponding to enabled hist units
  int InitHistograms();	//Initialize histograms
  void InitToNull(); //Initialize histogram pointers to NULL
  int DelHistograms();	//delete histograms
  int DelStdHistograms();	//delete histograms
  int SaveHistograms(string label);		//Save histograms to a root file
  int LoadHistograms(string label);		//Load histograms from a root file
  int SaveGraphs(string label);			//Save graphs
  int SetStdHist(string HistName,string labelName);	//Set histograms as std-histograms
  int SaveStdHist(string HistName,string label);	//Save std-histograms to a root file
  int LoadStdHist(string HistName,string label);	//Load std-histograms from a root file
  int RenewHistograms();				//Renew histograms
  int FillHistograms(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int ConfigHistogram(string Name,double xlow,double xhigh,int xNBin,double ylow=0,double yhigh=1000,int yNBin=100);
  //fill functions
  int FillRawScint(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillRawMWPC(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillCoinScint(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillCoinMWPC(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillMWPC_Image(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillMWPC_CAY(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillMWPCPosX(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillMWPCPosY(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillMWPC_vs_Scint(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillMCP_Image(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillMCP_R(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillTOFHist(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillTOF_vs_Scint(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillExitAngleHist(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillQ_Values(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillCosThetaENu(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillDECPos(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillEkIon(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillEIon0(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillEIon1(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillEIon2(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillHitAngle(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillMWPCHitShift(int i,int j,bool cond,SimRecDataStruct SimRecData);
  int FillCOSBetaShiftAngle(int i,int j,bool cond,SimRecDataStruct SimRecData);

  int FillSliceHistograms(int i,int j,SimRecDataStruct SimRecData);
  int AddSlicedHist(string HistName,string ConditionName);
  int ClearSlicedHistList();
  /*************************Input/Output Trees*****************************/  
  int LoadOutTrees(const char* fname);
  int InitOutTrees();
  int InitTreesToNull();
  int DelOutTrees();
  int SetOutTreesAddress(InDataStruct &InData, InDataStructDouble &InDataDouble, SimRecDataStruct &SimRecData); //Set branch address of OutTrees for Simulation
  int SetOutTreesAddress(RecDataStruct &ExpRecData); //Set branch address of OutRees for Experiment
  int FillOutTrees(int i,int j,bool cond);
  int WriteOutTrees();	
  int SetInTreeAddress(TTree* InTree, InDataStruct &InData); //Set branch address of InTree and friends to point to members of InData
  int SetExpInTreeAddress(TFile* &tfilein, TTree* &InTree, ExpDataStruct &ExpData);
  int SetupInputFile(int FileID,TFile* &tfilein, TTree* &InTree);
  int OpenExpInputFile(const char* fName, TFile* &tfilein);
  int OutputTreesToBin(vector<string> br,string fname);
  /*************************Processing Inputs************************/
  //int SetRootInputSim (bool Switch);
  int SetRootOutputSim (bool Switch, string fname = "");
  int SetRootOutputExp (bool Switch, string fname = "");
  int ProcessInputSim(int FileDim,int ConditionDim,int ParameterDim);	//Process input file from simulation, File ID list should be initialized
	int ConvertDataToDouble(InDataStruct &InData, InDataStructDouble &InDataDouble); //Convert compressed data from InData struct to doubles and put into InDataDouble struct
	//int ReadInSimEvent(ifstream &filein, InDataStruct &InData); //Read in event from dat file and put into InData struct;
	void ZeroCounters();
	/*************************Calibration************************/
  int CalConfigSim(string name,string status);
  int SetCalIDSim(string name,int val);
  int LoadSimCalibrationData(string name,bool Switch);
  /************************Output Spectra******************************/
  int OpenSpecFile(string SpecFileName);
  int CloseSpecFile();
  int PlotHistogram(string HistName,int i, int j);	//Plot one histogram on a page
  int PlotCombHistogram(string HistName,int i,string Select);	//Plot one combined histogram on a page
  int PlotHistogramList(string HistName,int iStart,int iEnd,int jStart,int jEnd);		//Plot the whole list of histograms
  int OutputSpectraGeneral(int i, int j);		//Output general spectra
//  int MultiOutputSpectra(string HistName,string option);  //Output one set of histograms with different conditions
  int OutputFittedHists(string HistName,int iStart,int iEnd,int jStart,int jEnd);     //Output Fitted histograms
  int ShowTitle(bool Opt);
  int ShowAxisTitle(bool Opt);
  int ShowGrid(bool Opt);
  int SetDrawOption2DHist(string Opt);
  int SetDrawOption2DGraph(string Opt);
  int OutputHistogramToTxt(string HistName, string fname);
  int MakeMovie(string HistName, string fname, int histdim=1, int index=0, string draw_option="");
 
  /************************Systematics********************************/
  
  int LsConditions();	//List all allowed conditions
  int LsEnabledCondFunc(); //List enabled conditions
  int ResetSystematics();
  //Parameter related
  int InitParameters();
  int ResetParameters();
  int InitParameterArray(string Name, int N, double low, double interval);
  int SetParameter(string Name, int N, double val);
  int SetParameterLoop(string Name, int index);
  int UnsetParameterLoop(string Name);
  int UnsetParameterLoops();
  int ConstructParameterList();

  //Condition Related
  int InitCondition(string Name,int N, double low, double interval);
  int SetCondition(string Name,int i,double val);
  int SetDefaultConditions();
  void SetAllowedConditions();
  void InitDefaultSimForbiddenCondFunctions();
  int AddCondLoopPtr(int index,string Name);
  int UnSetCondLoopPtrs();
  int SetCondFuncMap(string Name,bool On, bool Loop=false);
  int InitCondFuncMap();
  int CheckConditionSettings();
  void ProcessConditions(SimRecDataStruct SimRecData, int iFile = 0);
  bool Cond_ScintE(SimRecDataStruct SimRecData);
  bool Cond_MWPCE(SimRecDataStruct SimRecData);
  bool Cond_MWPCArea(SimRecDataStruct SimRecData);
  bool Cond_MCPArea(SimRecDataStruct SimRecData);
  bool Cond_Scatter(SimRecDataStruct SimRecData);
  bool Cond_MCPEff(SimRecDataStruct SimRecData);
  bool Cond_TOFRange(SimRecDataStruct SimRecData);
  bool Cond_TOFScintE2D(SimRecDataStruct SimRecData);
  bool Cond_QValue(SimRecDataStruct SimRecData);
  bool Cond_QValue_C(SimRecDataStruct SimRecData);
  bool Cond_ZDec(SimRecDataStruct SimRecData);
  int LogConditionList();
  //MWPC Reconstruction
  int ReconstructSim(int iPara, int jPara,SimRecDataStruct& SimRecData,InDataStructDouble& InDataDouble,InDataStruct InData);	//Apply calibration and reconstruct position information for simulation
  //Efficiencies
  int SetMCPefficiencyMap(int i){MCPefficiencyMapSwitch = i;return 0;}
  //File Related
  int SetFileSimDim(int dim1,int dim2);
  int SetFileIDEntrySim(int i,int j,int ID1, int ID2, int ID3, int ID4);
  int SetFileIDListSim(int j,int ID1, int ID2, int ID3, int ID4,string Inc);
  int SetFileIDListSim2D(string Inc);
  int InitSysWFile(string Name,int N, double low, double interval);
  int SetSysWFile(string Name, int i, double val);
  int LogSimFileIDList();
  int SetFileCondLnk(bool status);
  int SetCharge3Switch(bool status);
  //For sliced fit
  int InitSliceBoundary(int N,double begin,double interval);	//initialize the slice boundary list
  //Integrated Systematic study
//  void SetSystematicPurpose(string Purp){SystematicPurpose = Purp;}
  int SetSysAxis(int Dim,string Property1, string Name1,string Property2="",string Name2="");
  int InitGraph(int Dim,string GraphName);
  int SystematicFit_TOF(string GraphName, range &FitRange, int Dim, int iLim, int jLim=0);
  int SystematicGraphRate(string GraphName, int Dim, int iLim, int jLim=0); //graph rate from group time
  int SysPlotGraph(int Dim,string GraphName,string Purpose1,string XTitle, string YTitle,string Purpose2="", string ZTitle="");
  int SysFileListInit(int Dim, int BaseID1, int BaseID2, int BaseID3, int BaseID4, int dim1,string Inc1,int dim2=1,string Inc2="");
  int SystematicStudy_Condition(int fileID,range &FitRange,int ConDim,string Purpose1,int N1, double low1, double interval1,string XUnit,string Purpose2="",int N2=1,double low2=0.0, double interval2=0.0,string YUnit=""); 
  int SystematicStudy_Parameter(int fileID,range &FitRange,int ParDim,string Purpose1,int N1, double low1, double interval1,string XUnit,string Purpose2="",int N2=1,double low2=0.0, double interval2=0.0,string YUnit=""); 
  int SystematicStudy_Calibration(range &FitRange,double low1, double interval1, double E0, double E0err, string Purpose1);
  int SystematicStudy_File(int FDim,range &FitRange,string Purpose1,double low1, double interval1,string XUnit,string Purpose2="",double low2=0.0, double interval2=0.0, string YUnit="");
  int SystematicStudy_Mixed(range &FitRange,string PurposeCon,int N1, double low1, double interval1,string XUnit,string PurposeFile,double low2, double interval2, string YUnit);
  int SystematicStudy_Rate(int ConDim, int N_GT, double GT_inc, double GT_lo, double GT_hi,string Purpose1,double low1, double interval1,string XUnit,string Purpose2="",int N2=0,double low2=0.0, double interval2=0.0, string YUnit="");
//  int SystematicFit_Condition(range &FitRange,string option);//For Scint_Cut MWPCE_Cut EField MWPCR_Cut MCPR_Cut
/*  int Systematic_Init_w_File(string Input_Purpose,int StartID,int NOF,double low,double interval); //For MOT_r MOT_c EField ScintResponse MWPCResponse 
  int Systematic_Fit_w_File(range &FitRange,string option);//For MOT_r MOT_c EField ScintResponse MWPCResponse 
  int Systematic_Init_MOTC_shift(int StartID,int NOF,double low,double interval); //For MOT_c 
  int Systematic_Fit_MOTC_shift(range &FitRange,string option);//For MOT_c 
*/  
  /************************Fit********************************/
  //Fitting methods
  int FitConfig(string Type,string AllowTimeFloat,double Time_Shift);
  int InitFit(string FitHistName);
  int Fit_TOF(int i,int j,range &FitRange);
  int Fit_TOF_Gauss(int i,int j,range &FitRange); //Fit TOF spectrum to gaussian. Paramter of interest is mean of the TOF spike.
  int Fit_TOF_Hong(int i,int j,range &FitRange);	//Fit TOF spectrum and extract a allowing/forbid for floating leading edge using Ran Hong's method
//  int Fit_TOF_Noshift(int i,range &FitRange);	//Fit TOF spectrum and extract a
  int Fit_TOF_Minuit(int i,int j,range &FitRange);	//Fit TOF spectrum and extract a using Minuit
  int Fit_TOF_Root(int i,int j,range &FitRange);	//Fit TOF spectrum and extract a using Root
  int Fit_TOF_MultiSlice(int i,int j,range &FitRange);	//Fit TOF spectrum and extract a using multi-slice fit method
  int FitWithEZ(string StdName,int NE,int NZ,range &FitRange,string label); //Fit E field and Z position, and a

  /************************Experiment********************************/
  //Reading control
  int SetMode(string mode);
  void SetExpTime_rel(RecDataStruct& RecData); //sets chooses time for push beam/laser veto according to ReadMode
  int ConfigBackground(string ON,double bkgTime=9000.0,double totTime=12000.0, double ratio=0.1);
  int ConfigPushBeamVeto(string ON,double Period=1000.0/3.0,double Length=30.0);
  bool CheckPushBeamVeto(double Time); //returns true if vetoed
  //int ConfigLaserSwitch(string Option1,string Option2,double PushBeamPeriod=1000.0/3.0,double LaserPeriod=0.1,double Tstart=58.35,int Ncycle=1980);
  int ConfigLaserSwitch(string Option1,double PushBeamPeriod=1000.0/3.0,double LaserPeriod=0.1,int Ncycle=1980,double TWait = 0.0010);
  bool CheckLaserSwitchVeto(double Time); //returns true if vetoed
  int ProcessInputExp(int FileDim, int ConditionDim,int ParameterDim); //Process input file from experiment
  int SetExpFilePrefix(string prefix);
  void ClearTempData(ExpDataStruct& ExpData,RecDataStruct& RecData);
  int ConfigExpHistogram(string Name,double xlow,double xhigh,int xNBin,double ylow=0,double yhigh=1000,int yNBin=100);
  /**************************Exp Conditions************************/
  int LsExpConditions();	//List all allowed conditions
  int LsEnabledExpCondFunc(); //List enabled conditions
  int ResetExpConditions();
  //Condition Related
  //These may be OK to merge with the simulation ones, check the purpose. 
  //For now, I copy and paste the code and make them obvious.
  int InitExpCondition(string Name,int N, double low, double interval);
  int SetExpCondition(string Name,int i,double val);
  int SetDefaultExpConditions();
  void SetAllowedExpConditions();
  void InitDefaultExpForbiddenCondFunctions();
  void FindAndRemoveVector(vector<string> & StrVec,string Name);
  int AddExpCondLoopPtr(int index,string Name);
  int UnSetExpCondLoopPtrs();
  int SetExpCondFuncMap(string Name,bool On, bool Loop=false);
  int InitExpCondFuncMap();
  int CheckExpConditionSettings();
  void ProcessExpConditions(RecDataStruct& RecData, int iFile = 0);
  int LogExpConditionList();
  vector<double> GetExpCondition(string Name); 

  bool ExpCond_ScintE(RecDataStruct& RecData);
  bool ExpCond_MWPCE(RecDataStruct& RecData);
  bool ExpCond_MWPCArea(RecDataStruct& RecData);
  bool ExpCond_MCPArea(RecDataStruct& RecData);
  bool ExpCond_MCPCharge(RecDataStruct& RecData);
  bool ExpCond_MWPCT(RecDataStruct& RecData);
  bool ExpCond_MCPT1T2(RecDataStruct& RecData);
  bool ExpCond_TOFRange(RecDataStruct& RecData);
  bool ExpCond_TOFScintE2D(RecDataStruct& RecData);
  bool ExpCond_QValue(RecDataStruct& RecData);
  bool ExpCond_QValue_C(RecDataStruct& RecData);
  bool ExpCond_TriggerBlock(RecDataStruct& RecData);
  bool ExpCond_GroupTime(RecDataStruct& RecData);
  bool ExpCond_MCPTime(RecDataStruct& RecData);
  bool ExpCond_LaserSwitch(RecDataStruct& RecData);

  //File related
  int SetFileExpDim(int dim1,int dim2);
  int SetFileIDEntryExp(int i,int j,int ID);
  int SetFileIDListExp(int j,int startID,int Inc);
  int LogExpFileIDList();

  //Parameter related
  int InitExpParameters();
  int ResetExpParameters();
  int InitExpParameterArray(string Name, int N, double low, double interval);
  int SetExpParameter(string Name, int N, double val);
  int SetExpParameterLoop(string Name, int index);
  int UnsetExpParameterLoop(string Name);
  int UnsetExpParameterLoops();
  int ConstructExpParameterList();
  
  /**************************ExpHistograms***************/
  int LsExpHist();		//List all allowed exp histogram names
  int InitExpHistBox();	//Initialize histogram containers
  void InitToNullExp(); //Initialize Exphistogram pointers to NULL
  void InitToNullCombExp(); //Initialize Combined Exp histogram pointers to NULL
  int InitExpHistograms();	//Initialize histograms
  int InitCombExpHistograms();	//Initialize Combined histograms
  int DelExpHistograms();	//delete histograms
  int DelCombExpHistograms();	//delete Combined histograms
  int SaveExpHistograms(string label,string Opt);		//Save histograms to a root file
  int LoadExpHistograms(string label,string Opt);		//Load histograms from a root file
  int RenewExpHistograms();				//Renew histograms
  int RenewCombExpHistograms();				//Renew Combined histograms
  int FillExpHistograms(int i,int j,bool cond,double Time,RecDataStruct& RecData); 
  int CombineExpHistograms(int CombineDim,string BkgOpt);	//Combine(accumulate) histograms
  int PutBackCombHistograms();	//Put back the combined histograms to the histogram array, for the convenience of fitting
  //Use the first dimension only
  int PutBackOneCombHistograms(int TargetIndex);	//Put back the first combined histograms to one entry of the histogram array, for the convenience of fitting
  //Fill functions exp
  int InitExpFillFunctions(); //Fill FillFunctions container with function pointers corresponding to enabled hist units
  int FillScint_EA(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillScint_EDA(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillScint_Time(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMWPC_Anode(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillBeta_Time_diff(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMWPC_Cathode_Anode(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMWPC_Cathode_XY(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMWPC_vs_Scint(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMWPC_Image(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMWPC_CAY(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_TX1vQX1(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_TX2vQX2(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_TY1vQY1(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_TY2vQY2(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_TX1pTX2(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_TXSumPos(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_TY1pTY2(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_TYSumPos(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_Image(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_Image_Zoom(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_R(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_Time(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillMCP_Charge(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillTOFHist(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillTOF_vs_Scint(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillTOF_vs_QMCP(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillN2_Time(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillN2_Charge(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  //int FillN2_TOFHist(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillQ_Values(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillCosThetaENu(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillEIon1(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillEIon2(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  int FillGroupTime(int i,int j,bool cond,map <string,HistUnit> * ChosenHistBox,RecDataStruct& RecData);
  //
  int AddSlicedExpHist(string HistName,string ConditionName);
  int ClearSlicedExpHistList();
  int Reconstruct(int iPara,int jPara,RecDataStruct& RecData,ExpDataStruct& ExpData);	//Apply calibration and reconstruct position information

  //External Bkg studies
  int SubtractRandomCoincidence(string Name);
  int ConstructBkgHistogram(string Name);
  int NormalizeBkg(string label);
  int DelBkgHistogram();
  int SaveBkgHistogram(int ID);
  int LoadBkgHistogram(int ID);
  int SetBkgNorm(double Norm);
  int ExpBkgSubtraction(string label);
  int SimBkgMixing(string label);

  //Experimental Set Methodes
  //Fit Experimental Data
//  int Exp_Fit_TOF(int i,range &FitRange);	//Fit TOF spectrum and extract a
  
  //Set methods
  //Switches
  void SetPurpose(int val1, int val2);

  //Calibrations
  int CalConfigExp(string name,string status);
  int SetCalIDExp(string name,int val);
  int LoadExpCalibrationData(string name,bool Switch);
  int GenerateLEDStablizer(int iFile,int jFile);
  int ConfigLEDCalibration(string ON, double Period=300);
  int SetMWPCPosCalMode(string Mode);
  int SetMWPCKeepLessTrigger(int Val);
  int SetExpMCPCalMode(string Mode);
  int SetT0CorrMode(string Model);
  int SetT0CorrStrength(double Val);
  int CleanupCalibrationStructures();
  
  //Initialize external parameter list for user function studies
  int InitExtParList(string Name, int N, double low, double interval);
  int SetExtParList(string Name, int N, double val[]);
  //User Functions
  int User_MatchBetaSpectra(int Dim,string Purpose1,double low1, double interval1,string XUnit,string Purpose2="",double low2=0.0, double interval2=0.0, string YUnit="");
  int User_MCPGainStudy(string ParName,double Xlow, double XInc);
  int User_CompareMCPMap(string label,int CalID);
  int User_MWPCScatStudy(double Interval,string OutLabel);
  int User_CompareSpectra(string ExpHistName, int NExp,string SimHistName, int NSim, bool Normalize, int Style, string label);
  int User_PhotoIonTOFStudy(double Alpha_low, double Alpha_interval, double E0, double E0err,string label);
  int User_QMCPStudy(string PhotoIonInput, string PenningIonInput,string title, string label, double Alpha_low = 0, double Alpha_interval = 1);
  int User_PhotoIonRateStudy(string PhotoIonInput, string PenningIonInput,string title);
  int User_LocalRateTOFStudy(string PhotoIonInput, string PenningIonInput,string title);
  int User_LaserSwitchStartStudy(string Histname, string title, string label, double Alpha_low = 0, double Alpha_interval = 1);
  int User_TOFFit(string title, string label = "Run", double Alpha_low = 0, double Alpha_interval = 1);
  int User_TOFFit_2DCond(string title, string cond1, string cond2);
  int User_Gauss1DFit(string HistName,string title);
  int User_MOT2DFit(string title,int npeaks);//, string label = "Run", double Alpha_low = 0, double Alpha_interval = 1);
  int User_Monitor(string Object,string label);
  int User_MOTEvolution(double TStart,double TStop, int NInterval);
  int User_ChargeState3Ana(double KeptRatio, string label, string opt);
  int User_ChargeState12Ana(string label);
  int User_Monitor_Penning(string label,double PhotoionWidthX,double PhotoionWidthY);
  int User_RawDataStats();
  int User_AD_TOF(string ExpOutTrees, int m_min, int m_max); 
  //Conditions
/*  void SetMWPCThreshold(int i,double threshold){MWPCThreshold[i] = threshold;}	//Set MWPC threshold
  void SetScintThreshold(int i,double threshold){ScintThreshold[i] = threshold;}	//Set scintillator threshold
  void SetMWPCRangeCut(int i,double range){MWPCRangeCut[i] = range;}			//Set MWPC RangeCut
  void SetMCPRangeCut(int i,double range){MCPRangeCut[i] = range;}			//Set MCP RangeCut
  void SetScintEnergyWindow(int i,double val1,double val2);               //Set Scintillator energy window
  void SetMCPArea(int i,double val1,double val2,double val3,double val4,double val5,double val6); //Set MCP area
  void SetScattCut(int i,int val){ScattCut[i] = val;}
  void SetNCondition(int val);
  void SetConditions(int i,double threshold1, double threshold2, double R1, double R2,double win1,double win2,double val1,double val2,double val3,double val4,double val5,double val6, int ScattOpt );
  void SetSpectrumFileName(string filedes);
  void SetMOT_Size(int i, double Size);
  void SetMOT_CenterX(int i, double X);
  void SetMOT_CenterZ(int i, double Y);
  void SetEField(int i, double field);
  void SetScintWidth(int i, double Width);
  void SetMWPCWidth(int i, double Width);
  void SetTimeStep(int i, double Width);*/
  //Get methods
/*  int GetNCondition(){return nCond;}
  int GetNFileSim(){return nFileSim;}*/
  //For Future
//  int MCPArea_RCond_Init(double low, double high,int N);  //Divide MCP into rings 
};


//Fitting Function
//double tof_spline_func(double *x, double parm[]);

#endif
