#ifndef CELL_H
#define CELL_H

#include "TFile.h"
#include "TF1.h"
#include "TF2.h"
#include "TVector.h" 
#include "TCut.h"
#include "TGraph2DErrors.h"
#include "TH2.h"
 
using namespace std;

class Cell{
private:
  double dx,dy,r,x0,y0; //width of rectangular cell, radius of area, center of area circle wrt to center of center cell
protected:
  TVectorD xlo,xmid,xhi,ylo,ymid,yhi; //the x and y coordinates defining each cell
  TVectorD Ylo_short, Ylo_ind; //non-duplicate list of ylo values and corresponding indicies in long vectors
  int n_cell,n_cellY; //number of cells
  double X_bound,Y_bound; //width/2 and height/2 of area enclosing cells
  int nx,ny; //greater than/equal to number of bins/cells in horizontal and vertical  
  TFile* f;
public:
  //functions
  Cell(double delX, double delY, double rad, double X0=0, double Y0=0);
  Cell(const char* filename); //load from filename
  virtual ~Cell();
  void ResizeVectors(int n);
  void InitializeCells();
  void SaveToFile(const char* filename);
  
  void OpenFile(const char* filename, const char* opt);
  void CloseFile();
  void ReadData(const char* filename);
  void WriteData(const char* filename);
  void PrintClass();
  
  bool InCircle(double x, double y);  
  bool InCell(int i, double x, double y);
  int GetIndex(double x, double y);
  void GetRange(int i, double& x1, double& x2, double& y1, double& y2);
  void GetCenter(int i, double& x, double& y);
  int GetNCells() {return n_cell;}
  TCut GetXYCut(int i,string Xstring, string Ystring); //returns TCut string for Xstring and Ystring values within cell area
};

class Q_Cell: public Cell{
private:
  TVectorD Q_mean,Q_err; //Q_std; //mean, mean error, and standard deviation
  //TH2D *hist_Q;
  //TGraph2DErrors *Q_graph2D; //graph of the Qmean in each cell;
public:
  Q_Cell(double delX, double delY, double rad, double X0=0, double Y0=0);
  Q_Cell(const char* filename); //load from filename
  ~Q_Cell();
  void SaveToFile(const char* filename); 
  void ReadData(const char* filename);
  void WriteData(const char* filename);
  void OutputToTextFile(const char* filename); 
  void PrintClass(); 
  
  double GetQMean(int i) {return Q_mean[i];};
  //double GetQMean(double x, double y);
  void SetQMean(int i, double Q) {Q_mean[i] = Q;}
  //void SetQStd(int i, double Qstd) {Q_std[i] = Qstd;}
  void SetQErr(int i, double Qerr) {Q_err[i] = Qerr;}
  void SetPoint(int i,double Qmean, double Qerr);
};

class T0_Cell: public Cell{
private:
  TVectorD T0_abs, T0_abs_err, T0_rel; //absolute and relative T0 of cell; relative to rel_idx cell (0 by default)
  int rel_idx;
  TF2 *poly22; //2D 2nd order poly in x and y
  TF1 *dist; //1D 1st order poly in distance(x,y)
  double corr_st; //correction strength, multiplier
public:
  T0_Cell(double delX, double delY, double rad, double X0=0, double Y0=0);
  T0_Cell(const char* filename); //load from filename
  ~T0_Cell();
  void ResizeVectors(int n); //resize local vectors
  void SaveToFile(const char* filename); 
  void ReadData(const char* filename);
  void WriteData(const char* filename);
  void OutputToTextFile(const char* filename);
  void PrintClass();
   
  
  double GetT0_abs(int i) {return T0_abs[i];};
  void SetT0_abs(int i, double Q) {T0_abs[i] = Q;}
  void SetT0_abs_err(int i, double Qerr) {T0_abs_err[i] = Qerr;}
  void SetPoint(int i,double Qmean, double Qerr);
  void SetRelT0Index(int i){rel_idx = i;}
  void SetRelT0Corr(); //relative to index 0 by default;
  double GetRelT0Corr(double x, double y); 
  double GetRelT0CorrPoly(double x, double y);
  double GetRelT0CorrDist(double x, double y);
  void SetRelCorrStrength(double st){corr_st = st;}
};

#endif
