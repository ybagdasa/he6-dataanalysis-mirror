#ifndef CALIBRATION_H
#define CALIBRATION_H

#include <string>
#include <stdint.h>
#include <fstream>
#include <vector>
#include <map>
#include <ctime>
#include <time.h>

#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TRandom.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "TGraph2DErrors.h"

#include "Analyzer.h"
#include "DataStruct.h"

using namespace std;

class Calibration{
private:
  string CalDataFile;
  Analyzer* CurrentAnalyzer;
  //MWPC Related
  string CalibrationScheme;
  bool KeepLessTrigger;
  double MWPCAnodeThreshold;
  double MWPCCathodeThreshold;
  //MCP Related
  double PreShiftX;
  double PreShiftY;
  double PreScaleX;
  double PreScaleY;
  double PreCutR;
  string GridMode;
public:
  Calibration(Analyzer*);
  ~Calibration();

  //Experiment
  //MWPC
  int ConfigCalMWPC(string Mode,bool KeepLessTrigger);
  int CalibrateMWPCPreview(string FilePrefix,int FileID,string Source);
  int CalibrateMWPC(string FilePrefix,int FileID,int CalID,string Source);
  int CalibrateMWPCPreviewSim(int FileID);
  int CalibrateMWPCSim(int FileID,int CalID);
  //Beta
  int CalibrateBeta(string FilePrefix,int FileID,int CalID,int SimCalID,string Source,string GainMapSwitch="OFF");
  int CalibrateBetaSim(int FileID,int CalID,string Source);
  int CalibrateLED(string FilePrefix,int FileID,int CalID,string mode,double Period);
  double CalibrateTiming();
  int CalibrateTimingRes();
  int CalibrateTimingZero(string FilePrefix,int FileID,int CalID);
  //MCP
  int ConfigCalMCP(string Mode,double ShiftX,double ShiftY,double ScaleX, double ScaleY, double CutR);
  int CalibratePreviewMCP(string FilePrefix,int FileID);
  int CalibrateMCPEff_Simple(string ExpFileName, string SimFileName, int CalID);
  int CalibrateMCPEff(string FilePrefix,int FileID,int CalID,string SimFileName);
  int CalibrateMCP(string FilePrefix,int FileID,int CalID,string Reference);
  int CalibrateMCPRes(string FilePrefix,int FileID,int CalID);
  int CalibrateMOT(string FilePrefix,int FileID,int CalID);
  int CalibrateMOTSimple(int CalID);
  int CalibrateMOTPicture(string CalFile, string ImageFile, string BkgFile,int CalID);
  //T0 - position calibration functions
  //int GenerateQMap(double delx, double dely, int QCalID);
  //int GenerateT0CorrectionMap(int QCalID, int T0CalID);

//  int SetupMWPCCal(double A_Th,double C_Th);
};


//Function declarations
//MWPC
void CountTrigger(DataStruct_MWPC_X, DataStruct_MWPC_Y, int & CountX, int & CountY,double CathodeThreshold);
int ProcessMWPCData(double AMWPC_anode1,double AMWPC_anode2,DataStruct_MWPC_X MWPC_X, DataStruct_MWPC_Y MWPC_Y, double & AnodePos, double & MWPC_PosX, double & MWPC_PosY, double & MWPC_ChargeX, double & MWPC_ChargeY);
int ConstructCorrectedMWPCPosPol(double AMWPC_anode1,double AMWPC_anode2,DataStruct_MWPC_X MWPC_X, DataStruct_MWPC_Y MWPC_Y, double & AnodePos, double & MWPC_PosX, double & MWPC_PosY,double **Xsig,double **Ysig,double FitParametersA[],double FitParametersX0[],double FitParametersX1[][4],double FitParametersX2[][4],bool Keep, double CathodeThreshold);
int ConstructCorrectedMWPCPosMicro(double AMWPC_anode1,double AMWPC_anode2,DataStruct_MWPC_X MWPC_X, DataStruct_MWPC_Y MWPC_Y, double & AnodePos, double & MWPC_PosX, double & MWPC_PosY,double **Xsig,double **Ysig,double PeaksX0[],double PeaksX1[],double PeaksX2[],double PeaksA[],double ExactPos0[],double ExactPos1[],double ExactPos2[],double ExactPosA[],bool Keep, double CathodeThreshold);
//MCP
TVectorD fit_ellipse(TGraph *g);
TVectorD ConicToParametric(const TVectorD &conic);
void InitGrid(vector<double>& XPoints,vector<double>& YPoints,TH2* GridSearch);
void FindRim(TGraph* Rim,TH1** RimSearch);
void AdjustGrid(GridPoint** GridArray,int sizeX,int sizeY);
void AdjustFailed(GridPoint** GridArray,int sizeX,int sizeY,double shift);
void FindGrid(GridPoint** GridArray,int sizeX,int sizeY,TH2* GridSearch,double BinRange,bool FailedOnly=false);
void FineTuneGrid(GridPoint** GridArray,int sizeX,int sizeY,TH2* GridSearch,double BinRange);
void FindEdges(GridEdge** HEdges,GridEdge** VEdges,GridPoint** GridArray, int sizeX, int sizeY, TH2* GridSearch,int NPoint);
int FindSquare(GridSquare** SquareArray,int sizeX,int sizeY,double Xpos,double Ypos,int& IndexX, int& IndexY,int Npoints);
bool TestInSquare(GridSquare** SquareArray,int i,int j,double Xpos, double Ypos, double startX, double startY, int Npoints);
bool TestSameSide(double Xpos,double Ypos,double XCenter,double YCenter,double X1,double Y1,double X2,double Y2);
void LocalFit(TH2* GridSearch,TH1* LocalProject,double GridX,double GridY,string Direction,double& LeftRes,double& RightRes,double& Width);
double Gauss2DBkg(double *x,double* par);

int BMP2ROOT(TH2* hist,string filename);

#endif
