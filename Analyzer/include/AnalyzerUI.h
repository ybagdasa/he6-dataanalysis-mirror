#ifndef ANALYZERUI_H
#define ANALYZERUI_H

#include <string>
#include "Analyzer.h"
#include "Calibration.h"

using namespace std;

class AnalyzerUI{
private:
  string Purpose;
  string CommandLine;
  Analyzer *CurrentAnalyzer;
  Calibration *CurrentCalibration;
public:
  AnalyzerUI(Analyzer * Ana,Calibration * CalPtr);
  ~AnalyzerUI();
  int Init();
  int Start(int mode);
  int ProcessCommand(string Cmd);
  void LS();
};
#endif
