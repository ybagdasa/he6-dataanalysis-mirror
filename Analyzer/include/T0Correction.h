#ifndef T0CORRECTION_H
#define T0CORRECTION_H

#include "TTree.h"
#include "TFile.h"
#include "TVector.h"


using namespace std;

class T0Correction{
  private:
  string OutTreesDirect,CalDirect;
  TFile *fin;
  TTree *OutTrees;
  vector<double> Qlo,Qhi;
  
  public:
  T0Correction(const char* filename, int i=0, int j=0);//i and j are indecies of the EventTrees
  ~T0Correction();
  int InitQSlices(int nq,double qlo, double qhi, double inv);
  int InitQSlices(vector<double> qlo, vector<double> qhi);
  int GenerateQMap(double delX, double delY, double R, int QCalID);
  int GenerateT0CorrectionMap(int QCalID, int T0CalID, double t_lo, double t_hi);
};
#endif
