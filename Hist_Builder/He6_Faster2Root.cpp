//      Ran Hong
//      Aug 13th 2013
//
//	Convert .fast file into a root Tree within a root file
//	usage:
//	make He6_Faster2Root
//	./He6_Faster2Root filename_prefix(no number or .fast) number_of_file
//
// 	adjust nmax if only a sample is required
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TRandom.h"

#include "Data_Reader.h"

const double nmax= 100000000;
using namespace std;

//Define data structures


typedef struct DataStruct_MWPC_X{
  double AX1;
  double AX2;
  double AX3;
  double AX4;
//  double A_X5;
//  double A_X6;
}DataStruct_MWPC_X;
  
typedef struct DataStruct_MWPC_Y{
  double AY1;
  double AY2;
  double AY3;
  double AY4;
//  double A_Y5;
//  double A_Y6;
}DataStruct_MWPC_Y;

typedef struct DataStruct_TMWPC_X{
  double TX1;
  double TX2;
  double TX3;
  double TX4;
//  double T_X5;
//  double T_X6;
}DataStruct_TMWPC_X;
  
typedef struct DataStruct_TMWPC_Y{
  double TY1;
  double TY2;
  double TY3;
  double TY4;
//  double T_Y5;
//  double T_Y6;
}DataStruct_TMWPC_Y;

typedef struct DataStruct_QMCP_anodes{
  double QX1;
  double QX2;
  double QY1;
  double QY2;
}DataStruct_QMCP_anodes;

typedef struct DataStruct_TMCP_anodes{
  double TX1;
  double TX2;
  double TY1;
  double TY2;
}DataStruct_TMCP_anodes;



int main (int argc, char** argv) {
  string inputfile;
  char filenamebuf[30];
  int NFile;                           
//  double TMCP_ref;	//  Time ref for first event
  int ReadOutType;

  int n = 0;  //  nb of data read
  int nk = 0; //  nb of data kept

  long double Tref = 0;
  double Tref_lr = 0;
  double QScint;
  double TScint_rel;
  double AMWPC_andode;
  double TMWPC_rel;
  double TBeta_diff;
  DataStruct_MWPC_X MWPC_X;
  DataStruct_TMWPC_X TMWPC_X;
  DataStruct_MWPC_Y MWPC_Y;
  DataStruct_TMWPC_Y TMWPC_Y;
  double QMCP;
  DataStruct_QMCP_anodes QMCP_anodes;
  DataStruct_TMCP_anodes TMCP_anodes;
  double TMCP_rel;
  double TOF;

  double GroupTime;

  unsigned short TriggerMap;

  long double TScint,TMWPC_anode,TX1,TX2,TY1,TY2,TMCP;
  long double TMWPC_X1,TMWPC_X2,TMWPC_X3,TMWPC_X4;
  long double TMWPC_Y1,TMWPC_Y2,TMWPC_Y3,TMWPC_Y4;

  double TX1mTX2,TX1pTX2,TY1mTY2,TY1pTY2;
  double TMWPCX_rel,TMWPCY_rel,TMCP_anodes_rel;
  double MWPC_PosX;

  if (argc < 3) {   //  command args & usage
    printf ("usage : \n");
    printf ("        %s  filename_prefix Number_of_files\n", argv[0]);
    printf ("\n");
    return EXIT_SUCCESS;
  }
  
  //Set up output trees, histograms and file
  TString fName =  argv[1];
//  fName.ReplaceAll (".fast", ".root");
  fName += ".root";

  // output root file
  TFile *f = new TFile (fName.Data (), "recreate");

  // root tree
  TTree *Tree_Tref = new TTree ("Tree_Tref", "Time reference");
  TTree *Tree_Scint = new TTree ("Tree_Scint", "Scintillator single event");
  TTree *Tree_MWPC = new TTree ("Tree_MWPC", "MWPC single event (grouped)");	//Exclude Scint event
  TTree *Tree_MCP = new TTree ("Tree_MCP", "MCP single event (grouped)");	//Exclude double trigger from (Scint and MWPC) and tripple trigger
  TTree *Tree_Beta = new TTree ("Tree_Beta", "Beta coincidence event: Scint+MWPC");	//Exclude tripple trigger
  TTree *Tree_Tripple = new TTree ("Tree_Tripple", "Tripple trigger event: Scint+MWPC+MCP");	//MCP anodes may or may not all triggers, info in trigger-map

  TTree *Tree_MWPC_Iso = new TTree ("Tree_MWPC_Iso", "Isolated MWPC anode");
  TTree *Tree_MWPCX_Iso = new TTree ("Tree_MWPC_Iso", "Isolated MWPCX cathode");
  TTree *Tree_MWPCY_Iso = new TTree ("Tree_MWPC_Iso", "Isolated MWPCY cathode");
  TTree *Tree_MCP_Iso = new TTree ("Tree_MCP_Iso", "Isolated MCP");
  TTree *Tree_MCP_anode_Iso = new TTree ("Tree_MCP_anode_Iso", "Isolated MCP anodes");

  // Branches  
  Tree_Tref->Branch("Event_No",&n,"N/I");
  Tree_Tref->Branch("Tref",&Tref_lr,"Tref_lr/D");

  Tree_Scint->Branch("Event_No",&n,"N/I");
  Tree_Scint->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_Scint->Branch("QScint",&QScint,"QScint/D");
  Tree_Scint->Branch("TScint_rel",&TScint_rel,"TScint_rel/D");
  Tree_Scint->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  Tree_MWPC->Branch("Event_No",&n,"N/I");  
  Tree_MWPC->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_MWPC->Branch("AMWPC_andode",&AMWPC_andode,"AMWPC_andode/D");
  Tree_MWPC->Branch("TMWPC_rel",&TMWPC_rel,"TMWPC_rel/D");
  Tree_MWPC->Branch("MWPC_X",&MWPC_X,"AX1/D:AX2:AX3:AX4");
  Tree_MWPC->Branch("TMWPC_X",&TMWPC_X,"TX1/D:TX2:TX3:TX4");
  Tree_MWPC->Branch("MWPC_Y",&MWPC_Y,"AY1/D:AY2:AY3:AY4");
  Tree_MWPC->Branch("TMWPC_Y",&TMWPC_Y,"TY1/D:TY2:TY3:TY4");
  Tree_MWPC->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  Tree_MCP->Branch("Event_No",&n,"N/I");
  Tree_MCP->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_MCP->Branch("TMCP_rel",&TMCP_rel,"TMCP_rel/D");
  Tree_MCP->Branch("QMCP",&QMCP,"QMCP/D");
  Tree_MCP->Branch("QMCP_anodes",&QMCP_anodes,"QX1/D:QX2:QY1:QY2");
  Tree_MCP->Branch("TMCP_anodes",&TMCP_anodes,"TX1/D:TX2:TY1:TY2");
  Tree_MCP->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  Tree_Beta->Branch("Event_No",&n,"N/I");
  Tree_Beta->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_Beta->Branch("QScint",&QScint,"QScint/D");
  Tree_Beta->Branch("TScint_rel",&TScint_rel,"TScint_rel/D");
  Tree_Beta->Branch("AMWPC_andode",&AMWPC_andode,"AMWPC_andode/D");
  Tree_Beta->Branch("TBeta_diff",&TBeta_diff,"TBeta_diff/D");
  Tree_Beta->Branch("MWPC_X",&MWPC_X,"AX1/D:AX2:AX3:AX4");
  Tree_Beta->Branch("TMWPC_X",&TMWPC_X,"TX1/D:TX2:TX3:TX4");
  Tree_Beta->Branch("MWPC_Y",&MWPC_Y,"AY1/D:AY2:AY3:AY4");
  Tree_Beta->Branch("TMWPC_Y",&TMWPC_Y,"TY1/D:TY2:TY3:TY4");
  Tree_Beta->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  Tree_Tripple->Branch("Event_No",&n,"N/I");
  Tree_Tripple->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_Tripple->Branch("QScint",&QScint,"QScint/D");
  Tree_Tripple->Branch("TScint_rel",&TScint_rel,"TScint_rel/D");
  Tree_Tripple->Branch("AMWPC_andode",&AMWPC_andode,"AMWPC_andode/D");
  Tree_Tripple->Branch("TBeta_diff",&TBeta_diff,"TBeta_diff/D");
  Tree_Tripple->Branch("MWPC_X",&MWPC_X,"AX1/D:AX2:AX3:AX4");
  Tree_Tripple->Branch("TMWPC_X",&TMWPC_X,"TX1/D:TX2:TX3:TX4");
  Tree_Tripple->Branch("MWPC_Y",&MWPC_Y,"AY1/D:AY2:AY3:AY4");
  Tree_Tripple->Branch("TMWPC_Y",&TMWPC_Y,"TY1/D:TY2:TY3:TY4");
  Tree_Tripple->Branch("TMCP_rel",&TMCP_rel,"TMCP_rel/D");
  Tree_Tripple->Branch("QMCP",&QMCP,"QMCP/D");
  Tree_Tripple->Branch("QMCP_anodes",&QMCP_anodes,"QX1/D:QX2:QY1:QY2");
  Tree_Tripple->Branch("TMCP_anodes",&TMCP_anodes,"TX1/D:TX2:TY1:TY2");
  Tree_Tripple->Branch("TOF",&TOF,"TOF/D");
  Tree_Tripple->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  Tree_MWPC_Iso->Branch("Event_No",&n,"N/I");  
  Tree_MWPC_Iso->Branch("AMWPC_andode",&AMWPC_andode,"AMWPC_andode/D");
  Tree_MWPC_Iso->Branch("TMWPC_rel",&TMWPC_rel,"TMWPC_rel/D");

  Tree_MWPCX_Iso->Branch("Event_No",&n,"N/I");
  Tree_MWPCX_Iso->Branch("MWPC_X",&MWPC_X,"AX1/D:AX2:AX3:AX4");
  Tree_MWPCX_Iso->Branch("TMWPCX_rel",&TMWPCX_rel,"TMWPCX_rel/D");

  Tree_MWPCY_Iso->Branch("Event_No",&n,"N/I");
  Tree_MWPCY_Iso->Branch("MWPC_Y",&MWPC_Y,"AY1/D:AY2:AY3:AY4");
  Tree_MWPCY_Iso->Branch("TMWPCY_rel",&TMWPCY_rel,"TMWPCY_rel/D");

  Tree_MCP_Iso->Branch("Event_No",&n,"N/I");
  Tree_MCP_Iso->Branch("QMCP",&QMCP,"QMCP/D");
  Tree_MCP_Iso->Branch("TMCP_rel",&TMCP_rel,"TMCP_rel/D");

  Tree_MCP_anode_Iso->Branch("Event_No",&n,"N/I");
  Tree_MCP_anode_Iso->Branch("QMCP_anodes",&QMCP_anodes,"QX1/D:QX2:QY1:QY2");
  Tree_MCP_anode_Iso->Branch("TMCP_anodes_rel",&TMCP_anodes_rel,"TMCP_anodes_rel/D");

  // root histos
  TH2F *h_IMAGE = new TH2F("h_IMAGE","h_IMAGE",200,-100,100,200,-100,100);
  TH1F *h_X = new TH1F("h_X","h_X",1000,-100,100);
  TH1F *h_Y = new TH1F("h_Y","h_Y",1000,-100,100);
  TH1F *h_TX1pX2 = new TH1F("h_TX1pX2","h_TX1pX2",1000,0,200);
  TH1F *h_TY1pY2 = new TH1F("h_TY1pY2","h_TY1pY2",1000,0,200);
  TH1F *h_TMCP_rel = new TH1F("h_TMCP_rel","h_TMCP_rel",1600,-100,1500);

  TH1D *h_Scint = new TH1D("h_Scint","Scint. alone spectrum",1000,0,1048576);
  TH1D *h_MWPC  = new TH1D("h_MWPC","MWPC Anode alone spectrum",1000,0,16384);
  TH1D *h_Scint_Coin = new TH1D("h_Scint_Coin","Scint. coincidence spectrum",1000,0,1048576);
  TH1D *h_MWPC_Coin  = new TH1D("h_MWPC_Coin","MWPC Anode coincidence spectrum",1000,0,16384);
  TH1D *h_Cathode_X = new TH1D("h_Cathode_X","MWPC Cathode position spectrum",80,-20,20);
  /////////////////////////////////////////////End Root Definition//////////////////////////////////////


  Data_Reader* He6_Data_Reader = new Data_Reader();	//Create a Data reader

  NFile = atoi(argv[2]);

  for (int ifile=1;ifile<=NFile;ifile++){//File loop

  inputfile = argv[1];

  sprintf(filenamebuf,"_%04d.fast",ifile);
  inputfile += filenamebuf;

  if (He6_Data_Reader->OpenInputFile(inputfile) == -1) {
    printf ("error opening file %s\n",inputfile.c_str());
    return EXIT_FAILURE;
  }
  cout <<"Processing file" << inputfile.c_str()<<endl;
  //--------------------------------------------------//
  while (n<nmax) {  //  loop on data from file
    ReadOutType = He6_Data_Reader->Read_From_File();
    /*Check abnormals*******************************************/
    if (ReadOutType == TYPEEof){
      cout<<"End of file!\n";
      break;
    }else if(ReadOutType == TYPEERROR){
      cout<<"ERROR happened!\n";
      break;
    }
    /***********************************************************/

    if (n%100000==0) {
      cout<<n<<" read evts"<<endl;
      cout<<nk<<" kept evts"<<endl;
    }
    //--------------if complete event
    //Clean variables first
    QScint=TScint_rel=AMWPC_andode=TMWPC_rel=TBeta_diff=0;
    MWPC_X.AX1=MWPC_X.AX2=MWPC_X.AX3=MWPC_X.AX4=0;
    TMWPC_X.TX1=TMWPC_X.TX2=TMWPC_X.TX3=TMWPC_X.TX4=0;
    MWPC_Y.AY1=MWPC_Y.AY2=MWPC_Y.AY3=MWPC_Y.AY4=0;
    TMWPC_Y.TY1=TMWPC_Y.TY2=TMWPC_Y.TY3=TMWPC_Y.TY4=0;
    QMCP=0;
    QMCP_anodes.QX1=QMCP_anodes.QX2=QMCP_anodes.QY1=QMCP_anodes.QY2=0;
    TMCP_anodes.TX1=TMCP_anodes.TX2=TMCP_anodes.TY1=TMCP_anodes.TY2=0;
    TMCP_rel=TOF=0;

    GroupTime=0;

    TScint=TMWPC_anode=TX1=TX2=TY1=TY2=TMCP=0;
    TMWPC_X1=TMWPC_X2=TMWPC_X3=TMWPC_X4=0;
    TMWPC_Y1=TMWPC_Y2=TMWPC_Y3=TMWPC_Y4=0;

    TX1mTX2=TX1pTX2=TY1mTY2=TY1pTY2=0;
    TMWPCX_rel=TMWPCY_rel=TMCP_anodes_rel=0;
   
    //Start sorting

    TriggerMap = He6_Data_Reader->GetTriggerMap();
    if (ReadOutType==TYPEGroup){
      nk++;
      GroupTime = He6_Data_Reader->GetGroupTriggerTime();
      //Get data from Data_Reader
      if ((TriggerMap&1)!=0){
        He6_Data_Reader->GetTime_Tref(Tref);
        Tref_lr = double(Tref);
      }
      if ((TriggerMap&(1<<1))!=0){
        He6_Data_Reader->GetVal_Scint(QScint);
        He6_Data_Reader->GetTime_Scint(TScint);
        TScint_rel = (TScint - Tref)*1.e3;	//ms
      }
      if ((TriggerMap&(1<<2))!=0){
        He6_Data_Reader->GetVal_MWPC_anode(AMWPC_andode);
        He6_Data_Reader->GetTime_MWPC_anode(TMWPC_anode);
        TMWPC_rel = (TMWPC_anode - Tref)*1.e3;	//ms
      }
      if ((TriggerMap&(1<<3))!=0){
        He6_Data_Reader->GetVal_MCP(QMCP);
        He6_Data_Reader->GetTime_MCP(TMCP);
        TMCP_rel = (TMCP - Tref)*1.e3;	//ms
      }
      if ((TriggerMap&(1<<5))!=0){
        He6_Data_Reader->GetVal_MCP_anodes(QMCP_anodes.QX1,QMCP_anodes.QX2,QMCP_anodes.QY1,QMCP_anodes.QY2);
        He6_Data_Reader->GetTime_MCP_anodes(TX1,TX2,TY1,TY2);
      }
      if ((TriggerMap&(1<<6))!=0){
        He6_Data_Reader->GetVal_MWPC_X(MWPC_X.AX1,MWPC_X.AX2,MWPC_X.AX3,MWPC_X.AX4);
        He6_Data_Reader->GetTime_MWPC_X(TMWPC_X1,TMWPC_X2,TMWPC_X3,TMWPC_X4);
	MWPC_PosX = (-12.*MWPC_X.AX1 -4.*MWPC_X.AX2+4.*MWPC_X.AX3+12.*MWPC_X.AX4)/(MWPC_X.AX1+MWPC_X.AX2+MWPC_X.AX3+MWPC_X.AX4);
      }
      if ((TriggerMap&(1<<7))!=0){
        He6_Data_Reader->GetVal_MWPC_Y(MWPC_Y.AY1,MWPC_Y.AY2,MWPC_Y.AY3,MWPC_Y.AY4);
        He6_Data_Reader->GetTime_MWPC_Y(TMWPC_Y1,TMWPC_Y2,TMWPC_Y3,TMWPC_Y4);
      }

      //Calculated time differences
      if ((TriggerMap&6)==6) {
        TBeta_diff = (TScint - TMWPC_anode)*1.e9;	//ns
      }
      if ((TriggerMap&14)==14) {
        TOF = (TMCP-TScint)*1.e9;	//ns
      }
      if ((TriggerMap&40)==40) {
        TMCP_anodes.TX1=(TX1-TMCP)*1.e9;
        TMCP_anodes.TX2=(TX2-TMCP)*1.e9;
        TMCP_anodes.TY1=(TY1-TMCP)*1.e9;
        TMCP_anodes.TY2=(TY2-TMCP)*1.e9;
      }
      if ((TriggerMap&24)==24) {
        TX1mTX2=(TX1-TX2)*1.e9; //ns
        TX1pTX2=(TX1+TX2-2*TMCP)*1.e9;
        TY1mTY2=(TY1-TY2)*1.e9;
        TY1pTY2=(TY1+TY2-2*TMCP)*1.e9;
      }
      if ((TriggerMap&32)!=0 && (TriggerMap&8)==0) {
        TMCP_anodes.TX1=(TX1-Tref)*1.e3;
        TMCP_anodes.TX2=(TX2-Tref)*1.e3;
        TMCP_anodes.TY1=(TY1-Tref)*1.e3;
        TMCP_anodes.TY2=(TY2-Tref)*1.e3;
      }
      if ((TriggerMap&68)==68) {
        TMWPC_X.TX1 = (TMWPC_X1 - TMWPC_anode)*1.e9;	//ns
        TMWPC_X.TX2 = (TMWPC_X2 - TMWPC_anode)*1.e9;	//ns
        TMWPC_X.TX3 = (TMWPC_X3 - TMWPC_anode)*1.e9;	//ns
        TMWPC_X.TX4 = (TMWPC_X4 - TMWPC_anode)*1.e9;	//ns
      }
      if ((TriggerMap&68)==68) {
        TMWPC_Y.TY1 = (TMWPC_Y1 - TMWPC_anode)*1.e9;	//ns
        TMWPC_Y.TY2 = (TMWPC_Y2 - TMWPC_anode)*1.e9;	//ns
        TMWPC_Y.TY3 = (TMWPC_Y3 - TMWPC_anode)*1.e9;	//ns
        TMWPC_Y.TY4 = (TMWPC_Y4 - TMWPC_anode)*1.e9;	//ns
      }

      //Fill Trees and Histograms
      //Tref
      if ((TriggerMap&1)!=0){
        Tree_Tref->Fill();
      }
      //Scint Single, may overlap with MCP
      if ((TriggerMap&(1<<1))!=0 && (TriggerMap&(1<<2))==0){
        Tree_Scint->Fill();
	h_Scint->Fill(QScint);
      }
      //MWPC Single, may overlap with MCP
      if ((TriggerMap&(1<<2))!=0 && (TriggerMap&(1<<1))==0){
        Tree_MWPC->Fill();
	h_MWPC->Fill(AMWPC_andode);
	if ((TriggerMap&(1<<6))!=0){
	  h_Cathode_X->Fill(0.);
	}
      }
      //MCP Single, may overlap with Scint or MWPC
      if ((TriggerMap&(1<<3))!=0){
        Tree_MCP->Fill();
      }
      //Beta double coincidence, may overlap with MCP
      if ((TriggerMap&(1<<1))!=0 && (TriggerMap&(1<<2))!=0){
        Tree_Beta->Fill();
	h_Scint_Coin->Fill(QScint);
	h_MWPC_Coin->Fill(AMWPC_andode);
	if ((TriggerMap&(1<<6))!=0){
	  h_Cathode_X->Fill(MWPC_PosX);
	}
      }
      //Tripple coincidence
      if ((TriggerMap&14)==14){
        Tree_Tripple->Fill();
	h_Scint_Coin->Fill(QScint);
	h_MWPC_Coin->Fill(AMWPC_andode);
	if ((TriggerMap&(1<<6))!=0){
	  h_Cathode_X->Fill(MWPC_PosX);
	}
      }
      //MCP histograms
      if ((TriggerMap&24)==24){
        h_IMAGE->Fill(TX1mTX2,TY1mTY2);
        h_X->Fill(TX1mTX2);
        h_Y->Fill(TY1mTY2);
        h_TX1pX2->Fill(TX1pTX2);
        h_TY1pY2->Fill(TY1pTY2);
        h_TMCP_rel->Fill(TMCP_rel);
//        cout<<TMCP_rel<<endl;
      }
    }else if(ReadOutType==TYPET_ref){
      He6_Data_Reader->GetTime_Tref(Tref);
      Tref_lr = double(Tref);
      Tree_Tref->Fill();
//      cout<<Tref<<endl;
    }else if(ReadOutType==TYPEScint){
      He6_Data_Reader->GetVal_Scint(QScint);
      He6_Data_Reader->GetTime_Scint(TScint);
      TScint_rel = (TScint - Tref)*1.e3;	//ms
      Tree_Scint->Fill();
      h_Scint->Fill(QScint);
    }else if(ReadOutType==TYPEMWPC_anode){
      He6_Data_Reader->GetVal_MWPC_anode(AMWPC_andode);
      He6_Data_Reader->GetTime_MWPC_anode(TMWPC_anode);
      TMWPC_rel = (TMWPC_anode - Tref)*1.e3;	//ms
      Tree_MWPC_Iso->Fill();
      h_MWPC->Fill(AMWPC_andode);
    }else if(ReadOutType==TYPEMWPC_X){
      He6_Data_Reader->GetVal_MWPC_X(MWPC_X.AX1,MWPC_X.AX2,MWPC_X.AX3,MWPC_X.AX4);
      He6_Data_Reader->GetTime_MWPC_X(TMWPC_X1,TMWPC_X2,TMWPC_X3,TMWPC_X4);
      if (TMWPC_X1!=0) TMWPCX_rel = (TMWPC_X1-Tref)*1.e3;	//ms
      if (TMWPC_X2!=0) TMWPCX_rel = (TMWPC_X2-Tref)*1.e3;	//ms
      if (TMWPC_X3!=0) TMWPCX_rel = (TMWPC_X3-Tref)*1.e3;	//ms
      if (TMWPC_X4!=0) TMWPCX_rel = (TMWPC_X4-Tref)*1.e3;	//ms
      Tree_MWPCX_Iso->Fill();
    }else if(ReadOutType==TYPEMWPC_Y){
      He6_Data_Reader->GetVal_MWPC_Y(MWPC_Y.AY1,MWPC_Y.AY2,MWPC_Y.AY3,MWPC_Y.AY4);
      He6_Data_Reader->GetTime_MWPC_Y(TMWPC_Y1,TMWPC_Y2,TMWPC_Y3,TMWPC_Y4);
      if (TMWPC_Y1!=0) TMWPCY_rel = (TMWPC_Y1-Tref)*1.e3;	//ms
      if (TMWPC_Y2!=0) TMWPCY_rel = (TMWPC_Y2-Tref)*1.e3;	//ms
      if (TMWPC_Y3!=0) TMWPCY_rel = (TMWPC_Y3-Tref)*1.e3;	//ms
      if (TMWPC_Y4!=0) TMWPCY_rel = (TMWPC_Y4-Tref)*1.e3;	//ms
      Tree_MWPCY_Iso->Fill();
    }else if(ReadOutType==TYPEMCP){
      He6_Data_Reader->GetVal_MCP(QMCP);
      He6_Data_Reader->GetTime_MCP(TMCP);
      TMCP_rel = (TMCP - Tref)*1.e3;	//ms
      Tree_MCP_Iso->Fill();
    }else if(ReadOutType==TYPEMCP_anodes){
      He6_Data_Reader->GetVal_MCP_anodes(QMCP_anodes.QX1,QMCP_anodes.QX2,QMCP_anodes.QY1,QMCP_anodes.QY2);
      He6_Data_Reader->GetTime_MCP_anodes(TX1,TX2,TY1,TY2);
      if (TX1!=0) TMCP_anodes_rel = (TX1 - Tref)*1.e3;	//ms
      if (TX2!=0) TMCP_anodes_rel = (TX2 - Tref)*1.e3;	//ms
      if (TY1!=0) TMCP_anodes_rel = (TY1 - Tref)*1.e3;	//ms
      if (TY2!=0) TMCP_anodes_rel = (TY2 - Tref)*1.e3;	//ms
      Tree_MCP_anode_Iso->Fill();
    }else{
      ;
    }

//complete event
    n++;
  }//reader loop

  //  close input file
  He6_Data_Reader->closeInputFile();
  }//End file loop
  //--------------------------------------------------// 
  gStyle->SetPalette(1);		

  TCanvas *c1 = new TCanvas("c1","c1");
  c1->Clear();
  c1->Divide(3,2);
  c1->cd(1);
  h_IMAGE->Draw("colz");
  c1->cd(2);
  h_X->Draw();
  c1->cd(3);
  h_Y->Draw();
  c1->cd(4);
  h_TMCP_rel->Draw();
  c1->cd(5);
  h_TX1pX2->Draw();
  c1->cd(6);
  h_TY1pY2->Draw();
  c1->Update();
  c1->SaveAs("MCP_Spectra.pdf");

  TCanvas *c2 = new TCanvas("c2","c2");
  c2->Clear();
  c2->Divide(3,2);
  c2->cd(1);
  h_Scint->Draw();
  c2->cd(4);
  h_MWPC->Draw();
  c2->cd(2);
  h_Scint_Coin->Draw();
  c2->cd(5);
  h_MWPC_Coin->Draw();
  c2->cd(3);
  h_Cathode_X->Draw();

  c2->Update();
  c2->SaveAs("Beta_Spectra.pdf");
  
  f->Write ();
  f->Close ();

  delete He6_Data_Reader;
  return EXIT_SUCCESS;
}


