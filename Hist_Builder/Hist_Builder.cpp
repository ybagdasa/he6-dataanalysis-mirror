#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <string>

#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTree.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TRandom.h"




//using namespace std;

int main()
{

TH1F *dummy = new TH1F("dummy", "dummy", 10, 0, 1);
TFile *dummy_file = new TFile("dummy.root","recreate");
TTree *dummytree = new TTree("dummytree","dummytree",99);

//TFile *f = new TFile("../testfile.root","READ");
    
TFile *f = new TFile("/home/he6/MANIP_He6_CENPA_BIS/DAQ/DATA/20130912-173346_TC2_ON_BIS.root");

	TTree *Sim_Tree = (TTree*)f->Get("Tree_MCP");

	
	/*char file_name[20];
	sprintf(file_name,"file_name%d",1);
	string outfile;
	outfile = (string)file_name;
	cout << outfile.c_str() << endl;*/
	
	
	//Sim_Tree->MakeSelector("HistSelector");
	Sim_Tree->Process("HistSelector.C++");
	
	
	return 0;
}




