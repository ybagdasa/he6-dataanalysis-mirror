//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Aug 21 15:12:55 2013 by ROOT version 5.34/02
// from TTree Tree_MCP/MCP single event (grouped)
// found on file: ../testfile.root
//////////////////////////////////////////////////////////

#ifndef HistSelector_h
#define HistSelector_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TH2.h>
#include <TPaletteAxis.h>
#include <TF1.h>
#include <TGraph.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class HistSelector : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain


TFile *f;

double t_first,t_last,t_step,period;
double x_max_cut, x_min_cut, y_max_cut, y_min_cut;
double max_z, max_temp;
double max_rate_temp, max_rate, min_rate_temp, min_rate;
double max_x_prof_temp, max_x_prof, max_y_prof_temp, max_y_prof;
int n_hists,time;

char hname[200],htitle[200];
TH2D *MCP_Hists[2000];
TH1D *Rate_Hists[2000], *x_prof[2000], *y_prof[2000], *Total_Rate_Hist;

TGraph *x_size[2000], *y_size[2000];

TF1 *gaus;

TCanvas *can1,*can2;

TPaletteAxis *palette;

   // Declaration of leaf types
   Int_t           Event_No;
   Double_t        GroupTime;
   Double_t        TMCP_rel;
   Double_t        QMCP;
   Double_t        QMCP_anodes_QX1;
   Double_t        QMCP_anodes_QX2;
   Double_t        QMCP_anodes_QY1;
   Double_t        QMCP_anodes_QY2;
   Double_t        TMCP_anodes_TX1;
   Double_t        TMCP_anodes_TX2;
   Double_t        TMCP_anodes_TY1;
   Double_t        TMCP_anodes_TY2;
   UShort_t        TriggerMap;

   // List of branches
   TBranch        *b_N;   //!
   TBranch        *b_GroupTime;   //!
   TBranch        *b_TMCP_rel;   //!
   TBranch        *b_QMCP;   //!
   TBranch        *b_QMCP_anodes;   //!
   TBranch        *b_TMCP_anodes;   //!
   TBranch        *b_TriggerMap;   //!

   HistSelector(TTree * /*tree*/ =0) : fChain(0) { }
   virtual ~HistSelector() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(HistSelector,0);
};

#endif

#ifdef HistSelector_cxx
void HistSelector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Event_No", &Event_No, &b_N);
   fChain->SetBranchAddress("GroupTime", &GroupTime, &b_GroupTime);
   fChain->SetBranchAddress("TMCP_rel", &TMCP_rel, &b_TMCP_rel);
   fChain->SetBranchAddress("QMCP", &QMCP, &b_QMCP);
   fChain->SetBranchAddress("QMCP_anodes", &QMCP_anodes_QX1, &b_QMCP_anodes);
   fChain->SetBranchAddress("TMCP_anodes", &TMCP_anodes_TX1, &b_TMCP_anodes);
   fChain->SetBranchAddress("TriggerMap", &TriggerMap, &b_TriggerMap);
}

Bool_t HistSelector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef HistSelector_cxx
