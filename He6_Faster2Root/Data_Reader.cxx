//	Read .fast file and pre-analyze

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <bitset>

#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TStyle.h"

#include "Data_Reader.h"

using namespace std;

//Constructor
Data_Reader::Data_Reader()
{
  reader = NULL;
  data = NULL;
  Reset();
  randnum.SetSeed(0); //Set seed at random;
  dt = 8e-12; //uniform time interval for smoothing discretization [sec]
}

//Destructor
Data_Reader::~Data_Reader()
{
  ;
}

//Open input file
int Data_Reader::OpenInputFile(string input_file)
{
  reader = faster_file_reader_open (input_file.c_str());                  //  create a reader
  if (reader == NULL) {
    printf ("error opening file %s\n", input_file.c_str());
    return -1;
  }
  return 0;
}

//Close input file
int Data_Reader::closeInputFile()
{
  faster_file_reader_close (reader);
  return 0;
}

//Reset

void Data_Reader::Reset()
{
  //MCP QDCs
  QMCP.clear();
  QX1.clear();
  QX2.clear();
  QY1.clear();
  QY2.clear();
  TMCP.clear();
  TX1.clear();
  TX2.clear();
  TY1.clear();
  TY2.clear();
  //Tref ADC
  A_Tref.clear();
  T_Tref.clear();
  //Scintillator-PMT QDC
  QPMT_A[0].clear();
  QPMT_A[1].clear();
  TPMT_A.clear();
  QPMT_D[0].clear();
  QPMT_D[1].clear();
  TPMT_D.clear();
  A_LED.clear();
  T_LED.clear();
  Q_N2Laser.clear();
  T_N2Laser.clear();
  //MWPC ADCs
  A_MWPC_anode1.clear();
  T_MWPC_anode1.clear();
  A_MWPC_anode2.clear();
  T_MWPC_anode2.clear();
  A_MWPC_X1.clear();
  A_MWPC_X2.clear();
  A_MWPC_X3.clear();
  A_MWPC_X4.clear();
  A_MWPC_X5.clear();
  A_MWPC_X6.clear();
  A_MWPC_Y1.clear();
  A_MWPC_Y2.clear();
  A_MWPC_Y3.clear();
  A_MWPC_Y4.clear();
  A_MWPC_Y5.clear();
  A_MWPC_Y6.clear();
  T_MWPC_X1.clear();
  T_MWPC_X2.clear();
  T_MWPC_X3.clear();
  T_MWPC_X4.clear();
  T_MWPC_X5.clear();
  T_MWPC_X6.clear();
  T_MWPC_Y1.clear();
  T_MWPC_Y2.clear();
  T_MWPC_Y3.clear();
  T_MWPC_Y4.clear();
  T_MWPC_Y5.clear();
  T_MWPC_Y6.clear();

  TriggerMap=0;
  GroupTriggerTime=0;
}

//Read from file
int Data_Reader::Read_From_File()
{
  //faster data
  unsigned char  alias;
  unsigned short label;
  unsigned short lsize;
  double         clock;           // time stamp
  long double    hr_clock;        // time stamp + tdc
  double	 pk_clock;	  // time stamp + adc_delta_t (Peak time)
  //  qdc          (qdc_caras.h)
  qdc_t_x2 qq_tdc;
  //  adc	   (adc_caras.h)
  adc_data adc;
  //  group        (fast_data.h)
  faster_buffer_reader_p group_reader;
  char                   group_buffer[2500];
  faster_data_p          group_data;
  int                    group_n = 0;
  // ReadOutType
  int ReadOutType;
  // Trigger Map
  TRandom3 randnum;
  if ((data = faster_file_reader_next (reader)) != NULL){
    Reset();
    alias = faster_data_type_alias (data);
    label = faster_data_label      (data); 
    clock = faster_data_clock_sec  (data);
    lsize = faster_data_load_size  (data);
    switch(alias) {
      case GROUP_TYPE_ALIAS:
//	cout << "Type Group"<<endl;
//	cin.get();
        faster_data_load (data, group_buffer);
        group_reader = faster_buffer_reader_open (group_buffer, lsize);

        GroupTriggerTime = clock;
        if(Read_From_Group_Buffer(group_reader)!=0) return TYPEERROR;
        faster_buffer_reader_close (group_reader);
        if((TriggerMap & 15)==0 && ((TriggerMap>>8)&3)==0 ){
//          cout << "WARNING: Group with no primary trigger!\n";
//            cout << "         Trigger Map = " << std::bitset<16>(TriggerMap) << endl;
          //return TYPEERROR;
            return TYPEGroup;
        }
        return TYPEGroup;
        break;

      case QDC_TDC_X2_TYPE_ALIAS:
        faster_data_load (data, &qq_tdc);
	hr_clock = (long double)(clock) + (long double)(qx2_get_tdc_sec (qq_tdc)) +.5*dt*randnum.Uniform(-1,1);   // long double
//	cout << "Type QDC "<<QDC_TDC_X2_TYPE_ALIAS<<endl;
//	cout << "Label: "<< label << endl;
//	cin.get();
	switch(label){
	  case 1:
	    QX1.push_back(qq_tdc.q1);
	    TX1.push_back(hr_clock);
	    return TYPEMCP_anodes;
	    break;
	  case 2:
	    QX2.push_back(qq_tdc.q1);
	    TX2.push_back(hr_clock);
	    return TYPEMCP_anodes;
	    break;
	  case 3:
	    QY1.push_back(qq_tdc.q1);
	    TY1.push_back(hr_clock);
	    return TYPEMCP_anodes;
	    break;
	  case 4:
	    QY2.push_back(qq_tdc.q1);
	    TY2.push_back(hr_clock);
	    return TYPEMCP_anodes;
	    break;
	  case 5:
	    QMCP.push_back(qq_tdc.q1);
	    TMCP.push_back(hr_clock);
	    return TYPEMCP;
	    break;
	  case 6:
	    QPMT_A[0].push_back(qq_tdc.q1);
	    QPMT_A[1].push_back(qq_tdc.q2);
	    TPMT_A.push_back(hr_clock);
	    return TYPEPMT_A;
	    break;
	  case 7:
	    Q_N2Laser.push_back(qq_tdc.q1);
	    T_N2Laser.push_back(hr_clock);
	    return TYPEN2LASER;
	    break;
	  case 8:
	    QPMT_D[0].push_back(qq_tdc.q1);
	    QPMT_D[1].push_back(qq_tdc.q2);
	    TPMT_D.push_back(hr_clock);
	    return TYPEPMT_D;
	    break;
	}
	break;
      case ADC_DATA_TYPE_ALIAS:
	faster_data_load (data, &adc);
//	pk_clock = clock + delta_t_sec(adc);
//	pk_clock = clock + adc_delta_t_ns(adc)/1.0e9;
//	cout << "Type ADC "<<ADC_DATA_TYPE_ALIAS<<endl;
//	cout << "Label: "<< label << endl;
//	cin.get();
	switch(label){
	  case 9:
	    A_MWPC_X1.push_back(adc.measure);
	    T_MWPC_X1.push_back(clock);
            return TYPEMWPC_X;
	    break;
	  case 10:
	    A_MWPC_X2.push_back(adc.measure);
	    T_MWPC_X2.push_back(clock);
            return TYPEMWPC_X;
	    break;
	  case 11:
	    A_MWPC_X3.push_back(adc.measure);
	    T_MWPC_X3.push_back(clock);
            return TYPEMWPC_X;
	    break;
	  case 12:
	    A_MWPC_X4.push_back(adc.measure);
	    T_MWPC_X4.push_back(clock);
            return TYPEMWPC_X;
	    break;
	  case 13:
	    A_MWPC_X5.push_back(adc.measure);
	    T_MWPC_X5.push_back(clock);
            return TYPEMWPC_X;
	    break;
	  case 14:
	    A_MWPC_X6.push_back(adc.measure);
	    T_MWPC_X6.push_back(clock);
            return TYPEMWPC_X;
	    break;
	  case 15:
	    A_MWPC_Y1.push_back(adc.measure);
	    T_MWPC_Y1.push_back(clock);
            return TYPEMWPC_Y;
	    break;
	  case 16:
	    A_MWPC_Y2.push_back(adc.measure);
	    T_MWPC_Y2.push_back(clock);
            return TYPEMWPC_Y;
	    break;
	  case 17:
	    A_MWPC_Y3.push_back(adc.measure);
	    T_MWPC_Y3.push_back(clock);
            return TYPEMWPC_Y;
	    break;
	  case 18:
	    A_MWPC_Y4.push_back(adc.measure);
	    T_MWPC_Y4.push_back(clock);
            return TYPEMWPC_Y;
	    break;
	  case 19:
	    A_MWPC_Y5.push_back(adc.measure);
	    T_MWPC_Y5.push_back(clock);
            return TYPEMWPC_Y;
	    break;
	  case 20:
	    A_MWPC_Y6.push_back(adc.measure);
	    T_MWPC_Y6.push_back(clock);
            return TYPEMWPC_Y;
	    break;
	  case 21:
	    A_MWPC_anode1.push_back(adc.measure);
	    T_MWPC_anode1.push_back(clock);
            return TYPEMWPC_anode;
	    break;
	  case 22:
	    A_MWPC_anode2.push_back(adc.measure);
	    T_MWPC_anode2.push_back(clock);
            return TYPEMWPC_anode;
	    break;
	  case 23:
	    A_LED.push_back(adc.measure);
	    T_LED.push_back(clock);
            return TYPELED;
            break;
	  case 24:
	    A_Tref.push_back(adc.measure);
	    T_Tref.push_back(clock);
	    return TYPET_ref;
            break;
	}
	break;
      case SAMPLING_TYPE_ALIAS:
	cout << "Found sampling"<<endl;
	break;
      default:
//	cout << "Type Else "<< TYPEElse<<endl;
//	cin.get();
        return TYPEElse;
        break;
    }
  }
  return TYPEEof;
}

//Read from buffer
int Data_Reader::Read_From_Group_Buffer(faster_buffer_reader_p group_reader)
{
  //faster data
  unsigned char  alias;
  unsigned short label;
  unsigned short lsize;
  double         clock;           // time stamp
  long double    hr_clock;        // time stamp + tdc
  double	 pk_clock;	  // time stamp + adc_delta_t (Peak time)
    
  faster_data_p  group_data;
  //  qdc          (qdc_caras.h)
  qdc_t_x2    qq_tdc;
  //  adc          (adc_caras.h)
  adc_data adc;
  /***************/
  while ((group_data = faster_buffer_reader_next (group_reader)) != NULL) {
    alias = faster_data_type_alias (group_data);
    label = faster_data_label      (group_data);
    clock = faster_data_clock_sec  (group_data);
    lsize = faster_data_load_size  (group_data);
      //cout << " alias = " << (int)alias << " label = " << label << endl;
    switch(alias) {
      case GROUP_TYPE_ALIAS:
	printf("ERROR: Group within a group!\n");
	return -1;
	break;

      case QDC_TDC_X2_TYPE_ALIAS:
	faster_data_load (group_data, &qq_tdc);
	hr_clock = (long double)(clock) + (long double)(qx2_get_tdc_sec (qq_tdc)) + .5*dt*randnum.Uniform(-1,1);   // long double
	switch(label){
	  case 1:
	    QX1.push_back(qq_tdc.q1);
	    TX1.push_back(hr_clock);
	    TriggerMap |= 1<<5;
	    break;
	  case 2:
	    QX2.push_back(qq_tdc.q1);
	    TX2.push_back(hr_clock);
	    TriggerMap |= 1<<5;
	    break;
	  case 3:
	    QY1.push_back(qq_tdc.q1);
	    TY1.push_back(hr_clock);
	    TriggerMap |= 1<<5;
	    break;
	  case 4:
	    QY2.push_back(qq_tdc.q1);
	    TY2.push_back(hr_clock);
	    TriggerMap |= 1<<5;
	    break;
	  case 5:
	    QMCP.push_back(qq_tdc.q1);
	    TMCP.push_back(hr_clock);
	    TriggerMap |= 1<<3;
	    break;
	  case 6:
	    QPMT_A[0].push_back(qq_tdc.q1);
	    QPMT_A[1].push_back(qq_tdc.q2);
	    TPMT_A.push_back(hr_clock);
	    TriggerMap |= 1<<1;
	    break;
	  case 7:
	    Q_N2Laser.push_back(qq_tdc.q1);
	    T_N2Laser.push_back(hr_clock);
	    TriggerMap |= 1<<13;
	    break;
	  case 8:
	    QPMT_D[0].push_back(qq_tdc.q1);
	    QPMT_D[1].push_back(qq_tdc.q2);
	    TPMT_D.push_back(hr_clock);
	    TriggerMap |= 1<<2;
	    break;
	  default:
	    cout << "label = " << label << "  qq_tdc.q1 = " << qq_tdc.q1 << endl;
	    break;
	}
	break;

      case ADC_DATA_TYPE_ALIAS:
	faster_data_load (group_data, &adc);
//	pk_clock = clock + delta_t_sec(adc);
	//pk_clock = clock + adc_delta_t_ns(adc)/1.0e9;
	switch(label){
	  case 9:
	    A_MWPC_X1.push_back(adc.measure);
	    T_MWPC_X1.push_back(clock);
	    TriggerMap |= 1<<6;
	    break;
	  case 10:
	    A_MWPC_X2.push_back(adc.measure);
	    T_MWPC_X2.push_back(clock);
	    TriggerMap |= 1<<6;
	    break;
	  case 11:
	    A_MWPC_X3.push_back(adc.measure);
	    T_MWPC_X3.push_back(clock);
	    TriggerMap |= 1<<6;
	    break;
	  case 12:
	    A_MWPC_X4.push_back(adc.measure);
	    T_MWPC_X4.push_back(clock);
	    TriggerMap |= 1<<6;
	    break;
	  case 13:
	    A_MWPC_X5.push_back(adc.measure);
	    T_MWPC_X5.push_back(clock);
	    TriggerMap |= 1<<6;
	    break;
	  case 14:
	    A_MWPC_X6.push_back(adc.measure);
	    T_MWPC_X6.push_back(clock);
	    TriggerMap |= 1<<6;
	    break;
	  case 15:
	    A_MWPC_Y1.push_back(adc.measure);
	    T_MWPC_Y1.push_back(clock);
	    TriggerMap |= 1<<7;
	    break;
	  case 16:
	    A_MWPC_Y2.push_back(adc.measure);
	    T_MWPC_Y2.push_back(clock);
	    TriggerMap |= 1<<7;
	    break;
	  case 17:
	    A_MWPC_Y3.push_back(adc.measure);
	    T_MWPC_Y3.push_back(clock);
	    TriggerMap |= 1<<7;
	    break;
	  case 18:
	    A_MWPC_Y4.push_back(adc.measure);
	    T_MWPC_Y4.push_back(clock);
	    TriggerMap |= 1<<7;
	    break;
	  case 19:
	    A_MWPC_Y5.push_back(adc.measure);
	    T_MWPC_Y5.push_back(clock);
	    TriggerMap |= 1<<7;
	    break;
	  case 20:
	    A_MWPC_Y6.push_back(adc.measure);
	    T_MWPC_Y6.push_back(clock);
	    TriggerMap |= 1<<7;
	    break;
	  case 21:
	    A_MWPC_anode1.push_back(adc.measure);
	    T_MWPC_anode1.push_back(clock);
	    TriggerMap |= 1<<8;
	    break;
	  case 22:
	    A_MWPC_anode2.push_back(adc.measure);
	    T_MWPC_anode2.push_back(clock);
	    TriggerMap |= 1<<8;
	    break;
	  case 23:
	    A_LED.push_back(adc.measure);
	    T_LED.push_back(clock);
	    TriggerMap |= 1<<12;
            break;
	  case 24:
	    A_Tref.push_back(adc.measure);
	    T_Tref.push_back(clock);
	    TriggerMap |= 1;
            break;
	  default:
	    cout << "label = " << label << "  adc.measure = " << adc.measure << endl;
	    break;
	}
	break;
      default:
	cout << "alias = " << (int)alias << "label = " << label << endl;
	break;
    }
//    cout << "Lable: " <<label<<endl;
//    cin.get();
  }

  //Set Trigger Map bit 4
  if ((QX1.size()!=0)&&(QX2.size()!=0)&&(QY1.size()!=0)&&(QY2.size()!=0)&&(TX1.size()!=0)&&(TX2.size()!=0)&&(TY1.size()!=0)&&(TY2.size()!=0)){
    TriggerMap |= 1<<4;
  }
  // cout << TriggerMap << endl;
  //If the vector is empty, fill 0 in the first bin
  //TEMP.
  if (QMCP.size()==0){
    QMCP.push_back(0.0);
    TMCP.push_back(0.0);
  }
  if (QX1.size()==0){
    QX1.push_back(0.0);
    TX1.push_back(0.0);
  }
  if (QX2.size()==0){
    QX2.push_back(0.0);
    TX2.push_back(0.0);
  }
  if (QY1.size()==0){
    QY1.push_back(0.0);
    TY1.push_back(0.0);
  }
  if (QY2.size()==0){
    QY2.push_back(0.0);
    TY2.push_back(0.0);
  }
  if (A_Tref.size()==0){
    A_Tref.push_back(0.0);
    T_Tref.push_back(0.0);
  }
  if (QPMT_A[0].size()==0){
    QPMT_A[0].push_back(0.0);
    QPMT_A[1].push_back(0.0);
    TPMT_A.push_back(0.0);
  }
  if (QPMT_D[0].size()==0){
    QPMT_D[0].push_back(0.0);
    QPMT_D[1].push_back(0.0);
    TPMT_D.push_back(0.0);
  }
  if (A_LED.size()==0){
    A_LED.push_back(0.0);
    T_LED.push_back(0.0);
  }
  if (Q_N2Laser.size()==0){
    Q_N2Laser.push_back(0.0);
    T_N2Laser.push_back(0.0);
  }
  if (A_MWPC_anode1.size()==0){
    A_MWPC_anode1.push_back(0.0);;
    T_MWPC_anode1.push_back(0.0);;
  }
  if (A_MWPC_anode2.size()==0){
    A_MWPC_anode2.push_back(0.0);;
    T_MWPC_anode2.push_back(0.0);;
  }
  if (A_MWPC_X1.size()==0){
    A_MWPC_X1.push_back(0.0);
    T_MWPC_X1.push_back(0.0);
  }
  if (A_MWPC_X2.size()==0){
    A_MWPC_X2.push_back(0.0);
    T_MWPC_X2.push_back(0.0);
  }
  if (A_MWPC_X3.size()==0){
    A_MWPC_X3.push_back(0.0);
    T_MWPC_X3.push_back(0.0);
  }
  if (A_MWPC_X4.size()==0){
    A_MWPC_X4.push_back(0.0);
    T_MWPC_X4.push_back(0.0);
  }
  if (A_MWPC_X5.size()==0){
    A_MWPC_X5.push_back(0.0);
    T_MWPC_X5.push_back(0.0);
  }
  if (A_MWPC_X6.size()==0){
    A_MWPC_X6.push_back(0.0);
    T_MWPC_X6.push_back(0.0);
  }
  if (A_MWPC_Y1.size()==0){
    A_MWPC_Y1.push_back(0.0);
    T_MWPC_Y1.push_back(0.0);
  }
  if (A_MWPC_Y2.size()==0){
    A_MWPC_Y2.push_back(0.0);
    T_MWPC_Y2.push_back(0.0);
  }
  if (A_MWPC_Y3.size()==0){
    A_MWPC_Y3.push_back(0.0);
    T_MWPC_Y3.push_back(0.0);
  }
  if (A_MWPC_Y4.size()==0){
    A_MWPC_Y4.push_back(0.0);
    T_MWPC_Y4.push_back(0.0);
  }
  if (A_MWPC_Y5.size()==0){
    A_MWPC_Y5.push_back(0.0);
    T_MWPC_Y5.push_back(0.0);
  }
  if (A_MWPC_Y6.size()==0){
    A_MWPC_Y6.push_back(0.0);
    T_MWPC_Y6.push_back(0.0);
  }

/*  //Return coincidence types
  if ( QMCP!=0 && QPMT_A!=0){
    if ((QX1!=0)&&(QX2!=0)&&(QY1!=0)&&(QY2!=0)&&(TX1!=0)&&(TX2!=0)&&(TY1!=0)&&(TY2!=0)){
      return Full;
    }else{
      return IonAnodeMissing;
    }
  }else if (QMCP!=0 && QPMT_A==0){
    if ((QX1!=0)&&(QX2!=0)&&(QY1!=0)&&(QY2!=0)&&(TX1!=0)&&(TX2!=0)&&(TY1!=0)&&(TY2!=0)){
      return IonTrigger;
    }else{
      return IonAnodeMissing_no_Beta;
    }
  }else{
    return Else;
  }*/
  return 0;
}

//GetVal functions
void Data_Reader::GetVal_Tref(double &Tref_Val)
{
  Tref_Val = A_Tref[0];
}

void Data_Reader::GetVal_Scint(double *PMT_A_Val,double *PMT_D_Val)
{
  //Anode and Dynodes are in different channel, make sure there is no seg-fault if triggered individually
  if (QPMT_A[0].size()==0)QPMT_A[0].push_back(0.0);
  if (QPMT_D[0].size()==0)QPMT_D[0].push_back(0.0);
  if (QPMT_A[1].size()==0)QPMT_A[1].push_back(0.0);
  if (QPMT_D[1].size()==0)QPMT_D[1].push_back(0.0);
  for (int i=0;i<2;i++){
    PMT_A_Val[i] = QPMT_A[i][0];
    PMT_D_Val[i] = QPMT_D[i][0];
  }
}

void Data_Reader::GetVal_MWPC_anode(double &Anode_Val1,double &Anode_Val2)
{
  Anode_Val1 = A_MWPC_anode1[0];
  Anode_Val2 = A_MWPC_anode2[0];
}

void Data_Reader::GetVal_MWPC_X(double &X1,double &X2,double &X3,double &X4,double &X5,double &X6)
{
  X1 =  A_MWPC_X1[0];
  X2 =  A_MWPC_X2[0];
  X3 =  A_MWPC_X3[0];
  X4 =  A_MWPC_X4[0];
  X5 =  A_MWPC_X5[0];
  X6 =  A_MWPC_X6[0];
}

void Data_Reader::GetVal_MWPC_Y(double &Y1,double &Y2,double &Y3,double &Y4,double &Y5,double &Y6)
{
  Y1 =  A_MWPC_Y1[0];
  Y2 =  A_MWPC_Y2[0];
  Y3 =  A_MWPC_Y3[0];
  Y4 =  A_MWPC_Y4[0];
  Y5 =  A_MWPC_Y5[0];
  Y6 =  A_MWPC_Y6[0];
}

void Data_Reader::GetVal_MCP(double &MCP_Val)
{
  MCP_Val = QMCP[0];
}

void Data_Reader::GetVal_MCP_anodes(double &X1,double &X2,double &Y1,double &Y2)
{
  X1 = QX1[0];
  X2 = QX2[0];
  Y1 = QY1[0];
  Y2 = QY2[0];
}

void Data_Reader::GetVal_N2Laser(double &N2Laser_Val)
{
  N2Laser_Val= Q_N2Laser[0];
}

void Data_Reader::GetVal_LED(double &LED_Val)
{
  LED_Val = A_LED[0];
}

//GetTime functions
void Data_Reader::GetTime_Tref(double &Tref_Time)
{
  Tref_Time = T_Tref[0];
}

void Data_Reader::GetTime_Scint(long double &PMT_A_Time,long double &PMT_D_Time)
{
  if (TPMT_A.size()==0)TPMT_A.push_back(0.0);
  if (TPMT_D.size()==0)TPMT_D.push_back(0.0);
  PMT_A_Time = TPMT_A[0];
  PMT_D_Time = TPMT_D[0];
}

void Data_Reader::GetTime_MWPC_anode(double &Anode_Time1,double &Anode_Time2)
{
  Anode_Time1 = T_MWPC_anode1[0];
  Anode_Time2 = T_MWPC_anode2[0];
}

void Data_Reader::GetTime_MWPC_X(double &TX1,double &TX2,double &TX3,double &TX4,double &TX5,double &TX6)
{
  TX1 = T_MWPC_X1[0];
  TX2 = T_MWPC_X2[0];
  TX3 = T_MWPC_X3[0];
  TX4 = T_MWPC_X4[0];
  TX5 = T_MWPC_X5[0];
  TX6 = T_MWPC_X6[0];
}

void Data_Reader::GetTime_MWPC_Y(double &TY1,double &TY2,double &TY3,double &TY4,double &TY5,double &TY6)
{
  TY1 = T_MWPC_Y1[0];
  TY2 = T_MWPC_Y2[0];
  TY3 = T_MWPC_Y3[0];
  TY4 = T_MWPC_Y4[0];
  TY5 = T_MWPC_Y5[0];
  TY6 = T_MWPC_Y6[0];
}

void Data_Reader::GetTime_MCP(long double &MCP_Time)
{
  MCP_Time = TMCP[0];
}

void Data_Reader::GetTime_MCP_anodes(long double &X1_Time,long double &X2_Time,long double &Y1_Time,long double &Y2_Time)
{
  X1_Time = TX1[0];
  X2_Time = TX2[0];
  Y1_Time = TY1[0];
  Y2_Time = TY2[0];
}

void Data_Reader::GetTime_N2Laser(long double &N2Laser_Time)
{
  N2Laser_Time = T_N2Laser[0];
}

void Data_Reader::GetTime_LED(double &LED_Time)
{
  LED_Time = T_LED[0];
}

