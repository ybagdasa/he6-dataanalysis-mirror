/*
 * =====================================================================================
 *
 *       Filename:  MergeTrees.cxx
 *
 *    Description:  merging trees from different root files
 *
 *        Version:  1.0
 *        Created:  02/24/2015 12:12:09
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Ran Hong 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>

#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TString.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TRandom.h"

using namespace std;

int main (int argc, char** argv) {
  string inputfile;
  string treename;
  char filenamebuf[200];
  char outfilenamebuf[200];
  int startID;
  int NFile;
  int outID;

  if (argc < 4) {   //  command args & usage
    printf ("usage : \n");
    printf ("        %s TreeName filename_prefix OutputIndex\n", argv[0]);
    printf ("\n");
    return 1;
  }
  treename = argv[1];
  inputfile = argv[2];
//  startID = atoi(argv[3]);
//  NFile = atoi(argv[4]);
  outID = atoi(argv[3]);

  //Get Exp Data directory from environment
  char *ExpDataDirect;
  ExpDataDirect=getenv("HE6_EXPERIMENT_DATA_DIRECTORY");
  if (ExpDataDirect==NULL){
    cout << "The environment variable HE6_EXPERIMENT_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }

  TChain ch(treename.c_str());

  sprintf(filenamebuf,"%s/%s_????.root",ExpDataDirect,inputfile.c_str());
  int fileNumber = ch.Add(filenamebuf);

  sprintf(outfilenamebuf,"%s/%s_%s_%04d.root",ExpDataDirect,inputfile.c_str(),treename.c_str(),outID);
  ch.Merge(outfilenamebuf);
  cout <<fileNumber << " of files merged."<<endl;

  return 0;
}

