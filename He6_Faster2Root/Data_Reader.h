/*Read Data from .fast file, need the main function to steer and save data to root trees*/

#ifndef DATA_READER_H
#define DATA_READER_H
#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>

#include "TRandom3.h"

#include "fasterac/fasterac.h"
#include "fasterac/fast_data.h"
#include "fasterac/qdc_caras.h"
#include "fasterac/adc_caras.h"

using namespace std;

/*enum CoincidenceType{
  Full,			//Full coincidence: PMT and MWPC_Anode and MCP+Delay_Anodes, MWPC_Cathodes are not required
  BetaTrigger,		//PMT and MWPC_Anode, MWPC_Cathodes are not all required
  MWPCTrigger,		//MWPC_Anode only, MWPC_Cathodes are not all required
  IonTrigger,		//MCP and Dely_Anodes
  IonAnodeMissing,	//One of the Delay_Anodes is missing, MCP and PMT both trigger
  IonAnodeMissing_no_Beta,	//One of the Delay_Anodes is missing, no beta trigger
  Single,		//Isolated events
  Else
};*/

enum ReadOutType{
  TYPEGroup,	//grouped events
  TYPEPMT_A,	//PMT Anode single
  TYPEPMT_D,	//PMT Dynode single
  TYPEMWPC_anode,	//MWPC_anode single
  TYPEMWPC_X,	//any cathode strip from MWPC_X
  TYPEMWPC_Y,	//any cathode strip from MWPC_Y
  TYPEMCP,		//MCP
  TYPEMCP_anodes,	//any from Delay-line anodes
  TYPET_ref,	//Time reference
  TYPELED,	//LED Calibration
  TYPEN2LASER,	//N@LASER qdc
  TYPEElse,		//other types: counter, samples, which are not included now
  TYPEEof,		//End of file
  TYPEERROR,		//Error
  TYPEIGNORE	//Ignore
};

class Data_Reader{
  private:
    faster_file_reader_p  reader;                                //  data file reader
    faster_data_p         data;                                  //  data pointer
    //MCP QDCs
    vector <double> QMCP;
    vector <double> QX1;
    vector <double> QX2;
    vector <double> QY1;
    vector <double> QY2;
    vector <long double> TMCP;
    vector <long double> TX1;
    vector <long double> TX2;
//    double TX1_low, TX1_high, TMCP_low, TMCP_high;
    vector <long double> TY1;
    vector <long double> TY2;
    //Tref ADC
    vector <double> A_Tref;
    vector <double> T_Tref;
    //Scintillator-PMT QDC
    vector <double> QPMT_A[2];
    vector <long double> TPMT_A;
    vector <double> QPMT_D[2];
    vector <long double> TPMT_D;
    vector <double> A_LED;
    vector <double> T_LED;
    vector <double> Q_N2Laser;
    vector <long double> T_N2Laser;
    //MWPC ADCs
    vector <double> A_MWPC_anode1;
    vector <double> T_MWPC_anode1;
    vector <double> A_MWPC_anode2;
    vector <double> T_MWPC_anode2;
    vector <double> A_MWPC_X1;
    vector <double> A_MWPC_X2;
    vector <double> A_MWPC_X3;
    vector <double> A_MWPC_X4;
    vector <double> A_MWPC_X5;
    vector <double> A_MWPC_X6;
    vector <double> A_MWPC_Y1;
    vector <double> A_MWPC_Y2;
    vector <double> A_MWPC_Y3;
    vector <double> A_MWPC_Y4;
    vector <double> A_MWPC_Y5;
    vector <double> A_MWPC_Y6;
    vector <double> T_MWPC_X1;
    vector <double> T_MWPC_X2;
    vector <double> T_MWPC_X3;
    vector <double> T_MWPC_X4;
    vector <double> T_MWPC_X5;
    vector <double> T_MWPC_X6;
    vector <double> T_MWPC_Y1;
    vector <double> T_MWPC_Y2;
    vector <double> T_MWPC_Y3;
    vector <double> T_MWPC_Y4;
    vector <double> T_MWPC_Y5;
    vector <double> T_MWPC_Y6;
    //Group Time
    double GroupTriggerTime;
    //Trigger Map
    unsigned short TriggerMap;
    TRandom3 randnum;
    double dt; //time smoothing interval [sec]
    //private function: group reading
    int Read_From_Group_Buffer(faster_buffer_reader_p group_reader);
  public:
    Data_Reader();
    ~Data_Reader();
    int OpenInputFile(string input_file);
    int closeInputFile();
    void Reset();
    int Read_From_File();	//return coincidence type

    double GetGroupTriggerTime(){return GroupTriggerTime;}
    unsigned short GetTriggerMap(){return TriggerMap;}

    //GetVal functions
    void GetVal_Tref(double &Tref_Val);
    void GetVal_Scint(double *PMT_A_Val,double *PMT_D_Val);
    void GetVal_MWPC_anode(double &Anode_Val1,double &Anode_Val2);
    void GetVal_MWPC_X(double &X1,double &X2,double &X3,double &X4,double &X5,double &X6);
    void GetVal_MWPC_Y(double &Y1,double &Y2,double &Y3,double &Y4,double &Y5,double &Y6);
    void GetVal_MCP(double &MCP_Val);
    void GetVal_MCP_anodes(double &X1,double &X2,double &Y1,double &Y2);
    void GetVal_N2Laser(double &N2Laser_Val);
    void GetVal_LED(double &LED_Val);

    //GetTime functions
    void GetTime_Tref(double &Tref_Time);
    void GetTime_Scint(long double &PMT_A_Time,long double &PMT_D_Time);
    void GetTime_MWPC_anode(double &Anode_Time1,double &Anode_Time2);
    void GetTime_MWPC_X(double &TX1,double &TX2,double &TX3,double &TX4,double &TX5,double &TX6);
    void GetTime_MWPC_Y(double &TY1,double &TY2,double &TY3,double &TY4,double &TY5,double &TY6);
    void GetTime_MCP(long double &MCP_Time);
    void GetTime_MCP_anodes(long double &X1_Time,long double &X2_Time,long double &Y1_Time,long double &Y2_Time);
//    void GetTime_MCP_anodes_FullRes(double &X1_Time_low,double &X1_Time_high);
//    void GetTime_MCP_FullRes(double &MCP_Time_low, double &MCP_Time_high);
    void GetTime_N2Laser(long double &N2Laser_Time);
    void GetTime_LED(double &LED_Time);
};

#endif
