//      Ran Hong
//      Aug 13th 2013
//			April 14th modification by Xavier & Andreas
//
//	Convert .fast file into a root Tree within a root file
//	usage:
//	make He6_Faster2Root
//	./He6_Faster2Root filename_prefix(no number or .fast) first_file_number number_of_file outputfile_prefix
//
// 	adjust nmax if only a sample is required
//      Version 5.0
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>

#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TRandom.h"

#include "Data_Reader.h"

const double nmax= 10000000000;
//const double nmax= 10000;
using namespace std;

//Define data structures


typedef struct DataStruct_MWPC_X{
  double AX1;
  double AX2;
  double AX3;
  double AX4;
  double AX5;
  double AX6;
}DataStruct_MWPC_X;

typedef struct DataStruct_MWPC_Y{
  double AY1;
  double AY2;
  double AY3;
  double AY4;
  double AY5;
  double AY6;
}DataStruct_MWPC_Y;

typedef struct DataStruct_TMWPC_X{
  double MW_TX1;
  double MW_TX2;
  double MW_TX3;
  double MW_TX4;
  double MW_TX5;
  double MW_TX6;
}DataStruct_TMWPC_X;

typedef struct DataStruct_TMWPC_Y{
  double MW_TY1;
  double MW_TY2;
  double MW_TY3;
  double MW_TY4;
  double MW_TY5;
  double MW_TY6;
}DataStruct_TMWPC_Y;

typedef struct DataStruct_QMCP_anodes{
  double QX1;
  double QX2;
  double QY1;
  double QY2;
}DataStruct_QMCP_anodes;

typedef struct DataStruct_TMCP_anodes{
  double TX1;
  double TX2;
  double TY1;
  double TY2;
}DataStruct_TMCP_anodes;



int main (int argc, char** argv) {
  string inputfile;
  string spectrumfile;
  char filenamebuf[200];
  int NFile;
  int FirstFile;                           
  //  double TMCP_ref;	//  Time ref for first event
  int ReadOutType;

  int n = 0;  //  nb of data read
  int nk = 0; //  nb of data kept

  double Tref = 0;
  double LastTref = 0;
  double Tref_A = 0;
  double QPMT_A[2];
  double TPMT_A_rel;
  double QPMT_D[2];
  double TPMT_DA;
  double AMWPC_anode1;
  double AMWPC_anode2;
  double TMWPC_rel[2];
  double TBeta_diff[2];
  double LED_A;
  double T_LED;
  double T_LED_PMT;
  DataStruct_MWPC_X MWPC_X;
  DataStruct_TMWPC_X TMWPC_X;
  DataStruct_MWPC_Y MWPC_Y;
  DataStruct_TMWPC_Y TMWPC_Y;
  double QMCP;
  DataStruct_QMCP_anodes QMCP_anodes;
  DataStruct_TMCP_anodes TMCP_anodes;
  double TMCP_rel;
  double TOF;
  double Q_N2Laser;
  long double T_N2Laser;
  double T_N2Laser_lr;
  double TOF_N2Laser;
  double TOFMM[2];

  double GroupTime;
  vector<double> GroupTimeArray;

  unsigned short TriggerMap;

  long double TPMT_A,TPMT_D,TX1,TX2,TY1,TY2,TMCP;
  double TMWPC_anode1;
  double TMWPC_anode2;
  double TMWPC_X1,TMWPC_X2,TMWPC_X3,TMWPC_X4,TMWPC_X5,TMWPC_X6;
  double TMWPC_Y1,TMWPC_Y2,TMWPC_Y3,TMWPC_Y4,TMWPC_Y5,TMWPC_Y6;

  double TX1mTX2,TX1pTX2,TY1mTY2,TY1pTY2;
  double TMWPCX_rel,TMWPCY_rel,TMCP_anodes_rel;
  double MWPC_PosX,MWPC_PosY,MWPC_ChargeX,MWPC_ChargeY;
  double AnodePos;

  if (argc < 5) {   //  command args & usage
    printf ("usage : \n");
    printf ("        %s  filename_prefix First_file_number Number_of_files outputname_prefix\n", argv[0]);
    printf ("\n");
    return EXIT_SUCCESS;
  }

  //Logging to file
  ofstream logfile;
  logfile.open("ConversionLog.log",ios::app);
  //Get Exp Data directory from environment
  char *ExpDataDirect;
  ExpDataDirect=getenv("HE6_EXPERIMENT_DATA_DIRECTORY");
  if (ExpDataDirect==NULL){
    cout << "The environment variable HE6_EXPERIMENT_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
  //Set up output trees, histograms and file
  TString fName = string(ExpDataDirect) + string("/") + string(argv[4]);
  //  fName.ReplaceAll (".fast", ".root");
  fName += ".root";

  // output root file
  TFile *f = new TFile (fName.Data(), "recreate");

  // root tree
  TTree *Tree_Tref = new TTree ("Tree_Tref", "Time reference");
  TTree *Tree_Scint = new TTree ("Tree_Scint", "Scintillator single event");
  TTree *Tree_MWPC = new TTree ("Tree_MWPC", "MWPC single event (grouped)");	//Exclude Scint event
  TTree *Tree_MCP = new TTree ("Tree_MCP", "MCP single event (grouped)");	//Exclude double trigger from (Scint and MWPC) and tripple trigger
  TTree *Tree_Beta = new TTree ("Tree_Beta", "Beta coincidence event: Scint+MWPC");	//Exclude tripple trigger
  TTree *Tree_Double = new TTree ("Tree_Double", "Double trigger event: Scint+MCP");	//MCP anodes may or may not all triggers, info in trigger-map
  TTree *Tree_DoubleMM = new TTree ("Tree_DoubleMM", "Double trigger event: MWPC+MCP");	//MCP anodes may or may not all triggers, info in trigger-map
  TTree *Tree_Triple = new TTree ("Tree_Triple", "Triple trigger event: Scint+MWPC+MCP");	//MCP anodes may or may not all triggers, info in trigger-map

  TTree *Tree_MWPC_Iso = new TTree ("Tree_MWPC_Iso", "Isolated MWPC anode");
  TTree *Tree_MWPCX_Iso = new TTree ("Tree_MWPCX_Iso", "Isolated MWPCX cathode");
  TTree *Tree_MWPCY_Iso = new TTree ("Tree_MWPCY_Iso", "Isolated MWPCY cathode");
  TTree *Tree_MCP_Iso = new TTree ("Tree_MCP_Iso", "Isolated MCP");
  TTree *Tree_MCP_anode_Iso = new TTree ("Tree_MCP_anode_Iso", "Isolated MCP anodes");
  
  //Trees for calibration
  TTree *Tree_PMT_STB = new TTree ("Tree_PMT_STB", "PMT STB Calibration");	//LED calibration of PMT stability
  TTree *Tree_N2Laser = new TTree ("Tree_N2Laser", "N2 Laser Calibration");	//N2 Laser calibration 

  // Branches  
  Tree_Tref->Branch("Event_No",&n,"N/I");
  Tree_Tref->Branch("Tref",&Tref,"Tref/D");
  Tree_Tref->Branch("Tref_A",&Tref_A,"Tref_A/D");

  Tree_Scint->Branch("Event_No",&n,"N/I");
  Tree_Scint->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_Scint->Branch("QPMT_A",&QPMT_A,"QPMT_A1/D:QPMT_A2");
  Tree_Scint->Branch("QPMT_D",&QPMT_D,"QPMT_D1/D:QPMT_D2");
  Tree_Scint->Branch("TPMT_A_rel",&TPMT_A_rel,"TPMT_A_rel/D");
  Tree_Scint->Branch("TPMT_DA",&TPMT_DA,"TPMT_DA/D");
  Tree_Scint->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  Tree_MWPC->Branch("Event_No",&n,"N/I");  
  Tree_MWPC->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_MWPC->Branch("AMWPC_anode1",&AMWPC_anode1,"AMWPC_anode1/D");
  Tree_MWPC->Branch("AMWPC_anode2",&AMWPC_anode2,"AMWPC_anode2/D");
  Tree_MWPC->Branch("TMWPC_rel",&TMWPC_rel,"TMWPC_rel1/D:TMWPC_rel2");
  Tree_MWPC->Branch("MWPC_X",&MWPC_X,"AX1/D:AX2:AX3:AX4:AX5:AX6");
  Tree_MWPC->Branch("TMWPC_X",&TMWPC_X,"MW_TX1/D:MW_TX2:MW_TX3:MW_TX4:MW_TX5:MW_TX6");
  Tree_MWPC->Branch("MWPC_Y",&MWPC_Y,"AY1/D:AY2:AY3:AY4:AY5:AY6");
  Tree_MWPC->Branch("TMWPC_Y",&TMWPC_Y,"MW_TY1/D:MW_TY2:MW_TY3:MW_TY4:MW_TX5:MW_TX6");
  Tree_MWPC->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  Tree_MCP->Branch("Event_No",&n,"N/I");
  Tree_MCP->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_MCP->Branch("TMCP_rel",&TMCP_rel,"TMCP_rel/D");
  Tree_MCP->Branch("QMCP",&QMCP,"QMCP/D");
  Tree_MCP->Branch("QMCP_anodes",&QMCP_anodes,"QX1/D:QX2:QY1:QY2");
  Tree_MCP->Branch("TMCP_anodes",&TMCP_anodes,"TX1/D:TX2:TY1:TY2");
  Tree_MCP->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  Tree_Beta->Branch("Event_No",&n,"N/I");
  Tree_Beta->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_Beta->Branch("QPMT_A",&QPMT_A,"QPMT_A1/D:QPMT_A2");
  Tree_Beta->Branch("QPMT_D",&QPMT_D,"QPMT_D1/D:QPMT_D2");
  Tree_Beta->Branch("TPMT_A_rel",&TPMT_A_rel,"TPMT_A_rel/D");
  Tree_Beta->Branch("TPMT_DA",&TPMT_DA,"TPMT_DA/D");
  Tree_Beta->Branch("AMWPC_anode1",&AMWPC_anode1,"AMWPC_anode1/D");
  Tree_Beta->Branch("AMWPC_anode2",&AMWPC_anode2,"AMWPC_anode2/D");
  Tree_Beta->Branch("TBeta_diff",&TBeta_diff,"TBeta_diff1/D:TBeta_diff2");
  Tree_Beta->Branch("MWPC_X",&MWPC_X,"AX1/D:AX2:AX3:AX4:AX5:AX6");
  Tree_Beta->Branch("TMWPC_X",&TMWPC_X,"MW_TX1/D:MW_TX2:MW_TX3:MW_TX4:MW_TX5:MW_TX6");
  Tree_Beta->Branch("MWPC_Y",&MWPC_Y,"AY1/D:AY2:AY3:AY4:AY5:AY6");
  Tree_Beta->Branch("TMWPC_Y",&TMWPC_Y,"MW_TY1/D:MW_TY2:MW_TY3:MW_TY4:MW_TX5:MW_TX6");
  Tree_Beta->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  Tree_Double->Branch("Event_No",&n,"N/I");
  Tree_Double->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_Double->Branch("QPMT_A",&QPMT_A,"QPMT_A1/D:QPMT_A2");
  Tree_Double->Branch("QPMT_D",&QPMT_D,"QPMT_D1/D:QPMT_D2");
  Tree_Double->Branch("TPMT_A_rel",&TPMT_A_rel,"TPMT_A_rel/D");
  Tree_Double->Branch("TPMT_DA",&TPMT_DA,"TPMT_DA/D");
  Tree_Double->Branch("TMCP_rel",&TMCP_rel,"TMCP_rel/D");
  Tree_Double->Branch("QMCP",&QMCP,"QMCP/D");
  Tree_Double->Branch("QMCP_anodes",&QMCP_anodes,"QX1/D:QX2:QY1:QY2");
  Tree_Double->Branch("TMCP_anodes",&TMCP_anodes,"TX1/D:TX2:TY1:TY2");
  Tree_Double->Branch("TOF",&TOF,"TOF/D");
  Tree_Double->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  Tree_DoubleMM->Branch("Event_No",&n,"N/I");
  Tree_DoubleMM->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_DoubleMM->Branch("AMWPC_anode1",&AMWPC_anode1,"AMWPC_anode1/D");
  Tree_DoubleMM->Branch("AMWPC_anode2",&AMWPC_anode2,"AMWPC_anode2/D");
  Tree_DoubleMM->Branch("TMWPC_rel",&TMWPC_rel,"TMWPC_rel1/D:TMWPC_rel2");
  Tree_DoubleMM->Branch("MWPC_X",&MWPC_X,"AX1/D:AX2:AX3:AX4:AX5:AX6");
  Tree_DoubleMM->Branch("TMWPC_X",&TMWPC_X,"MW_TX1/D:MW_TX2:MW_TX3:MW_TX4:MW_TX5:MW_TX6");
  Tree_DoubleMM->Branch("MWPC_Y",&MWPC_Y,"AY1/D:AY2:AY3:AY4:AY5:AY6");
  Tree_DoubleMM->Branch("TMWPC_Y",&TMWPC_Y,"MW_TY1/D:MW_TY2:MW_TY3:MW_TY4:MW_TX5:MW_TX6");
  Tree_DoubleMM->Branch("TMCP_rel",&TMCP_rel,"TMCP_rel/D");
  Tree_DoubleMM->Branch("QMCP",&QMCP,"QMCP/D");
  Tree_DoubleMM->Branch("QMCP_anodes",&QMCP_anodes,"QX1/D:QX2:QY1:QY2");
  Tree_DoubleMM->Branch("TMCP_anodes",&TMCP_anodes,"TX1/D:TX2:TY1:TY2");
  Tree_DoubleMM->Branch("TOFMM",&TOFMM,"TOF1/D:TOF2");
  Tree_DoubleMM->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  Tree_Triple->Branch("Event_No",&n,"N/I");
  Tree_Triple->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_Triple->Branch("QPMT_A",&QPMT_A,"QPMT_A1/D:QPMT_A2");
  Tree_Triple->Branch("QPMT_D",&QPMT_D,"QPMT_D1/D:QPMT_D2");
  Tree_Triple->Branch("TPMT_A_rel",&TPMT_A_rel,"TPMT_A_rel/D");
  Tree_Triple->Branch("TPMT_DA",&TPMT_DA,"TPMT_DA/D");
  Tree_Triple->Branch("AMWPC_anode1",&AMWPC_anode1,"AMWPC_anode1/D");
  Tree_Triple->Branch("AMWPC_anode2",&AMWPC_anode2,"AMWPC_anode2/D");
  Tree_Triple->Branch("TBeta_diff",&TBeta_diff,"TBeta_diff1/D:TBeta_diff2");
  Tree_Triple->Branch("MWPC_X",&MWPC_X,"AX1/D:AX2:AX3:AX4:AX5:AX6");
  Tree_Triple->Branch("TMWPC_X",&TMWPC_X,"MW_TX1/D:MW_TX2:MW_TX3:MW_TX4:MW_TX5:MW_TX6");
  Tree_Triple->Branch("MWPC_Y",&MWPC_Y,"AY1/D:AY2:AY3:AY4:AY5:AY6");
  Tree_Triple->Branch("TMWPC_Y",&TMWPC_Y,"MW_TY1/D:MW_TY2:MW_TY3:MW_TY4:MW_TX5:MW_TX6");
  Tree_Triple->Branch("TMCP_rel",&TMCP_rel,"TMCP_rel/D");
  Tree_Triple->Branch("QMCP",&QMCP,"QMCP/D");
  Tree_Triple->Branch("QMCP_anodes",&QMCP_anodes,"QX1/D:QX2:QY1:QY2");
  Tree_Triple->Branch("TMCP_anodes",&TMCP_anodes,"TX1/D:TX2:TY1:TY2");
  Tree_Triple->Branch("TOF",&TOF,"TOF/D");
  Tree_Triple->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  Tree_MWPC_Iso->Branch("Event_No",&n,"N/I");  
  Tree_MWPC_Iso->Branch("AMWPC_anode1",&AMWPC_anode1,"AMWPC_anode1/D");
  Tree_MWPC_Iso->Branch("AMWPC_anode2",&AMWPC_anode2,"AMWPC_anode2/D");
  Tree_MWPC_Iso->Branch("TMWPC_rel",&TMWPC_rel,"TMWPC_rel1/D:TMWPC_rel2");

  Tree_MWPCX_Iso->Branch("Event_No",&n,"N/I");
  Tree_MWPCX_Iso->Branch("MWPC_X",&MWPC_X,"AX1/D:AX2:AX3:AX4");
  Tree_MWPCX_Iso->Branch("TMWPCX_rel",&TMWPCX_rel,"TMWPCX_rel/D");

  Tree_MWPCY_Iso->Branch("Event_No",&n,"N/I");
  Tree_MWPCY_Iso->Branch("MWPC_Y",&MWPC_Y,"AY1/D:AY2:AY3:AY4");
  Tree_MWPCY_Iso->Branch("TMWPCY_rel",&TMWPCY_rel,"TMWPCY_rel/D");

  Tree_MCP_Iso->Branch("Event_No",&n,"N/I");
  Tree_MCP_Iso->Branch("QMCP",&QMCP,"QMCP/D");
  Tree_MCP_Iso->Branch("TMCP_rel",&TMCP_rel,"TMCP_rel/D");

  Tree_MCP_anode_Iso->Branch("Event_No",&n,"N/I");
  Tree_MCP_anode_Iso->Branch("QMCP_anodes",&QMCP_anodes,"QX1/D:QX2:QY1:QY2");
  Tree_MCP_anode_Iso->Branch("TMCP_anodes_rel",&TMCP_anodes_rel,"TMCP_anodes_rel/D");

  Tree_PMT_STB->Branch("LED",&LED_A,"LED/D");
  Tree_PMT_STB->Branch("LED_T",&T_LED,"LED_T/D");
  Tree_PMT_STB->Branch("Scint",&QPMT_A,"QPMT_A1/D:QPMT_A2");
  Tree_PMT_STB->Branch("Timing",&T_LED_PMT,"Timing/D");

  Tree_N2Laser->Branch("GroupTime",&GroupTime,"GroupTime/D");
  Tree_N2Laser->Branch("QN2Laser",&Q_N2Laser,"QN2Laser/D");
  Tree_N2Laser->Branch("TN2Laser",&T_N2Laser_lr,"TN2Laser/D");
  Tree_N2Laser->Branch("QMCP",&QMCP,"QMCP/D");
  Tree_N2Laser->Branch("TMCP_rel",&TMCP_rel,"TMCP_rel/D");
  Tree_N2Laser->Branch("TOFN2Laser",&TOF_N2Laser,"TOFN2Laser/D");
  Tree_N2Laser->Branch("QMCP_anodes",&QMCP_anodes,"QX1/D:QX2:QY1:QY2");
  Tree_N2Laser->Branch("TMCP_anodes",&TMCP_anodes,"TX1/D:TX2:TY1:TY2");
  Tree_N2Laser->Branch("TriggerMap",&TriggerMap,"TriggerMap/s");

  // root histos
  TH1D *h_Tref = new TH1D("h_Tref","h_Tref interval",120,-60,60);
  TH1D *h_TrefA = new TH1D("h_TrefA","h_TrefA",100,-10,900);
//MCP histos
  TH2F *h_IMAGE = new TH2F("h_IMAGE","h_IMAGE",200,-100,100,200,-100,100);
  TH1F *h_X = new TH1F("h_X","h_X",1000,-100,100);
  TH1F *h_Y = new TH1F("h_Y","h_Y",1000,-100,100);
  TH1F *h_TX1pX2 = new TH1F("h_TX1pX2","h_TX1pX2",1000,0,200);
  TH1F *h_TY1pY2 = new TH1F("h_TY1pY2","h_TY1pY2",1000,0,200);
  TH1F *h_TMCP_rel = new TH1F("h_TMCP_rel","h_TMCP_rel",1600,-100,1500);
  TH2D *h_TX1vQX1 = new TH2D("h_TX1vQX1","TX1 vs QX1",1000,0,50000,1000,0,80);
  TH2D *h_TX2vQX2 = new TH2D("h_TX2vQX2","TX2 vs QX2",1000,0,50000,1000,0,80);
  TH2D *h_TY1vQY1 = new TH2D("h_TY1vQY1","TY1 vs QY1",1000,0,50000,1000,0,80);
  TH2D *h_TY2vQY2 = new TH2D("h_TY2vQY2","TY2 vs QY2",1000,0,50000,1000,0,80);
  TH2D *h_TX1vQX1_double = new TH2D("h_TX1vQX1_double","TX1 vs QX1 for Double Coin",1000,0,50000,1000,0,80);
  TH2D *h_TX2vQX2_double = new TH2D("h_TX2vQX2_double","TX2 vs QX2 for Double Coin",1000,0,50000,1000,0,80);
  TH2D *h_TY1vQY1_double = new TH2D("h_TY1vQY1_double","TY1 vs QY1 for Double Coin",1000,0,50000,1000,0,80);
  TH2D *h_TY2vQY2_double = new TH2D("h_TY2vQY2_double","TY2 vs QY2 for Double Coin",1000,0,50000,1000,0,80);
//MWPC Scint histos
  TH1D *h_Scint = new TH1D("h_Scint","Scint. alone spectrum",1024,0,2048000);
  TH1D *h_MWPC1  = new TH1D("h_MWPC1","MWPC Anode1 spectrum",1024,0,655360);
  TH1D *h_MWPC2  = new TH1D("h_MWPC2","MWPC Anode2 spectrum",1024,0,655360);
  TH1D *h_MWPC_tot  = new TH1D("h_MWPC_tot","MWPC Anode_tot spectrum",1024,0,655360);
  TH1D *h_Scint_Coin = new TH1D("h_Scint_Coin","Scint. coincidence spectrum",1024,0,2048000);
  TH1D *h_MWPC_Coin  = new TH1D("h_MWPC_Coin","MWPC Anode coincidence spectrum",1024,0,65536);
  TH2D *h_MWPC_Scint = new TH2D("h_MWPC_Scint","MWPC vs Scint 2D",1024,0,65536,1024,0,2048000);
//TOF histos
  TH1D *h_TOF = new TH1D("h_TOF","Time of Flight Spectrum",2400,-200,1000);
  TH2 *h_TOF_PMT = new TH2D("h_TOF_PMT","TOF vs PMT",2400,-200,1000,1024,0,2048000);

//MWPC histos
  TH1D *h_Cathode_X = new TH1D("h_Cathode_X","MWPC CathodeX position spectrum",100,-25,25);
  TH1D *h_Cathode_Y = new TH1D("h_Cathode_Y","MWPC CathodeY position spectrum",100,-25,25);
  TH1D *h_AnodeY = new TH1D("h_AnodeY","MWPC AnodeY position spectrum",100,-25,25);
  TH2D *h_Cathode_Image = new TH2D("h_Cathode_Image","2D image of MWPC",100,-25,25,100,-25,25);
  TH2D *h_Anode_Image = new TH2D("h_Anode_Image","2D Anode image of MWPC",100,-25,25,100,-25,25);
  TH2D *h_XCorrelation = new TH2D("h_XCorrelation","MWPC anode cathode x correlation",100,-25,25,100,-25,25);
  TH2D *h_Cathode_Image_Coin = new TH2D("h_Cathode_Image_Coin","2D image of MWPC in coincidence with Scint",100,-25,25,100,-25,25);
  TH1D *h_Charge_X = new TH1D("h_Charge_X","Total Charge on MWPC X",1024,0,51200);
  TH1D *h_Charge_Y = new TH1D("h_Charge_Y","Total Charge on MWPC Y",1024,0,51200);

  TH2D *h_ChargeX_vs_Anode = new TH2D("h_ChargeX_vs_Anode","ChargeX vs Anode",1024,0,51200,1024,0,65536);
  TH2D *h_ChargeY_vs_Anode = new TH2D("h_ChargeY_vs_Anode","ChargeY vs Anode",1024,0,51200,1024,0,65536);
  TH2D *h_ChargeX_vs_ChargeY = new TH2D("h_ChargeX_vs_ChargeY","ChargeX vs ChargeY",1024,0,204800,1024,0,204800);
  TH2D *h_TotCharge_vs_Anode = new TH2D("h_TotCharge_vs_Anode","Total Charge vs Anode",1024,0,204800,1024,0,65536);

  /////////////////////////////////////////////End Root Definition//////////////////////////////////////


  Data_Reader* He6_Data_Reader = new Data_Reader();	//Create a Data reader

  NFile = atoi(argv[3]);
	FirstFile= atoi(argv[2]);
  int EffEvent = 0;
  for (int ifile=FirstFile;ifile<NFile+FirstFile;ifile++){//File loop

    inputfile = string(ExpDataDirect) +string("/") + string(argv[1]);

    sprintf(filenamebuf,"_%04d.fast",ifile);
    inputfile += filenamebuf;

    if (He6_Data_Reader->OpenInputFile(inputfile) == -1) {
      printf ("error opening file %s\n",inputfile.c_str());
      return EXIT_FAILURE;
    }
    cout <<"Processing file" << inputfile.c_str()<<endl;
    //--------------------------------------------------//
    while (n<nmax) {  //  loop on data from file
      ReadOutType = He6_Data_Reader->Read_From_File();
      /*Check abnormals*******************************************/
      if (ReadOutType == TYPEEof){
	cout<<"End of file!\n";
	break;
      }else if(ReadOutType == TYPEERROR){
	cout<<"ERROR happened!\n";
	break;
      }
      /***********************************************************/

      if (n%100000==0) {
	cout<<n<<" read evts"<<endl;
	cout<<nk<<" kept evts"<<endl;
      }
      //--------------if complete event
      //Clean variables first
      QPMT_A[0]=QPMT_A[1]=TPMT_A_rel=QPMT_D[0]=QPMT_D[1]=TPMT_DA=AMWPC_anode1=AMWPC_anode2=0.0;
      TMWPC_rel[0]=TMWPC_rel[1]==0.0;
      TBeta_diff[0]=TBeta_diff[1]=0.0;
      MWPC_X.AX1=MWPC_X.AX2=MWPC_X.AX3=MWPC_X.AX4=MWPC_X.AX5=MWPC_X.AX6=0.0;
      TMWPC_X.MW_TX1=TMWPC_X.MW_TX2=TMWPC_X.MW_TX3=TMWPC_X.MW_TX4=TMWPC_X.MW_TX5=TMWPC_X.MW_TX6=0.0;
      MWPC_Y.AY1=MWPC_Y.AY2=MWPC_Y.AY3=MWPC_Y.AY4=MWPC_Y.AY5=MWPC_Y.AY6=0.0;
      TMWPC_Y.MW_TY1=TMWPC_Y.MW_TY2=TMWPC_Y.MW_TY3=TMWPC_Y.MW_TY4=TMWPC_Y.MW_TY5=TMWPC_Y.MW_TY6=0.0;
      QMCP=0.0;
      QMCP_anodes.QX1=QMCP_anodes.QX2=QMCP_anodes.QY1=QMCP_anodes.QY2=0.0;
      TMCP_anodes.TX1=TMCP_anodes.TX2=TMCP_anodes.TY1=TMCP_anodes.TY2=0.0;
      TMCP_rel=TOF=0.0;

      GroupTime=0.0;

      TPMT_A=TPMT_D=TMWPC_anode1=TMWPC_anode2=TX1=TX2=TY1=TY2=TMCP=0.0;
      TMWPC_X1=TMWPC_X2=TMWPC_X3=TMWPC_X4=0.0;
      TMWPC_Y1=TMWPC_Y2=TMWPC_Y3=TMWPC_Y4=0.0;

      TX1mTX2=TX1pTX2=TY1mTY2=TY1pTY2=0.0;
      TMWPCX_rel=TMWPCY_rel=TMCP_anodes_rel=0.0;

      LED_A=0.0;
      T_LED=0.0;
      T_LED_PMT=0.0;

      Q_N2Laser=0.0;
      T_N2Laser=0.0;
      T_N2Laser_lr=0.0;
      TOF_N2Laser=0.0;
      TOFMM[0]=TOFMM[1]=0.0;
      //Start sorting

      TriggerMap = He6_Data_Reader->GetTriggerMap();
      if (ReadOutType==TYPEGroup){
	nk++;
	GroupTime = He6_Data_Reader->GetGroupTriggerTime();
	GroupTimeArray.push_back(GroupTime);
	//Get data from Data_Reader
	if ((TriggerMap&1)!=0){
	  LastTref = Tref;
	  He6_Data_Reader->GetTime_Tref(Tref);
	  He6_Data_Reader->GetVal_Tref(Tref_A);
	  h_Tref->Fill(Tref-LastTref);
	  h_TrefA->Fill(Tref_A/100000);
	}
	if ((TriggerMap&(1<<1))!=0 || (TriggerMap&(1<<2))!=0){
	  He6_Data_Reader->GetTime_Scint(TPMT_A,TPMT_D);
	  He6_Data_Reader->GetVal_Scint(QPMT_A,QPMT_D);
	  if (Tref>0.001 && TPMT_A>0.0)TPMT_A_rel = (double(TPMT_A) - Tref)*1.e3;	//ms
	}
	if ((TriggerMap&(1<<3))!=0){
	  He6_Data_Reader->GetVal_MCP(QMCP);
	  He6_Data_Reader->GetTime_MCP(TMCP);
	  if (Tref>0.001)TMCP_rel = (double(TMCP) - Tref)*1.e3;	//ms
	}
	if ((TriggerMap&(1<<5))!=0){
	  He6_Data_Reader->GetVal_MCP_anodes(QMCP_anodes.QX1,QMCP_anodes.QX2,QMCP_anodes.QY1,QMCP_anodes.QY2);
	  He6_Data_Reader->GetTime_MCP_anodes(TX1,TX2,TY1,TY2);
	}
	if ((TriggerMap&(1<<6))!=0){
	  He6_Data_Reader->GetVal_MWPC_X(MWPC_X.AX1,MWPC_X.AX2,MWPC_X.AX3,MWPC_X.AX4,MWPC_X.AX5,MWPC_X.AX6);
	  He6_Data_Reader->GetTime_MWPC_X(TMWPC_X1,TMWPC_X2,TMWPC_X3,TMWPC_X4,TMWPC_X5,TMWPC_X6);
	  MWPC_ChargeX = MWPC_X.AX1+MWPC_X.AX2+MWPC_X.AX3+MWPC_X.AX4+MWPC_X.AX5+MWPC_X.AX6;
	  MWPC_PosX = (-20.*MWPC_X.AX1 -12.*MWPC_X.AX2-4.*MWPC_X.AX3+4.*MWPC_X.AX4+12.*MWPC_X.AX5+20.*MWPC_X.AX6)/MWPC_ChargeX;
	}
	if ((TriggerMap&(1<<7))!=0){
	  He6_Data_Reader->GetVal_MWPC_Y(MWPC_Y.AY1,MWPC_Y.AY2,MWPC_Y.AY3,MWPC_Y.AY4,MWPC_Y.AY5,MWPC_Y.AY6);
	  He6_Data_Reader->GetTime_MWPC_Y(TMWPC_Y1,TMWPC_Y2,TMWPC_Y3,TMWPC_Y4,TMWPC_Y5,TMWPC_Y6);
	  MWPC_ChargeY = MWPC_Y.AY1+MWPC_Y.AY2+MWPC_Y.AY3+MWPC_Y.AY4+MWPC_Y.AY5+MWPC_Y.AY6;
	  MWPC_PosY = (-20.*MWPC_Y.AY1 -12.*MWPC_Y.AY2-4.*MWPC_Y.AY3+4.*MWPC_Y.AY4+12.*MWPC_Y.AY5+20.*MWPC_Y.AY6)/MWPC_ChargeY;
	}
	if ((TriggerMap&(1<<8))!=0){
	  He6_Data_Reader->GetVal_MWPC_anode(AMWPC_anode1,AMWPC_anode2);
	  //AMWPC_anode1=AMWPC_anode1/6.54*7.51;
	  He6_Data_Reader->GetTime_MWPC_anode(TMWPC_anode1,TMWPC_anode2);
	  if (Tref>0.001){
	    if (TMWPC_anode1>0.0) TMWPC_rel[0] = (TMWPC_anode1 - Tref)*1.e3;	//ms
	    if (TMWPC_anode2>0.0) TMWPC_rel[1] = (TMWPC_anode2 - Tref)*1.e3;	//ms
	  }
	}
	if ((TriggerMap&(1<<12))!=0){
	  He6_Data_Reader->GetVal_LED(LED_A);
	  He6_Data_Reader->GetTime_LED(T_LED);
	}
	if ((TriggerMap&(1<<13))!=0){
	  He6_Data_Reader->GetVal_N2Laser(Q_N2Laser);
	  He6_Data_Reader->GetTime_N2Laser(T_N2Laser);
          T_N2Laser_lr = double(T_N2Laser);
	}

	//Calculated time differences
	if ((TriggerMap&258)==258) {
	  if (TMWPC_anode1>0.0) TBeta_diff[0] = (TMWPC_anode1-double(TPMT_A))*1.e9;	//ns
	  if (TMWPC_anode2>0.0) TBeta_diff[1] = (TMWPC_anode2-double(TPMT_A))*1.e9;	//ns
	}
	if ((TriggerMap&6)==6) {
	  TPMT_DA = double(TPMT_A-TPMT_D)*1.e9;	//ns
	}
	if ((TriggerMap&10)==10) {	//Requires both 
	  TOF = double(TMCP-TPMT_A)*1.e9;	//ns
	}
	if ((TriggerMap&40)==40) {
	  if(TX1>0.0)TMCP_anodes.TX1=(TX1-TMCP)*1.e9;
	  if(TX2>0.0)TMCP_anodes.TX2=(TX2-TMCP)*1.e9;
	  if(TY1>0.0)TMCP_anodes.TY1=(TY1-TMCP)*1.e9;
	  if(TY2>0.0)TMCP_anodes.TY2=(TY2-TMCP)*1.e9;
	}
	if ((TriggerMap&24)==24) {
	  TX1mTX2=(TX1-TX2)*1.e9; //ns
	  TX1pTX2=(TX1+TX2-2*TMCP)*1.e9;
	  TY1mTY2=(TY1-TY2)*1.e9;
	  TY1pTY2=(TY1+TY2-2*TMCP)*1.e9;
	}
	if ((TriggerMap&32)!=0 && (TriggerMap&8)==0) {
	  if (Tref>0.001){
	    TMCP_anodes.TX1=(TX1-Tref)*1.e3;
	    TMCP_anodes.TX2=(TX2-Tref)*1.e3;
	    TMCP_anodes.TY1=(TY1-Tref)*1.e3;
	    TMCP_anodes.TY2=(TY2-Tref)*1.e3;
	  }
	}
	if ((TriggerMap&320)==320) {
	  TMWPC_X.MW_TX1 = (TMWPC_X1 - TMWPC_anode1)*1.e9;	//ns
	  TMWPC_X.MW_TX2 = (TMWPC_X2 - TMWPC_anode1)*1.e9;	//ns
	  TMWPC_X.MW_TX3 = (TMWPC_X3 - TMWPC_anode1)*1.e9;	//ns
	  TMWPC_X.MW_TX4 = (TMWPC_X4 - TMWPC_anode1)*1.e9;	//ns
	  TMWPC_X.MW_TX5 = (TMWPC_X5 - TMWPC_anode1)*1.e9;	//ns
	  TMWPC_X.MW_TX6 = (TMWPC_X6 - TMWPC_anode1)*1.e9;	//ns
	}
	if ((TriggerMap&384)==384) {
	  TMWPC_Y.MW_TY1 = (TMWPC_Y1 - TMWPC_anode1)*1.e9;	//ns
	  TMWPC_Y.MW_TY2 = (TMWPC_Y2 - TMWPC_anode1)*1.e9;	//ns
	  TMWPC_Y.MW_TY3 = (TMWPC_Y3 - TMWPC_anode1)*1.e9;	//ns
	  TMWPC_Y.MW_TY4 = (TMWPC_Y4 - TMWPC_anode1)*1.e9;	//ns
	  TMWPC_Y.MW_TY5 = (TMWPC_Y5 - TMWPC_anode1)*1.e9;	//ns
	  TMWPC_Y.MW_TY6 = (TMWPC_Y6 - TMWPC_anode1)*1.e9;	//ns
	}
	if ((TriggerMap&8200)==8200) {
	  TOF_N2Laser = double(TMCP-T_N2Laser)*1.e9;
	}
	if ((TriggerMap&264)==264) {
	  if (TMWPC_anode1>0.0) TOFMM[0] = -(TMWPC_anode1-double(TMCP))*1.e9;	//ns
	  if (TMWPC_anode2>0.0) TOFMM[1] = -(TMWPC_anode2-double(TMCP))*1.e9;	//ns
	}

	//Fill Trees and Histograms
	//Tref
	if ((TriggerMap&1)!=0){
	  Tree_Tref->Fill();
	}
	//Scint Single, may overlap with MCP
	if ((TriggerMap&(1<<1))!=0 || (TriggerMap&(1<<2))!=0){
	  Tree_Scint->Fill();
	  h_Scint->Fill(QPMT_A[0]);
	}
	//MWPC Single, may overlap with MCP
	if ((TriggerMap&(1<<8))!=0 && (TriggerMap&(1<<1))==0){
	  Tree_MWPC->Fill();
	  h_MWPC1->Fill(AMWPC_anode1);
	  h_MWPC2->Fill(AMWPC_anode2);
	  h_MWPC_tot->Fill(AMWPC_anode1+AMWPC_anode2);
	  if ((TriggerMap&(1<<6))!=0){
	    h_Cathode_X->Fill(MWPC_PosX);
	    h_Charge_X->Fill(MWPC_ChargeX);
	    h_ChargeX_vs_Anode->Fill(MWPC_ChargeX,AMWPC_anode1+AMWPC_anode2);
	  }
	  if ((TriggerMap&(1<<7))!=0){
	    h_Cathode_Y->Fill(MWPC_PosY);
	    h_Charge_Y->Fill(MWPC_ChargeY);
	    h_ChargeY_vs_Anode->Fill(MWPC_ChargeY,AMWPC_anode1+AMWPC_anode2);
	  }
	  if ((TriggerMap&(1<<6))!=0 && (TriggerMap&(1<<7))!=0){
	    h_Cathode_Image->Fill(MWPC_PosX,MWPC_PosY);
	    h_ChargeX_vs_ChargeY->Fill(MWPC_ChargeX,MWPC_ChargeY);
	    h_TotCharge_vs_Anode->Fill(MWPC_ChargeX+MWPC_ChargeY,AMWPC_anode1+AMWPC_anode2);
	  }
	  if ((TriggerMap&(1<<8))!=0 && (TriggerMap&(1<<12))!=0){
	    AnodePos = 21.0*(AMWPC_anode2-AMWPC_anode1)/(AMWPC_anode1+AMWPC_anode2);
	    h_AnodeY->Fill(AnodePos);
	    if ((TriggerMap&(1<<6))!=0){
	      h_Anode_Image->Fill(MWPC_PosX,AnodePos);
	    }
	    if ((TriggerMap&(1<<7))!=0){
	      h_XCorrelation->Fill(MWPC_PosY,AnodePos);
	    }
	  }
	}
	//MCP Single, may overlap with Scint or MWPC
	if ((TriggerMap&(1<<3))!=0){
	  Tree_MCP->Fill();
	}
	//Beta double coincidence, may overlap with MCP, either pmt anode or dynode
	if ((TriggerMap&(1<<1))!=0 && (TriggerMap&(1<<8))!=0 && (TriggerMap&(1<<12))==0){
	  Tree_Beta->Fill();
	  h_Scint_Coin->Fill(QPMT_A[0]);
	  h_MWPC_Coin->Fill(AMWPC_anode1);
	  h_MWPC_Scint->Fill(AMWPC_anode1+AMWPC_anode2,QPMT_A[0]);
	  if ((TriggerMap&(1<<6))!=0){
	    h_Cathode_X->Fill(MWPC_PosX);
	    h_Charge_X->Fill(MWPC_ChargeX);
	    h_ChargeX_vs_Anode->Fill(MWPC_ChargeX,AMWPC_anode1);
	  }
	  if ((TriggerMap&(1<<7))!=0){
	    h_Cathode_Y->Fill(MWPC_PosY);
	    h_Charge_Y->Fill(MWPC_ChargeY);
	    h_ChargeY_vs_Anode->Fill(MWPC_ChargeY,AMWPC_anode1);
	  }
	  if ((TriggerMap&(1<<6))!=0 && (TriggerMap&(1<<7))!=0){
	    h_Cathode_Image_Coin->Fill(MWPC_PosX,MWPC_PosY);
	    h_ChargeX_vs_ChargeY->Fill(MWPC_ChargeX,MWPC_ChargeY);
	    h_TotCharge_vs_Anode->Fill(MWPC_ChargeX+MWPC_ChargeY,AMWPC_anode1);
	  }
	}
	//Double and Triple coincidence, require both anode and dynode of PMT

	if((TriggerMap&10)==10 && (TriggerMap&(1<<12))==0){
	  Tree_Double->Fill();

          h_TX1vQX1_double->Fill(QMCP_anodes.QX1,TMCP_anodes.TX1); 
          h_TX2vQX2_double->Fill(QMCP_anodes.QX2,TMCP_anodes.TX2); 
          h_TY1vQY1_double->Fill(QMCP_anodes.QY1,TMCP_anodes.TY1); 
          h_TY2vQY2_double->Fill(QMCP_anodes.QY2,TMCP_anodes.TY2); 
	}
	if((TriggerMap&264)==264){
	  Tree_DoubleMM->Fill();
	}
	if ((TriggerMap&266)==266 && (TriggerMap&(1<<12))==0){
	  Tree_Triple->Fill();
	  h_Scint_Coin->Fill(QPMT_A[0]);
	  h_MWPC_Coin->Fill(AMWPC_anode1);
	  h_TOF->Fill(TOF);
	  h_TOF_PMT->Fill(TOF,QPMT_A[0]);
	  //Accumulate effective events
	  //if (TOF>75 && TOF<400)EffEvent++;
	  if (TOF>50 && TOF<400)EffEvent++;

	  if ((TriggerMap&(1<<6))!=0){
	    h_Cathode_X->Fill(MWPC_PosX);
	    h_Charge_X->Fill(MWPC_ChargeX);
	    h_ChargeX_vs_Anode->Fill(MWPC_ChargeX,AMWPC_anode1);
	  }
	  if ((TriggerMap&(1<<7))!=0){
	    h_Cathode_Y->Fill(MWPC_PosY);
	    h_Charge_Y->Fill(MWPC_ChargeY);
	    h_ChargeY_vs_Anode->Fill(MWPC_ChargeY,AMWPC_anode1);
	  }
	  if ((TriggerMap&(1<<6))!=0 && (TriggerMap&(1<<7))!=0){
	    h_Cathode_Image_Coin->Fill(MWPC_PosX,MWPC_PosY);
	    h_ChargeX_vs_ChargeY->Fill(MWPC_ChargeX,MWPC_ChargeY);
	    h_TotCharge_vs_Anode->Fill(MWPC_ChargeX+MWPC_ChargeY,AMWPC_anode1);
	  }
	}
	if((TriggerMap&8216)==8216){
	  Tree_N2Laser->Fill();
	}
	//MCP histograms
	if ((TriggerMap&24)==24){
	  h_IMAGE->Fill(TX1mTX2,TY1mTY2);
	  h_X->Fill(TX1mTX2);
	  h_Y->Fill(TY1mTY2);
	  h_TX1pX2->Fill(TX1pTX2);
	  h_TY1pY2->Fill(TY1pTY2);
	  h_TMCP_rel->Fill(TMCP_rel);
          h_TX1vQX1->Fill(QMCP_anodes.QX1,TMCP_anodes.TX1); 
          h_TX2vQX2->Fill(QMCP_anodes.QX2,TMCP_anodes.TX2); 
          h_TY1vQY1->Fill(QMCP_anodes.QY1,TMCP_anodes.TY1); 
          h_TY2vQY2->Fill(QMCP_anodes.QY2,TMCP_anodes.TY2); 
	  //        cout<<TMCP_rel<<endl;
	}

	//Calibration trees
	if ((TriggerMap&(1<<12))!=0 && (TriggerMap&(1<<1))!=0){
	  Tree_PMT_STB->Fill();
	}
	//*****************For non-grouped data*********************
      }else if(ReadOutType==TYPET_ref){
	He6_Data_Reader->GetTime_Tref(Tref);
	Tree_Tref->Fill();
	//      cout<<Tref<<endl;
      }else if(ReadOutType==TYPEPMT_A || ReadOutType==TYPEPMT_D){
	He6_Data_Reader->GetTime_Scint(TPMT_A,TPMT_D);
	He6_Data_Reader->GetVal_Scint(QPMT_A,QPMT_D);
	TPMT_A_rel = (double(TPMT_A) - Tref)*1.e3;	//ms
	TPMT_DA = double(TPMT_A-TPMT_D)*1.e9;
	Tree_Scint->Fill();
	h_Scint->Fill(QPMT_A[0]);
      }else if(ReadOutType==TYPEMWPC_anode){
	He6_Data_Reader->GetVal_MWPC_anode(AMWPC_anode1,AMWPC_anode2);
	He6_Data_Reader->GetTime_MWPC_anode(TMWPC_anode1,TMWPC_anode2);
	if (TMWPC_anode1!=0)TMWPC_rel[0] = (TMWPC_anode1 - Tref)*1.e3;	//ms
	if (TMWPC_anode2!=0)TMWPC_rel[1] = (TMWPC_anode2 - Tref)*1.e3;	//ms
	Tree_MWPC_Iso->Fill();
	h_MWPC1->Fill(AMWPC_anode1);
	h_MWPC2->Fill(AMWPC_anode2);
      }else if(ReadOutType==TYPEMWPC_X){
	He6_Data_Reader->GetVal_MWPC_X(MWPC_X.AX1,MWPC_X.AX2,MWPC_X.AX3,MWPC_X.AX4,MWPC_X.AX5,MWPC_X.AX6);
	He6_Data_Reader->GetTime_MWPC_X(TMWPC_X1,TMWPC_X2,TMWPC_X3,TMWPC_X4,TMWPC_X5,TMWPC_X6);
	if (TMWPC_X1!=0) TMWPCX_rel = (TMWPC_X1-Tref)*1.e3;	//ms
	if (TMWPC_X2!=0) TMWPCX_rel = (TMWPC_X2-Tref)*1.e3;	//ms
	if (TMWPC_X3!=0) TMWPCX_rel = (TMWPC_X3-Tref)*1.e3;	//ms
	if (TMWPC_X4!=0) TMWPCX_rel = (TMWPC_X4-Tref)*1.e3;	//ms
	if (TMWPC_X5!=0) TMWPCX_rel = (TMWPC_X5-Tref)*1.e3;	//ms
	if (TMWPC_X6!=0) TMWPCX_rel = (TMWPC_X6-Tref)*1.e3;	//ms
	Tree_MWPCX_Iso->Fill();
      }else if(ReadOutType==TYPEMWPC_Y){
	He6_Data_Reader->GetVal_MWPC_Y(MWPC_Y.AY1,MWPC_Y.AY2,MWPC_Y.AY3,MWPC_Y.AY4,MWPC_Y.AY5,MWPC_Y.AY6);
	He6_Data_Reader->GetTime_MWPC_Y(TMWPC_Y1,TMWPC_Y2,TMWPC_Y3,TMWPC_Y4,TMWPC_Y5,TMWPC_Y6);
	if (TMWPC_Y1!=0) TMWPCY_rel = (TMWPC_Y1-Tref)*1.e3;	//ms
	if (TMWPC_Y2!=0) TMWPCY_rel = (TMWPC_Y2-Tref)*1.e3;	//ms
	if (TMWPC_Y3!=0) TMWPCY_rel = (TMWPC_Y3-Tref)*1.e3;	//ms
	if (TMWPC_Y4!=0) TMWPCY_rel = (TMWPC_Y4-Tref)*1.e3;	//ms
	if (TMWPC_Y5!=0) TMWPCY_rel = (TMWPC_Y5-Tref)*1.e3;	//ms
	if (TMWPC_Y6!=0) TMWPCY_rel = (TMWPC_Y6-Tref)*1.e3;	//ms
	Tree_MWPCY_Iso->Fill();
      }else if(ReadOutType==TYPEMCP){
	He6_Data_Reader->GetVal_MCP(QMCP);
	He6_Data_Reader->GetTime_MCP(TMCP);
	TMCP_rel = (TMCP - Tref)*1.e3;	//ms
	Tree_MCP_Iso->Fill();
      }else if(ReadOutType==TYPEMCP_anodes){
	He6_Data_Reader->GetVal_MCP_anodes(QMCP_anodes.QX1,QMCP_anodes.QX2,QMCP_anodes.QY1,QMCP_anodes.QY2);
	He6_Data_Reader->GetTime_MCP_anodes(TX1,TX2,TY1,TY2);
	if (TX1!=0) TMCP_anodes_rel = (double(TX1) - Tref)*1.e3;	//ms
	if (TX2!=0) TMCP_anodes_rel = (double(TX2) - Tref)*1.e3;	//ms
	if (TY1!=0) TMCP_anodes_rel = (double(TY1) - Tref)*1.e3;	//ms
	if (TY2!=0) TMCP_anodes_rel = (double(TY2) - Tref)*1.e3;	//ms
	Tree_MCP_anode_Iso->Fill();
      }else{
	;
      }

      //complete event
      n++;
    }//reader loop

    //  close input file
    He6_Data_Reader->closeInputFile();
  }//End file loop
  //--------------------------------------------------// 

  string sfilename = string(ExpDataDirect) + string("/../Spectra/Faster2Root/") + string(argv[1]) + "_spectra.pdf";
  
  gStyle->SetPalette(1);	

  TCanvas *c1 = new TCanvas("c1","Image");
  c1->Clear();
  c1->Divide(3,2);
  c1->cd(1);
  h_Cathode_Image->Draw("colz");
  c1->cd(2);
  h_Cathode_Y->Draw();
  c1->cd(3);
  h_Cathode_X->Draw();
  c1->cd(4);
  h_Anode_Image->Draw("colz");
  c1->cd(5);
  h_AnodeY->Draw();
  c1->cd(6);
  h_XCorrelation->Draw("colz");
  c1->Update();


  spectrumfile = sfilename +"(";	
  //c1->SaveAs(spectrumfile.c_str());

  TCanvas *c2 = new TCanvas("c2","Charge");
  c2->Clear();
  c2->Divide(3,2);
  c2->cd(1);
  h_MWPC1->Draw();
  c2->cd(2);
  h_MWPC2->Draw();
  c2->cd(3);
  h_MWPC_tot->Draw();
  c2->cd(4);
  h_ChargeX_vs_Anode->Draw("colz");
  c2->cd(5);
  h_ChargeY_vs_Anode->Draw("colz");
  c2->cd(6);
  h_ChargeX_vs_ChargeY->Draw("colz");

  c2->Update();

  spectrumfile = sfilename;	
  //c2->SaveAs(spectrumfile.c_str());

  TCanvas *c3 = new TCanvas("c3","TOF",0,0,800,1600);
  c3->Divide(2,1);
  c3->cd(1);
// h_MWPC_tot->Draw();
  h_TOF->Draw();
  c3->cd(2);
  h_TOF_PMT->Draw();

  spectrumfile =sfilename + ")";	
  //c3->SaveAs(spectrumfile.c_str());

  int bytesWritten = f->Write ();
  cout << "Bytes written: "<< bytesWritten<<endl;
  cout<< "File path: "<< fName<<endl;
  f->Close ();

  delete He6_Data_Reader;
  delete f;
  //Calculate time and rate
  int lastEvent = GroupTimeArray.size();
  double TotTime = GroupTimeArray[lastEvent-1]-GroupTimeArray[0];

  cout <<argv[1]<<" " << EffEvent << " "<<TotTime<<" "<< EffEvent/TotTime<<endl;
  logfile <<argv[1]<<" " << EffEvent << " "<<TotTime<<" "<< EffEvent/TotTime<<endl;
  logfile.close();
  return EXIT_SUCCESS;
}


