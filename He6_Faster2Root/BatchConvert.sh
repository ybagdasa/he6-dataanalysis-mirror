#!/bin/bash
RunID_begin=$1
RunID_end=$2
echo Run ID Range: $RunID_begin $RunID_end
for RunID in $( seq $RunID_begin $RunID_end )
    do
    InFileName=`printf "N2_060418_v%d" $RunID`
    OutFileName=`printf "N2_060418_%04d" $(($RunID))`
    echo Converting $InFileName to $OutFileName
    ./He6_Faster2Root $InFileName 1 1 $OutFileName
done

